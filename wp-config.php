<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ibo' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'test' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('WP_MEMORY_LIMIT', '256M');



/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          ':1PI3d2l@YE[ r(:L&suw5a,F@:Q kJ=g9o|:d8]KZy0w[79AJsQ:]5!+I9yLCoo' );
define( 'SECURE_AUTH_KEY',   'XwMn-4_FZMv-[k.F:`IaOX|jI,F4f=%X`yfX8*DR1M=hCIxk)ePfqmZj2p|OX/J$' );
define( 'LOGGED_IN_KEY',     'J*x4EdY*6TGp4`p;0Kd|>;@w~`L2~z8: l`IE|sRg&?Y$@tXb#7Xz*wQGy: f.4(' );
define( 'NONCE_KEY',         '?3B{%>#`Q,2t0@L5r|Ie^.*>]}3aR>[tu7_i4hss({R^PSOmLDoU_z&-7Y3!){u[' );
define( 'AUTH_SALT',         ',>v9aqDiWl%`,e,cv8 xizi9onqKle?1uY?FPxZv[Xc>m.3DQ_ftq-dB.<f,EX9[' );
define( 'SECURE_AUTH_SALT',  '`+JMlMSCa8C%b+nsMg8w}o`n^|+orj9u)F)xrj)s@~x4,/g@Azj-=Df$qNWs+-P<' );
define( 'LOGGED_IN_SALT',    '~o4?&ZEA[)$2P-{9+arVWo}S2Yk:8+5c8RRGT*B}H0tb%#Qf$0hq}Y7A%K.3>8$L' );
define( 'NONCE_SALT',        '){da) 1uQ!X<(ORBJZ~t`Uso?hK=&I3yO[*uh+LZy:5?OJ!;-6LJQP-QW!JB- zg' );
define( 'WP_CACHE_KEY_SALT', 'm8HtK|I}Y%C}?OS[o(%OiR,vZ3WwWtBi:gT[s@e!-i!m&Z+j}*SQ58{l:@X!ufBB' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
//Kinsta hack cleanup Mon Jun 17 22:20:51 UTC 2019
define('DISALLOW_FILE_EDIT', false);
