<?php if ( is_active_sidebar( 'footer_top' ) ) { ?>

		<?php 
			$footer_enabled = stm_option('footer_top');
			$widget_areas = stm_option('footer_first_columns');
			if( ! $widget_areas ){
				$widget_areas = 4;
			};
		?>
		<?php if ( $footer_enabled ) { ?>
		<div id="footer_top">
			<div class="footer_widgets_wrapper">
				<div class="container">
					<div class="row row-eq-height">
						<div class="col-md-6 col-sm-8">
							<div class="clearfix">
								<div class="pull-left">
									<div class="widgets <?php echo 'cols_' . esc_attr( $widget_areas ); ?> clearfix">
										<?php dynamic_sidebar( 'footer_top' ); ?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-4">
							<div class="clearfix">
								<div class="pull-right xs-pull-left hidden-sm hidden-xs">
									<ul class="footer_menu heading_font clearfix">
										<?php
											wp_nav_menu( array(
												'menu'              => 'secondary',
												'theme_location'    => 'secondary',
												'depth'             => 1,
												'container'         => false,
												'menu_class'        => 'header-menu clearfix',
												'items_wrap'        => '%3$s',
												'fallback_cb' => false
												)
											);
										?>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<?php } ?>

<?php } ?>