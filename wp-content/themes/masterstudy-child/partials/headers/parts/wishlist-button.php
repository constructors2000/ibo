<?php
$icon = (empty($icon)) ? 'lnr lnr-heart' : esc_attr($icon);
$class = (empty($class)) ? '' : esc_attr($class);
?>

<div class="stm_lms_wishlist_button" data-toggle="tooltip" data-placement="bottom" title="<?php esc_attr_e('Wishlist', 'masterstudy-lms-learning-management-system'); ?>">
    <a href="<?php echo esc_url(STM_LMS_User::wishlist_url()); ?>" data-text="<?php esc_html_e('Favorites', 'masterstudy-lms-learning-management-system'); ?>">
        <i class="<?php echo esc_attr($icon); ?> <?php echo esc_attr($class); ?>"></i>
    </a>
	
			<?php if (is_user_logged_in()): ?>
			<?php $current_user = wp_get_current_user(); ?>
			<?php $wishlist = STM_LMS_User::get_wishlist($current_user->ID); ?>
		<?php else: ?>
			<?php if(!empty($_COOKIE['stm_lms_wishlist'])):
					$wishlist = sanitize_text_field($_COOKIE['stm_lms_wishlist']);
					$wishlist = ltrim($wishlist, ',');
					$wishlist = explode(',', $wishlist);
				  endif;
			?>
		<?php endif; ?>
						<?php
		$count  = 0;
		if(!empty($wishlist))
		    $count = count($wishlist);
		if ( $count > 0 ) { ?>
			<span class="wishlist-count"><?php echo esc_html( $count ); ?></span>
		<?php }?>
	
	<div class="wishlist_list">
		
		<?php if (!empty($wishlist)):?>
		
		<?php $args = array(
				'post_type' => 'product',
				'posts_per_page' => 12,
				'post__in' => $wishlist
			  );
		?>
		
		<ul class="list-unstyled">
			<?php
				$loop = new WP_Query( $args );
				if ( $loop->have_posts() ) {
					while ( $loop->have_posts() ) : $loop->the_post();
						?>
						<li>
							<a href="<?php the_permalink() ?>"  title="<?php esc_attr_e('View product', 'masterstudy') ?> - <?php the_title(); ?>">
								<?php the_title(); ?>
							</a>
						</li>
						<?php
					endwhile;
				}
				else {
					?>
					<li class="empty"><?php _e( 'Your whish list is empty.', 'masterstudy'); ?></li>
					<li class="empty"><a href="/courses"><?php _e( 'Explore Products', 'masterstudy'); ?></a></li>
					<?php
				}
			?>
		</ul>
		<p class="buttons">
			<a href="/lms-wishlist" class="btn btn-default"><?php _e( 'View Whislist', 'masterstudy'); ?></a>
		</p>
		<?php else: ?>
			<ul class="list-unstyled">
				<li class="empty"><?php _e( 'Your whish list is empty.', 'masterstudy'); ?></li>
				<li class="empty"><a href="/courses"><?php _e( 'Explore Products', 'masterstudy'); ?></a></li>
			</ul>
		<?php endif; ?>
		
	</div>
</div>