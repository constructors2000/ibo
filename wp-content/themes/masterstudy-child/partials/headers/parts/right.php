<?php if (class_exists('STM_LMS_Templates')): ?>
	<?php if (is_user_logged_in()): ?>
		<?php get_template_part('partials/headers/parts/wishlist-button'); ?>
				<?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
				$count = WC()->cart->cart_contents_count;
				?><div class="cart-contents"><?php
				if ( $count > 0 ) { ?>
					<span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
				<?php }
				?><div class="mini-cart"><?php woocommerce_mini_cart(); ?></div></div>
		<?php } ?>
		<?php STM_LMS_Templates::show_lms_template('global/settings-button'); ?>
		<?php STM_LMS_Templates::show_lms_template('global/account-dropdown'); ?>
	<?php else: ?>
		<?php get_template_part('partials/headers/parts/wishlist-button'); ?>
		
		<?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
				$count = WC()->cart->cart_contents_count;
				?><div class="cart-contents"><?php
				if ( $count > 0 ) { ?>
					<span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
				<?php }
				?><div class="mini-cart"><?php woocommerce_mini_cart(); ?></div></div>
		<?php } ?>
		<?php get_template_part('partials/headers/parts/log-in'); ?>
		<?php get_template_part('partials/headers/parts/sign-up'); ?>
	<?php endif; ?>
<?php endif;