<?php
wp_enqueue_script('vue.js');
wp_enqueue_script('vue-resource.js');

if (function_exists('stm_lms_register_style')) {
	enqueue_login_script();
	stm_lms_register_style('login');
	stm_lms_register_style('register');
	enqueue_register_script();
}
?>

<div class="sign-in">
	<a href="https://xbackoffice.thinkmelius.com/"
	   class="stm_lms_log_in">
		<span><?php esc_html_e('Sign in', 'masterstudy'); ?></span>
		<!--<span class="lnr lnr-chevron-down"></span>-->
	</a>
	<!--<ul class="list-unstyled">
		<li><a href="https://www.meliusnode.com/learn" target="_blank">MELiUS Node <span class="lnr lnr-chevron-right"></span></a></li>
		<li><a href="http://xbackoffice.thinkmelius.com" target="_blank">MELiUS BAckoffice <span class="lnr lnr-chevron-right"></span></a></li>
	</ul>-->
</div>