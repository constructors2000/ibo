<?php
//error_reporting(E_ERROR | E_WARNING);
//ini_set("display_errors", 1);
require_once(__DIR__ . '/extension/carbon/src/Carbon/Carbon.php');
require_once(__DIR__ . '/background/authentication.php');


use Carbon\Carbon;


function theme_enqueue_styles()
{
    // Styles
    wp_enqueue_style('boostrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', NULL, STM_THEME_VERSION, 'all');
    wp_enqueue_style('font-awesome-min', get_template_directory_uri() . '/assets/css/font-awesome.min.css', NULL, STM_THEME_VERSION, 'all');
    wp_enqueue_style('font-icomoon', get_template_directory_uri() . '/assets/css/icomoon.fonts.css', NULL, STM_THEME_VERSION, 'all');
    wp_enqueue_style('fancyboxcss', get_template_directory_uri() . '/assets/css/jquery.fancybox.css', NULL, STM_THEME_VERSION, 'all');
    wp_enqueue_style('select2-min', get_template_directory_uri() . '/assets/css/select2.min.css', NULL, STM_THEME_VERSION, 'all');
    wp_enqueue_style('theme-style-less', get_template_directory_uri() . '/assets/css/styles.css', NULL, STM_THEME_VERSION, 'all');

    // Animations
    if (!wp_is_mobile()) {
        wp_enqueue_style('theme-style-animation', get_template_directory_uri() . '/assets/css/animation.css', NULL, STM_THEME_VERSION, 'all');
    }

    // Theme main stylesheet
    wp_enqueue_style('theme-style', get_stylesheet_uri(), null, STM_THEME_VERSION, 'all');

    // FrontEndCustomizer
//    wp_enqueue_style( 'skin_red_green', get_template_directory_uri() . '/assets/css/skins/skin_red_green.css', NULL, STM_THEME_VERSION, 'all' );
//    wp_enqueue_style( 'skin_blue_green', get_template_directory_uri() . '/assets/css/skins/skin_blue_green.css', NULL, STM_THEME_VERSION, 'all' );
//    wp_enqueue_style( 'skin_red_brown', get_template_directory_uri() . '/assets/css/skins/skin_red_brown.css', NULL, STM_THEME_VERSION, 'all' );
//    wp_enqueue_style( 'skin_custom_color', get_template_directory_uri() . '/assets/css/skins/skin_custom_color.css', NULL, STM_THEME_VERSION, 'all' );
}

add_action('wp_enqueue_scripts', 'theme_enqueue_styles');

function themename_add_to_cart_fragment($fragments)
{
    ob_start();

    if (class_exists('STM_LMS_Templates')): ?>
        <?php if (is_user_logged_in()): ?>

            <?php if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
                $count = WC()->cart->cart_contents_count;
                ?>
                <div class="cart-contents"><?php
                    if ($count > 0) { ?>
                        <span class="cart-contents-count"><?php echo esc_html($count); ?></span>
                    <?php }
                    ?>
                    <div class="mini-cart"><?php woocommerce_mini_cart(); ?></div>
                </div>
            <?php } ?>

        <?php else: ?>

            <?php if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
                $count = WC()->cart->cart_contents_count;
                ?>
                <div class="cart-contents"><?php
                    if ($count > 0) { ?>
                        <span class="cart-contents-count"><?php echo esc_html($count); ?></span>
                    <?php }
                    ?>
                    <div class="mini-cart"><?php woocommerce_mini_cart(); ?></div>
                </div>
            <?php } ?>

        <?php endif; ?>
    <?php endif;

    $fragments['div.cart-contents'] = ob_get_clean();

    return $fragments;
}

add_filter('woocommerce_add_to_cart_fragments', 'themename_add_to_cart_fragment');

/**
 * Custome checkout total
 * */
function themename_add_to_checkout_total($fragments)
{
    ob_start();
    ?>

    <div class="checkout-total-info">
        <h2>Product Information</h2>
        <?php do_action('woocommerce_checkout_before_order_review'); ?>

        <div id="order_review" class="woocommerce-checkout-review-order">
            <?php do_action('woocommerce_checkout_order_review'); ?>
        </div>

        <div class="col-md-12 total-sum">
            <div class="row  h-100 align-items-center">
                <div class="col-md-6 col-6">
                    <a href="<?php echo wc_get_cart_url(); ?>">Edit Order</a>
                </div>
                <div class="col-md-6 col-6">
                    <?php
                    global $woocommerce;

                    $discount_total = 0;
                    $regular_total = 0;
                    foreach ($woocommerce->cart->get_cart() as $cart_item_key => $values) {

                        $_product = $values['data'];

                        if ($_product->is_on_sale()) {
                            $regular_price = $_product->get_regular_price();
                            $sale_price = $_product->get_sale_price();
                            $regular_total += $regular_price * $values['quantity'];
                            $discount = ($regular_price - $sale_price) * $values['quantity'];
                            $discount_total += $discount;
                        }

                    }

                    $discount_total = ($discount_total / $regular_total) * 100; ?>
                    <h5><?php esc_html_e('Total: ', 'masterstudy'); ?><?php wc_cart_totals_order_total_html();
                        if ($discount_total > 0) { ?> <span
                                class="cart-discount"><?php echo wc_price($regular_total); ?></span> <span
                                class="discount-percent"><?php echo round($discount_total) . '%off'; ?></span><?php } ?>
                    </h5>
                </div>
            </div>
        </div>
    </div>

    <?php

    $fragments['div.checkout-total-info'] = ob_get_clean();

    return $fragments;
}

add_filter('woocommerce_add_to_cart_fragments', 'themename_add_to_checkout_total');


wp_dequeue_script('testimonials');
wp_enqueue_script('owl.carousel');
wp_enqueue_script('testimonials-style-3', get_stylesheet_directory_uri() . '/assets/js/vc_modules/testimonials/style_3.js', array('owl.carousel'), '1.0.0', true);

wp_enqueue_style('product-style', get_stylesheet_directory_uri() . '/assets/css/products-style.css', false);

//wp_enqueue_script( 'woo-filter', get_stylesheet_directory_uri() . '/assets/js/woo-filter.js', array('jquery') );
//wp_enqueue_script( 'wishlist-ajax', get_stylesheet_directory_uri() . '/assets/js/wishlist-ajax.js', array('jquery') );
//wp_enqueue_script( 'go-to-cart', get_stylesheet_directory_uri() . '/assets/js/go-to-cart.js', array('jquery') );
//wp_enqueue_script( 'invoice-btn', get_stylesheet_directory_uri() . '/assets/js/invoice-btn.js', array('jquery') );
//wp_enqueue_script( 'hover-box', get_stylesheet_directory_uri() . '/assets/js/hover-box.js', array('jquery') );

wp_enqueue_script('hover-box', get_stylesheet_directory_uri() . '/assets/js/child.js', array('jquery'));

add_theme_support('woocommerce', array(
    'thumbnail_image_width' => 150
));

remove_action('woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20);
add_action('woocommerce_checkout_after_order_review', 'woocommerce_checkout_payment', 10);


/**
 * Change email
 * */
function change_admin_email_subject($subject, $order)
{
    global $woocommerce;

    $subject = sprintf('OSA - (USD) - Product Purchase Alert!! %s', $order->id);

    return $subject;
}

add_filter('woocommerce_email_subject_new_order', 'change_admin_email_subject', 1, 2);

/**
 * Change generate payment request
 * */
add_filter('wc_stripe_generate_payment_request', function ($post_data, $order) {

    $product_names = '';
    foreach ($order->get_items() as $item) {
        $product_names .= $item['name'];
        $product_names .= ', ';
    }

    $post_data['description'] = sprintf(
        '%1$s – Order %2$s',
        $product_names,
        $order->get_order_number()
    );

    return $post_data;
}, 10, 2);


/**
 * Add Custom Field to Product Variations - WooCommerce
 */

/**
 * 1. Add custom field input @ Product Data > Variations > Single Variation
 * */
function add_custom_field_to_variations($loop, $variation_data, $variation)
{
    ?>
    <div style="clear: both;"><?php
        woocommerce_wp_text_input(array(
                'id' => 'cv_points[' . $loop . ']',
                'class' => 'short wc_input_decimal',
                'label' => __('CV Points', 'woocommerce'),
                'value' => get_post_meta($variation->ID, 'cv_points', true)
            )
        );
        ?></div>
    <div style="clear: both;"><?php
        woocommerce_wp_text_input(array(
                'id' => 'qv_points[' . $loop . ']',
                'class' => 'short wc_input_decimal	',
                'label' => __('QV Points', 'woocommerce'),
                'value' => get_post_meta($variation->ID, 'qv_points', true)
            )
        );
        ?></div>
    <div style="clear: both;"><?php
    woocommerce_wp_text_input(array(
            'id' => 'product_id_xC[' . $loop . ']',
            'class' => 'short wc_input_decimal	',
            'label' => __('Product ID', 'woocommerce'),
            'value' => get_post_meta($variation->ID, 'product_id_xC', true)
        )
    );
    ?></div><?php
}

add_action('woocommerce_variation_options_pricing', 'add_custom_field_to_variations', 10, 3);

/**
 * 2. Save custom field on product variation save
 * */
function save_custom_field_variations($variation_id, $i)
{
    $cv_points = $_POST['cv_points'][$i];
    if (!empty($cv_points)) {
        update_post_meta($variation_id, 'cv_points', esc_attr($cv_points));
    } else delete_post_meta($variation_id, 'cv_points');

    $qv_points = $_POST['qv_points'][$i];
    if (!empty($qv_points)) {
        update_post_meta($variation_id, 'qv_points', esc_attr($qv_points));
    } else delete_post_meta($variation_id, 'qv_points');

    $product_id_xC = $_POST['product_id_xC'][$i];
    if (!empty($product_id_xC)) {
        update_post_meta($variation_id, 'product_id_xC', esc_attr($product_id_xC));
    } else delete_post_meta($variation_id, 'product_id_xC');
}

add_action('woocommerce_save_product_variation', 'save_custom_field_variations', 10, 2);

/**
 * 3. Store custom field value into variation data
 * */
function add_custom_field_variation_data($variations)
{
    $variations['cv_points'] = '<div class="woocommerce_custom_field">CV Points: <span>' . get_post_meta($variations['variation_id'], 'cv_points', true) . '</span></div>';
    $variations['qv_points'] = '<div class="woocommerce_custom_field">QV Points: <span>' . get_post_meta($variations['variation_id'], 'qv_points', true) . '</span></div>';
    $variations['product_id_xC'] = '<div class="woocommerce_custom_field">Product ID: <span>' . get_post_meta($variations['variation_id'], 'product_id_xC', true) . '</span></div>';
    return $variations;
}

add_filter('woocommerce_available_variation', 'add_custom_field_variation_data');
add_filter('woocommerce_show_variation_price', '__return_true');

function filter_woocommerce_coupon_message($msg, $msg_code, $instance)
{
    $msg = 'Coupon code applied successfully. CV/QV points shall be adjusted accordance to the price.';
    return $msg;
}

add_filter('woocommerce_coupon_message', 'filter_woocommerce_coupon_message', 10, 3);
add_filter('woocommerce_checkout_get_value', '__return_empty_string', 10);

/**
 * xC Order ID custom field on WC Order post type
 */
function xC_data($order)
{
    woocommerce_wp_text_input(array(
        'id' => 'xC_Order_Id',
        'label' => __("xC Order Id:", "woocommerce"),
        'value' => get_post_meta($order->get_id(), 'xC_Order_Id', true),
        'wrapper_class' => 'form-field-wide',
        'custom_attributes' => array('readonly' => 'readonly'),
    ));

    woocommerce_wp_text_input(array(
        'id' => 'xC_Order_Legacy_Number',
        'label' => __("xC Order Legacy Number:", "woocommerce"),
        'value' => get_post_meta($order->get_id(), 'xC_Order_Legacy_Number', true),
        'wrapper_class' => 'form-field-wide',
        'custom_attributes' => array('readonly' => 'readonly'),
    ));

    woocommerce_wp_text_input(array(
        'id' => 'xC_Order_Invoice_Number',
        'label' => __("xC Order Invoice Number:", "woocommerce"),
        'value' => get_post_meta($order->get_id(), 'xC_Order_Invoice_Number', true),
        'wrapper_class' => 'form-field-wide',
        'custom_attributes' => array('readonly' => 'readonly'),
    ));
}

add_action('woocommerce_admin_order_data_after_order_details', 'xC_data', 12, 1);

/**
 * Show all enable gateways (include not supported method )
 * @param $allowed_gateways
 * @return mixed
 */
function change_wc_gateway($allowed_gateways)
{
    foreach (WC()->payment_gateways->payment_gateways as $payment_gateway) {
        if ($payment_gateway->enabled == "yes") {
            $allowed_gateways[$payment_gateway->id] = $payment_gateway;
        }
    }

    return $allowed_gateways;
}

add_filter('woocommerce_available_payment_gateways', 'change_wc_gateway', 999, 1);

/**
 * Init for admin
 * */
function admin_init()
{
    add_meta_box('coupon_cv-meta', 'CV Points', 'coupon_cv_points', 'shop_coupon', 'side', 'low');
    add_meta_box('coupon_qv-meta', 'QV Points', 'coupon_qv_points', 'shop_coupon', 'side', 'low');
}

add_action('admin_init', 'admin_init');

/**
 *
 * */
function coupon_cv_points()
{
    global $post;
    $custom = get_post_custom($post->ID);
    $coupon_cv = $custom['coupon_cv'][0];
    ?>
    <label>CV Points:</label>
    <input name="coupon_cv" value="<?php echo $coupon_cv; ?>"/>
    <?php
}

/**
 *
 * */
function coupon_qv_points()
{
    global $post;
    $custom = get_post_custom($post->ID);
    $coupon_qv = $custom['coupon_qv'][0];
    ?>
    <label>QV Points:</label>
    <input name="coupon_qv" value="<?php echo $coupon_qv; ?>"/>
    <?php
}

/**
 *
 * */
function save_coupon_cv()
{
    global $post;

    update_post_meta($post->ID, 'coupon_cv', $_POST['coupon_cv']);
}

add_action('save_post', 'save_coupon_cv');

/**
 *
 * */
function save_coupon_qv()
{
    global $post;

    update_post_meta($post->ID, 'coupon_qv', $_POST['coupon_qv']);
}

add_action('save_post', 'save_coupon_qv');

/**
 * Custom gateways icon
 * @param $icon
 * @param $gateway_id
 * @return string
 */
function custom_payment_gateway_icons($icon, $gateway_id)
{

    foreach (WC()->payment_gateways->get_available_payment_gateways() as $gateway)
        if ($gateway->id == $gateway_id) {
            $title = $gateway->get_title();
            break;
        }

    // The path (subfolder name(s) in the active theme)
    $path = '/wp-content/plugins/woocommerce-gateway-stripe/assets/images/';

    // Setting (or not) a custom icon to the payment IDs
    if ($gateway_id == 'abzer_networkonline')
        $icon = '<img class="stripe-amex-icon stripe-icon" src="' . WC_HTTPS::force_https_url("$path/visa.svg") . '" alt="' . esc_attr($title) . '" width="37px" /><img class="stripe-amex-icon stripe-icon" src="' . WC_HTTPS::force_https_url("$path/amex.svg") . '" alt="' . esc_attr($title) . '" width="37px" /><img class="stripe-amex-icon stripe-icon" src="' . WC_HTTPS::force_https_url("$path/mastercard.svg") . '" alt="' . esc_attr($title) . '" width="37px" />';
    else if ($gateway_id == 'coinpayments')
        $icon = '<img class="stripe-amex-icon stripe-icon" src="' . WC_HTTPS::force_https_url("/wp-content/themes/masterstudy-child/assets/images/bitcoin.svg") . '" alt="' . esc_attr($title) . '" width="37px" />';

    return $icon;
}

add_filter('woocommerce_gateway_icon', 'custom_payment_gateway_icons', 10, 2);

wp_enqueue_script('validation-user', get_stylesheet_directory_uri() . '/assets/js/user_validation.js', array(), '1.0.0', false);

/**
 * Main init
 * */
add_action('init', function () {
    if (!is_admin() && !session_id()) {
        session_start();
    }


    if (!empty($_GET['dist'])) {
        $_SESSION['EnrollerId'] = $_GET['dist'];
    }

    if(stripos($_SERVER['HTTP_REFERER'], 'landing-')){
        add_filter( 'wp_setup_nav_menu_item', 'filter_function_name' );
    }

    /**
     * This Temp solution because we use custom ajax login on checkout
     *
     * */
    if (!is_admin() && !empty(WC()->session)) {
        $user_id = WC()->session->get('wc_user_id');

        if (!empty($user_id) && !is_ajax()) {
            $_user = get_user_by('id', $user_id);

            if (!empty($_user)) {
                wp_set_current_user($_user->ID, $_user->display_name);
                wp_set_auth_cookie($_user->ID);
                WC()->session->set('wc_user_id', false);
            }
        }
    }

    /**
     * This block work if user login on back office and redirect to shop with distributorId (distributorId_xirect_64)[dt]
     * @example ibo.shop.com?dt=2JxO5NNWdwgqbHDr5A8SSw==
     *
     * $distributor_info its array after method getDistributorInfo
     * */
    $distributor_info = null;
    if (!empty($_GET['dt'])) {
        global $wp;

        /*
         * Logout if old user logged
         * */
        if (is_user_logged_in()) {
            wp_logout();
        }

        $distributorId = str_replace(' ', '+', $_GET['dt']);
        try {
            $distributor_info = BackOfficeApi::getDistributorInfo($distributorId);
        } catch (Exception $exception) {
            $log = date("F j, Y, g:i a") . "distributor Id {$distributorId} NOT FOND" . PHP_EOL;
            file_put_contents('xirect.log', $log, FILE_APPEND);
            wp_die($log);
        }

        /**
         * Get user by $distributorId in usermeta DB table when key distributorId_xirect_64
         * */
        $users = get_users(array('meta_key' => 'distributorId_xirect_64', 'meta_compare' => '=', 'meta_value' => $distributorId));

        if (!empty($users) && is_array($users)) {
            $user = $users[0];
        }

        if (!empty($user)) {
            $legacynumber = get_user_meta($user->ID, 'User ID', true);

            if (!empty($legacynumber)) {
                if ($distributor_info['Data']['legacynumber'] != $legacynumber) {
                    /**
                     * If legacynumber unequal
                     * */
                    $log = date("F j, Y, g:i a") .
                        "distributor Id {$distributorId} FOND but legacynumber not correct({$distributor_info['Data']['legacynumber']}!={$legacynumber})" .
                        PHP_EOL;
                    file_put_contents('xirect.log', $log, FILE_APPEND);
                }
            } else {
                /**
                 * If legacynumber empty
                 * */
                $log = date("F j, Y, g:i a") .
                    "distributor Id {$distributorId} FOND but legacynumber not found" . PHP_EOL;
                file_put_contents('xirect.log', $log, FILE_APPEND);
            }
            wp_set_current_user($user->ID, $distributor_info['Data']['username']);
        } else {

            /**
             * Fin or create user by email
             * */
            $user_e = get_user_by('email', $distributor_info['Data']['email']);
            $user_l = get_user_by('user_login', $distributor_info['Data']['username']);

            if (empty($user_e)) {
                if (!empty($user_l)) {
                    $distributor_info['Data']['username'] = wp_generate_uuid4();
                }

                /**
                 * Create new user in shop with random password.
                 * This solution for old user which not found in shop.
                 * */
                $user_data = [
                    'user_login' => sanitize_user($distributor_info['Data']['username']),
                    'user_email' => sanitize_email($distributor_info['Data']['email']),
                    'user_pass' => wp_generate_password(8, false),
                    'user_nicename' => $distributor_info['Data']['displayname'],
                    'display_name' => $distributor_info['Data']['displayname'],
                ];

                $user_id = wp_insert_user($user_data);
                wp_set_current_user($user_id, $distributor_info['Data']['username']);
                update_user_meta($user_id, 'back_office_insert', date('Y-m-d H:i'));
            } else {
                /**
                 * Set current user
                 * */
                wp_set_current_user($user_e->ID, $distributor_info['Data']['username']);
                update_user_meta($user_e->ID, 'back_office_insert', date('Y-m-d H:i'));
            }
        }

        $user = wp_get_current_user();

        wp_set_current_user($user->ID, $user->display_name);

        update_user_meta($user->ID, 'distributorId_xirect_64', $distributorId);
        update_user_meta($user->ID, 'User ID', $distributor_info['Data']['legacynumber']);
        update_user_meta($user->ID, 'login_time', Carbon::now()->timestamp);

        //login

        wp_set_auth_cookie($user->ID);


        /**
         * logout if user didn`t pay
         * */
        if (is_user_logged_in()) {

            $user = wp_get_current_user();
            $roles = ( array )$user->roles;

            if (empty($roles) or empty($roles[0])) {
                /*
                 * Show page (you didn't pay )
                 * */
                wp_logout();
            }
        }

        if (is_user_logged_in()) {

            $distributorId = get_user_meta(get_current_user_id(), 'distributorId_xirect_64', true);

            if (!empty($distributorId)) {
                $distributor_info = BackOfficeApi::getDistributorInfo($distributorId);
                $end_subscription = $distributor_info['Data']['Endsubscription'];

                if (!empty($end_subscription) && (Carbon::createFromFormat('m/d/Y H:i:s', $end_subscription))->greaterThan(Carbon::now())) {
                    update_user_meta(get_current_user_id(), 'subscription_active', $end_subscription);
                }

                WC()->session->set('has_ibo', ($distributor_info['Data']['HasIBOKit'] == 'true') ? 1 : 0);

                $site_url = home_url($wp->request);

                $dt = 'dt=' . urlencode($_GET['dt']);
                $query = str_replace($dt, '', $_SERVER['REQUEST_URI']);
                $current_url = $_SERVER['HTTP_HOST'] . $query;

                if ($distributor_info['Data']['HasIBOKit'] == 'true') {

                    header("Location: " . $site_url . "");

//            if ($distributor_info['IsKitFee'] == $IBO_bought) {
//                add_action('pre_get_posts', 'hide_ibo_function', 999);
//            }
//
//            if ($distributor_info['IsKitFee'] == $IBO_not_bought || $distributor_info['IsKitFee'] == $IBO_pending) {
//                add_action('woocommerce_add_to_cart', 'add_ibo_to_cart', 10, 2);
//                add_filter('woocommerce_cart_item_remove_link', 'customized_cart_item_remove_link', 20, 2);
//            }

                } else {
                    add_action('pre_get_posts', 'hide_ibo_function', 999);
                    header("Location: " . $site_url . "");
                }
            }
        }

    }
});


add_action('wp_logout', 'auto_redirect_after_logout');
function auto_redirect_after_logout()
{
    global $wp;

    if (!empty($_GET['dt'])) {
        $site_url = home_url($wp->request);
        header("Location: " . $site_url . "");
    }
}

/**
 * Add sj scripts to DOM in footer
 * intercom is help chat
 * */
function add_intercom()
{

    if (is_user_logged_in()) {
        /** @var WP_User $user */
        $user = wp_get_current_user();
        ?>
        <script>(function () {

                window.intercomSettings = {
                    app_id: "lt9yzgpm",
                    name: '<?=($user->display_name)?>', // Full name
                    email: '<?=($user->user_email)?>', // Email address
                    created_at: <?=(get_user_meta($user->ID, 'login_time') ?: Carbon::now()->timestamp)?> // Signup date as a Unix timestamp
                };

                var w = window;
                var ic = w.Intercom;
                if (typeof ic === "function") {
                    ic('reattach_activator');
                    ic('update', w.intercomSettings);
                } else {
                    var d = document;
                    var i = function () {
                        i.c(arguments);
                    };
                    i.q = [];
                    i.c = function (args) {
                        i.q.push(args);
                    };
                    w.Intercom = i;
                    var l = function () {
                        var s = d.createElement('script');
                        s.type = 'text/javascript';
                        s.async = true;
                        s.src = 'https://widget.intercom.io/widget/lt9yzgpm';
                        var x = d.getElementsByTagName('script')[0];
                        x.parentNode.insertBefore(s, x);
                    };
                    if (w.attachEvent) {
                        w.attachEvent('onload', l);
                    } else {
                        w.addEventListener('load', l, false);
                    }
                }
            })();</script>
        <?php
    }
}

add_action('wp_footer', 'add_intercom');

/**
 * Hiden ibo functions
 *
 * @param $query
 */
function hide_ibo_function($query)
{
    $overridden_array = $query->get('post__not_in', array());
    $overridden_array[] = 766;
    $query->set('post__not_in', $overridden_array);
}

/**
 * Add product to cart
 * */
function add_product_to_cart()
{
    if (!is_admin()) {
        $product_id = 766;
        $found = false;
        //check if product already in cart
        if (sizeof(WC()->cart->get_cart()) > 0) {
            foreach (WC()->cart->get_cart() as $cart_item_key => $values) {
                $_product = $values['data'];
                if ($_product->get_id() == $product_id)
                    $found = true;
            }
            // if product not found, add it
            if (!$found)
                WC()->cart->add_to_cart($product_id);
        } else {
            // if no products in cart, add it
            WC()->cart->add_to_cart($product_id);
        }
    }
}

//add_action( 'template_redirect', 'add_product_to_cart' );

/**
 * Customized cart item remove link
 *
 * @param $button_link
 * @param $cart_item_key
 * @return string
 */
function customized_cart_item_remove_link($button_link, $cart_item_key)
{

    $cart_item = WC()->cart->get_cart()[$cart_item_key];

    if (WC()->cart->get_cart_contents_count() > 1) {

        $targeted_products_sku = array('IBOENR00');

        if (in_array($cart_item['data']->sku, $targeted_products_sku))
            $button_link = '';

        return $button_link;

    } elseif (WC()->cart->get_cart_contents_count() == 1) {
        echo '<a href="' . wc_get_cart_remove_url($cart_item_key) . '" class="remove" aria-label="' . __('Remove this item', 'masterstudy') . '" data-product_id="' . $cart_item['data']->get_id() . '" data-product_sku="' . $cart_item['data']->sku . '">Remove</a>';
    }
}


function custom_select_options_text()
{
    global $product;
    $product_type = $product->product_type;

    if ('variable' === $product_type) {
        return '';
    }
}


/**
 * landing
 * Add product to cart by landing
 *
 * */
function landing_checkout()
{
    $_quantity = 1;
    $product_id = $_POST['product_id'];
    $variant_id = $_POST['variant_id'];

    /** @var WC_Product $product */
    $product = wc_get_product($product_id);
    /** @var WC_Product $variant */
    $variant = wc_get_product($variant_id);

    if(empty($product) || empty($variant)){
        wp_send_json_error(array('error' => true, 'data' =>  'product empty'));
        exit();
    }

    $exist = false;

    /**
     * if cart already exist product by id 13344
     * */
    if (!empty(WC()->cart)) {
        foreach (WC()->cart->get_cart() as $key => $val) {
            $_product = $val['data'];
            if ($product_id == $_product->id) {
                $exist = true;
                break;
            }
        }
    } else {

        WC()->session = new WC_Session_Handler();
        WC()->session->init();
        WC()->customer = new WC_Customer(get_current_user_id(), true);
        WC()->cart = new WC_Cart();
    }

    $user_id = get_current_user_id() || WC()->session->get('wc_user_id');

    $end_subscription = get_user_meta($user_id, 'subscription_active', true);

    if (!$exist && (empty($end_subscription) || Carbon::createFromFormat('m/d/Y H:i:s', $end_subscription)->lessThan(Carbon::now()))) {

        $_variations = [];

        $children = $product->get_children($args = '', $output = OBJECT);
        foreach ($children as $key => $value) {
            $product_variatons = new WC_Product_Variation($value);
            if ($product_variatons->exists() && $product_variatons->variation_is_visible() && $product_variatons->get_id() == $variant_id) {
                $_variations = $product_variatons->get_variation_attributes();
            }
        }

        if(empty($_variations)){
            wp_send_json_error(array('error' => true, 'data' =>  'Not found variation'));
            exit();
        }

        WC()->cart->add_to_cart($product_id, $_quantity, $variant_id, $_variations);
    }

    wp_send_json_success(array('success' => true, 'url' =>  wc_get_checkout_url()));
    wp_die();
}

add_action('wp_ajax_landingp_product_ajax', 'landing_checkout', 0);
add_action('wp_ajax_nopriv_landingp_product_ajax', 'landing_checkout');



/**
 * Short code for landing page
 * */
add_shortcode('landingproduct', 'landing_product');
function landing_product($atts)
{
    global $post;

    $rg = (object)shortcode_atts([
        'id' => null,
        'variant_id' => null
    ], $atts);

    if (!$post = get_post($rg->id))
        return '';

    /** @var WC_Product $product */
    $product = wc_get_product($rg->id);
    /** @var WC_Product $variant */
    $variant = wc_get_product($rg->variant_id);

    $variants = $product->get_children();

    if (empty($variant) || !in_array($variant->get_id(), $variants)) {
        return 'Invalid variant';
    }

    wp_reset_postdata();

    $url = wp_get_attachment_url(get_post_thumbnail_id($product->get_id()));

    $out = "
        <div class='container text-center landing'>
        <div style='display:none;' class='row load-spin'>
            <div class='login-loading col-md-12 hide'>
                <img style='display:block; margin:0 auto; width:30px; height:auto;' src='/wp-content/themes/masterstudy/images/loading.gif' />
            </div>
        </div>
        <form action='' id='landing' method='post'>
           <input type='hidden' name='product_id' value='{$product->get_id()}'>
           <input type='hidden' name='variant_id' value='{$variant->get_id()}'>
            
            <div class='text-center'>
                <h1>{$product->get_title()}</h1>
            </div>
            <div class='d-flex justify-content-center'>
                <div class='col-12 '>
                    <div class='col-sm-12'>
                        <div class='card mb-3 rounded-0'>
                          <div class='row '>
                            <div class='col-md-7'>
                              <img src='{$url}' class='card-img' >
                            </div>
                            <div class='offset-md-1 col-md-4'>
                                  <div class='card-body'>
                                    <h3 class='card-title'>{$product->get_title()}</h3>
                                    
                                    <p class='card-text'>
                                        {$product->get_short_description()}
                                    </p>
                                    <p class='card-text'>
                                        <small class='text-muted'>
                                            {$product->get_price_html()}
                                        </small>
                                    </p>
                                   
                                  </div>
                                <div class=''>
                                    <button type='submit' class='btn btn-primary btn-lg btn-block button product_type_variable'>Checkout</button>
                                </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
             </div>
             <button type='submit' class='btn btn-primary btn-lg btn-block button product_type_variable mw-100'>Checkout</button>
             <br>
             <br>
        </form>
        </div>
        
        <script >
        (function($) {
            $('.landing').parents('body').find('.header-menu li').first().hide();
            $('body #landing').on('submit', function(event) {
                  event.stopPropagation();
                  event.preventDefault();
                  
                  let _data = $(this).serializeArray();
                 
                  let result = {};
                    for (var i = 0; i < _data.length; i++) {
                      result[_data[i].name] = _data[i].value;
                    }
                  
                  result.action = 'landingp_product_ajax';
                 
                  $.ajax({
                      type: 'POST',
                      url: '" . admin_url('admin-ajax.php') . "',
                      data: result,
                      dataType: 'json',
                      cache: false,
                      beforeSend: function (results) {
                        $('.button').attr('disabled', 'disabled');
                        
                        $('.login-loading').show();
			            
                    },
                      success: function(result){
      
                            if(result.data.url){
                                window.location.href = result.data.url
                            } else {
                                console.log('Error');
                            }
                       },
                      error: function (results) {
                            console.log(results);
                            alert('Error ajax');
                            return false;
                      },
                    complete: function (response) {
                        setTimeout(function(){
                            $('.login-loading').hide();
                            $('.button').removeAttr( 'disabled' );
                            console.log('stop spinner');
                        }, 1000);
                    }
                  }) 
              
           });
        })(jQuery);
        </script>
        
        ";

    return $out;
}


/**
 * Filter menu
 *
 * @param WP_Post $menu_item
 * @return mixed
 */
function filter_function_name($menu_item ){

    if($menu_item->title == "Shop"){
        return [];
    }

    return $menu_item;
}



add_filter( 'woocommerce_product_data_tabs', 'extension_subscriber' );
/**
 * Extension subscription product.
 * Add tab "Grouped product"
 * $tab array
 * */
function extension_subscriber( $tabs) {
    $tabs['grouped_products'] = array(
        'label'	 => __( 'Grouped products', 'gp_product' ),
        'target' => 'grouped_products_options',
        'class'  => ['show_if_subscription'],
    );
    return $tabs;
}

/**
 * Add custom field (select2) for attach products to subscription
 * */
add_action( 'woocommerce_product_data_panels', 'extension_subscriber_tab_content' );
function extension_subscriber_tab_content() {
    global $post;

    ?><div id='grouped_products_options' class='panel woocommerce_options_panel'><?php
    ?><div class='options_group'>
    <p class="form-field">
        <label for="grouped_products_ids"><?php echo __( 'List of products ', 'woocommerce' ); ?></label>
        <select class="wc-product-search" multiple="multiple" style="width: 50%;" id="grouped_products_ids" name="grouped_products_ids[]" data-placeholder="<?php esc_attr_e( 'Search for a product&hellip;', 'woocommerce' ); ?>" data-action="woocommerce_json_search_products_and_variations" data-exclude="<?php echo intval( $post->ID ); ?>">
            <?php
            $product_ids =  get_post_meta( $post->ID, 'grouped_products_ids', true ) ?: [];
            foreach ( $product_ids as $product_id ) {
                $product = wc_get_product( $product_id );
                if ( is_object( $product ) ) {
                    echo '<option value="' . esc_attr( $product_id ) . '"' . selected( true, true, false ) . '>' . wp_kses_post( $product->get_formatted_name() ) . '</option>';
                }
            }
            ?>
        </select> <?php echo wc_help_tip( __( 'Group are grouped products which you include into this subscription.', 'woocommerce' ) ); ?>

    </p>
    </div>
    </div><?php
}

/**
 * Save grouped ids of products
 * */
add_action( 'woocommerce_process_product_meta', 'save_extension_subscriber_products_group' );
function save_extension_subscriber_products_group( $post_id ){
    $demo_product_info = $_POST['grouped_products_ids'];
    if( !empty( $demo_product_info ) && is_array($demo_product_info) ) {
        update_post_meta( $post_id, 'grouped_products_ids', $demo_product_info );
    }
}


