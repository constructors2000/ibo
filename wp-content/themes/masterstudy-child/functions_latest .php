<?php
add_action('wp_enqueue_scripts', 'theme_enqueue_styles');
function theme_enqueue_styles()
{
    // Styles
    wp_enqueue_style('boostrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', NULL, STM_THEME_VERSION, 'all');
    wp_enqueue_style('font-awesome-min', get_template_directory_uri() . '/assets/css/font-awesome.min.css', NULL, STM_THEME_VERSION, 'all');
    wp_enqueue_style('font-icomoon', get_template_directory_uri() . '/assets/css/icomoon.fonts.css', NULL, STM_THEME_VERSION, 'all');
    wp_enqueue_style('fancyboxcss', get_template_directory_uri() . '/assets/css/jquery.fancybox.css', NULL, STM_THEME_VERSION, 'all');
    wp_enqueue_style('select2-min', get_template_directory_uri() . '/assets/css/select2.min.css', NULL, STM_THEME_VERSION, 'all');
    wp_enqueue_style('theme-style-less', get_template_directory_uri() . '/assets/css/styles.css', NULL, STM_THEME_VERSION, 'all');

    // Animations
    if (!wp_is_mobile()) {
        wp_enqueue_style('theme-style-animation', get_template_directory_uri() . '/assets/css/animation.css', NULL, STM_THEME_VERSION, 'all');
    }

    // Theme main stylesheet
    wp_enqueue_style('theme-style', get_stylesheet_uri(), null, STM_THEME_VERSION, 'all');

    // FrontEndCustomizer
    /*wp_enqueue_style( 'skin_red_green', get_template_directory_uri() . '/assets/css/skins/skin_red_green.css', NULL, STM_THEME_VERSION, 'all' );
    wp_enqueue_style( 'skin_blue_green', get_template_directory_uri() . '/assets/css/skins/skin_blue_green.css', NULL, STM_THEME_VERSION, 'all' );
    wp_enqueue_style( 'skin_red_brown', get_template_directory_uri() . '/assets/css/skins/skin_red_brown.css', NULL, STM_THEME_VERSION, 'all' );
    wp_enqueue_style( 'skin_custom_color', get_template_directory_uri() . '/assets/css/skins/skin_custom_color.css', NULL, STM_THEME_VERSION, 'all' );*/
}

function themename_add_to_cart_fragment($fragments)
{
    ob_start();
    $count = WC()->cart->cart_contents_count;
    ?><a class="cart-contents"href="<?php echo esc_url(wc_get_cart_url()); ?>"
         title="<?php _e('View your shopping cart'); ?>"><?php
    if ($count > 0) {
        ?>
        <span class="cart-contents-count"><?php echoesc_html($count); ?></span>
        <?php
    }
    ?></a><?php
    $fragments['a.cart-contents'] = ob_get_clean();

    return $fragments;
}

add_filter('woocommerce_add_to_cart_fragments', 'themename_add_to_cart_fragment');


wp_dequeue_script('testimonials');
wp_enqueue_script('owl.carousel');
wp_enqueue_script('testimonials-style-3', get_stylesheet_directory_uri() . '/assets/js/vc_modules/testimonials/style_3.js', array('owl.carousel'), '1.0.0', true);

wp_enqueue_style('product-style', get_stylesheet_directory_uri() . '/assets/css/products-style.css', false);

/*wp_enqueue_script( 'woo-filter', get_stylesheet_directory_uri() . '/assets/js/woo-filter.js', array('jquery') );

wp_enqueue_script( 'wishlist-ajax', get_stylesheet_directory_uri() . '/assets/js/wishlist-ajax.js', array('jquery') );

wp_enqueue_script( 'go-to-cart', get_stylesheet_directory_uri() . '/assets/js/go-to-cart.js', array('jquery') );

wp_enqueue_script( 'invoice-btn', get_stylesheet_directory_uri() . '/assets/js/invoice-btn.js', array('jquery') );

wp_enqueue_script( 'hover-box', get_stylesheet_directory_uri() . '/assets/js/hover-box.js', array('jquery') );*/

wp_enqueue_script('hover-box', get_stylesheet_directory_uri() . '/assets/js/child.js', array('jquery'));

add_theme_support('woocommerce', array(
    'thumbnail_image_width' => 150
));

/* WooCommerce: The Code Below Removes Checkout Fields */

add_filter('woocommerce_checkout_fields', 'custom_override_checkout_fields');
function custom_override_checkout_fields($fields)
{
    unset($fields['billing']['billing_company']);
    /*unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_state']);*/
    unset($fields['billing']['billing_phone']);
    unset($fields['order']['order_comments']);
    unset($fields['account']['account_username']);
    unset($fields['account']['account_password']);
    unset($fields['account']['account_password-2']);

    $fields['billing']['billing_first_name']['placeholder'] = 'First Name';
    $fields['billing']['billing_first_name']['label'] = '';
    $fields['billing']['billing_last_name']['placeholder'] = 'Last Name';
    $fields['billing']['billing_last_name']['label'] = '';
    $fields['billing']['billing_email']['placeholder'] = 'Email Address';
    $fields['billing']['billing_email']['label'] = '';

    $fields['billing']['billing_country']['label'] = '<span>Billing Payment Details:</span><br/>Country';

    return $fields;
}


/**
 * Outputs a rasio button form field
 */
function woocommerce_form_field_radio($key, $args, $value = '')
{
    global $woocommerce;
    $defaults = array(
        'type' => 'radio',
        'label' => '',
        'placeholder' => '',
        'required' => false,
        'class' => array(),
        'label_class' => array(),
        'return' => false,
        'options' => array()
    );
    $args = wp_parse_args($args, $defaults);
    if ((isset($args['clear']) && $args['clear']))
        $after = '<div class="clear"></div>';
    else
        $after = '';
    $required = ($args['required']) ? ' <abbr class="required" title="' . esc_attr__('required', 'woocommerce') . '">*</abbr>' : '';
    switch ($args['type']) {
        case "select":
            $options = '';
            if (!empty($args['options']))
                foreach ($args['options'] as $option_key => $option_text)
                    $options .= '<input type="radio" name="' . $key . '" id="' . $key . '" value="' . $option_key . '" ' . selected($value, $option_key, false) . 'class="select">' . $option_text . '' . "\r\n";
            $field = '<p class="form-row ' . implode(' ', $args['class']) . '" id="' . $key . '_field">
<label for="' . $key . '" class="' . implode(' ', $args['label_class']) . '">' . $args['label'] . $required . '</label>
' . $options . '
</p>' . $after;
            break;
    } //$args[ 'type' ]
    if ($args['return'])
        return $field;
    else
        echo $field;
}

/**
 * Add the field to the checkout
 **/
add_action('woocommerce_after_checkout_billing_form', 'for_yourself', 10);
function for_yourself($checkout)
{
    echo '<div id="for_yourself"><p>' . __('Are you making the purchased for yourself?') . '</p>';
    woocommerce_form_field_radio('for_yourself', array(
        'type' => 'select',
        'class' => array('for-yourself'),
        'label' => __(''),
        'placeholder' => __(''),
        'required' => false,
        'options' => array(
            'Myself' => 'Myself',
            'Somebody else' => 'Somebody else',
        )
    ), $checkout->get_value('for_yourself'));
    echo '</div>';
}

/**
 * Process the checkout
 **/
add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');
function my_custom_checkout_field_process()
{
    global $woocommerce;
    // Check if set, if its not set add an error.
    if (!$_POST['for_yourself'])
        wc_add_notice(__('Please select for whom are you buying!'), 'error');
}

/**
 * Update the order meta with field value
 **/
add_action('woocommerce_checkout_update_order_meta', 'for_yourself_update_order_meta');
function for_yourself_update_order_meta($order_id)
{
    if ($_POST['for_yourself'])
        update_post_meta($order_id, 'Are you making the purchased for yourself?', esc_attr($_POST['for_yourself']));
}


add_action('woocommerce_after_checkout_form', 'hide_show_user_field', 6);

function hide_show_user_field()
{

    ?>
    <script type="text/javascript">

        jQuery('#user_id').hide();
        jQuery('#user_name').hide();

        jQuery('input[type="text"]#user_id').addClass('popmake-myself');

        jQuery('input#for_yourself').change(function () {

            if (jQuery('input[name=for_yourself]:checked').val() == 'Somebody else') {
                jQuery('#user_id').fadeIn();
                jQuery('#user_name').fadeIn();
                jQuery('#pum-1236').popmake('open');
            }
            else {
                jQuery('#user_id').fadeIn();
                jQuery('#user_name').hide();
                jQuery('#pum-1233').popmake('open');
            }

        });


        jQuery('input.input-radio').change(function () {
            jQuery('document.body').trigger('update_checkout');
        });

    </script>
    <?php

}


/**
 * Add custom field to the checkout page
 */

add_action('woocommerce_after_checkout_billing_form', 'user_id');
function user_id($checkout)
{
    echo '<div id="user_id">';
    woocommerce_form_field('user_id', array(
        'type' => 'text',
        'class' => array(
            'user_id form-row-wide'
        ),
        'label' => __(''),
        'required' => true,
        'placeholder' => __('Enter User ID'),
    ),
        $checkout->get_value('user_id'));
    echo '</div>';
}

/**
 * Checkout Process
 */

add_action('woocommerce_checkout_process', 'user_id_field_process');
function user_id_field_process()
{
    // Show an error message if the field is not set.
    if (!$_POST['user_id']) wc_add_notice(__('Please enter User ID!'), 'error');
    if (!preg_match("/^[0-9]{5}$/", $_POST['user_id'])) wc_add_notice(__('User ID is a 5 digit number!'), 'error');
}

/**
 * Update the value given in custom field
 */

add_action('woocommerce_checkout_update_order_meta', 'user_id_field_update_order_meta');
function user_id_field_update_order_meta($order_id)
{
    if (!empty($_POST['user_id'])) {
        update_post_meta($order_id, 'User ID', sanitize_text_field($_POST['user_id']));
    }
}

/**
 * Add custom field to the checkout page
 */

add_action('woocommerce_after_checkout_billing_form', 'user_name');
function user_name($checkout)
{
    echo '<div id="user_name">';
    woocommerce_form_field('user_name', array(
        'type' => 'text',
        'class' => array(
            'user_name form-row-wide'
        ),
        'label' => __(''),
        'required' => false,
        'placeholder' => __('Recipient Full Name'),
    ),
        $checkout->get_value('user_name'));
    echo '</div>';
}

/**
 * Checkout Process
 */

/*add_action('woocommerce_checkout_process', 'user_name_field_process');
function user_name_field_process() {
	// Show an error message if the field is not set.
	if (!$_POST['user_name']) wc_add_notice(__('Please enter User Name!') , 'error');
}*/

/**
 * Update the value given in custom field
 */

add_action('woocommerce_checkout_update_order_meta', 'user_name_field_update_order_meta');
function user_name_field_update_order_meta($order_id)
{
    if (!empty($_POST['user_name'])) {
        update_post_meta($order_id, 'User Name', sanitize_text_field($_POST['user_name']));
    }
}

/* Checkbox */
add_action('woocommerce_after_checkout_billing_form', 'ibo_checkbox_field');
function ibo_checkbox_field($checkout)
{
    echo '<div class="ibo-box">';
    woocommerce_form_field('ibo_checkbox', array(
        'type' => 'checkbox',
        'label' => __('<p>If you plan to be MELiUS Marketing IBO, you must purchase the IBO Kit.</p>'),
        'required' => false,
        'default' => 1
    ), $checkout->get_value('ibo_checkbox'));
    echo '</div>';
}

/*add_action('woocommerce_checkout_process', 'ibo_process_checkbox');
function ibo_process_checkbox() {
    global $woocommerce;
        if (!$_POST['ibo_checkbox'])
            wc_add_notice( __( 'Notification message.' ), 'error' );
}*/

add_action('woocommerce_checkout_update_order_meta', 'ibo_checkout_order_meta');
function ibo_checkout_order_meta($order_id)
{
    if ($_POST['ibo_checkbox']) update_post_meta($order_id, 'IBO Check', esc_attr($_POST['ibo_checkbox']));
}


add_action('woocommerce_after_checkout_billing_form', 'privacy_checkbox_field');
function privacy_checkbox_field($checkout)
{
    echo '<div class="privacy-box">';
    woocommerce_form_field('privacy_checkbox', array(
        'type' => 'checkbox',
        'label' => __('<p>I have reviewed and agree to the <a href="http://staging-meliuscommerce.kinsta.cloud/wp-content/uploads/2019/02/Terms_of_Use_Melius.pdf" target="_blank">Terms & Conditions</a> and <a href="http://staging-meliuscommerce.kinsta.cloud/wp-content/uploads/2019/02/Policies_and_Procedures_Melius.pdf" target="_blank">Policies & Procedures</a>.</p>'),
        'required' => true,
    ), $checkout->get_value('privacy_checkbox'));
    echo '</div>';
}

add_action('woocommerce_checkout_process', 'privacy_process_checkbox');
function privacy_process_checkbox()
{
    global $woocommerce;
    if (!$_POST['privacy_checkbox'])
        wc_add_notice(__('Please review our Terms & Policies and check if you agree.'), 'error');
}

add_action('woocommerce_checkout_update_order_meta', 'privacy_checkout_order_meta');
function privacy_checkout_order_meta($order_id)
{
    if ($_POST['privacy_checkbox']) update_post_meta($order_id, 'Privacy', esc_attr($_POST['privacy_checkbox']));
}


add_action('woocommerce_after_checkout_billing_form', 'return_checkbox_field');
function return_checkbox_field($checkout)
{
    echo '<div class="return-box">';
    woocommerce_form_field('return_checkbox', array(
        'type' => 'checkbox',
        'label' => __('<p>I have reviewed and agree to the <a href="http://staging-meliuscommerce.kinsta.cloud/wp-content/uploads/2019/02/MELIUS_REFUND_AND_DELIVERY_POLICIES.pdf" target="_blank">Return/Refund Policy</a> and <a href="http://staging-meliuscommerce.kinsta.cloud/wp-content/uploads/2019/02/MELIUS_REFUND_AND_DELIVERY_POLICIES.pdf" target="_blank">Cancelation Policy</a>.</p>'),
        'required' => true,
    ), $checkout->get_value('return_checkbox'));
    echo '</div>';
}

add_action('woocommerce_checkout_process', 'return_process_checkbox');
function return_process_checkbox()
{
    global $woocommerce;
    if (!$_POST['return_checkbox'])
        wc_add_notice(__('Please review our Refund & Cancelation Policies and check if you agree.'), 'error');
}

add_action('woocommerce_checkout_update_order_meta', 'return_checkout_order_meta');
function return_checkout_order_meta($order_id)
{
    if ($_POST['return_checkbox']) update_post_meta($order_id, 'Return Policies', esc_attr($_POST['return_checkbox']));
}

/* End Checkbox*/

remove_action('woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20);
add_action('woocommerce_checkout_after_order_review', 'woocommerce_checkout_payment', 10);


add_filter('woocommerce_email_subject_new_order', 'change_admin_email_subject', 1, 2);
function change_admin_email_subject($subject, $order)
{
    global $woocommerce;

    $subject = sprintf('OSA - (USD) - Product Purchase Alert!! %s', $order->id);

    return $subject;
}


add_filter('wc_stripe_generate_payment_request', function ($post_data, $order) {

    $product_names = '';
    foreach ($order->get_items() as $item) {
        $product_names .= $item['name'];
        $product_names .= ', ';
    }

    $post_data['description'] = sprintf(
        '%1$s – Order %2$s',
        $product_names,
        $order->get_order_number()
    );

    return $post_data;
}, 10, 2);


/**
 * Add Custom Field to Product Variations - WooCommerce
 */

// -----------------------------------------
// 1. Add custom field input @ Product Data > Variations > Single Variation

add_action('woocommerce_variation_options_pricing', 'add_custom_field_to_variations', 10, 3);

function add_custom_field_to_variations($loop, $variation_data, $variation)
{
    ?>
    <div style="clear: both;"><?php
        woocommerce_wp_text_input(array(
                'id' => 'cv_points[' . $loop . ']',
                'class' => 'short wc_input_decimal',
                'label' => __('CV Points', 'woocommerce'),
                'value' => get_post_meta($variation->ID, 'cv_points', true)
            )
        );
        ?></div>
    <div style="clear: both;"><?php
        woocommerce_wp_text_input(array(
                'id' => 'qv_points[' . $loop . ']',
                'class' => 'short wc_input_decimal	',
                'label' => __('QV Points', 'woocommerce'),
                'value' => get_post_meta($variation->ID, 'qv_points', true)
            )
        );
        ?></div>
    <div style="clear: both;"><?php
    woocommerce_wp_text_input(array(
            'id' => 'product_id_xC[' . $loop . ']',
            'class' => 'short wc_input_decimal	',
            'label' => __('Product ID', 'woocommerce'),
            'value' => get_post_meta($variation->ID, 'product_id_xC', true)
        )
    );
    ?></div><?php
}

// -----------------------------------------
// 2. Save custom field on product variation save

add_action('woocommerce_save_product_variation', 'save_custom_field_variations', 10, 2);

function save_custom_field_variations($variation_id, $i)
{
    $cv_points = $_POST['cv_points'][$i];
    if (!empty($cv_points)) {
        update_post_meta($variation_id, 'cv_points', esc_attr($cv_points));
    } else delete_post_meta($variation_id, 'cv_points');

    $qv_points = $_POST['qv_points'][$i];
    if (!empty($qv_points)) {
        update_post_meta($variation_id, 'qv_points', esc_attr($qv_points));
    } else delete_post_meta($variation_id, 'qv_points');

    $product_id_xC = $_POST['product_id_xC'][$i];
    if (!empty($product_id_xC)) {
        update_post_meta($variation_id, 'product_id_xC', esc_attr($product_id_xC));
    } else delete_post_meta($variation_id, 'product_id_xC');
}

// -----------------------------------------
// 3. Store custom field value into variation data

add_filter('woocommerce_available_variation', 'add_custom_field_variation_data');

function add_custom_field_variation_data($variations)
{
    $variations['cv_points'] = '<div class="woocommerce_custom_field">CV Points: <span>' . get_post_meta($variations['variation_id'], 'cv_points', true) . '</span></div>';
    $variations['qv_points'] = '<div class="woocommerce_custom_field">QV Points: <span>' . get_post_meta($variations['variation_id'], 'qv_points', true) . '</span></div>';
    $variations['product_id_xC'] = '<div class="woocommerce_custom_field">Product ID: <span>' . get_post_meta($variations['variation_id'], 'product_id_xC', true) . '</span></div>';
    return $variations;
}

add_filter('woocommerce_show_variation_price', '__return_true');

add_filter('woocommerce_coupon_message', 'filter_woocommerce_coupon_message', 10, 3);
function filter_woocommerce_coupon_message($msg, $msg_code, $instance)
{
    $msg = 'Coupon code applied successfully. CV/QV points shall be adjusted accordance to the price.';
    return $msg;
}

;

add_filter('woocommerce_checkout_get_value', '__return_empty_string', 10);

/**
 * xC Order ID custom field on WC Order post type
 */
add_action('woocommerce_admin_order_data_after_order_details', 'xC_data', 12, 1);
function xC_data($order)
{
    woocommerce_wp_text_input(array(
        'id' => 'xC_Order_Id',
        'label' => __("xC Order Id:", "woocommerce"),
        'value' => get_post_meta($order->get_id(), 'xC_Order_Id', true),
        'wrapper_class' => 'form-field-wide',
        'custom_attributes' => array('readonly' => 'readonly'),
    ));

    woocommerce_wp_text_input(array(
        'id' => 'xC_Order_Legacy_Number',
        'label' => __("xC Order Legacy Number:", "woocommerce"),
        'value' => get_post_meta($order->get_id(), 'xC_Order_Legacy_Number', true),
        'wrapper_class' => 'form-field-wide',
        'custom_attributes' => array('readonly' => 'readonly'),
    ));

    woocommerce_wp_text_input(array(
        'id' => 'xC_Order_Invoice_Number',
        'label' => __("xC Order Invoice Number:", "woocommerce"),
        'value' => get_post_meta($order->get_id(), 'xC_Order_Invoice_Number', true),
        'wrapper_class' => 'form-field-wide',
        'custom_attributes' => array('readonly' => 'readonly'),
    ));
}

/*function add_coupon_cv() {	
	woocommerce_wp_text_input( array(
        'id'            => 'coupon_cv',
        'label'         => __("Coupon Cv Points:", "woocommerce"),
        'value'         => get_post_meta( $order->get_id(), 'coupon_cv', true ),
        'wrapper_class' => 'form-field-wide',
    ));
}
add_action( 'woocommerce_coupon_options', 'add_coupon_cv', 10, 0 );

function save_cv_coupon( $post_id ) {
	$cv_points = $_POST['coupon_cv'];
    if ( ! empty( $cv_points ) ) {
		update_post_meta( $post_id, 'coupon_cv', $coupon_cv );
	}
}
add_action( 'woocommerce_coupon_options_save', 'save_cv_coupon');*/


function admin_init()
{
    add_meta_box('coupon_cv-meta', 'CV Points', 'coupon_cv_points', 'shop_coupon', 'side', 'low');
    add_meta_box('coupon_qv-meta', 'QV Points', 'coupon_qv_points', 'shop_coupon', 'side', 'low');
}

add_action('admin_init', 'admin_init');

function coupon_cv_points()
{
    global $post;
    $custom = get_post_custom($post->ID);
    $coupon_cv = $custom['coupon_cv'][0];
    ?>
    <label>CV Points:</label>
    <input name="coupon_cv" value="<?php echo $coupon_cv; ?>"/>
    <?php
}

function coupon_qv_points()
{
    global $post;
    $custom = get_post_custom($post->ID);
    $coupon_qv = $custom['coupon_qv'][0];
    ?>
    <label>QV Points:</label>
    <input name="coupon_qv" value="<?php echo $coupon_qv; ?>"/>
    <?php
}

function save_coupon_cv()
{
    global $post;

    update_post_meta($post->ID, 'coupon_cv', $_POST['coupon_cv']);
}

add_action('save_post', 'save_coupon_cv');

function save_coupon_qv()
{
    global $post;

    update_post_meta($post->ID, 'coupon_qv', $_POST['coupon_qv']);
}

add_action('save_post', 'save_coupon_qv');

add_filter('woocommerce_gateway_icon', 'custom_payment_gateway_icons', 10, 2);
function custom_payment_gateway_icons($icon, $gateway_id)
{

    foreach (WC()->payment_gateways->get_available_payment_gateways() as $gateway)
        if ($gateway->id == $gateway_id) {
            $title = $gateway->get_title();
            break;
        }

    // The path (subfolder name(s) in the active theme)
    $path = '/wp-content/plugins/woocommerce-gateway-stripe/assets/images/';

    // Setting (or not) a custom icon to the payment IDs
    if ($gateway_id == 'abzer_networkonline')
        $icon = '<img class="stripe-amex-icon stripe-icon" src="' . WC_HTTPS::force_https_url("$path/visa.svg") . '" alt="' . esc_attr($title) . '" width="37px" /><img class="stripe-amex-icon stripe-icon" src="' . WC_HTTPS::force_https_url("$path/amex.svg") . '" alt="' . esc_attr($title) . '" width="37px" /><img class="stripe-amex-icon stripe-icon" src="' . WC_HTTPS::force_https_url("$path/mastercard.svg") . '" alt="' . esc_attr($title) . '" width="37px" />';
    else if ($gateway_id == 'coinpayments')
        $icon = '<img class="stripe-amex-icon stripe-icon" src="' . WC_HTTPS::force_https_url("/wp-content/themes/masterstudy-child/assets/images/bitcoin.svg") . '" alt="' . esc_attr($title) . '" width="37px" />';

    return $icon;
}

global $wp;
$get_api_data = 'http://xwebservicesmelius.xssclients.com/ThirdParty/Services.svc/Distributor_GetInformation';

$ch = curl_init($get_api_data);

$data = "//" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

add_action('init', 'start_session', 1);
function start_session()
{
    if (session_status() !== PHP_SESSION_ACTIVE) {
        session_start();
    }
}

if (strpos($data, '/?dt=') !== false) {
    $distributorId = substr($data, strpos($data, "=") + 1);
    $_SESSION['user_id'] = $distributorId;
}

$jsonData = array(
    'distributorId' => $_SESSION['user_id'],
    'returnType' => '4',
    'partyToken' => '78697265637420746F6B656E',
);
$jsonDataEncoded = json_encode($jsonData);


curl_setopt($ch, CURLOPT_POST, 1);

curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$exec = curl_exec($ch);

curl_close($ch);

$response = json_decode($exec, true);
$result = json_decode($response['Response'], true);

//echo $_SESSION['user_id'];

//AccountType Status
$Distributor = 10;
$Cusomer = 40;

//IBO Status
$IBO_not_bought = 0;
$IBO_bought = 1;
$IBO_pending = 2;

$site_url = home_url($wp->request);

$current_url = "//" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];


if ($result['AccountType'] == $Distributor) {

    if ($current_url == '//' . $_SERVER['HTTP_HOST'] . '/ibo/') {
        header("Location: " . $site_url . "");
    }

    if ($result['IsKitFee'] == $IBO_bought) {
        add_action('pre_get_posts', 'hide_ibo_function', 999);
    }

    if ($result['IsKitFee'] == $IBO_not_bought) {
        add_action('woocommerce_add_to_cart', 'add_ibo_to_cart', 10, 2);
        add_filter('woocommerce_cart_item_remove_link', 'customized_cart_item_remove_link', 20, 2);
    }


} else if ($result['AccountType'] == $Cusomer) {

    add_action('pre_get_posts', 'hide_ibo_function', 999);

} else {
    // Remove links to the product details pages from the product listing page of a WooCommerce store
    add_action('wp_head', 'add_js_functions');
    add_filter('woocommerce_loop_add_to_cart_link', 'custom_select_options_text');

    if ($current_url == '//' . $_SERVER['HTTP_HOST'] . '/ibo/') {
        header("Location: " . $site_url . "");
    }

}


function hide_ibo_function($query)
{
    $overridden_array = $query->get('post__not_in', array());
    $overridden_array[] = 766;
    $query->set('post__not_in', $overridden_array);
}

function custom_select_options_text()
{
    global $product;
    $product_type = $product->product_type;

    if ('variable' === $product_type) {
        return '';
    }
}

function add_ibo_to_cart($item_key, $product_id)
{

    $cats = get_terms(array('taxonomy' => 'product_cat', 'hide_empty' => 1, 'orderby' => 'ASC', 'parent' => 0));
    foreach ($cats as $cat) {
        $term_id = $cat->term_id;
    }
    $product_category_id = $term_id; // cricket bat category id
    $product_cats_ids = wc_get_product_term_ids($product_id, 'product_cat');
    if (!is_admin() && in_array($product_category_id, $product_cats_ids)) {
        $free_product_id = 766;  // Product Id of the free product which will get added to cart
        $found = false;
        //check if product already in cart
        if ($product_id != $free_product_id) {
            if (sizeof(WC()->cart->get_cart()) > 0) {
                foreach (WC()->cart->get_cart() as $cart_item_key => $values) {
                    $_product = $values['data'];
                    if ($_product->get_id() == $free_product_id)
                        $found = true;
                }
                // if product not found, add it
                if (!$found)
                    WC()->cart->add_to_cart($free_product_id);
            } else {
                // if no products in cart, add it
                WC()->cart->add_to_cart($free_product_id);
            }
        }
    }
}

function customized_cart_item_remove_link($button_link, $cart_item_key)
{

    $cart_item = WC()->cart->get_cart()[$cart_item_key];

    if (WC()->cart->get_cart_contents_count() > 1) {

        $targeted_products_sku = array('IBOENR00');

        if (in_array($cart_item['data']->sku, $targeted_products_sku))
            $button_link = '';

        return $button_link;

    } elseif (WC()->cart->get_cart_contents_count() == 1) {
        echo '<a href="' . wc_get_cart_remove_url($cart_item_key) . '" class="remove" aria-label="' . __('Remove this item', 'masterstudy') . '" data-product_id="' . $cart_item['data']->get_id() . '" data-product_sku="' . $cart_item['data']->sku . '">Remove</a>';
    }
}


function add_js_functions()
{
    ?>
    <script>
        jQuery(document).ready(function () {
            jQuery('.stm_archive_product_inner_grid_content ul li.product').each(function () {
                jQuery(this).find('a').removeAttr("href");
            })
        })
    </script>
    <?php
}