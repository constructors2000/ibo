(function ($) {

    $(document).ready(function () {

		$('.stm-lms-wishlist').click(function(e) {
			var addressValue = window.location.href;
			jQuery.ajax({url: addressValue, cache: true, success: function(result){
				$('.wishlist-count').replaceWith($(result).find('.wishlist-count'));
				$('.wishlist-list').replaceWith($(result).find('.wishlist-list'));
			}})
		});
		
		$('.invoice-2').hide();

		$('#view-invoice').click(function(e) {
			$( '.invoice-1' ).hide();
			$( '.invoice-2' ).show();			
		});
		
		$('.yith-wacp-close').click(function(e) {
			$( '.single_add_to_cart_button.added' ).replaceWith( '<a href="/basket" class="button single_add_to_cart_button">Go to cart</a>' );
			var addressValue = $(this).attr('href');
			$.ajax({url: addressValue, success: function(result){
				$('.cart-contents').replaceWith($(result).find('.cart-contents'));
			}}) 
		});
		
		$('.product-category-link').click(function(e) {
			var addressValue = $(this).attr('href');
			$.ajax({url: addressValue, success: function(result){
				$('.stm_archive_product_inner_grid_content').replaceWith($(result).find('.stm_archive_product_inner_grid_content'));
				$('.featured_products_list').replaceWith($(result).find('.stm_archive_product_inner_grid_content'));
			}})
			$( '.product-category-link' ).removeClass( 'active' );
			$(this).addClass( 'active' );
			e.preventDefault(); 
		});

        function myFunction() {
            $('.stm_archive_product_inner_grid_content ul li').each(function () {
                var product_id = $(this).find('.add_to_cart_button').data('product_id');

                if (product_id !== 766) {
                    $('.add_to_cart_button').addClass('disable');
                }
            })
		}



	});

})(jQuery);