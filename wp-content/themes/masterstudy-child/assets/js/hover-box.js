(function ($) {

    $(document).load(function () {

		$('.stm_archive_product_inner_unit').mouseenter(function() {
			$(this).find('.stm_lms_courses__single--info').css({'visibility': 'visible', 'opacity': '1'});
		});
		
		$('.stm_lms_courses__single--info').mouseleave(function() {
			$(this).css({'visibility': 'hidden', 'opacity': '0'});
		});
			
		$('.select2-container').append('.stm_lms_courses__single--info');
	});

})(jQuery);