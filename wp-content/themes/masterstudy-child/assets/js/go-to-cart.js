(function ($) {

    $(document).ready(function () {

		$('.yith-wacp-close').click(function(e) {
			$( '.single_add_to_cart_button.added' ).replaceWith( '<a href="/basket" class="button single_add_to_cart_button">Go to cart</a>' );
			var addressValue = $(this).attr('href');
			$.ajax({url: addressValue, success: function(result){
				$('.cart-contents').replaceWith($(result).find('.cart-contents'));
			}}) 
		});
	
	});

})(jQuery);