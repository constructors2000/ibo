(function ($) {

    $(document).ready(function () {

		$('.stm-lms-wishlist').click(function(e) {
			var addressValue = window.location.href;
			jQuery.ajax({url: addressValue, cache: true, success: function(result){
				$('.wishlist-count').replaceWith($(result).find('.wishlist-count'));
				$('.wishlist-list').replaceWith($(result).find('.wishlist-list'));
			}})
		});
		
	});

})(jQuery);