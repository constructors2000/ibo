(function ($) {

    $(document).ready(function () {

		$('.product-category-link').click(function(e) {
			var addressValue = $(this).attr('href');
			$.ajax({url: addressValue, success: function(result){
				$('.stm_archive_product_inner_grid_content').replaceWith($(result).find('.stm_archive_product_inner_grid_content'));
				$('.featured_products_list').replaceWith($(result).find('.stm_archive_product_inner_grid_content'));
			}})
			$( '.product-category-link' ).removeClass( 'active' );
			$(this).addClass( 'active' );
			e.preventDefault(); 
		});
	
	});

})(jQuery);