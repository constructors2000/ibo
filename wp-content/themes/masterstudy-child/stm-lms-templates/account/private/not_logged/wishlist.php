<h2><?php esc_html_e('My Wishlist', 'masterstudy-lms-learning-management-system'); ?></h2>

<?php if(!empty($_COOKIE['stm_lms_wishlist'])):
	$wishlist = sanitize_text_field($_COOKIE['stm_lms_wishlist']);
	$wishlist = ltrim($wishlist, ',');
	$wishlist = explode(',', $wishlist);
				$args = array(
					'post_type' => 'product',
					'posts_per_page' => 12,
					'post__in' => $wishlist
				);
				?>
				<div class="stm_archive_product_inner_grid_content">
				<ul class="stm-courses row list-unstyled">
				<?php
				$loop = new WP_Query( $args );
				if ( $loop->have_posts() ) {
					while ( $loop->have_posts() ) : $loop->the_post();

						
						global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

// Extra post classes
$classes = array();


$shop_sidebar_position = stm_option( 'shop_sidebar_position', 'none' );
if(isset($_GET['sidebar_position']) and $_GET['sidebar_position'] == 'none') {
	$shop_sidebar_position = 'none';
}

if($shop_sidebar_position == 'none') {
	$classes[] = 'col-md-3 col-sm-4 col-xs-6 course-col';
} else {
	$classes[] = 'col-md-3 col-sm-4 col-xs-6 course-col';
}
?>
<!-- Custom Meta -->
<?php
$experts = get_post_meta(get_the_id(), 'course_expert', true);
$stock = get_post_meta(get_the_id(), '_stock', true );
$regular_price = get_post_meta(get_the_id(), '_regular_price', true );
$sale_price = get_post_meta(get_the_id(), '_sale_price', true );
?>
						<li <?php post_class( $classes ); ?>>
	<?php
	/**
	 * woocommerce_before_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );
	?>

	<div class="stm_archive_product_inner_unit heading_font">
		<div class="stm_archive_product_inner_unit_centered">

			<div class="stm_featured_product_image">
				<?php if($product->is_type( 'simple' )) {  ?>
					<?php $status = get_post_meta($product->get_id(), 'course_status', false); ?>
					<?php if(!empty($status)): ?>
						<div class="stm_lms_post_status heading_font <?php echo $status; ?>">
							<?php echo $status[0]; ?>
						</div>
					<?php endif; ?>
						
				<?php }	elseif( $product->is_type( 'variable' ) ){ ?>
					<?php $available_variations = $product->get_available_variations(); ?>
					<?php if(!empty($available_variations[0]['display_regular_price'])): ?>

						<div class="stm_featured_product_price">
							<div class="price">
								<?php if(!empty($available_variations[0]['display_price'])): ?>
									<?php echo(wc_price( $available_variations[0]['display_price']) ); ?>
								<?php else: ?>
									<?php echo(wc_price( $available_variations[0]['display_regular_price']) ); ?>
								<?php endif; ?>
							</div>
						</div>
					<?php endif; ?>
				<?php } ?>

				<?php if(has_post_thumbnail()): ?>
					<a href="<?php the_permalink() ?>" title="<?php esc_attr_e('View course', 'masterstudy') ?> - <?php the_title(); ?>">
						<?php the_post_thumbnail('img-270-283', array('class'=>'img-responsive')); ?>
					</a>
				<?php else: ?>
					<div class="no_image_holder"></div>
				<?php endif; ?>

			</div>



			<div class="stm_featured_product_body">
				<?php $product_cats = get_the_terms($product->ID, 'product_cat');
				if (!empty($product_cats)):?>
                    <div class="xs-product-cats">
                        <div class="meta-unit categories clearfix">
                            <div class="meta_values">
                                <div class="value h6">
									<?php foreach ($product_cats as $product_cat): ?>
                                        <!--<a href="<?php echo get_term_link($product_cat); ?>">-->
                                            <?php echo sanitize_text_field($product_cat->name); ?>
                                        <!--</a>-->
									<?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
				<?php endif; ?>
				<a href="<?php the_permalink() ?>"  title="<?php esc_attr_e('View course', 'masterstudy') ?> - <?php the_title(); ?>">
					<div class="title"><?php the_title(); ?></div>
				</a>
			</div>

			<div class="stm_featured_product_footer">
				<div class="clearfix">
					<div class="pull-left">
						<?php woocommerce_template_loop_rating(); ?>
					</div>
					<div class="pull-right">
						<?php woocommerce_template_loop_price();?>
					</div>
				</div>
			</div>

		</div> <!-- stm_archive_product_inner_unit_centered -->
		<?php STM_LMS_Templates::show_lms_template('courses/parts/course_info',
			array_merge(array(
				'post_id' => $post_id,
			), compact('post_status', 'sale_price', 'price', 'author_id', 'id'))
		); ?>
	</div> <!-- stm_archive_product_inner_unit -->
</li>

						<?php
					endwhile;
				} else {
					echo __( 'No products found' );
				}
				wp_reset_postdata();
			?>
				</ul>
				</div>

<?php else: ?>
    <h4><?php printf(__('Wishlist is empty. <a href="%s">View courses</a>', 'masterstudy-lms-learning-management-system'), STM_LMS_Course::courses_page_url()); ?></h4>
<?php endif; ?>