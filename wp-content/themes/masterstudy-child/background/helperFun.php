<?php
require_once(__DIR__ . '/BackOfficeApi.php');
require_once(__DIR__ . '/helperFun.php');
require_once(__DIR__ . './../extension/carbon/src/Carbon/Carbon.php');


use Carbon\Carbon;

function guidv4($data = null)
{
    $data = random_bytes(16);
    assert(strlen($data) == 16);

    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}


function consecutiveAlphabeticAndNumeric($str, $maxAlpha = 4, $maxNum = 3)
{

    preg_match_all('!\d+!', $str, $matches);
    $words = preg_replace('/[0-9]+/', ' ', $str);
    $words = explode(' ', $words);

    $words = array_merge($matches[0], $words);

    foreach ($words as $word) {

        if (is_numeric($word) && !empty($maxNum)) {
            //echo "numeric {$word}\n";
            if (hasOrderedCharacters($word, $maxNum)) {
                return true;
            }
        } elseif (!empty($maxAlpha)) {
            // echo "alphabet {$word}\n";
            if (hasOrderedCharacters($word, $maxAlpha)) {
                return true;
            }
        }
    }

    return false;
}

function hasOrderedCharacters($string, $num = 4)
{
    $i = 0;
    $j = strlen($string);
    $str = implode('', array_map(function ($m) use (&$i, &$j) {
        return chr((ord($m[0]) + $j--) % 256) . chr((ord($m[0]) + $i++) % 256);
    }, str_split($string, 1)));
    return preg_match('#(.)(.\1){' . ($num - 1) . '}#', $str);
}

function repeatedLetters($password, $permit = 2)
{
    if (preg_match('/(\w)\1{' . $permit . ',}/', $password)) {
        return true;
    }
    return false;
}

function qwertyKeyboardConsecutive($password, $permit = 4)
{
    $qwerty = 'qwertyuiopasdfghjklzxcvbnm';

    for ($start = 0; $start < strlen($qwerty); $start++) {
        $sub_string = substr($qwerty, $start, $permit);
        if(strlen($sub_string) < 4)
            return false;
        if (stripos($password, $sub_string) !== false) {
            return true;
        }
    }

    return false;
}



class Cryptor
{

    protected $method = 'aes-128-ctr'; // default cipher method if none supplied
    private $key;

    protected function iv_bytes()
    {
        return openssl_cipher_iv_length($this->method);
    }

    public  static function GenerateKey($token = null){
        $key = guidv4();
        file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'private.key' , $key);

        if(!empty($token)){
            $token_crypt = self::fastEncrypt($token);
            echo "partyToken: ".$token_crypt."\n";
            file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'token.key' , $token_crypt);
        }
    }

    /**
     * Init method ssl
     *
     * @param string|bool $key
     * @param string|bool $method
     */
    public function __construct($key = FALSE, $method = FALSE)
    {
        if(!$key) {
            $_key = file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'private.key');
            if(empty($_key)) {
                $key = php_uname(); // default encryption key if none supplied
            } else {
                $key = $_key;
            }
        }
        if(ctype_print($key)) {
            // convert ASCII keys to binary format
            $this->key = openssl_digest($key, 'SHA256', TRUE);
        } else {
            $this->key = $key;
        }
        if($method) {
            if(in_array(strtolower($method), openssl_get_cipher_methods())) {
                $this->method = $method;
            } else {
                die(__METHOD__ . ": unrecognised cipher method: {$method}");
            }
        }
    }

    /**
     * Encrypt text
     *
     * @param string $data
     * @return string
     */
    public function encrypt($data)
    {
        $iv = openssl_random_pseudo_bytes($this->iv_bytes());
        return bin2hex($iv) . openssl_encrypt($data, $this->method, $this->key, 0, $iv);
    }

    /**
     * Decrypt text
     *
     * @param string $data
     * @return string
     */
    public function decrypt($data)
    {
        $iv_strlen = 2  * $this->iv_bytes();
        if(preg_match("/^(.{" . $iv_strlen . "})(.+)$/", $data, $regs)) {
            list(, $iv, $crypted_string) = $regs;
            if(ctype_xdigit($iv) && strlen($iv) % 2 == 0) {
                return openssl_decrypt($crypted_string, $this->method, $this->key, 0, hex2bin($iv));
            }
        }
        return FALSE; // failed to decrypt
    }

    public static function fastEncrypt($data){
        $self = new self();
       return $self->encrypt($data);
    }

    public static function fastDecrypt($data){
        $self = new self();
       return $self->decrypt($data);
    }

}

if(!empty($argv[1])){
    Cryptor::GenerateKey($argv[1]);
}