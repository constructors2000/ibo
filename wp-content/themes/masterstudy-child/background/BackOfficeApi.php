<?php
/**
 * Created by PhpStorm.
 * User: andrewsachno
 * Date: 2019-07-09
 * Time: 07:50
 */

/**
 * Custom office API
 *
 * */
class BackOfficeApi
{

    public static function GET_API_TOKEN()
    {
        return file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'token.key') ?: '';
    }

    public static $langs = ['en' => 11, 'es' => 2121];
    public static $accounts = ['ibo' => 10, 'customer' => 40];

    public static $paymant_methods = [
        'bitpay_checkout_gateway' => '31',
        'maxicashgateway' => '22',
        'coinpayments' => '26',
        'stripe' => '23',
        'paypal' => '24',
        'tap' => '25',
        'abzer_networkonline' => '27',
    ];

    public static $link_test = "https://xwebservicesmelius.xssclients.com/ThirdParty/Services.svc/";
    public static $link_live = "http://xwebservices.thinkmelius.com/Thirdparty/Services.svc/";
    public $link = '';

    const TEST = false;

    public function __construct()
    {
        $this->link = $this->getLink();
    }

    /**
     * This prog-custom trigger for enable or disable tester service.
     * Change this status only manual (developer).
     *
     * @return string
     */
    public function getLink()
    {
        return self::TEST ? self::$link_test : self::$link_live;
    }

    /**
     * Fast create user on BackOffice
     *
     * @param array $data
     * @return array|mixed|object
     */
    public static function fastCreateUser(array $data)
    {
        $api = new self();
        return $api->createUser($data);

    }

    /**
     * Fast create order on BackOffice
     *
     * @param integer|WC_Order $order
     * @return array
     * @throws Exception
     */
    public static function fastCreateOrder($order)
    {
        $api = new self();
        return $api->createOrder($order);
    }

    /**
     * Get Distributor info by distributorId.
     * Return array include status of result [Result] and [Data=>Products] for show
     *
     * @param $distributorId
     * @return array|mixed|object
     * @throws Exception
     */
    public static function getDistributorInfo($distributorId)
    {
        $api = new self();
        return $api->getDistributor($distributorId);
    }

    /**
     * Auth on xirect
     *
     * @param $username
     * @param $password
     * @return array|mixed|object
     */
    public static function fastAuth($username, $password)
    {
        $api = new self();
        return $api->auth($username, $password);
    }

    /**
     * Create user on BackOffice. Most data for user in $data param
     *
     * @param $data
     * @return array|mixed|object
     */
    public function createUser($data)
    {
        $link = "{$this->link}Distributor_Insert";

        $data = [
            "distributor" => array_merge($data, [
                "Id" => "0",
                "UserStatus" => "2"
            ]),
            "returnType" => "4",
            "partyToken" => Cryptor::fastDecrypt(BackOfficeApi::GET_API_TOKEN()),
        ];
        return self::curl_response($link, $data);
    }

    /**
     * Get Distributor info by distributorId.
     * Return array include status of result [Result] and [Data=>Products] for show
     *
     * @param $distributorId
     * @return array|mixed|object
     * @throws Exception
     */
    public function getDistributor($distributorId)
    {
        $link = "{$this->link}Distributor_GetInformation";

        $data = array(
            'distributorId' => $distributorId,
            'returnType' => '4',
            'partyToken' => Cryptor::fastDecrypt(BackOfficeApi::GET_API_TOKEN()),
        );

        $result = self::curl_post($link, $data);

        if (empty($result['Result']) || (int)$result['Result'] > 220 || (int)$result['Result'] < 200) {
            throw new Exception($result['Message'] ?: "Unknown error");
        }

        return $result;
    }

    /**
     * Insert Order on backOffice
     *
     * @param integer|WC_Order $order_id
     * @return mixed
     * @throws Exception
     */
    public function createOrder($order_id)
    {
        if (is_integer($order_id)) {
            $order = wc_get_order($order_id);
        } elseif ($order_id instanceof WC_Order) {
            $order = wc_get_order($order_id); } else {
            throw new Exception('Unknown order (create order API)');
        }

        $added = get_post_meta($order->get_id(), 'xC_Order_Invoice_Number', true);
        if (empty($added)) {
            $url = $this->getLink() . 'Order_Insert';

            /**
             * Get payment Type Id
             * */
            $PaymentMethod = $order->get_payment_method();
            $PaymentType = BackOfficeApi::$paymant_methods[$PaymentMethod] ?: 23;

            /**
             * If user wnat paid e-money
             * */
            if ($PaymentMethod != 'bacs') {

                /* Array of Items */
                $ItemsArray = array();
                $QvArray = array();
                $OrderTotal = 0;
                $AccountChange = 'false';
                $OrderType = '108';
                $IsFreeEnrollment = 'true';

                /** @var WC_Order_Item_Product  $item */
                foreach ($order->get_items() as $item) {
                    var_dump($item);
                    exit();
                    $product = wc_get_product($item->get_product_id());
                    $product_variation = wc_get_product($item->get_variation_id());
                    $skuId = $product_variation->get_sku();

                    /**
                     * Get Terms (taxonomy) of product
                     * */
                    $product_cats = get_the_terms($item->get_product_id(), 'product_cat');

                    $categories = [];
                    foreach ($product_cats as $category) $categories[] = $category->slug;

                    /**
                     * IF term melius-other not find in terms of products.
                     * "melius-other" it`s another category for product such as Event Ticket, Merchandise ...
                     * */
                    if (in_array('melius-other', $categories) == false) {
                        var_dump($item);
                        exit();
                        $_days = trim(str_replace(' days', '', strtolower($item['subscription'])));
                        switch ($_days) {
                            case '30':
                                if (strpos($product->get_name(), 'Gemini')) {
                                    $SubscriptionArray = array(
                                        'Id' => '25',
                                        'LenghtDays' => '30',
                                        'Name' => 'Traders Gemini - 30',
                                        'Fee' => 'true'
                                    );
                                } else {
                                    $SubscriptionArray = array(
                                        'Id' => '16',
                                        'LenghtDays' => '37',
                                        'Name' => 'Subscription 1 months - 30 Days',
                                        'Fee' => 'false'
                                    );
                                }
                                break;
                            case '90':
                                if (strcmp($product_variation->get_sku(), 'TGEMINI2020') == 0) {
                                    $SubscriptionArray = array(
                                        'Id' => '26',
                                        'LenghtDays' => '90',
                                        'Name' => 'Traders Gemini - 90',
                                        'Fee' => 'true'
                                    );
                                } else {
                                    $SubscriptionArray = array(
                                        'Id' => '15',
                                        'LenghtDays' => '97',
                                        'Name' => 'Subscription 3 months - 90 Days',
                                        'Fee' => 'false'
                                    );
                                }
                                break;
                            case '180':
                                $SubscriptionArray = array(
                                    'Id' => '18',
                                    'LenghtDays' => '187',
                                    'Name' => 'Subscription 6 months - 180 Days',
                                    'Fee' => 'false'
                                );
                                break;
                            case '365':
                                if (strpos($product->get_name(), 'IBO')) {
                                    $SubscriptionArray = array(
                                        'Id' => '17',
                                        'LenghtDays' => '360',
                                        'Name' => 'IBO KIT',
                                        'Fee' => 'true'
                                    );
                                    $AccountChange = 'true';
                                    $OrderType = '114';
                                    $IsFreeEnrollment = 'false';
                                    break;
                                } else {
                                    $SubscriptionArray = array(
                                        'Id' => '19',
                                        'LenghtDays' => '367',
                                        'Name' => 'Subscription 12 months - 360 Days',
                                        'Fee' => 'false'
                                    );
                                    break;
                                }
                            case '545':
                                $SubscriptionArray = array(
                                    'Id' => '24',
                                    'LenghtDays' => '547',
                                    'Name' => 'Subscription 18 months - 545 Days',
                                    'Fee' => 'false'
                                );
                                break;
                            default:
                                $SubscriptionArray = array(
                                    'Id' => '',
                                    'LenghtDays' => '',
                                    'Name' => '',
                                    'Fee' => ''
                                );
                        }

                        /**
                         * SET Cv and Qv Points
                         * */
                        $CvPoints = '0.00';
                        $QvPoints = '0.00';
                        if (strpos($product->get_name(), 'IBO') == false || strcmp($product_variation->get_sku(), 'TGEMINI2020') == 0) {

                            $CvPoints = get_post_meta($item->get_variation_id(), 'cv_points', true);
                            $QvPoints = get_post_meta($item->get_variation_id(), 'qv_points', true);

                            /**
                             * If order has coupons used only first
                             * */
                            foreach ($order->get_used_coupons() as $coupon_name) {
                                $coupon_post_obj = get_page_by_title($coupon_name, OBJECT, 'shop_coupon');
                                $coupon_id = $coupon_post_obj->ID;

                                $coupon_cv = get_post_meta($coupon_id, 'coupon_cv', true);
                                $coupon_qv = get_post_meta($coupon_id, 'coupon_qv', true);

                                $coupons_obj = new WC_Coupon($coupon_id);

                                /** CV */
                                if (empty($coupon_cv)) {
                                    if ($coupons_obj->get_discount_type() == 'percent') {
                                        $coupons_amount1 = $coupons_obj->get_amount();
                                        $CvPoints = number_format($CvPoints, 2) * (100 - $coupons_amount1) / 100;
                                    }
                                    if ($coupons_obj->is_type('fixed_cart')) {
                                        $coupons_amount2 = $coupons_obj->get_amount();
                                    }
                                } else {
                                    $CvPoints = $coupon_cv;
                                }

                                /** QV */
                                if (empty($coupon_qv)) {
                                    if ($coupons_obj->get_discount_type() == 'percent') {
                                        $coupons_amount1 = $coupons_obj->get_amount();
                                        $QvPoints = number_format($QvPoints, 2) * (100 - $coupons_amount1) / 100;
                                    }
                                    if ($coupons_obj->is_type('fixed_cart')) {
                                        $coupons_amount2 = $coupons_obj->get_amount();
                                    }
                                } else {
                                    $QvPoints = $coupon_qv;
                                }

                                /*$coupons_obj = new WC_Coupon($coupon_id);
                                if ( $coupons_obj->get_discount_type() == 'percent' ){
                                    $coupons_amount1 = $coupons_obj->get_amount();
                                    $CvPoints = number_format( $CvPoints, 2 ) * (100 - $coupons_amount1) / 100;
                                    $QvPoints = number_format( $QvPoints, 2 ) * (100 - $coupons_amount1) / 100;
                                }

                                if( $coupons_obj->is_type( 'fixed_cart' ) ){
                                    $coupons_amount2 = $coupons_obj->get_amount();
                                }*/
                            }
                        }

                        $order_item = array(
                            'Id' => $item->get_product_id(),
                            'SkuId' => $skuId,
                            'Quantity' => $item->get_quantity(),
                            'UnitPrice' => number_format($item->get_total(), 2),
                            'TotalPrice' => number_format($item->get_total(), 2),
                            'Cv' => number_format($CvPoints, 2),
                            'Qv' => number_format($QvPoints, 2),
                            'Name' => $product->get_name() . ' - ' . $item['subscription'],
                            'ProductId' => get_post_meta($item->get_variation_id(), 'product_id_xC', true),
                            'IsFreeEnrollment' => $IsFreeEnrollment,
                            'Subscription' => $SubscriptionArray,
                            'AdditionalField' => array(
                                'Escape' => 'false',
                                'Adventure' => 'false',
                                'Experience' => 'false',
                                'Elite' => 'false'
                            ),
                            'ListCourses' => array(
                                array(
                                    'CourseId' => ''
                                )
                            ),
                        );

                        $OrderTotal += (float)$item->get_total();

                        array_push($QvArray, $QvPoints);
                        array_push($ItemsArray, $order_item);

                        $IsFreeEnrollment = 'true';
                    }
                }

                /**
                 * If user buy product by xirect
                 * */
                if (!empty($ItemsArray)) {
                    $TotalVolumen = array_sum($QvArray);

                    $args = array(
                        'orderHeader' => array(
                            'Id' => '0',
                            'MarketId' => '118',
                            'MarketName' => 'United Kingdom',
                            'PaymentType' => $PaymentType,
                            'OrderDate' => date('m/d/Y H:i:s', strtotime($order->order_date)),
                            'OrderType' => $OrderType,
                            'TotalTax' => number_format($order->get_total_tax(), 2),
                            'TotalShipping' => '0.00',
                            'SubTotal' => number_format($OrderTotal, 2),
                            'TotalDiscounts' => number_format($order->get_discount_total(), 2), //Not required
                            'OrderTotal' => number_format($OrderTotal, 2),
                            'TotalVolumen' => number_format($TotalVolumen, 2),
                            'TotalQualifyVolumen' => number_format($TotalVolumen, 2),
                            'DigitalSignature' => '', //Not required
                            'Status' => '3',
                            'Notes' => '',
                            'Comments' => '',
                            'OrderSource' => '12',
                            'Items' => $ItemsArray,
                            'ShipToName' => $order->get_formatted_billing_full_name(),
                            'PromoCodeId' => '0',
                            'TaxationProviderValidateAddress' => '0',
                            'TaxationProviderCalculateTax' => '0',
                            'MerchantFeeTotal' => 0.00000000,
                            'AccountChange' => $AccountChange,
                            'Distributor' => array(
                                'Id' => get_post_meta($order->get_id(), 'User ID', true),
                                'FirstName' => $order->get_billing_first_name(),
                                'LastName' => $order->get_billing_last_name(),
                                'UserName' => $order->get_billing_email(),
                                'Email' => $order->get_billing_email(),
                                'LegacyNumber' => get_post_meta($order->get_id(), 'User ID', true)
                            ),
                            'BAddress' => array(
                                'Id' => '3',
                                'Address' => $order->get_billing_address_1(),
                                'Address1' => $order->get_billing_address_2(),
                                'City' => $order->get_billing_city(),
                                'State' => $order->get_billing_state(),
                                'County' => '',
                                'Zip' => $order->get_billing_postcode(),
                                'Country' => $order->get_billing_country(),
                                'IsSameShippingAddress' => 'true'
                            ),
                            'SAddress' => array(
                                'Id' => '3',
                                'Address' => $order->get_shipping_address_1(),
                                'Address1' => $order->get_shipping_address_2(),
                                'City' => $order->get_shipping_city(),
                                'State' => $order->get_shipping_state(),
                                'County' => '',
                                'Zip' => $order->get_shipping_postcode(),
                                'Country' => $order->get_shipping_country()
                            ),
                            'DetailtPayment' => array(
                                array(
                                    'Amount' => $order->get_subtotal(),
                                    'TransactionId' => $order->get_transaction_id(),
                                ),
                            ),
                        ),
                        'returnType' => '4',
                        'partyToken' => Cryptor::fastDecrypt(BackOfficeApi::GET_API_TOKEN()),
                        'cultureName' => 'en-GB'
                    );

                    $args = json_encode($args);

                    var_dump($args);

                    exit();

                    $url = curl_init($url);
                    curl_setopt($url, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($url, CURLOPT_POSTFIELDS, $args);
                    curl_setopt($url, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($url, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Content-Length: ' . strlen($args))
                    );

                    $result = curl_exec($url);

                    //var_dump($result);

                    $result = json_decode($result);
                    $response = explode(',', $result->Response);
                    $xCorderId = explode(':', $response[0]);
                    $xCorderLegacyNumber = explode(':', $response[1]);
                    $xCorderInvoiceNumber = explode(':', $response[2]);
                    $xCorderInvoiceNumber = explode('"', $xCorderInvoiceNumber[1]);

                    if (!empty($xCorderLegacyNumber[1])) {
                        $log = date("F j, Y, g:i a") . ' - WooCommerce Order #' . $order->get_id() . ' - API Result: Success - Order Legacy Number: ' . $xCorderLegacyNumber[1] . '.' . PHP_EOL;
                    } else {
                        $to = 'helpdesk@thinkmelius.com';
                        $subject = 'WooCommerce order #' . $order->get_id() . ' was not sent to Xirect!';
                        $body = 'The API call for WooCommerce order #' . $order->get_id() . ' did not go through to Xirect.';
                        $headers = array('Content-Type: text/html; charset=UTF-8');

                        wp_mail($to, $subject, $body, $headers);
                        $log = date("F j, Y, g:i a") . ' - WooCommerce Order #' . $order->get_id() . ' - API Result: Failed.' . PHP_EOL;
                    }
                    //Save string to log, use FILE_APPEND to append.
                    file_put_contents('xirect.log', $log, FILE_APPEND);

                    update_post_meta($order->get_id(), 'xC_Order_Id', $xCorderId[1]);
                    update_post_meta($order->get_id(), 'xC_Order_Legacy_Number', $xCorderLegacyNumber[1]);
                    update_post_meta($order->get_id(), 'xC_Order_Invoice_Number', $xCorderInvoiceNumber[1]);
                }
                /**
                 * If user buy product not by xirect (Event Ticket, Merchandise ...)
                 * */
                else {
                    update_post_meta($order->get_id(), 'xC_Order_Legacy_Number', $order->get_id());
                }
            }

            /**
             * If user want paid cash
             * */
            else {
                do_action('woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id());
                update_post_meta($order->get_id(), 'xC_Order_Legacy_Number', $order->get_id());
            }
        }
        return $added;
    }

    /**
     * @param WC_Order $order
     * @param WC_Product $product
     * @param WC_Product $product_variation
     */
    protected function prepareProduct($order, $product, $product_variation){

            $skuId = $product_variation->get_sku();

            /**
             * Get Terms (taxonomy) of product
             * */
            $product_cats = get_the_terms($product->get_id(), 'product_cat');

            $categories = [];
            foreach ($product_cats as $category) $categories[] = $category->slug;

            /**
             * IF term melius-other not find in terms of products.
             * "melius-other" it`s another category for product such as Event Ticket, Merchandise ...
             * */
            if (in_array('melius-other', $categories) == false) {
                $_days = trim(str_replace(' days', '', strtolower($item['subscription'])));
                switch ($_days) {
                    case '30':
                        if (strpos($product->get_name(), 'Gemini')) {
                            $SubscriptionArray = array(
                                'Id' => '25',
                                'LenghtDays' => '30',
                                'Name' => 'Traders Gemini - 30',
                                'Fee' => 'true'
                            );
                        } else {
                            $SubscriptionArray = array(
                                'Id' => '16',
                                'LenghtDays' => '37',
                                'Name' => 'Subscription 1 months - 30 Days',
                                'Fee' => 'false'
                            );
                        }
                        break;
                    case '90':
                        if (strcmp($product_variation->get_sku(), 'TGEMINI2020') == 0) {
                            $SubscriptionArray = array(
                                'Id' => '26',
                                'LenghtDays' => '90',
                                'Name' => 'Traders Gemini - 90',
                                'Fee' => 'true'
                            );
                        } else {
                            $SubscriptionArray = array(
                                'Id' => '15',
                                'LenghtDays' => '97',
                                'Name' => 'Subscription 3 months - 90 Days',
                                'Fee' => 'false'
                            );
                        }
                        break;
                    case '180':
                        $SubscriptionArray = array(
                            'Id' => '18',
                            'LenghtDays' => '187',
                            'Name' => 'Subscription 6 months - 180 Days',
                            'Fee' => 'false'
                        );
                        break;
                    case '365':
                        if (strpos($product->get_name(), 'IBO')) {
                            $SubscriptionArray = array(
                                'Id' => '17',
                                'LenghtDays' => '360',
                                'Name' => 'IBO KIT',
                                'Fee' => 'true'
                            );
                            $AccountChange = 'true';
                            $OrderType = '114';
                            $IsFreeEnrollment = 'false';
                            break;
                        } else {
                            $SubscriptionArray = array(
                                'Id' => '19',
                                'LenghtDays' => '367',
                                'Name' => 'Subscription 12 months - 360 Days',
                                'Fee' => 'false'
                            );
                            break;
                        }
                    case '545':
                        $SubscriptionArray = array(
                            'Id' => '24',
                            'LenghtDays' => '547',
                            'Name' => 'Subscription 18 months - 545 Days',
                            'Fee' => 'false'
                        );
                        break;
                    default:
                        $SubscriptionArray = array(
                            'Id' => '',
                            'LenghtDays' => '',
                            'Name' => '',
                            'Fee' => ''
                        );
                }

                /**
                 * SET Cv and Qv Points
                 * */
                $CvPoints = '0.00';
                $QvPoints = '0.00';
                if (strpos($product->get_name(), 'IBO') == false || strcmp($product_variation->get_sku(), 'TGEMINI2020') == 0) {

                    $CvPoints = get_post_meta($item->get_variation_id(), 'cv_points', true);
                    $QvPoints = get_post_meta($item->get_variation_id(), 'qv_points', true);

                    /**
                     * If order has coupons used only first
                     * */
                    foreach ($order->get_used_coupons() as $coupon_name) {
                        $coupon_post_obj = get_page_by_title($coupon_name, OBJECT, 'shop_coupon');
                        $coupon_id = $coupon_post_obj->ID;

                        $coupon_cv = get_post_meta($coupon_id, 'coupon_cv', true);
                        $coupon_qv = get_post_meta($coupon_id, 'coupon_qv', true);

                        $coupons_obj = new WC_Coupon($coupon_id);

                        /** CV */
                        if (empty($coupon_cv)) {
                            if ($coupons_obj->get_discount_type() == 'percent') {
                                $coupons_amount1 = $coupons_obj->get_amount();
                                $CvPoints = number_format($CvPoints, 2) * (100 - $coupons_amount1) / 100;
                            }
                            if ($coupons_obj->is_type('fixed_cart')) {
                                $coupons_amount2 = $coupons_obj->get_amount();
                            }
                        } else {
                            $CvPoints = $coupon_cv;
                        }

                        /** QV */
                        if (empty($coupon_qv)) {
                            if ($coupons_obj->get_discount_type() == 'percent') {
                                $coupons_amount1 = $coupons_obj->get_amount();
                                $QvPoints = number_format($QvPoints, 2) * (100 - $coupons_amount1) / 100;
                            }
                            if ($coupons_obj->is_type('fixed_cart')) {
                                $coupons_amount2 = $coupons_obj->get_amount();
                            }
                        } else {
                            $QvPoints = $coupon_qv;
                        }

                        /*$coupons_obj = new WC_Coupon($coupon_id);
                        if ( $coupons_obj->get_discount_type() == 'percent' ){
                            $coupons_amount1 = $coupons_obj->get_amount();
                            $CvPoints = number_format( $CvPoints, 2 ) * (100 - $coupons_amount1) / 100;
                            $QvPoints = number_format( $QvPoints, 2 ) * (100 - $coupons_amount1) / 100;
                        }

                        if( $coupons_obj->is_type( 'fixed_cart' ) ){
                            $coupons_amount2 = $coupons_obj->get_amount();
                        }*/
                    }
                }

                $order_item = array(
                    'Id' => $item->get_product_id(),
                    'SkuId' => $skuId,
                    'Quantity' => $item->get_quantity(),
                    'UnitPrice' => number_format($item->get_total(), 2),
                    'TotalPrice' => number_format($item->get_total(), 2),
                    'Cv' => number_format($CvPoints, 2),
                    'Qv' => number_format($QvPoints, 2),
                    'Name' => $product->get_name() . ' - ' . $item['subscription'],
                    'ProductId' => get_post_meta($item->get_variation_id(), 'product_id_xC', true),
                    'IsFreeEnrollment' => $IsFreeEnrollment,
                    'Subscription' => $SubscriptionArray,
                    'AdditionalField' => array(
                        'Escape' => 'false',
                        'Adventure' => 'false',
                        'Experience' => 'false',
                        'Elite' => 'false'
                    ),
                    'ListCourses' => array(
                        array(
                            'CourseId' => ''
                        )
                    ),
                );

                $OrderTotal += (float)$item->get_total();

                array_push($QvArray, $QvPoints);
                array_push($ItemsArray, $order_item);

                $IsFreeEnrollment = 'true';
            }

    }


    /**
     * @param $username
     * @param $password
     * @return array|mixed|object
     */
    public function auth($username, $password)
    {
        $link = "{$this->link}_3PartyAuthGet";

        return $this->curl_get($link, [
            'username' => $username,
            'password' => md5($password),
            "returnType" => "2",
            "partyToken" => Cryptor::fastDecrypt(BackOfficeApi::GET_API_TOKEN()),
        ]);
    }

    /**
     * helper method
     *
     * @param string $link
     * @param array $data
     * @return array|mixed|object
     */
    public static function curl_response($link, $data)
    {
        $ch = curl_init($link);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $exec = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($exec, true);

        //['Response']
        return json_decode($response['Response'], true);
    }

    /**
     * helper method
     *
     * @param string $link
     * @param array $data
     * @return array|mixed|object
     */
    public static function curl_post($link, $data)
    {
        $ch = curl_init($link);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $exec = curl_exec($ch);

        curl_close($ch);

        $response = json_decode($exec, true);

        return $response;
    }

    /**
     * @param $link
     * @param $data
     * @return array|mixed|object
     */
    public static function curl_get($link, $data)
    {

        $query = http_build_query($data);

        $ch = curl_init($link . (!empty($query) ? "?" : "") . $query);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $exec = curl_exec($ch);

        curl_close($ch);

        $response = json_decode($exec, true);

        return $response;
    }
}