<?php
/**
 * Created by PhpStorm.
 * User: AndrewSachno
 * Date: 2019-07-04
 * Time: 11:46
 */

require_once(__DIR__ . '/BackOfficeApi.php');
require_once(__DIR__ . '/helperFun.php');
require_once(__DIR__ . './../extension/carbon/src/Carbon/Carbon.php');

use Carbon\Carbon;

/**
 * Emulator Register new user by AJAX
 *
 * This method also validate fields.
 * If fields not valid return error json object include array of fields and messages
 *
 * This methods create user without role. And not login.
 * Create array for register on backOffice and save to user meta "back_office_data"
 */
function register_user_ajax()
{
    /**
     * First check the nonce, if it fails the function will break
     * */
    //check_ajax_referer('ajax-login-nonce', 'security');

    $user_enroller_Id = stripcslashes($_POST['user_enroller_Id']);
    $user_login = stripcslashes($_POST['user_login']);
    $user_first_name = ucfirst(strtolower(stripcslashes($_POST['user_first_name'])));
    $user_last_name = ucfirst(strtolower(stripcslashes($_POST['user_last_name'])));
    $user_email = stripcslashes($_POST['user_email']);
    $user_password = $_POST['user_pass'];
    $user_confirm_password = $_POST['user_confirm_pass'];
    $nice_name = "{$user_first_name} {$user_last_name}";

    $user_dob = $_POST['user_dob'];
    $user_country = $_POST['user_country'];
    $user_government_ID = $_POST['user_government_id'];
    $user_type = $_POST['user_type'];
    $user_lang = $_POST['user_lang'];
    $user_phone = $_POST['user_phone'];

    /*
     * Validate of fields
     * */
    $reg_errors = new WP_Error;

    if (!validate_username($user_enroller_Id))
        $reg_errors->add('user_enroller_Id', 'Enroller_Id is invalidate');

    if (!validate_username($user_login))
        $reg_errors->add('user_login', 'User login is invalidate');

    if (username_exists($user_login))
        $reg_errors->add('user_login', 'User name already exists');

    if (!validate_username($user_first_name))
        $reg_errors->add('user_first_name', 'User first name is invalidate');

    if (!validate_username($user_last_name))
        $reg_errors->add('user_last_name', 'User last name is invalidate');

    if (!is_email($user_email))
        $reg_errors->add('user_email', 'Email is invalidate');

    if (email_exists($user_email))
        $reg_errors->add('user_email', 'User email already exists');

    if (strlen($user_password) < 5)
        $reg_errors->add('user_pass', 'Password length must be greater than 5');

    if (is_numeric($user_password) or ctype_alpha($user_password))
        $reg_errors->add('user_pass', 'Password cannot contain letters or numbers only');


    if (consecutiveAlphabeticAndNumeric($user_password))
        $reg_errors->add('user_pass', 'Password cannot contain (4 or more alphabetically/3 or more numbers) consecutive characters');

    if (repeatedLetters($user_password))
        $reg_errors->add('user_pass', 'Password cannot contain 3 or more repeated characters');

    if (qwertyKeyboardConsecutive($user_password))
        $reg_errors->add('user_pass', 'Password cannot contain 4 or more QWERTY-keyboard-consecutive characters');

    if ($user_password == $user_confirm_password)
        $reg_errors->add('user_pass', 'Password is not confirmed');

    if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $user_dob))
        $reg_errors->add('user_dob', 'Date of birthday is invalidate');

    if (!is_numeric($user_country) && !((int)$user_country > 0))
        $reg_errors->add('user_country', "Country is invalidate");

    if (strlen($user_government_ID) > 14 && !preg_match("/^[1-9][0-9]*$/", $user_government_ID))
        $reg_errors->add('user_government_ID', 'Government ID length must be 14');

    if (!in_array($user_type, ['customer', 'ibo']))
        $reg_errors->add('user_type', 'User type is invalidate');

    if (!in_array($user_lang, ['en', 'es']))
        $reg_errors->add('user_lang', 'User lang is invalidate');

    if (!preg_match("'^(([\+]([\d]{2,}))([0-9\.\-\/\s]{5,})|([0-9\.\-\/\s]{5,}))*$'", $user_phone))
        $reg_errors->add('user_phone', 'Phone is invalidate');

    if (is_user_logged_in()) {
        $reg_errors->add('api', 'You are already registered');
    }

    if (count($reg_errors->get_error_messages())) {
        wp_send_json_error($reg_errors);
        die();
    }

    $user_data = array(
        'user_login' => sanitize_user($user_login),
        'user_email' => sanitize_email($user_email),
        'user_pass' => esc_attr($user_password),
        'user_nicename' => $nice_name,
        'display_name' => $nice_name,
        'first_name' => $user_first_name,
        'last_name' => $user_last_name
        //'role' => 'subscriber'
    );

    $back_ofice_data = array(
        'FirstName' => $user_first_name,
        'LastName' => $user_last_name,
        'Email' => $user_email,
        'UserName' => $user_login,
        'Password' => $user_password,
        'EnrollerId' => $user_enroller_Id,
        'PreferedLanguage' => BackOfficeApi::$langs[$user_lang],
        'AccountType' => BackOfficeApi::$accounts[$user_type],
        'Ssn' => $user_government_ID ? $user_government_ID : '',
        'Birthday' => str_replace('-', '/', $user_dob),
        'PhoneNumber' => $user_phone ? $user_phone : '',
        'MarketId' => $user_country,
        'Addresses' => [

        ]
    );

    $api_result = BackOfficeApi::fastCreateUser($back_ofice_data);

    if (!empty($api_result['ResultMessage'])) {
        $reg_errors->add('api', $api_result['ResultMessage']);
    } elseif (empty($api_result['DistributorLegacyNumber'])) {
        $reg_errors->add('api', "API not work");
    }


    if (count($reg_errors->get_error_messages())) {
        wp_send_json_error($reg_errors);
        die();
    }


    /**
     * Try create user. If successes return user_id else return WP_Error
     * */
    $user_id = wp_insert_user($user_data);
    if (is_wp_error($user_id)) {

        if (isset($user_id->errors['empty_user_login']))
            $reg_errors->add('empty_user_login', __('User Name and Email are mandatory'));

        elseif (isset($user_id->errors['existing_user_login']))
            $reg_errors->add('user_login', __('User name already exists'));

        elseif (isset($user_id->errors['existing_user_email']))
            $reg_errors->add('user_email', __('User email already exists'));

        else
            $reg_errors->add('empty_fill', __('Error Occurred please fill up the sign up form carefully'));
    }

    if (count($reg_errors->get_error_messages())) {
        wp_send_json_error($reg_errors);
        die();
    }

    /**
     * Save shop user_id for attach user to order
     * */
    WC()->session->set('wc_user_id', $user_id);

    $user = get_user_by('id', $user_id);
    $user->remove_role('subscriber');

    /**
     * Update LegacyNumber ('User ID')
     * */
    update_user_meta($user_id, 'User ID', $api_result['DistributorLegacyNumber']);
    update_user_meta($user_id, 'back_office_insert', date('Y-m-d H:i'));

    wp_send_json_success(array('success' => true, 'message' => __('We have Created an account for you')));
    die();
}

add_action('wp_ajax_register_user_ajax', 'register_user_ajax', 0);
add_action('wp_ajax_nopriv_register_user_ajax', 'register_user_ajax');


/**
 * Emulator Login by ajax
 *
 * This method also validate fields.
 * If fields not valid return error json object include array of fields and messages
 * If user not has "LegacyNumber" return error.
 *
 */
function ajax_login()
{
    /*
     * Validate of fields
     * */
    $reg_errors = new WP_Error;

    // Nonce is checked, get the POST data and sign user on

    $user_login = $_POST['user_login'];
    $user_password = $_POST['user_pass'];

    $user_by_log = null;
    $result = null;

    if (!validate_username($user_login))
        $reg_errors->add('user_login', 'Enroller_Id is invalidate');

    if (!validate_username($user_password))
        $reg_errors->add('user_pass', 'Password is not correct');


    if (count($reg_errors->get_error_messages())) {
        wp_send_json_error($reg_errors);
        die();
    }

    $api_result = BackOfficeApi::fastAuth($user_login, $user_password);


    if (!empty($api_result['Data']) && is_array($api_result['Data'])) {

        $data = $api_result['Data'];

        $user_by_log = get_user_by('login', sanitize_user($user_login));
        $user_by_email = get_user_by('email', sanitize_user($data['email']));

        if (empty($user_by_log) && !empty($user_by_email))
            $reg_errors->add('user_login', 'User has problems sync email and login');

        if (count($reg_errors->get_error_messages())) {
            wp_send_json_error($reg_errors);
            die();
        }

        if (!$user_by_log) {
            $user_data = array(
                'user_login' => sanitize_user($user_login),
                'user_email' => sanitize_email($data['email']),
                'user_pass' => esc_attr($user_password),
                'user_nicename' => $data['displayname'],
                'display_name' => $data['displayname'],
            );

            $user_id = wp_insert_user($user_data);

            if (is_wp_error($user_id)) {

                if (isset($user_id->errors['empty_user_login']))
                    $reg_errors->add('empty_user_login', __('User Name and Email are mandatory'));

                elseif (isset($user_id->errors['existing_user_login']))
                    $reg_errors->add('user_login', __('User name already exists'));

                elseif (isset($user_id->errors['existing_user_email']))
                    $reg_errors->add('user_email', __('User email already exists'));

                else
                    $reg_errors->add('empty_fill', __('Error Occurred please fill up the sign up form carefully'));
            }

            if (count($reg_errors->get_error_messages())) {
                wp_send_json_error($reg_errors);
                die();
            }

            /**
             * Update LegacyNumber
             * */
            update_user_meta($user_id, 'User ID', $data['legacynumber']);
            update_user_meta($user_id, 'back_office_insert', date('Y-m-d H:i'));

            /**
             * Save shop user_id for attach user to order
             * */
            WC()->session->set('wc_user_id', $user_id);
            //wp_set_auth_cookie($user_id);
            //wp_set_current_user($user_by_log->ID, $user_data["display_name"]);

            wp_send_json_success(array('status' => true, 'message' => __('Login successful'), 'user_display_name' => $user_data["display_name"]));

        } else {
            update_user_meta($user_by_log->ID, 'User ID', $data['legacynumber']);
            $id = $data['legacynumber'];

            if (empty($id)) {
                wp_send_json_error(array('status' => false, 'message' => __('Error user not has LegacyNumber.')));

            } else {
                /*
                 * Save shop user_id for attach user to order
                 * */
                WC()->session->set('wc_user_id', $user_by_log->ID);
                //wp_set_auth_cookie($user_by_log->ID);
                //wp_set_current_user($user_by_log->ID, $user_by_log->display_name);

                wp_send_json_success(array('status' => true, 'message' => __('Login successful'), 'user_display_name' => $user_by_log->display_name));
            }
        }
    } else {
        wp_send_json_error(array('status' => false, 'message' => __('Wrong username or password (MELIUS).')));
    }
    wp_die();
}

add_action('wp_ajax_login_user_ajax', 'ajax_login', 0);
add_action('wp_ajax_nopriv_login_user_ajax', 'ajax_login');


/**
 * Attach user to order before create order.
 * Because user not login
 *
 * @param $order_id
 */
function attach_user($order_id)
{
    $user_id = get_current_user_id() ?: WC()->session->get('wc_user_id');
    if ($user_id) {
        update_post_meta($order_id, '_customer_user', $user_id);
        $legacynumber = get_user_meta($user_id, 'User ID', true);
        if(!get_post_meta($order_id, 'User ID', true)) {
            update_post_meta($order_id, 'User ID', $legacynumber);
        }
        WC()->session->set('wc_user_id', false);
    }
}

add_action('woocommerce_checkout_update_order_meta', 'attach_user', 10, 1);

/**
 * Hoock pay status completed (success)
 *
 * @param $order_id
 * @return array
 * @throws Exception
 */
function pay_completed($order_id)
{

    $order = wc_get_order($order_id);
    /** @var WP_User $user */
    $user = $order->get_user();

    // Get all the user roles as an array.
    $user_roles = $user->roles;

    // Check if the role you're interested in, is present in the array.
    if ((empty($user_roles) || !in_array('subscriber', $user_roles, true))
        && !in_array('administrator', $user_roles, true)) {
        // Do something.
        $user->add_role('subscriber');
    }
    if ($user) {
        /**
         * Check if user has LegacyNumber
         * */
        $DistributorLegacyNumber = get_user_meta((int)$user->ID, 'User ID', true);

        if (empty($DistributorLegacyNumber)) {
            update_user_meta($user->ID, 'back_office_error', 'User not has legacynumber');
            $log = date("F j, Y, g:i a") . ' - WooCommerce Order #' . $order_id . '[pay_completed] - LegacyNumber not found.' . PHP_EOL;
            file_put_contents('xirect.log', $log, FILE_APPEND);
            throw new Exception('LegacyNumber not found');
        }

        update_user_meta((int)$user->ID, 'pay_completed', Carbon::now()->timestamp);

        /**
         * Try create order on backOffice
         * */
//        return BackOfficeApi::fastCreateOrder($order_id);
    } else {
        $log = date("F j, Y, g:i a") . ' - WooCommerce Order #' . $order_id . '[pay_completed] - User not found.' . PHP_EOL;
        file_put_contents('xirect.log', $log, FILE_APPEND);
        throw new Exception('User not found');
    }
}

add_action('woocommerce_order_status_completed', 'pay_completed', 10, 1);

/**
 * Disable user if order status refund or canceled.
 * But if user has another success order skip disable.
 *
 * @param $order_id
 */
function try_disable_user($order_id)
{
    $order = wc_get_order($order_id);
    /** @var WP_User $user */
    $user = $order->get_user();

    if ($user) {
        ## ==> Define HERE the statuses of that orders
        $order_statuses = array('wc-on-hold', 'wc-processing', 'wc-completed');

        // Getting current customer orders
        $customer_orders = wc_get_orders(array(
            'meta_key' => '_customer_user',
            'meta_value' => $user->ID,
            'post_status' => $order_statuses,
            'numberposts' => -1
        ));

        if (!count($customer_orders)) {
            $user->remove_role('subscriber');
        }
    }
}

add_action('woocommerce_order_status_refunded', 'try_disable_user', 10, 1);
add_action('woocommerce_order_status_cancelled', 'try_disable_user', 10, 1);


/**
 * Redirect customer to home page if customer try open wp-admin
 * */
function my_account_permalink($permalink)
{
    return home_url();
}

add_filter('woocommerce_get_myaccount_page_permalink', 'my_account_permalink', 1);


/**
 * Update cart and product information if user before registration selected IOB status of account
 *
 * @throws Exception
 */
function woocommerce_ajax_add_to_cart()
{
    $sku_ibo = "IBOENR00";

    $product_id = wc_get_product_id_by_sku($sku_ibo);
    $quantity = 1;
    $variation_id = 0;
    $product_status = get_post_status($product_id);

    $exist = false;

    /*
     * if cart already has this product by id 766
     * */
    foreach (WC()->cart->get_cart() as $key => $val) {
        $_product = $val['data'];
        if ($product_id == $_product->id) {
            $exist = true;
            break;
        }
    }

    if (!WC()->session->get('has_ibo')) {
        if (!$exist && !empty($_POST['type']) && stripos(stripcslashes($_POST['type']), 'ibo') !== false) {
            $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);

            /** @var WC_Product $product */
            $product = wc_get_product($product_id);
            $variations = [];

            $children = $product->get_children($args = '', $output = OBJECT);
            foreach ($children as $key => $value) {
                $product_variatons = new WC_Product_Variation($value);
                if ($product_variatons->exists() && $product_variatons->variation_is_visible()) {
                    $variation_id = $product_variatons->get_id();
                    $variations = $product_variatons->get_variation_attributes();
                }
            }

            if(!$variation_id){
                throw new Exception('Variant not found');
            }

            if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id, $variations) && 'publish' === $product_status) {

                do_action('woocommerce_ajax_added_to_cart', $product_id);

                if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
                    wc_add_to_cart_message(array($product_id => $quantity), true);
                }
            } else {
                $data = array(
                    'error' => true,
                    'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id));
                wp_send_json($data);
            }
        }
    }

    WC_AJAX:: get_refreshed_fragments();

    wp_die();
}

add_action('wp_ajax_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
add_action('wp_ajax_nopriv_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');


/**
 * Add product(Experience) to car  If user want add gemini to cart and he das not has eny product
 *
 * */
function so_validate_add_cart_gemini($passed, $product_id)
{
    $sku_gemini = "TGEMINI2020";
    $sku_experience = "ME12LVX3";
    $sku_experience_variant_90 = "MEX3M06";

    $_product_id = wc_get_product_id_by_sku($sku_experience);
    $_variation_id =  wc_get_product_id_by_sku($sku_experience_variant_90);;
    $_quantity = 1;
    $exist = false;

    if ($product_id == wc_get_product_id_by_sku($sku_gemini)) {

        /**
         * if cart not empty
         * */
        if (count(WC()->cart->get_cart()) > 0) {
            $exist = true;
        }

        /**
         * if cart already exist product by id 13344
         * */
        foreach (WC()->cart->get_cart() as $key => $val) {
            $_product = $val['data'];
            if ($_product_id == $_product->id) {
                $exist = true;
                break;
            }
        }

        $user_id = get_current_user_id() || WC()->session->get('wc_user_id');

        $end_subscription = get_user_meta($user_id, 'subscription_active', true);

        if (!$exist && (empty($end_subscription) || Carbon::createFromFormat('m/d/Y H:i:s', $end_subscription)->lessThan(Carbon::now()))) {

            $product = wc_get_product($_product_id);
            $_variations = [];

            $children = $product->get_children($args = '', $output = OBJECT);
            foreach ($children as $key => $value) {
                $product_variatons = new WC_Product_Variation($value);
                if ($product_variatons->exists() && $product_variatons->variation_is_visible() && $product_variatons->get_id() == $_variation_id) {
                    $_variations = $product_variatons->get_variation_attributes();
                }
            }

            WC()->cart->add_to_cart($_product_id, $_quantity, $_variation_id, $_variations);
        }
    }

    return $product_id;
}
add_filter('woocommerce_add_to_cart_validation', 'so_validate_add_cart_gemini', 10, 2);


/**
 * Delete gemini from cart if cart has 2 product and user want delete another product
 * @param $cart_item_key
 * @param $cart
 */
function so_validate_remove_cart_gemini($cart_item_key, $cart)
{
    /**
     * Sku of Gemini
     * */
    $sku = "TGEMINI2020";

    $product_id = wc_get_product_id_by_sku($sku);

    $good_user = false;

    if (is_user_logged_in()) {
        $end_subscription = get_user_meta(get_current_user_id(), 'subscription_active', true);

        if (!empty($end_subscription) && Carbon::createFromFormat('m/d/Y H:i:s', $end_subscription)->greaterThan(Carbon::now())) {
            $good_user = true;
        }
    }

    if (!$good_user) {
        if ((count($cart->cart_contents) == 2) && $cart->cart_contents[$cart_item_key]['product_id'] != $product_id) {
            foreach ($cart->cart_contents as $key => $val) {
                if ($key != $cart_item_key && $val['product_id'] == $product_id) {
                    //var_dump($key, $val['product_id']);
                    if (!($cart->remove_cart_item($key))) {
                        exit();
                    }
                }
            }
        }
    }
}
add_action('woocommerce_remove_cart_item', 'so_validate_remove_cart_gemini', 10, 2);

/**
 * Define the woocommerce_before_checkout_process callback
 * */
function action_woocommerce_before_checkout_process($array)
{
    if (!is_user_logged_in()) {
        $user_id = WC()->session->get('wc_user_id');
        $user = get_user_by('id', $user_id);

        //login
        set_current_user($user_id, $user->display_name);
        wp_set_auth_cookie($user_id);
        //do_action('wp_login', $user->display_name, $user);
    }
    return $array;
}
add_action('woocommerce_before_checkout_process', 'action_woocommerce_before_checkout_process', 10, 1);

/**
 *
 * */
function wl8OrderPlacedTriggerSomething($order_id)
{
    //do something...
}
add_action('woocommerce_thankyou', 'wl8OrderPlacedTriggerSomething', 10, 1);

/**
 * Check_user_logged_in
 * */
function ajax_check_user_logged_in()
{
    echo is_user_logged_in() ? 'yes' : 'no';
    die();
}
add_action('wp_ajax_is_user_logged_in', 'ajax_check_user_logged_in');
add_action('wp_ajax_nopriv_is_user_logged_in', 'ajax_check_user_logged_in');

/**
 * Create order
 * */
add_action('woocommerce_checkout_create_order', function ($order, $data) {
    /** @var WC_Order $order */


    /** @var WP_User $user */
    $user = wp_get_current_user();

    $address = array();

    array_filter($data, function ($key) use (&$address, $data) {
        $value = ($data[$key]);

        if (stripos($key, '_is')) {
            $key = str_replace('_is', '', $key);
            $address[$key] = $value;
        }
    }, ARRAY_FILTER_USE_KEY
    );

    $address = array_merge($address, array(
        'first_name' => $user->first_name,
        'last_name' => $user->last_name,
        'email' => $user->user_email,
    ));

    $order->save();

    $order->set_billing_email($user->user_email);
    $order->set_billing_first_name($user->first_name);
    $order->set_billing_last_name($user->last_name);
    $order->set_billing_address_1($address['address_1']);
    $order->set_billing_address_2($address['address_2'] ?: '');
    $order->set_billing_city($address['city']);
    $order->set_billing_country($address['country']);
    $order->set_billing_state($address['state'] ?: '');
    $order->set_billing_postcode($address['postcode'] ?: '');

    if(!empty($address['address_3'])){
        update_post_meta($order->get_id(), '_billing_address_3', $data['address_3']);
    }

    update_post_meta($order->get_id(), 'someone_else', $data['someone_else']);

    if (stripos($data['someone_else'], 'Yes') !== false) {
        update_post_meta($order->get_id(), 'User ID', $data['legacynumber']);
        update_post_meta($order->get_id(), 'User Name', $data['user_name']);
    }

//    $order->set_address($address);

}, 10, 2);


add_filter('woocommerce_checkout_fields', 'mod_checkout_field');
function mod_checkout_field($fields){
    $fields['billing']['legacynumber']['class'] [] = 'd-none friend';
    $fields['billing']['user_name']['class'] [] = 'd-none friend';
    return $fields;
}
