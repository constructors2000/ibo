<?php

/**
 * @var $view_type
 * @var $per_page
 * @var $expert_id
 * @var $meta_key
 * @var $per_row
 * @var $hide_price
 * @var $hide_comments
 * @var $hide_rating
 */

extract(shortcode_atts(array(
	'meta_key'       => 'all',
	'expert_id'      => 'no_expert',
	'category_id'    => 'no_category',
	'product_tag_id' => 'no_tag',
	'view_type'      => 'featured_products_carousel',
	'auto'           => '0',
	'per_page'       => '-1',
	'per_row'        => 4,
	'order'          => 'DESC',
	'orderby'        => 'date',
	'hide_price'     => false,
	'hide_rating'    => false,
	'hide_comments'  => false,
	'price_bg'       => '#48a7d4',
	'free_price_bg'  => '#eab830',
	'css'            => ''
), $atts));

$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class($css, ' '));

if ($view_type == 'featured_products_carousel') {
	wp_enqueue_script('owl.carousel');
	wp_enqueue_style('owl.carousel');
	stm_module_scripts('featured_products');
}

stm_module_styles('featured_products');

stm_module_styles('courses_grid', 'style_1');
stm_module_scripts('courses_grid');

// All args for extract all products
$args = array(
	'post_type'      => 'product',
	'post_status'    => 'publish',
	'order'          => $order,
	'orderby'        => $orderby,
	'posts_per_page' => $per_page,
);

if (!empty($category_id) and $category_id != 'no_category') {
	$args['product_cat'] = $category_id;
}

if (!empty($product_tag_id) and $product_tag_id != 'no_tag') { // ALP for filtering  STM products by tags
	$args['product_tag'] = $product_tag_id;
}

$args['meta_query'][] = array(
	'key'   => '_stock_status',
	'value' => 'instock',
);

// Get featured products
if ($meta_key == '_featured') {
	$args['meta_query'][] = array(
		'key'   => '_featured',
		'value' => 'yes',
	);
} elseif ($meta_key == 'expert' and $meta_key != 'no_expert') {
	$args['meta_query'][] = array(
		'key'     => 'course_expert',
		'value'   => $expert_id,
		'compare' => 'LIKE'
	);
}

$featured_query = new WP_Query($args);

$featured_product_num = stm_create_unique_id($atts);

$cols_per_row = 12 / $per_row;

wp_enqueue_style( 'yith-wacp-frontend', $stylesheet );
wp_enqueue_script( 'yith-wacp-frontend-script' );
?>


<?php if ($featured_query->have_posts()): ?>

	<?php //wc_get_template_part('global/helpbar'); ?>

    <div class="stm_featured_products_unit <?php echo esc_attr($view_type); ?>">

		<?php if ($view_type == 'featured_products_carousel'): ?>
        <div class="simple_carousel_with_bullets">
            <div class="simple_carousel_bullets_init_<?php echo esc_attr($featured_product_num); ?> clearfix simple_carousel_init"
                 data-items="<?php echo esc_attr($per_row); ?>">
				<?php else: ?>
                <div class="row">
					<?php endif; ?>

					<?php while ($featured_query->have_posts()): $featured_query->the_post(); ?>
						<?php
						global $product;
						$experts = get_post_meta(get_the_id(), 'course_expert', true);
						$stock = get_post_meta(get_the_id(), '_stock', true);
						$regular_price = get_post_meta(get_the_id(), '_regular_price', true);
						$sale_price = get_post_meta(get_the_id(), '_sale_price', true);
						?>
                        <div class="col-md-<?php echo esc_attr($cols_per_row); ?> col-sm-4 col-xs-12">
                            <div class="stm_archive_product_inner_unit <?php echo esc_attr($css_class); ?> heading_font">
                                <div class="stm_archive_product_inner_unit_centered">

								<div class="stm_featured_product_image">
									<?php if($product->is_type( 'simple' ) || $product->is_type( 'variable' )) {  ?>
										<?php $status = get_post_meta($product->get_id(), 'course_status', false); ?>
										<?php if(!empty($status)): ?>
											<div class="stm_lms_post_status heading_font <?php echo $status; ?>">
												<?php echo $status[0]; ?>
											</div>
										<?php endif; ?>
											
									<?php } ?>

									<?php if(has_post_thumbnail()): ?>
										<a href="<?php the_permalink() ?>" title="<?php esc_attr_e('View course', 'masterstudy') ?> - <?php the_title(); ?>">
											<?php the_post_thumbnail('img-270-283', array('class'=>'img-responsive')); ?>
										</a>
									<?php else: ?>
										<div class="no_image_holder"></div>
									<?php endif; ?>

								</div>

			<div class="stm_featured_product_body">
				<?php $product_cats = get_the_terms($product->ID, 'product_cat');
				if (!empty($product_cats)):?>
                    <div class="xs-product-cats">
                        <div class="meta-unit categories clearfix">
                            <div class="meta_values">
                                <div class="value h6">
									<?php foreach ($product_cats as $product_cat): ?>
                                        <!--<a href="<?php echo get_term_link($product_cat); ?>">-->
                                            <?php echo sanitize_text_field($product_cat->name); ?>
                                        <!--</a>-->
									<?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
				<?php endif; ?>
				<a href="<?php the_permalink() ?>"  title="<?php esc_attr_e('View course', 'masterstudy') ?> - <?php the_title(); ?>">
					<div class="title"><?php the_title(); ?></div>
				</a>
			</div>

                                    			<div class="stm_featured_product_footer">
				<div class="clearfix">
					<div class="pull-left">
						<?php woocommerce_template_loop_rating(); ?>
					</div>
					<div class="pull-right">
						<?php woocommerce_template_loop_price();?>
					</div>
				</div>
			</div>

                                </div>
								
											<?php STM_LMS_Templates::show_lms_template('courses/parts/course_info',
			array_merge(array(
				'post_id' => $id,
			), compact('post_status', 'sale_price', 'price', 'author_id', 'id'))
		); ?>
								
                            </div>
                        </div>
					<?php endwhile; ?>

					<?php if ($view_type == 'featured_products_carousel'): ?>
                </div> <!-- simple_carousel_with_bullets_init -->
            </div> <!-- simple_carousel_with_bullets -->
			<?php else: ?>
        </div> <!-- row -->
	<?php endif; ?>

    </div> <!-- stm_featured_products_unit -->

<?php endif; ?>

<?php wp_reset_postdata(); ?>