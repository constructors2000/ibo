<?php // Shop before sidebar template ?>

<?php
$post_id = get_the_ID();
?>

    <div class="stm-lms-course__sidebar">

        <div class="stm_lms_course__image hidden-on-phone">
            <?php if(function_exists('stm_get_VC_attachment_img_safe')) {
                echo stm_get_VC_attachment_img_safe(get_post_thumbnail_id($post_id), 'full');
            }; ?>
        </div>
		
		<?php

		woocommerce_template_single_price();
		
		woocommerce_template_single_add_to_cart();
		
		// Product meta
		$stock = get_post_meta(get_the_id(), '_stock', true);
		$duration = get_post_meta(get_the_id(), 'duration', true);
		$level = get_post_meta(get_the_id(), 'level', true);
		$subscription = get_post_meta(get_the_id(), 'subscription', true);
		
		echo do_shortcode('[woocommerce-currency-selector format="{{code}}: {{name}} ({{symbol}})"]');
?>
		
	<?php if(!empty($level) or !empty($subscription)): ?>

	<div class="stm_product_sidebar_meta_units">
	
		<h3>Includes:</h3>
	
		<?php if(!empty($level)): ?>
			<div class="stm_product_sidebar_meta_unit">
				<table>
					<tr>
						<td class="icon"><i class="fas fa-graduation-cap"></i> <?php echo esc_attr(__('Level:', 'masterstudy')); ?></td>
						<td class="value h5"><?php echo esc_attr(__($level)); ?></td>
					</tr>
				</table>
			</div> <!-- unit -->
		<?php endif; ?>
		
		<?php if(!empty($subscription)): ?>
			<div class="stm_product_sidebar_meta_unit">
				<table>
					<tr>
						<td class="icon"><i class="fas fa-calendar-alt"></i> <?php echo esc_attr(__('Subscription:', 'masterstudy')); ?></td>
						<td class="value h5"><?php echo esc_attr(__($subscription)); ?></td>
					</tr>
				</table>
			</div> <!-- unit -->
		<?php endif; ?>
		
	</div><!-- units -->
	<?php endif; ?>
	
	<?php $gain_access = get_post_meta(get_the_id(), 'gain_access', true); ?>
	<?php if(!empty($gain_access)): ?>
		<div class="stm_product_sidebar_gain_access">
			<h4>Gain Access to:</h4>
			<ul>
			<?php
				$access_to = explode( PHP_EOL, $gain_access );
				foreach($access_to as $access){
					echo '<li>'.$access.'</li>';
				}
			?>
			</ul>
		</div>
	<?php endif; ?>
	</div>