<?php // Shop before sidebar template ?>

<?php
$post_id = get_the_ID();
?>

    <div class="stm-lms-course__sidebar">

        <div class="stm_lms_course__image hidden-on-phone">
            <?php if(function_exists('stm_get_VC_attachment_img_safe')) {
                echo stm_get_VC_attachment_img_safe(get_post_thumbnail_id($post_id), 'full');
            }; ?>
        </div>

		<?php

		woocommerce_template_single_price();
		
		$product_cats = get_the_terms($post_id, 'product_cat');
		foreach ( $product_cats as $category ) $categories[] = $category->slug;
		if( !in_array( 'melius-other', $categories ) ) {
			?>
			<script type="text/javascript">

      jQuery(document).ready(function($){
          
         $('form.cart').on( 'click', 'button.plus, button.minus', function() {
 
            // Get current quantity values
            var qty = $( this ).closest( 'form.cart' ).find( '.qty' );
            var val   = parseFloat(qty.val());
            var max = parseFloat(qty.attr( 'max' ));
            var min = parseFloat(qty.attr( 'min' ));
            var step = parseFloat(qty.attr( 'step' ));
 
            // Change the value if plus or minus
            if ( $( this ).is( '.plus' ) ) {
               if ( max && ( max <= val ) ) {
                  qty.val( max );
               } else {
                  qty.val( val + step );
               }
            } else {
               if ( min && ( min >= val ) ) {
                  qty.val( min );
               } else if ( val > 1 ) {
                  qty.val( val - step );
               }
            }
             
         });
          
      });
          
      </script><?php
		}
		$product = wc_get_product( $post_id );
		if($product->is_type( 'external' )) {
			?><a href="https://store.thinkmelius.com/" class="single_add_to_cart_button button alt">Buy T Shirt</a><?php
		}
		else {
			woocommerce_template_single_add_to_cart();
		}
		
		// Product meta
		$stock = get_post_meta(get_the_id(), '_stock', true);
		$duration = get_post_meta(get_the_id(), 'duration', true);
		$level = get_post_meta(get_the_id(), 'level', true);
		$subscription = get_post_meta(get_the_id(), 'subscription', true);
		
		echo do_shortcode('[woocommerce-currency-selector format="{{code}}: {{name}} ({{symbol}})"]');
?>
	<?php if(!empty($level) or !empty($subscription)): ?>

	<div class="stm_product_sidebar_meta_units">
	
		<h3>Includes:</h3>
	
		<?php if(!empty($level)): ?>
			<div class="stm_product_sidebar_meta_unit">
				<table>
					<tr>
						<td class="icon"><i class="fas fa-graduation-cap"></i> <?php echo esc_attr(__('Level:', 'masterstudy')); ?></td>
						<td class="value h5"><?php echo esc_attr(__($level)); ?></td>
					</tr>
				</table>
			</div> <!-- unit -->
		<?php endif; ?>
		
		<?php if(!empty($subscription)): ?>
			<div class="stm_product_sidebar_meta_unit">
				<table>
					<tr>
						<td class="icon"><i class="fas fa-calendar-alt"></i> <?php echo esc_attr(__('Subscription:', 'masterstudy')); ?></td>
						<td class="value h5"><?php echo esc_attr(__($subscription)); ?></td>
					</tr>
				</table>
			</div> <!-- unit -->
		<?php endif; ?>
		
	</div><!-- units -->
	<?php endif; ?>
	
	<?php $gain_access = get_post_meta(get_the_id(), 'gain_access', true); ?>
	<?php if(!empty($gain_access)): ?>
		<div class="stm_product_sidebar_gain_access">
			<h4>Gain Access to:</h4>
			<ul>
			<?php
				$access_to = explode( PHP_EOL, $gain_access );
				foreach($access_to as $access){
					echo '<li>'.$access.'</li>';
				}
			?>
			</ul>
		</div>
	<?php endif; ?>
	</div>