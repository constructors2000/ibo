<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

stm_lms_register_style('course');
stm_lms_register_style('course-udemy');
$udemy_meta = STM_LMS_Helpers::simplify_meta_array(get_post_meta(get_the_ID()));

get_header(); 


	$shop_sidebar_id = stm_option( 'shop_sidebar' );
	$shop_sidebar_position = stm_option( 'shop_sidebar_position', 'none' );
	$content_before = $content_after =  $sidebar_before = $sidebar_after = '';
	
	// For demo
	if(isset($_GET['sidebar_position']) and $_GET['sidebar_position']=='right') {
		$shop_sidebar_position = 'right';
	} elseif (isset($_GET['sidebar_position']) and $_GET['sidebar_position']=='left') {
		$shop_sidebar_position = 'left';
	}
		
	if( $shop_sidebar_id ) {
		$shop_sidebar = get_post( $shop_sidebar_id );
	}

    if(is_active_sidebar('shop')) {
        $shop_sidebar = 'widget_area';
        $shop_sidebar_position = 'right';
    }
	
	if( $shop_sidebar_position == 'right' ) {
		$content_before .= '<div class="row">';
			$content_before .= '<div class="col-md-8 col-sm-8">';
				// .products
			$content_after .= '</div>'; // col
			$sidebar_before .= '<div class="col-md-4 col-sm-4 udemy-sidebar-holder">';
				// .sidebar-area
			$sidebar_after .= '</div>'; // col
		$sidebar_after .= '</div>'; // row
	}
	
	if( $shop_sidebar_position == 'left' ) {
		$content_before .= '<div class="row">';
			$content_before .= '<div class="col-md-8 col-sm-8">';
				// .products
			$content_after .= '</div>'; // col
			$sidebar_before .= '<div class="col-md-4 col-sm-4 udemy-sidebar-holder">';
				// .sidebar-area
			$sidebar_after .= '</div>'; // col
		$sidebar_after .= '</div>'; // row
	};
?>

	<div class="container">
		<div class="stm-lms-wrapper stm_lms_udemy_course">

		<?php echo wp_kses_post($content_before); ?>
				
			<div class="sidebar_position_<?php echo esc_attr($shop_sidebar_position); ?>">
				<?php while ( have_posts() ) : the_post(); ?>
		
					<?php wc_get_template_part( 'content', 'single-product' ); ?>
		
				<?php endwhile; // end of the loop. ?>
			</div>
		
		<?php echo wp_kses_post($content_after); ?>
		
		<?php echo wp_kses_post($sidebar_before); ?>
		
			<div class="stm-lms-course__sidebar-holder hidden-sm hidden-xs">
				<?php while ( have_posts() ) : the_post(); ?>
		
					<?php wc_get_template_part( 'content', 'single-product-meta-side' ); ?>
		
				<?php endwhile; // end of the loop. ?>
			</div>
			
			<div class="shop_sidebar_single_page sidebar-area sidebar-area-<?php echo esc_attr($shop_sidebar_position); ?>">
				<?php
					if( isset( $shop_sidebar ) && $shop_sidebar_position != 'none' ) {
						if($shop_sidebar == 'widget_area') {
							dynamic_sidebar('shop');
						} else {
							echo apply_filters('the_content', $shop_sidebar->post_content);
						}
					}
				?>
			</div>
			
		<?php echo wp_kses_post($sidebar_after); ?>
			
	</div>
	</div> <!-- container -->

<?php get_footer(); ?>
