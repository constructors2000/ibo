<div class="stm_woo_helpbar clearfix">
	<?php
	$get_array = $_GET;
	$params_grid = array_merge($get_array, array("view_type" => "grid"));
	$new_query_grid = http_build_query($params_grid);

	$params_list = array_merge($get_array, array("view_type" => "list"));
	$new_query_list = http_build_query($params_list);

	$active_type = 'active_' . stm_option('shop_layout');
	if (isset($_GET['view_type']) and $_GET['view_type'] == 'list') {
		$active_type = 'active_list';
	} elseif (isset($_GET['view_type']) and $_GET['view_type'] == 'grid') {
		$active_type = 'active_grid';
	}
	?>
			<?php
			// Building terms args
			$taxonomy = array(
				'product_cat',
			);

			$args = array(
				'hide_empty' => true,
			);

			$terms = get_terms($taxonomy, $args);

			$current_term = get_queried_object();
			
			if( is_shop() || is_front_page() ) {
				$all = 'active';
			}
			else {
				$all = 'non-active';
			}

			?>
				<?php if (!empty($terms)): ?>
                    <ul id="product_categories_filter">
                        <li>
							<a href="<?php echo get_post_type_archive_link('product') ?>" class="<?php echo sanitize_text_field($all); ?> product-category-link">
								<?php _e('All Categories', 'masterstudy'); ?>
							</a>
                        </li>
						<?php foreach ($terms as $term): ?>
							<?php
							$selected = 'active';
							if (!empty($current_term->slug) and $term->slug == $current_term->slug) {
								$selected = 'active';
							} else {
								$selected = 'non-active';
							}
							?>
                            <li>
								<a href="<?php echo esc_url(get_term_link($term)); ?>" class="<?php echo sanitize_text_field($selected); ?> product-category-link">
									<?php echo sanitize_text_field($term->name); ?>
								</a>
                            </li>
						<?php endforeach; ?>
                    </ul>
				<?php endif; ?>
</div>