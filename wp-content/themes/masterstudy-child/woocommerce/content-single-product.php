<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

$experts = get_post_meta(get_the_id(), 'course_expert', true);
$rating_enabled = get_option('woocommerce_enable_reviews');

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>

    <div class="stm_lms_udemy_bar">

		<div class="stm_lms_course__image visible-on-phone">
            <?php if(function_exists('stm_get_VC_attachment_img_safe')) {
                echo stm_get_VC_attachment_img_safe(get_post_thumbnail_id($post_id), 'full');
            }; ?>
        </div>

        <h1 class="stm_lms_course__title"><?php the_title(); ?></h1>

			<?php STM_LMS_Templates::show_lms_template('course/udemy/parts/headline', array('udemy_meta' => $udemy_meta)); ?>

			<?php STM_LMS_Templates::show_lms_template('course/udemy/parts/panel_info', array('udemy_meta' => $udemy_meta)); ?>

			<?php STM_LMS_Templates::show_lms_template('course/udemy/parts/panel_info/rate'); ?>

			<div class="pull-left xs-comments-left">
				<?php remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 ); ?>
				<?php do_action('woocommerce_after_shop_loop_item_title'); ?>
				<?php $comments_num = get_comments_number(get_the_id()); ?>
				<?php if ($comments_num): ?>
                    <div class="meta-unit text-right xs-text-left">
                        <div class="value h6"><?php echo esc_attr($comments_num) . ' ' . __('Reviews', 'masterstudy'); ?></div>
                    </div>
				<?php endif; ?>
            </div>

			<?php STM_LMS_Templates::show_lms_template('global/wish-list-course', array('course_id' => $post_id)); ?>

    </div>

    <!-- Sidebar visible sm and xs -->
    <div class="stm_product_meta_single_page visible-sm visible-xs">
		<?php wc_get_template_part('content-single-product-meta-side'); ?>
    </div>

	<div class="stm_lms_course__content">
		<!-- Content -->
		<?php the_content(); ?>
		<!-- Content END -->
	</div>


<?php do_action('woocommerce_after_single_product'); ?>
