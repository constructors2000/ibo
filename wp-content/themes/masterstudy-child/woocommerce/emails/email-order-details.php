<?php
/**
 * Order details table shown in emails.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.3.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$text_align = is_rtl() ? 'right' : 'left';

do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>

<h2>
	<?php
	if ( $sent_to_admin ) {
		$before = '<a class="link" href="' . esc_url( $order->get_edit_order_url() ) . '">';
		$after  = '</a>';
	} else {
		$before = '';
		$after  = '';
	}
	/* translators: %s: Order ID. */
	echo wp_kses_post( $before . sprintf( __( '[Order #%s]', 'woocommerce' ) . $after . ' (<time datetime="%s">%s</time>)', $order->get_order_number(), $order->get_date_created()->format( 'c' ), wc_format_datetime( $order->get_date_created() ) ) );
	?>
</h2>

<div style="margin-bottom: 40px;">
	<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
		<tbody>
			<tr>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Full Name:', 'woocommerce' ); ?></th>
				<td class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php echo $order->get_billing_first_name() .' '. $order->get_billing_last_name();?></td>
			</tr>
			<tr>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Email:', 'woocommerce' ); ?></th>
				<td class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php echo $order->get_billing_email();?></td>
			</tr>
			<tr>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'For whom are you buying?', 'woocommerce' ); ?></th>
				<td class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;">
				<?php echo get_post_meta( $order->get_id(), 'someone_else', true ) == 'No'? "Myself": "Somebody else"; ?></br>
				<?php if(!empty(get_post_meta( $order->get_id(), 'legacynumber', true ))) { ?>
							<?php echo get_post_meta( $order->get_id(), 'legacynumber', true ); ?>
				<?php } ?>
				<?php if(!empty(get_post_meta( $order->get_id(), 'User Name', true ))) { ?>
							<?php echo '('.get_post_meta( $order->get_id(), 'User Name', true ).')'; ?>
				<?php } ?>
				</td>
			</tr>
			
			<?php 
			$items = $order->get_items();
			foreach ( $items as $key => $item ) {
				$subscription = $item['subscription'];
			}
			if(!empty($subscription)) {
			?>
			
			<?php } ?>
			
			<tr>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Payment Source:', 'woocommerce' ); ?></th>
				<td class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php echo get_post_meta( $order->get_id(), '_payment_method', true ); ?></td>
			</tr>
			
			<?php
			$coupons = $order->get_used_coupons();
			
			global $woocommerce;

			if(!empty($coupons)) {
			?>
			<tr>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Coupon:', 'woocommerce' ); ?></th>
				<td class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php foreach($coupons as $coupon) { $c = new WC_Coupon($coupon); echo '<p>'. $coupon .'</p><p>'. $c->get_description() .'</p>'; } ?></td>
			</tr>
			<?php } ?>

			<?php
			echo wc_get_email_order_items( $order, array(
				'show_sku'      => $sent_to_admin,
				'show_image'    => false,
				'image_size'    => array( 32, 32 ),
				'plain_text'    => $plain_text,
				'sent_to_admin' => $sent_to_admin,
			) );
			?>

			<?php
				$privacy = get_post_meta( $order->get_id(), 'Privacy', true );
				$policies = get_post_meta( $order->get_id(), 'Return Policies', true );
				
				if($privacy == 1 & $policies == 1) {
					$accepted = "Accepted";
				}
				else {
					$accepted = "Not accepted";
				}
			?>
			
			<tr>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'T&C and 7 Days Refund Policy:', 'woocommerce' ); ?></th>
				<td class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php echo $accepted; ?></td>
			</tr>

			<tr>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Subtotal:', 'woocommerce' ); ?></th>
				<td class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php echo $order->get_subtotal(); ?></td>
			</tr>
			
		</tbody>
	</table>
</div>

<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>
