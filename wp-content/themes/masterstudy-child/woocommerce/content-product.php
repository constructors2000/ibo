<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

// Extra post classes
$classes = array();


$shop_sidebar_position = stm_option( 'shop_sidebar_position', 'none' );
if(isset($_GET['sidebar_position']) and $_GET['sidebar_position'] == 'none') {
	$shop_sidebar_position = 'none';
}

if(is_cart()){
    $classes[] = 'col-md-6 col-sm-6 col-xs-12 course-col';
}
elseif($shop_sidebar_position == 'none') {
	$classes[] = 'col-md-3 col-sm-4 col-xs-6 course-col';
} else {
	$classes[] = 'col-md-3 col-sm-4 col-xs-6 course-col';
}
?>
<!-- Custom Meta -->
<?php
$experts = get_post_meta(get_the_id(), 'course_expert', true);
$stock = get_post_meta(get_the_id(), '_stock', true );
$regular_price = get_post_meta(get_the_id(), '_regular_price', true );
$sale_price = get_post_meta(get_the_id(), '_sale_price', true );


$product_cats = get_the_terms($product->ID, 'product_cat');
foreach ( $product_cats as $category ) $categories[] = $category->slug;
if( in_array( 'melius', $categories ) ) {
?>
<li <?php post_class( $classes ); ?>>
	<?php
	/**
	 * woocommerce_before_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );
	?>

	<div class="stm_archive_product_inner_unit heading_font">
		<div class="stm_archive_product_inner_unit_centered">

			<div class="stm_featured_product_image">
				<?php if($product->is_type( 'simple' ) || $product->is_type( 'variable' )) {  ?>
					<?php $status = get_post_meta($product->get_id(), 'course_status', false); ?>
					<?php if(!empty($status)): ?>
						<div class="stm_lms_post_status heading_font <?php echo $status; ?>">
							<?php echo $status[0]; ?>
						</div>
					<?php endif; ?>

				<?php } ?>

				<?php if(has_post_thumbnail()): ?>
					<a href="<?php the_permalink() ?>" title="<?php esc_attr_e('View course', 'masterstudy') ?> - <?php the_title(); ?>">
						<?php the_post_thumbnail('img-270-283', array('class'=>'img-responsive')); ?>
					</a>
				<?php else: ?>
					<div class="no_image_holder"></div>
				<?php endif; ?>

			</div>



			<div class="stm_featured_product_body">
				<?php
				if (!empty($product_cats)):?>
                    <div class="xs-product-cats">
                        <div class="meta-unit categories clearfix">
                            <div class="meta_values">
                                <div class="value h6">
									<?php foreach ($product_cats as $product_cat): ?>
                                        <!--<a href="<?php echo get_term_link($product_cat); ?>">-->
                                            <?php echo sanitize_text_field($product_cat->name); ?>
                                        <!--</a>-->
									<?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
				<?php endif; ?>
				<a href="<?php the_permalink() ?>"  title="<?php esc_attr_e('View course', 'masterstudy') ?> - <?php the_title(); ?>">
					<div class="title"><?php the_title(); ?></div>
				</a>
			</div>

			<div class="stm_featured_product_footer">
				<div class="clearfix">
					<div class="pull-right">
						<?php woocommerce_template_loop_price();?>
					</div>
				</div>
			</div>

		</div> <!-- stm_archive_product_inner_unit_centered -->
		<?php STM_LMS_Templates::show_lms_template('courses/parts/course_info',
			array_merge(array(
				'post_id' => $product->get_id(),
			), compact('post_status', 'sale_price', 'price', 'author_id', 'id'))
		); ?>
	</div> <!-- stm_archive_product_inner_unit -->
</li>
<?php } ?>