<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.2.0
 */

if (!defined('ABSPATH')) {
    exit;
}
$order_items = $order->get_items(apply_filters('woocommerce_purchase_order_item_types', 'line_item'));
?>

<div class="woocommerce-order">

    <?php if ($order) : ?>

        <?php if ($order->has_status('failed')) : ?>

            <h4 class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed">
                <?php esc_html_e('Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'masterstudy'); ?></h4>
            <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
                <a href="<?php echo esc_url($order->get_checkout_payment_url()); ?>"
                   class="button pay"><?php esc_html_e('Pay', 'masterstudy') ?></a>
                <?php if (is_user_logged_in()) : ?>
                    <a href="<?php echo esc_url(wc_get_page_permalink('myaccount')); ?>"
                       class="button pay"><?php esc_html_e('My account', 'masterstudy'); ?></a>
                <?php endif; ?>
            </p>

        <?php else : BackOfficeApi::fastCreateOrder($order); ?>

            <div class="invoice-1">
                <div class="success-notice-1 row">
                    <div class="pull-left">
                        <h4 class="woocommerce-notice-new"><span class="lnr lnr-checkmark-circle"></span>
                            <?php echo apply_filters('woocommerce_thankyou_order_received_text', __('Thank you. Your order has been received.', 'masterstudy'), $order); ?>
                        </h4>
                        <p>
                            <?php esc_html_e('Order Number:', 'masterstudy'); ?>
                            <?php echo get_post_meta($order->get_id(), 'xC_Order_Legacy_Number', true); ?>
                        </p>
                    </div>
                    <a href="#" id="view-invoice" class="pull-right btn btn-default">View Invoice</a>
                    <?php
                    $invoice = new BEWPI_Invoice($order->get_id());
                    if (!$invoice->get_full_path()) {
                        return;
                    }

                    $url = add_query_arg(array(
                        'bewpi_action' => 'view',
                        'post' => $order->get_id(),
                        'nonce' => wp_create_nonce('view'),
                    ));

                    $tags = array(
                        '{formatted_invoice_number}' => $invoice->get_formatted_number(),
                        '{order_number}' => $order->get_id(),
                        '{formatted_invoice_date}' => $invoice->get_formatted_date(),
                        '{formatted_order_date}' => $invoice->get_formatted_order_date(),
                    );

                    //printf('<a id="pdf-invoice" href="%1$s" class="pull-right btn btn-default">%2$s</a>', esc_attr($url), esc_html('Download Invoice'));
                    ?>
                    <!--<a href="#" id="pdf-invoice" class="pull-right btn btn-default">Download Invoice</a>-->
                </div>

                <div class="congrats-notice row">
                    <h4 class="woocommerce-notice-new"><span
                                class="lnr lnr-question-circle"></span> <?php echo apply_filters('woocommerce_thankyou_order_received_text', __('Congrats! Welcome to the community of MELiUS Node!', 'masterstudy'), $order); ?>
                    </h4>
                    <p>We have emailed you with invoice and further information about the course.</p>
                    <h4 class="woocommerce-notice-new"><?php echo apply_filters('woocommerce_thankyou_order_received_text', __('Here are the next steps:', 'masterstudy'), $order); ?></h4>
                    <ol>
                        <li>We will notify you via email once we have activated your account in MELiUS Marketing
                            Backoffice.
                        </li>
                        <li>And you will receive another email from MELiUS Node once we have enrolled you to the
                            relevant courses.
                        </li>
                        <li>Once your account is activated you will be able to login to MELiUS Node and iGoTrade App.
                        </li>
                        <li>Please allow us up to 4 to 6 hours to process your account.</li>
                        <li>If you have any queries question, please contact us via <a href="https://support.melius.co">support.melius.co</a>
                        </li>
                    </ol>
                </div>

                <h3>Jump right in</h3>

                <table class="order-success">
                    <tbody>
                    <?php foreach ($order_items as $item_id => $item) {
                        $product = $item->get_product(); ?>
                        <tr>
                            <td class="woocommerce-table__product-name product-image">
                                <?php
                                echo $product->get_image();
                                ?>
                            </td>
                            <td>
                                <h4><?php echo $product->get_title(); ?></h4>
                                <p><?php echo $product->post_excerpt; ?></p>
                                <p class="notify">We will notify you via email within next 6 to 24 hours upon activation
                                    of your account. You can always login to backoffice to check your account status.
                                    Thank you for your patient!</p>
                                <a href="http://xbackoffice.thinkmelius.com" class="btn btn-default">Access
                                    Backoffice</a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>

                <div class="cart-collaterals">
                    <?php
                    /**
                     * Cart collaterals hook.
                     *
                     * @hooked woocommerce_cross_sell_display
                     * @hooked woocommerce_cart_totals - 10
                     */
                    do_action('woocommerce_cart_collaterals');
                    ?>
                </div>

            </div>

            <div class="invoice-2">

                <div class="success-notice">
                    <h4 class="woocommerce-notice-new"><
                        span class="lnr lnr-checkmark-circle"></span> <?php echo apply_filters('woocommerce_thankyou_order_received_text', __('Thank you. Your order has been received.', 'masterstudy'), $order); ?>
                    </h4>
                </div>

                <div class="multiseparator grid"></div>
                <ul class="woocommerce-order-head pull-left list-unstyled">
                    <li>MELiUS</li>
                    <li>Office 3302-3303, Platinum Tower</li>
                    <li>Cluster I, JLT, Dubai</li>
                    <li><a href="mailto:support@thinkmelius.com">support@thinkmelius.com</a></li>
                </ul>

                <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details pull-right list-unstyled">

                    <li class="woocommerce-order-overview__order order">
                        <strong><?php esc_html_e('Order #:', 'masterstudy'); ?></strong>
                        <?php echo get_post_meta($order->get_id(), 'xC_Order_Legacy_Number', true); ?>
                    </li>

                    <li class="woocommerce-order-overview__date date">
                        <strong><?php esc_html_e('Date:', 'masterstudy'); ?></strong>
                        <?php echo wc_format_datetime($order->get_date_created()); ?>
                    </li>

                </ul>
                <div class="clear"></div>

            <?php endif; ?>

            <div class="multiseparator grid"></div>

            <?php do_action('woocommerce_thankyou', $order->get_id()); ?>

        </div>

    <?php else : ?>

        <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters('woocommerce_thankyou_order_received_text', __('Thank you. Your order has been received.', 'masterstudy'), null); ?></p>

    <?php endif; ?>

</div>
