<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/** @global WC_Checkout $checkout */
?> <?php if (is_user_logged_in()) {
    $displaynone = 'display:none;';
} else {
    $displaynone = '';
} ?>
<ul style="<?php echo $displaynone; ?>" class="tab-group">
    <li class="tab active"><a class="tab1" href="#signup">I am new user</a></li>
    <li class="tab"><a class="tab2" href="#login">I am existing user</a></li>
</ul>

<div style="<?php echo $displaynone; ?>" class="tab-content">
    <div id="signup">

        <div class="checkout-panel">
            <div class="panel-body">


                <div class="input-fields">

                    <?php if (wc_ship_to_billing_address_only() && WC()->cart->needs_shipping()) : ?>


                    <?php else : ?>


                    <?php endif; ?>



                    <?php if (!is_user_logged_in() && $checkout->is_registration_enabled()) : ?>
                        <div class="woocommerce-account-fields">
                            <?php if (!$checkout->is_registration_required()) : ?>

                                <p class="form-row form-row-wide create-account">
                                    <input class="input-checkbox"
                                           id="createaccount" <?php checked((true === $checkout->get_value('createaccount') || (true === apply_filters('woocommerce_create_account_default_checked', false))), true) ?>
                                           type="checkbox" name="createaccount" value="1"/>
                                    <label for="createaccount"
                                           class="checkbox"><?php esc_html_e('Create an account?', 'masterstudy'); ?></label>
                                </p>

                            <?php endif; ?>

                            <?php do_action('woocommerce_before_checkout_registration_form', $checkout); ?>

                            <?php if ($checkout->get_checkout_fields('account')) : ?>

                                <div class="create-account">
                                    <p class="mg-bt-10"><?php esc_html_e('Create an account by entering the information below. If you are a returning customer please login at the top of the page.', 'masterstudy'); ?></p>
                                    <?php foreach ($checkout->get_checkout_fields('account') as $key => $field) : ?>
                                        <?php woocommerce_form_field($key, $field, $checkout->get_value($key)); ?>
                                    <?php endforeach; ?>
                                    <div class="clear"></div>
                                </div>

                            <?php endif; ?>

                            <?php do_action('woocommerce_after_checkout_registration_form', $checkout); ?>
                        </div>
                    <?php endif; ?>


                    <div class="register-new" data-ok="0" data-click="1">
                        <div style="display:none;" class="row load-spin">
                            <div class="login-loading col-md-12"></div>
                        </div>
                        <div style="color:red;" class="error-user error-api"></div>
                        <div class="errortable">
                            <h4 style="color:red;"></h4>
                        </div>

                        <div class="row">
                            <div class="column-1 col-md-6">
                                <label for="id">Enroller ID <a
                                            title="Please enter the ID of that person who referred you.<br> if you don't have that ID Please ask that person<br> to provide you ID"
                                            class="podska vtip">?</a></label>
                                <div style="color:red;" class="error-user error-user_enroller_Id"></div>
                                <input type="text" id="user_enroller_Id" placeholder="Enter Enroller ID"
                                       value="<?php echo $_SESSION['EnrollerId'] ?: ''; ?>"/>
                            </div>
                        </div>

                        <div class="row">
                            <div class="column-1 col-md-6">
                                <label for="Customer">IBO/Customer</label>
                                <div style="color:red;" class="error-user error-user_type"></div>
                                <select id="user_type" class="selectt buttonsss">
                                    <option value="customer" selected>Customer</option>
                                    <option value="ibo">IBO</option>
                                </select>
                            </div>

                            <div class="column-1 col-md-6">
                                <label for="country">Country</label>
                                <select id="user_country" class="selectt">
                                    <option value="7" selected="selected">United States</option>
                                    <option value="193">Aland Islands</option>
                                    <option value="194">Albania</option>
                                    <option value="195">Algeria</option>
                                    <option value="196">American Samoa</option>
                                    <option value="197">Andorra</option>
                                    <option value="198">Angola</option>
                                    <option value="199">Anguilla</option>
                                    <option value="200">Antigua and Barbuda</option>
                                    <option value="201">Argentina</option>
                                    <option value="202">Armenia</option>
                                    <option value="203">Aruba</option>
                                    <option value="204">Australia</option>
                                    <option value="205">Austria</option>
                                    <option value="206">Azerbaijan</option>
                                    <option value="207">Bahamas</option>
                                    <option value="208">Bahrain</option>
                                    <option value="209">Bangladesh</option>
                                    <option value="210">Barbados</option>
                                    <option value="211">Belarus</option>
                                    <option value="212">Belgium</option>
                                    <option value="213">Belize</option>
                                    <option value="214">Benin</option>
                                    <option value="215">Bermuda</option>
                                    <option value="216">Bhutan</option>
                                    <option value="217">Bolivia</option>
                                    <option value="218">Bosnia and Herzegovina</option>
                                    <option value="219">Botswana</option>
                                    <option value="220">Brazil</option>
                                    <option value="221">British Virgin Islands</option>
                                    <option value="222">Brunei Darussalam</option>
                                    <option value="223">Bulgaria</option>
                                    <option value="224">Burkina Faso</option>
                                    <option value="225">Burundi</option>
                                    <option value="226">Cambodia</option>
                                    <option value="227">Cameroon</option>
                                    <option value="228">Canada</option>
                                    <option value="229">Cape Verde</option>
                                    <option value="230">Cayman Islands</option>
                                    <option value="231">Chad</option>
                                    <option value="232">Chile</option>
                                    <option value="233">China</option>
                                    <option value="189">Colombia</option>
                                    <option value="236">Comoros</option>
                                    <option value="237">Congo (Brazzaville)</option>
                                    <option value="238">Congo, (Kinshasa)</option>
                                    <option value="415">Costa Rica</option>
                                    <option value="239">Côte d'Ivoire</option>
                                    <option value="240">Croatia</option>
                                    <option value="241">Cuba</option>
                                    <option value="242">Cyprus</option>
                                    <option value="243">Czech Republic</option>
                                    <option value="244">Denmark</option>
                                    <option value="245">Djibouti</option>
                                    <option value="246">Dominica</option>
                                    <option value="247">Dominican Republic</option>
                                    <option value="248">Ecuador</option>
                                    <option value="249">Egypt</option>
                                    <option value="250">El Salvador</option>
                                    <option value="251">Equatorial Guinea</option>
                                    <option value="252">Eritrea</option>
                                    <option value="253">Estonia</option>
                                    <option value="254">Ethiopia</option>
                                    <option value="255">Faroe Islands</option>
                                    <option value="256">Fiji</option>
                                    <option value="257">Finland</option>
                                    <option value="258">France</option>
                                    <option value="259">French Polynesia</option>
                                    <option value="260">Gabon</option>
                                    <option value="261">Gambia</option>
                                    <option value="262">Georgia</option>
                                    <option value="188">Germany</option>
                                    <option value="263">Ghana</option>
                                    <option value="264">Gibraltar</option>
                                    <option value="265">Greece</option>
                                    <option value="266">Greenland</option>
                                    <option value="267">Grenada</option>
                                    <option value="268">Guadeloupe</option>
                                    <option value="269">Guam</option>
                                    <option value="270">Guatemala</option>
                                    <option value="272">Guernsey</option>
                                    <option value="271">Guinea</option>
                                    <option value="273">Guinea-Bissau</option>
                                    <option value="274">Guyana</option>
                                    <option value="275">Haiti</option>
                                    <option value="276">Honduras</option>
                                    <option value="234">Hong Kong</option>
                                    <option value="277">Hungary</option>
                                    <option value="278">Iceland</option>
                                    <option value="279">India</option>
                                    <option value="280">Indonesia</option>
                                    <option value="281">Iran</option>
                                    <option value="282">Iraq</option>
                                    <option value="283">Ireland</option>
                                    <option value="284">Italy</option>
                                    <option value="285">Jamaica</option>
                                    <option value="286">Japan</option>
                                    <option value="287">Jordan</option>
                                    <option value="288">Kazakhstan</option>
                                    <option value="289">Kenya</option>
                                    <option value="290">Kiribati</option>
                                    <option value="292">Kuwait</option>
                                    <option value="293">Kyrgyzstan</option>
                                    <option value="294">Lao</option>
                                    <option value="338">Lebanon</option>
                                    <option value="339">Lesotho</option>
                                    <option value="340">Liechtenstein</option>
                                    <option value="295">Lithuania</option>
                                    <option value="296">Luxembourg</option>
                                    <option value="235">Macao</option>
                                    <option value="341">Macedonia</option>
                                    <option value="342">Madagascar</option>
                                    <option value="343">Malawi</option>
                                    <option value="297">Malaysia</option>
                                    <option value="344">Maldives</option>
                                    <option value="345">Mali</option>
                                    <option value="298">Malta</option>
                                    <option value="346">Martinique</option>
                                    <option value="347">Mauritania</option>
                                    <option value="348">Mauritius</option>
                                    <option value="187">Mexico</option>
                                    <option value="349">Moldova</option>
                                    <option value="299">Monaco</option>
                                    <option value="350">Mongolia</option>
                                    <option value="351">Montenegro</option>
                                    <option value="300">Morocco</option>
                                    <option value="352">Mozambique</option>
                                    <option value="301">Myanmar</option>
                                    <option value="353">Namibia</option>
                                    <option value="355">Nauru</option>
                                    <option value="354">Nepal</option>
                                    <option value="190">Netherlands</option>
                                    <option value="356">New Caledonia</option>
                                    <option value="302">New Zealand</option>
                                    <option value="357">Nicaragua</option>
                                    <option value="303">Niger</option>
                                    <option value="304">Nigeria</option>
                                    <option value="305">Norway</option>
                                    <option value="306">Oman</option>
                                    <option value="307">Pakistan</option>
                                    <option value="374">Palau</option>
                                    <option value="308">Panama</option>
                                    <option value="358">Papua New Guinea</option>
                                    <option value="309">Paraguay</option>
                                    <option value="310">Peru</option>
                                    <option value="186">Philippines</option>
                                    <option value="311">Pitcairn</option>
                                    <option value="312">Poland</option>
                                    <option value="313">Portugal</option>
                                    <option value="314">Puerto Rico</option>
                                    <option value="359">Qatar</option>
                                    <option value="191">Rest of the World</option>
                                    <option value="361">Reunion</option>
                                    <option value="315">Romania</option>
                                    <option value="360">Rwanda</option>
                                    <option value="362">Saudi Arabia</option>
                                    <option value="363">Senegal</option>
                                    <option value="316">Serbia</option>
                                    <option value="317">Seychelles</option>
                                    <option value="318">Sierra Leone</option>
                                    <option value="319">Singapore</option>
                                    <option value="320">Slovakia</option>
                                    <option value="321">Slovenia</option>
                                    <option value="322">Somalia</option>
                                    <option value="323">South Africa</option>
                                    <option value="291">South Korea</option>
                                    <option value="324">Spain</option>
                                    <option value="325">Sri Lanka</option>
                                    <option value="326">Sudan</option>
                                    <option value="327">Suriname</option>
                                    <option value="328">Sweden</option>
                                    <option value="329">Switzerland</option>
                                    <option value="330">Taiwan</option>
                                    <option value="364">Tajikistan</option>
                                    <option value="365">Tanzania</option>
                                    <option value="192">Thailand</option>
                                    <option value="366">Togo</option>
                                    <option value="367">Tonga</option>
                                    <option value="368">Trinidad and Tobago</option>
                                    <option value="369">Tunisia</option>
                                    <option value="370">Turkey</option>
                                    <option value="371">Turkmenistan</option>
                                    <option value="372">Uganda</option>
                                    <option value="331">Ukraine</option>
                                    <option selected="selected" value="118">United Kingdom</option>
                                    <option value="332">Uruguay</option>
                                    <option value="333">Uzbekistan</option>
                                    <option value="334">Venezuela</option>
                                    <option value="335">Vietnam</option>
                                    <option value="336">Virgin Islands, US</option>
                                    <option value="373">Zambia</option>
                                    <option value="337">Zimbabwe</option>
                                </select>
                            </div>
                        </div>


                        <div class="row">
                            <div class="column-1 col-md-6">
                                <label for="firstname">First name</label>
                                <div style="color:red;" class="error-user error-user_first_name"></div>
                                <input type="text" id="user_first_name" placeholder="Enter First name"/>
                            </div>
                            <div class="column-1 col-md-6">
                                <label for="lastname">Last name</label>
                                <div style="color:red;" class="error-user error-user_last_name"></div>
                                <input type="text" id="user_last_name" placeholder="Enter Last name"/>
                            </div>

                        </div>
                        <div class="row">
                            <div class="column-1 col-md-6">
                                <label for="SSN">SSN No./Government ID <a
                                            title="If you do not have Government ID,<br> you can leave it blank."
                                            class="podska vtip">?</a></label>
                                <div style="color:red;" class="error-user error-user_government_id"></div>
                                <input type="text" maxlength="14" id="user_government_id"
                                       placeholder="Enter SSN No./Government ID"/>
                            </div>
                            <div class="column-1 col-md-6">
                                <label for="DOB">DOB</label>
                                <div style="color:red;" class="error-user error-user_dob"></div>
                                <input type="date" name="bday" max="3000-12-31" min="1000-01-01" id="user_dob"
                                       placeholder="Enter Last name"/>
                            </div>
                        </div>

                        <div class="row">
                            <div class="column-1 col-md-6">
                                <label for="country">Language</label>
                                <div style="color:red;" class="error-user error-user_lang"></div>
                                <select id="user_lang" class="selectt">
                                    <option value="es">Spanish</option>
                                    <option value="en" selected="selected">English (United Kingdom)</option>
                                </select>
                            </div>
                            <div class="column-1 col-md-6">
                                <label for="email">Email</label>
                                <div style="color:red;" class="error-user error-user_email"></div>
                                <input type="email" id="new_user_email" placeholder="Enter Email"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column-1 col-md-6">
                                <label for="Phone">Phone number</label>
                                <div style="color:red;" class="error-user error-user_phone"></div>
                                <input type="text" id="user_phone" placeholder="Enter Phone"/>
                            </div>
                            <div class="column-1 col-md-6">
                                <label for="Username">Username</label>
                                <div style="color:red;" class="error-user error-user_login"></div>
                                <input type="Username" id="user_login" placeholder="Enter Username"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column-1 col-md-6">
                                <label for="password">Password <a
                                            title=" - Password cannot contain 4 or more alphabetically consecutive characters.<br>
                                                    - Password cannot contain 3 or more consecutive numbers.<br>
                                                    - Password cannot contain 3 or more repeating characters.<br>
                                                    - Password cannot contain 4 or more QWERTY-keyboard-consecutive characters<br>
                                                    - Password cannot be easily-guessable."
                                            class="podska vtip">?</a></label>
                                <div style="color:red;" class="error-user error-user_pass"></div>
                                <div class="error-password"></div>
                                <input type="password" id="new_user_password" placeholder="Enter Password"/>
                            </div>
                            <div class="column-1 col-md-6">
                                <label for="password1">Confirm Password</label>
                                <div style="color:red;" class="error-user error-user_confirm_pass"></div>
                                <input type="password" id="user_confirm_pass" placeholder="Confirm Password"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column-1 col-md-12"><br><br>
                                <div style="color:red;" class="error-user_check"></div>
                            </div>
                            <div class="column-1 col-md-12 pravila1"><br>

                                <input type="checkbox" value="" name="checkbox" id="agree4"/> I have read, understood
                                and
                                agree to <a target="_blank"
                                            href="http://shop.thinkmelius.com/wp-content/uploads/2019/07/terms-1.pdf">Terms
                                    of Use</a> and <a target="_blank"
                                                      href="http://shop.thinkmelius.com/wp-content/uploads/2019/07/procedures.pdf">Policies
                                    & Procedures</a><br> <br>
                                <input type="checkbox" value="" name="checkbox" id="agree5"/> I have read, understood
                                and
                                agree to <a target="_blank"
                                            href="http://shop.thinkmelius.com/wp-content/uploads/2019/07/terms-1.pdf">Return/Refund/Cancellation
                                    Policy</a> and <a target="_blank"
                                                      href="http://shop.thinkmelius.com/wp-content/uploads/2019/07/privacy.pdf">MELiUS
                                    General Privacy Policy</a><br><br>

                                <!--input type="checkbox" value="" name="checkbox" id="agree6"/> I have read, understood and
                                agree to <a target="_blank"
                                            href="https://drive.google.com/open?id=1vGLcZ6YXFjt2mUtUp9kr8H-0mw6NlLWy">GDRP
                                    Policy</a> <br><br -->
                            </div>
                        </div>

                    </div>
                    <div><br><br>
                        <h2 class="error-ok" style="color:#009500 !important;"></h2></div>
                </div>
            </div>


            <h2>Billing address:</h2>


            <div class="input-fields">

            </div>
        </div>
    </div>
    <div id="login">

        <div class="checkout-panel">
            <div class="panel-body">

                <div id="loginbag" class="input-fields">


                    <?php if (is_user_logged_in()) { ?>
                        <h2>Hello, <?php echo wp_get_current_user()->user_login; ?></h2>
                        <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Logout">Logout</a>
                    <?php } else { ?>
                        <div class="row for-login" data-ok="0" data-click="1">
                            <div class="login-loading col-md-12"></div>
                            <div class="col-md-12">
                                <h4 align="center" style="color:red !important;" class="login-error-text"></h4>
                            </div>
                            <div class="column-1 col-md-6">
                                <label for="Username">Username</label>

                                <input type="text" name="login" id="user_login_name" placeholder="Enter Username"/>
                            </div>
                            <div class="column-1 col-md-6">
                                <label for="password">Password</label>
                                <input type="password" name="password" id="user_pass" placeholder="Enter Password"/>
                            </div>
                            <button class="request btn float-left login_btn login_btn2 col-md-4" type="button">LOGIN
                            </button>
                            <div class="message"></div>
                        </div>
                    <?php } ?>


                </div>
            </div>


            <h2>Billing address:</h2>


            <div class="input-fields">
            </div>
        </div>

    </div>

</div><!-- tab-content -->


<div class="input-fields">

    <?php if (is_user_logged_in() && !WC()->session->get('has_ibo')): ?>
        <select id="user_type" class="selectt buttonsss">
            <option value="customer">Customer</option>
            <option value="ibo">IBO</option>
        </select>
    <?php endif; ?>


    <div class="woocommerce-billing-fields">

        <?php do_action('woocommerce_before_checkout_billing_form', $checkout); ?>

        <div class="woocommerce-billing-fields__field-wrapper">
            <?php
            $fields = $checkout->get_checkout_fields('billing');

            foreach ($fields as $key => $field) {
                if (isset($field['country_field'], $fields[$field['country_field']])) {
                    $field['country'] = $checkout->get_value($field['country_field']);
                }
                woocommerce_form_field($key, $field, $checkout->get_value($key));
            }
            ?>
        </div>

        <?php do_action('woocommerce_after_checkout_billing_form', $checkout); ?>
    </div>
</div>
