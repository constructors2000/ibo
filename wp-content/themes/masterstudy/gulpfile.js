var gulp = require('gulp');
var lessToScss = require('gulp-less-to-scss');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const watch = require('gulp-watch');
const cssmin = require('gulp-cssmin');
var sourcemaps = require('gulp-sourcemaps');
var cache = require('gulp-cached');
var livereload = require('gulp-livereload');
const babel = require('gulp-babel');
const cssbeautify = require('gulp-cssbeautify');

gulp.task('serve', function () {
    "use strict";
    browserSync.init({
        proxy: "http://lms.loc",
        host: "192.168.0.124",
        port: 3000,
        notify: true,
        ui: {
            port: 3001
        },
        open: false
    });
});

gulp.task('watch', function () {
    watch('./assets/scss').on('change', (e) => {
        let directory = e.split('masterstudy');
        directory = directory[1];

        let dest = './assets/css/';

        let path = directory.replace('scss', 'css').split('/');
        let path2 = path.pop();

        if (directory.indexOf('vc_modules') !== -1) {
            dest = '.';
            path.forEach(function (v) {
                dest += v + '/';
            });
        } else if (path2[0] === '_') {
            directory = '/assets/scss/*.scss';
        }

        gulp.src(['.' + directory])
        //.pipe(sourcemaps.init())
            .pipe(sass({
                "sourceComments": true
            }).on('error', sass.logError))
            //.pipe(autoprefixer())
            //.pipe(cssmin())
            //.pipe(sourcemaps.write('../sourcemap'))
            .pipe(gulp.dest(dest))
            .pipe(browserSync.stream());
    });
});

gulp.task('admin-css', () => {
    livereload.listen();

    admin_css();

    watch('./assets/admin_scss/**/*.scss').on('change', (e) => {
        admin_css();
    });
});

gulp.task('es6', () => {
    //es6();
    console.log('ES 6');
    watch('./assets/es6/**/*.js').on('change', (e) => {
        es6(e);
    });
});

gulp.task('beautify_css', () => {
    gulp.src(['./assets/css/**/*.css'])
        .pipe(cssbeautify({
            indent: '  ',
            autosemicolon: true
        }))
        .pipe(gulp.dest('./assets/css/'))
});

function es6(e) {
    let src = (typeof e === 'undefined') ? './assets/es6/**/*.js' : e;
    let dest = (typeof e === 'undefined') ? './assets/js/' : e;
    if(typeof e !== 'undefined') {
        src = src.split('/masterstudy/');
        src = './' + src[1];

        dest = dest.split('/masterstudy/');
        dest = dest[1];
        dest = dest.split('/');
        dest.splice(-1,1);
        dest = './' + dest.join('/');
        dest = dest.replace('es6', 'js');
    }

    gulp.src(src)
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .on('error', console.error.bind(console))
        .pipe(gulp.dest(dest));

    gulp.src('./assets/es6/vc_modules/vue-autocomplete/vue2-autocomplete.js')
        .pipe(gulp.dest('./assets/js/vc_modules/vue-autocomplete/'));

}

function admin_css() {
    return gulp.src('./assets/admin_scss/**/*.scss')
        .pipe(cache('admin-css'))
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 4 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./assets/css/'))
        .pipe(livereload())
        .pipe(browserSync.stream());
}

gulp.task('default', ['watch', 'serve', 'admin-css', 'es6']);