
<?php /* Template Name: restore */ ?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=1,initial-scale=1,user-scalable=1" />
	<title>Login</title>

	<link href="http://fonts.googleapis.com/css?family=Lato:100italic,100,300italic,300,400italic,400,700italic,700,900italic,900" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/assets/css/style.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/c24d200276.js"></script>
</head>
<body>
	<section class="login-page">
	<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 left-login-page d-flex flex-column justify-content-center align-items-center">
		<div>
			<h1>Restore</h1>
			<p>Hi there! You are visiting xbackoffice.thinkmelius.com log in to proceed.</p>
		</div>
		</div>
		<div class="col-md-6 right-login-page d-flex flex-column justify-content-center align-items-center">
			<a href="<?php echo get_site_url();?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/logo-login.png" /></a>
				<h4 class="line">Reset Password</h4>
				<div class="login-loading-product"><img class='pload' style='display:none;margin:0 auto; width:50px; height:auto;' src='<?php echo get_template_directory_uri();?>/images/loading.gif' /></div> 
				<div style="color:red;"><h5  class="errorka"></h5></div>
				<form class="formlogin form1">
					
				</form>
				<div class="formlogin form2" >
					<?php echo do_shortcode('[reset_password]'); ?>
				</div>
				
				<a id="forgotpass" class="forgotpass" href="/login">Sign in</a>
		</div>	
		</div>
		</div>
	</section>
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="<?php echo get_template_directory_uri();?>/js/login.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>