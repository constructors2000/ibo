jQuery("#forgotpassword").on("click", function (e) {
	e.preventDefault();
	$(this).fadeOut(200);
	$(this).siblings(".formlogin").toggle(200);
})


// jQuery(document).on("ready", function(){
// jQuery("[name=\"payment_method\"]").each(function(e){
// if(jQuery(this).attr("checked")) {
// jQuery("li.wc_payment_method").removeClass("border-payment");
// jQuery(this).parent().addClass("border-payment");
// }
// else jQuery(this).parent().removeClass("border-payment");
// });
// });


// jQuery("[name=\"payment_method\"]").change(function(e){

// if(jQuery(this).attr("checked")) {
// jQuery("li.wc_payment_method").removeClass("border-payment");
// jQuery(this).parent().addClass("border-payment");
// }
// else jQuery(this).parent().removeClass("border-payment");
// }); 

jQuery('.form').find('input, textarea').on('keyup blur focus', function (e) {

	var $this = jQuery(this),
		label = $this.prev('label');

	if (e.type === 'keyup') {
		if ($this.val() === '') {
			label.removeClass('active highlight');
		} else {
			label.addClass('active highlight');
		}
	} else if (e.type === 'blur') {
		if ($this.val() === '') {
			label.removeClass('active highlight');
		} else {
			label.removeClass('highlight');
		}
	} else if (e.type === 'focus') {

		if ($this.val() === '') {
			label.removeClass('highlight');
		} else if ($this.val() !== '') {
			label.addClass('highlight');
		}
	}

});

jQuery("input[required]").addClass("is_required");

jQuery('.tab a').on('click', function (e) {

	e.preventDefault();

	jQuery(this).parent().addClass('active');
	jQuery(this).parent().siblings().removeClass('active');

	target = jQuery(this).attr('href');

	jQuery('.tab-content > div').not(target).hide();

	jQuery(target).find("is_required").prop("required", true);
	sibling = jQuery(this).parent().siblings("li.tab").find("a").attr('href');
	jQuery(sibling).find("is_required").prop("required", false).prop("disabled", true);

	jQuery(target).fadeIn(600);

});

jQuery(document).ready(function () {


	// Validation
	var $cardInput = jQuery('.input-fields input');

	jQuery('.next-btn').on('click', function (e) {

		$cardInput.removeClass('warning');

		$cardInput.each(function () {
			var $this = $(this);

			if (!$this.val()) {
				$this.addClass('warning');
			}
		});

	});

});

/**
 Vertigo Tip by www.vertigo-project.com
 Requires jQuery
 */

this.vtip = function () {
	this.xOffset = -10; // x distance from mouse
	this.yOffset = 10; // y distance from mouse

	jQuery(".vtip").unbind().hover(
		function (e) {
			this.t = this.title;
			this.title = '';
			this.top = (e.pageY + yOffset);
			this.left = (e.pageX + xOffset);

			jQuery('body').append('<p id="vtip">' + this.t + '</p>');
			jQuery('p#vtip #vtipArrow').attr("src", 'images/vtip_arrow.png');
			jQuery('p#vtip').css("top", this.top + "px").css("left", this.left + "px").fadeIn("slow");

		},
		function () {
			this.title = this.t;
			jQuery("p#vtip").fadeOut("slow").remove();
		}
	).mousemove(
		function (e) {
			this.top = (e.pageY + yOffset);
			this.left = (e.pageX + xOffset);

			jQuery("p#vtip").css("top", this.top + "px").css("left", this.left + "px");
		}
	);

};

jQuery(document).ready(function ($) {
	vtip();
})


jQuery('#button_register_user').on('click', function (e) {
	let $register_button = jQuery('#button_register_user');
	var user_login = jQuery("#user_login").val(),
		user_enroller_Id = jQuery("#user_enroller_Id").val(),
		new_user_email = jQuery("#new_user_email").val(),
		user_first_name = jQuery("#user_first_name").val(),
		user_last_name = jQuery("#user_last_name").val(),
		user_dob = jQuery("#user_dob").val(),
		user_country = jQuery("#user_country").val(),
		user_type = jQuery("#user_type").val(),
		user_lang = jQuery("#user_lang").val(),
		user_phone = jQuery("#user_phone").val(),
		user_government_id = jQuery("#user_government_id").val(),
		new_user_password = jQuery("#new_user_password").val(),
		user_confirm_pass = jQuery("#user_confirm_pass").val();

	jQuery(".error-user_check").text('');


	if (!(jQuery('#agree4').prop('checked') && jQuery('#agree5').prop('checked'))){
		e.stopPropagation();
		jQuery(".error-user_check").text('Please check all boxes if you want to proceed');
		return false;
	}

	if (new_user_password.length != user_confirm_pass.length) {
		e.stopPropagation();
		jQuery(".error-user_confirm_pass").text('Passwords do not match');
		return false;
	}

	if (!jQuery(".register-new").data("ok")) {

		e.stopPropagation();

		jQuery(".error-user").text('');

		jQuery.ajax({
			url: "/wp-admin/admin-ajax.php",
			type: "POST",
			dataType: "json",
			cache: false,
			data: {
				"action": "register_user_ajax",
				"user_login": user_login,
				"user_email": new_user_email,
				"user_enroller_Id": user_enroller_Id,
				"user_first_name": user_first_name,
				"user_last_name": user_last_name,
				"user_dob": user_dob,
				"user_country": user_country,
				"user_government_id": user_government_id,
				"user_type": user_type,
				"user_lang": user_lang,
				"user_phone": user_phone,
				"user_pass": new_user_password
			},
			beforeSend: function (results) {
				jQuery('.login-loading').show();
				jQuery('.load-spin').show();
				jQuery('.login-loading').html("<img style='display:block; margin:0 auto; width:60px; height:auto;margin-top:40%;' src='/wp-content/themes/masterstudy/images/loading.gif' />");
			},
			success: function (results) {
				console.log(results);
				if (results.success) {
					jQuery(".register-new").hide();
					$register_button.remove();
					jQuery('.error-ok').text("We have created an account for you!");
					jQuery('.tab-group').remove();
					jQuery('#place_order').click();
					jQuery('.login-loading').hide();
					jQuery('.load-spin').hide();
				} else {
					for (var i = 0; i < results.data.length; i++) {
						jQuery(".error-" + results.data[i].code).text(results.data[i].message);
					}
					return false;
				}
			},
			error: function (results) {
				console.log(results);
				alert('Error ajax');
				return false;
			},
			complete: function () {
				jQuery('.button-order-product').removeAttr('disabled');
				jQuery('.login-loading').hide();
				jQuery('.load-spin').hide();
			}
		});
		return false;
	}


});
jQuery(document).keypress(function (e) {
	if (e.which == 13) {
		jQuery('.button-order-product').click();
	}
});

jQuery('.tab1').on('click', function (e) {
	jQuery('.button-order-product').show();
	jQuery('#place_order').removeAttr('disabled');
	console.log('tab1');
	jQuery(document).keypress(function (e) {
		if (e.which == 13) {
			jQuery('.button-order-product').click();
		}
	});
});
jQuery('.tab2').on('click', function (e) {
	jQuery('.button-order-product').hide();
	jQuery('#place_order').attr('disabled', 'disabled');
	console.log('tab2');
	jQuery(document).keypress(function (e) {
		if (e.which == 13) {
			jQuery('#place_order').click();
		}
	});
});


jQuery(document).ready(function () {
	var data = {
		action: 'is_user_logged_in'
	};

	jQuery.post(ajaxurl, data, function (response) {
		if (response == 'yes') {
			jQuery('.button-order-product').remove();
			jQuery('#place_order').removeAttr('disabled');
		} else {

		}
	});
});


jQuery('.login_btn').on('click', function (e) {
	var login = jQuery("#user_login_name").val(),
		pass = jQuery("#user_pass").val();


	if (login.length < 3 && pass.length < 3) {
		jQuery(".for-login #user_login_name").addClass("req-input");
		jQuery(".for-login #user_pass").addClass("req-input");
		return false;
	} else {
		jQuery(".for-login #user_login_name").removeClass("req-input");
		jQuery(".for-login #user_pass").removeClass("req-input");
	}
	if (login.length < 3) {
		jQuery(".for-login #user_login_name").addClass("req-input");
		return false;
	} else {
		jQuery(".for-login #user_login_name").removeClass("req-input");
	}
	if (pass.length < 3) {
		jQuery(".for-login #user_pass").addClass("req-input");
		return false;
	} else {
		jQuery(".for-login #user_pass").removeClass("req-input");
	}

	console.log("login: " + login);
	console.log("Password: " + pass);
	jQuery.ajax({
		url: "/wp-admin/admin-ajax.php",
		type: "POST",
		data: {
			action: "login_user_ajax",
			user_login: login,
			user_pass: pass
		},
		dataType: "json",
		beforeSend: function (results) {
			jQuery('.login-loading').show();
			jQuery('.login-loading').html("<img style='display:block; margin:0 auto; width:30px; height:auto;' src='/wp-content/themes/masterstudy/images/loading.gif' />");
			jQuery('.login-error-text').hide();
		},
		success: function (results) {
			console.log(results);
			if (results.success) {
				jQuery('.tab-group').remove();
				jQuery('.button-order-product').remove();
				jQuery('#loginbag').html('<h2>Hello, ' + results.data.user_display_name + '</h2>');
				jQuery('#place_order').removeAttr('disabled');
				jQuery('.login-loading').hide();
			} else {
				jQuery('.login-error-text').show();
				jQuery('.login-error-text').text(results.data.message);
				jQuery('.login-loading').hide();
			}

		},
		error: function (results) {
		}
	});
});

jQuery('#place_order').on('click', function (e) {
	jQuery('html, body').animate({scrollTop: 0}, 850);

	jQuery('.woocommerce-message').hide();
});
jQuery('.button-order-product').on('click', function (e) {
	jQuery('html, body').animate({scrollTop: 0}, 850);
	jQuery('.woocommerce-message').hide();
});

jQuery('.buttonsss').on('change', function () {
	var data = {
		type: this.value,
		action: "woocommerce_ajax_add_to_cart"
	};

	jQuery.ajax({
		type: "post",
		url: "/wp-admin/admin-ajax.php",
		data: data,
		// beforeSend: function (response) {
		// $thisbutton.removeClass('added').addClass('loading');
		// },
		// complete: function (response) {
		// $thisbutton.addClass('added').removeClass('loading');
		// },
		beforeSend: function (response) {
			jQuery('.spinner-plus .login-loading-product').show();
			jQuery('.spinner-plus .login-loading-product').css("z-index", "1");
			console.log('start spinner');
		},
		success: function (response) {
			jQuery("div.checkout-total-info").replaceWith(response.fragments['div.checkout-total-info'])
			jQuery("div.cart-contents").replaceWith(response.fragments['div.cart-contents'])
			console.log(response);
		},
		complete: function (response) {
			jQuery('.spinner-plus .login-loading-product').hide();
			jQuery('.spinner-plus .login-loading-product').css("z-index", "-1");
			console.log('stop spinner');
		},
	});
});

jQuery(document).on('click', '.delete-product-from-order', function (e) {
	e.stopPropagation();
	e.preventDefault();
	let $this = jQuery(this);
	let link = $this.data('href');

	jQuery.ajax({
		url: link,
		beforeSend: function (response) {
			jQuery('.spinner-plus .login-loading-product').show();
			jQuery('.spinner-plus .login-loading-product').css("z-index", "1");
			console.log('start spinner');
			$this.parents('.product-review').hide();
		},
		success: function (response) {
			jQuery('.buttonsss').change();
		},
		error: function (response) {
			jQuery('.spinner-plus .login-loading-product').hide();
			jQuery('.spinner-plus .login-loading-product').css("z-index", "-1");
			console.log('stop spinner');
			$this.parents('.product-review').show();
		},
	});
});



