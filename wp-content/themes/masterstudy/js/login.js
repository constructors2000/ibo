jQuery("#forgotpassword").on("click", function(e) {
  e.preventDefault();
  $(this).fadeOut(200);
  $(this).siblings(".formlogin").toggle(200);
})


// jQuery(document).on("ready", function(){
	// jQuery("[name=\"payment_method\"]").each(function(e){
		// if(jQuery(this).attr("checked")) {
			// jQuery("li.wc_payment_method").removeClass("border-payment");
			// jQuery(this).parent().addClass("border-payment");
		// }
		// else jQuery(this).parent().removeClass("border-payment");
	// }); 
// });


// jQuery("[name=\"payment_method\"]").change(function(e){
	
	// if(jQuery(this).attr("checked")) {
		// jQuery("li.wc_payment_method").removeClass("border-payment");
		// jQuery(this).parent().addClass("border-payment");
	// }
	// else jQuery(this).parent().removeClass("border-payment");
// }); 

jQuery('.form').find('input, textarea').on('keyup blur focus', function (e) {
  
  var $this = jQuery(this),
      label = $this.prev('label');

	  if (e.type === 'keyup') {
			if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
    	if( $this.val() === '' ) {
    		label.removeClass('active highlight'); 
			} else {
		    label.removeClass('highlight');   
			}   
    } else if (e.type === 'focus') {
      
      if( $this.val() === '' ) {
    		label.removeClass('highlight'); 
			} 
      else if( $this.val() !== '' ) {
		    label.addClass('highlight');
			}
    }

});

jQuery("input[required]").addClass("is_required");

jQuery('.tab a').on('click', function (e) {
  
  e.preventDefault();
  
  jQuery(this).parent().addClass('active');
  jQuery(this).parent().siblings().removeClass('active');
  
  target = jQuery(this).attr('href');

  jQuery('.tab-content > div').not(target).hide();

  jQuery(target).find("is_required").prop("required", true);
  sibling = jQuery(this).parent().siblings("li.tab").find("a").attr('href');
  jQuery(sibling).find("is_required").prop("required", false).prop("disabled", true);
  
  jQuery(target).fadeIn(600);
  
});

jQuery(document).ready(function() {


  // Validation
  var $cardInput = jQuery('.input-fields input');

  jQuery('.next-btn').on('click', function(e) {

    $cardInput.removeClass('warning');

    $cardInput.each(function() {
       var $this = $(this);

       if (!$this.val()) {
         $this.addClass('warning');
       }
    });

  });

});

/**
Vertigo Tip by www.vertigo-project.com
Requires jQuery
*/

this.vtip = function() {    
    this.xOffset = -10; // x distance from mouse
    this.yOffset = 10; // y distance from mouse       
    
    jQuery(".vtip").unbind().hover(    
        function(e) {
            this.t = this.title;
            this.title = ''; 
            this.top = (e.pageY + yOffset); this.left = (e.pageX + xOffset);
            
            jQuery('body').append( '<p id="vtip">' + this.t + '</p>' );            
            jQuery('p#vtip #vtipArrow').attr("src", 'images/vtip_arrow.png');
            jQuery('p#vtip').css("top", this.top+"px").css("left", this.left+"px").fadeIn("slow");
            
        },
        function() {
            this.title = this.t;
            jQuery("p#vtip").fadeOut("slow").remove();
        }
    ).mousemove(
        function(e) {
            this.top = (e.pageY + yOffset);
            this.left = (e.pageX + xOffset);
                         
            jQuery("p#vtip").css("top", this.top+"px").css("left", this.left+"px");
        }
    );            
    
};

jQuery(document).ready(function($){vtip();}) 




jQuery(".sign_login").click(function() {    

                var login = jQuery('.form1 #username-login').val();
                var pass = jQuery('.form1 #pass-login').val();
				
				if(login.length < 3 && pass.length < 3){
				jQuery(".form1 #username-login").addClass( "req-input" );	
				jQuery(".form1 #pass-login").addClass( "req-input" );	
				return false;
				}
				else {
					jQuery(".form1 #username-login").removeClass( "req-input" );
					jQuery(".form1 #pass-login").removeClass( "req-input" );
				}
				if(login.length < 3){
					jQuery(".form1 #username-login").addClass( "req-input" );	
					return false;
				}
				else {
					jQuery(".form1 #username-login").removeClass( "req-input" );
				}
				if(pass.length < 3){
					jQuery(".form1 #pass-login").addClass( "req-input" );	
					return false;
				}
				else {
					jQuery(".form1 #pass-login").removeClass( "req-input" );
				}

                var data = {
                    'log':  login,
                    'pwd':  pass,
					'wp-submit': 'Log In'
                }

                $.ajax({
                    url: '/wp-login.php',
                    data: data,
                    type: 'POST',
					beforeSend : function(result) {
						jQuery('.pload').show();
						jQuery(".errorka").text('');
				    },
                    success: function(result) {
						if (result.search(new RegExp("The username or password you entered is incorrect","i")) !== (-1)) {					
							jQuery(".errorka").text('The username or password you entered is incorrect');
						}
						else {
							window.location.href = "/";
						}
                    },
				    complete: function(results){
						jQuery('.pload').hide();
				    },
                }); 

});

jQuery(".restore_login").click(function() {    

                var login = jQuery('.form2 #name-restore').val();
				
				console.log(login);
				
                var data = {
                    'somfrp_user_info':  login,
					'wc_reset_password': 'true'
                }

                $.ajax({
                    url: '/restore/',
                    data: data,
                    type: 'POST',
					beforeSend : function(result) {
						jQuery('.pload').show();
						jQuery(".errorka").text('');
				    },
                    success: function(result) {
						if (result.search(new RegExp("Enter a username or email address","i")) !== (-1)) {					
							jQuery(".errorka").text('Enter a username or email address');
						}
						else {
							console.log('OK');
						}
                    },
				    complete: function(results){
						jQuery('.pload').hide();
				    },
                }); 

});

    if(event.keyCode == 13){
        $("#enter_but").click();
    }