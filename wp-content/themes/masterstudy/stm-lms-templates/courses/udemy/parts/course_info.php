<?php if (!defined('ABSPATH')) exit; //Exit if accessed directly ?>

<?php
/**
 * @var $post_id
 * @var $post_status
 * @var $rates
 * @var $average
 * @var $total
 * @var $percent
 * @var $sale_price
 * @var $price
 * @var $author_id
 */
 

$stock = get_post_meta(get_the_id(), '_stock', true);
$duration = get_post_meta(get_the_id(), 'duration', true);
$level = get_post_meta(get_the_id(), 'level', true);
$subscription = get_post_meta(get_the_id(), 'subscription', true);

?>

<div class="stm_lms_courses__single--info">

        <div class="stm_lms_courses__single--info_author">
			<img class="img-responsive logo_transparent_static visible" src="/wp-content/uploads/2019/02/logo.svg" style="width: 66px;" alt="melius node courses">
        </div>

    <div class="stm_lms_courses__single--info_title">
        <a href="<?php the_permalink(); ?>">
            <h4><?php the_title(); ?></h4>
        </a>
    </div>

	<!--<?php if(!empty($duration) or !empty($level) or !empty($subscription)): ?>
	<div class="stm_lms_courses__single--info_meta">
		<?php if(!empty($subscription)): ?>
			<div class="level"><span><i class="fas fa-graduation-cap"></i> <?php echo esc_attr(__('Level:', 'masterstudy')); ?></span><?php echo esc_attr(__($level)); ?></div>
		<?php endif; ?>
		<?php if(!empty($subscription)): ?>
			<div class="level"><span><i class="far fa-clock"></i> <?php echo esc_attr(__('Duration:', 'masterstudy')); ?></span><?php echo esc_attr(__($duration)); ?></div>
		<?php endif; ?>
		<?php if(!empty($subscription)): ?>
			<div class="level"><span><i class="fas fa-calendar-alt"></i> <?php echo esc_attr(__('Subscription:', 'masterstudy')); ?></span><?php echo esc_attr(__($subscription)); ?></div>
		<?php endif; ?>
    </div>
	<?php endif; ?>-->

    <div class="stm_lms_courses__single--info_excerpt">
		<?php
		echo stm_lms_minimize_word(wp_kses_post(strip_shortcodes(get_the_excerpt())), 150, '...');
		?>
    </div>
	
	<?php $gain_access = get_post_meta(get_the_id(), 'gain_access', true); ?>
	<?php if(!empty($gain_access)): ?>
		<div class="stm_lms_courses__single--info_access">
			<h4>Gain Access to:</h4>
			<ul>
			<?php
				$access_to = explode( PHP_EOL, $gain_access );
				foreach($access_to as $access){
					echo '<li>'.$access.'</li>';
				}
			?>
			</ul>
		</div>
	<?php endif; ?>

    <div class="stm_lms_courses__single--info_bottom">
	
		<?php
			global $product;
			if($product->is_type( 'simple' )) {
				echo '<a href="' . $product->get_permalink() . '" class="button product_type_simple add_to_cart_button ajax_add_to_cart">View Product</a>';
			}
			else {
				woocommerce_template_loop_add_to_cart();
			}
		?>
		
		<?php //woocommerce_template_single_add_to_cart();?>

		<?php STM_LMS_Templates::show_lms_template('global/wish-list', array('course_id' => $post_id)); ?>
    </div>

</div>