<?php if ( ! defined( 'ABSPATH' ) ) exit; //Exit if accessed directly ?>


<?php
	$tabs = array(
		'description' => esc_html__('Description', 'masterstudy-lms-learning-management-system-pro'),
		'curriculum' => esc_html__('Curriculum', 'masterstudy-lms-learning-management-system-pro'),
		'faq' => esc_html__('FAQ', 'masterstudy-lms-learning-management-system-pro'),
		'reviews' => esc_html__('Reviews', 'masterstudy-lms-learning-management-system-pro'),
	);

	$active = 'description';
?>

<div class="tab-content">
	<h3>Description</h3>
	<div class="stm_lms_course__content">
		<?php the_content(); ?>
	</div>
</div>