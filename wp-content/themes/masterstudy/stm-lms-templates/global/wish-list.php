<?php
/**
 * @var $course_id
 *
 */


stm_lms_register_style('wishlist');
stm_lms_register_script('wishlist');
if (!is_user_logged_in()) wp_enqueue_script('jquery.cookie');

global $product;

$course_id = $product->get_id();

$wishlisted = STM_LMS_User::is_wishlisted($course_id); ?>

<div class="stm-lms-wishlist"
     data-add="<?php esc_html_e('Add to Wishlist', 'masterstudy-lms-learning-management-system'); ?>"
     data-add-icon="far fa-heart"
     data-remove="<?php esc_html_e('Wishlisted', 'masterstudy-lms-learning-management-system'); ?>"
     data-remove-icon="fa fa-heart"
     data-id="<?php echo intval($course_id); ?>">
	<?php if ($wishlisted): ?>
        <i class="fa fa-heart"></i>
	<?php else: ?>
        <i class="far fa-heart"></i>
	<?php endif; ?>
</div>
