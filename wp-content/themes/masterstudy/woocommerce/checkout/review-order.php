<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.3.0
 */

if (!defined('ABSPATH')) {
    exit;
}
?>
<div class="col-md-12 product-item">
    <div class="row">

    </div>

    <?php
    do_action('woocommerce_review_order_before_cart_contents');

    // This field for check if remote button is work
    // for customer/guest ist true
    // for ibo false
    $type = empty($_POST['type']) ?: stripos(stripcslashes($_POST['type']), 'ibo') === false;


    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
        $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
        $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);
        if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key)) {
            $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);

            ?>

            <div class="product-review">
                <div class="col-md-4 ">
                    <a class="count-prod"><?php echo $i += 1; ?></a>
                    <?php
                    $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);

                    if (!$product_permalink) {
                        echo stm_echo_safe_output($thumbnail); // PHPCS: XSS ok.
                    } else {
                        printf('<a href="%s" class="shop_table_small_thumb">%s</a>', $_product->get_permalink($cart_item), $thumbnail);
                    }
                    ?>
                </div>
                <div class="col-md-8">

                    <?php
                    $product_id = $_product->get_parent_id();
                    $product = wc_get_product($product_id);
                    ?>

                    <?php echo "<h4>" . $_product->get_title() . "</h4>"; ?>


                </div>

                <div class="col-md-12 price-count-product">
                    <div class="row h-100 align-items-center">
                        <div class="col-md-4 col-4">
                            <h5 align="left"><?php echo ($_product->get_attributes())["subscription"]; ?></h5>
                        </div>
                        <div class="col-md-4 col-4">
                            <h5 align="center">

                                <?php echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key); ?>
                            </h5>
                        </div>
                        <div class="col-md-4 col-4">
                            <?php if (!(defined('DOING_AJAX') && DOING_AJAX && stripos($_product->get_title(), "MELiUS IBO") !== false && !$type)): ?>
                                <a class="delete-product-from-order" href="#"
                                   data-href="<?php echo WC()->cart->get_remove_url($cart_item_key); ?>"><img
                                            class="delete-img"
                                            src="<?php echo get_template_directory_uri(); ?>/assets/images/delete.png"/></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

            </div>

            <?php
        }
    }

    do_action('woocommerce_review_order_after_cart_contents');
    ?>


    <div class="col-md-12 price-count-products">
        <div class="row h-100 align-items-center">
            <div class="col-md-12">
                <h5 align="center">

                    <?php foreach (WC()->cart->get_coupons() as $code => $coupon) : ?>
                        <tr class="cart-discount coupon-<?php echo esc_attr(sanitize_title($code)); ?>">
                            <th><?php wc_cart_totals_coupon_label($coupon); ?></th>
                            <td><?php wc_cart_totals_coupon_html($coupon); ?></td>
                        </tr>
                    <?php endforeach; ?>

                    <?php if (WC()->cart->needs_shipping() && WC()->cart->show_shipping()) : ?>

                        <?php do_action('woocommerce_review_order_before_shipping'); ?>

                        <?php wc_cart_totals_shipping_html(); ?>

                        <?php do_action('woocommerce_review_order_after_shipping'); ?>

                    <?php endif; ?>

                    <?php foreach (WC()->cart->get_fees() as $fee) : ?>
                        <tr class="fee">
                            <th><?php echo esc_html($fee->name); ?></th>
                            <td><?php wc_cart_totals_fee_html($fee); ?></td>
                        </tr>
                    <?php endforeach; ?>



                    <?php do_action('woocommerce_review_order_before_order_total'); ?>

                    <?php do_action('woocommerce_review_order_after_order_total'); ?>

                </h5>
            </div>
        </div>
    </div>
</div>