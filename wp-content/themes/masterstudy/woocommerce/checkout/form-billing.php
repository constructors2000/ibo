<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/** @global WC_Checkout $checkout */

?> <?php if (is_user_logged_in()) {
    $displaynone = 'display:none;';
} else {
    $displaynone = '';
} ?>
<ul style="<?php echo $displaynone; ?>" class="tab-group">
    <li class="tab active"><a class="tab1" href="#signup">I am new user</a></li>
    <li class="tab"><a class="tab2" href="#login">I am existing user</a></li>
</ul>

<div style="<?php echo $displaynone; ?>" class="tab-content">
    <div id="signup">

        <div class="checkout-panel">
            <div class="panel-body">


                <div class="input-fields">

                    <?php if (wc_ship_to_billing_address_only() && WC()->cart->needs_shipping()) : ?>


                    <?php else : ?>


                    <?php endif; ?>



                    <?php if (!is_user_logged_in() && $checkout->is_registration_enabled()) : ?>
                        <div class="woocommerce-account-fields">
                            <?php if (!$checkout->is_registration_required()) : ?>

                                <p class="form-row form-row-wide create-account">
                                    <input class="input-checkbox"
                                           id="createaccount" <?php checked((true === $checkout->get_value('createaccount') || (true === apply_filters('woocommerce_create_account_default_checked', false))), true) ?>
                                           type="checkbox" name="createaccount" value="1"/>
                                    <label for="createaccount"
                                           class="checkbox"><?php esc_html_e('Create an account?', 'masterstudy'); ?></label>
                                </p>

                            <?php endif; ?>

                            <?php do_action('woocommerce_before_checkout_registration_form', $checkout); ?>

                            <?php if ($checkout->get_checkout_fields('account')) : ?>

                                <div class="create-account">
                                    <p class="mg-bt-10"><?php esc_html_e('Create an account by entering the information below. If you are a returning customer please login at the top of the page.', 'masterstudy'); ?></p>
                                    <?php foreach ($checkout->get_checkout_fields('account') as $key => $field) : ?>
                                        <?php woocommerce_form_field($key, $field, $checkout->get_value($key)); ?>
                                    <?php endforeach; ?>
                                    <div class="clear"></div>
                                </div>

                            <?php endif; ?>

                            <?php do_action('woocommerce_after_checkout_registration_form', $checkout); ?>
                        </div>
                    <?php endif; ?>




                    <div class="register-new" data-ok="0" data-click="1">
					<div style="display:none;" class="row load-spin">
						<div class="login-loading col-md-12"></div>
					</div>
					<div style="color:red;" class="error-user error-api"></div>
                        <div class="errortable">
                            <h4 style="color:red;"></h4>
                        </div>
						
                        <div class="row">
                            <div class="column-1 col-md-6">
                                <label for="id">Enroller ID</label>
                                <div style="color:red;" class="error-user error-user_enroller_Id"></div>
                                <input type="text" id="user_enroller_Id" placeholder="Enter Enroller ID" value="<?php echo WC()->session->get('EnrollerId') ?: ''; ?>"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column-1 col-md-6">
                                <label for="firstname">First name</label>
                                <div style="color:red;" class="error-user error-user_first_name"></div>
                                <input type="text" id="user_first_name" placeholder="Enter First name"/>
                            </div>
                            <div class="column-1 col-md-6">
                                <label for="lastname">Last name</label>
                                <div style="color:red;" class="error-user error-user_last_name"></div>
                                <input type="text" id="user_last_name" placeholder="Enter Last name"/>
                            </div>

                        </div>
                        <div class="row">
                            <div class="column-1 col-md-6">
                                <label for="SSN">SSN No./Government ID <a
                                            title="Melius requires that your social <br> security number be provided for <br> tax purposes"
                                            class="podska vtip">?</a></label>
                                <div style="color:red;" class="error-user error-user_government_id"></div>
                                <input type="text" maxlength="14" id="user_government_id"
                                       placeholder="Enter SSN No./Government ID"/>
                            </div>
                            <div class="column-1 col-md-6">
                                <label for="DOB">DOB</label>
                                <div style="color:red;" class="error-user error-user_dob"></div>
                                <input type="date" name="bday" max="3000-12-31" min="1000-01-01" id="user_dob"
                                       placeholder="Enter Last name"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column-1 col-md-6">
                                <label for="Customer">IBo/Customer</label>
                                <div style="color:red;" class="error-user error-user_type"></div>
                                <select id="user_type" class="selectt buttonsss">
                                    <option value="customer" selected>Customer</option>
                                    <option value="ibo">IBo</option>
                                </select>
                            </div>

                            <div class="column-1 col-md-6">
                                <label for="country">Country</label>
                                <select id="user_country" class="selectt">
                                    <option selected="selected">United States</option>
                                    <option>Aland Islands</option>
                                    <option>Albania</option>
                                    <option>Algeria</option>
                                    <option>American Samoa</option>
                                    <option>Andorra</option>
                                    <option>Angola</option>
                                    <option>Anguilla</option>
                                    <option>Antigua and Barbuda</option>
                                    <option>Argentina</option>
                                    <option>Armenia</option>
                                    <option>Aruba</option>
                                    <option>Australia</option>
                                    <option>Austria</option>
                                    <option>Azerbaijan</option>
                                    <option>Bahamas</option>
                                    <option>Bahrain</option>
                                    <option>Bangladesh</option>
                                    <option>Barbados</option>
                                    <option>Belarus</option>
                                    <option>Belgium</option>
                                    <option>Belize</option>
                                    <option>Benin</option>
                                    <option>Bermuda</option>
                                    <option>Bhutan</option>
                                    <option>Bolivia</option>
                                    <option>Bosnia and Herzegovina</option>
                                    <option>Botswana</option>
                                    <option>Brazil</option>
                                    <option>British Virgin Islands</option>
                                    <option>Brunei Darussalam</option>
                                    <option>Bulgaria</option>
                                    <option>Burkina Faso</option>
                                    <option>Burundi</option>
                                    <option>Cambodia</option>
                                    <option>Cameroon</option>
                                    <option>Canada</option>
                                    <option>Cape Verde</option>
                                    <option>Cayman Islands</option>
                                    <option>Chad</option>
                                    <option>Chile</option>
                                    <option>China</option>
                                    <option>Colombia</option>
                                    <option>Comoros</option>
                                    <option>Congo (Brazzaville)</option>
                                    <option>Congo, (Kinshasa)</option>
                                    <option>Costa Rica</option>
                                    <option>Côte d'Ivoire</option>
                                    <option>Croatia</option>
                                    <option>Cuba</option>
                                    <option>Cyprus</option>
                                    <option>Czech Republic</option>
                                    <option>Denmark</option>
                                    <option>Djibouti</option>
                                    <option>Dominica</option>
                                    <option>Dominican Republic</option>
                                    <option>Ecuador</option>
                                    <option>Egypt</option>
                                    <option>El Salvador</option>
                                    <option>Equatorial Guinea</option>
                                    <option>Eritrea</option>
                                    <option>Estonia</option>
                                    <option>Ethiopia</option>
                                    <option>Faroe Islands</option>
                                    <option>Fiji</option>
                                    <option>Finland</option>
                                    <option>France</option>
                                    <option>French Polynesia</option>
                                    <option>Gabon</option>
                                    <option>Gambia</option>
                                    <option>Georgia</option>
                                    <option>Germany</option>
                                    <option>Ghana</option>
                                    <option>Gibraltar</option>
                                    <option>Greece</option>
                                    <option>Greenland</option>
                                    <option>Grenada</option>
                                    <option>Guadeloupe</option>
                                    <option>Guam</option>
                                    <option>Guatemala</option>
                                    <option>Guernsey</option>
                                    <option>Guinea</option>
                                    <option>Guinea-Bissau</option>
                                    <option>Guyana</option>
                                    <option>Haiti</option>
                                    <option>Honduras</option>
                                    <option>Hong Kong</option>
                                    <option>Hungary</option>
                                    <option>Iceland</option>
                                    <option>India</option>
                                    <option>Indonesia</option>
                                    <option>Iran</option>
                                    <option>Iraq</option>
                                    <option>Ireland</option>
                                    <option>Italy</option>
                                    <option>Jamaica</option>
                                    <option>Japan</option>
                                    <option>Jordan</option>
                                    <option>Kazakhstan</option>
                                    <option>Kenya</option>
                                    <option>Kiribati</option>
                                    <option>Kuwait</option>
                                    <option>Kyrgyzstan</option>
                                    <option>Lao</option>
                                    <option>Lebanon</option>
                                    <option>Lesotho</option>
                                    <option>Liechtenstein</option>
                                    <option>Lithuania</option>
                                    <option>Luxembourg</option>
                                    <option>Macao</option>
                                    <option>Macedonia</option>
                                    <option>Madagascar</option>
                                    <option>Malawi</option>
                                    <option>Malaysia</option>
                                    <option>Maldives</option>
                                    <option>Mali</option>
                                    <option>Malta</option>
                                    <option>Martinique</option>
                                    <option>Mauritania</option>
                                    <option>Mauritius</option>
                                    <option>Mexico</option>
                                    <option>Moldova</option>
                                    <option>Monaco</option>
                                    <option>Mongolia</option>
                                    <option>Montenegro</option>
                                    <option>Morocco</option>
                                    <option>Mozambique</option>
                                    <option>Myanmar</option>
                                    <option>Namibia</option>
                                    <option>Nauru</option>
                                    <option>Nepal</option>
                                    <option>Netherlands</option>
                                    <option>New Caledonia</option>
                                    <option>New Zealand</option>
                                    <option>Nicaragua</option>
                                    <option>Niger</option>
                                    <option>Nigeria</option>
                                    <option>Norway</option>
                                    <option>Oman</option>
                                    <option>Pakistan</option>
                                    <option>Palau</option>
                                    <option>Panama</option>
                                    <option>Papua New Guinea</option>
                                    <option>Paraguay</option>
                                    <option>Peru</option>
                                    <option>Philippines</option>
                                    <option>Pitcairn</option>
                                    <option>Poland</option>
                                    <option>Portugal</option>
                                    <option>Puerto Rico</option>
                                    <option>Qatar</option>
                                    <option>Rest of the World</option>
                                    <option>Reunion</option>
                                    <option>Romania</option>
                                    <option>Rwanda</option>
                                    <option>Saudi Arabia</option>
                                    <option>Senegal</option>
                                    <option>Serbia</option>
                                    <option>Seychelles</option>
                                    <option>Sierra Leone</option>
                                    <option>Singapore</option>
                                    <option>Slovakia</option>
                                    <option>Slovenia</option>
                                    <option>Somalia</option>
                                    <option>South Africa</option>
                                    <option>South Korea</option>
                                    <option>Spain</option>
                                    <option>Sri Lanka</option>
                                    <option>Sudan</option>
                                    <option>Suriname</option>
                                    <option>Sweden</option>
                                    <option>Switzerland</option>
                                    <option>Taiwan</option>
                                    <option>Tajikistan</option>
                                    <option>Tanzania</option>
                                    <option>Thailand</option>
                                    <option>Togo</option>
                                    <option>Tonga</option>
                                    <option>Trinidad and Tobago</option>
                                    <option>Tunisia</option>
                                    <option>Turkey</option>
                                    <option>Turkmenistan</option>
                                    <option>Uganda</option>
                                    <option>Ukraine</option>
                                    <option>United Kingdom</option>
                                    <option>Uruguay</option>
                                    <option>Uzbekistan</option>
                                    <option>Venezuela</option>
                                    <option>Vietnam</option>
                                    <option>Virgin Islands, US</option>
                                    <option>Zambia</option>
                                    <option>Zimbabwe</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column-1 col-md-6">
                                <label for="country">Language</label>
                                <div style="color:red;" class="error-user error-user_lang"></div>
                                <select id="user_lang" class="selectt">
                                    <option value="es">Spanish</option>
                                    <option value="en" selected="selected">English (United Kingdom)</option>
                                </select>
                            </div>
                            <div class="column-1 col-md-6">
                                <label for="email">Email</label>
                                <div style="color:red;" class="error-user error-user_email"></div>
                                <input type="email" id="new_user_email" placeholder="Enter Email"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column-1 col-md-6">
                                <label for="Phone">Phone number</label>
                                <div style="color:red;" class="error-user error-user_phone"></div>
                                <input type="text" id="user_phone" placeholder="Enter Phone"/>
                            </div>
                            <div class="column-1 col-md-6">
                                <label for="Username">Username</label>
                                <div style="color:red;" class="error-user error-user_login"></div>
                                <input type="Username" id="user_login" placeholder="Enter Username"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column-1 col-md-6">
                                <label for="password">Password</label>
                                <div style="color:red;" class="error-user error-user_pass"></div>
                                <div class="error-password"></div>
                                <input type="password" id="new_user_password" placeholder="Enter Password"/>
                            </div>
                            <div class="column-1 col-md-6">
                                <label for="password1">Confirm Password</label>
                                <div style="color:red;" class="error-user error-user_confirm_pass"></div>
                                <input type="password" id="user_confirm_pass" placeholder="Confirm Password"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column-1 col-md-12"><br><br>
                                <div style="color:red;" class="error-user_check"></div>
                            </div>
                            <div class="column-1 col-md-12 pravila1"><br>

                                <input type="checkbox" value="" name="checkbox" id="agree4"/> I have read, understood and
                                agree to <a target="_blank"
                                            href="https://drive.google.com/file/d/1YLsbyYCT3hvVf-yAxc4p1R4hKRwyq8mh/view">Terms
                                    of Use</a> and <a target="_blank" href="#">Policies & Procedures</a><br> <br>
                                <input type="checkbox" value="" name="checkbox" id="agree5"/> I have read, understood and
                                agree to <a target="_blank"  href="https://drive.google.com/open?id=1UYULO6gvZbg_SotJNU-8bXJ2ZP_SOGok">Return/Refund/Cancellation Policy</a> and <a target="_blank"
                                                                                                                                                                       href="https://drive.google.com/open?id=1vGLcZ6YXFjt2mUtUp9kr8H-0mw6NlLWy">MELiUS General Privacy Policy</a><br><br>

                                <input type="checkbox" value="" name="checkbox" id="agree6"/> I have read, understood and
                                agree to <a target="_blank"
                                            href="https://drive.google.com/open?id=1vGLcZ6YXFjt2mUtUp9kr8H-0mw6NlLWy">GDRP
                                    Policy</a> <br><br>
                            </div>

                            <div class="column-1 col-md-12 pravila2"><br>

                                <input type="checkbox" value="" name="checkbox" id="agree1"/> I have read, understood and
                                agree to <a target="_blank"
                                            href="https://drive.google.com/file/d/1YLsbyYCT3hvVf-yAxc4p1R4hKRwyq8mh/view">Terms
                                    of Use</a> and <a target="_blank" href="#">Policies & Procedures</a><br> <br>
                                <input type="checkbox" value="" name="checkbox" id="agree2"/> I have read, understood and
                                agree to <a target="_blank"  href="https://drive.google.com/open?id=1UYULO6gvZbg_SotJNU-8bXJ2ZP_SOGok">Return/Refund/Cancellation Policy</a> and <a target="_blank"  href="https://drive.google.com/open?id=1AVsEaOms5x-3rgoN4rZEPpshxbEq6gt5">IBO Agreement</a><br><br>

                                <input type="checkbox" value="" name="checkbox" id="agree3"/> I have read, understood and
                                agree to <a target="_blank"
                                            href="https://drive.google.com/open?id=1vGLcZ6YXFjt2mUtUp9kr8H-0mw6NlLWy">GDRP
                                    Policy</a> and <a target="_blank"
                                                      href="https://drive.google.com/open?id=1vGLcZ6YXFjt2mUtUp9kr8H-0mw6NlLWy">MELiUS General Privacy Policy</a><br><br>
                            </div>
                        </div>

                    </div>
                    <div><br><br>
                        <h2 class="error-ok" style="color:#009500 !important;"></h2></div>
                </div>
            </div>


            <h2>Billing address:</h2>


            <div class="input-fields">

            </div>
        </div>
    </div>
    <div id="login">

        <div class="checkout-panel">
            <div class="panel-body">

                <div id="loginbag" class="input-fields">


                    <?php if (is_user_logged_in()) { ?>
                        <h2>Hello, <?php echo wp_get_current_user()->user_login; ?></h2>
                        <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Logout">Logout</a>
                    <?php } else { ?>
                        <div class="row for-login" data-ok="0" data-click="1">
                            <div class="login-loading col-md-12"></div>
                            <div class="col-md-12">
                                <h4 align="center" style="color:red !important;" class="login-error-text"></h4>
                            </div>
                            <div class="column-1 col-md-6">
                                <label for="Username">Username</label>

                                <input type="text" name="login" id="user_login_name" placeholder="Enter Username"/>
                            </div>
                            <div class="column-1 col-md-6">
                                <label for="password">Password</label>
                                <input type="password" name="password" id="user_pass" placeholder="Enter Password"/>
                            </div>
                            <button class="request btn float-left login_btn login_btn2 col-md-4" type="button">LOGIN
                            </button>
                            <div class="message"></div>
                        </div>
                    <?php } ?>


                </div>
            </div>


            <h2>Billing address:</h2>


            <div class="input-fields">
            </div>
        </div>

    </div>

</div><!-- tab-content -->


<div class="input-fields">
    <div class="woocommerce-billing-fields">


        <?php do_action('woocommerce_before_checkout_billing_form', $checkout); ?>

        <div class="woocommerce-billing-fields__field-wrapper">
            <?php
            $fields = $checkout->get_checkout_fields('billing');

            foreach ($fields as $key => $field) {
                if (isset($field['country_field'], $fields[$field['country_field']])) {
                    $field['country'] = $checkout->get_value($field['country_field']);
                }
                woocommerce_form_field($key, $field, $checkout->get_value($key));
            }
            ?>
        </div>

        <?php do_action('woocommerce_after_checkout_billing_form', $checkout); ?>
    </div>
</div>
