<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if (!defined('ABSPATH')) {
    exit;
}


// If checkout registration is disabled and not logged in, the user cannot checkout.
if (!$checkout->is_registration_enabled() && $checkout->is_registration_required() && !is_user_logged_in()) {
    echo esc_html(apply_filters('woocommerce_checkout_must_be_logged_in_message', __('You must be logged in to checkout.', 'masterstudy')));
    return;
}

?>


<form name="checkout" method="post" class="checkout woocommerce-checkout"
      action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">

    <div id="personal-info" class="col-md-8 personal-info">

        <?php if ($checkout->get_checkout_fields()) : ?>

            <div id="customer_details" class="form">
                <h2>Personal Information</h2>
                <?php do_action('woocommerce_checkout_billing'); ?>
                <?php do_action('woocommerce_checkout_shipping'); ?>
            </div>

            <?php do_action('woocommerce_checkout_after_customer_details'); ?>

        <?php endif; ?>

    </div>


    <div class="col-md-4">
        <div class="col-md-12 checkout-total info-info spinner-plus">
		<div class="login-loading-product"><img class='pload' style='display:block;margin:0 auto; width:50px; height:auto;' src='<?php echo get_template_directory_uri();?>/images/loading.gif' /></div> 
            <div class="checkout-total-info">
                <h2>Product Information</h2>
                <?php do_action('woocommerce_checkout_before_order_review'); ?>

                <div id="order_review" class="woocommerce-checkout-review-order">
                    <?php do_action('woocommerce_checkout_order_review'); ?>
                </div>

                <div class="col-md-12 total-sum">
                    <div class="row  h-100 align-items-center">
                        <div class="col-md-6 col-6">
                            <a href="<?php echo wc_get_cart_url(); ?>">Edit Order</a>
                        </div>
                        <div class="col-md-6 col-6">
                            <?php global $woocommerce;

                            $discount_total = 0;

                            foreach ($woocommerce->cart->get_cart() as $cart_item_key => $values) {

                                $_product = $values['data'];

                                if ($_product->is_on_sale()) {
                                    $regular_price = $_product->get_regular_price();
                                    $sale_price = $_product->get_sale_price();
                                    $regular_total += $regular_price * $values['quantity'];
                                    $discount = ($regular_price - $sale_price) * $values['quantity'];
                                    $discount_total += $discount;
                                }

                            }

                            $discount_total = ($discount_total / $regular_total) * 100; ?>
                            <h5><?php esc_html_e('Total: ', 'masterstudy'); ?><?php wc_cart_totals_order_total_html();
                                if ($discount_total > 0) { ?> <span
                                        class="cart-discount"><?php echo wc_price($regular_total); ?></span> <span
                                        class="discount-percent"><?php echo round($discount_total) . '%off'; ?></span><?php } ?>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 checkout-total info-info">

            <?php global $woocommerce;

            $discount_total = 0;

            foreach ($woocommerce->cart->get_cart() as $cart_item_key => $values) {

                $_product = $values['data'];

                if ($_product->is_on_sale()) {
                    $regular_price = $_product->get_regular_price();
                    $sale_price = $_product->get_sale_price();
                    $regular_total += $regular_price * $values['quantity'];
                    $discount = ($regular_price - $sale_price) * $values['quantity'];
                    $discount_total += $discount;
                }

            }

            $discount_total = ($discount_total / $regular_total) * 100; ?>


            <?php do_action('woocommerce_checkout_after_order_review'); ?>

        </div>

        <?php
        if (!is_ajax()) {
            ?>
            <?php
            do_action('woocommerce_review_order_before_payment');
        }
        ?>

        <div class="col-md-12 checkout-total">

            <div class="form-row place-order">
                <noscript>
                    <?php esc_html_e('Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'masterstudy'); ?>
                    <br/>
                    <button type="submit" class="button alt" name="woocommerce_checkout_update_totals"
                            value="<?php esc_attr_e('Update totals', 'masterstudy'); ?>"><?php esc_html_e('Update totals', 'masterstudy'); ?></button>
                </noscript>

                <?php wc_get_template('checkout/terms.php'); ?>

                <?php do_action('woocommerce_review_order_before_submit'); ?>

                <?php $order_button_text = 'PLACE OLDER';
                echo apply_filters('woocommerce_order_button_html', '<button type="submit" class="button alt btn btn-default pull-right" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr($order_button_text) . '" data-value="' . esc_attr($order_button_text) . '">' . $order_button_text . '</button>'); // @codingStandardsIgnoreLine ?>
                <button class="button-order-product" id="button_register_user">PLACE OLDER</button>
                <?php do_action('woocommerce_review_order_after_submit'); ?>

                <?php wp_nonce_field('woocommerce-process_checkout', 'woocommerce-process-checkout-nonce'); ?>
            </div>

        </div>


    </div>


</form>

<?php do_action('woocommerce_after_checkout_form', $checkout); ?>
