<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$order_items = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
?>

<div class="woocommerce-order">

	<?php if ( $order ) : ?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>
		
            <h4 class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'masterstudy'); ?></h4>
            <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
                <a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php esc_html_e( 'Pay', 'masterstudy') ?></a>
				<?php if ( is_user_logged_in() ) : ?>
                    <a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php esc_html_e( 'My account', 'masterstudy'); ?></a>
				<?php endif; ?>
            </p>

		<?php else : ?>
		
			<?php	
			
			$added = get_post_meta( $order->get_id(), 'xC_Order_Invoice_Number', true );
			
			if ( !empty($added)) {
				 //skip if order exists
			}
			else {
				//https://staging-meliuscommerce.kinsta.cloud/checkout/order-received/1256/?key=wc_order_43UgVFmkFaDSc
				
				$url = 'http://xwebservices.thinkmelius.com/ThirdParty/Services.svc/Order_Insert'; //Live
				
				//$url = 'http://xwebservicesmelius.xsslients.com/ThirdParty/Services.svc/Order_Insert'; //Test
				
				/* Payment Type Id */
				$PaymentMethod = $order->get_payment_method();
				$PaymentType = '';
				
				//var_dump($PaymentMethod);
				
				if($PaymentMethod == 'maxicashgateway') {
					$PaymentType = '22';
				}
				if($PaymentMethod == 'coinpayments') {
					$PaymentType = '26';
				}
				if($PaymentMethod == 'stripe') {
					$PaymentType = '23';
				}
				if($PaymentMethod == 'paypal') {
					$PaymentType = '24';
				}
				if($PaymentMethod == 'tap') {
					$PaymentType = '25';
				}
				if($PaymentMethod == 'abzer_networkonline') {
					$PaymentType = '27';
				}
				
				if($PaymentMethod != 'bacs') {
				
					/* Promo Code ID */
					$PromoCode = $order->get_used_coupons();
					$PromoCodeId = '';
					if(!empty($PromoCode)) {
						$PromoCodeId = $PromoCode[0];
					}
					else {
						$PromoCodeId = '0';
					}
								
					/* Order Items */
					$order_items = $order->get_items();
					
					/* Array of Items */
					$ItemsArray = array();
					
					$QvArray = array();
					
					$TotalVolumen = 0;
					
					$OrderTotal = 0;
					
					$AccountChange = 'false';
					$OrderType = '108';
					$IsFreeEnrollment = 'true';
					
					foreach( $order->get_items() as $item ) {
						$product = wc_get_product($item->get_product_id());
						//$skuId = $product->get_sku();
						
						$variation_id = $item->get_variation_id();
						
						$product_variation = wc_get_product($variation_id );
						$skuId = $product_variation->get_sku();
						
						$product_cats = get_the_terms($item->get_product_id(), 'product_cat');
						foreach ( $product_cats as $category ) $categories[] = $category->slug;
						if( !in_array( 'melius-other', $categories ) ) {
						
						switch(str_replace(' days', '', strtolower( $item['subscription'] )) ) {
							case '30':
								if(strpos($product->get_name(), 'Gemini')) {
									$SubscriptionArray = array(
														 'Id' => '25',
														 'LenghtDays' => '30',
														 'Name' => 'Traders Gemini - 30',
														 'Fee' => 'true'
									);
								}
								else {
									$SubscriptionArray = array(
															'Id' => '16',
															'LenghtDays' => '37',
															'Name' => 'Subscription 1 months - 30 Days',
															'Fee' => 'false'
									);
								}
								break;
							case '90':
								if(strcmp($product_variation->get_sku(), 'TGEMINI2020') == 0) {
									$SubscriptionArray = array(
														 'Id' => '26',
														 'LenghtDays' => '90',
														 'Name' => 'Traders Gemini - 90',
														 'Fee' => 'true'
									);
								}
								else {
									$SubscriptionArray = array(
														 'Id' => '15',
														 'LenghtDays' => '97',
														 'Name' => 'Subscription 3 months - 90 Days',
														 'Fee' => 'false'
													 );
								}
								break;
							case '180':
								$SubscriptionArray = array(
														 'Id' => '18',
														 'LenghtDays' => '187',
														 'Name' => 'Subscription 6 months - 180 Days',
														 'Fee' => 'false'
													 );
								break;
							case '365':
								if(strpos($product->get_name(), 'IBO')) {
									$SubscriptionArray = array(
															 'Id' => '17',
															 'LenghtDays' => '360',
															 'Name' => 'IBO KIT',
															 'Fee' => 'true'
														);
									$AccountChange = 'true';
									$OrderType = '114';
									$IsFreeEnrollment = 'false';
									break;
								}
								else {
									$SubscriptionArray = array(
															 'Id' => '19',
															 'LenghtDays' => '367',
															 'Name' => 'Subscription 12 months - 360 Days',
															 'Fee' => 'false'
														 );
									break;
								}
							case '545':
								$SubscriptionArray = array(
														 'Id' => '24',
														 'LenghtDays' => '547',
														 'Name' => 'Subscription 18 months - 545 Days',
														 'Fee' => 'false'
													 );
								break;
							default:
								$SubscriptionArray = array(
														 'Id' => '',
														 'LenghtDays' => '',
														 'Name' => '',
														 'Fee' => ''
													 );
						}

						
						/* Cv and Qv Points */
						$CvPoints = '0.00';
						$QvPoints = '0.00';
						if(strpos($product->get_name(), 'IBO') == false || strcmp($product_variation->get_sku(), 'TGEMINI2020') == 0) {
							$CvPoints = get_post_meta( $variation_id, 'cv_points', true );
							$QvPoints = get_post_meta( $variation_id, 'qv_points', true );
						
							foreach( $order->get_used_coupons() as $coupon_name ){
								$coupon_post_obj = get_page_by_title($coupon_name, OBJECT, 'shop_coupon');
								$coupon_id = $coupon_post_obj->ID;
								
								$coupon_cv = get_post_meta( $coupon_id, 'coupon_cv', true );
								$coupon_qv = get_post_meta( $coupon_id, 'coupon_qv', true );
								
								$coupons_obj = new WC_Coupon($coupon_id);
								
								if(empty($coupon_cv)){
									if ( $coupons_obj->get_discount_type() == 'percent' ){
										$coupons_amount1 = $coupons_obj->get_amount();
										$CvPoints = number_format( $CvPoints, 2 ) * (100 - $coupons_amount1) / 100;
									}
									if( $coupons_obj->is_type( 'fixed_cart' ) ){
										$coupons_amount2 = $coupons_obj->get_amount();
									}
								}
								else{
									$CvPoints = $coupon_cv;
								}
								
								if(empty($coupon_qv)){
									if ( $coupons_obj->get_discount_type() == 'percent' ){
										$coupons_amount1 = $coupons_obj->get_amount();
										$QvPoints = number_format( $QvPoints, 2 ) * (100 - $coupons_amount1) / 100;
									}
									if( $coupons_obj->is_type( 'fixed_cart' ) ){
										$coupons_amount2 = $coupons_obj->get_amount();
									}
								}
								else{							
									$QvPoints = $coupon_qv;
								}
								
								/*$coupons_obj = new WC_Coupon($coupon_id);
								if ( $coupons_obj->get_discount_type() == 'percent' ){
									$coupons_amount1 = $coupons_obj->get_amount();
									$CvPoints = number_format( $CvPoints, 2 ) * (100 - $coupons_amount1) / 100;
									$QvPoints = number_format( $QvPoints, 2 ) * (100 - $coupons_amount1) / 100;
								}
								
								if( $coupons_obj->is_type( 'fixed_cart' ) ){
									$coupons_amount2 = $coupons_obj->get_amount();
								}*/
							}
						}
						
						$order_item = array(
										'Id' => $item->get_product_id(),
										'SkuId' => $skuId,
										'Quantity' => $item->get_quantity(),
										'UnitPrice' => number_format( $item->get_total(), 2 ),
										'TotalPrice' => number_format( $item->get_total(), 2 ),
										'Cv' => number_format( $CvPoints, 2 ),
										'Qv' => number_format( $QvPoints, 2 ),
										'Name' => $product->get_name() .' - '. $item['subscription'],
										'ProductId' => get_post_meta( $variation_id, 'product_id_xC', true ),
										'IsFreeEnrollment' => $IsFreeEnrollment,
										'Subscription' => $SubscriptionArray,
										'AdditionalField' => array(
																'Escape' => 'false',
																'Adventure' => 'false',
																'Experience' => 'false',
																'Elite' => 'false'
															 ),
										'ListCourses' => array(
															array(
																'CourseId' => ''
															)
														),
						);
						
						$OrderTotal += (float)$item->get_total();
						
						array_push($QvArray, $QvPoints);		
						array_push($ItemsArray, $order_item);
						
						$IsFreeEnrollment = 'true';
						
						}
					}
					
					if( !empty($ItemsArray) ){
					$TotalVolumen = array_sum($QvArray);
					
					$args = array(
						'orderHeader' => array(
											 'Id' => '0',
											 'MarketId' => '118',
											 'MarketName' => 'United Kingdom',
											 'PaymentType' => $PaymentType,
											 'OrderDate' => date('m/d/Y H:i:s', strtotime($order->order_date)),
											 'OrderType' => $OrderType,
											 'TotalTax' => number_format( $order->get_total_tax(), 2 ),
											 'TotalShipping' => '0.00',
											 'SubTotal' => number_format( $OrderTotal, 2 ),
											 'TotalDiscounts' => number_format( $order->get_discount_total(), 2 ), //Not required
											 'OrderTotal' => number_format( $OrderTotal, 2 ),
											 'TotalVolumen' => number_format( $TotalVolumen, 2 ),
											 'TotalQualifyVolumen' => number_format( $TotalVolumen, 2 ),
											 'DigitalSignature' => '', //Not required
											 'Status' => '3',
											 'Notes' => '',
											 'Comments' => '',
											 'OrderSource' => '12',
											 'Items' => $ItemsArray,
											 'ShipToName' => $order->get_formatted_billing_full_name(),
											 'PromoCodeId' => '0',
											 'TaxationProviderValidateAddress' => '0',
											 'TaxationProviderCalculateTax' => '0',
											 'MerchantFeeTotal' => 0.00000000,
											 'AccountChange' => $AccountChange,
											 'Distributor' => array(
																  'Id' => get_post_meta( $order->get_id(), 'legacynumber', true ),
																  'FirstName' => $order->get_billing_first_name(),
																  'LastName' => $order->get_billing_last_name(),
																  'UserName' => $order->get_billing_email(),
																  'Email' => $order->get_billing_email(),
																  'LegacyNumber' => get_post_meta( $order->get_id(), 'legacynumber', true )
															  ),
											 'BAddress' => array(
															   'Id' => '3',
															   'Address' => $order->get_billing_address_1(),
															   'Address1' => $order->get_billing_address_2(),
															   'City' => $order->get_billing_city(),
															   'State' => $order->get_billing_state(),
															   'County' => '',
															   'Zip' => $order->get_billing_postcode(),
															   'Country' => $order->get_billing_country(),
															   'IsSameShippingAddress' => 'true'
														   ),
											 'SAddress' => array(
															   'Id' => '3',
															   'Address' => $order->get_shipping_address_1(),
															   'Address1' => $order->get_shipping_address_2(),
															   'City' => $order->get_shipping_city(),
															   'State' => $order->get_shipping_state(),
															   'County' => '',
															   'Zip' => $order->get_shipping_postcode(),
															   'Country' => $order->get_shipping_country()
														   ),
											 'DetailtPayment' => array(
																	array(
																		'Amount' => $order->get_subtotal(),
																		'TransactionId' => $order->get_transaction_id(),
																	),
																 ),
										 ),
						'returnType' => '4',
						'partyToken' => '78697265637420746F6B656E',
						'cultureName' => 'en-GB'
					);
					
					$args = json_encode($args);
					
					//var_dump($args);
					
					$url = curl_init($url);
					curl_setopt($url, CURLOPT_CUSTOMREQUEST, "POST");
					curl_setopt($url, CURLOPT_POSTFIELDS, $args);
					curl_setopt($url, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($url, CURLOPT_HTTPHEADER, array(
						'Content-Type: application/json',
						'Content-Length: ' . strlen($args))
					);
					
					$result = curl_exec($url);
					
					//var_dump($result); 
					
					$result = json_decode($result);
					$response = explode(',', $result->Response);
					$xCorderId = explode(':', $response[0]);
					$xCorderLegacyNumber = explode(':', $response[1]);
					$xCorderInvoiceNumber = explode(':', $response[2]);
					$xCorderInvoiceNumber = explode('"', $xCorderInvoiceNumber[1]);
					
					if(!empty($xCorderLegacyNumber[1])) {
						$log  = date("F j, Y, g:i a").' - WooCommerce Order #'.$order->get_id().' - API Result: Success - Order Legacy Number: '.$xCorderLegacyNumber[1] .'.'. PHP_EOL;
					}
					else {
						$to = 'helpdesk@thinkmelius.com';
						$subject = 'WooCommerce order #'. $order->get_id() .' was not sent to Xirect!';
						$body = 'The API call for WooCommerce order #'. $order->get_id() .' did not go through to Xirect.';
						$headers = array('Content-Type: text/html; charset=UTF-8');
						 
						wp_mail( $to, $subject, $body, $headers );
						$log  = date("F j, Y, g:i a").' - WooCommerce Order #'.$order->get_id().' - API Result: Failed.'. PHP_EOL;
					}
					//Save string to log, use FILE_APPEND to append.
					file_put_contents('xirect.log', $log, FILE_APPEND);
					
					update_post_meta( $order->get_id(), 'xC_Order_Id', $xCorderId[1] );
					update_post_meta( $order->get_id(), 'xC_Order_Legacy_Number', $xCorderLegacyNumber[1] );
					update_post_meta( $order->get_id(), 'xC_Order_Invoice_Number', $xCorderInvoiceNumber[1] );
					}
					else {
						update_post_meta( $order->get_id(), 'xC_Order_Legacy_Number', $order->get_id() );
					}
				}
				else {
					do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() );
					update_post_meta( $order->get_id(), 'xC_Order_Legacy_Number', $order->get_id() );
				}
			}

			?>
		
		
			<div class="invoice-1">
			<div class="success-notice-1 row">
				<div class="pull-left">
					<h4 class="woocommerce-notice-new"><span class="lnr lnr-checkmark-circle"></span> <?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'masterstudy'), $order ); ?></h4>
		<?php 	
        $user = $order->get_user();			
		?>
					<p><?php esc_html_e( 'Username:', 'masterstudy'); ?>  &nbsp;<?php echo $user->user_login; ?>  &nbsp; &nbsp; &nbsp; &nbsp;<?php esc_html_e( 'Order Number:', 'masterstudy'); ?> <?php echo $order->get_id(); ?></p>
				</div>
				</div>
		
			<div class="congrats-notice row">
				<div class="col-md-12">
					<h4 class="woocommerce-notice-new"><span class="lnr lnr-question-circle"></span> <?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Oh, hello there! ', 'masterstudy'), $order ); ?></h4>
				</div>
				<div class="col-md-12">
					<p>Thank you for signing up with us. </p>
				
				<p>
You are now part of a community that strives to bring out the best in you. <br>
At MELiUS, we understand that building a business isn't easy, which is why we will support you in every step of the way and equip you with powerful tools to get work done! 
<br><br>
Getting started is simple. 
<br><br>
Your Back Office - Manage your business, all in one space. <br>
Node -  Check out our growing list of courses provided by industry experts. <br>
<?php

		foreach ( $order_items as $item_id => $item ) {
			if($item->get_product_id() == '13328'){
				echo 'iGoTrade - You trading strategy tool that comes in handy. <br>';
			} 			
			else {}
		}

?>
<br><br>
Stay connected with MELiUS for the latest updates - follow our social media pages and subscribe to our official Telegram groups. 
<br><br>
Facebook: <a href="https://www.facebook.com/thinkmelius/">https://www.facebook.com/thinkmelius/</a><br>
LinkedIn: <a href="https://www.linkedin.com/company/meliusco/">https://www.linkedin.com/company/meliusco/</a><br>
Instagram: <a href="https://www.instagram.com/thinkmelius/?hl=en">https://www.instagram.com/thinkmelius/?hl=en</a><br>
YouTube: <a href="https://www.youtube.com/channel/UCmAKZYe3mQ0VC4v_BxiDoDA">https://www.youtube.com/channel/UCmAKZYe3mQ0VC4v_BxiDoDA</a><br>
Blog: <a href="https://thinkmelius.com/en/blog/">https://thinkmelius.com/en/blog/</a><br>
Telegram:<br>
MELiUS Official English<br>
<a href="https://t.me/joinchat/AAAAAE2DpSqWG1XEPGCaFQ">https://t.me/joinchat/AAAAAE2DpSqWG1XEPGCaFQ</a><br>

MELiUS Official (Spanish)<br>
<a href="https://t.me/joinchat/AAAAAFSuegn3D3SJO7q6Zg">https://t.me/joinchat/AAAAAFSuegn3D3SJO7q6Zg</a><br>

MELiUS Official (French)<br>
<a href="https://t.me/joinchat/AAAAAE4fmF6HZC8yTMTxCg">https://t.me/joinchat/AAAAAE4fmF6HZC8yTMTxCg</a><br><br>

Traders Hub English<br>
<a href="https://t.me/joinchat/AAAAAEUefhm-3XkA0BEb_g">https://t.me/joinchat/AAAAAEUefhm-3XkA0BEb_g</a><br><br>

Traders Hub French<br>
<a href="https://t.me/joinchat/AAAAAEmiat7d4fNXLclqpg">https://t.me/joinchat/AAAAAEmiat7d4fNXLclqpg</a><br><br>

Traders Hub Spanish<br>
<a href="https://t.me/joinchat/AAAAAFauIvj75L1AIWtrMw">https://t.me/joinchat/AAAAAFauIvj75L1AIWtrMw</a><br><br>

Enjoy having complete control of your time.<br>
- MELiUS Team<br>
			    </p>
				</div>
			</div>
			
			<h3>Jump right in</h3>
			
			<table class="order-success">
				<tbody>
					<?php foreach ( $order_items as $item_id => $item ) {
							$product = $item->get_product(); ?>
					<tr>
						<td class="woocommerce-table__product-name product-image">
							<?php
								echo $product->get_image();
							?>
						</td>
						<td>
							<h4><?php echo $product->get_title(); ?></h4>
							<p><?php echo $product->post_excerpt; ?></p>
							<p class="notify">We will notify you via email within next 6 to 24 hours upon activation of your account. You can always login to backoffice to check your account status. Thank you for your patient!</p>
							<a href="http://xbackoffice.thinkmelius.com" class="btn btn-default">Access Backoffice</a>
						</td>
					</tr>
					<?php }?>
				</tbody>
			</table>
			
			<div class="cart-collaterals">
				<?php
					/**
					 * Cart collaterals hook.
					 *
					 * @hooked woocommerce_cross_sell_display
					 * @hooked woocommerce_cart_totals - 10
					 */
					do_action( 'woocommerce_cart_collaterals' );
				?>
			</div>
			
			</div>
			
			<div class="invoice-2">

			<div class="success-notice">
				<h4 class="woocommerce-notice-new"><span class="lnr lnr-checkmark-circle"></span> <?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'masterstudy'), $order ); ?></h4>
			</div>
			
			<div class="multiseparator grid"></div>
			<ul class="woocommerce-order-head pull-left list-unstyled">
				<li>MELiUS</li>
				<li>Office 3302-3303, Platinum Tower</li>
				<li>Cluster I, JLT, Dubai</li>
				<li><a href="mailto:support@thinkmelius.com">support@thinkmelius.com</a></li>
			</ul>
			
            <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details pull-right list-unstyled">

                <li class="woocommerce-order-overview__order order">
					<strong><?php esc_html_e( 'Order #:', 'masterstudy'); ?></strong>
                    <?php echo get_post_meta( $order->get_id(), 'xC_Order_Legacy_Number', true ); ?>
                </li>

                <li class="woocommerce-order-overview__date date">
					<strong><?php esc_html_e( 'Date:', 'masterstudy'); ?></strong>
                    <?php echo wc_format_datetime( $order->get_date_created() ); ?>
                </li>

            </ul>
            <div class="clear"></div>

		<?php endif; ?>
		
		<div class="multiseparator grid"></div>
		
		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>
		
		</div>

	<?php else : ?>

        <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'masterstudy'), null ); ?></p>

	<?php endif; ?>

</div>
