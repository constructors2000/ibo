#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Gift Cards (Gift Vouchers and Packages) (WooCommerce "
"Supported)\n"
"POT-Creation-Date: 2019-05-24 16:55+0530\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Language-Team: Codemenschen <gdpr@codemenschen.at>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.6\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-KeywordsList: __;_e;esc_html__;esc_html_e;esc_attr_e\n"
"Last-Translator: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"X-Poedit-SearchPath-0: admin.php\n"
"X-Poedit-SearchPath-1: front.php\n"
"X-Poedit-SearchPath-2: classes\n"
"X-Poedit-SearchPath-3: include\n"
"X-Poedit-SearchPath-4: templates\n"
"X-Poedit-SearchPath-5: giftitems.php\n"
"X-Poedit-SearchPath-6: upgrade.php\n"
"X-Poedit-SearchPath-7: gift-voucher.php\n"

#: admin.php:43 admin.php:202 include/voucher_template.php:9
msgid "Voucher Templates"
msgstr ""

#: admin.php:44 include/new_voucher_template.php:14
#: include/voucher_template.php:9
msgid "Add New Template"
msgstr ""

#: admin.php:45
msgid "View Voucher Details"
msgstr ""

#: admin.php:46 gift-voucher.php:107 include/voucher_settings.php:181
msgid "Settings"
msgstr ""

#: admin.php:47
msgid "Gift Voucher Orders"
msgstr ""

#: admin.php:166
msgid "Invalid Request."
msgstr ""

#: admin.php:172
msgid "Export All Orders"
msgstr ""

#: admin.php:185 include/voucher_posttype.php:11
msgid "Gift Vouchers"
msgstr ""

#: classes/WPGV_Plugin_Updater.php:225
#, php-format
msgid ""
"There is a new version of %1$s available. %2$s View version %3$s details "
"%4$s."
msgstr ""

#: classes/WPGV_Plugin_Updater.php:233
#, php-format
msgid ""
"There is a new version of %1$s available. %2$s View version %3$s details "
"%4$s or %5$s update now %6$s."
msgstr ""

#: classes/WPGV_Plugin_Updater.php:466
msgid "You do not have permission to install plugin updates"
msgstr ""

#: classes/WPGV_Plugin_Updater.php:466
msgid "Error"
msgstr ""

#: classes/class-nag.php:116
#, php-format
msgid ""
"You've been using <b>What The File</b> for some time now, could you please "
"give it a review at wordpress.org? <br /><br /> <a href='%s' "
"target='_blank'>Yes, take me there!</a> - <a href='%s'>I've already done "
"this!</a><br/><br/><small><a href='http://www.never5.com/' "
"target='_blank'>Check out other Never5 plugins</a></small>"
msgstr ""

#: classes/page_template.php:72
msgid "PDF Voucher Viewer"
msgstr ""

#: classes/page_template.php:73
msgid "PDF Gift Item Viewer"
msgstr ""

#: classes/template.php:16
msgid "Template"
msgstr ""

#: classes/template.php:17
msgid "Templates"
msgstr ""

#: classes/template.php:78
msgid "No templates yet."
msgstr ""

#: classes/template.php:113
msgid "Template ID"
msgstr ""

#: classes/template.php:114 include/new_voucher_template.php:120
msgid "Title"
msgstr ""

#: classes/template.php:115 include/new_voucher_template.php:133
msgid "Image"
msgstr ""

#: classes/template.php:116 include/new_voucher_template.php:146
#: include/view_voucher_details.php:36
msgid "Status"
msgstr ""

#: classes/template.php:117 classes/voucher.php:206
#: include/view_voucher_details.php:35 templates/pdfstyles/receipt.php:51
msgid "Order Date"
msgstr ""

#: classes/template.php:150 include/new_voucher_template.php:21
#: include/new_voucher_template.php:22
msgid "Edit Template"
msgstr ""

#: classes/template.php:151 classes/template.php:198 classes/voucher.php:428
msgid "Delete"
msgstr ""

#: classes/voucher.php:16
msgid "Voucher Order"
msgstr ""

#: classes/voucher.php:17 include/voucher_list.php:12
msgid "Voucher Orders"
msgstr ""

#: classes/voucher.php:161
msgid "No purchased voucher codes yet."
msgstr ""

#: classes/voucher.php:199
msgid "Order id"
msgstr ""

#: classes/voucher.php:200 include/view_voucher_details.php:34
msgid "Voucher Code"
msgstr ""

#: classes/voucher.php:201 include/view_voucher_details.php:83
msgid "Voucher Information"
msgstr ""

#: classes/voucher.php:202
msgid "Buyer's Information"
msgstr ""

#: classes/voucher.php:203 classes/voucher.php:361 classes/voucher.php:426
msgid "Mark as Used"
msgstr ""

#: classes/voucher.php:204 classes/voucher.php:383 classes/voucher.php:427
msgid "Mark as Paid"
msgstr ""

#: classes/voucher.php:205
msgid "Voucher"
msgstr ""

#: classes/voucher.php:261 front.php:40 giftitems.php:36
#: include/view_voucher_details.php:87
msgid "Buying For"
msgstr ""

#: classes/voucher.php:265 front.php:54 front.php:65 front.php:75 front.php:183
#: front.php:218 front.php:255 front.php:413 front.php:445 giftitems.php:50
#: giftitems.php:63 giftitems.php:75 giftitems.php:275 giftitems.php:326
#: giftitems.php:361 giftitems.php:400 giftitems.php:441
#: include/view_voucher_details.php:88 templates/pdfstyles/receipt.php:59
#: templates/pdfstyles/style1.php:42 templates/pdfstyles/style2.php:44
#: templates/pdfstyles/style3.php:43
msgid "Your Name"
msgstr ""

#: classes/voucher.php:270 front.php:58 front.php:69 front.php:187
#: front.php:222 front.php:259 front.php:449 giftitems.php:55 giftitems.php:68
#: giftitems.php:365 giftitems.php:404 giftitems.php:445
#: include/view_voucher_details.php:90 templates/pdfstyles/receipt.php:68
#: templates/pdfstyles/style1.php:55 templates/pdfstyles/style2.php:57
#: templates/pdfstyles/style3.php:56
msgid "Recipient Name"
msgstr ""

#: classes/voucher.php:275 front.php:84 front.php:329 front.php:441
#: giftitems.php:85 templates/pdfstyles/style1.php:69
#: templates/pdfstyles/style2.php:71 templates/pdfstyles/style3.php:70
msgid "Voucher Value"
msgstr ""

#: classes/voucher.php:279 include/view_voucher_details.php:93
msgid "Message"
msgstr ""

#: classes/voucher.php:298 front.php:350 front.php:419 front.php:459
#: giftitems.php:236 giftitems.php:281 include/view_voucher_details.php:112
msgid "Shipping"
msgstr ""

#: classes/voucher.php:303
msgid "Recipient Email"
msgstr ""

#: classes/voucher.php:307 classes/voucher.php:316
msgid "Buyer Email"
msgstr ""

#: classes/voucher.php:312 include/view_voucher_details.php:116
msgid "Name"
msgstr ""

#: classes/voucher.php:320 front.php:380 front.php:479 giftitems.php:263
#: include/view_voucher_details.php:118
msgid "Address"
msgstr ""

#: classes/voucher.php:324 front.php:384 front.php:483 giftitems.php:264
#: include/view_voucher_details.php:119
msgid "Postcode"
msgstr ""

#: classes/voucher.php:328 include/view_voucher_details.php:120
msgid "Shipping Method"
msgstr ""

#: classes/voucher.php:333 front.php:153 front.php:406 front.php:491
#: giftitems.php:121 giftitems.php:296 giftitems.php:335
#: include/view_voucher_details.php:122 templates/pdfstyles/receipt.php:109
msgid "Payment Method"
msgstr ""

#: classes/voucher.php:337 templates/pdfstyles/receipt.php:117
msgid "Payment Status"
msgstr ""

#: classes/voucher.php:341
msgid "Expiry"
msgstr ""

#: classes/voucher.php:365 include/view_voucher_details.php:43
msgid "Voucher Used"
msgstr ""

#: classes/voucher.php:387 include/voucher-shortcodes.php:26
#: include/voucher-shortcodes.php:243 templates/pdfstyles/receipt.php:120
msgid "Paid"
msgstr ""

#: classes/voucher.php:525
msgid "Order Details"
msgstr ""

#: classes/wpgv-gift-voucher-activity.php:55
#, php-format
msgid "Could not find activity record %d."
msgstr ""

#: classes/wpgv-gift-voucher-activity.php:67
msgid "Invalid action value: "
msgstr ""

#: classes/wpgv-gift-voucher.php:49
msgid "Gift Voucher does not exist."
msgstr ""

#: classes/wpgv-gift-voucher.php:52
msgid "Enter a card number."
msgstr ""

#: classes/wpgv-gift-voucher.php:107
msgid "Amount added should be greater than zero."
msgstr ""

#: classes/wpgv-gift-voucher.php:115
msgid "Amount deducted should be less than zero."
msgstr ""

#: classes/wpgv-gift-voucher.php:124
msgid "Unable to adjust balance, card is not active."
msgstr ""

#: classes/wpgv-gift-voucher.php:128
#, php-format
msgid "Balance is currently %s, unable to adjust by %s"
msgstr ""

#: classes/wpgv-gift-voucher.php:186
msgid "Card Number cannot be empty."
msgstr ""

#: classes/wpgv-gift-voucher.php:218
#, php-format
msgid "Failed to generate a unique random card number after %d attempts. %s"
msgstr ""

#: classes/wpgv-gift-voucher.php:246
#, php-format
msgid "Property %s does not exist on %s"
msgstr ""

#: front.php:44 giftitems.php:40
msgid "Someone Else"
msgstr ""

#: front.php:48 giftitems.php:44
msgid "Yourself"
msgstr ""

#: front.php:93
#, php-format
msgid "(Min Voucher Value %s)"
msgstr ""

#: front.php:146
msgid "Choose Voucher Style"
msgstr ""

#: front.php:148 giftitems.php:116
msgid "Style"
msgstr ""

#: front.php:156 giftitems.php:124
msgid "Paypal"
msgstr ""

#: front.php:157 giftitems.php:125
msgid "Sofort"
msgstr ""

#: front.php:159 giftitems.php:127
msgid "Per Invoice"
msgstr ""

#: front.php:169 giftitems.php:106 include/wpgv_item_pdf.php:39
#: include/wpgv_voucher_pdf.php:43 templates/wpgv_item_pdf.php:38
msgid "No Expiry"
msgstr ""

#: front.php:180 front.php:216 front.php:249
#: templates/woocommerce/cart/wpgv-gift-vouchers.php:25
#: templates/woocommerce/checkout/wpgv-gift-vouchers.php:25
msgid "Gift Voucher"
msgstr ""

#: front.php:192 front.php:229 front.php:266 front.php:453 giftitems.php:370
#: giftitems.php:411 giftitems.php:452 templates/pdfstyles/style1.php:82
#: templates/pdfstyles/style2.php:84 templates/pdfstyles/style3.php:83
msgid "Personal Message"
msgstr ""

#: front.php:196 front.php:233 front.php:270 giftitems.php:374
#: giftitems.php:415 giftitems.php:456 templates/pdfstyles/style1.php:96
#: templates/pdfstyles/style2.php:98 templates/pdfstyles/style3.php:97
msgid "Date of Expiry"
msgstr ""

#: front.php:200 front.php:237 front.php:274 giftitems.php:378
#: giftitems.php:419 giftitems.php:460 templates/pdfstyles/receipt.php:93
#: templates/pdfstyles/style1.php:107 templates/pdfstyles/style2.php:109
#: templates/pdfstyles/style3.php:108
msgid "Coupon Code"
msgstr ""

#: front.php:205 front.php:242 front.php:279 giftitems.php:383
#: giftitems.php:424 giftitems.php:465 templates/pdfstyles/style1.php:123
#: templates/pdfstyles/style2.php:125 templates/pdfstyles/style3.php:124
msgid "Cash payment is not possible. The terms and conditions apply."
msgstr ""

#: front.php:309 front.php:311
msgid "Select Templates"
msgstr ""

#: front.php:320 front.php:322
msgid "Personalize"
msgstr ""

#: front.php:334 giftitems.php:220
msgid "Personal Message (Optional)"
msgstr ""

#: front.php:334 giftitems.php:220
msgid "Max: 250 Characters"
msgstr ""

#: front.php:343 front.php:345
msgid "Payment"
msgstr ""

#: front.php:354 giftitems.php:240 include/view_voucher_details.php:114
#: include/view_voucher_details.php:117 templates/pdfstyles/receipt.php:77
msgid "Email"
msgstr ""

#: front.php:358 giftitems.php:244
msgid "Post"
msgstr ""

#: front.php:364 front.php:395 front.php:463 giftitems.php:250
#: giftitems.php:312
msgid "What email address should we send it to?"
msgstr ""

#: front.php:368 front.php:399 front.php:467 giftitems.php:255
#: giftitems.php:317
msgid "Your email address (for the receipt)"
msgstr ""

#: front.php:372 front.php:471 giftitems.php:261
msgid "First Name"
msgstr ""

#: front.php:376 front.php:475 giftitems.php:262
msgid "Last Name"
msgstr ""

#: front.php:388 front.php:487 giftitems.php:267
msgid "Shipping method"
msgstr ""

#: front.php:409 giftitems.php:271 giftitems.php:322
msgid "Your Order"
msgstr ""

#: front.php:425 giftitems.php:287
msgid "Total"
msgstr ""

#: front.php:435 front.php:437
msgid "Overview"
msgstr ""

#: front.php:460 gift-voucher.php:124
msgid "Shipping via Email"
msgstr ""

#: front.php:499 giftitems.php:304 giftitems.php:343
msgid "Pay Now"
msgstr ""

#: front.php:504 giftitems.php:292 giftitems.php:331
msgid "Show Preview as PDF"
msgstr ""

#: gift-voucher.php:108
msgid "Documentation"
msgstr ""

#: gift-voucher.php:115
msgid "Please select voucher template"
msgstr ""

#: gift-voucher.php:116
msgid "Please accept the terms and conditions"
msgstr ""

#: gift-voucher.php:117
msgid "Finish"
msgstr ""

#: gift-voucher.php:118 giftitems.php:226
msgid "Continue"
msgstr ""

#: gift-voucher.php:119 giftitems.php:227 giftitems.php:305 giftitems.php:344
msgid "Back"
msgstr ""

#: gift-voucher.php:120
msgid "Submitted!"
msgstr ""

#: gift-voucher.php:121
msgid "Error occurred"
msgstr ""

#: gift-voucher.php:122
msgid "Total Characters"
msgstr ""

#: gift-voucher.php:123
msgid "Shipping via Post"
msgstr ""

#: gift-voucher.php:125
msgid "Please check email address."
msgstr ""

#: gift-voucher.php:126
msgid "This field is required."
msgstr ""

#: gift-voucher.php:127
msgid "Please fix this field."
msgstr ""

#: gift-voucher.php:128
msgid "Please enter no more than {0} characters."
msgstr ""

#: gift-voucher.php:129
msgid "Please enter a valid email address."
msgstr ""

#: gift-voucher.php:130
msgid "Please enter a value less than or equal to {0}."
msgstr ""

#: gift-voucher.php:131
msgid "Please enter a value greater than or equal to {0}."
msgstr ""

#: giftitems.php:51 giftitems.php:64 giftitems.php:76
msgid "Your name is required"
msgstr ""

#: giftitems.php:56 giftitems.php:69
msgid "Recipient name is required"
msgstr ""

#: giftitems.php:200
msgid "Buy"
msgstr ""

#: giftitems.php:221
msgid "Please enter no more than 250 characters."
msgstr ""

#: giftitems.php:251 giftitems.php:313
msgid "Required"
msgstr ""

#: giftitems.php:256 giftitems.php:318
msgid "Your email address is required"
msgstr ""

#: giftitems.php:260
msgid "Shipping address"
msgstr ""

#: giftitems.php:386 giftitems.php:427 giftitems.php:468
msgid "Voucher Preview"
msgstr ""

#: include/new_voucher_template.php:15
msgid "Add Template"
msgstr ""

#: include/new_voucher_template.php:48
msgid "Template Updated Successfully!"
msgstr ""

#: include/new_voucher_template.php:77
msgid "Template Added Successfully!"
msgstr ""

#: include/new_voucher_template.php:139 include/voucher_metabox.php:212
msgid "Upload Image"
msgstr ""

#: include/new_voucher_template.php:140 include/voucher_metabox.php:45
#: include/voucher_metabox.php:79 include/voucher_metabox.php:213
msgid "Remove Image"
msgstr ""

#: include/new_voucher_template.php:150
msgid "Active"
msgstr ""

#: include/new_voucher_template.php:151
msgid "Inactive"
msgstr ""

#: include/redeem-voucher.php:268
#, php-format
msgid "Shipping via %s"
msgstr ""

#: include/redeem-voucher.php:284
msgid "Gift card applied."
msgstr ""

#: include/redeem-voucher.php:298
msgid "Gift card removed."
msgstr ""

#: include/redeem-voucher.php:324
msgid "Your voucher has expired."
msgstr ""

#: include/redeem-voucher.php:327
msgid "This gift voucher has a zero balance."
msgstr ""

#: include/view_voucher_details.php:29
msgid "Voucher Order ID"
msgstr ""

#: include/view_voucher_details.php:30
msgid "Here you can find detailed information for a voucher code."
msgstr ""

#: include/view_voucher_details.php:37
msgid "See Receipt (PDF)"
msgstr ""

#: include/view_voucher_details.php:43
msgid "Unused"
msgstr ""

#: include/view_voucher_details.php:49
msgid "Template Information"
msgstr ""

#: include/view_voucher_details.php:53
msgid "Template Name"
msgstr ""

#: include/view_voucher_details.php:54
msgid "Template Image"
msgstr ""

#: include/view_voucher_details.php:65
msgid "Item Information"
msgstr ""

#: include/view_voucher_details.php:69
msgid "Item Name"
msgstr ""

#: include/view_voucher_details.php:70
msgid "Item Description"
msgstr ""

#: include/view_voucher_details.php:71
msgid "Item Image"
msgstr ""

#: include/view_voucher_details.php:92 include/voucher-shortcodes.php:353
#: templates/pdfstyles/receipt.php:85
msgid "Amount"
msgstr ""

#: include/view_voucher_details.php:108
msgid "Buyers Information"
msgstr ""

#: include/view_voucher_details.php:123
msgid "Expiry Date"
msgstr ""

#: include/view_voucher_details.php:143
msgid "Back to Vouchers List"
msgstr ""

#: include/voucher-shortcodes.php:17 include/voucher-shortcodes.php:98
#: include/voucher-shortcodes.php:141
msgid "This URL is invalid. You can not access this page directly."
msgstr ""

#: include/voucher-shortcodes.php:95 include/voucher-shortcodes.php:302
msgid ""
"Some Error Occurred From Sending this Email! <br>(Reload and Retry Again!) "
"or Contact Us"
msgstr ""

#: include/voucher-shortcodes.php:246
msgid "Stripe"
msgstr ""

#: include/voucher-shortcodes.php:305 include/voucher-shortcodes.php:308
msgid "Transaction has been failed"
msgstr ""

#: include/voucher-shortcodes.php:324
msgid "Search by Gift voucher code"
msgstr ""

#: include/voucher-shortcodes.php:325
msgid "Check Balance"
msgstr ""

#: include/voucher-shortcodes.php:346
msgid "Current Voucher Balance:"
msgstr ""

#: include/voucher-shortcodes.php:350
msgid "Date"
msgstr ""

#: include/voucher-shortcodes.php:351
msgid "Action"
msgstr ""

#: include/voucher-shortcodes.php:352
msgid "Note"
msgstr ""

#: include/voucher-shortcodes.php:354
msgid "Balance"
msgstr ""

#: include/voucher-shortcodes.php:387
msgid "This is a invalid voucher code."
msgstr ""

#: include/voucher_list.php:26
msgid "Unused Gift Vouchers"
msgstr ""

#: include/voucher_list.php:27
msgid "Total Unused Voucher Amount"
msgstr ""

#: include/voucher_list.php:39
msgid "Import Vouchers"
msgstr ""

#: include/voucher_list.php:41
msgid "Purchased Voucher Codes"
msgstr ""

#: include/voucher_list.php:42
msgid "Purchased Items"
msgstr ""

#: include/voucher_metabox.php:40 include/voucher_metabox.php:67
#: include/voucher_posttype.php:27
msgid "Featured Image"
msgstr ""

#: include/voucher_metabox.php:44 include/voucher_metabox.php:78
msgid "Add Image"
msgstr ""

#: include/voucher_metabox.php:106
msgid "Insert"
msgstr ""

#: include/voucher_metabox.php:154
msgid "Item Details"
msgstr ""

#: include/voucher_metabox.php:188
msgid "Description"
msgstr ""

#: include/voucher_metabox.php:189
msgid "Item Price"
msgstr ""

#: include/voucher_metabox.php:190
msgid "Item Special Price"
msgstr ""

#: include/voucher_posttype.php:9
msgid "Gift Items"
msgstr ""

#: include/voucher_posttype.php:10 include/voucher_posttype.php:12
#: include/voucher_posttype.php:38
msgid "Item"
msgstr ""

#: include/voucher_posttype.php:13
msgid "Item Archives"
msgstr ""

#: include/voucher_posttype.php:14
msgid "Item Attributes"
msgstr ""

#: include/voucher_posttype.php:15
msgid "Parent Item:"
msgstr ""

#: include/voucher_posttype.php:16
msgid "All Gift Items"
msgstr ""

#: include/voucher_posttype.php:17 include/voucher_posttype.php:18
msgid "Add New Item"
msgstr ""

#: include/voucher_posttype.php:19
msgid "New Item"
msgstr ""

#: include/voucher_posttype.php:20
msgid "Edit Item"
msgstr ""

#: include/voucher_posttype.php:21
msgid "Update Item"
msgstr ""

#: include/voucher_posttype.php:22
msgid "View Item"
msgstr ""

#: include/voucher_posttype.php:23
msgid "View Items"
msgstr ""

#: include/voucher_posttype.php:24
msgid "Search Item"
msgstr ""

#: include/voucher_posttype.php:25
msgid "Not found"
msgstr ""

#: include/voucher_posttype.php:26
msgid "Not found in Trash"
msgstr ""

#: include/voucher_posttype.php:28
msgid "Set Item image"
msgstr ""

#: include/voucher_posttype.php:29
msgid "Remove featured image"
msgstr ""

#: include/voucher_posttype.php:30
msgid "Use as featured image"
msgstr ""

#: include/voucher_posttype.php:31
msgid "Insert into Item"
msgstr ""

#: include/voucher_posttype.php:32
msgid "Uploaded to this Item"
msgstr ""

#: include/voucher_posttype.php:33
msgid "Items list"
msgstr ""

#: include/voucher_posttype.php:34
msgid "Items list navigation"
msgstr ""

#: include/voucher_posttype.php:35
msgid "Filter Items list"
msgstr ""

#: include/voucher_posttype.php:39
msgid "Create your store Items as a product"
msgstr ""

#: include/voucher_posttype.php:67
msgid "Item Categories"
msgstr ""

#: include/voucher_posttype.php:68 include/voucher_posttype.php:69
msgid "Item Category"
msgstr ""

#: include/voucher_posttype.php:70
msgid "All Item Categories"
msgstr ""

#: include/voucher_posttype.php:71
msgid "Parent Category"
msgstr ""

#: include/voucher_posttype.php:72
msgid "Parent Category:"
msgstr ""

#: include/voucher_posttype.php:73
msgid "New Category Name"
msgstr ""

#: include/voucher_posttype.php:74
msgid "Add New Category"
msgstr ""

#: include/voucher_posttype.php:75
msgid "Edit Category"
msgstr ""

#: include/voucher_posttype.php:76
msgid "Update Category"
msgstr ""

#: include/voucher_posttype.php:77
msgid "View Category"
msgstr ""

#: include/voucher_posttype.php:78
msgid "Separate Categories with commas"
msgstr ""

#: include/voucher_posttype.php:79
msgid "Add or remove Categories"
msgstr ""

#: include/voucher_posttype.php:80
msgid "Choose from the most used"
msgstr ""

#: include/voucher_posttype.php:81
msgid "Popular Categories"
msgstr ""

#: include/voucher_posttype.php:82
msgid "Search Categories"
msgstr ""

#: include/voucher_posttype.php:83
msgid "Not Found"
msgstr ""

#: include/voucher_posttype.php:84
msgid "No Categories"
msgstr ""

#: include/voucher_posttype.php:85
msgid "Categories list"
msgstr ""

#: include/voucher_posttype.php:86
msgid "Categories list navigation"
msgstr ""

#: include/voucher_settings.php:149
msgid "Your Settings Saved Successfully."
msgstr ""

#: include/voucher_settings.php:192 include/voucher_settings.php:203
msgid "General Settings"
msgstr ""

#: include/voucher_settings.php:193 include/voucher_settings.php:505
msgid "Payment Settings"
msgstr ""

#: include/voucher_settings.php:194 include/voucher_settings.php:639
msgid "Email Settings"
msgstr ""

#: include/voucher_settings.php:195 include/voucher_settings.php:743
msgid "Custom CSS"
msgstr ""

#: include/voucher_settings.php:206
msgid "WooCommerce"
msgstr ""

#: include/voucher_settings.php:218
msgid "Can customers choose voucher styles?"
msgstr ""

#: include/voucher_settings.php:230
msgid "Voucher Style"
msgstr ""

#: include/voucher_settings.php:243 templates/pdfstyles/receipt.php:19
msgid "Company Name"
msgstr ""

#: include/voucher_settings.php:251
msgid "Currency Code"
msgstr ""

#: include/voucher_settings.php:260
msgid "Currency Symbol"
msgstr ""

#: include/voucher_settings.php:268
msgid "Currency Position"
msgstr ""

#: include/voucher_settings.php:279
msgid "Voucher Background Color"
msgstr ""

#: include/voucher_settings.php:287
msgid "Voucher Text Color"
msgstr ""

#: include/voucher_settings.php:295
msgid "Templates Columns"
msgstr ""

#: include/voucher_settings.php:296
msgid "How many templates show in a row. (Gift Voucher Shortcode)"
msgstr ""

#: include/voucher_settings.php:307
msgid "Minimum Voucher Value"
msgstr ""

#: include/voucher_settings.php:308
msgid "Leave 0 if no minimum value"
msgstr ""

#: include/voucher_settings.php:316
msgid "Maximum Voucher Value"
msgstr ""

#: include/voucher_settings.php:324
msgid "Add expiry in voucher"
msgstr ""

#: include/voucher_settings.php:335
msgid "Voucher Expiry Type"
msgstr ""

#: include/voucher_settings.php:336
msgid "Select the type of voucher expiration?"
msgstr ""

#: include/voucher_settings.php:347
msgid "Voucher Expiry Value"
msgstr ""

#: include/voucher_settings.php:348
msgid "Example: (Days: 60, Fixed Date: 20.05.2018)"
msgstr ""

#: include/voucher_settings.php:356
msgid "Expiry date format"
msgstr ""

#: include/voucher_settings.php:365
msgid "Terms and Condition Checkbox Text"
msgstr ""

#: include/voucher_settings.php:373
msgid "Voucher Terms Note"
msgstr ""

#: include/voucher_settings.php:374
msgid "Terms note in voucher order page"
msgstr ""

#: include/voucher_settings.php:382
msgid "Buying for"
msgstr ""

#: include/voucher_settings.php:394
msgid "Hide price from voucher"
msgstr ""

#: include/voucher_settings.php:405
msgid "Post Shipping"
msgstr ""

#: include/voucher_settings.php:416
msgid "Shipping Method for Post Shipping"
msgstr ""

#: include/voucher_settings.php:421
msgid "Multiple methods seperate by comma(,)"
msgstr ""

#: include/voucher_settings.php:426
msgid "Voucher preview Button"
msgstr ""

#: include/voucher_settings.php:438
msgid "Change PDF Save Option"
msgstr ""

#: include/voucher_settings.php:450
msgid "Add Your Custom Demo Image"
msgstr ""

#: include/voucher_settings.php:459
msgid "Add Your Custom Loader URL"
msgstr ""

#: include/voucher_settings.php:468
msgid "Successful Page Message"
msgstr ""

#: include/voucher_settings.php:469
msgid "Message appear after payment successful."
msgstr ""

#: include/voucher_settings.php:473
msgid "Display the email address of the customer"
msgstr ""

#: include/voucher_settings.php:478
msgid "Order Cancellation Message"
msgstr ""

#: include/voucher_settings.php:479
msgid "Message appear after order cancelled"
msgstr ""

#: include/voucher_settings.php:487
msgid "Website URL on PDF in Footer"
msgstr ""

#: include/voucher_settings.php:495
msgid "Email on PDF in Footer"
msgstr ""

#: include/voucher_settings.php:508
msgid "Paypal Enable"
msgstr ""

#: include/voucher_settings.php:519
msgid "Paypal Email"
msgstr ""

#: include/voucher_settings.php:523
msgid "This address is used for paypal payment."
msgstr ""

#: include/voucher_settings.php:528
msgid "Paypal Testmode"
msgstr ""

#: include/voucher_settings.php:539
msgid "Stripe Enable"
msgstr ""

#: include/voucher_settings.php:550
msgid "Stripe Text"
msgstr ""

#: include/voucher_settings.php:551
msgid "This will show on frontend Form"
msgstr ""

#: include/voucher_settings.php:559
msgid "Stripe Publishable key"
msgstr ""

#: include/voucher_settings.php:560
msgid "Collect the Publishable API key from below link."
msgstr ""

#: include/voucher_settings.php:568
msgid "Stripe Secret Key"
msgstr ""

#: include/voucher_settings.php:569
msgid "Collect the Secret API key from below link."
msgstr ""

#: include/voucher_settings.php:577
msgid "Stripe Checkout Page"
msgstr ""

#: include/voucher_settings.php:581
msgid ""
"This page is automatically created for you when you enable stripe payment "
"method."
msgstr ""

#: include/voucher_settings.php:586
msgid "Sofort Enable"
msgstr ""

#: include/voucher_settings.php:597
msgid "Sofort Configuration Key"
msgstr ""

#: include/voucher_settings.php:598
msgid ""
"Enter your configuration key. you only can create a new configuration key by "
"creating a new Gateway project in your account at sofort.com."
msgstr ""

#: include/voucher_settings.php:602
msgid "This key is used for Sofort Payment."
msgstr ""

#: include/voucher_settings.php:607
msgid "Reason for Payment"
msgstr ""

#: include/voucher_settings.php:608
msgid "Reason for payment from Sofort."
msgstr ""

#: include/voucher_settings.php:616
msgid "Selection Per Invoice"
msgstr ""

#: include/voucher_settings.php:617
msgid ""
"With this payment method user don't have to pay immediately, They can "
"directly transfer amount to your bank."
msgstr ""

#: include/voucher_settings.php:628
msgid "Bank Details"
msgstr ""

#: include/voucher_settings.php:629
msgid "This details will show to user who would pay as per invoice."
msgstr ""

#: include/voucher_settings.php:642
msgid "Sender Name"
msgstr ""

#: include/voucher_settings.php:643 include/voucher_settings.php:652
msgid "For emails send by this plugin."
msgstr ""

#: include/voucher_settings.php:651
msgid "Sender Email"
msgstr ""

#: include/voucher_settings.php:660
msgid "Send Customer Receipt"
msgstr ""

#: include/voucher_settings.php:671
msgid "Buyer Email Subject"
msgstr ""

#: include/voucher_settings.php:672 include/voucher_settings.php:722
msgid "Subject for emails send to customers."
msgstr ""

#: include/voucher_settings.php:681
msgid "Buyer Email Body"
msgstr ""

#: include/voucher_settings.php:682 include/voucher_settings.php:732
msgid "Body message for emails send to customers."
msgstr ""

#: include/voucher_settings.php:691
msgid "Buyer Email Body for Per Invoice Selection"
msgstr ""

#: include/voucher_settings.php:692
msgid "This email body is used when customer select payment as per invoice."
msgstr ""

#: include/voucher_settings.php:701
msgid "Recipient Email Subject"
msgstr ""

#: include/voucher_settings.php:702
msgid "Subject for emails send to recipient."
msgstr ""

#: include/voucher_settings.php:711
msgid "Recipient Email Body"
msgstr ""

#: include/voucher_settings.php:712
msgid "Body message for emails send to recipient."
msgstr ""

#: include/voucher_settings.php:721
msgid "Admin Email Subject"
msgstr ""

#: include/voucher_settings.php:731
msgid "Admin Email Body"
msgstr ""

#: include/voucher_settings.php:751
msgid "Save Settings"
msgstr ""

#: include/wpgv_license_page.php:7
msgid "Plugin License Options"
msgstr ""

#: include/wpgv_license_page.php:16
msgid "License Key"
msgstr ""

#: include/wpgv_license_page.php:20
msgid "Enter your license key"
msgstr ""

#: include/wpgv_license_page.php:26 include/wpgv_license_page.php:35
msgid "Activate License"
msgstr ""

#: include/wpgv_license_page.php:30
msgid "active"
msgstr ""

#: include/wpgv_license_page.php:32
msgid "Deactivate License"
msgstr ""

#: templates/pdfstyles/receipt.php:12
msgid "Customer Receipt"
msgstr ""

#: templates/pdfstyles/receipt.php:27
msgid "Company Email"
msgstr ""

#: templates/pdfstyles/receipt.php:35
msgid "Company Website"
msgstr ""

#: templates/pdfstyles/receipt.php:43
msgid "Order Number"
msgstr ""

#: templates/pdfstyles/receipt.php:46
msgid " #"
msgstr ""

#: templates/pdfstyles/receipt.php:101
msgid "Coupon Expiry date"
msgstr ""

#: templates/woocommerce/cart/wpgv-gift-voucher-form.php:14
msgid "Gift Card:"
msgstr ""

#: templates/woocommerce/cart/wpgv-gift-voucher-form.php:15
#: templates/woocommerce/checkout/wpgv-gift-voucher-form.php:15
msgid "Gift voucher code"
msgstr ""

#: templates/woocommerce/cart/wpgv-gift-voucher-form.php:16
#: templates/woocommerce/checkout/wpgv-gift-voucher-form.php:19
msgid "Apply gift card"
msgstr ""

#: templates/woocommerce/cart/wpgv-gift-voucher-form.php:16
#: templates/woocommerce/checkout/wpgv-gift-voucher-form.php:19
msgid "Please wait..."
msgstr ""

#: templates/woocommerce/cart/wpgv-gift-vouchers.php:27
#: templates/woocommerce/checkout/wpgv-gift-vouchers.php:27
msgid "Code"
msgstr ""

#: templates/woocommerce/cart/wpgv-gift-vouchers.php:28
#: templates/woocommerce/checkout/wpgv-gift-vouchers.php:28
#, php-format
msgid "Remaining balance is %s"
msgstr ""

#: templates/woocommerce/cart/wpgv-gift-vouchers.php:34
#: templates/woocommerce/checkout/wpgv-gift-vouchers.php:34
msgid "Expired"
msgstr ""

#: templates/woocommerce/cart/wpgv-gift-vouchers.php:43
#: templates/woocommerce/checkout/wpgv-gift-vouchers.php:43
msgid "[Remove]"
msgstr ""

#: templates/woocommerce/checkout/wpgv-gift-voucher-form.php:7
msgid "Have a gift voucher?"
msgstr ""

#: templates/woocommerce/checkout/wpgv-gift-voucher-form.php:7
msgid "Click here to enter your gift voucher code"
msgstr ""

#: templates/woocommerce/checkout/wpgv-gift-voucher-form.php:12
msgid "If you have a gift card number, please apply it below."
msgstr ""

#: templates/wpgv_item_pdf.php:11 templates/wpgv_item_pdf.php:13
#: templates/wpgv_voucher_pdf.php:11 templates/wpgv_voucher_pdf.php:13
msgid "This is a preview voucher."
msgstr ""

#: upgrade.php:70 upgrade.php:116
msgid "An error occurred, please try again."
msgstr ""

#: upgrade.php:83
#, php-format
msgid "Your license key expired on %s."
msgstr ""

#: upgrade.php:90
msgid "Your license key has been disabled."
msgstr ""

#: upgrade.php:95
msgid "Invalid license."
msgstr ""

#: upgrade.php:101
msgid "Your license is not active for this URL."
msgstr ""

#: upgrade.php:106
#, php-format
msgid "This appears to be an invalid license key for %s."
msgstr ""

#: upgrade.php:111
msgid "Your license key has reached its activation limit."
msgstr ""

#: upgrade.php:161
msgid "Plugin Activated Successfully."
msgstr ""
