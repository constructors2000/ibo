﻿=== Gift Cards (Gift Vouchers and Packages) (WooCommerce Supported) ===
Contributors: codemenschen
Tags: gift cards, gift certificates, gift voucher, woocommerce gift card, gift card wordpress
Requires at least: 4.0
Tested up to: 5.2.1
Stable tag: 3.3.0
Requires PHP: 5.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Let your customers buy gift cards/certificates for your services & products directly on your website.

== Description ==

Trapped over deciding what could you can gift your customers, employees, relatives or neighbours. Here we come to your rescue with our brilliantly customizable gift cards plugin. 

The Gift Cards is a multi-supported (Woocommerce and Wordpress) plugin that is unique in its own way. It is a perfect solution for gift cards, rewards, promotions, fitness & wellness packages and event tickets that includes variety of services. The merchant can  create many gift card products or gift packages according to the fest and occasion with  perfect prices.

Not only this, the plugin also opens the door for you to sell any product, service or experience online using PDF gift cards that can be printed and redeemed in person.

Basic vouchers for events like ‘Birthday’, ‘Anniversary’, ‘New Year’, ‘ Valentine’s Day’ etc can be easily created. They largely reduce manual efforts because all you need to do is just select a template of your choice from available options, set your logo or event image and you are DONE! You see how simple is that! 

Now this plugin is also WooCommerce compatible. So customers can redeem the vouchers in your WooCommerce store also.

<h3>Highlights</h3>
> **Read out the highlights and features to explore what’s more this plugin has in store for you.**
> 
> * **ONE FOR ALL -** Allows customers to buy gift cards/vouchers much like any other product. Several customizable options: choosing card designs, assigning card values, writing personal messages in addition to offering regular product characteristics.
> * **BOOST YOUR BUSINESS -** Connects your online store with your on-the-ground business making life easier for your customers with great customer service.
> * **WooCommerce COMPATIBLE -** This plugin supports WooCommerce websites. Customers can check their voucher balance from ‘My Account’ and redeem the voucher/package from checkout page.
> * **SAFER REDEMPTION -** Convenient and secured modes for purchasing & payments. Proven valid and authentic gift cards (with coupon codes)that deliver your product or service on a later date at a physical location.
> * **TRACKING -** Powerful reporting and tracking features enable you to track the purchased voucher codes. Once the gift card is purchased, its further use can be tracked by the administrator through the unique gift card codes.
> * **EASY DESIGNING -** Design templates for different themes as ‘Birthday’, ‘New Year’, ‘Valentine’s day’, ‘Independence day’ etc. 
> * **POSTAL DELIVERY -** You can accept postal orders for your gift cards. If you want, you can turn on the ability for your customers to buy your own printed gift cards/certificates. So you can send gift cards via post basis directly to the recipient.

**Voucher Booking Forms**
This plugin provides two sorts of voucher booking forms:
* **<a href="https://www.codemenschen.at/gift-items-packages/">Fixed Value Vouchers</a>:** Administrator can provide its customers with a certain set of fixed priced gift items. 
* **<a href="https://www.codemenschen.at/gift-voucher/">Custom Priced Vouchers</a>:**  Price of the vouchers can be defined as per one’s requirement.

<h3><a href="https://www.codemenschen.at/plugins/wordpress-gift-voucher-plugin/" target="_blank">check videos how it works</a></h3>

= Main Plugin Features =

**Front end:**

* Create and design templates with your own logo according to the requirement( fest or occasion)
* Customers can pick from a selection of templates uploaded by the admin and sorted by event (wedding, birth, Valentine’s day, birthday etc)
* Redeem gift cards from WooCommerce checkout page
* Customers can view coupon balance.
* Curate your fixed price gift items according to the required services and products
* Show and preview of gift card option available for the customers while they configure on the booking page
* Email of the purchased  gift card/voucher is sent to the recipient on successful completion
* Customization of customer’s/admin’s email template
* Add color themes and brand symbol/logo on the voucher
* Add a message to be printed on the PDF  gift card/voucher.
* Admin can configure post shipping options through postal delivery to customers
* Redeem via unique auto-generated coupon codes
* Multiple payment gateway integrations: Stripe, PayPal and Sofort Pay.
* Invoice based payment solutions (Customer can pay directly to your bank after purchasing the voucher that is it gives the option of payment per invoice)
* Allow customers to view a list of their gift cards used so far and show both the order on which they have been used and the available credit left.
* Provides 3 varieties for voucher styles in PDF formats
* Set voucher expiry (fixed date, days)
* Remove Expiry date from vouchers
* Hide Price from vouchers
* Send customer receipt after successful order

**Backend:**

* View all gift orders.
* Track the gift cards/vouchers whether they are used or paid orders
* Add edit/delete/view to your voucher/card templates and gift items
* View details of each gift card: full amount, credit left, associated gift-card order, orders on which it has been applied and the total spend it generated.
* Create gift categories
* Set your sender name, email, company name along with company details from plugin settings etc.
* Enable multiple payment gateways

= <a href="https://www.codemenschen.at/plugins/wordpress-gift-voucher-plugin">PREMIUM FEATURES of Gift cards Plugin</a> =

* Create unlimited gift voucher templates
* No limit for customer orders
* Create unlimited  gift items/packages with fixed price
* Create unlimited gift item categories
* Stripe, PayPal and Sofort Payment Gateway Integration
* 6 month support & updates included

**We provides support to only premium users.**

= Premium Live Demos =

Do you want to discover all plugin features? Would you like to try it?
 
Watch the premium demo to get a crystal clear understanding of the features of Gift Cards.

For easy understanding visit our demos from below links -
– **[Gift Cards Demo](https://www.codemenschen.at/gift-voucher/)**
Copy this shortcode and paste it where you want the gift voucher form appear;
`[wpgv_giftvoucher]`

– **[Gift Items/Packages Demo](https://www.codemenschen.at/gift-items-packages/)**
Copy this shortcode and paste it where you want the gift item/package form appear;
`[wpgv_giftitems]`

By accessing our testing platform, you will be able to discover all plugin features and test them as your prefer only in front-end mode.

For more information about the PREMIUM version of Gift Cards (Gift Vouchers and Packages) (WooCommerce Supported), visit the official page on **[codemenschen.at](https://www.codemenschen.at/plugins/wordpress-gift-voucher-plugin/ "Gift Cards (Gift Vouchers and Packages) (WooCommerce Supported) Plugin")**
++++

= This plugin is already translated in Czech (Czech Republic), Danish, Deutsch, French, Hindi, Spanish and Swedish Languages =

If you help us in translating this plugin in your language, It would be very helpful for us.
You can translate this plugin by clicking on the link [here](https://translate.wordpress.org/projects/wp-plugins/gift-voucher/dev "Gift Cards Plugin").

== Installation ==
1. Unzip the downloaded zip file.
2. Upload the plugin folder into the wp-content/plugins/ directory of your WordPress site.
3. Activate 'Gift Cards (Gift Vouchers and Packages) (WooCommerce Supported)' from Plugins page.

== Frequently Asked Questions ==
= Installation instructions
1. Unzip the downloaded plugin zip file.
2. Upload the plugin folder into the wp-content/plugins/ directory of your WordPress site.
3. Activate 'Gift Cards (Gift Vouchers and Packages) (WooCommerce Supported)' from the plugins page.

**CONFIGURATION**
Gift Cards will add a new tab called ‘Settings’ in ‘Gift Vouchers’ menu item. There you will find quick access to the plugin settings page.

= Can I share the gift card?

It can be shared to recipient’s email address in PDF format

= Is multiple website licensing available for premium version?

We provide two kinds of licensing 
* <a href="https://www.codemenschen.at/plugins/wordpress-gift-voucher-plugin">Single website licensing</a> at $19
* <a href="https://www.codemenschen.at/plugins/wordpress-gift-voucher-plugin">Multi website licensing</a> at $34

= Is there any limit to usage under free version?

Up to 10 gift vouchers/templates can be used under free version. In the premium version, you can avail unlimited vouchers.

= Which format will be used in coupon codes?

The plugin will auto-generate a 16 digit unique code and it will be sent as a pdf format to the recipient’s billing email address.

= Can the voucher be sent as a postal delivery?

Yes, this plugin provides a postal delivery option.

= Can I customize the email that is sent?

Yes, admin can customize the email template for customers and himself.

= What happens if the customers spend less than the total voucher value?

Customers can view the voucher balance from ‘ My accounts” page and use the remaining amount later on.

= How do we receive voucher codes?

The voucher codes are sent on recipient’s email address.

= Does it work with Multisite WordPress Environment?

Yes, This plugin will work.

== Documentation ==
Please, read the [official documentation of Gift Cards (Gift Cards and Packages)](https://www.codemenschen.at/docs/wordpress-gift-vouchers-documentation "Documentation of Gift Cards") to learn more about all plugin features.

== Suggestions ==

If you have suggestions about how to improve Gift Cards plugin, you can [write to us](mailto:gdpr@codemenschen.at "Codemenschen").

== Screenshots ==
1. Gift Voucher Form Step 1
2. Gift Voucher Form Step 2
3. Gift Voucher Form Step 3
4. Gift Voucher Form Step 4
5. Gift Items Form Step 1
6. Gift Items Form Step 2
7. Gift Items Form Step 3
8. Plugin Settings Page
9. All Voucher Orders
10. WooCommerce Checkout
11. WooCommerce Order Page
12. Check Voucher Balance

== Changelog ==

= Version 3.3.0 - Released: May 24, 2019 =
* Compatible with latest WordPress 5.2.1 version
* Fixed decimal amount issue in gift items form
* Fixed issues on success order page
* Fixed issues for PDF not found
* Updated all language translations

= Version 3.2.9 - Released: April 10, 2019 =
* Fixed issues on success order page
* Fixed issues for PDF not found
* Updated all language translations

= Version 3.2.8 - Released: April 09, 2019 =
* Added customer receipt feature
* Added feature to add custom stripe text
* Fixed security issues on success order page
* Updated all language translations

= Version 3.2.7 - Released: April 09, 2019 =
* Added customer receipt feature
* Added feature to add custom stripe text
* Fixed security issues on success order page
* Updated all language translations

= Version 3.2.6 - Released: March 27, 2019 =
* Fixed PDF message box issue
* Fixed headers already sent issue
* Added three new languages translation
* Updated all language translations

= Version 3.2.5 - Released: March 06, 2019 =
* Compatible with latest WordPress version
* Fixed scrolling issue with some themes
* Updated language translations

= Version 3.2.4 - Released: February 18, 2019 =
* Added hide price option
* Added no expiry option
* Added custom date formats for expiry date
* Added option to remove someoneelse/yourself tabs
* Added gift voucher redeem form in cart page
* Fixed redeem voucher multiple issue
* Fixed stripe payment card errors
* Fixed other small bugs
* Updated language translations

= Version 3.2.3 - Released: January 03, 2019 =
* Changed readme details
* Updated website design and URL’s

= Version 3.2.2 - Released: December 24, 2018 =
* Fixed timezone problem in voucher order time
* Fixed timezone problem in template added time
* Plugin update issue fixed

= Version 3.2.1 - Released: December 19, 2018 =
* Fixed special character issues in items page
* Fixed message limit in gift items form
* Fixed stripe payment gateway problem

= Version 3.2.0 - Released: December 17, 2018 =
* Fixed PayPal issues
* Fixed twice mail sending issue
* Fixed WooCommerce double priced discount issue
* Added translations for Czech, Spanish and Dutch

= Version 3.1.5 - Released: November 12, 2018 =
* Added terms and condition in gift items form
* Fixed message line issue in pdf vouchers
* Fixed multiple choosen styling option for customers
* Fixed payment gateway issue
* Updated all language fixed

= Version 3.1.4 - Released: October 22, 2018 =
* Added plugin licence activation system
* Added multiple choosen styling option for customers
* Fixed mail sending problem
* Fixed paypal email issue
* Updated all language fixed

= Version 3.1.3 - Released: October 17, 2018 =
* Fixed New Template Page
* Fixed order details page
* Fixed backend view order page
* Fixed order success page mail issue

= Version 3.1.2 - Released: October 13, 2018 =
* Fixed New Template Page
* Fixed New Gift Item Page
* Fixed all shortcodes print issue
* Fixed email sending issues

= Version 3.1.1 - Released: October 10, 2018 =
* Fixed settings saving issue
* Fixed other bugs

= Version 3.1.0 - Released: October 07, 2018 =
* Added new feature in Gift Voucher booking form
* Added new feature in Gift Items booking form
* Added new feature for customization of voucher
* Fixed mail sending issue
* Fixed PayPal issue
* Fixed Stripe issue
* Fixed other bugs

= Version 3.0.1 - Released: September 28, 2018 =
* Compatible with WooCommerce
* Added option for pdf footer url and email
* Added option for demo image
* Added option for success and order cancellation page message
* Added option for disable preview button
* Fixed other bugs

= Version 2.0.3 - Released: September 22, 2018 =
* Compatible with Multisite WP Environment
* Fixed small bugs

= Version 2.0.2 - Released: August 28, 2018 =
* Fixed email sending problem
* Fixed plugin settings page
* Fixed other bugs

= Version 2.0.1 - Released: August 28, 2018 =
* Fixed email sending problem
* Fixed plugin settings page
* Fixed other bugs
* Added Gift Item creating feature

= Version 1.0.5 - Released: May 30, 2018 =
* Added French translations
* Fixed Deutsch and Hindi translations
* Updated voucher-script.js file

= Version 1.0.4 - Released: May 28, 2018 =
* Fixed Deutsch Language translation
* Fixed some bugs

= Version 1.0.3 - Released: May 25, 2018 =
* Added textual changes
* Added default error message
* Upgrading Some styles
* Fixing some bugs

= Version 1.0.2 - Released: May 15, 2018 =
* Added Voucher max value setting
* Added voucher expiry type as fixed date
* Replaced voucher default title to template title
* Updated voucher preview
* Updated wordpress-gift-voucher.php

= Version 1.0.1 - Released: April 26, 2018 =
* Updated wordpress-gift-voucher.php
* Updated front.php
* Fixed 'Note' text problem on voucher order

= Version 1.0.0 - Released: March 30, 2018 =
* Initial release