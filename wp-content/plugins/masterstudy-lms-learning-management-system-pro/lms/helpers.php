<?php
function stm_lms_pro_register_style($style, $deps = array(), $inline_css = '')
{
	$default_path = STM_LMS_PRO_URL . 'assets/css/parts/';
	if(stm_lms_has_custom_colors()) $default_path = stm_lms_custom_styles_url();

	wp_enqueue_style('stm-lms-' . $style, $default_path . $style . '.css', $deps, stm_lms_custom_styles_v());

	if(!empty($inline_css)) wp_add_inline_style('stm-lms-' . $style, $inline_css);
}

function stm_lms_pro_register_script($script, $deps = array(), $footer = false)
{
	wp_enqueue_script('stm-lms-' . $script, STM_LMS_PRO_URL . 'assets/js/' . $script . '.js', $deps, stm_lms_custom_styles_v(), $footer);
}