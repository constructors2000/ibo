<?php

STM_LMS_Pro_Addons::init();

class STM_LMS_Pro_Addons
{
	public static function init()
	{
		add_action('init', 'STM_LMS_Pro_Addons::manage_addons');
		add_action('wp_ajax_stm_lms_pro_save_addons', 'STM_LMS_Pro_Addons::save_addons');
	}

	public static function available_addons()
	{
		return array(
			'udemy' => array(
				'name'     => esc_html__('Udemy Course Importer', 'masterstudy-lms-learning-management-system-pro'),
				'url'      => esc_url(STM_LMS_URL . 'post_type/metaboxes/assets/addons/udemy.png'),
				'settings' => admin_url('admin.php?page=stm-lms-udemy-settings')
			),
			'prerequisite' => array(
				'name'     => esc_html__('Prerequisites', 'masterstudy-lms-learning-management-system-pro'),
				'url'      => esc_url(STM_LMS_URL . 'post_type/metaboxes/assets/addons/msp.png'),
			),
			'online_testing' => array(
				'name'     => esc_html__('Online Testing', 'masterstudy-lms-learning-management-system-pro'),
				'url'      => esc_url(STM_LMS_URL . 'post_type/metaboxes/assets/addons/mst.png'),
				'settings' => admin_url('admin.php?page=stm-lms-online-testing')
			),
		);
	}

	public static function manage_addons()
	{
		$addons_enabled = get_option('stm_lms_addons', array());
		$available_addons = STM_LMS_Pro_Addons::available_addons();

		foreach ($available_addons as $addon => $settings) {
			if (!empty($addons_enabled[$addon]) and $addons_enabled[$addon] == 'on') {
				require_once STM_LMS_PRO_PATH . "/addons/{$addon}/main.php";
			}
		}
	}

	public static function save_addons() {
		$addons = json_decode(stripcslashes($_POST['addons']), true);
		update_option('stm_lms_addons', $addons);
		wp_send_json('done');
	}
}