<?php

$routes = new STM_LMS_WP_Router_Pro();
$routes->create_routes();

class STM_LMS_WP_Router_Pro extends STM_LMS_WP_Router
{

	public function routes($routes = array())
	{
		return parent::routes(array(
			'/lms-manage' => 'stm_lms_manage_course',
			'/lms-manage/{course_id}' => 'stm_lms_manage_course',
		));
	}
}