<?php ob_start(); ?>

<script>
    Vue.component('stm-lesson', {
        data: function () {
            return {
                id: '',
                title: '',
                loading: false,
                fields: {
                    content: '',
                    type: '',
                    duration: '',
                    lesson_excerpt: '',
                    preview: ''
                }
            }
        },
        mounted () {
            var _this = this;
            STM_LMS_EventBus.$on('STM_LMS_Curriculum_item', function (item) {
                console.log(item);
                _this.id = item.id;
                _this.title = item.title;
                _this.opened = true;
                _this.loading = true;

                var url = stm_lms_ajaxurl + '?action=stm_curriculum_get_item&id=' + _this.id;
                this.$http.get(url).then(function(response){

                    var json = response.body;
                    var json_meta = response.body['meta'];

                    if(json.content) {
                        _this.$set(_this.fields, 'content', json.content);
                    } else {
                        _this.$set(_this.fields, 'content', '');
                    }

                    if(json_meta['type']) {
                        _this.$set(_this.fields, 'type', json_meta['type']);
                    } else {
                        _this.$set(_this.fields, 'type', '');
                    }

                    if(json_meta['duration']) {
                        _this.$set(_this.fields, 'duration', json_meta['duration']);
                    } else {
                        _this.$set(_this.fields, 'duration', '');
                    }

                    if(json_meta['lesson_excerpt']) {
                        _this.$set(_this.fields, 'lesson_excerpt', json_meta['lesson_excerpt']);
                    } else {
                        _this.$set(_this.fields, 'lesson_excerpt', '');
                    }

                    if(json_meta['preview']) {
                        _this.$set(_this.fields, 'preview', json_meta['preview']);
                    } else {
                        _this.$set(_this.fields, 'preview', '');
                    }

                    STM_LMS_EventBus.$emit('STM_LMS_Editor_Changed', _this.fields.content);

                    _this.loading = false;
                });
            });
        },
		template: '<?php echo preg_replace(
			"/\r|\n/",
			"",
			addslashes(STM_LMS_Templates::load_lms_template('manage_course/forms/html/lesson'))
		); ?>',
        methods: {
            saveChanges: function() {
                var _this = this;
                _this.loading = true;

                var url = stm_lms_ajaxurl + '?action=stm_lms_pro_save_lesson&post_id=' + _this.id + '&post_title=' + _this.title;

                Object.keys(_this.fields).map(function(objectKey) {
                    url += '&' + objectKey + '=' + _this.fields[objectKey];
                });

                _this.$http.get(url).then(function(r){
                    STM_LMS_EventBus.$emit('STM_LMS_Close_Modal');
                    _this.loading = false;
                });
            },
            discardChanges: function() {
                STM_LMS_EventBus.$emit('STM_LMS_Close_Modal');
            }
        }
    });
</script>

<?php wp_add_inline_script('stm-lms-manage_course', str_replace(array('<script>', '</script>'), '', ob_get_clean())); ?>