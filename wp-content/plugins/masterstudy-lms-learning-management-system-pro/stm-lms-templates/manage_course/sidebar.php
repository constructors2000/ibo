<div class="stm-lms-course__sidebar">

	<?php $post_id = 748; ?>

	<?php STM_LMS_Templates::show_lms_template('manage_course/global/wish-list'); ?>

	<?php STM_LMS_Templates::show_lms_template('manage_course/global/buy-button'); ?>

	<?php STM_LMS_Templates::show_lms_template('manage_course/parts/info', array('course_id' => $post_id)); ?>

	<div class="stm_lms_manage_course__sidebar"></div>

</div>