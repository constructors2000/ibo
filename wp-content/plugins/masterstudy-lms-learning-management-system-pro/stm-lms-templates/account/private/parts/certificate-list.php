<?php
/**
 * @var $current_user
 */

stm_lms_register_style('user-quizzes');
stm_lms_register_style('user-certificates');
$completed = stm_lms_get_user_completed_courses($current_user['id'], array('course_id'), -1);
if (!empty($completed)): ?>
    <div class="stm-lms-user-quizzes stm-lms-user-certificates">
        <div class="stm-lms-user-quiz__head heading_font">
            <div class="stm-lms-user-quiz__head_title">
				<?php esc_html_e('Course', 'masterstudy-lms-learning-management-system-pro'); ?>
            </div>
            <div class="stm-lms-user-quiz__head_status">
				<?php esc_html_e('Certificate', 'masterstudy-lms-learning-management-system-pro'); ?>
            </div>
        </div>
		<?php foreach ($completed as $course): ?>
            <div class="stm-lms-user-quiz">
                <div class="stm-lms-user-quiz__title">
                    <a href="<?php echo esc_url(get_the_permalink($course['course_id'])); ?>">
                        <?php echo wp_kses_post(get_the_title($course['course_id'])); ?>
                    </a>
                </div>
                <a href="<?php echo STM_LMS_Course::certificates_page_url($course['course_id']); ?>"
                   target="_blank"
                   class="stm-lms-user-quiz__name">
					<?php esc_html_e('Download', 'masterstudy-lms-learning-management-system-pro'); ?>
                </a>
            </div>
		<?php endforeach; ?>
    </div>
<?php endif; ?>