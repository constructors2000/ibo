=== WooCommerce Multi-currency ===
Contributors: tivnet
Tags: WooCommerce, Multi-currency
Requires at least: 4.9
Tested up to: 5.0
Requires PHP: 5.3
Stable tag: trunk
License: GPL-3.0-or-later
License URI: https://spdx.org/licenses/GPL-3.0-or-later.html

Multi-currency support for WooCommerce

== Description ==

WooCommerce Multi-currency is a WooCommerce extension that provides switching currencies and re-calculating rates on-the-fly.

== Changelog ==

See the file changelog.txt
