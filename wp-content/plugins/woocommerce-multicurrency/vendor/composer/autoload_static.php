<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit1071ccaa265c91095686032179d5adb1
{
    public static $prefixLengthsPsr4 = array (
        'W' => 
        array (
            'WOOMC\\' => 6,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'WOOMC\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit1071ccaa265c91095686032179d5adb1::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit1071ccaa265c91095686032179d5adb1::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
