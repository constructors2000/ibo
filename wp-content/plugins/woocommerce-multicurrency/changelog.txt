*** WooCommerce Multi-currency ***

2019-01-09 - version 1.13.0
* Tested with WordPress 5.x
* WC tested up to: 3.5.3
* Added: Multi-currency info in the WooCommerce Status Report (for technical support).
* Added: 'Checkout Add-Ons' integration.
* Added: 'Accommodation Booking' integration.
* Fix: (Product Vendors) correct commission calculation if price adjustments are set.
* Fix: (Bookings) correctly convert costs per person.
* Fix: (Bookings) recalculate prices in the Cart when the currency changed.
* Fix: (Product Add-ons) refactored to work correctly on WP Engine servers.

2018-11-30 - version 1.12.0
* WC tested up to: 3.5.2
* Fix: User geolocation was attempted too early, before WC populated the list of countries.
* Added: Initial integration with the 'Product Vendors' extension: calculate commission in the store base currency.

2018-11-24 - version 1.11.0
* Added: integration with the 'Name Your Price' extension.

2018-11-03 - version 1.10.1
* WC tested up to: 3.5.1
* Fix: Canada Post rates converted correctly regardless the base Store currency.

2018-10-30 - version 1.10.0
* WC tested up to: 3.5.0
* Added: integration with the 'Dynamic Pricing' extension.
* Tweak: do not apply rounding formula on the default `round=0.01`.
* Internal: verify minimum required WooCommerce version without querying `get_plugins`

2018-10-15 - version 1.9.0
* WC tested up to: 3.4.6
* Fix: recalculate mini-cart and cart icon totals on currency change.
* Fix: fatal error on PHP < 5.6 (expression in `const`).
* Fix: correct conversion of Flat Rate shipping settings with shortcodes.
* Fix: avoid multiple addition to the cart when switching currency.
* Fix: convert subscription setup fee.
* Added: integration with the 'WooCommerce Royal Mail' extension.
* Added: integration with the 'WooCommerce Canada Post Shipping' extension.
* Added: integration with the 'WooCommerce Australia Post Shipping' extension.
* Added: integration with the 'WooCommerce FedEx Shipping' extension.
* Added: integration with the 'WooCommerce UPS Shipping' extension.
* Added: integration with the 'WooCommerce PostNL' extension.
* Added: API for 3rd parties to add currency rates providers.
* Added: query parameter to force currency (integration with the Google Product Feed extension).
* Internal: added method of getting the active currency via the `woocommerce_multicurrency_active_currency` filter.
* Internal: added "before" and "after" loading hooks.

2018-08-17 - version 1.8.0
* Added: support for the 'WooCommerce USPS Shipping' extension.
* Added: support for 'WP Super Cache' plugin version 1.6.3+.
* Added: metabox to change the currency of orders and subscriptions (does not change the amounts!).
* Fix: convert the fixed-amount coupons data to the actual currency.

2018-08-01 - version 1.7.0
* WC tested up to: 3.4.4
* Added: filtering WooCommerce Reports and Dashboard Widget (total sales) by currency.

2018-07-24 - version 1.6.0
* Fix: various cases of incorrect shipping fee conversions.
* Added: support for 'Product Add-ons' extension.
* Added: support for 'WP Super Cache' plugin.

2018-07-18 - version 1.5.0
* Added: set the price decimals corresponding to the selected currency. For example JPY is shown without "cents".

2018-07-12 - version 1.4.0
* Added: initial selection of the currency by the visitor's location.
* Added: "WooCommerce Subscribe All The Things" (WCS_ATT) extension support.
* Tweak: in multilingual mode, allow some of all languages not to be linked to a currency.

2018-07-07 - version 1.3.0
* Added: support for 'Variable Subscriptions' and 'Bookings' product types.
* Internal: added Migration module to handle potential database changes in the future versions.

2018-06-22 - version 1.2.1
* Tweak: better Price Conversion Setting example.

2018-06-20 - version 1.2.0
* First public release.
* WC tested up to: 3.4.3

2018-05-04 - version 1.1.2
* WC tested up to: 3.4.0
* Fix: `wp_enqueue_script` was called too early.
* Tweak: link to documentation in the Settings Panel.
* Tweak: Widget title changed to 'Currency Selector' (removed 'WooCommerce').

2018-04-04 - version 1.1.1
* Various UX improvements and code cleanup.

2018-03-25 - version 1.0.0
* The first stable version.
