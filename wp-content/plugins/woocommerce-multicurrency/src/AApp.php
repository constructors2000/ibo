<?php

namespace WOOMC;

/**
 * Class AApp
 */
abstract class AApp {

	/**
	 * Hook priority.
	 * Don't use +/- PHP_INT_MAX in case someone wants to stay in front of us.
	 *
	 * @var int
	 */
	const HOOK_PRIORITY_LATE = 4242;

	/**
	 * Negative priority for early hooks.
	 *
	 * @var int
	 */
	const HOOK_PRIORITY_EARLY = - 4242;

	/**
	 * `__FILE__` from the @see configure.
	 *
	 * @var string
	 */
	public $plugin_file = '';

	/**
	 * Basename from `__FILE__`.
	 *
	 * @var string
	 */
	public $plugin_basename = '';

	/**
	 * Plugin directory URL. Initialized by the @see configure.
	 *
	 * @var string
	 */
	public $plugin_dir_url = '';

	/**
	 * Plugin directory path. Initialized by the @see configure.
	 *
	 * @var string
	 */
	protected $plugin_dir_path;

	/**
	 * Used in the `load_textdomain` call.
	 *
	 * @var string
	 */
	protected $textdomain = '';

	/**
	 * Setup important WP variables.
	 *
	 * @param string $plugin_file The full path to the plugin's main file.
	 *
	 * @return $this (fluent)
	 */
	public function configure( $plugin_file ) {
		$this->plugin_file     = $plugin_file;
		$this->plugin_basename = plugin_basename( $this->plugin_file );
		$this->plugin_dir_url  = plugin_dir_url( $this->plugin_file );
		$this->plugin_dir_path = plugin_dir_path( $this->plugin_file );

		return $this;
	}

	/**
	 * Load PO/MO.
	 * $this->textdomain must be set by the child class' constructor.
	 */
	public function load_translations() {
		if ( $this->textdomain ) {
			load_plugin_textdomain( $this->textdomain, false,
				dirname( $this->plugin_basename ) . '/languages'
			);
		}
	}
}
