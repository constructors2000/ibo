<?php

namespace WOOMC;

/**
 * Interface IHookable
 */
interface IHookable {
	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks();
}
