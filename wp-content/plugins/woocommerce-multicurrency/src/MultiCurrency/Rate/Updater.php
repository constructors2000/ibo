<?php

namespace WOOMC\MultiCurrency\Rate;

use WOOMC\MultiCurrency\DAO\Factory;
use WOOMC\MultiCurrency\Rate\Provider\ProviderAbstract;

/**
 * Class Updater
 */
class Updater {

	/**
	 * @param ProviderAbstract $provider
	 * @param string           $option_name
	 *
	 * @return int
	 */
	public function update( $provider, $option_name ) {

		$rates = $provider->retrieve_rates();

		if ( count( $rates ) ) {
			update_option( $option_name, $rates );
			Factory::getDao()->saveRatesTimestamp( $provider->getTimestamp() );
		}

		return count( $rates );
	}
}
