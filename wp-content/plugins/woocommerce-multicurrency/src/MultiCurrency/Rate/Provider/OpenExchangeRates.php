<?php

namespace WOOMC\MultiCurrency\Rate\Provider;

/**
 * Class Provider\OpenExchangeRates
 */
class OpenExchangeRates extends ProviderAbstract {

	/**
	 * @inheritdoc
	 */
	public function configure( array $settings ) {
		$this->url_get_rates = 'https://openexchangerates.org/api/latest.json?app_id=';
		$this->section_rates = 'rates';

		parent::configure( $settings );
	}
}
