<?php

namespace WOOMC\MultiCurrency\Rate\Provider;

/**
 * Class Provider\Currencylayer
 */
class Currencylayer extends ProviderAbstract {

	/**
	 * @inheritdoc
	 */
	public function configure( array $settings ) {
		$this->url_get_rates = 'http://apilayer.net/api/live?access_key=';
		$this->section_rates = 'quotes';

		parent::configure( $settings );
	}


	/**
	 * Remove the "USD" prefix from the currency codes.
	 * 'USDAED' becomes 'AED'.
	 *
	 * @param array $rates
	 *
	 * @return array
	 * @example
	 * "USDAED" => 3.672982,
	 * "USDAFN"=> 57.8936,
	 * "USDALL"=> 126.1652,
	 */
	protected function sanitize_rates( array $rates ) {

		return array_combine(
			array_map( function ( $code ) {
				return substr( $code, 3 );
			}, array_keys( $rates ) ),
			$rates
		);

		//Alternative way:
		//$sanitized_rates = array();
		//array_walk( $rates, function ( $rate, $code ) use ( &$sanitized_rates ) {
		//	$sanitized_rates[ substr( $code, 3 ) ] = $rate;
		//} );

	}
}
