<?php

namespace WOOMC\MultiCurrency\Rate\Provider;

/**
 * Abstract Provider Class
 */
abstract class ProviderAbstract {

	/**
	 * Provider's API URL.
	 *
	 * @var string
	 */
	protected $url_get_rates;

	/**
	 * Rates section in the data received from the provider.
	 *
	 * @var string
	 */
	protected $section_rates;

	/**
	 * Timestamp section in the data received from the provider.
	 *
	 * @var string
	 */
	protected $section_timestamp = 'timestamp';

	/**
	 * @var mixed
	 */
	protected $credentials;

	/**
	 * Formatted rates timestamp.
	 *
	 * @var string
	 */
	protected $timestamp;

	/**
	 * @return mixed
	 */
	public function getCredentials() {
		return $this->credentials;
	}

	/**
	 * @param mixed $credentials
	 */
	protected function setCredentials( $credentials ) {
		$this->credentials = $credentials;
	}

	/**
	 * @return string
	 */
	public function getTimestamp() {
		return $this->timestamp;
	}

	/**
	 * @param string $timestamp
	 */
	public function setTimestamp( $timestamp ) {
		$this->timestamp = $timestamp;
	}

	/**
	 * This method must be called to pass the credentials.
	 * The derived classes also must set the URL and additional parameters.
	 *
	 * @param array $settings
	 *
	 * @return void
	 */
	public function configure( array $settings ) {
		if ( isset( $settings['credentials'] ) ) {
			$this->setCredentials( $settings['credentials'] );
		}
	}

	/**
	 * Call the provider API to retrieve the rates.
	 *
	 * @return float[]
	 */
	public function retrieve_rates() {
		$rates = array();

		$credentials = $this->getCredentials();
		if ( $credentials ) {
			$response_json = wp_remote_retrieve_body( wp_safe_remote_get( $this->url_get_rates . $credentials ) );

			if ( $response_json ) {
				$response_array = json_decode( $response_json, true );
				if ( ! empty( $response_array[ $this->section_rates ] ) ) {
					$rates = $this->sanitize_rates( $response_array[ $this->section_rates ] );
					$this->setTimestamp( $response_array[ $this->section_timestamp ] );
					//$this->setTimestamp( $this->format_timestamp( $response_array[ $this->section_timestamp ] ) );
				}
			}
		}

		return $rates;

	}

	/**
	 * Stub for sanitizing rates.
	 *
	 * @param array $rates
	 *
	 * @return array
	 */
	protected function sanitize_rates( array $rates ) {
		return $rates;
	}

	/**
	 * @param $numeric_timestamp
	 *
	 * @return false|string
	 */
	//protected function format_timestamp( $numeric_timestamp ) {
	//	return date( 'Y-m-d H:i:s', $numeric_timestamp );
	//}
}
