<?php

namespace WOOMC\MultiCurrency\Rate;

use WOOMC\AApp;
use WOOMC\IHookable;
use WOOMC\MultiCurrency\DAO\Factory;
use WOOMC\MultiCurrency\Rate\Provider\ProviderAbstract;
use WOOMC\MultiCurrency\Settings\Panel;

/**
 * Class Rate\Storage
 */
class Storage implements IHookable {

	/**
	 * Transient name used to cache the rates.
	 *
	 * @var string
	 */
	const TRANSIENT_NAME = 'woocommerce_multicurrency_rates_updated';

	/**
	 * Options table key.
	 *
	 * @var string
	 */
	const OPTION_NAME = 'woocommerce_multicurrency_rates';

	/**
	 * Currency rates in the format (string) CODE => (float) RATE
	 *
	 * @var  float[]
	 */
	protected $rates;

	/**
	 * @return float[]
	 */
	public function getRates() {
		return $this->rates;
	}

	/**
	 * @param float[] $rates
	 */
	public function setRates( $rates ) {
		$this->rates = $rates;
	}

	/**
	 * Rate\Storage constructor.
	 */
	public function __construct() {
		$this->load_rates();
	}

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {

		$this->setup_hooks_on_provider_change();

		/**
		 * After saving the settings, reload (and maybe update) the rates.
		 * (Late priority hooks this definitely after the @see Panel::save_fields has finished).
		 */
		add_action( 'woocommerce_update_options_' . Panel::TAB_SLUG, array(
			$this,
			'load_rates',
		), AApp::HOOK_PRIORITY_LATE );
	}

	/**
	 * If Provider ID or credentials changed, we need to update the rates.
	 * Called by @see Panel::save_fields
	 */
	protected function setup_hooks_on_provider_change() {

		$dao = Factory::getDao();

		// TODO get from provider classes
		$providers = array(
			'OpenExchangeRates' => 'OpenExchangeRates',
			'Currencylayer'     => 'Currencylayer',
		);

		add_action( 'update_option_' . $dao->key_rates_provider_id(), array(
			$this,
			'force_update_rates',
		) );

		foreach ( $providers as $provider_id => $provider_name ) {
			$key_rates_provider_credentials = $dao->key_rates_provider_credentials( $provider_id );
			add_action( 'add_option_' . $key_rates_provider_credentials, array(
				$this,
				'force_update_rates',
			) );
			add_action( 'update_option_' . $key_rates_provider_credentials, array(
				$this,
				'force_update_rates',
			) );
		}
	}

	/**
	 * Force updating rates by deleting the transient.
	 *
	 * @internal
	 */
	public function force_update_rates() {
		\delete_transient( self::TRANSIENT_NAME );
	}

	/**
	 * Called by Constructor and also hooked to update settings.
	 */
	public function load_rates() {

		if ( ! defined( 'DOING_AJAX' ) ) {
			// Assuming that the rates were updated at least once during the initial setup,
			// let's not do it on AJAX calls.
			$this->maybe_update_rates();
		}

		$this->setRates( \get_option( self::OPTION_NAME, array() ) );

	}

	/**
	 * Update the currency rates once in..
	 */
	protected function maybe_update_rates() {
		// TODO move transient to DAO.
		if ( ! \get_transient( self::TRANSIENT_NAME ) ) {

			$provider_id = Factory::getDao()->getRatesProviderID();
			if ( ! $provider_id ) {
				return;
			}

			$class_name = __NAMESPACE__ . '\\Provider\\' . $provider_id;
			/** @var ProviderAbstract $provider */
			$provider = new $class_name();
			$provider->configure( array( 'credentials' => Factory::getDao()->getRatesProviderCredentials() ) );

			$rate_updater = new Updater();
			if ( $rate_updater->update( $provider, self::OPTION_NAME ) ) {
				// Got rates. Pause updates for the next 12 hours.
				\set_transient( self::TRANSIENT_NAME, true, HOUR_IN_SECONDS * 12 );
			} else {
				// Could not get rates. Try again in 10 minutes.
				\set_transient( self::TRANSIENT_NAME, true, MINUTE_IN_SECONDS * 10 );
			}
		}
	}

	/**
	 * Get the rate of the specified currency against the default currency.
	 *
	 * Note: the rates retrieved from OpenExchangeRates are against the "base" currency.
	 *
	 * @link https://docs.openexchangerates.org/docs/changing-base-currency
	 *
	 * @example
	 * If our default currency is GBP and we want prices in CAD:
	 * CAD/GBP = CAD/base * base/GBP = CAD/base / GBP/base
	 *
	 * @param string $currency
	 *
	 * @param string $default_currency
	 *
	 * @return float
	 */
	public function get_rate( $currency, $default_currency ) {

		// Sanitize.

		if ( empty( $this->rates[ $currency ] ) ) {
			$this->rates[ $currency ] = 1;
		}
		if ( empty( $this->rates[ $default_currency ] ) ) {
			$this->rates[ $default_currency ] = 1;
		}

		return $this->rates[ $currency ] / $this->rates[ $default_currency ];
	}

	/**
	 * List of currencies we have rates for.
	 *
	 * @return string[]
	 */
	public function get_currencies() {
		return array_keys( $this->rates );
	}

	/**
	 * List of WooCommerce currencies filtered by those we have rates.
	 * We do not use a filter because we need it only for our lists.
	 *
	 * @return array
	 */
	public function woocommerce_currencies_with_rates() {

		// All WC's currencies in the form Code => Name.
		$woocommerce_currencies = get_woocommerce_currencies();

		// The currencies we have rates.
		$rates = $this->getRates();

		// Remove those currencies that we do not have rates.
		return array_intersect_key( $woocommerce_currencies, $rates );
	}
}
