<?php
/**
 * Env.php
 *
 * @package WOOMC\MultiCurrency
 * @since   1.3.0
 */

namespace WOOMC\MultiCurrency;

/**
 * Class Env
 *
 * @package WOOMC\MultiCurrency
 */
class Env {

	/**
	 * Check if doing AJAX call.
	 *
	 * @return bool
	 */
	public static function is_doing_ajax() {
		return ( defined( 'DOING_AJAX' ) && DOING_AJAX ) || self::is_doing_wc_ajax();
	}

	/**
	 * Check if doing WooCommerce AJAX call.
	 *
	 * @return bool
	 */
	public static function is_doing_wc_ajax() {
		return ( ! empty( $_GET['wc-ajax'] ) ); // phpcs:ignore WordPress.CSRF.NonceVerification
	}

	/**
	 * Attempt to check if an AJAX call was originated from admin screen.
	 *
	 * @todo There should be other actions. See $core_actions_get in admin-ajax.php
	 *       Can also check $GLOBALS['_SERVER']['HTTP_REFERER']
	 *       and $GLOBALS['current_screen']->in_admin()
	 *
	 * @return bool
	 */
	public static function is_admin_doing_ajax() {
		return (
			self::is_doing_ajax() &&
			self::is_http_post_action( array(
				'heartbeat',
				'inline-save',
				'save-widget',
				'customize_save',
				'woocommerce_load_variations',
				'ajax-tag-search',
				'wc_braintree_paypal_get_client_token',
			) )
		);
	}

	/**
	 * @param string|string[] $action
	 *
	 * @return bool
	 */
	public static function is_http_post_action( $action ) {

		$action = (array) $action;

		// phpcs:ignore WordPress.CSRF.NonceVerification
		return ( ! empty( $_POST['action'] ) && in_array( $_POST['action'], $action, true ) ); // WPCS: input var ok, sanitization ok.
	}

	/**
	 * @param string|string[] $action
	 *
	 * @return bool
	 */
	public static function is_http_get_action( $action ) {

		$action = (array) $action;

		// phpcs:ignore WordPress.CSRF.NonceVerification
		return ( ! empty( $_GET['action'] ) && in_array( $_GET['action'], $action, true ) ); // WPCS: input var ok, sanitization ok.
	}

	/**
	 * True if I am in the Admin Panel, not doing AJAX
	 *
	 * @return bool
	 */
	public static function in_wp_admin() {
		return ( is_admin() && ! self::is_doing_ajax() );
	}

	/**
	 * True if I am on a front page (not admin area), or doing AJAX from the front.
	 *
	 * @return bool
	 */
	public static function on_front() {
		return ! is_admin() || ( self::is_doing_ajax() && ! self::is_admin_doing_ajax() );
	}


	/**
	 * Check if was called by a specific function (could be any levels deep).
	 *
	 * @param callable|string $method Function name or array(class,function).
	 *
	 * @return bool True if Function is in backtrace.
	 */
	public static function is_function_in_backtrace( $method ) {
		$function_in_backtrace = false;

		// Parse callable into class and function.
		if ( is_string( $method ) ) {
			$function_name = $method;
			$class_name    = '';
		} elseif ( is_array( $method ) && isset( $method[0], $method[1] ) ) {
			list( $class_name, $function_name ) = $method;
		} else {
			return false;
		}

		// Traverse backtrace and stop if the callable is found there.
		foreach ( debug_backtrace() as $_ ) { // phpcs:ignore WordPress.PHP.DevelopmentFunctions.error_log_debug_backtrace
			if ( isset( $_['function'] ) && $_['function'] === $function_name ) {
				$function_in_backtrace = true;
				if ( $class_name && isset( $_['class'] ) && $_['class'] !== $class_name ) {
					$function_in_backtrace = false;
				}
				if ( $function_in_backtrace ) {
					break;
				}
			}
		}

		return $function_in_backtrace;
	}

	/**
	 * To call @see is_function_in_backtrace with the array of parameters.
	 *
	 * @param callable[] $callables Array of callables.
	 *
	 * @return bool True if any of the pair is found in the backtrace.
	 */
	public static function is_functions_in_backtrace( array $callables ) {
		foreach ( $callables as $callable ) {
			if ( self::is_function_in_backtrace( $callable ) ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Get the method that initiated the filter.
	 *
	 * @since 1.6.0
	 *        1.13.0 Refactored: traverse until @see \apply_filters() found.
	 * @return array|false
	 */
	public static function get_hook_caller() {
		$dbt = debug_backtrace(); // phpcs:ignore WordPress.PHP.DevelopmentFunctions.error_log_debug_backtrace

		$next_stop = false;
		foreach ( $dbt as $_ ) {
			if ( $next_stop ) {
				return $_;
			}
			/**
			 * Find the @see \apply_filters() function,
			 * not the @see \WP_Hook::apply_filters() (thus, class must not present).
			 */
			if ( empty( $_['class'] ) && isset( $_['function'] ) && 'apply_filters' === $_['function'] ) {
				// Found. So, the next trace element is the caller.
				$next_stop = true;
			}
		}

		return false;
	}

	/**
	 * Borrowed from WP Super Cache. Reserved for possible future use.
	 *
	 * @see wpsc_is_backend()
	 * @return bool
	 */
	public static function is_backend() {
		static $is_backend;

		if ( isset( $is_backend ) ) {
			return $is_backend;
		}

		$is_backend = is_admin();
		if ( $is_backend ) {
			return $is_backend;
		}

		$script = isset( $_SERVER['PHP_SELF'] ) ? basename( $_SERVER['PHP_SELF'] ) : '';
		if ( 'index.php' !== $script ) {
			if ( in_array( $script, array( 'wp-login.php', 'xmlrpc.php', 'wp-cron.php' ), true ) ) {
				$is_backend = true;
			} elseif ( defined( 'DOING_CRON' ) && DOING_CRON ) {
				$is_backend = true;
			} elseif ( PHP_SAPI === 'cli' || ( defined( 'WP_CLI' ) && WP_CLI ) ) {
				$is_backend = true;
			}
		}

		return $is_backend;
	}

	/**
	 * Returns the current URL.
	 * There is no method of getting the current URL in WordPress.
	 * Various snippets published on the Web use a combination of home_url and add_query_arg.
	 * However, none of them work when WordPress is installed in a subfolder.
	 * The method below looks valid. There is a theoretical chance of HTTP_HOST tampered, etc.
	 * However, the same line of code is used by the WordPress core,
	 * for example in @see wp_admin_canonical_url, so we are going to use it, too
	 * *
	 * Note that #hash is always lost because it's a client-side parameter.
	 * We might add it using a JavaScript call.
	 *
	 * @since 1.9.0
	 */
	public static function current_url() {
		return set_url_scheme( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
	}
}
