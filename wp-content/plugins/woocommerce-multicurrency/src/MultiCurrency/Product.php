<?php
/**
 * Product class.
 *
 * @since 1.13.0
 * Copyright (c) 2018. TIV.NET INC. All Rights Reserved.
 */

namespace WOOMC\MultiCurrency;

/**
 * Class Product
 *
 * @package WOOMC\MultiCurrency
 */
class Product {

	/**
	 * @var string
	 */
	const NOT_AN_OBJECT = 'NOT_AN_OBJECT';

	/**
	 * Get the class of product object.
	 *
	 * @param mixed $product Can be anything: product, its ID, null, etc.
	 *
	 * @return string The class.
	 */
	public static function classname( $product ) {
		$product_class = self::NOT_AN_OBJECT;

		if ( null !== $product ) {
			if ( is_numeric( $product ) ) {
				// Product ID passed.
				$product = \wc_get_product( $product );
			}
			if ( is_object( $product ) ) {
				$product_class = get_class( $product );
			}
		}

		return $product_class;

	}
}
