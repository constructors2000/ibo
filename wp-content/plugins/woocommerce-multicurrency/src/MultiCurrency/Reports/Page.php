<?php
/**
 * Reports page.
 *
 * @since 1.7.0
 * Copyright (c) 2018. TIV.NET INC. All Rights Reserved.
 */

namespace WOOMC\MultiCurrency\Reports;

use WOOMC\IHookable;
use WOOMC\MultiCurrency\App;

class Page implements IHookable {

	/**
	 * The selected currency.
	 *
	 * @var string
	 */
	protected $selected_currency = '';

	/**
	 * Page constructor.
	 *
	 * @param string $selected_currency
	 */
	public function __construct( $selected_currency ) {
		$this->selected_currency = $selected_currency;
	}

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {
		add_action( 'wc_reports_tabs', array( $this, 'currency_selector' ), App::HOOK_PRIORITY_LATE );

		add_filter( 'woocommerce_reports_get_order_report_query', array(
			$this,
			'query_filter',
		) );
	}

	/**
	 * Add a tab with the currency selector dropdown.
	 *
	 * @internal
	 */
	public function currency_selector() {
		?>
		<div class="nav-tab" style="padding: 2px 5px;">
			<?php CurrencySelector::render( $this->selected_currency ); ?>
		</div>
		<?php
	}

	/**
	 * Restrict the reports query to the selected currency.
	 *
	 * @param array $query The SQL query parts.
	 *
	 * @return array
	 * @internal
	 */
	public function query_filter( $query ) {
		/** @global \wpdb $wpdb */
		global $wpdb;

		$query['join']  .= " LEFT JOIN {$wpdb->postmeta} AS meta_currency ON meta_currency.post_id = posts.ID ";
		$query['where'] .= $wpdb->prepare( ' AND meta_currency.meta_key = %s AND meta_currency.meta_value = %s ',
			'_order_currency',
			$this->selected_currency
		);

		return $query;
	}
}
