<?php
/**
 * Reports currency selector.
 *
 * @since 1.7.0
 * Copyright (c) 2018. TIV.NET INC. All Rights Reserved.
 */

namespace WOOMC\MultiCurrency\Reports;

use WOOMC\MultiCurrency\DAO\Factory;

class CurrencySelector {

	/**
	 * The currency selector dropdown.
	 *
	 * @param string $selected_currency The active currency.
	 */
	public static function render( $selected_currency ) {
		$currencies = Factory::getDao()->getEnabledCurrencies();
		?>
		<label for="<?php echo esc_attr( Controller::TAG_CURRENCY_SELECTOR ); ?>">
			<?php esc_html_e( 'Currency', 'woocommerce' ); ?>:
		</label>
		<select id="<?php echo esc_attr( Controller::TAG_CURRENCY_SELECTOR ); ?>">
			<?php foreach ( $currencies as $code ) : ?>
				<option value="<?php echo esc_attr( $code ); ?>"<?php selected( $code, $selected_currency ); ?>><?php echo esc_html( $code ); ?>
				</option>
			<?php endforeach; ?>
		</select>
		<script>
            jQuery(function ($) {
                var name = "<?php echo esc_js( Controller::COOKIE_REPORTS_CURRENCY ); ?>";
                var expires = <?php echo esc_js( YEAR_IN_SECONDS ); ?>;
                var path = "<?php echo esc_js( ADMIN_COOKIE_PATH ); ?>";
                var domain = "<?php echo esc_js( COOKIE_DOMAIN ); ?>";
                var secure = <?php echo is_ssl() ? 'true' : 'false'; ?>;

                $("#<?php echo esc_js( Controller::TAG_CURRENCY_SELECTOR ); ?>").on("change", function () {
                    wpCookies.set(name, this.value, expires, path, domain, secure);
                    window.location.reload();
                });
            });
		</script>

		<?php
	}
}
