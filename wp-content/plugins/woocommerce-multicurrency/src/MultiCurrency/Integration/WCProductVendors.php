<?php
/**
 * Integration.
 * Plugin Name: WooCommerce Product Vendors
 * Plugin URI: https://woocommerce.com/products/product-vendors/
 *
 * @since 1.12.0
 */

namespace WOOMC\MultiCurrency\Integration;

use WOOMC\IHookable;
use WOOMC\MultiCurrency\App;
use WOOMC\MultiCurrency\DAO\Factory;
use WOOMC\MultiCurrency\Env;
use WOOMC\MultiCurrency\Price;

class WCProductVendors implements IHookable {
	/**
	 * DI: Price Controller.
	 *
	 * @var Price\Controller
	 */
	protected $price_controller;

	/**
	 * @var string
	 */
	protected $default_currency;

	/**
	 * WCProductVendors constructor.
	 *
	 * @param Price\Controller $price_controller
	 */
	public function __construct( Price\Controller $price_controller ) {
		$this->price_controller = $price_controller;
		$this->default_currency = Factory::getDao()->getDefaultCurrency();
	}

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {
		add_filter( 'woocommerce_order_get_items',
			array( $this, 'filter__woocommerce_order_get_items' ),
			App::HOOK_PRIORITY_EARLY, 2
		);
	}

	/**
	 * Shortcut: Convert from order currency to the base store currency.
	 *
	 * @note: reverse=true
	 *
	 * @param float|int|string $value
	 * @param string           $order_currency
	 *
	 * @return float|int|string
	 */
	protected function convert_to_store_currency( $value, $order_currency ) {
		return $this->price_controller->convert( $value, null, $this->default_currency, $order_currency, true );
	}

	/**
	 * When an order is processed by @see \WC_Product_Vendors_Order::process,
	 * the list of items in the order is retrieved, the commission is calculated and
	 * inserted to the table by @see \WC_Product_Vendors_Commission::insert.
	 * This filter converts all amounts in the order items to the Store base currency, so that
	 * the commission is calculated in the Store currency.
	 *
	 * @param \WC_Order_Item_Product[] $items
	 * @param \WC_Order                $order
	 *
	 * @return \WC_Order_Item_Product[]
	 * @internal filter
	 */
	public function filter__woocommerce_order_get_items( $items, $order ) {

		$order_currency = $order->get_currency();
		if (
			count( $items )
			&& $this->default_currency !== $order_currency
			&& Env::is_function_in_backtrace( array( 'WC_Product_Vendors_Order', 'process' ) )
		) {

			// $logger  = wc_get_logger();
			// $context = array( 'source' => 'WOOMC-ProductVendors' );
			//
			// $logger->debug( 'Processing order ' . $order->get_id(), $context );
			// $logger->debug( 'Order currency: ' . $order_currency, $context );
			// $logger->debug( 'Store currency: ' . $this->default_currency, $context );

			foreach ( $items as $product ) {
				// $logger->debug( 'Processing product ' . $product->get_id(), $context );
				// $logger->debug( 'Subtotal: ' . $product->get_subtotal(), $context );
				// $logger->debug( 'Subtotal in Store currency: ' . $this->price_controller->convert( $product->get_subtotal(), null, $this->default_currency, $order_currency ), $context );
				// $logger->debug( 'Total ' . $product->get_total(), $context );
				// $logger->debug( 'Total in Store currency: ' . $this->price_controller->convert( $product->get_total(), null, $this->default_currency, $order_currency ), $context );

				$product->set_subtotal( $this->convert_to_store_currency( $product->get_subtotal(), $order_currency ) );
				$product->set_total( $this->convert_to_store_currency( $product->get_total(), $order_currency ) );
				$product->set_subtotal_tax( $this->convert_to_store_currency( $product->get_subtotal_tax(), $order_currency ) );
				$product->set_total_tax( $this->convert_to_store_currency( $product->get_total_tax(), $order_currency ) );
			}

			// add_filter( 'query', function ( $query ) {
			// 	if ( false !== strpos( $query, 'INSERT INTO wp_wcpv_commissions' ) ) {
			// 		$logger  = wc_get_logger();
			// 		$context = array( 'source' => 'WOOMC-ProductVendors' );
			// 		$logger->debug( 'DB Query: ' . $query, $context );
			// 	}
			//
			// 	return $query;
			//
			// } );
		}

		return $items;
	}
}
