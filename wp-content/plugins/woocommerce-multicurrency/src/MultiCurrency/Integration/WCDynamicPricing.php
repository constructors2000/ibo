<?php
/**
 * Integration.
 * Plugin Name: WooCommerce Dynamic Pricing
 * Plugin URI: https://woocommerce.com/products/dynamic-pricing/
 *
 * @package WOOMC\MultiCurrency\Integration
 * @since   1.10.0
 */

namespace WOOMC\MultiCurrency\Integration;

use WOOMC\AApp;
use WOOMC\IHookable;
use WOOMC\MultiCurrency\Env;
use WOOMC\MultiCurrency\Price;


/**
 * Class WCDynamicPricing
 */
class WCDynamicPricing implements IHookable {

	/**
	 *
	 */
	const CONVERT_AMOUNTS_ONLY = 'convert_amounts_only';

	// const CONVERT_FROM_TO_ALSO = 'convert_from_to_also';

	/**
	 * DI: Price Controller.
	 *
	 * @var Price\Controller
	 */
	protected $price_controller;

	/**
	 * Constructor.
	 *
	 * @param Price\Controller $price_controller
	 */
	public function __construct( Price\Controller $price_controller ) {
		$this->price_controller = $price_controller;
	}

	/**
	 * Shortcut to @see \WOOMC\MultiCurrency\Price\Controller::convert().
	 *
	 * @internal filter
	 *
	 * @param float|int|string $price
	 *
	 * @return float|int|string
	 */
	public function convert( $price ) {
		return $this->price_controller->convert( $price );
	}

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {
		if ( ! Env::in_wp_admin() ) {

			// Convert the price used by WC Dynamic Prices as the base for calculations.
			add_filter( 'woocommerce_dynamic_pricing_get_price_to_discount',
				array( $this, 'convert' ), AApp::HOOK_PRIORITY_EARLY
			);

			// Convert the rules in store-wide adjustment sets when modules loaded.
			1 && add_filter( 'wc_dynamic_pricing_load_modules', array( $this, 'filter__load_modules' ) );

			// Alternatively: Use a filter that passes module's $this to convert the rules in adjustment sets.
			0 && add_filter( 'woocommerce_dynamic_pricing_process_product_discounts',
				array( $this, 'convert_adjustment_sets' ),
				AApp::HOOK_PRIORITY_EARLY, 5
			);

			// Convert the rules in the individual products: 1.
			add_filter( 'wc_dynamic_pricing_get_product_pricing_rule_sets', array(
				$this,
				'convert_sets_amounts_only',
			), AApp::HOOK_PRIORITY_EARLY );

			// ...: 2.
			add_filter( 'dynamic_pricing_product_rules', array(
				$this,
				'convert_sets_amounts_only',
			), AApp::HOOK_PRIORITY_EARLY );

			// --- Do not need: covered by sets conversion.
			// add_filter( 'woocommerce_dynamic_pricing_get_rule_amount', function ( $amount ) {
			// 	return $amount;
			// } );

			// --- To check.
			// add_filter( 'wc_dynamic_pricing_get_cart_item_pricing_rule_sets', function ( $pricing_rule_sets ) {
			// 	return $pricing_rule_sets;
			// } );

		}
	}

	/**
	 * Convert array of sets, amounts only, not changing `from-to`.
	 *
	 * @internal filter
	 *
	 * @param array|string|\Traversable $sets Array of rulesets of empty string iа product does not have rules.
	 *
	 * @return array
	 */
	public function convert_sets_amounts_only( $sets ) {
		if ( is_array( $sets ) || ( is_object( $sets ) && $sets instanceof \Traversable ) ) {
			foreach ( $sets as &$set ) {
				$this->convert_set( $set, self::CONVERT_AMOUNTS_ONLY );
			}
		}

		return $sets;
	}

	/**
	 * Convert the rules in adjustment sets.
	 *
	 * @internal filter.
	 *
	 * @param bool                              $is_true        Pass-through unchanged.
	 * @param \WC_Product                       $cart_item_data Unused.
	 * @param string                            $module_id      Type of the module.
	 * @param \WC_Dynamic_Pricing_Advanced_Base $module         Module object.
	 * @param array                             $cart_item      Unused.
	 *
	 * @return bool
	 */
	public function convert_adjustment_sets(
		$is_true,
		/** @noinspection PhpUnusedParameterInspection */
		$cart_item_data,
		/** @noinspection PhpUnusedParameterInspection */
		$module_id,
		$module,
		/** @noinspection PhpUnusedParameterInspection */
		$cart_item
	) {

		$this->convert_module( $module );

		return $is_true;
	}

	/**
	 * Convert set of rules.
	 *
	 * @param array|\stdClass $set
	 * @param string          $module_id Type of the module.
	 */
	protected function convert_set( &$set, $module_id ) {

		if ( is_array( $set ) ) {

			// Flag to prevent repeated conversions.
			if ( isset( $set['woomc_converted'] ) ) {
				return;
			}

			if ( ! empty( $set['rules'] ) ) {
				$this->convert_rules( $set['rules'], $module_id );
			}
			if ( ! empty( $set['blockrules'] ) ) {
				$this->convert_rules( $set['blockrules'], $module_id );
			}

			$set['woomc_converted'] = true;
		}

		if ( is_object( $set ) && ! empty( $set->pricing_rules ) ) {

			// Flag to prevent repeated conversions.
			if ( isset( $set->woomc_converted ) ) {
				return;
			}

			$this->convert_rules( $set->pricing_rules, $module_id );

			$set->woomc_converted = true;
		}

	}

	/**
	 * Convert array or rules.
	 *
	 * @param array[] $rules     The rules.
	 * @param string  $module_id Type of the module.
	 */
	protected function convert_rules( &$rules, $module_id ) {
		foreach ( $rules as &$rule ) {
			$this->convert_rule( $rule, $module_id );
		}
	}

	/**
	 * Convert rule.
	 *
	 * @param array  $rule      The rule (from, to, amount, etc.)
	 * @param string $module_id Type of the module.
	 */
	protected function convert_rule( &$rule, $module_id ) {
		// In categories, from and to are quantities, so do not convert.
		if ( ! in_array( $module_id, array(
			'simple_category',
			'advanced_category',
			'simple_product',
			self::CONVERT_AMOUNTS_ONLY,
		), true ) ) {
			if ( isset( $rule['from'] ) ) {
				$rule['from'] = $this->convert( $rule['from'] );
			}
			if ( isset( $rule['to'] ) ) {
				$rule['to'] = $this->convert( $rule['to'] );
			}
		}

		// Example: Simple Category - available_advanced_rulesets - set - blockrules.
		if ( isset( $rule['adjust'] ) ) {
			$rule['adjust'] = $this->convert( $rule['adjust'] );
		}

		if ( in_array( $rule['type'], array(
			'price_discount',
			'fixed_product',
			'fixed_price',
			'fixed_adjustment',
		), true ) ) {
			$rule['amount'] = $this->convert( $rule['amount'] );
		}
	}

	/**
	 * Convert modules when loaded.
	 *
	 * @param \WC_Dynamic_Pricing_Advanced_Base[] $modules Module objects.
	 *
	 * @return \WC_Dynamic_Pricing_Advanced_Base[]
	 */
	public function filter__load_modules( $modules ) {
		foreach ( $modules as &$module ) {
			$this->convert_module( $module );
		}

		return $modules;
	}

	/**
	 * Convert module.
	 *
	 * @param \WC_Dynamic_Pricing_Advanced_Base $module Module object.
	 */
	protected function convert_module( &$module ) {
		if ( ! empty( $module->available_advanced_rulesets ) ) {
			foreach ( $module->available_advanced_rulesets as &$set ) {
				$this->convert_set( $set, $module->module_id );
			}
		}

		if ( ! empty( $module->available_rulesets ) ) {
			foreach ( $module->available_rulesets as &$set ) {
				$this->convert_set( $set, $module->module_id );
			}
		}

		if ( ! empty( $module->adjustment_sets ) ) {
			foreach ( $module->adjustment_sets as &$set ) {
				$this->convert_set( $set, $module->module_id );
			}
		}
	}
}
