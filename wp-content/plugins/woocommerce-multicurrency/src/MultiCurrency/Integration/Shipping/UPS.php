<?php
/**
 * UPS.php
 * Support the WooCommerce Shipping extension.
 *
 * @package WOOMC\MultiCurrency\Integration\Shipping
 * @since   1.9.0
 * Copyright (c) 2018. TIV.NET INC. All Rights Reserved.
 */

namespace WOOMC\MultiCurrency\Integration\Shipping;

/**
 * Data and method specific to this shipping method.
 */
class UPS extends AbstractController {
	const METHOD_ID = 'ups';
}
