<?php
/**
 * Copyright (c) 2019. TIV.NET INC. All Rights Reserved.
 */

/**
 * Integration.
 * Plugin Name: WooCommerce Bookings.
 * Plugin URI: https://woocommerce.com/products/woocommerce-bookings/
 *
 * @package WOOMC\MultiCurrency\Integration
 * @since   1.3.0
 *          1.13.0 in its own class.
 */

namespace WOOMC\MultiCurrency\Integration\Booking;

use WOOMC\IHookable;
use WOOMC\MultiCurrency\Env;

/**
 * Class WCBookings
 */
class WCBookings extends ABookingIntegration implements IHookable {

	/**
	 * The PHP class of the product I am working with.
	 *
	 * @return string
	 */
	protected function my_product_class() {
		return 'WC_Product_Booking';
	}

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {
		if ( Env::in_wp_admin() ) {
			return;
		}

		add_filter( 'woocommerce_multicurrency_get_props_filters', array(
			$this,
			'filter__woocommerce_multicurrency_get_props_filters',
		) );


		parent::setup_hooks();

	}

	/**
	 * Additional tags to process by @see \WOOMC\MultiCurrency\Price\Controller::filter__calculate_price.
	 *
	 * @param string[] $filter_tags
	 *
	 * @return string[]
	 */
	public function filter__woocommerce_multicurrency_get_props_filters( $filter_tags ) {
		/**
		 * @see \WC_Product_Booking::get_block_cost
		 * @see \WC_Product_Booking::get_cost
		 * @see \WC_Product_Booking_Person_Type::get_cost
		 * @see \WC_Product_Booking_Person_Type::get_block_cost
		 */
		$filter_tags[] = 'woocommerce_product_get_block_cost';
		$filter_tags[] = 'woocommerce_product_get_cost';
		$filter_tags[] = 'woocommerce_product_booking_person_type_get_block_cost';
		$filter_tags[] = 'woocommerce_product_booking_person_type_get_cost';

		return $filter_tags;
	}

	/**
	 * Short-circuit the price conversion for Bookings in some specific cases.
	 *
	 * @internal filter.
	 *
	 * @param false                                  $not_my_business Passed as "false".
	 * @param string|int|float         $value         The price.
	 * @param \WC_Product_Booking|null $product       The product object.
	 *
	 * @return string|int|float|false
	 */
	public function filter__woocommerce_multicurrency_pre_product_get_price( $not_my_business, $value, $product = null ) {

		if ( $this->is_my_product( $product ) ) {

			/**
			 * Convert @see \WC_Product_Booking::get_price_html
			 * but not if called by @see \WC_Product::get_price_suffix
			 */
			if (
				Env::is_function_in_backtrace( array( 'WC_Product_Booking', 'get_price_html' ) )
				&& ! Env::is_function_in_backtrace( array( 'WC_Product', 'get_price_suffix' ) )
			) {
				return $value;
			}

			/**
			 * Do not convert when added to Cart (values are already converted).
			 */
			if ( Env::is_functions_in_backtrace( array(
				array( 'WC_Cart_Totals', 'get_items_from_cart' ),
				array( 'WC_Cart', 'calculate_totals' ),
				array( 'WC_Cart', 'get_product_price' ),
				array( 'WC_Cart', 'get_product_subtotal' ),
			) )
			) {
				return $value;
			}
		}

		// If false the we do not interfere. Let the calling method continue.
		return $not_my_business;
	}

}
