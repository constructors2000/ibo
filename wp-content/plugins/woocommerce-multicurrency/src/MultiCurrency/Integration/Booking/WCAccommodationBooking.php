<?php
/**
 * Copyright (c) 2019. TIV.NET INC. All Rights Reserved.
 */

/**
 * Integration.
 * Plugin Name: WooCommerce Accommodation Bookings.
 * Plugin URI: https://woocommerce.com/products/woocommerce-accommodation-bookings/
 *
 * @package WOOMC\MultiCurrency\Integration
 * @since   1.13.0
 *
 */

namespace WOOMC\MultiCurrency\Integration\Booking;

use WOOMC\IHookable;
use WOOMC\MultiCurrency\Env;

/**
 * Class WCAccommodationBooking
 */
class WCAccommodationBooking extends ABookingIntegration implements IHookable {

	/**
	 * The PHP class of the product I am working with.
	 *
	 * @return string
	 */
	protected function my_product_class() {
		return 'WC_Product_Accommodation_Booking';
	}

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {
		if ( Env::in_wp_admin() ) {
			return;
		}

		parent::setup_hooks();

	}

	/**
	 * Short-circuit the price conversion for Bookings in some specific cases.
	 *
	 * @internal filter.
	 *
	 * @param false                                  $not_my_business Passed as "false".
	 * @param string|int|float                       $value           The price.
	 * @param \WC_Product_Accommodation_Booking|null $product         The product object.
	 *
	 * @return string|int|float|false
	 */
	public function filter__woocommerce_multicurrency_pre_product_get_price( $not_my_business, $value, $product = null ) {

		if ( $this->is_my_product( $product ) ) {

			/**
			 * Do not convert when added to Cart (values are already converted).
			 */
			if ( Env::is_functions_in_backtrace( array(
				array( 'WC_Cart_Totals', 'get_items_from_cart' ),
				array( 'WC_Cart', 'calculate_totals' ),
				array( 'WC_Cart', 'get_product_price' ),
				array( 'WC_Cart', 'get_product_subtotal' ),
			) )
			) {
				return $value;
			}
		}

		// If false the we do not interfere. Let the calling method continue.
		return $not_my_business;
	}

}
