<?php
/**
 * Booking integration abstract.
 *
 * @since 1.13.0
 * Copyright (c) 2019. TIV.NET INC. All Rights Reserved.
 */

namespace WOOMC\MultiCurrency\Integration\Booking;

use WOOMC\MultiCurrency\App;
use WOOMC\MultiCurrency\Log;
use WOOMC\MultiCurrency\Price;
use WOOMC\MultiCurrency\Product;

abstract class ABookingIntegration {

	/**
	 * The PHP class of the product I am working with.
	 *
	 * @return string
	 */
	abstract protected function my_product_class();

	/**
	 * Is the product "mine" (by its PHP class name)?
	 *
	 * @param \WC_Product_Booking $product The Product object.
	 *
	 * @return bool
	 */
	protected function is_my_product( $product ) {
		return $this->my_product_class() === Product::classname( $product );
	}

	/**
	 * DI: Price Controller.
	 *
	 * @var Price\Controller
	 */
	protected $price_controller;

	/**
	 * Constructor.
	 *
	 * @param Price\Controller $price_controller
	 */
	public function __construct( Price\Controller $price_controller ) {
		$this->price_controller = $price_controller;
	}

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {

		/**
		 * Special filters for Booking resources.
		 *
		 * @see \WC_Product_Booking::get_resource_block_costs
		 * @see \WC_Product_Booking::get_resource_base_costs
		 */
		static $filter_tags_booking_resources = array(
			'woocommerce_product_get_resource_block_costs',
			'woocommerce_product_get_resource_base_costs',
		);

		foreach ( $filter_tags_booking_resources as $tag ) {
			add_filter( $tag, array(
				$this,
				'filter__booking_resources',
			), App::HOOK_PRIORITY_EARLY, 2 );
		}

		/**
		 * @see \WC_Product_Booking::get_pricing
		 */
		add_filter( 'woocommerce_product_get_pricing', array(
			$this,
			'filter__woocommerce_product_get_pricing',
		), App::HOOK_PRIORITY_EARLY, 2 );

		// Disable the conversion in certain circumstances.
		add_filter( 'woocommerce_multicurrency_pre_product_get_price', array(
			$this,
			'filter__woocommerce_multicurrency_pre_product_get_price',
		), App::HOOK_PRIORITY_EARLY, 3 );

		add_action( 'woocommerce_before_calculate_totals', array(
			$this,
			'action__woocommerce_before_calculate_totals',
		), App::HOOK_PRIORITY_EARLY );

	}

	/**
	 * Convert Booking resource costs.
	 *
	 * @internal     filter.
	 *
	 * @noinspection PhpUndefinedClassInspection If not using Booking.
	 *
	 * @param array               $resources The `id=>value` array of all resources.
	 * @param \WC_Product_Booking $product   The Product object.
	 *
	 * @return array
	 */
	public function filter__booking_resources( $resources, $product = null ) {

		if ( $this->is_my_product( $product ) ) {
			foreach ( $resources as $id => $value ) {
				$resources[ $id ] = $this->price_controller->convert( $value, $product );
			}
		}

		return $resources;
	}

	/**
	 * Convert additional booking prices (eg, costs per person).
	 *
	 * @since        1.13.0
	 *
	 * @internal     filter.
	 *
	 * @noinspection PhpUndefinedClassInspection If not using Booking.
	 *
	 * @param array               $pricing
	 * @param \WC_Product_Booking $product
	 *
	 * @return mixed
	 */
	public function filter__woocommerce_product_get_pricing( $pricing, $product ) {

		if ( count( $pricing ) && $this->is_my_product( $product ) ) {
			foreach ( $pricing as &$costs ) {
				if ( ! empty( $costs['cost'] ) ) {
					$costs['cost'] = $this->price_controller->convert( $costs['cost'], $product );
				}
				if ( ! empty( $costs['base_cost'] ) ) {
					$costs['base_cost'] = $this->price_controller->convert( $costs['base_cost'], $product );
				}
			}
		}

		return $pricing;
	}

	/**
	 * Recalculate booking cost for each item in the cart.
	 * Without it, when the currency changes, costs in the cart stay the same.
	 *
	 * @param \WC_Cart $cart The cart.
	 *
	 * @return \WC_Cart
	 */
	public function action__woocommerce_before_calculate_totals( $cart ) {

		foreach ( $cart->cart_contents as $key => $cart_item ) {

			if ( ! isset( $cart_item['booking'] ) ) {
				continue;
			}

			/** @var \WC_Product_Booking $product */
			$product = $cart_item['data'];
			if ( ! $this->is_my_product( $product ) ) {
				continue;
			}

			$booking_data = $cart_item['booking'];

			// Emulate $_POST from the Booking Form at the add-to-cart.
			$posted = array(
				'wc_bookings_field_start_date_year'  => $booking_data['_year'],
				'wc_bookings_field_start_date_month' => $booking_data['_month'],
				'wc_bookings_field_start_date_day'   => $booking_data['_day'],
				'add-to-cart'                        => $product->get_id(),
			);

			if ( isset( $booking_data['_duration'] ) ) {
				$posted['wc_bookings_field_duration'] = $booking_data['_duration'];
			}

			if ( isset( $booking_data['_time'] ) ) {
				$date = isset( $booking_data['_date'] ) ? $booking_data['_date'] :
					sprintf( '%1$4d-%2$02d-%3$02d',
						$booking_data['_year'],
						$booking_data['_month'],
						$booking_data['_day']
					);

				$timestamp = strtotime( $date . ' ' . $booking_data['_time'] );

				$posted['wc_bookings_field_start_date_time'] = date( 'c', $timestamp );
			}

			if ( $product->has_persons() && isset( $booking_data['_persons'] ) ) {
				if ( $product->has_person_types() ) {
					foreach ( $booking_data['_persons'] as $person_id => $value ) {
						$posted[ 'wc_bookings_field_persons_' . $person_id ] = $value;
					}
				} else {
					$posted['wc_bookings_field_persons'] = $booking_data['_persons'][0];
				}
			}

			if ( isset( $booking_data['_resource_id'] ) ) {
				$posted['wc_bookings_field_resource'] = $booking_data['_resource_id'];
			}

			/**
			 * TODO
			 * wc_bookings_field_start_date_yearmonth
			 * wc_bookings_field_start_date_local_timezone
			 */

			/**
			 * @note This is a child class that does not validate "is_bookable".
			 *       Otherwise, we cannot calculate the costs when there are limits in bookable spaces.
			 */
			$booking_form = new OWCBookingForm( $product );

			/** @var string|\WP_Error $cost */
			$cost = $booking_form->calculate_booking_cost( $posted );
			if ( ! is_wp_error( $cost ) ) {
				$product->set_price( $cost );
			} else {
				/**
				 * If there was an error, let's remove this item from the cart.
				 *
				 * Example: the cart was on the screen too long.
				 * Recalculation returned "Date must be in the future" error.
				 */
				$cart->remove_cart_item( $key );
				Log::error( implode( '|', array(
					$cost->get_error_message(),
					__METHOD__,
					__LINE__,
				) ) );
			}
		}

		return $cart;
	}
}
