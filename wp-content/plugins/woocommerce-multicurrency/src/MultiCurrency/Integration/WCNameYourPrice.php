<?php
/**
 * Integration.
 * Plugin Name: Name Your Price
 * Plugin URI: https://woocommerce.com/products/name-your-price/
 *
 * @package WOOMC\MultiCurrency\Integration
 * @since   1.11.0
 */

namespace WOOMC\MultiCurrency\Integration;

use WOOMC\AApp;
use WOOMC\IHookable;
use WOOMC\MultiCurrency\Env;
use WOOMC\MultiCurrency\Price;


/**
 * Class WCNameYourPrice
 */
class WCNameYourPrice implements IHookable {

	/**
	 * DI: Price Controller.
	 *
	 * @var Price\Controller
	 */
	protected $price_controller;

	/**
	 * Constructor.
	 *
	 * @param Price\Controller $price_controller
	 */
	public function __construct( Price\Controller $price_controller ) {
		$this->price_controller = $price_controller;
	}

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {
		if ( ! Env::in_wp_admin() ) {

			add_filter( 'woocommerce_raw_suggested_price', array(
				$this->price_controller,
				'filter__woocommerce_product_get_price',
			), AApp::HOOK_PRIORITY_EARLY, 2 );

			add_filter( 'woocommerce_raw_minimum_price', array(
				$this->price_controller,
				'filter__woocommerce_product_get_price',
			), AApp::HOOK_PRIORITY_EARLY, 2 );

			add_filter( 'woocommerce_raw_maximum_price', array(
				$this->price_controller,
				'filter__woocommerce_product_get_price',
			), AApp::HOOK_PRIORITY_EARLY, 2 );

			add_action( 'woocommerce_add_cart_item_data', array( $this, 'add_initial_currency' ) );

			add_filter( 'woocommerce_get_cart_item_from_session', array(
				$this,
				'filter__woocommerce_get_cart_item_from_session',
			), AApp::HOOK_PRIORITY_LATE, 2 );
		}
	}

	/**
	 * Store the initial currency when item is added.
	 *
	 * @since 1.11.0
	 *
	 * @param array $cart_item_data
	 *
	 * @return array
	 */
	public function add_initial_currency( $cart_item_data ) {

		if ( isset( $cart_item_data['nyp'] ) ) {
			$cart_item_data['nyp_currency'] = get_woocommerce_currency();
		}

		return $cart_item_data;
	}

	/**
	 * Filter Name Your Price Cart prices.
	 *
	 * @internal filter.
	 *
	 * @since    1.11.0
	 *
	 * @param array  $session_data
	 * @param array  $values
	 *
	 * @return array
	 */
	public function filter__woocommerce_get_cart_item_from_session( $session_data, $values ) {

		// Preserve original currency.
		if ( isset( $values['nyp_currency'] ) ) {
			$session_data['nyp_currency'] = $values['nyp_currency'];
		}

		/**
		 * Special processing for Name Your Price.
		 */
		if ( isset( $session_data['nyp'] ) && isset( $session_data['nyp_currency'] ) ) {

			/** @var \WC_Product $product */
			$product =& $session_data['data'];

			$amount_entered_by_the_client   = $session_data['nyp'];
			$currency_of_the_entered_amount = $session_data['nyp_currency'];
			$the_store_currency             = get_option( 'woocommerce_currency' );

			$new_price = $this->price_controller->convert( $amount_entered_by_the_client, $product, $the_store_currency, $currency_of_the_entered_amount );

			$product->set_price( $new_price );
			$product->set_regular_price( $new_price );
			$product->set_sale_price( $new_price );

			// Subscription-specific price and variable billing period.
			if ( $product->is_type( array( 'subscription', 'subscription_variation' ) ) ) {
				$product->update_meta_data( '_subscription_price', $new_price );
			}
		}

		return $session_data;

	}
}
