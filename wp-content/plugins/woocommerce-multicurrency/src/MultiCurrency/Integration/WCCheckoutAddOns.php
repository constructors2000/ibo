<?php
/**
 * Integration.
 * Plugin Name: WooCommerce Checkout Add-Ons
 * Plugin URI: https://woocommerce.com/products/woocommerce-checkout-add-ons/
 *
 * @package WOOMC\MultiCurrency\Integration
 * @since   1.13.0
 */

namespace WOOMC\MultiCurrency\Integration;

use WOOMC\IHookable;
use WOOMC\MultiCurrency\App;
use WOOMC\MultiCurrency\Price;


class WCCheckoutAddOns implements IHookable {

	/**
	 * DI: Price Controller.
	 *
	 * @var Price\Controller
	 */
	protected $price_controller;

	/**
	 * Constructor.
	 *
	 * @param Price\Controller $price_controller
	 */
	public function __construct( Price\Controller $price_controller ) {
		$this->price_controller = $price_controller;
	}

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {
		add_filter( 'wc_checkout_add_ons_add_on_get_cost', array(
			$this,
			'filter__wc_checkout_add_ons_add_on_get_cost',
		), App::HOOK_PRIORITY_EARLY, 2 );

		add_filter( 'wc_checkout_add_ons_add_on_option_cost', array(
			$this,
			'filter__wc_checkout_add_ons_add_on_option_cost',
		), App::HOOK_PRIORITY_EARLY, 3 );

	}

	/**
	 * Filter the add-on cost.
	 *
	 * @param mixed               $cost   The add-on cost.
	 * @param \WC_Checkout_Add_On $add_on This instance of WC_Checkout_Add_On class.
	 *
	 * @return \WC_Checkout_Add_On
	 */
	public function filter__wc_checkout_add_ons_add_on_get_cost( $cost, $add_on ) {
		if ( 'fixed' === $add_on->get_cost_type() ) {
			$cost = $this->price_controller->convert( $cost );
		}

		return $cost;
	}

	/**
	 * Filter the individual option cost.
	 *
	 * @param float               $cost   The option cost.
	 * @param array               $option The option data.
	 * @param \WC_Checkout_Add_On $add_on The full add-on object.
	 *
	 * @return float
	 */
	public function filter__wc_checkout_add_ons_add_on_option_cost(
		$cost, $option,
		/** @noinspection PhpUnusedParameterInspection */
		$add_on
	) {
		if ( ! empty( $option['cost_type'] ) && 'fixed' === $option['cost_type'] ) {
			$cost = $this->price_controller->convert( $cost );
		}

		return $cost;
	}
}
