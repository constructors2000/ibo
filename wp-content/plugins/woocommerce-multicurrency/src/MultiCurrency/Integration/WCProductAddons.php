<?php
/**
 * Copyright (c) 2019. TIV.NET INC. All Rights Reserved.
 */

/**
 * Integration.
 * Plugin Name: WooCommerce Product Add-ons.
 * Plugin URI: https://woocommerce.com/products/product-add-ons/
 *
 * @package WOOMC\MultiCurrency\Integration
 * @since   1.6.0
 *          1.13.0 Refactored and moved to a separate class.
 */

namespace WOOMC\MultiCurrency\Integration;

use WOOMC\IHookable;
use WOOMC\MultiCurrency\App;
use WOOMC\MultiCurrency\Env;
use WOOMC\MultiCurrency\Price;


class WCProductAddons implements IHookable {

	/**
	 * DI: Price Controller.
	 *
	 * @var Price\Controller
	 */
	protected $price_controller;

	/**
	 * Constructor.
	 *
	 * @param Price\Controller $price_controller
	 */
	public function __construct( Price\Controller $price_controller ) {
		$this->price_controller = $price_controller;
	}

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {
		add_filter( 'woocommerce_product_addons_option_price_raw', array(
			$this,
			'filter__woocommerce_product_addons_option_price_raw',
		), App::HOOK_PRIORITY_EARLY );

		add_filter( 'woocommerce_get_price_excluding_tax', array(
			$this,
			'filter__product_addons',
		), App::HOOK_PRIORITY_EARLY );

		add_filter( 'woocommerce_get_price_including_tax', array(
			$this,
			'filter__product_addons',
		), App::HOOK_PRIORITY_EARLY );
	}

	/**
	 * Convert option prices. This is the main conversion for WC Product Add-ons.
	 *
	 * @internal filter
	 *
	 * @param $option_price
	 *
	 * @return float|int|string
	 */
	public function filter__woocommerce_product_addons_option_price_raw( $option_price ) {
		return $this->price_controller->convert( $option_price );
	}

	/**
	 * Filter Product Add-ons display prices. For special cases only.
	 *
	 * @note     As of POA Version: 3.0.5, there is a bug in the function
	 * @see      \WC_Product_Addons_Helper::get_product_addon_price_for_display.
	 * `if ( ( is_cart() || is_checkout() ) && null !== $cart_item ) {` is wrong
	 * because it does not consider the mini-cart widget.
	 *
	 * @internal filter.
	 *
	 * @since    1.6.0
	 *
	 * @param int|float|string $price The price.
	 *
	 * @return float|int|string
	 */
	public function filter__product_addons( $price ) {

		// Only if called by certain functions.
		if (
		Env::is_functions_in_backtrace(
			array(
				array( 'WC_Product_Addons_Cart', 'get_item_data' ),
				array( 'Product_Addon_Display', 'totals' ),
			)
		)
		) {
			/**
			 * Only if the price was not retrieved from the Product (and therefore already converted),
			 * but passed as a parameter to...
			 *
			 * @see \wc_get_price_excluding_tax
			 * @see \wc_get_price_including_tax
			 */
			$called_by = Env::get_hook_caller();
			if ( ! empty( $called_by['args'][1]['price'] ) ) {
				$price = $this->price_controller->convert( $price );
			}
		}

		return $price;
	}
}
