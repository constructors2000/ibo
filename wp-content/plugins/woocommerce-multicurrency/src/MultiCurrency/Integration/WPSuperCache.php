<?php
/**
 * WP Super Cache integration.
 *
 * @since 1.8.0
 * Copyright (c) 2018, TIV.NET INC. All Rights Reserved.
 */

namespace WOOMC\MultiCurrency\Integration;

use WOOMC\IHookable;
use WOOMC\MultiCurrency\Currency\Detector;

/**
 * Class WPSuperCache
 *
 * @package WOOMC\MultiCurrency\Integration
 */
class WPSuperCache implements IHookable {

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {
		do_action( 'wpsc_add_cookie', Detector::COOKIE_FORCED_CURRENCY );
	}
}
