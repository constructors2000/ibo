<?php
/**
 * Status Report.
 *
 * @since   1.13.0
 * @package WOOMC\MultiCurrency\Admin
 */

namespace WOOMC\MultiCurrency\Admin;

use WOOMC\IHookable;
use WOOMC\MultiCurrency\DAO\WP;

/**
 * Class StatusReport
 */
class StatusReport implements IHookable {

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {
		add_action( 'woocommerce_system_status_report', array( $this, 'action__woocommerce_system_status_report' ) );
	}

	/**
	 * Render the report.
	 *
	 * @internal action.
	 */
	public function action__woocommerce_system_status_report() {

		/** @global \wpdb $wpdb */
		global $wpdb;

		// Retrieve all our options except for the credentials.
		$options = $wpdb->get_results( $wpdb->prepare( "SELECT option_name, option_value FROM $wpdb->options WHERE option_name LIKE %s AND option_name NOT LIKE %s ORDER BY option_name", WP::OPTIONS_PREFIX . '%', '%credentials%' ), OBJECT );

		?>
		<table class="wc_status_table widefat" cellspacing="0">
			<thead>
			<tr>
				<th colspan="3" data-export-label="Multi-currency">
					<h2><?php esc_html_e( 'Multi-currency', 'woocommerce-multicurrency' ); ?></h2></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ( $options as $option ) : ?>
				<?php
				$option_name  = $option->option_name;
				$option_value = trim( $option->option_value );

				// Replace the long list of rates with just count.
				if ( 'woocommerce_multicurrency_rates' === $option_name ) {
					$option_value = maybe_unserialize( $option_value );
					if ( is_array( $option_value ) ) {
						$option_value = 'array(' . count( $option_value ) . ')';
					}
				}

				// Dash instead of empty strings.
				if ( '' === $option_value ) {
					$option_value = '-';
				}

				// Strip option prefix for brevity.
				$option_name = substr( $option_name, strlen( WP::OPTIONS_PREFIX ) );
				?>

				<tr>
					<td data-export-label="<?php echo \esc_attr( $option_name ); ?>"><?php echo \esc_html( $option_name ); ?></td>
					<td class="help">&nbsp;</td>
					<td><?php echo \esc_html( $option_value ); ?></td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		<?php
	}
}
