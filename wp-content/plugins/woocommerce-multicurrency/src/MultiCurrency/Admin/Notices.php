<?php
/**
 * Copyright (c) 2018. TIV.NET INC. All Rights Reserved.
 */

namespace WOOMC\MultiCurrency\Admin;

use WOOMC\MultiCurrency\App;

/**
 * Class Notices
 *
 * @since   1.10.0
 *
 * @package WOOMC\MultiCurrency\Admin
 */
class Notices {

	/**
	 * Requirements not met.
	 */
	public static function requirements() {
		?>
		<div class="error">
			<p>
				<?php
				printf( esc_html( __( '%1$s requires %2$s version 3+ to be installed and active.', 'woocommerce-multicurrency' ) ),
					\esc_html__( 'WooCommerce Multi-currency', 'woocommerce-multicurrency' ),
					'<a href="https://woocommerce.com" target="_blank">WooCommerce</a>'
				);
				?>
			</p>
		</div>
		<?php
	}

	/**
	 * Plugin activated.
	 *
	 * @param string $url_settings      The settings URL.
	 * @param string $url_documentation The documentation URL.
	 */
	public static function activation( $url_settings, $url_documentation ) {
		?>
		<div class="notice notice-info is-dismissible">
			<p>
				<?php
				echo \esc_html__( 'WooCommerce Multi-currency', 'woocommerce-multicurrency' )
				     . ' ' . \esc_html__( 'installed', 'woocommerce-multicurrency' ) . '.';
				?>
			</p>
			<p>
				<?php if ( App::requirements_met() ) : ?>
					<a href="<?php echo \esc_url( $url_settings ); ?>" class="button button-primary">
						<?php \esc_html_e( 'Settings' ); ?>
					</a>
				<?php endif; ?>
				<a href="<?php echo \esc_url( $url_documentation ); ?>" class="button">
					<?php \esc_html_e( 'Documentation', 'woocommerce-multicurrency' ); ?>
				</a>
			</p>
		</div>
		<?php
	}
}
