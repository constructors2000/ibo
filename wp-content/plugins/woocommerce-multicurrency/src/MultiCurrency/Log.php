<?php
/**
 * WC_Logger wrapper class.
 *
 * @since 1.13.0
 * Copyright (c) 2019. TIV.NET INC. All Rights Reserved.
 */

namespace WOOMC\MultiCurrency;

/**
 * Class Log
 *
 * @package WOOMC\MultiCurrency
 */
class Log {

	/**
	 * Add a log entry by calling @see \WC_Logger::log().
	 *
	 * @param string $level   One of the following:
	 *                        'emergency': System is unusable.
	 *                        'alert': Action must be taken immediately.
	 *                        'critical': Critical conditions.
	 *                        'error': Error conditions.
	 *                        'warning': Warning conditions.
	 *                        'notice': Normal but significant condition.
	 *                        'info': Informational messages.
	 *                        'debug': Debug-level messages.
	 * @param string $message Log message.
	 */
	public static function log( $level, $message ) {
		static $context = array( 'source' => 'WooCommerce-Multicurrency' );

		$logger = wc_get_logger();
		$logger->log( $level, $message, $context );

	}

	/**
	 * Adds an error level message.
	 *
	 * @param string $message Message to log.
	 */
	public static function error( $message ) {
		self::log( \WC_Log_Levels::ERROR, $message );
	}
}
