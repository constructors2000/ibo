<?php
/**
 * User.php
 *
 * @package WOOMC\MultiCurrency
 * @since   1.4.0
 */

namespace WOOMC\MultiCurrency;

use WOOMC\IHookable;

/**
 * Class User
 */
class User extends \WC_Data implements IHookable {

	/**
	 * This is the name of this object type.
	 *
	 * @var string
	 */
	protected $object_type = 'multicurrency_user';

	/**
	 * Data array.
	 *
	 * @var array
	 */
	protected $data = array(
		'currency' => '',
		'country'  => '',
	);

	/**
	 * Key for storing data.
	 *
	 * @var string
	 */
	protected $storage_key = '';

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {
		if ( Env::on_front() ) {
			/**
			 * @since 1.12.0 Fix: Hooked to 'woocommerce_init'. Otherwise, WC()->countries is not initialized yet.
			 */
			add_action( 'woocommerce_init', array( $this, 'init' ), App::HOOK_PRIORITY_EARLY );
		}
	}

	/**
	 * Initialize the User.
	 *
	 * @internal
	 */
	public function init() {

		$this->storage_key = 'woocommerce_' . $this->object_type;

		// Try to retrieve the stored data.
		$this->retrieve();

		// If not retrieved, get the data and store.
		if ( ! $this->get_currency() ) {
			$this->geolocate();
			$this->store();
		}
	}

	/**
	 * Get user data by location.
	 * Actual if Geolocation is enabled in Woo settings.
	 *
	 * @return void
	 */
	protected function geolocate() {

		$location = wc_get_customer_default_location();
		if ( ! empty( $location['country'] ) ) {

			$this->set_country( $location['country'] );

			$country_obj = new Country( $location['country'] );

			$this->set_currency( $country_obj->getCurrency() );

		}

	}

	/**
	 * Store the user data (caching until browser is closed).
	 */
	protected function store() {
		if ( ! headers_sent() ) {
			setcookie( $this->storage_key, wp_json_encode( $this->data ), 0, COOKIEPATH, COOKIE_DOMAIN, is_ssl(), true );
		}
	}

	/**
	 * Retrieve the user data.
	 */
	protected function retrieve() {
		if ( ! empty( $_COOKIE[ $this->storage_key ] ) ) {
			/** @noinspection PhpComposerExtensionStubsInspection */
			$retrieved_data = (array) json_decode( wp_unslash( $_COOKIE[ $this->storage_key ] ), JSON_OBJECT_AS_ARRAY );
			foreach ( array_keys( $this->data ) as $key ) {
				if ( isset( $retrieved_data[ $key ] ) ) {
					$this->set_prop( $key, $retrieved_data[ $key ] );
				}
			}
		}
	}

	/*
	|--------------------------------------------------------------------------
	| Getters
	|--------------------------------------------------------------------------
	*/

	/**
	 * Get currency.
	 *
	 * @param  string $context What the value is for. Valid values are 'view' and 'edit'.
	 *
	 * @return string
	 */
	public function get_currency( $context = 'view' ) {
		return $this->get_prop( 'currency', $context );
	}

	/**
	 * Get country.
	 *
	 * @param  string $context What the value is for. Valid values are 'view' and 'edit'.
	 *
	 * @return string
	 */
	public function get_country( $context = 'view' ) {
		return $this->get_prop( 'country', $context );
	}

	/*
	|--------------------------------------------------------------------------
	| Setters
	|--------------------------------------------------------------------------
	*/

	/**
	 * Set currency.
	 *
	 * @param string $value Currency.
	 */
	public function set_currency( $value ) {
		$this->set_prop( 'currency', wc_clean( $value ) );
	}

	/**
	 * Set country.
	 *
	 * @param string $value Country.
	 */
	public function set_country( $value ) {
		$this->set_prop( 'country', wc_clean( $value ) );
	}
}
