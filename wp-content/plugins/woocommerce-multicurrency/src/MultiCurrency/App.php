<?php

namespace WOOMC\MultiCurrency;

use WOOMC\AApp;
use WOOMC\IHookable;
use WOOMC\MultiCurrency\Admin\EditCurrency;
use WOOMC\MultiCurrency\Admin\Notices;
use WOOMC\MultiCurrency\Admin\StatusReport;
use WOOMC\MultiCurrency\Currency\Detector;
use WOOMC\MultiCurrency\Currency\Selector\Shortcode;
use WOOMC\MultiCurrency\Currency\Selector\Widget;
use WOOMC\MultiCurrency\Price\Calculator;
use WOOMC\MultiCurrency\Price\Formatter;
use WOOMC\MultiCurrency\Price\Rounder;
use WOOMC\MultiCurrency\Rate\Storage;
use WOOMC\MultiCurrency\Settings\Migration;
use WOOMC\MultiCurrency\Settings\Panel;

/**
 * Class MultiCurrency\App
 *
 * @property string textdomain
 */
class App extends AApp implements IHookable {

	/**
	 * To display a message after activation.
	 *
	 * @var string
	 */
	const ACTIVATION_TRANSIENT = 'woocommerce-multicurrency-activated';

	/**
	 * @var bool
	 */
	protected $read_only_settings = false;

	/**
	 * @return bool
	 */
	public function isReadOnlySettings() {
		return $this->read_only_settings;
	}

	/**
	 * @param bool $read_only_settings
	 */
	public function setReadOnlySettings( $read_only_settings ) {
		$this->read_only_settings = (bool) $read_only_settings;
	}

	/**
	 * @var string[]
	 */
	protected $enabled_languages;

	/**
	 * @return string[]
	 */
	public function getEnabledLanguages() {
		return $this->enabled_languages;
	}

	/**
	 * @var string
	 */
	protected $language;

	/**
	 * @return string
	 */
	public function getLanguage() {
		return $this->language;
	}

	/**
	 * @return string[][]
	 */
	protected $en_language_name;

	/**
	 * @param string $language
	 *
	 * @return \string[][]
	 */
	public function getEnLanguageName( $language ) {
		return $this->en_language_name[ $language ];
	}

	/**
	 * @var string
	 */
	protected $url_support = 'https://woocommerce.com/my-account/tickets/';

	/**
	 * @var string
	 */
	protected $url_documentation = 'https://docs.woocommerce.com/document/multi-currency/';

	/**
	 * @return string
	 */
	public function getUrlSupport() {
		return $this->url_support;
	}

	/**
	 * @return string
	 */
	public function getUrlDocumentation() {
		return $this->url_documentation;
	}

	/**
	 * @var User
	 */
	protected $user;

	/**
	 * Getter.
	 *
	 * @return User
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * Private App constructor (Singleton).
	 */
	private function __construct() {
		/**
		 * Required by @see \WOOMC\AApp::load_translations().
		 */
		$this->textdomain = 'woocommerce-multicurrency';
	}

	/**
	 * Access to the lazy-loaded instance.
	 * Can be used to un-hook filters if necessary.
	 *
	 * @example
	 *      remove_action( 'woocommerce_product_get_price', array(
	 *          \WOOMC\MultiCurrency\App::instance()
	 *          'filter__calculate_price'
	 *      ), AApp::HOOK_PRIORITY_EARLY );
	 *
	 * @return App
	 */
	public static function instance() {
		static $o;

		return $o ?: $o = new self();
	}

	/**
	 * @inheritdoc
	 */
	public function setup_hooks() {
		$this->load_translations();

		\register_activation_hook( $this->plugin_file, array( $this, 'set_activation_transient' ) );
		\add_action( 'admin_notices', array( $this, 'display_activation_notice' ) );

		\add_action( 'plugins_loaded', array( $this, 'setup_hooks_after_plugins_loaded' ), self::HOOK_PRIORITY_LATE );
	}

	/**
	 * Set transient to flag that the plugin was just activated.
	 */
	public function set_activation_transient() {
		\set_transient( self::ACTIVATION_TRANSIENT, true );
	}

	/**
	 * Display the activation notice once.
	 */
	public function display_activation_notice() {

		if ( \get_transient( self::ACTIVATION_TRANSIENT ) ) {
			$url_settings = \add_query_arg(
				array(
					'page' => 'wc-settings',
					'tab'  => Panel::TAB_SLUG,
				),
				\admin_url( 'admin.php' )
			);

			Notices::activation( $url_settings, $this->getUrlDocumentation() );

			\delete_transient( self::ACTIVATION_TRANSIENT );
		}
	}

	/**
	 * Check the prerequisites.
	 *
	 * @since 1.10.0
	 *
	 * @return bool
	 */
	public static function requirements_met() {
		return class_exists( 'WooCommerce', false )
		       && version_compare( wc()->version, '3.0.0', '>=' );
	}

	/**
	 * This runs only after we checked the prerequisites.
	 *
	 * @internal filter.
	 */
	public function setup_hooks_after_plugins_loaded() {

		if ( ! self::requirements_met() ) {
			add_action( 'admin_notices', array( 'WOOMC\MultiCurrency\Admin\Notices', 'requirements' ) );

			return;
		}

		// This must be the first action. Do not do anything before it.
		$this->get_multilingual_config();

		$migration = new Migration();
		$migration->maybe_migrate();

		/**
		 * @since 1.9.0
		 */
		\do_action( 'woocommerce_multicurrency_before_loading' );

		/**
		 * @since 1.10.0 Moved to run after plugins loaded.
		 */
		\add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

		/**
		 * @since 1.9.0 Recalculate totals on "refresh fragments" AJAX.
		 *
		 * @since 1.10.0 Moved to run after plugins loaded.
		 * WC Subscriptions do the same on priority 1. Let us work later.
		 * However, we cannot run on priority >= 10, because @see \WC_AJAX::get_refreshed_fragments() dies there.
		 */
		\add_action( 'wc_ajax_get_refreshed_fragments', array(
			$this,
			'action__wc_ajax_get_refreshed_fragments',
		), 9 );

		/**
		 * @since 1.4.0
		 */
		$this->user = new User();
		$this->user->setup_hooks();

		$currency_detector = new Detector();
		$currency_detector->setup_hooks();

		$rate_storage = new Storage();
		$rate_storage->setup_hooks();

		$price_rounder = new Rounder();

		$price_calculator = new Calculator( $rate_storage, $price_rounder );

		$currency_controller = new Currency\Controller( $currency_detector );
		$currency_controller->setup_hooks();

		/**
		 * @since 1.5.0
		 */
		$decimals = new Currency\Decimals( $currency_detector );
		$decimals->setup_hooks();

		$price_controller = new Price\Controller( $price_calculator, $currency_detector );
		$price_controller->setup_hooks();

		$price_formatter = new Formatter();
		$price_formatter->setup_hooks();

		$currency_selector_shortcode = new Shortcode();
		$currency_selector_shortcode->setup_hooks();

		$currency_selector_widget = new Widget();
		$currency_selector_widget->setup_hooks();

		$settings = new Settings\Controller( $rate_storage );
		$settings->setup_hooks();

		/**
		 * @since 1.7.0
		 */
		$reports = new Reports\Controller();
		$reports->setup_hooks();

		if ( Env::in_wp_admin() ) {
			/**
			 * @since 1.8.0
			 */
			$edit_currency = new EditCurrency();
			$edit_currency->setup_hooks();

			/**
			 * @since 1.13.0
			 */
			$status_report = new StatusReport();
			$status_report->setup_hooks();
		} else {
			/**
			 * Integration.
			 * Plugin Name: WP Super Cache
			 * Version: 1.6.3+
			 * Plugin URI: https://wordpress.org/plugins/wp-super-cache/
			 *
			 * @since 1.8.0
			 */
			if ( function_exists( 'wpsc_add_cookie' ) ) {
				$integration_wp_super_cache = new Integration\WPSuperCache();
				$integration_wp_super_cache->setup_hooks();
			}
		}

		/**
		 * @since 1.9.0
		 */
		do_action( 'woocommerce_multicurrency_loaded' );
	}

	/**
	 * Multilingual support.
	 * Get WPGlobus language settings and other configuration options into the App instance.
	 */
	public function get_multilingual_config() {
		if ( class_exists( '\WPGlobus', false ) ) {
			/**
			 * WPGlobus is active. We'll use its language settings.
			 */
			/** @noinspection PhpUndefinedClassInspection */
			$this->enabled_languages = \WPGlobus::Config()->enabled_languages;
			/** @noinspection PhpUndefinedClassInspection */
			$this->language = \WPGlobus::Config()->language;
			/** @noinspection PhpUndefinedClassInspection */
			$this->en_language_name = \WPGlobus::Config()->en_language_name;
		} else {
			// No WPGlobus. Use the current site language.
			$admin_locale            = \get_option( 'WPLANG', 'en_US' );
			$this->enabled_languages = array( substr( $admin_locale, 0, 2 ) );
			$this->language          = $this->enabled_languages[0];
			$this->en_language_name  = array( $this->enabled_languages[0] => $admin_locale );
		}
	}

	/**
	 * Enqueue scripts.
	 *
	 * @since 1.1.2
	 */
	public function enqueue_scripts() {
		// We use wpCookies().
		\wp_enqueue_script( 'utils' );
	}

	/**
	 * Recalculate totals on "refresh fragments" AJAX.
	 * Without this, the mini-cart widget and the top bar cart show wrong totals when the currency is switched.
	 *
	 * Related: Currency/Selector/ShortcodeJS.php
	 *
	 * @internal action.
	 *
	 * @since    1.9.0
	 */
	public function action__wc_ajax_get_refreshed_fragments() {
		\wc()->cart->calculate_totals();
	}

}
