<?php

namespace WOOMC\MultiCurrency\DAO;

use WOOMC\MultiCurrency\App;
use WOOMC\MultiCurrency\Price\Rounder;

/**
 * Class WP
 */
class WP implements IDAO {

	/**
	 * Options table keys prefix.
	 *
	 * @var string
	 */
	const OPTIONS_PREFIX = 'woocommerce_multicurrency_';

	/** @var string[] */
	protected $language_to_currency;

	/** @var string */
	protected $default_currency;

	/** @var string[] */
	protected $currency_to_price_format;

	/**
	 * WP constructor.
	 */
	public function __construct() {

		// WC's default currency (from the Options, with no filtering).
		$this->setDefaultCurrency( get_option( 'woocommerce_currency', 'USD' ) );

		/**
		 * Language-currency pairs are set in WPGlobus admin panel.
		 *
		 * @see \WOOMC\MultiCurrency\Settings\Panel::_build_panel
		 */
		foreach ( App::instance()->getEnabledLanguages() as $language ) {
			$this->language_to_currency[ $language ] = get_option( $this->key_language_to_currency( $language ), '' );
		}

		foreach ( $this->getEnabledCurrencies() as $currency ) {
			$this->currency_to_price_format[ $currency ] = get_option( $this->key_price_format( $currency ), '' );
		}
	}

	/**
	 * Store any value.
	 *
	 * @see \update_option for the parameter descriptions.
	 *
	 * @param string    $key
	 * @param mixed     $value
	 * @param bool|null $autoload
	 *
	 * @return bool
	 */
	public function store( $key, $value, $autoload = null ) {
		return update_option( $key, $value, $autoload );
	}

	/**
	 * Retrieve the value by key.
	 *
	 * @see \get_option for the parameter descriptions.
	 *
	 * @param string $key
	 * @param mixed  $default_value
	 *
	 * @return mixed
	 */
	public function retrieve( $key, $default_value = false ) {
		return get_option( $key, $default_value );
	}

	/**
	 * This is not loaded at constructor because we need to get it again when the provider
	 * is changed in the Panel.
	 *
	 * @return string
	 */
	public function getRatesProviderCredentials() {
		return get_option( $this->key_rates_provider_credentials( $this->getRatesProviderID() ), '' );
	}

	/**
	 * @return string[]
	 */
	public function getLanguageToCurrency() {
		return $this->language_to_currency;
	}

	/**
	 * @return string
	 */
	public function getDefaultCurrency() {
		return $this->default_currency;
	}


	/**
	 * @param string $default_currency
	 */
	public function setDefaultCurrency( $default_currency ) {
		$this->default_currency = $default_currency;
	}

	/**
	 * @return string[]
	 */
	public function getCurrencyToPriceFormat() {
		return $this->currency_to_price_format;
	}

	/**
	 * @param string $language
	 *
	 * @return string
	 */
	public function key_language_to_currency( $language ) {
		return self::OPTIONS_PREFIX . 'currency_' . $language;
	}

	/**
	 * Options table key for the Rates Provider Credentials.
	 *
	 * @param string $provider_id
	 *
	 * @return string
	 */
	public function key_rates_provider_credentials( $provider_id ) {
		return self::OPTIONS_PREFIX . 'rates_credentials_' . $provider_id;
	}

	/**
	 * Options table key for the Rates Provider Service.
	 *
	 * @return string
	 */
	public function key_rates_provider_id() {
		return self::OPTIONS_PREFIX . 'rates_provider_id';
	}

	/**
	 * Options table keys for the Price Format.
	 *
	 * @param string $currency
	 *
	 * @return string
	 */
	public function key_price_format( $currency ) {
		return self::OPTIONS_PREFIX . 'price_format_' . $currency;
	}

	/**
	 * Options table key for the Enabled Currencies list.
	 *
	 * @return string
	 */
	public function key_enabled_currencies() {
		return self::OPTIONS_PREFIX . 'enabled_currencies';
	}

	/**
	 * Returns array of enabled currency codes.
	 *
	 * @return string[]
	 */
	public function getEnabledCurrencies() {
		return get_option( $this->key_enabled_currencies(), (array) $this->getDefaultCurrency() );
	}

	/**
	 * Get the Rates Provider Service ID.
	 *
	 * @return string
	 */
	public function getRatesProviderID() {
		return get_option( $this->key_rates_provider_id(), '' );
	}

	/**
	 * Get the Rates Timestamp.
	 *
	 * @return string
	 */
	public function getRatesTimestamp() {
		return get_option( $this->key_rates_timestamp(), '' );
	}

	/**
	 * Options table key for the Rates Timestamp.
	 *
	 * @return string
	 */
	public function key_rates_timestamp() {
		return self::OPTIONS_PREFIX . 'rates_timestamp';
	}

	/**
	 * Save the Rates Timestamp.
	 *
	 * @param string $timestamp
	 *
	 * @return void
	 */
	public function saveRatesTimestamp( $timestamp ) {
		update_option( $this->key_rates_timestamp(), $timestamp );
	}

	/**
	 * Options table keys for the Currency Symbol.
	 *
	 * @param string $currency
	 *
	 * @return string
	 */
	public function key_currency_symbol( $currency ) {
		return self::OPTIONS_PREFIX . 'currency_symbol_' . $currency;
	}

	/**
	 * @inheritdoc
	 */
	public function getCustomCurrencySymbol( $currency ) {
		return get_option( $this->key_currency_symbol( $currency ), '' );
	}

	/**
	 * Options table key.
	 *
	 * @return string
	 */
	public function key_round_to() {
		return self::OPTIONS_PREFIX . 'round_to';
	}

	/**
	 * Getter.
	 *
	 * @return float
	 */
	public function getRoundTo() {
		return get_option( $this->key_round_to(), Rounder::DEFAULT_ROUND_TO );
	}

	/**
	 * Options table key.
	 *
	 * @return string
	 */
	public function key_price_charm() {
		return self::OPTIONS_PREFIX . 'price_charm';
	}

	/**
	 * Getter.
	 *
	 * @return float
	 */
	public function getPriceCharm() {
		return get_option( $this->key_price_charm(), Rounder::DEFAULT_PRICE_CHARM );
	}

	/**
	 * Options table key.
	 *
	 * @return string
	 */
	public function key_fee_percent() {
		return self::OPTIONS_PREFIX . 'fee_percent';
	}

	/**
	 * Getter.
	 *
	 * @return float
	 */
	public function getFeePercent() {
		return get_option( $this->key_fee_percent(), Rounder::DEFAULT_FEE_PERCENT );
	}

	/**
	 * Options table key.
	 *
	 * @return string
	 */
	public function key_version_in_db() {
		return self::OPTIONS_PREFIX . 'version';
	}

	/**
	 * Getter.
	 *
	 * @return string
	 */
	public function getVersionInDB() {
		return get_option( $this->key_version_in_db(), '' );
	}

	/**
	 * Setter.
	 *
	 * @param string Version to set.
	 */
	public function setVersionInDB( $version ) {
		update_option( $this->key_version_in_db(), $version );
	}
}