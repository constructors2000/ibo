<?php

namespace WOOMC\MultiCurrency\DAO;

/**
 * Interface IDAO
 */
interface IDAO {

	/**
	 * Store any value.
	 *
	 * @param string $key
	 * @param mixed  $value
	 *
	 * @return bool
	 */
	public function store( $key, $value );

	/**
	 * Retrieve the value by key.
	 *
	 * @param string $key
	 * @param mixed  $default_value
	 *
	 * @return mixed
	 */
	public function retrieve( $key, $default_value = false );

	/**
	 * @return string[]
	 */
	public function getLanguageToCurrency();

	/**
	 * Returns array of enabled currency codes.
	 *
	 * @return string[]
	 */
	public function getEnabledCurrencies();

	/**
	 * @return string
	 */
	public function getDefaultCurrency();

	/**
	 * @param string $default_currency
	 */
	public function setDefaultCurrency( $default_currency );

	/**
	 * @return string
	 */
	public function getRatesProviderCredentials();

	/**
	 * @return string[]
	 */
	public function getCurrencyToPriceFormat();

	/**
	 * Get the Rates Provider Service ID.
	 *
	 * @return string
	 */
	public function getRatesProviderID();

	/**
	 * Get the Rates Timestamp.
	 *
	 * @return string
	 */
	public function getRatesTimestamp();

	/**
	 * Save the Rates Timestamp.
	 *
	 * @param string $timestamp
	 *
	 * @return void
	 */
	public function saveRatesTimestamp( $timestamp );

	/**
	 * @param string $language
	 *
	 * @return string
	 */
	public function key_language_to_currency( $language );

	/**
	 * Options table key for the Enabled Currencies list.
	 *
	 * @return string
	 */
	public function key_enabled_currencies();

	/**
	 * Options table key for the Rates Provider Service.
	 *
	 * @return string
	 */
	public function key_rates_provider_id();

	/**
	 * Options table key for the Rates Timestamp.
	 *
	 * @return string
	 */
	public function key_rates_timestamp();

	/**
	 * Options table key for the Rates Provider Credentials.
	 *
	 * @param string $provider_id
	 *
	 * @return string
	 */
	public function key_rates_provider_credentials( $provider_id );

	/**
	 * Options table keys for the Price Format.
	 *
	 * @param string $currency
	 *
	 * @return string
	 */
	public function key_price_format( $currency );

	/**
	 * Options table keys for the Currency Symbol.
	 *
	 * @param string $currency
	 *
	 * @return string
	 */
	public function key_currency_symbol( $currency );

	/**
	 * Get the Currency Symbol set in the settings tab.
	 *
	 * @param string $currency
	 *
	 * @return string
	 */
	public function getCustomCurrencySymbol( $currency );

	/**
	 * Options table key.
	 *
	 * @return string
	 */
	public function key_round_to();

	/**
	 * Getter.
	 *
	 * @return float
	 */
	public function getRoundTo();

	/**
	 * Options table key.
	 *
	 * @return string
	 */
	public function key_price_charm();

	/**
	 * Getter.
	 *
	 * @return float
	 */
	public function getPriceCharm();

	/**
	 * Options table key.
	 *
	 * @return string
	 */
	public function key_fee_percent();

	/**
	 * Getter.
	 *
	 * @return float
	 */
	public function getFeePercent();

	/**
	 * Options table key.
	 *
	 * @return string
	 */
	public function key_version_in_db();

	/**
	 * Getter.
	 *
	 * @return string
	 */
	public function getVersionInDB();

	/**
	 * Setter.
	 *
	 * @param string Version to set.
	 */
	public function setVersionInDB( $version );
}
