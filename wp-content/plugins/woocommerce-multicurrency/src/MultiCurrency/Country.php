<?php
/**
 * Country.php
 *
 * @package WOOMC\MultiCurrency
 * @since   1.4.0
 * Copyright (c) 2018. TIV.NET INC. All Rights Reserved.
 */

namespace WOOMC\MultiCurrency;

/**
 * Class Country
 *
 */
class Country {

	/**
	 * @var string[]
	 */
	protected static $currency_of_country = array(
		'AD' => 'EUR',
		'AE' => 'AED',
		'AF' => 'AFN',
		'AG' => 'XCD',
		'AI' => 'XCD',
		'AL' => 'ALL',
		'AM' => 'AMD',
		'AO' => 'AOA',
		'AR' => 'ARS',
		'AT' => 'EUR',
		'AU' => 'AUD',
		'AW' => 'AWG',
		'AZ' => 'AZN',
		'BA' => 'BAM',
		'BB' => 'BBD',
		'BD' => 'BDT',
		'BE' => 'EUR',
		'BF' => 'XOF',
		'BG' => 'BGN',
		'BH' => 'BHD',
		'BI' => 'BIF',
		'BJ' => 'XOF',
		'BM' => 'BMD',
		'BN' => 'BND',
		'BO' => 'BOB',
		'BQ' => 'USD',
		'BR' => 'BRL',
		'BS' => 'BSD',
		'BT' => 'BTN',
		'BW' => 'BWP',
		'BY' => 'BYN',
		'BZ' => 'BZD',
		'CA' => 'CAD',
		'CC' => 'AUD',
		'CF' => 'XAF',
		'CG' => 'CDF',
		'CH' => 'CHF',
		'CI' => 'XOF',
		'CK' => 'NZD',
		'CL' => 'CLP',
		'CM' => 'XAF',
		'CN' => 'CNY',
		'CO' => 'COP',
		'CR' => 'CRC',
		'CU' => 'CUP',
		'CV' => 'CVE',
		'CW' => 'ANG',
		'CY' => 'EUR',
		'CZ' => 'CZK',
		'DE' => 'EUR',
		'DJ' => 'DJF',
		'DK' => 'DKK',
		'DM' => 'XCD',
		'DO' => 'DOP',
		'DZ' => 'DZD',
		'EC' => 'USD',
		'EE' => 'EUR',
		'EG' => 'EGP',
		'ER' => 'ERN',
		'ES' => 'EUR',
		'ET' => 'ETB',
		'FI' => 'EUR',
		'FJ' => 'FJD',
		'FK' => 'FKP',
		'FM' => 'USD',
		'FO' => 'DKK',
		'FR' => 'EUR',
		'GA' => 'XAF',
		'GB' => 'GBP',
		'GD' => 'XCD',
		'GE' => 'GEL',
		'GG' => 'GBP',
		'GH' => 'GHS',
		'GI' => 'GIP',
		'GM' => 'GMD',
		'GN' => 'GNF',
		'GQ' => 'XAF',
		'GR' => 'EUR',
		'GS' => 'GBP',
		'GT' => 'GTQ',
		'GW' => 'XOF',
		'GY' => 'GYD',
		'HK' => 'HKD',
		'HN' => 'HNL',
		'HR' => 'HRK',
		'HT' => 'HTG',
		'HU' => 'HUF',
		'ID' => 'IDR',
		'IE' => 'EUR',
		'IL' => 'ILS',
		'IM' => 'GBP',
		'IN' => 'INR',
		'IO' => 'USD',
		'IQ' => 'IQD',
		'IR' => 'IRR',
		'IS' => 'ISK',
		'IT' => 'EUR',
		'JE' => 'JEP',
		'JM' => 'JMD',
		'JO' => 'JOD',
		'JP' => 'JPY',
		'KE' => 'KES',
		'KG' => 'KGS',
		'KH' => 'KHR',
		'KI' => 'AUD',
		'KM' => 'KMF',
		'KN' => 'XCD',
		'KP' => 'KPW',
		'KR' => 'KRW',
		'KW' => 'KWD',
		'KY' => 'KYD',
		'KZ' => 'KZT',
		'LA' => 'LAK',
		'LB' => 'LBP',
		'LC' => 'XCD',
		'LI' => 'CHF',
		'LK' => 'LKR',
		'LR' => 'LRD',
		'LS' => 'LSL',
		'LT' => 'EUR',
		'LU' => 'EUR',
		'LV' => 'EUR',
		'LY' => 'LYD',
		'MA' => 'MAD',
		'MC' => 'EUR',
		'MD' => 'MDL',
		'ME' => 'EUR',
		'MG' => 'MGA',
		'MH' => 'USD',
		'MK' => 'MKD',
		'ML' => 'XOF',
		'MM' => 'MMK',
		'MN' => 'MNT',
		'MO' => 'MOP',
		'MR' => 'MRO',
		'MS' => 'XCD',
		'MT' => 'EUR',
		'MU' => 'MUR',
		'MV' => 'MVR',
		'MW' => 'MWK',
		'MX' => 'MXN',
		'MY' => 'MYR',
		'MZ' => 'MZN',
		'NA' => 'NAD',
		'NC' => 'XPF',
		'NE' => 'XOF',
		'NG' => 'NGN',
		'NI' => 'NIO',
		'NL' => 'EUR',
		'NO' => 'NOK',
		'NP' => 'NPR',
		'NR' => 'AUD',
		'NU' => 'NZD',
		'NZ' => 'NZD',
		'OM' => 'OMR',
		'PA' => 'PAB',
		'PE' => 'PEN',
		'PF' => 'XPF',
		'PG' => 'PGK',
		'PH' => 'PHP',
		'PK' => 'PKR',
		'PL' => 'PLN',
		'PN' => 'NZD',
		'PS' => 'JOD',
		'PT' => 'EUR',
		'PW' => 'USD',
		'PY' => 'PYG',
		'QA' => 'QAR',
		'RO' => 'RON',
		'RS' => 'RSD',
		'RU' => 'RUB',
		'RW' => 'RWF',
		'SA' => 'SAR',
		'SB' => 'SBD',
		'SC' => 'SCR',
		'SD' => 'SDG',
		'SE' => 'SEK',
		'SG' => 'SGD',
		'SH' => 'SHP',
		'SI' => 'EUR',
		'SK' => 'EUR',
		'SL' => 'SLL',
		'SM' => 'EUR',
		'SN' => 'XOF',
		'SO' => 'SOS',
		'SR' => 'SRD',
		'SS' => 'SSP',
		'ST' => 'STD',
		'SV' => 'USD',
		'SX' => 'ANG',
		'SY' => 'SYP',
		'SZ' => 'SZL',
		'TC' => 'USD',
		'TD' => 'XAF',
		'TG' => 'XOF',
		'TH' => 'THB',
		'TJ' => 'TJS',
		'TM' => 'TMT',
		'TN' => 'TND',
		'TO' => 'TOP',
		'TP' => 'USD',
		'TR' => 'TRY',
		'TT' => 'TTD',
		'TV' => 'AUD',
		'TW' => 'TWD',
		'TZ' => 'TZS',
		'UA' => 'UAH',
		'UG' => 'UGX',
		'US' => 'USD',
		'UY' => 'UYU',
		'UZ' => 'UZS',
		'VA' => 'EUR',
		'VC' => 'XCD',
		'VE' => 'VEF',
		'VG' => 'USD',
		'VN' => 'VND',
		'VU' => 'VUV',
		'WF' => 'XPF',
		'WS' => 'WST',
		'XK' => 'EUR',
		'YE' => 'YER',
		'ZA' => 'ZAR',
		'ZM' => 'ZMW',
		'ZW' => 'ZAR',
	);

	/**
	 * @var string
	 */
	protected $alpha2 = '';

	/**
	 * @var string
	 */
	protected $currency = '';

	/**
	 * Getter.
	 *
	 * @return string
	 */
	public function getCurrency() {
		return $this->currency;
	}

	/**
	 * Country constructor.
	 *
	 * @param string $alpha2 Two-letter country code.
	 */
	public function __construct( $alpha2 ) {
		$this->alpha2 = $alpha2;

		if ( isset( self::$currency_of_country[ $this->alpha2 ] ) ) {
			$this->currency = self::$currency_of_country[ $this->alpha2 ];
		}
	}
}