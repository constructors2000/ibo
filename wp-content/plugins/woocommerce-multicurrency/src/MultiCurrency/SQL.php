<?php
/**
 * Copyright (c) 2018. TIV.NET INC. All Rights Reserved.
 */

namespace WOOMC\MultiCurrency;

class SQL {

	/**
	 * Builds the SQL IN(...) statement.
	 *
	 * @param mixed|array $items  List of items.
	 * @param string      $format printf format; default is '%s'.
	 *
	 * @return string
	 */
	public static function in( $items, $format = '%s' ) {
		/** @global \wpdb $wpdb */
		global $wpdb;

		$sql = '';

		$items = (array) $items;
		if ( count( $items ) ) {
			$template = implode( ', ', array_fill( 0, count( $items ), $format ) );
			$sql      = $wpdb->prepare( $template, $items ); // phpcs:ignore WordPress.WP.PreparedSQL
		}

		return $sql;
	}

}
