<?php

namespace WOOMC\MultiCurrency\Price;

use WOOMC\IHookable;
use WOOMC\MultiCurrency\App;
use WOOMC\MultiCurrency\Currency\Detector;
use WOOMC\MultiCurrency\Env;
use WOOMC\MultiCurrency\Integration\Shipping;
use WOOMC\MultiCurrency\Integration\Booking;
use WOOMC\MultiCurrency\Integration;

/**
 * Class Price\Controller
 */
class Controller implements IHookable {

	/** @var Calculator */
	protected $price_calculator;

	/** @var Detector */
	protected $currency_detector;

	/**
	 * Controller constructor.
	 *
	 * @param Calculator $price_calculator
	 * @param Detector   $currency_detector
	 */
	public function __construct( Calculator $price_calculator, Detector $currency_detector ) {
		$this->price_calculator  = $price_calculator;
		$this->currency_detector = $currency_detector;
	}

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {

		if ( ! Env::on_front() ) {
			return;
		}

		/**
		 * Simple and Variable product prices.
		 *
		 * @note The 'woocommerce_product_get_price' tag is not here
		 * because it needs to be hooked differently if some 3rd party plugins are active.
		 *
		 */
		static $filter_tags = array(
			// 'woocommerce_product_get_price' -- Excluded intentionally @since 1.3.0.
			'woocommerce_product_get_regular_price',
			'woocommerce_product_get_sale_price',
			'woocommerce_product_variation_get_price',
			'woocommerce_product_variation_get_sale_price',
			'woocommerce_product_variation_get_regular_price',
		);

		/**
		 * @note  DO NOT MOVE.
		 * @todo  Initialize all integrations at the App level, not here (?).
		 * Integration.
		 * Plugin Name: WooCommerce Bookings.
		 * Plugin URI: https://woocommerce.com/products/woocommerce-bookings/
		 *
		 * @since 1.3.0
		 * @since 1.13.0 - Moved to a separate class.
		 */
		if ( class_exists( 'WC_Bookings', false ) ) {
			$bookings = new Booking\WCBookings( $this );
			$bookings->setup_hooks();
		}

		/**
		 * Integration.
		 * Plugin Name: WooCommerce Accommodation Bookings
		 * Plugin URI: https://woocommerce.com/products/woocommerce-accommodation-bookings/
		 *
		 * @since 1.13.0
		 */
		if ( class_exists( 'WC_Accommodation_Booking', false ) ) {
			$accommodation_booking = new Booking\WCAccommodationBooking( $this );
			$accommodation_booking->setup_hooks();
		}

		/**
		 * Integration with WooCommerce Subscriptions.
		 * Plugin URI: https://woocommerce.com/products/woocommerce-subscriptions/
		 *
		 * @since 1.3.0
		 */
		if ( class_exists( 'WC_Subscriptions', false ) ) {
			/**
			 * Additional tags to process by @see filter__calculate_price.
			 * These values are stored in metas, but the @see \WC_Data::get_meta hook
			 * is similar to @see \WC_Data::get_prop.
			 */
			$filter_tags[] = 'woocommerce_product_variation_get__subscription_price';
			/**
			 * Subscriptions sign-up fee.
			 *
			 * @since 1.9.0 - added at the product level, removed at variation level.
			 */
			$filter_tags[] = 'woocommerce_subscriptions_product_sign_up_fee';
			// $filter_tags[] = 'woocommerce_product_variation_get__subscription_sign_up_fee';
		}

		/**
		 * --- DO NOT MOVE THIS. MUST COME AFTER THE INTEGRATIONS! ---
		 * Some WC extensions need to convert product price conditionally.
		 */
		add_filter( 'woocommerce_product_get_price', array(
			$this,
			'filter__woocommerce_product_get_price',
		), App::HOOK_PRIORITY_EARLY, 2 );

		/**
		 * --- DO NOT MOVE THIS. MUST COME AFTER THE INTEGRATIONS! ---
		 *
		 * @param string[] $filter_tags
		 */
		$filter_tags = \apply_filters( 'woocommerce_multicurrency_get_props_filters', $filter_tags );

		foreach ( $filter_tags as $tag ) {
			add_filter( $tag, array( $this, 'filter__calculate_price' ), App::HOOK_PRIORITY_EARLY, 2 );
		}

		add_filter( 'woocommerce_variation_prices', array(
			$this,
			'filter__woocommerce_variation_prices',
		), App::HOOK_PRIORITY_EARLY, 3 );

		/**
		 * Special filter for the "WooCommerce Subscribe All The Things" (WCS_ATT) extension.
		 *
		 * @link  https://github.com/Prospress/woocommerce-subscribe-all-the-things
		 *
		 * WCS_ATT adds its own filters to the prices - @see \WCS_ATT_Product_Price_Filters::add,
		 * so our previously converted prices are ineffective when the subscription scheme prices
		 * are calculated. Thus, we convert the prices again, using this filter.
		 *
		 * @since 1.4.0
		 */
		if ( class_exists( 'WCS_ATT', false ) ) {
			add_filter( 'wcsatt_subscription_scheme_prices', array( $this, 'convert_array' ) );
		}

		/**
		 * --- DO NOT USE ---
		 *
		 * - woocommerce_get_price_excluding_tax
		 * - woocommerce_get_price_including_tax
		 * They usually come after the prices already calculated. Exception: Product Add-ons, see below.
		 *
		 * - raw_woocommerce_price
		 * This is a "strange" filter.
		 * Not sure how it can be used, because it affects every price,
		 * and does not tell, which one.
		 *
		 * - woocommerce_subscriptions_cart_get_price
		 * - woocommerce_variation_prices_price
		 * - woocommerce_variation_prices_regular_price
		 * - woocommerce_variation_prices_sale_price
		 * - woocommerce_variation_prices_sign_up_fee
		 * Covered by other hooks.
		 *
		 * Additional hooks are in @see WC_Product_Variable_Data_Store_CPT::read_price_data.
		 * Should not use those because the prices then stored in transients.
		 */

		/**
		 * @since 1.6.0
		 */
		$this->setup_shipping_hooks();

		/**
		 * Integration.
		 * Plugin Name: WooCommerce Product Add-ons.
		 * Plugin URI: https://woocommerce.com/products/product-add-ons/
		 *
		 * @since 1.6.0
		 */
		if ( class_exists( 'WC_Product_Addons', false ) ) {
			$product_addons = new Integration\WCProductAddons( $this );
			$product_addons->setup_hooks();
		}

		/**
		 * Integration.
		 * Plugin Name: WooCommerce USPS Shipping.
		 * Plugin URI: https://woocommerce.com/products/usps-shipping-method/
		 *
		 * @since 1.8.0
		 */
		if ( class_exists( 'WC_USPS', false ) ) {
			$usps = new Shipping\USPS( $this );
			$usps->setup_hooks();
		}

		/**
		 * Integration.
		 * Plugin Name: WooCommerce Royal Mail
		 * Plugin URI: https://woocommerce.com/products/royal-mail/
		 *
		 * @since 1.9.0
		 */
		if ( class_exists( 'WC_RoyalMail', false ) ) {
			$royal_mail = new Shipping\RoyalMail( $this );
			$royal_mail->setup_hooks();
		}

		/**
		 * Integration.
		 * Plugin Name: WooCommerce Table Rate Shipping
		 * Plugin URI: https://woocommerce.com/products/table-rate-shipping/
		 *
		 * @since 1.8.0
		 */
		if ( class_exists( 'WC_Table_Rate_Shipping', false ) ) {
			$table_rate = new Shipping\TableRate( $this );
			$table_rate->setup_hooks();
		}

		/**
		 * Integration.
		 * Plugin Name: WooCommerce Canada Post Shipping
		 * Plugin URI: https://woocommerce.com/products/canada-post-shipping-method/
		 *
		 * @since 1.9.0
		 */
		if ( class_exists( 'WC_Shipping_Canada_Post_Init', false ) ) {
			$canada_post = new Shipping\CanadaPost( $this );
			$canada_post->setup_hooks();
		}

		/**
		 * Integration.
		 * Plugin Name: WooCommerce Australia Post Shipping
		 * Plugin URI: https://woocommerce.com/products/australia-post-shipping-method/
		 *
		 * @since 1.9.0
		 */
		if ( class_exists( 'WC_Shipping_Australia_Post_Init', false ) ) {
			$canada_post = new Shipping\AustraliaPost( $this );
			$canada_post->setup_hooks();
		}

		/**
		 * Integration.
		 * Plugin Name: WooCommerce FedEx Shipping
		 * Plugin URI: https://woocommerce.com/products/fedex-shipping-module/
		 *
		 * @since 1.9.0
		 */
		if ( class_exists( 'WC_Shipping_Fedex_Init', false ) ) {
			$fedex = new Shipping\FedEx( $this );
			$fedex->setup_hooks();
		}

		/**
		 * Integration.
		 * Plugin Name: WooCommerce UPS Shipping
		 * Plugin URI: https://woocommerce.com/products/ups-shipping-method/
		 *
		 * @since 1.9.0
		 */
		if ( class_exists( 'WC_Shipping_UPS_Init', false ) ) {
			$ups = new Shipping\UPS( $this );
			$ups->setup_hooks();
		}

		/**
		 * Integration.
		 * Plugin Name: WooCommerce PostNL
		 * Plugin URI: http://www.woocommerce.com/products/woocommerce-shipping-postnl/
		 *
		 * @since 1.9.0
		 */
		if ( class_exists( 'WC_PostNL', false ) ) {
			$ups = new Shipping\UPS( $this );
			$ups->setup_hooks();
		}

		/**
		 * Integration.
		 * Plugin Name: WooCommerce Name Your Price
		 * Plugin URI: https://woocommerce.com/products/name-your-price/
		 *
		 * @since 1.11.0
		 */
		if ( class_exists( 'WC_Name_Your_Price', false ) ) {
			$name_your_price = new Integration\WCNameYourPrice( $this );
			$name_your_price->setup_hooks();
		}

		/**
		 * @since 1.8.0
		 */
		add_action( 'woocommerce_coupon_loaded', array( $this, 'filter__woocommerce_coupon_loaded' ) );

		/**
		 * Integration.
		 * Plugin Name: WooCommerce Dynamic Pricing
		 * Plugin URI: https://woocommerce.com/products/dynamic-pricing/
		 *
		 * @since 1.10.0
		 */
		if ( class_exists( 'WC_Dynamic_Pricing', false ) ) {
			$dynamic_pricing = new Integration\WCDynamicPricing( $this );
			$dynamic_pricing->setup_hooks();
		}

		/**
		 * Integration.
		 * Plugin Name: WooCommerce Product Vendors
		 * Plugin URI: https://woocommerce.com/products/product-vendors/
		 *
		 * @since 1.12.0
		 */
		if ( class_exists( 'WC_Product_Vendors', false ) ) {
			$product_vendors = new Integration\WCProductVendors( $this );
			$product_vendors->setup_hooks();
		}

		/**
		 * Integration.
		 * Plugin Name: WooCommerce Checkout Add-Ons
		 * Plugin URI: https://woocommerce.com/products/woocommerce-checkout-add-ons/
		 *
		 * @since 1.13.0
		 */
		if ( class_exists( 'WC_Checkout_Add_Ons', false ) ) {
			$checkout_add_ons = new Integration\WCCheckoutAddOns( $this );
			$checkout_add_ons->setup_hooks();
		}

		// --- Convert the product data when adding to cart.
		// add_filter( 'woocommerce_add_cart_item', function ( $cart_item_data ) {
		// 	/** @var \WC_Product $product */
		// 	$product = $cart_item_data['data'];
		// 	$product->set_price( $this->convert( $product->get_price( 'edit' ) ) );
		//
		// 	return $cart_item_data;
		// } );

	}

	/**
	 * Setup shipping hooks.
	 *
	 * @since 1.6.0
	 *        1.8.0 - Hooked early.
	 */
	protected function setup_shipping_hooks() {

		/** @global \wpdb $wpdb */
		global $wpdb;

		// Find all shipping methods and their instances in the database.
		$methods = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}woocommerce_shipping_zone_methods" );
		// Add filters to get shipping method settings from the Options table.
		foreach ( $methods as $method ) {
			$option_name = sprintf( 'woocommerce_%s_%d_settings', $method->method_id, $method->instance_id );
			add_filter( 'option_' . $option_name, array(
				$this,
				'filter__shipping_method_costs',
			), App::HOOK_PRIORITY_EARLY );
		}

	}

	/**
	 * Convert every shipping setting that "looks like" cost or amount.
	 *
	 * @internal filter.
	 *
	 * @since    1.6.0
	 *           1.8.0 - added the "_fee" pattern (used by USPS and Table Rate).
	 *           1.9.0 - convert numbers extracted from string - for the settings with shortcodes such as `1 * [qty]`.
	 *
	 * @param array $settings Shipping method settings.
	 *
	 * @return array Settings with the amounts converted.
	 */
	public function filter__shipping_method_costs( $settings ) {

		// Filter out settings keys by regex.
		$metrics = preg_grep( '/cost|amount|_fee/', array_keys( $settings ) );

		foreach ( $metrics as $metric ) {
			if ( ! empty( $settings[ $metric ] ) ) {
				$settings[ $metric ] = preg_replace_callback(
					'/[\d.]+/',
					array( $this, 'filter__shipping_method_costs__callback' ),
					$settings[ $metric ]
				);
			}
		}

		return $settings;
	}

	/**
	 * Callback for @see filter__shipping_method_costs.
	 *
	 * @internal filter callback.
	 *
	 * @param array $matches Matches from preg_replace.
	 *
	 * @return float|int|string Converted value of the 1st match.
	 */
	public function filter__shipping_method_costs__callback( $matches ) {
		return $this->convert( $matches[0] );
	}

	/**
	 * Generic currency conversion filter.
	 *
	 * @internal filter.
	 *
	 * @param string|int|float $value   The price.
	 * @param \WC_Product|null $product The product object.
	 *
	 * @return string
	 */
	public function filter__calculate_price( $value, $product = null ) {
		return $this->convert( $value, $product );
	}

	/**
	 * Filter the `get_price` with 3rd party plugins integration.
	 *
	 * @internal filter.
	 *
	 * @since    1.3.0
	 *           1.13.0 Refactored to pre-filter. Thanks Kathy Darling (helgatheviking) for the suggestion.
	 *
	 * @param string|int|float     $value   The price.
	 * @param \WC_Product|int|null $product The product object or ID.
	 *
	 * @return string|int|float
	 */
	public function filter__woocommerce_product_get_price( $value, $product = null ) {

		/**
		 * Pre-filter to allow short-circuiting.
		 * If a non-false value comes out of the filter, it will be returned.
		 *
		 * @param false                $use_our_value Passed as "false". Can return the actual value.
		 * @param string|int|float     $value         The price.
		 * @param \WC_Product|null     $product       The product object.
		 *
		 * @var false|string|int|float $pre_value
		 */
		$pre_value = apply_filters( 'woocommerce_multicurrency_pre_product_get_price', false, $value, $product );
		if ( false !== $pre_value ) {
			return $pre_value;
		}

		return $this->convert( $value, $product );
	}

	/**
	 * Convert variation prices.
	 *
	 * @internal filter.
	 *
	 * @param string[][]  $transient_cached_prices_array The `$price_type => $values` array.
	 * @param \WC_Product $product                       The Product object.
	 * @param bool        $include_taxes                 Currently unused.
	 *
	 * @return mixed
	 */
	public function filter__woocommerce_variation_prices(
		$transient_cached_prices_array,
		/* @noinspection PhpUnusedParameterInspection */
		$product = null,
		/* @noinspection PhpUnusedParameterInspection */
		$include_taxes = false
	) {
		$currency         = $this->currency_detector->currency();
		$default_currency = $this->currency_detector->getDefaultCurrency();

		foreach ( $transient_cached_prices_array as $price_type => $values ) {
			foreach ( $values as $key => $value ) {
				if ( $value ) {
					$transient_cached_prices_array[ $price_type ][ $key ] = $this->price_calculator->calculate( $value, $currency, $default_currency );
				}
			}
		}

		return $transient_cached_prices_array;
	}

	/**
	 * Convert the fixed amount coupon data.
	 *
	 * @internal filter.
	 *
	 * @since    1.8.0
	 *
	 * @param \WC_Coupon $coupon
	 *
	 * @return \WC_Coupon
	 */
	public function filter__woocommerce_coupon_loaded( $coupon ) {

		$discount_type = $coupon->get_discount_type();
		if ( in_array( $discount_type, array(
			'fixed_cart',
			'fixed_product',
			'sign_up_fee',
			'recurring_fee',
			'renewal_fee',
			'renewal_cart',
			'booking_person',
		), true ) ) {

			$coupon->set_amount( $this->convert( $coupon->get_amount( 'edit' ) ) );
			$coupon->set_maximum_amount( $this->convert( $coupon->get_maximum_amount( 'edit' ) ) );
			$coupon->set_minimum_amount( $this->convert( $coupon->get_minimum_amount( 'edit' ) ) );
		}

		return $coupon;
	}

	/**
	 * The Converter.
	 *
	 * @since 1.3.0 is in a separate method.
	 *
	 * @param string|int|float $value   The price.
	 * @param \WC_Product      $product The Product object. Reserved for future use.
	 *
	 * @param string           $to      Currency convert to. Default is the currently selected. @since 1.10.1
	 * @param string           $from    Currency convert from. Default is store base. @since 1.10.1
	 * @param bool             $reverse If this is a reverse conversion. @since 1.12.1
	 *
	 * @return float|int|string
	 */
	public function convert(
		$value,
		/* @noinspection PhpUnusedParameterInspection */
		$product = null,
		$to = '',
		$from = '',
		$reverse = false
	) {
		if ( $value ) {
			$to   = $to ?: $this->currency_detector->currency();
			$from = $from ?: $this->currency_detector->getDefaultCurrency();

			$value = $this->price_calculator->calculate( $value, $to, $from, $reverse );
		}

		return $value;
	}

	/**
	 * @deprecated 1.12.1
	 * Use @see convert() with reverse=true.
	 *
	 * Convert back to the default currency.
	 *
	 * @note       The result might differ from the original amount because of rounding and adjustments.
	 *
	 * @since      1.8.0
	 *
	 * @param string|int|float $value   The amount.
	 * @param \WC_Product      $product The Product object. Reserved for future use.
	 *
	 * @return float|int|string
	 */
	public function convert_back(
		$value,
		/* @noinspection PhpUnusedParameterInspection */
		$product = null
	) {
		if ( $value ) {
			$currency         = $this->currency_detector->currency();
			$default_currency = $this->currency_detector->getDefaultCurrency();

			// The arguments are swapped here:
			$converted_value = $this->price_calculator->calculate( $value, $default_currency, $currency );
			$value           = $converted_value;
		}

		return $value;
	}

	/**
	 * Convert an array of prices.
	 *
	 * @example convert_array( ['price' => '10', 'sale_price' => 5] ) --> ['price' => '12.4', 'sale_price' => 6.2]
	 * @since   1.4.0
	 *
	 * @param array  $values
	 * @param string $to      Currency convert to. Default is the currently selected. @since 1.10.1
	 * @param string $from    Currency convert from. Default is store base. @since 1.10.1
	 * @param bool   $reverse If this is a reverse conversion. @since 1.12.1
	 *
	 * @return array
	 */
	public function convert_array( $values, $to = '', $from = '', $reverse = false ) {
		foreach ( $values as $key => $value ) {
			$values[ $key ] = $this->convert( $value, $to, $from, $reverse );
		}

		return $values;
	}

}
