<?php

namespace WOOMC\MultiCurrency\Price;

use WOOMC\MultiCurrency\DAO\Factory;


/**
 * Price\Rounder
 */
class Rounder {

	/**
	 * @var float
	 */
	const DEFAULT_ROUND_TO = 0.01;

	/**
	 * @var float
	 */
	const DEFAULT_PRICE_CHARM = 0.0;

	/**
	 * @var float
	 */
	const DEFAULT_FEE_PERCENT = 0.0;

	/**
	 * @var float[]
	 */
	protected $settings = array(
		'round_to'    => self::DEFAULT_ROUND_TO,
		'price_charm' => self::DEFAULT_PRICE_CHARM,
		'fee_percent' => self::DEFAULT_FEE_PERCENT,
	);

	/**
	 * Price\Rounder constructor.
	 */
	public function __construct() {

		$dao = Factory::getDao();
		$this->setRoundTo( $dao->getRoundTo() );
		$this->setPriceCharm( $dao->getPriceCharm() );
		$this->setFeePercent( $dao->getFeePercent() );
	}

	/**
	 * Discard invalid parameters and values lower than 1 cent (that also discards negative values).
	 *
	 * @param mixed $value
	 *
	 * @param float $fallback
	 *
	 * @return float
	 */
	protected function sanitize_setting_value( $value, $fallback = 0.0 ) {
		if ( ! is_numeric( $value ) || $value < 0.01 ) {
			$value = $fallback;
		}

		return (float) $value;

	}

	/**
	 * @return float
	 */
	public function getRoundTo() {
		return $this->settings['round_to'];
	}

	/**
	 * @param float|int $value
	 */
	public function setRoundTo( $value ) {
		$this->settings['round_to'] = $this->sanitize_setting_value( $value, 0.01 );
	}

	/**
	 * @return float
	 */
	public function getPriceCharm() {
		return $this->settings['price_charm'];
	}

	/**
	 * @param float|int $value
	 */
	public function setPriceCharm( $value ) {
		$this->settings['price_charm'] = $this->sanitize_setting_value( $value );
	}

	/**
	 * @return float
	 */
	public function getFeePercent() {
		return $this->settings['fee_percent'];
	}

	/**
	 * @param float|int $value
	 */
	public function setFeePercent( $value ) {
		$this->settings['fee_percent'] = $this->sanitize_setting_value( $value );
	}

	/**
	 * Round up a float value.
	 *
	 * @param float  $value
	 *
	 * @param string $currency
	 *
	 * @return float
	 */
	public function up( $value, $currency = '' ) {
		if ( ! is_numeric( $value ) ) {
			$value = 0.0;
		}

		// Do not touch negative values.
		if ( $value < 0 ) {
			return $value;
		}

		$value = (float) $value;

		$this->settings = \apply_filters( 'woocommerce_multicurrency_rounder_settings', $this->settings, $currency );

		$value *= ( 1.0 + $this->settings['fee_percent'] / 100.0 );

		if ( $this->settings['round_to'] > 0.01 ) {
			$value = ceil( $value / $this->settings['round_to'] ) * $this->settings['round_to'];
		}

		$value -= $this->settings['price_charm'];

		return round( $value, 2 );
	}

	/**
	 * Reverse to the @see up().
	 *
	 * @param float  $value
	 *
	 * @param string $currency
	 *
	 * @return float
	 */
	public function down( $value, $currency = '' ) {
		if ( ! is_numeric( $value ) ) {
			$value = 0.0;
		}

		// Do not touch negative values.
		if ( $value < 0 ) {
			return $value;
		}

		$value = (float) $value;

		$this->settings = \apply_filters( 'woocommerce_multicurrency_rounder_settings', $this->settings, $currency );

		// Un-charm.
		$value += $this->settings['price_charm'];

		if ( $this->settings['round_to'] > 0.01 ) {
			// Cannot restore the value before rounding, so make it down by half of the "round_to".
			$value -= $this->settings['round_to'] / 2;
		}

		// Un-fee.
		$value /= ( 1.0 + $this->settings['fee_percent'] / 100.0 );

		return round( $value, 2 );
	}
}
