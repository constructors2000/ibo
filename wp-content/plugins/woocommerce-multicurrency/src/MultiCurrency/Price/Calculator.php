<?php

namespace WOOMC\MultiCurrency\Price;

use WOOMC\MultiCurrency\Rate\Storage;


/**
 * Class Price\Calculator
 */
class Calculator {

	/** @var  Storage */
	protected $rate_storage;

	/** @var  Rounder */
	protected $price_rounder;

	/**
	 * Price\Calculator constructor.
	 *
	 * @param Storage $rate_storage
	 * @param Rounder $price_rounder
	 *
	 * @codeCoverageIgnore
	 */
	public function __construct( Storage $rate_storage, Rounder $price_rounder ) {
		$this->rate_storage  = $rate_storage;
		$this->price_rounder = $price_rounder;
	}

	/**
	 * Calculate the price for the currency specified.
	 *
	 * @param int|float|string $value
	 * @param string           $to
	 * @param string           $from
	 * @param bool             $reverse If this is a reverse conversion. @since 1.12.1
	 *
	 * @return int|float|string
	 */
	public function calculate( $value, $to, $from, $reverse = false ) {

		// Default return value (invalid parameters)
		if ( ! $value || ! is_numeric( $value ) ) {
			return 0.0;
		}
		if ( $to === $from ) {
			return $value;
		}


		if ( $reverse ) {
			// Undo price adjustments.
			$calculated = $this->price_rounder->down( $value, $to );
			// Convert.
			$calculated = (float) $calculated * $this->rate_storage->get_rate( $to, $from );
		} else {
			// Convert.
			$calculated = (float) $value * $this->rate_storage->get_rate( $to, $from );
			// Do price adjustments.
			$calculated = $this->price_rounder->up( $calculated, $to );
		}

		// Try to cast back to the input variable's type.
		if ( settype( $calculated, gettype( $value ) ) ) {
			return $calculated;
		}

		// Could not cast? Return the unchanged input. Probably, unreachable code.
		// @codeCoverageIgnoreStart
		return $value;
		// @codeCoverageIgnoreEnd
	}
}
