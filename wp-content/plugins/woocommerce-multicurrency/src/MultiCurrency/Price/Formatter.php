<?php

namespace WOOMC\MultiCurrency\Price;

use WOOMC\AApp;
use WOOMC\IHookable;
use WOOMC\MultiCurrency\DAO\Factory;

/**
 * Class Formatter
 */
class Formatter implements IHookable {

	/**
	 * Array of price formats per currency.
	 *
	 * @var string[]
	 */
	protected $currency_to_price_format;

	/**
	 * Price\Formatter constructor.
	 * @codeCoverageIgnore
	 */
	public function __construct() {
		$this->setCurrencyToPriceFormat( Factory::getDao()->getCurrencyToPriceFormat() );
	}

	/**
	 * Setter.
	 *
	 * @param string[] $currency_to_price_format
	 */
	public function setCurrencyToPriceFormat( $currency_to_price_format ) {
		$this->currency_to_price_format = $currency_to_price_format;
	}

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 * @codeCoverageIgnore
	 */
	public function setup_hooks() {
		add_filter( 'woocommerce_price_format', array(
			$this,
			'filter__woocommerce_price_format',
		), AApp::HOOK_PRIORITY_EARLY );
	}

	/**
	 * If we have a format for the current WC currency, return it.
	 *
	 * @param string $format
	 *
	 * @return string
	 */
	public function filter__woocommerce_price_format( $format ) {
		return $this->get_format( $this->get_woocommerce_currency() ) ?: $format;
	}

	/**
	 * If we have a format for this currency, return it.
	 *
	 * @param string $currency
	 *
	 * @return string
	 */
	protected function get_format( $currency ) {
		return empty( $this->currency_to_price_format[ $currency ] )
			? ''
			: $this->currency_to_price_format[ $currency ];
	}

	/**
	 * Wrapper for PHPUnit mocking.
	 *
	 * @return string
	 * @codeCoverageIgnore
	 */
	protected function get_woocommerce_currency() {
		return \get_woocommerce_currency();
	}
}
