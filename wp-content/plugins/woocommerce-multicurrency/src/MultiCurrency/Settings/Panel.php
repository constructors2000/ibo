<?php

namespace WOOMC\MultiCurrency\Settings;

use WOOMC\IHookable;
use WOOMC\MultiCurrency\App;

/**
 * Class Panel
 */
class Panel implements IHookable {

	/**
	 * @var string
	 */
	const TAB_SLUG = 'multicurrency';

	/** @var Fields */
	protected $fields;

	/**
	 * Panel constructor.
	 *
	 * @param Fields $fields
	 */
	public function __construct( Fields $fields ) {
		$this->fields = $fields;
	}

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {
		add_filter( 'woocommerce_settings_tabs_array', array( $this, 'add_settings_tab' ), 50 );
		add_action( 'woocommerce_settings_' . self::TAB_SLUG, array( $this, 'output_fields' ) );

		if ( ! App::instance()->isReadOnlySettings() ) {
			add_action( 'woocommerce_update_options_' . self::TAB_SLUG, array( $this, 'save_fields' ) );
		}

	}

	/**
	 * Add our tab to the WooCommerce Settings.
	 *
	 * @param array $settings_tabs Existing tabs.
	 *
	 * @return array Tabs with ours added.
	 */
	public static function add_settings_tab( array $settings_tabs ) {
		$settings_tabs[ self::TAB_SLUG ] = _x( 'Multi-currency', 'Settings tab title', 'woocommerce-multicurrency' );

		return $settings_tabs;
	}

	/**
	 * Display fields on our tab.
	 */
	public function output_fields() {
		\WC_Admin_Settings::output_fields( $this->fields->get_all() );
		$this->fields->js_show_hide_credentials();
		$this->fields->js_rounding_calculator();
		if ( App::instance()->isReadOnlySettings() ) {
			$this->fields->js_disable_save();
		}
	}

	/**
	 * Update the settings.
	 */
	public function save_fields() {
		\WC_Admin_Settings::save_fields( $this->fields->get_all() );
	}
}
