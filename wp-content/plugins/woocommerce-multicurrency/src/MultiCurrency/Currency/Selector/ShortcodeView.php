<?php

namespace WOOMC\MultiCurrency\Currency\Selector;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * File: ShortcodeView.php
 *
 * @global string[] $currencies
 * @global string[] $woocommerce_currencies
 * @global string   $current_currency
 * @global string   $format
 */
// @formatter:off
?>
<select title="<?php esc_attr_e( 'Currency selector drop-down', 'woocommerce-multicurrency' ); ?>" class="<?php echo esc_attr( Shortcode::TAG ); ?>"><?php

	foreach ( $currencies as $code ) :

		?><option value="<?php echo esc_attr( $code ); ?>"<?php selected( $code, $current_currency ); ?>><?php

			echo esc_html(
				str_replace(
					array(
						'{{code}}',
						'{{name}}',
						'{{symbol}}',
					),
					array(
						$code,
						$woocommerce_currencies[ $code ],
						get_woocommerce_currency_symbol( $code ),
					),
					$format
				)
			);

			?></option><?php

	endforeach;

?></select><?php
