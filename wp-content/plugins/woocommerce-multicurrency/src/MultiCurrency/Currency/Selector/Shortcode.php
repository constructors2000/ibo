<?php
/**
 * Currency Selector Shortcode.
 * @example
 *  [TAG format="{{code}}: {{name}} ({{symbol}})"]
 */

namespace WOOMC\MultiCurrency\Currency\Selector;

use WOOMC\IHookable;
use WOOMC\MultiCurrency\DAO\Factory;

/**
 * Class Shortcode
 */
class Shortcode implements IHookable {

	/**
	 * Default format, if not passed.
	 *
	 * @var string
	 */
	const DEFAULT_FORMAT = '{{code}}: {{name}} ({{symbol}})';

	/**
	 * Shortcode tag.
	 *
	 * @var string
	 */
	const TAG = 'woocommerce-currency-selector';

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 * @codeCoverageIgnore
	 */
	public function setup_hooks() {
		add_shortcode( self::TAG, array( $this, 'process_shortcode' ) );
		add_action( 'wp_footer', function () {
			require __DIR__ . '/ShortcodeJS.php';
		} );
	}

	/**
	 * Process shortcode.
	 *
	 * @internal
	 *
	 * @param string[] $params
	 *
	 * @return string
	 */
	public function process_shortcode( $params ) {
		// Default if not passed.
		$params = shortcode_atts( array( 'format' => self::DEFAULT_FORMAT ), $params, self::TAG );

		// Vars for the View.
		/** @noinspection PhpUnusedLocalVariableInspection */
		$woocommerce_currencies = get_woocommerce_currencies();
		/** @noinspection PhpUnusedLocalVariableInspection */
		$currencies = Factory::getDao()->getEnabledCurrencies();
		/** @noinspection PhpUnusedLocalVariableInspection */
		$current_currency = get_woocommerce_currency();
		/** @noinspection PhpUnusedLocalVariableInspection */
		$format = apply_filters( 'woocommerce_multicurrency_shortcode_format', $params['format'] );

		ob_start();
		require __DIR__ . '/ShortcodeView.php';

		return ob_get_clean();
	}
}
