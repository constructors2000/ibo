<?php
/**
 * File: ShortcodeJS.php
 *
 * @since 1.0.0
 * @since 1.13.0 Added setting `COOKIE_FORCED_CURRENCY_CHANGED`
 */

use WOOMC\MultiCurrency\Currency\ChangeHandler;
use WOOMC\MultiCurrency\Currency\Detector;
use WOOMC\MultiCurrency\Currency\Selector\Shortcode;
use WOOMC\MultiCurrency\Env;

if ( defined( 'DOING_PHPUNIT' ) ) {
	return;
}

?>
<script>
    jQuery(function ($) {

        var name = "<?php echo esc_js( Detector::COOKIE_FORCED_CURRENCY ); ?>";
        var expires = <?php echo esc_js( YEAR_IN_SECONDS ); ?>;
        var path = "<?php echo esc_js( COOKIEPATH ); ?>";
        var domain = "<?php echo esc_js( COOKIE_DOMAIN ); ?>";
        var secure = <?php echo is_ssl() ? 'true' : 'false'; ?>;

        var currentURL = "<?php echo esc_js( remove_query_arg( Detector::GET_FORCED_CURRENCY, Env::current_url() ) ); ?>";

        $(".<?php echo esc_js( Shortcode::TAG ); ?>").on("change", function () {

            wpCookies.set(name, this.value, expires, path, domain, secure);

			<?php
			/**
			 * Trigger the "refresh fragments" AJAX to update the mini-cart widget and the top bar cart icon totals.
			 *
			 * Related: @see \WOOMC\MultiCurrency\App::action__wc_ajax_get_refreshed_fragments
			 *
			 * @since 1.9.0
			 */
			?>
            $(document.body).trigger("wc_fragment_refresh");

			<?php
			/**
			 * Set `window.location` instead of calling `reload`.
			 *
			 * - To strip the `?currency=XXX` query arg
			 * - To avoid multiple addition to the cart if we just added and still stay on that page.
			 *
			 * @since 1.9.0
			 */
			?>
            window.location = currentURL;
        });
    });
</script>
