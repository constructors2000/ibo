<?php

namespace WOOMC\MultiCurrency\Currency\Selector;

use WOOMC\IHookable;

/**
 * Class Widget
 */
class Widget extends \WP_Widget implements IHookable {

	/**
	 * Widget constructor.
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'                   => 'widget-' . Shortcode::TAG,
			'description'                 => _x( 'Drop-down currency selector.', 'Widget', 'woocommerce-multicurrency' ),
			'customize_selective_refresh' => true,
		);

		parent::__construct(
			Shortcode::TAG . '-widget',
			_x( 'Currency Selector', 'Widget', 'woocommerce-multicurrency' ),
			$widget_ops
		);
	}

	/**
	 * Widget front-end.
	 *
	 * @inheritdoc
	 */
	public function widget( $args, $instance ) {
		// Defaults.
		$instance = wp_parse_args( (array) $instance, array(
			'title'  => _x( 'Currency', 'Widget', 'woocommerce-multicurrency' ),
			'format' => Shortcode::DEFAULT_FORMAT,
		) );
		$title    = sanitize_text_field( $instance['title'] );
		$format   = sanitize_text_field( $instance['format'] );

		$title = apply_filters( 'widget_title', $title );

		// Before and after widget arguments are defined by themes.
		echo $args['before_widget']; // WPCS: XSS ok.
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title']; // WPCS: XSS ok.
		}

		// Run the code and display the output.
		echo do_shortcode( '[' . Shortcode::TAG . ' format="' . $format . '"]' );

		echo $args['after_widget']; // WPCS: XSS ok.
	}

	/**
	 * Widget back-end.
	 *
	 * @inheritdoc
	 */
	public function form( $instance ) {

		// Defaults.
		$instance = wp_parse_args( (array) $instance, array(
			'title'  => _x( 'Currency', 'Widget', 'woocommerce-multicurrency' ),
			'format' => Shortcode::DEFAULT_FORMAT,
		) );
		$title    = sanitize_text_field( $instance['title'] );
		$format   = sanitize_text_field( $instance['format'] );
		// Widget admin form
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:' ); ?></label>
			<br/>
			<!--suppress XmlDefaultAttributeValue -->
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
					name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text"
					value="<?php echo esc_attr( $title ); ?>"/>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'format' ) ); ?>"><?php echo esc_html_x( 'Display format:', 'Widget', 'woocommerce-multicurrency' ); ?></label>
			<br/>
			<?php
			/**
			 * The "format" input does not have `type="text"`.
			 * 1. WPGlobus does not add its Globe icon to translate this field.
			 * 2. WP does not show its content adjacent to the widget title (bug?).
			 */
			?>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'format' ) ); ?>"
					name="<?php echo esc_attr( $this->get_field_name( 'format' ) ); ?>"
					value="<?php echo esc_attr( $format ); ?>"/>
			<br/>
			<small><?php echo esc_html_x( 'Example:', 'Widget', 'woocommerce-multicurrency' ); ?><?php echo esc_attr( Shortcode::DEFAULT_FORMAT ); ?></small>
		</p>
		<?php
	}

	/**
	 * Updating widget replacing old instances with new.
	 *
	 * @inheritdoc
	 */
	public function update( $new_instance, $old_instance ) {
		$instance           = $old_instance;
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		$instance['format'] = ! empty( $new_instance['format'] ) ? sanitize_text_field( $new_instance['format'] ) : Shortcode::DEFAULT_FORMAT;

		return $instance;
	}

	/**
	 * Register and load the widget.
	 */
	public function register() {
		register_widget( __CLASS__ );
	}

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {
		add_action( 'widgets_init', array( $this, 'register' ) );
	}
}
