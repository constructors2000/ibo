<?php

namespace WOOMC\MultiCurrency\Currency;

use WOOMC\IHookable;
use WOOMC\MultiCurrency\App;
use WOOMC\MultiCurrency\DAO\Factory;
use WOOMC\MultiCurrency\Settings\Panel;

/**
 * Class Currency Controller
 */
class Controller implements IHookable {

	/** @var Detector */
	protected $currency_detector;

	/**
	 * Currency Controller constructor.
	 *
	 * @param Detector $currency_detector
	 */
	public function __construct( Detector $currency_detector ) {
		$this->currency_detector = $currency_detector;
	}

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {
		add_filter( 'woocommerce_currency_symbol', array(
			$this,
			'filter__woocommerce_currency_symbol',
		), App::HOOK_PRIORITY_EARLY, 2 );

		add_filter( 'woocommerce_currency', array(
			$this,
			'filter__woocommerce_currency',
		), App::HOOK_PRIORITY_EARLY, 0 );

	}

	/**
	 * Change the currency symbol to the one from our settings panel.
	 *
	 * @param string $currency_symbol
	 * @param string $currency
	 *
	 * @return string
	 */
	public function filter__woocommerce_currency_symbol( $currency_symbol, $currency ) {

		// Do not use this filter on our own settings tab.
		if ( isset( $_GET['page'], $_GET['tab'] ) // WPCS: CSRF ok.
		     && 'wc-settings' === $_GET['page'] // WPCS: CSRF ok.
		     && Panel::TAB_SLUG === $_GET['tab'] // WPCS: CSRF ok.
		     && is_admin()
		) {
			return $currency_symbol;
		}

		// If the symbol is set in our Settings tab, return it.
		$custom_symbol = Factory::getDao()->getCustomCurrencySymbol( $currency );
		if ( $custom_symbol ) {
			$currency_symbol = $custom_symbol;
		}

		return $currency_symbol;
	}

	/**
	 * Set active currency to the one returned by the @see Detector.
	 *
	 * @return string
	 */
	public function filter__woocommerce_currency() {

		return $this->currency_detector->get( App::instance()->getLanguage() );
	}
}
