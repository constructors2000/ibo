<?php
/**
 * Decimals.php
 *
 * @package WOOMC\MultiCurrency\Currency
 * @since   1.5.0
 * Copyright (c) 2018. TIV.NET INC. All Rights Reserved.
 */

namespace WOOMC\MultiCurrency\Currency;

use WOOMC\IHookable;
use WOOMC\MultiCurrency\App;
use WOOMC\MultiCurrency\Env;

/**
 * Class Decimals
 */
class Decimals implements IHookable {

	/** @var array */
	protected static $decimals_of_currency = array(
		'BIF' => 0,
		'CLP' => 0,
		'CVE' => 0,
		'DJF' => 0,
		'GNF' => 0,
		'ISK' => 0,
		'JPY' => 0,
		'KMF' => 0,
		'KRW' => 0,
		'PYG' => 0,
		'RWF' => 0,
		'UGX' => 0,
		'UYI' => 0,
		'VND' => 0,
		'VUV' => 0,
		'XAF' => 0,
		'XOF' => 0,
		'XPF' => 0,
		'BHD' => 3,
		'IQD' => 3,
		'JOD' => 3,
		'KWD' => 3,
		'LYD' => 3,
		'OMR' => 3,
		'TND' => 3,
		'CLF' => 4,
	);

	/** @var string */
	protected $currency;

	/**
	 * Constructor.
	 *
	 * @param Detector $currency_detector
	 */
	public function __construct( Detector $currency_detector ) {
		$this->currency = $currency_detector->currency();
	}

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {
		if ( ! Env::on_front() ) {
			return;
		}

		if ( isset( self::$decimals_of_currency[ $this->currency ] ) ) {
			add_filter( "pre_option_woocommerce_price_num_decimals", array(
				$this,
				'decimals_of_active_currency',
			), App::HOOK_PRIORITY_EARLY, 0 );
		}


	}

	/**
	 * Returns the number of decimals of the currently active currency.
	 *
	 * @internal because does not check for `isset`.
	 * @return int
	 */
	public function decimals_of_active_currency() {
		return self::$decimals_of_currency[ $this->currency ];
	}
}