<?php

namespace WOOMC\MultiCurrency\Currency;

use WOOMC\IHookable;
use WOOMC\MultiCurrency\App;
use WOOMC\MultiCurrency\DAO\Factory;
use WOOMC\MultiCurrency\DAO\IDAO;
use WOOMC\MultiCurrency\Env;

/**
 * Class Detector
 */
class Detector implements IHookable {

	/** @var string */
	const COOKIE_FORCED_CURRENCY = 'woocommerce_multicurrency_forced_currency';

	/** @var string */
	const GET_FORCED_CURRENCY = 'currency';

	/** @var string[] */
	protected $language_to_currency;

	/** @var string */
	protected $default_currency;

	/** @var string */
	protected $forced_currency;

	/**
	 * @return string
	 */
	public function getForcedCurrency() {
		return apply_filters( 'woocommerce_multicurrency_forced_currency', $this->forced_currency );
	}

	/**
	 * @param string $forced_currency
	 */
	public function setForcedCurrency( $forced_currency ) {
		$this->forced_currency = $forced_currency;
	}

	/**
	 * @return string
	 */
	public function getDefaultCurrency() {
		return $this->default_currency;
	}

	/**
	 * @param string $default_currency
	 */
	public function setDefaultCurrency( $default_currency ) {
		$this->default_currency = $default_currency;
	}

	/**
	 * @param string[] $language_to_currency
	 */
	public function setLanguageToCurrency( $language_to_currency ) {
		$this->language_to_currency = $language_to_currency;
	}

	/** @var  IDAO */
	protected $dao;

	/**
	 * Currency\Detector constructor.
	 */
	public function __construct() {

		$this->dao = Factory::getDao();

		$this->setLanguageToCurrency( $this->dao->getLanguageToCurrency() );

		$this->setDefaultCurrency( $this->dao->getDefaultCurrency() );

		$this->setForcedCurrency( $this->currency_from_url() );

	}

	/**
	 * Determine the currency settings by several criteria.
	 *
	 * @param string $language
	 *
	 * @return string
	 */
	public function get( $language ) {
		/**
		 * If in admin area, always return the default.
		 *
		 * @since 1.1.0
		 * @since 1.3.0 - Check also for AJAX from within the admin area.
		 */
		if ( Env::on_front() ) {

			/**
			 * 1. The currency is set ("forced") via the Widget / shortcode / URL.
			 */
			$forced_currency = $this->getForcedCurrency();
			if ( $forced_currency ) {
				return $forced_currency;
			}

			/**
			 * 2. The currency is linked to the language (on multilingual sites).
			 */
			if ( ! empty( $this->language_to_currency[ $language ] ) ) {
				return $this->language_to_currency[ $language ];
			}

			/**
			 * 3. The currency is defined by the user's location, if one of the enabled currencies.
			 *
			 * @since 1.4.0
			 */
			$currency_of_user = App::instance()->getUser()->get_currency();
			if ( $currency_of_user && in_array( $currency_of_user, $this->dao->getEnabledCurrencies(), true ) ) {
				return $currency_of_user;
			}
		}

		return $this->default_currency;
	}

	/**
	 * The current currency.
	 *
	 * @return string
	 */
	public function currency() {
		return $this->get( App::instance()->getLanguage() );
	}

	/**
	 * Setup actions and filters.
	 *
	 * @return void
	 */
	public function setup_hooks() {
		add_filter( 'woocommerce_multicurrency_forced_currency', array(
			$this,
			'filter__woocommerce_multicurrency_forced_currency',
		) );

		/**
		 * @since 1.9.0
		 */
		add_filter( 'woocommerce_multicurrency_active_currency', array( $this, 'currency' ) );
	}

	/**
	 * Set the forced currency value.
	 *
	 * @internal
	 *
	 * @return string
	 */
	public function filter__woocommerce_multicurrency_forced_currency() {
		return $this->currency_from_cookie();
		// return $this->currency_from_url() ?: $this->currency_from_cookie();
	}

	/**
	 * Get the forced currency value from cookie.
	 *
	 * @return string
	 */
	protected function currency_from_cookie() {
		$currency = '';
		if ( ! empty( $_COOKIE[ self::COOKIE_FORCED_CURRENCY ] ) ) {
			$currency = $_COOKIE[ self::COOKIE_FORCED_CURRENCY ];
		}

		return $currency;
	}

	/**
	 * Get the forced currency value from URL.
	 *
	 * @return string
	 * @since 1.9.0
	 */
	protected function currency_from_url() {
		// Prevent multiple setcookie() calls.
		static $cookie_set = false;

		$currency = '';
		if ( ! empty( $_GET[ self::GET_FORCED_CURRENCY ] ) ) { // phpcs:ignore WordPress.CSRF.NonceVerification
			$currency = strtoupper( $_GET[ self::GET_FORCED_CURRENCY ] ); // phpcs:ignore WordPress.CSRF.NonceVerification

			$enabled_currencies = Factory::getDao()->getEnabledCurrencies();

			if ( ! $cookie_set && ! headers_sent() && in_array( $currency, $enabled_currencies, true ) ) {
				setcookie( self::COOKIE_FORCED_CURRENCY, $currency, time() + YEAR_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN, is_ssl(), false );
				$cookie_set = true;

				$_COOKIE[ self::COOKIE_FORCED_CURRENCY ] = $currency;
			}
		}

		return $currency;
	}
}
