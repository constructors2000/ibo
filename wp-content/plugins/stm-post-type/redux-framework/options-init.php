<?php

/**
 * ReduxFramework Barebones Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 *
 * For a more extensive sample-config file, you may look at:
 * https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
 *
 */

stm_load_theme_options();

function stm_load_theme_options()
{

	if (!class_exists('Redux')) {
		return;
	}

	$opt_name = "stm_option";

	$default_colors = stm_default_colors();
	$layout = stm_get_layout();

	$args = array(
		'opt_name'              => 'stm_option',
		'display_name'          => 'MasterStudy',
		'page_title'            => __('Theme Options', 'masterstudy'),
		'menu_title'            => __('Theme Options', 'masterstudy'),
		'update_notice'         => false,
		'admin_bar'             => true,
		'dev_mode'              => false,
		'menu_icon'             => 'dashicons-hammer',
		'menu_type'             => 'menu',
		'allow_sub_menu'        => true,
		'page_parent_post_type' => '',
		'default_mark'          => '',
		'hints'                 => array(
			'icon_position' => 'right',
			'icon_size'     => 'normal',
			'tip_style'     => array(
				'color' => 'light',
			),
			'tip_position'  => array(
				'my' => 'top left',
				'at' => 'bottom right',
			),
			'tip_effect'    => array(
				'show' => array(
					'duration' => '500',
					'event'    => 'mouseover',
				),
				'hide' => array(
					'duration' => '500',
					'event'    => 'mouseleave unfocus',
				),
			),
		),
		'output'                => true,
		'output_tag'            => true,
		'compiler'              => true,
		'page_permissions'      => 'manage_options',
		'save_defaults'         => true,
		'database'              => 'options',
		'transient_time'        => '3600',
		'show_import_export'    => true,
		'network_sites'         => true
	);

	Redux::setArgs($opt_name, $args);

	/*
	 * ---> END ARGUMENTS
	 */


	/*
	 *
	 * ---> START SECTIONS
	 *
	 */

	Redux::setSection($opt_name, array(
		'title'   => __('General', 'masterstudy'),
		'desc'    => '',
		'submenu' => true,
		'fields'  => array(
			array(
				'id'       => 'logo',
				'url'      => false,
				'type'     => 'media',
				'title'    => __('Site Logo', 'masterstudy'),
				'default'  => array('url' => get_template_directory_uri() . '/assets/img/tmp/logo-colored.png'),
				'subtitle' => __('Upload your logo file here.', 'masterstudy'),
			),
			array(
				'id'       => 'logo_transparent',
				'url'      => false,
				'type'     => 'media',
				'title'    => __('White-text Logo', 'masterstudy'),
				'default'  => array('url' => get_template_directory_uri() . '/assets/img/tmp/logo_transparent.png'),
				'subtitle' => __('For our transparent header options, we need your logo to be in white to stand out.', 'masterstudy'),
			),
			array(
				'id'             => 'logo_text_font',
				'type'           => 'typography',
				'title'          => __('Logo Typography', 'masterstudy'),
				'compiler'       => true,
				'google'         => true,
				'font-backup'    => false,
				'font-weight'    => false,
				'all_styles'     => true,
				'font-style'     => false,
				'subsets'        => true,
				'font-size'      => true,
				'line-height'    => false,
				'word-spacing'   => false,
				'letter-spacing' => false,
				'color'          => true,
				'preview'        => true,
				'output'         => array('.logo-unit .logo'),
				'units'          => 'px',
				'subtitle'       => __('Select custom font for your logo (choose these parametrs if you want to display Blogname instead of logo image).', 'masterstudy'),
				'default'        => array(
					'color'       => "#fff",
					'font-family' => 'Montserrat',
					'font-size'   => '23px',
				)
			),
			array(
				'id'      => 'logo_width',
				'type'    => 'text',
				'title'   => __('Logo Width (px)', 'masterstudy'),
				'default' => '253'
			),
			array(
				'id'      => 'menu_top_margin',
				'type'    => 'text',
				'title'   => __('Menu margin top (px)', 'masterstudy'),
				'default' => '5'
			),
			array(
				'id'       => 'main_paddings',
				'type'     => 'spacing',
				'compiler' => true,
				'title'    => __('Main Content Paddings', 'masterstudy'),
				'default'  => '',
			),
			array(
				'id'      => 'preloader',
				'type'    => 'switch',
				'title'   => __('Enable preloader.', 'masterstudy'),
				'default' => false
			),
			array(
				'id'       => 'favicon',
				'url'      => false,
				'type'     => 'media',
				'title'    => __('Site Favicon', 'masterstudy'),
				'default'  => '',
				'subtitle' => __('Upload a 16px x 16px .png or .gif image here', 'masterstudy'),
			)
		)
	));

	Redux::setSection($opt_name, array(
		'title'   => __('Header', 'masterstudy'),
		'desc'    => '',
		'icon'    => 'el-icon-file',
		'submenu' => true,
		'fields'  => array(
			array(
				'id'       => 'header_style',
				'type'     => 'button_set',
				'title'    => __('Header Style Options', 'masterstudy'),
				'subtitle' => __('Select your header style option', 'masterstudy'),
				'options'  => array(
					'header_default' => __('Offline Courses', 'masterstudy'),
					'header_2'       => __('Online Courses', 'masterstudy'),
					'header_3'       => __('Academy', 'masterstudy'),
					'header_4'       => __('Logo Centered', 'masterstudy'),
				),
				'default'  => 'header_default'
			),
			array(
				'id'      => 'sticky_header',
				'type'    => 'switch',
				'title'   => __('Enable fixed header on scroll.', 'masterstudy'),
				'default' => false
			),

			/*Default Header Settings*/
			array(
				'id'       => 'default_show_wishlist',
				'type'     => 'switch',
				'title'    => __('Show LMS Wishlist', 'masterstudy'),
				'required' => array('header_style', '=', 'header_default',),
				'default'  => true
			),
			array(
				'id'       => 'default_show_search',
				'type'     => 'switch',
				'title'    => __('Show Search Icon', 'masterstudy'),
				'required' => array('header_style', '=', 'header_default',),
				'default'  => true
			),
			array(
				'id'       => 'default_show_socials',
				'type'     => 'switch',
				'title'    => __('Show Socials', 'masterstudy'),
				'required' => array('header_style', '=', 'header_default',),
				'default'  => true
			),

			/*Header 2 SETTINGS*/
			array(
				'id'       => 'online_show_wpml',
				'type'     => 'switch',
				'title'    => __('Show WPML Switcher', 'masterstudy'),
				'required' => array('header_style', '=', 'header_2',),
				'default'  => true
			),
			array(
				'id'       => 'online_show_socials',
				'type'     => 'switch',
				'title'    => __('Show Socials', 'masterstudy'),
				'required' => array('header_style', '=', 'header_2',),
				'default'  => true
			),
			array(
				'id'       => 'online_show_search',
				'type'     => 'switch',
				'title'    => __('Show Search', 'masterstudy'),
				'required' => array('header_style', '=', 'header_2',),
				'default'  => true
			),
			array(
				'id'       => 'online_show_links',
				'type'     => 'switch',
				'title'    => __('Show Popup links', 'masterstudy'),
				'required' => array('header_style', '=', 'header_2',),
				'default'  => true
			),
			/*Header 3 SETTINGS*/
			array(
				'id'      => 'header_course_categories',
				'type'    => 'select',
				'multi' => true,
				'data' => 'terms',
				'args' => array(
					'taxonomies' => array( 'stm_lms_course_taxonomy' ),
				),
				'title'   => __('Course Categories', 'masterstudy'),
				'required' => array('header_style', '=', 'header_3'),
				'default' => ''
			),
			array(
				'id'      => 'header_course_categories_online',
				'type'    => 'select',
				'multi' => true,
				'data' => 'terms',
				'args' => array(
					'taxonomies' => array( 'stm_lms_course_taxonomy' ),
				),
				'title'   => __('Course Categories', 'masterstudy'),
				'required' => array('header_style', '=', 'header_2'),
				'default' => ''
			),
			/*Header 4 SETTINGS*/
			array(
				'id'       => 'header_4_show_socials',
				'type'     => 'switch',
				'title'    => __('Show Socials', 'masterstudy'),
				'required' => array('header_style', '=', 'header_4',),
				'default'  => true
			),
			array(
				'id'       => 'header_4_phone',
				'type'     => 'text',
				'title'    => __('Phone number', 'masterstudy'),
				'required' => array('header_style', '=', 'header_4',),
			),
		)
	));

	Redux::setSection($opt_name, array(
		'title'   => __('Top Bar', 'masterstudy'),
		'desc'    => '',
		'icon'    => 'el el-website',
		'submenu' => true,
		'fields'  => array(
			array(
				'title'   => __('Enable Top Bar', 'masterstudy'),
				'id'      => 'top_bar',
				'type'    => 'switch',
				'default' => true
			),
			array(
				'id'       => 'top_bar_login',
				'type'     => 'switch',
				'title'    => __('Show login url', 'masterstudy'),
				'required' => array('top_bar', '=', true,),
				'default'  => true
			),
			array(
				'id'      => 'top_bar_social',
				'type'    => 'switch',
				'title'   => __('Enable Top Bar Social Media icons', 'masterstudy'),
				'default' => true
			),
			array(
				'id'      => 'top_bar_wpml',
				'type'    => 'switch',
				'title'   => __('Enable Top Bar WPML switcher(if WPML Plugin installed)', 'masterstudy'),
				'default' => true
			),
			array(
				'id'      => 'top_bar_color',
				'type'    => 'color',
				'title'   => __('Top Bar Background Color', 'masterstudy'),
				'default' => '#333333'
			),
			array(
				'id'             => 'font_top_bar',
				'type'           => 'typography',
				'title'          => __('Top Bar', 'masterstudy'),
				'compiler'       => true,
				'google'         => true,
				'font-backup'    => false,
				'font-weight'    => true,
				'all_styles'     => true,
				'font-style'     => true,
				'subsets'        => true,
				'font-size'      => true,
				'line-height'    => false,
				'word-spacing'   => false,
				'letter-spacing' => false,
				'color'          => true,
				'preview'        => true,
				'output'         => array('.header_top_bar, .header_top_bar a'),
				'units'          => 'px',
				'subtitle'       => __('Select custom font for your top bar text.', 'masterstudy'),
				'default'        => array(
					'color'       => "#aaaaaa",
					'font-family' => 'Montserrat',
					'font-size'   => '12px',
				)
			),
			array(
				'id'       => 'top_bar_use_social',
				'type'     => 'sortable',
				'mode'     => 'checkbox',
				'title'    => __('Select Social Media Icons to display', 'masterstudy'),
				'subtitle' => __('The urls for your social media icons will be taken from Social Media settings tab.', 'masterstudy'),
				'required' => array(
					array('top_bar_social', '=', true,)
				),
				'options'  => array(
					'facebook'     => 'Facebook',
					'twitter'      => 'Twitter',
					'instagram'    => 'Instagram',
					'behance'      => 'Behance',
					'dribbble'     => 'Dribbble',
					'flickr'       => 'Flickr',
					'git'          => 'Git',
					'linkedin'     => 'Linkedin',
					'pinterest'    => 'Pinterest',
					'yahoo'        => 'Yahoo',
					'delicious'    => 'Delicious',
					'dropbox'      => 'Dropbox',
					'reddit'       => 'Reddit',
					'soundcloud'   => 'Soundcloud',
					'google'       => 'Google',
					'google-plus'  => 'Google +',
					'skype'        => 'Skype',
					'youtube'      => 'Youtube',
					'youtube-play' => 'Youtube Play',
					'tumblr'       => 'Tumblr',
					'whatsapp'     => 'Whatsapp',
					'telegram'     => 'Telegram',
				),
				'default'  => array(
					'facebook'     => '0',
					'twitter'      => '0',
					'instagram'    => '0',
					'behance'      => '0',
					'dribbble'     => '0',
					'flickr'       => '0',
					'git'          => '0',
					'linkedin'     => '0',
					'pinterest'    => '0',
					'yahoo'        => '0',
					'delicious'    => '0',
					'dropbox'      => '0',
					'reddit'       => '0',
					'soundcloud'   => '0',
					'google'       => '0',
					'google-plus'  => '0',
					'skype'        => '0',
					'youtube'      => '0',
					'youtube-play' => '0',
					'tumblr'       => '0',
					'whatsapp'     => '0',
					'telegram'     => '0',
				),
			),
			array(
				'id'       => 'top_bar_address',
				'type'     => 'text',
				'title'    => __('Address', 'masterstudy'),
				'required' => array('top_bar', '=', true,),
				'default'  => __('1010 Moon ave, New York, NY US', 'masterstudy'),
			),
			array(
				'id'       => 'top_bar_address_mobile',
				'type'     => 'switch',
				'title'    => __('Show address on mobile', 'masterstudy'),
				'required' => array('top_bar', '=', true,),
			),
			array(
				'id'      => 'top_bar_working_hours',
				'type'    => 'text',
				'title'   => __('Working Hours', 'masterstudy'),
				'default' => __('Mon - Sat 8.00 - 18.00', 'masterstudy'),
			),
			array(
				'id'       => 'top_bar_working_hours_mobile',
				'type'     => 'switch',
				'title'    => __('Show Working hours on mobile', 'masterstudy'),
				'required' => array('top_bar', '=', true,),
			),
			array(
				'id'      => 'top_bar_phone',
				'type'    => 'text',
				'title'   => __('Phone number', 'masterstudy'),
				'default' => __('+1 212-226-3126', 'masterstudy'),
			),
			array(
				'id'       => 'top_bar_phone_mobile',
				'type'     => 'switch',
				'title'    => __('Show Phone on mobile', 'masterstudy'),
				'required' => array('top_bar', '=', true,),
			),
		)
	));

	Redux::setSection($opt_name, array(
		'title'   => __('Styling', 'masterstudy'),
		'desc'    => '',
		'icon'    => 'el-icon-tint',
		'submenu' => true,
		'fields'  => array(
			array(
				'id'      => 'color_skin',
				'type'    => 'button_set',
				'title'   => __('Color Skin', 'masterstudy'),
				'options' => array(
					''                  => __('Default', 'masterstudy'),
					'skin_custom_color' => __('Custom color', 'masterstudy'),
				),
				'default' => ''
			),
			array(
				'id'       => 'primary_color',
				'type'     => 'color',
				'compiler' => true,
				'title'    => __('Primary Color', 'masterstudy'),
				'default'  => $default_colors[$layout]['primary_color'],
				'required' => array('color_skin', '=', 'skin_custom_color'),
				'output'   => array(
					'background-color'    => '
body.skin_custom_color .post_list_main_section_wrapper .post_list_meta_unit .sticky_post,
body.skin_custom_color .overflowed_content .wpb_column .icon_box,
body.skin_custom_color .stm_countdown_bg,
body.skin_custom_color #searchform-mobile .search-wrapper .search-submit,
body.skin_custom_color .header-menu-mobile .header-menu > li .arrow.active,
body.skin_custom_color .header-menu-mobile .header-menu > li.opened > a,
body.skin_custom_color mark,
body.skin_custom_color .woocommerce .cart-totals_wrap .shipping-calculator-button:hover,
body.skin_custom_color .detailed_rating .detail_rating_unit tr td.bar .full_bar .bar_filler,
body.skin_custom_color .product_status.new,
body.skin_custom_color .stm_woo_helpbar .woocommerce-product-search input[type="submit"],
body.skin_custom_color .stm_archive_product_inner_unit .stm_archive_product_inner_unit_centered .stm_featured_product_price .price.price_free,
body.skin_custom_color .sidebar-area .widget:after,
body.skin_custom_color .sidebar-area .socials_widget_wrapper .widget_socials li .back a,
body.skin_custom_color .socials_widget_wrapper .widget_socials li .back a,
body.skin_custom_color .widget_categories ul li a:hover:after,
body.skin_custom_color .event_date_info_table .event_btn .btn-default,
body.skin_custom_color .course_table tr td.stm_badge .badge_unit.quiz,
body.skin_custom_color div.multiseparator:after,
body.skin_custom_color .page-links span:hover,
body.skin_custom_color .page-links span:after,
body.skin_custom_color .page-links > span:after,
body.skin_custom_color .page-links > span,
body.skin_custom_color .stm_post_unit:after,
body.skin_custom_color .blog_layout_grid .post_list_content_unit:after,
body.skin_custom_color ul.page-numbers > li a.page-numbers:after,
body.skin_custom_color ul.page-numbers > li span.page-numbers:after,
body.skin_custom_color ul.page-numbers > li a.page-numbers:hover,
body.skin_custom_color ul.page-numbers > li span.page-numbers:hover,
body.skin_custom_color ul.page-numbers > li a.page-numbers.current:after,
body.skin_custom_color ul.page-numbers > li span.page-numbers.current:after,
body.skin_custom_color ul.page-numbers > li a.page-numbers.current,
body.skin_custom_color ul.page-numbers > li span.page-numbers.current,
body.skin_custom_color .triangled_colored_separator,
body.skin_custom_color .magic_line,
body.skin_custom_color .navbar-toggle .icon-bar,
body.skin_custom_color .navbar-toggle:hover .icon-bar,
body.skin_custom_color #searchform .search-submit,
body.skin_custom_color .header_main_menu_wrapper .header-menu > li > ul.sub-menu:before,
body.skin_custom_color .search-toggler:after,
body.skin_custom_color .modal .popup_title,
body.skin_custom_color .widget_pages ul.style_2 li a:hover:after,
body.skin_custom_color .sticky_post,
body.skin_custom_color .btn-carousel-control:after,
.primary_bg_color,
.mbc,
.stm_lms_courses_carousel_wrapper .owl-dots .owl-dot.active,
.stm_lms_courses_carousel__term.active,
body.course_hub .header_default.header_2,
.triangled_colored_separator:before,
.triangled_colored_separator:after,
body.skin_custom_color.udemy .btn-default,
body.skin_custom_color.udemy .nav.nav-tabs>li.active a,
.single_instructor .stm_lms_courses .stm_lms_load_more_courses, 
.single_instructor .stm_lms_courses .stm_lms_load_more_courses:hover,
body.skin_custom_color.language-center .btn-default
',
					'border-color'        => '
body.skin_custom_color ul.page-numbers > li a.page-numbers:hover,
body.skin_custom_color ul.page-numbers > li a.page-numbers.current,
body.skin_custom_color ul.page-numbers > li span.page-numbers.current,
body.skin_custom_color .custom-border textarea:active, 
body.skin_custom_color .custom-border input[type=text]:active, 
body.skin_custom_color .custom-border input[type=email]:active, 
body.skin_custom_color .custom-border input[type=number]:active, 
body.skin_custom_color .custom-border input[type=password]:active, 
body.skin_custom_color .custom-border input[type=tel]:active,
body.skin_custom_color .custom-border .form-control:active,
body.skin_custom_color .custom-border textarea:focus, 
body.skin_custom_color .custom-border input[type=text]:focus, 
body.skin_custom_color .custom-border input[type=email]:focus, 
body.skin_custom_color .custom-border input[type=number]:focus, 
body.skin_custom_color .custom-border input[type=password]:focus, 
body.skin_custom_color .custom-border input[type=tel]:focus,
body.skin_custom_color .custom-border .form-control:focus,
body.skin_custom_color .icon-btn:hover .icon_in_btn,
body.skin_custom_color .icon-btn:hover,
body.skin_custom_color .average_rating_unit,
body.skin_custom_color blockquote,
body.skin_custom_color .tp-caption .icon-btn:hover .icon_in_btn,
body.skin_custom_color .tp-caption .icon-btn:hover,
body.skin_custom_color .stm_theme_wpb_video_wrapper .stm_video_preview:after,
body.skin_custom_color .btn-carousel-control,
body.skin_custom_color .post_list_main_section_wrapper .post_list_meta_unit .post_list_comment_num,
body.skin_custom_color .post_list_main_section_wrapper .post_list_meta_unit,
body.skin_custom_color .search-toggler:hover,
body.skin_custom_color .search-toggler,
.stm_lms_courses_carousel_wrapper .owl-dots .owl-dot.active,
.triangled_colored_separator .triangle:before
',
					'color'               => '
body.skin_custom_color .icon-btn:hover .icon_in_btn,
body.skin_custom_color .icon-btn:hover .link-title,
body.skin_custom_color .stats_counter .h1,
body.skin_custom_color .event_date_info .event_date_info_unit .event_labels,
body.skin_custom_color .event-col .event_archive_item .event_location i,
body.skin_custom_color .event-col .event_archive_item .event_start i,
body.skin_custom_color .gallery_terms_list li.active a,
body.skin_custom_color .tp-caption .icon-btn:hover .icon_in_btn,
body.skin_custom_color .widget_pages ul.style_2 li a:hover .h6,
body.skin_custom_color .teacher_single_product_page>a:hover .title,
body.skin_custom_color .sidebar-area .widget ul li a:hover:after,
body.skin_custom_color div.pp_woocommerce .pp_gallery ul li a:hover,
body.skin_custom_color div.pp_woocommerce .pp_gallery ul li.selected a,
body.skin_custom_color .single_product_after_title .meta-unit.teacher:hover .value,
body.skin_custom_color .single_product_after_title .meta-unit i,
body.skin_custom_color .single_product_after_title .meta-unit .value a:hover,
body.skin_custom_color .woocommerce-breadcrumb a:hover,
body.skin_custom_color #footer_copyright .copyright_text a:hover,
body.skin_custom_color .widget_stm_recent_posts .widget_media .cats_w a:hover,
body.skin_custom_color .widget_pages ul.style_2 li a:hover,
body.skin_custom_color .sidebar-area .widget_categories ul li a:hover,
body.skin_custom_color .sidebar-area .widget ul li a:hover,
body.skin_custom_color .widget_categories ul li a:hover,
body.skin_custom_color .stm_product_list_widget li a:hover .title,
body.skin_custom_color .widget_contacts ul li .text a:hover,
body.skin_custom_color .sidebar-area .widget_pages ul.style_1 li a:focus .h6,
body.skin_custom_color .sidebar-area .widget_nav_menu ul.style_1 li a:focus .h6,
body.skin_custom_color .sidebar-area .widget_pages ul.style_1 li a:focus,
body.skin_custom_color .sidebar-area .widget_nav_menu ul.style_1 li a:focus,
body.skin_custom_color .sidebar-area .widget_pages ul.style_1 li a:active .h6,
body.skin_custom_color .sidebar-area .widget_nav_menu ul.style_1 li a:active .h6,
body.skin_custom_color .sidebar-area .widget_pages ul.style_1 li a:active,
body.skin_custom_color .sidebar-area .widget_nav_menu ul.style_1 li a:active,
body.skin_custom_color .sidebar-area .widget_pages ul.style_1 li a:hover .h6,
body.skin_custom_color .sidebar-area .widget_nav_menu ul.style_1 li a:hover .h6,
body.skin_custom_color .sidebar-area .widget_pages ul.style_1 li a:hover,
body.skin_custom_color .sidebar-area .widget_nav_menu ul.style_1 li a:hover,
body.skin_custom_color .widget_pages ul.style_1 li a:focus .h6,
body.skin_custom_color .widget_nav_menu ul.style_1 li a:focus .h6,
body.skin_custom_color .widget_pages ul.style_1 li a:focus,
body.skin_custom_color .widget_nav_menu ul.style_1 li a:focus,
body.skin_custom_color .widget_pages ul.style_1 li a:active .h6,
body.skin_custom_color .widget_nav_menu ul.style_1 li a:active .h6,
body.skin_custom_color .widget_pages ul.style_1 li a:active,
body.skin_custom_color .widget_nav_menu ul.style_1 li a:active,
body.skin_custom_color .widget_pages ul.style_1 li a:hover .h6,
body.skin_custom_color .widget_nav_menu ul.style_1 li a:hover .h6,
body.skin_custom_color .widget_pages ul.style_1 li a:hover,
body.skin_custom_color .widget_nav_menu ul.style_1 li a:hover,
body.skin_custom_color .see_more a:after,
body.skin_custom_color .see_more a,
body.skin_custom_color .transparent_header_off .header_main_menu_wrapper ul > li > ul.sub-menu > li a:hover,
body.skin_custom_color .stm_breadcrumbs_unit .navxtBreads > span a:hover,
body.skin_custom_color .btn-carousel-control,
body.skin_custom_color .post_list_main_section_wrapper .post_list_meta_unit .post_list_comment_num,
body.skin_custom_color .post_list_main_section_wrapper .post_list_meta_unit .date-m,
body.skin_custom_color .post_list_main_section_wrapper .post_list_meta_unit .date-d,
body.skin_custom_color .stats_counter h1,
body.skin_custom_color .yellow,
body.skin_custom_color ol li a:hover,
body.skin_custom_color ul li a:hover,
body.skin_custom_color .search-toggler,
.primary_color,
.mtc_h:hover,
body.classic_lms .header_top_bar .header_top_bar_socs ul li a:hover,
body.classic_lms .header_top_bar a:hover,
#footer .widget_stm_lms_popular_courses ul li a:hover .meta .h5.title,
body.classic_lms .stm_lms_wishlist_button a:hover i,
.classic_lms .post_list_main_section_wrapper .post_list_item_title:hover,
.stm_lms_courses__single.style_2 .stm_lms_courses__single--title h5:hover
',
					'border-bottom-color' => '
body.skin_custom_color .triangled_colored_separator .triangle,
body.skin_custom_color .magic_line:after
',
				)
			),
			array(
				'id'       => 'secondary_color',
				'type'     => 'color',
				'compiler' => true,
				'title'    => __('Secondary Color', 'masterstudy'),
				'default'  => $default_colors[$layout]['secondary_color'],
				'required' => array('color_skin', '=', 'skin_custom_color'),
				'output'   => array(
					'background-color' => '
body.skin_custom_color .blog_layout_grid .post_list_meta_unit .sticky_post,
body.skin_custom_color .blog_layout_list .post_list_meta_unit .sticky_post,
body.skin_custom_color .product_status.special,
body.skin_custom_color .view_type_switcher a:hover,
body.skin_custom_color .view_type_switcher a.view_list.active_list,
body.skin_custom_color .view_type_switcher a.view_grid.active_grid,
body.skin_custom_color .stm_archive_product_inner_unit .stm_archive_product_inner_unit_centered .stm_featured_product_price .price,
body.skin_custom_color .sidebar-area .widget_text .btn,
body.skin_custom_color .stm_product_list_widget.widget_woo_stm_style_2 li a .meta .stm_featured_product_price .price,
body.skin_custom_color .widget_tag_cloud .tagcloud a:hover,
body.skin_custom_color .sidebar-area .widget ul li a:after,
body.skin_custom_color .sidebar-area .socials_widget_wrapper .widget_socials li a,
body.skin_custom_color .socials_widget_wrapper .widget_socials li a,
body.skin_custom_color .gallery_single_view .gallery_img a:after,
body.skin_custom_color .course_table tr td.stm_badge .badge_unit,
body.skin_custom_color .widget_mailchimp .stm_mailchimp_unit .button,
body.skin_custom_color .textwidget .btn:active,
body.skin_custom_color .textwidget .btn:focus,
body.skin_custom_color .form-submit .submit:active,
body.skin_custom_color .form-submit .submit:focus,
body.skin_custom_color .button:focus,
body.skin_custom_color .button:active,
body.skin_custom_color .btn-default:active,
body.skin_custom_color .btn-default:focus,
body.skin_custom_color .button:hover,
body.skin_custom_color .textwidget .btn:hover,
body.skin_custom_color .form-submit .submit,
body.skin_custom_color .button,
body.skin_custom_color .btn-default,
.btn.btn-default:hover, .button:hover, .textwidget .btn:hover,
body.skin_custom_color .short_separator,
.sbc,
.sbc_h:hover,
.wpb-js-composer .vc_general.vc_tta.vc_tta-tabs.vc_tta-style-classic li.vc_tta-tab>a,
.wpb-js-composer .vc_general.vc_tta.vc_tta-tabs.vc_tta-style-classic li.vc_tta-tab>a:hover,
#header.transparent_header .header_2 .stm_lms_account_dropdown .dropdown button,
.stm_lms_courses_categories.style_3 .stm_lms_courses_category>a:hover,
.stm_lms_udemy_course .nav.nav-tabs>li a,
body.classic_lms .classic_style .nav.nav-tabs>li.active a,
.header_bottom:after,
.sbc:hover
',
					'border-color'     => '
body.skin_custom_color .wpb_tabs .form-control:focus,
body.skin_custom_color .wpb_tabs .form-control:active,
body.skin_custom_color .woocommerce .cart-totals_wrap .shipping-calculator-button,
body.skin_custom_color .sidebar-area .widget_text .btn,
body.skin_custom_color .widget_tag_cloud .tagcloud a:hover,
body.skin_custom_color .icon_box.dark a:hover,
body.skin_custom_color .simple-carousel-bullets a.selected,
body.skin_custom_color .stm_sign_up_form .form-control:active,
body.skin_custom_color .stm_sign_up_form .form-control:focus,
body.skin_custom_color .form-submit .submit,
body.skin_custom_color .button,
body.skin_custom_color .btn-default,
.sbrc,
.sbrc_h:hover,
.vc_general.vc_tta.vc_tta-tabs,
body.skin_custom_color .blog_layout_grid .post_list_meta_unit,
body.skin_custom_color .blog_layout_grid .post_list_meta_unit .post_list_comment_num,
body.skin_custom_color .blog_layout_list .post_list_meta_unit .post_list_comment_num,
body.skin_custom_color .blog_layout_list .post_list_meta_unit,
#header.transparent_header .header_2 .stm_lms_account_dropdown .dropdown button
',
					'color'            => '
.header_2_top_bar__inner .top_bar_right_part .header_top_bar_socs ul li a:hover,
.secondary_color,
body.skin_custom_color .icon_box .icon_text>h3>span,
body.skin_custom_color .stm_woo_archive_view_type_list .stm_featured_product_stock i,
body.skin_custom_color .stm_woo_archive_view_type_list .expert_unit_link:hover .expert,
body.skin_custom_color .stm_archive_product_inner_unit .stm_archive_product_inner_unit_centered .stm_featured_product_body a .title:hover,
body.skin_custom_color .stm_product_list_widget.widget_woo_stm_style_2 li a:hover .title,
body.skin_custom_color .blog_layout_grid .post_list_meta_unit .post_list_comment_num,
body.skin_custom_color .blog_layout_grid .post_list_meta_unit .date-m,
body.skin_custom_color .blog_layout_grid .post_list_meta_unit .date-d,
body.skin_custom_color .blog_layout_list .post_list_meta_unit .post_list_comment_num,
body.skin_custom_color .blog_layout_list .post_list_meta_unit .date-m,
body.skin_custom_color .blog_layout_list .post_list_meta_unit .date-d,
body.skin_custom_color .widget_stm_recent_posts .widget_media a:hover .h6,
body.skin_custom_color .widget_product_search .woocommerce-product-search:after,
body.skin_custom_color .widget_search .search-form > label:after,
body.skin_custom_color .sidebar-area .widget ul li a,
body.skin_custom_color .sidebar-area .widget_categories ul li a,
body.skin_custom_color .widget_contacts ul li .text a,
body.skin_custom_color .event-col .event_archive_item > a:hover .title,
body.skin_custom_color .stm_contact_row a:hover,
body.skin_custom_color .comments-area .commentmetadata i,
body.skin_custom_color .stm_post_info .stm_post_details .comments_num .post_comments:hover,
body.skin_custom_color .stm_post_info .stm_post_details .comments_num .post_comments i,
body.skin_custom_color .stm_post_info .stm_post_details .post_meta li a:hover span,
body.skin_custom_color .stm_post_info .stm_post_details .post_meta li i,
body.skin_custom_color .blog_layout_list .post_list_item_tags .post_list_divider,
body.skin_custom_color .blog_layout_list .post_list_item_tags a,
body.skin_custom_color .blog_layout_list .post_list_cats .post_list_divider,
body.skin_custom_color .blog_layout_list .post_list_cats a,
body.skin_custom_color .blog_layout_list .post_list_item_title a:hover,
body.skin_custom_color .blog_layout_grid .post_list_item_tags .post_list_divider,
body.skin_custom_color .blog_layout_grid .post_list_item_tags a,
body.skin_custom_color .blog_layout_grid .post_list_cats .post_list_divider,
body.skin_custom_color .blog_layout_grid .post_list_cats a,
body.skin_custom_color .blog_layout_grid .post_list_item_title:focus,
body.skin_custom_color .blog_layout_grid .post_list_item_title:active,
body.skin_custom_color .blog_layout_grid .post_list_item_title:hover,
body.skin_custom_color .stm_featured_products_unit .stm_featured_product_single_unit .stm_featured_product_single_unit_centered .stm_featured_product_body a .title:hover,
body.skin_custom_color .icon_box.dark a:hover,
body.skin_custom_color .post_list_main_section_wrapper .post_list_item_tags .post_list_divider,
body.skin_custom_color .post_list_main_section_wrapper .post_list_item_tags a,
body.skin_custom_color .post_list_main_section_wrapper .post_list_cats .post_list_divider,
body.skin_custom_color .post_list_main_section_wrapper .post_list_cats a,
body.skin_custom_color .post_list_main_section_wrapper .post_list_item_title:active,
body.skin_custom_color .post_list_main_section_wrapper .post_list_item_title:focus,
body.skin_custom_color .post_list_main_section_wrapper .post_list_item_title:hover,
body.skin_custom_color a:hover,
.secondary_color,
#header.transparent_header .header_2 .header_top .stm_lms_categories .heading_font, 
#header.transparent_header .header_2 .header_top .stm_lms_categories i,
.classic_lms .post_list_main_section_wrapper .post_list_cats a,
.classic_lms .post_list_main_section_wrapper .post_list_item_tags a,
.stm_lms_courses__single__inner .stm_lms_courses__single--info_title a:hover h4
',
				)
			),
			array(
				'id'       => 'link_color',
				'type'     => 'color',
				'compiler' => true,
				'title'    => __('Link Color', 'masterstudy'),
				'default'  => $default_colors[$layout]['link_color'],
				'required' => array('color_skin', '=', 'skin_custom_color'),
				'output'   => array(
					'color' => 'a'
				)
			),
			array(
				'id'       => 'button_radius',
				'type'     => 'text',
				'compiler' => true,
				'title'    => __('Button Border Radius (px)', 'masterstudy'),
				'default'  => '0',
			),
			array(
				'id'       => 'button_dimensions',
				'type'     => 'spacing',
				'compiler' => true,
				'title'    => __('Button Paddings', 'masterstudy'),
				'default'  => '',
			),
		)
	));

	Redux::setSection($opt_name, array(
		'title'   => __('Sidebars', 'masterstudy'),
		'desc'    => '',
		'icon'    => 'el-icon-website',
		'submenu' => true,
		'fields'  => array(
			array(
				'id'      => 'blog_layout',
				'type'    => 'button_set',
				'options' => array(
					'grid' => __('Grid view', 'masterstudy'),
					'list' => __('List view', 'masterstudy')
				),
				'default' => 'grid',
				'title'   => __('Blog Layout', 'masterstudy')
			),
			array(
				'id'      => 'blog_sidebar',
				'type'    => 'select',
				'data'    => 'posts',
				'args'    => array('post_type' => array('sidebar'), 'posts_per_page' => -1),
				'title'   => __('Blog Sidebar', 'masterstudy'),
				'default' => '655'
			),
			array(
				'id'      => 'blog_sidebar_position',
				'type'    => 'button_set',
				'title'   => __('Blog Sidebar - Position', 'masterstudy'),
				'options' => array(
					'left'  => __('Left', 'masterstudy'),
					'none'  => __('No Sidebar', 'masterstudy'),
					'right' => __('Right', 'masterstudy')
				),
				'default' => 'right'
			),
		)
	));

	Redux::setSection($opt_name, array(
		'title'      => __('Teachers', 'masterstudy'),
		'desc'       => '',
		'icon'       => 'el-icon-user',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'    => 'teachers_sidebar',
				'type'  => 'select',
				'data'  => 'posts',
				'args'  => array('post_type' => array('sidebar'), 'posts_per_page' => -1),
				'title' => __('Teachers Sidebar', 'masterstudy'),
			),
			array(
				'id'      => 'teachers_sidebar_position',
				'type'    => 'button_set',
				'title'   => __('Teachers Sidebar - Position', 'masterstudy'),
				'options' => array(
					'left'  => __('Left', 'masterstudy'),
					'none'  => __('No Sidebar', 'masterstudy'),
					'right' => __('Right', 'masterstudy')
				),
				'default' => 'none'
			),
		)
	));

	Redux::setSection($opt_name, array(
		'title'      => __('Events', 'masterstudy'),
		'desc'       => '',
		'icon'       => 'el-icon-calendar',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'    => 'events_sidebar',
				'type'  => 'select',
				'data'  => 'posts',
				'args'  => array('post_type' => array('sidebar'), 'posts_per_page' => -1),
				'title' => __('Events Sidebar', 'masterstudy'),
			),
			array(
				'id'      => 'events_sidebar_position',
				'type'    => 'button_set',
				'title'   => __('Events Sidebar - Position', 'masterstudy'),
				'options' => array(
					'left'  => __('Left', 'masterstudy'),
					'none'  => __('No Sidebar', 'masterstudy'),
					'right' => __('Right', 'masterstudy')
				),
				'default' => 'none'
			),
		)
	));

	Redux::setSection($opt_name, array(
		'title'      => __('Gallery', 'masterstudy'),
		'desc'       => '',
		'icon'       => 'el-icon-picture',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'    => 'gallery_sidebar',
				'type'  => 'select',
				'data'  => 'posts',
				'args'  => array('post_type' => array('sidebar'), 'posts_per_page' => -1),
				'title' => __('Gallery Sidebar', 'masterstudy'),
			),
			array(
				'id'      => 'gallery_sidebar_position',
				'type'    => 'button_set',
				'title'   => __('Gallery Sidebar - Position', 'masterstudy'),
				'options' => array(
					'left'  => __('Left', 'masterstudy'),
					'none'  => __('No Sidebar', 'masterstudy'),
					'right' => __('Right', 'masterstudy')
				),
				'default' => 'none'
			),
		)
	));

	Redux::setSection($opt_name, array(
		'title'      => __('Shop', 'masterstudy'),
		'desc'       => '',
		'icon'       => 'el el-shopping-cart',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'      => 'shop_layout',
				'type'    => 'button_set',
				'options' => array(
					'grid' => __('Grid view', 'masterstudy'),
					'list' => __('List view', 'masterstudy')
				),
				'default' => 'grid',
				'title'   => __('Shop Layout', 'masterstudy')
			),
			array(
				'id'      => 'shop_sidebar',
				'type'    => 'select',
				'data'    => 'posts',
				'args'    => array('post_type' => array('sidebar'), 'posts_per_page' => -1),
				'title'   => __('Sidebar', 'masterstudy'),
				'default' => '740'
			),
			array(
				'id'      => 'shop_sidebar_position',
				'type'    => 'button_set',
				'title'   => __('Sidebar - Position', 'masterstudy'),
				'options' => array(
					'left'  => __('Left', 'masterstudy'),
					'right' => __('Right', 'masterstudy')
				),
				'default' => 'right'
			),
		)
	));

	Redux::setSection($opt_name, array(
		'title'   => __('Events', 'masterstudy'),
		'desc'    => '',
		'icon'    => 'el el-calendar',
		'submenu' => true,
		'fields'  => array(
			array(
				'id'    => 'paypal_email',
				'type'  => 'text',
				'title' => __('Paypal Email', 'masterstudy'),
			),
			array(
				'id'          => 'currency',
				'type'        => 'text',
				'title'       => __('Currency', 'masterstudy'),
				'default'     => __('USD', 'masterstudy'),
				'description' => __('Ex. USD', 'masterstudy'),
			),
			array(
				'id'      => 'paypal_mode',
				'type'    => 'select',
				'title'   => __('Paypal Mode', 'masterstudy'),
				'options' => array(
					'sand' => 'SandBox',
					'live' => 'Live',
				),
				'default' => 'sand',
			),
			array(
				'id'      => 'admin_subject',
				'type'    => 'text',
				'title'   => __('Admin Subject', 'masterstudy'),
				'default' => __('New Participant for [event]', 'masterstudy'),
			),
			array(
				'id'          => 'admin_message',
				'type'        => 'textarea',
				'title'       => __('Admin Message', 'masterstudy'),
				'default'     => __('A new member wants to join your [event]<br>Participant Info:<br>Name: [name]<br>Email: [email]<br>Phone: [phone]<br>Message: [message]', 'masterstudy'),
				'description' => __('Shortcodes Available - [name], [email], [phone], [message].', 'masterstudy')
			),
			array(
				'id'      => 'user_subject',
				'type'    => 'text',
				'title'   => __('User Subject', 'masterstudy'),
				'default' => __('Confirmation of your pariticipation in the [event]', 'masterstudy'),
			),
			array(
				'id'          => 'user_message',
				'type'        => 'textarea',
				'title'       => __('User Message', 'masterstudy'),
				'default'     => __('Dear [name].<br/> This email is sent to you to confirm your participation in the event.<br/>We will contact you soon with further details.<br>With any question, feel free to phone +999999999999 or write to <a href="mailto:timur@stylemix.net">timur@stylemix.net</a>.<br>Regards,<br>MasterStudy Team.', 'masterstudy'),
				'description' => __('Shortcodes Available - [name], [email], [phone], [message].', 'masterstudy')
			),
		)
	));

	Redux::setSection($opt_name, array(
		'title'   => __('Typography', 'masterstudy'),
		'icon'    => 'el-icon-font',
		'submenu' => true,
		'fields'  => array(
			array(
				'id'             => 'font_body',
				'type'           => 'typography',
				'title'          => __('Body', 'masterstudy'),
				'compiler'       => true,
				'google'         => true,
				'font-backup'    => false,
				'font-weight'    => false,
				'all_styles'     => true,
				'font-style'     => false,
				'subsets'        => true,
				'font-size'      => true,
				'line-height'    => false,
				'word-spacing'   => false,
				'letter-spacing' => false,
				'color'          => true,
				'preview'        => true,
				'output'         => array('body, .normal_font'),
				'units'          => 'px',
				'subtitle'       => __('Select custom font for your main body text.', 'masterstudy'),
				'default'        => array(
					'color'       => "#555555",
					'font-family' => 'Open Sans',
					'font-size'   => '14px',
				)
			),
			array(
				'id'             => 'font_btn',
				'type'           => 'typography',
				'title'          => __('Button', 'masterstudy'),
				'compiler'       => true,
				'google'         => true,
				'font-backup'    => false,
				'font-weight'    => false,
				'all_styles'     => false,
				'font-style'     => false,
				'subsets'        => false,
				'font-size'      => true,
				'line-height'    => true,
				'word-spacing'   => true,
				'letter-spacing' => true,
				'color'          => false,
				'preview'        => true,
				'output'         => array('.btn'),
				'units'          => 'px',
				'subtitle'       => __('Select custom font for your button.', 'masterstudy'),
				'default'        => array(
					'font-family' => 'Montserrat',
					'font-size'   => '14px',
				)
			),
			array(
				'id'             => 'menu_heading',
				'type'           => 'typography',
				'title'          => __('Menu', 'masterstudy'),
				'compiler'       => true,
				'google'         => true,
				'font-backup'    => false,
				'all_styles'     => true,
				'font-weight'    => true,
				'font-style'     => false,
				'subsets'        => true,
				'font-size'      => false,
				'line-height'    => false,
				'word-spacing'   => false,
				'letter-spacing' => false,
				'color'          => true,
				'preview'        => true,
				'output'         => array('.header-menu'),
				'units'          => 'px',
				'subtitle'       => __('Select custom font for menu', 'masterstudy'),
				'default'        => array(
					'color'       => "#fff",
					'font-family' => 'Montserrat',
					'font-weight' => '900',
				)
			),
			array(
				'id'             => 'font_heading',
				'type'           => 'typography',
				'title'          => __('Heading', 'masterstudy'),
				'compiler'       => true,
				'google'         => true,
				'font-backup'    => false,
				'all_styles'     => true,
				'font-weight'    => false,
				'font-style'     => false,
				'subsets'        => true,
				'font-size'      => false,
				'line-height'    => false,
				'word-spacing'   => false,
				'letter-spacing' => false,
				'color'          => true,
				'preview'        => true,
				'output'         => array('h1,.h1,h2,.h2,h3,.h3,h4,.h4,h5,.h5,h6,.h6,.nav-tabs>li>a,.heading_font,table,.widget_categories ul li a,.sidebar-area .widget ul li a,.select2-selection__rendered,blockquote,.select2-chosen,.vc_tta-tabs.vc_tta-tabs-position-top .vc_tta-tabs-container .vc_tta-tabs-list li.vc_tta-tab a,.vc_tta-tabs.vc_tta-tabs-position-left .vc_tta-tabs-container .vc_tta-tabs-list li.vc_tta-tab a'),
				'units'          => 'px',
				'subtitle'       => __('Select custom font for headings', 'masterstudy'),
				'default'        => array(
					'color'       => "#333333",
					'font-family' => 'Montserrat',
				)
			),
			array(
				'id'             => 'h1_params',
				'type'           => 'typography',
				'title'          => __('H1', 'masterstudy'),
				'compiler'       => true,
				'google'         => false,
				'font-backup'    => false,
				'all_styles'     => true,
				'font-weight'    => true,
				'font-family'    => false,
				'text-align'     => false,
				'font-style'     => false,
				'subsets'        => false,
				'font-size'      => true,
				'line-height'    => true,
				'word-spacing'   => true,
				'letter-spacing' => true,
				'color'          => false,
				'preview'        => true,
				'output'         => array('h1,.h1'),
				'units'          => 'px',
				'default'        => array(
					'font-size'   => '50px',
					'font-weight' => '700'
				)
			),
			array(
				'id'       => 'h1_dimensions',
				'type'     => 'spacing',
				'compiler' => true,
				'mode'     => 'margin',
				'title'    => __('H1 Margins', 'masterstudy'),
				'default'  => '',
			),
			array(
				'id'             => 'h2_params',
				'type'           => 'typography',
				'title'          => __('H2', 'masterstudy'),
				'compiler'       => true,
				'google'         => false,
				'font-backup'    => false,
				'all_styles'     => true,
				'font-weight'    => true,
				'font-family'    => false,
				'text-align'     => false,
				'font-style'     => false,
				'subsets'        => false,
				'font-size'      => true,
				'line-height'    => true,
				'word-spacing'   => true,
				'letter-spacing' => true,
				'color'          => false,
				'preview'        => true,
				'output'         => array('h2,.h2'),
				'units'          => 'px',
				'default'        => array(
					'font-size'   => '32px',
					'font-weight' => '700'
				)
			),
			array(
				'id'       => 'h2_dimensions',
				'type'     => 'spacing',
				'compiler' => true,
				'mode'     => 'margin',
				'title'    => __('H2 Margins', 'masterstudy'),
				'default'  => '',
			),
			array(
				'id'             => 'h3_params',
				'type'           => 'typography',
				'title'          => __('H3', 'masterstudy'),
				'compiler'       => true,
				'google'         => false,
				'font-backup'    => false,
				'all_styles'     => true,
				'font-weight'    => true,
				'font-family'    => false,
				'text-align'     => false,
				'font-style'     => false,
				'subsets'        => false,
				'font-size'      => true,
				'line-height'    => true,
				'word-spacing'   => true,
				'letter-spacing' => true,
				'color'          => false,
				'preview'        => true,
				'output'         => array('h3,.h3'),
				'units'          => 'px',
				'default'        => array(
					'font-size'   => '18px',
					'font-weight' => '700'
				)
			),
			array(
				'id'       => 'h3_dimensions',
				'type'     => 'spacing',
				'compiler' => true,
				'mode'     => 'margin',
				'title'    => __('H3 Margins', 'masterstudy'),
				'default'  => '',
			),
			array(
				'id'             => 'h4_params',
				'type'           => 'typography',
				'title'          => __('H4', 'masterstudy'),
				'compiler'       => true,
				'google'         => false,
				'font-backup'    => false,
				'all_styles'     => true,
				'font-weight'    => true,
				'font-family'    => false,
				'text-align'     => false,
				'font-style'     => false,
				'subsets'        => false,
				'font-size'      => true,
				'line-height'    => true,
				'word-spacing'   => true,
				'letter-spacing' => true,
				'color'          => false,
				'preview'        => true,
				'output'         => array('h4,.h4,blockquote'),
				'units'          => 'px',
				'default'        => array(
					'font-size'   => '16px',
					'font-weight' => '400'
				)
			),
			array(
				'id'       => 'h4_dimensions',
				'type'     => 'spacing',
				'compiler' => true,
				'mode'     => 'margin',
				'title'    => __('H4 Margins', 'masterstudy'),
				'default'  => '',
			),
			array(
				'id'             => 'h5_params',
				'type'           => 'typography',
				'title'          => __('H5', 'masterstudy'),
				'compiler'       => true,
				'google'         => false,
				'font-backup'    => false,
				'all_styles'     => true,
				'font-weight'    => true,
				'font-family'    => false,
				'text-align'     => false,
				'font-style'     => false,
				'subsets'        => false,
				'font-size'      => true,
				'line-height'    => true,
				'word-spacing'   => true,
				'letter-spacing' => true,
				'color'          => false,
				'preview'        => true,
				'output'         => array('h5,.h5,.select2-selection__rendered'),
				'units'          => 'px',
				'default'        => array(
					'font-size'   => '14px',
					'font-weight' => '700'
				)
			),
			array(
				'id'       => 'h5_dimensions',
				'type'     => 'spacing',
				'compiler' => true,
				'mode'     => 'margin',
				'title'    => __('H5 Margins', 'masterstudy'),
				'default'  => '',
			),
			array(
				'id'             => 'h6_params',
				'type'           => 'typography',
				'title'          => __('H6', 'masterstudy'),
				'compiler'       => true,
				'google'         => false,
				'font-backup'    => false,
				'all_styles'     => true,
				'font-weight'    => true,
				'font-family'    => false,
				'text-align'     => false,
				'font-style'     => false,
				'subsets'        => false,
				'font-size'      => true,
				'line-height'    => true,
				'word-spacing'   => true,
				'letter-spacing' => true,
				'color'          => false,
				'preview'        => true,
				'output'         => array('h6,.h6,.widget_pages ul li a, .widget_nav_menu ul li a, .footer_menu li a,.widget_categories ul li a,.sidebar-area .widget ul li a'),
				'units'          => 'px',
				'default'        => array(
					'font-size'   => '12px',
					'font-weight' => '400'
				)
			),
			array(
				'id'       => 'h6_dimensions',
				'type'     => 'spacing',
				'compiler' => true,
				'mode'     => 'margin',
				'title'    => __('H6 Margins', 'masterstudy'),
				'default'  => '',
			),
		)
	));

	Redux::setSection($opt_name, array(
		'title'   => __('Footer', 'masterstudy'),
		'desc'    => '',
		'icon'    => 'el-icon-photo',
		'submenu' => true,
		'fields'  => array(
			array(
				'id'      => 'footer_top',
				'type'    => 'switch',
				'title'   => __('Enable footer widgets area.', 'masterstudy'),
				'default' => true,
			),
			array(
				'id'      => 'footer_top_color',
				'type'    => 'color',
				'title'   => __('Footer Background Color', 'masterstudy'),
				'output'  => array('background-color' => '#footer_top'),
				'default' => '#414b4f',
			),
			array(
				'id'       => 'footer_first_columns',
				'type'     => 'button_set',
				'title'    => __('Footer Columns', 'masterstudy'),
				'desc'     => __('Select the number of columns to display in the footer.', 'masterstudy'),
				'type'     => 'button_set',
				'default'  => '4',
				'required' => array('footer_top', '=', true,),
				'options'  => array(
					'1' => __('1 Column', 'masterstudy'),
					'2' => __('2 Columns', 'masterstudy'),
					'3' => __('3 Columns', 'masterstudy'),
					'4' => __('4 Columns', 'masterstudy'),
				),
			),
		)
	));

	Redux::setSection($opt_name, array(
		'title'      => __('Footer Bottom', 'masterstudy'),
		'desc'       => '',
		'icon'       => 'el-icon-photo',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'      => 'footer_bottom',
				'type'    => 'switch',
				'title'   => __('Enable footer bottom widgets area.', 'masterstudy'),
				'default' => false,
			),
			array(
				'id'      => 'footer_bottom_color',
				'type'    => 'color',
				'title'   => __('Footer Bottom Background Color', 'masterstudy'),
				'output'  => array('background-color' => '#footer_bottom'),
				'default' => '#414b4f',
			),

			array(
				'id'             => 'footer_bottom_title',
				'type'           => 'typography',
				'title'          => __('Footer bottom Title typography', 'masterstudy'),
				'compiler'       => true,
				'google'         => false,
				'font-backup'    => false,
				'font-weight'    => true,
				'font-family'    => false,
				'text-align'     => false,
				'all_styles'     => true,
				'font-style'     => false,
				'subsets'        => false,
				'font-size'      => true,
				'line-height'    => false,
				'word-spacing'   => false,
				'letter-spacing' => false,
				'color'          => true,
				'preview'        => true,
				'output'         => array('#footer_bottom .widget_title h3'),
				'units'          => 'px',
			),
			array(
				'id'      => 'footer_bottom_title_uppercase',
				'type'    => 'switch',
				'title'   => __('Footer bottom uppercase title.', 'masterstudy'),
				'default' => true,
			),
			array(
				'id'      => 'footer_bottom_text_color',
				'type'    => 'color',
				'title'   => __('Footer Bottom Text Color', 'masterstudy'),
				'output'  => array(
					'color'            => '#footer_bottom, .widget_contacts ul li .text, 
				.footer_widgets_wrapper .widget ul li a,
				.widget_nav_menu ul.style_1 li a .h6, 
				.widget_pages ul.style_2 li a .h6,
				#footer .stm_product_list_widget.widget_woo_stm_style_2 li a .meta .title,
				.widget_pages ul.style_1 li a .h6',
					'background-color' => '.widget_pages ul.style_2 li a:after'
				),
				'default' => '#fff',
			),
			array(
				'id'       => 'footer_bottom_columns',
				'type'     => 'button_set',
				'title'    => __('Footer Bottom Columns', 'masterstudy'),
				'desc'     => __('Select the number of columns to display in the footer bottom.', 'masterstudy'),
				'default'  => '4',
				'required' => array('footer_bottom', '=', true,),
				'options'  => array(
					'1' => __('1 Column', 'masterstudy'),
					'2' => __('2 Columns', 'masterstudy'),
					'3' => __('3 Columns', 'masterstudy'),
					'4' => __('4 Columns', 'masterstudy'),
				),
			),
			array(
				'id'       => 'footer_bottom_socials',
				'type'     => 'button_set',
				'title'    => __('Footer Bottom Socials', 'masterstudy'),
				'desc'     => __('Select column number to insert socials.', 'masterstudy'),
				'default'  => 'none',
				'required' => array('footer_bottom', '=', true,),
				'options'  => array(
					'none' => __('None', 'masterstudy'),
					'1'    => __('1 Column', 'masterstudy'),
					'2'    => __('2 Column', 'masterstudy'),
					'3'    => __('3 Column', 'masterstudy'),
					'4'    => __('4 Column', 'masterstudy'),
				),
			),
		)
	));

	$copyright_fields = apply_filters('masterstudy_copyright_fields', array(
			array(
				'id'      => 'footer_copyright',
				'type'    => 'switch',
				'title'   => __('Enable footer copyright section.', 'masterstudy'),
				'default' => true,
			),
			array(
				'id'       => 'footer_copyright_bg_color',
				'type'     => 'color',
				'title'    => __('Footer Copyright Background Color', 'masterstudy'),
				'output'   => array('background-color' => '#footer_copyright'),
				'default'  => '#5e676b',
				'required' => array(
					array('footer_copyright', '=', true,),
				),
			),
			array(
				'id'       => 'footer_copyright_text_color',
				'type'     => 'color',
				'title'    => __('Footer Copyright Text Color', 'masterstudy'),
				'output'   => array('color' => '#footer_copyright .copyright_text, #footer_copyright .copyright_text a'),
				'default'  => '#fff',
				'required' => array(
					array('footer_copyright', '=', true,),
				),
			),
			array(
				'id'       => 'footer_copyright_border_color',
				'type'     => 'color',
				'title'    => __('Footer Copyright Border Color', 'masterstudy'),
				'output'   => array('border-color' => '#footer_copyright'),
				'default'  => '#5e676b',
				'required' => array(
					array('footer_copyright', '=', true,),
				),
			),
			array(
				'id'       => 'footer_logo_enabled',
				'type'     => 'switch',
				'required' => array(
					array('footer_copyright', '=', true,),
				),
				'title'    => __('Enable footer logo.', 'masterstudy'),
				'default'  => true,
			),
			array(
				'id'       => 'footer_logo',
				'url'      => false,
				'type'     => 'media',
				'title'    => __('Footer Logo', 'masterstudy'),
				'required' => array(
					array('footer_copyright', '=', true,),
					array('footer_logo_enabled', '=', true,),
				),
				'default'  => array('url' => get_template_directory_uri() . '/assets/img/tmp/footer-logo2x.png'),
				'subtitle' => __('Upload your logo file here. Size - 50*56 (Retina 2x). Note, bigger images will be cropped to default size', 'masterstudy'),
			),
			array(
				'id'       => 'footer_copyright_text',
				'type'     => 'textarea',
				'title'    => __('Footer Copyright', 'masterstudy'),
				'subtitle' => __('Enter the copyright text.', 'masterstudy'),
				'required' => array(
					array('footer_copyright', '=', true,),
				),
				'default'  => __('Copyright &copy; 2015 MasterStudy Theme by <a target="_blank" href="http://www.stylemixthemes.com/">Stylemix Themes</a>', 'masterstudy'),
			),
			array(
				'id'       => 'copyright_use_social',
				'type'     => 'sortable',
				'mode'     => 'checkbox',
				'title'    => __('Select Social Media Icons to display in copyright section', 'masterstudy'),
				'subtitle' => __('The urls for your social media icons will be taken from Social Media settings tab.', 'masterstudy'),
				'options'  => array(
					'facebook'     => 'Facebook',
					'twitter'      => 'Twitter',
					'instagram'    => 'Instagram',
					'behance'      => 'Behance',
					'dribbble'     => 'Dribbble',
					'flickr'       => 'Flickr',
					'git'          => 'Git',
					'linkedin'     => 'Linkedin',
					'pinterest'    => 'Pinterest',
					'yahoo'        => 'Yahoo',
					'delicious'    => 'Delicious',
					'dropbox'      => 'Dropbox',
					'reddit'       => 'Reddit',
					'soundcloud'   => 'Soundcloud',
					'google'       => 'Google',
					'google-plus'  => 'Google +',
					'skype'        => 'Skype',
					'youtube'      => 'Youtube',
					'youtube-play' => 'Youtube Play',
					'tumblr'       => 'Tumblr',
					'whatsapp'     => 'Whatsapp',
					'telegram'     => 'Telegram',
				),
				'default'  => array(
					'facebook'     => '0',
					'twitter'      => '0',
					'instagram'    => '0',
					'behance'      => '0',
					'dribbble'     => '0',
					'flickr'       => '0',
					'git'          => '0',
					'linkedin'     => '0',
					'pinterest'    => '0',
					'yahoo'        => '0',
					'delicious'    => '0',
					'dropbox'      => '0',
					'reddit'       => '0',
					'soundcloud'   => '0',
					'google'       => '0',
					'google-plus'  => '0',
					'skype'        => '0',
					'youtube'      => '0',
					'youtube-play' => '0',
					'tumblr'       => '0',
					'whatsapp'     => '0',
				),
			),
		)
	);

	Redux::setSection($opt_name, array(
		'title'      => __('Copyright', 'masterstudy'),
		'desc'       => __('Copyright block at the bottom of footer', 'masterstudy'),
		'id'         => 'footer_copyright',
		'subsection' => true,
		'fields'     => $copyright_fields
	));

	Redux::setSection($opt_name, array(
		'title'   => __('Social Media', 'masterstudy'),
		'icon'    => 'el-icon-address-book',
		'desc'    => __('Enter social media urls here and then you can enable them for footer or header. Please add full URLs including "http://".', 'masterstudy'),
		'submenu' => true,
		'fields'  => array(
			array(
				'id'       => 'facebook',
				'type'     => 'text',
				'title'    => __('Facebook', 'masterstudy'),
				'subtitle' => '',
				'default'  => 'https://www.facebook.com/',
				'desc'     => __('Enter your Facebook URL.', 'masterstudy'),
			),
			array(
				'id'       => 'twitter',
				'type'     => 'text',
				'title'    => __('Twitter', 'masterstudy'),
				'subtitle' => '',
				'default'  => 'https://www.twitter.com/',
				'desc'     => __('Enter your Twitter URL.', 'masterstudy'),
			),
			array(
				'id'       => 'instagram',
				'type'     => 'text',
				'title'    => __('Instagram', 'masterstudy'),
				'subtitle' => '',
				'default'  => 'https://www.instagram.com/',
				'desc'     => __('Enter your Instagram URL.', 'masterstudy'),
			),
			array(
				'id'       => 'behance',
				'type'     => 'text',
				'title'    => __('Behance', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Behance URL.', 'masterstudy'),
			),
			array(
				'id'       => 'dribbble',
				'type'     => 'text',
				'title'    => __('Dribbble', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Dribbble URL.', 'masterstudy'),
			),
			array(
				'id'       => 'flickr',
				'type'     => 'text',
				'title'    => __('Flickr', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Flickr URL.', 'masterstudy'),
			),
			array(
				'id'       => 'git',
				'type'     => 'text',
				'title'    => __('Git', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Git URL.', 'masterstudy'),
			),
			array(
				'id'       => 'linkedin',
				'type'     => 'text',
				'title'    => __('Linkedin', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Linkedin URL.', 'masterstudy'),
			),
			array(
				'id'       => 'pinterest',
				'type'     => 'text',
				'title'    => __('Pinterest', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Pinterest URL.', 'masterstudy'),
			),
			array(
				'id'       => 'yahoo',
				'type'     => 'text',
				'title'    => __('Yahoo', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Yahoo URL.', 'masterstudy'),
			),
			array(
				'id'       => 'delicious',
				'type'     => 'text',
				'title'    => __('Delicious', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Delicious URL.', 'masterstudy'),
			),
			array(
				'id'       => 'dropbox',
				'type'     => 'text',
				'title'    => __('Dropbox', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Dropbox URL.', 'masterstudy'),
			),
			array(
				'id'       => 'reddit',
				'type'     => 'text',
				'title'    => __('Reddit', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Reddit URL.', 'masterstudy'),
			),
			array(
				'id'       => 'soundcloud',
				'type'     => 'text',
				'title'    => __('Soundcloud', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Soundcloud URL.', 'masterstudy'),
			),
			array(
				'id'       => 'google',
				'type'     => 'text',
				'title'    => __('Google', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Google URL.', 'masterstudy'),
			),
			array(
				'id'       => 'google-plus',
				'type'     => 'text',
				'title'    => __('Google +', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Google + URL.', 'masterstudy'),
			),
			array(
				'id'       => 'skype',
				'type'     => 'text',
				'title'    => __('Skype', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Skype URL.', 'masterstudy'),
			),
			array(
				'id'       => 'youtube',
				'type'     => 'text',
				'title'    => __('Youtube', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Youtube URL.', 'masterstudy'),
			),
			array(
				'id'       => 'youtube-play',
				'type'     => 'text',
				'title'    => __('Youtube Play', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Youtube Play(only icon differ) URL.', 'masterstudy'),
			),
			array(
				'id'       => 'tumblr',
				'type'     => 'text',
				'title'    => __('Tumblr', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Tumblr URL.', 'masterstudy'),
			),
			array(
				'id'       => 'whatsapp',
				'type'     => 'text',
				'title'    => __('Whatsapp', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Whatsapp URL.', 'masterstudy'),
			),
			array(
				'id'       => 'telegram',
				'type'     => 'text',
				'title'    => __('Telegram', 'masterstudy'),
				'subtitle' => '',
				'desc'     => __('Enter your Telegram URL.', 'masterstudy'),
			),
		)
	));

	Redux::setSection($opt_name, array(
		'title'      => __('Social Widget', 'masterstudy'),
		'desc'       => __('Choose socials for widget, and their order', 'masterstudy'),
		'id'         => 'social_widget_opt',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'stm_social_widget_sort',
				'type'     => 'sortable',
				'mode'     => 'checkbox',
				'title'    => __('Select Social Widget Icons to display', 'masterstudy'),
				'subtitle' => __('The urls for your social media icons will be taken from Social Media settings tab.', 'masterstudy'),
				'options'  => array(
					'facebook'     => 'Facebook',
					'twitter'      => 'Twitter',
					'instagram'    => 'Instagram',
					'behance'      => 'Behance',
					'dribbble'     => 'Dribbble',
					'flickr'       => 'Flickr',
					'git'          => 'Git',
					'linkedin'     => 'Linkedin',
					'pinterest'    => 'Pinterest',
					'yahoo'        => 'Yahoo',
					'delicious'    => 'Delicious',
					'dropbox'      => 'Dropbox',
					'reddit'       => 'Reddit',
					'soundcloud'   => 'Soundcloud',
					'google'       => 'Google',
					'google-plus'  => 'Google +',
					'skype'        => 'Skype',
					'youtube'      => 'Youtube',
					'youtube-play' => 'Youtube Play',
					'tumblr'       => 'Tumblr',
					'whatsapp'     => 'Whatsapp',
					'telegram'     => 'Telegram',
				),
			),
		)
	));

	Redux::setSection($opt_name, array(
		'title'   => __('MailChimp', 'masterstudy'),
		'icon'    => 'el-icon-paper-clip',
		'submenu' => true,
		'fields'  => array(
			array(
				'id'       => 'mailchimp_api_key',
				'type'     => 'text',
				'title'    => __('Mailchimp API key', 'masterstudy'),
				'subtitle' => __('Paste your MailChimp API key', 'masterstudy'),
				'default'  => ""
			),
			array(
				'id'       => 'mailchimp_list_id',
				'type'     => 'text',
				'title'    => __('Mailchimp list id', 'masterstudy'),
				'subtitle' => __('Paste your MailChimp List id', 'masterstudy'),
				'default'  => ""
			)
		)
	));

	Redux::setSection($opt_name, array(
		'title'   => __('Google API', 'masterstudy'),
		'icon'    => 'el-icon-paper-clip',
		'submenu' => true,
		'fields'  => array(
			array(
				'id'      => 'g_recaptcha_public_key',
				'type'    => 'text',
				'title'   => __('Recaptcha Public key', 'masterstudy'),
				'default' => ""
			),
			array(
				'id'      => 'g_recaptcha_private_key',
				'type'    => 'text',
				'title'   => __('Recaptcha Private key', 'masterstudy'),
				'default' => ""
			),
		)
	));

	Redux::setSection($opt_name, array(
		'title'   => __('Custom CSS', 'masterstudy'),
		'icon'    => 'el-icon-css',
		'submenu' => true,
		'fields'  => array(
			array(
				'id'       => 'site_css',
				'type'     => 'ace_editor',
				'title'    => __('CSS Code', 'masterstudy'),
				'subtitle' => __('Paste your custom CSS code here.', 'masterstudy'),
				'mode'     => 'css',
				'default'  => ""
			)
		)
	));
}

/*
 * <--- END SECTIONS
 */

if (!function_exists('stm_option')) {
	function stm_option($id, $fallback = false, $key = false)
	{
		global $stm_option;
		if ($fallback == false) {
			$fallback = '';
		}
		$output = (isset($stm_option[$id]) && $stm_option[$id] !== '') ? $stm_option[$id] : $fallback;
		if (!empty($stm_option[$id]) && $key) {
			$output = $stm_option[$id][$key];
		}

		return $output;
	}
}

// Remove redux demo
function removeDemoModeLink()
{
	if (class_exists('ReduxFrameworkPlugin')) {
		remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2);
	}
	if (class_exists('ReduxFrameworkPlugin')) {
		remove_action('admin_notices', array(ReduxFrameworkPlugin::get_instance(), 'admin_notices'));
	}
}

add_action('init', 'removeDemoModeLink');