﻿=== Country Based Restrictions for WooCommerce ===
Contributors: zorem
Donate link: 
Tags: woocommerce
Requires at least: 4.0
Tested up to: 5.1.1
Stable tag: 5.1.1
License: GPLv2 
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Restrict your WooCommerce shop products to sell (or not) on specific countries!

== Description ==

This plugin allows you to restrict products on WooCommerce shop to sell (or not) on specific countries, using WooCommerce Geolocation.

Do you have products that you sell only to specific countries? Do you have products that you Do Not sell only to specific countries? Then this plugin is for you!

== Visibility options (plugin settings) ==

* Hide catalog visibility - checking this option will hide products you set with country restrictions from shop and search results. However these products will still be accessible and purchasable via direct link.
 * Make non-purchasable - checking this option will make  products you set with country restrictions non-purchasable (i.e. product can't be added to the cart).
 * Default message
 * Messagfe Position

* Hide completely - checking this will hide products you set with country restrictions completely (including direct link).

== How it works ==

* Go to plugin settings and setup the general visibility options
* For each product in your catalog you can set a list of countries to allow or disallow for sale.
* WooCommerce shipping country is used to determine what country the visitor is from a country which is restricted for a product, if a shipping country is not set, WooCommerce Geolocation is used.

You will need WooCommerce 3.0 or newer.
Does support translation.

== Frequently Asked Questions == 

= Can i sell product in specific country? =
Yes

= Can i restrict product to sell in specific country? =
Yes

== Installation ==

1. Upload the folder 'woo-product-country-base-restrictions` to the `/wp-content/plugins/` folder
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Make sure you have setup "shipping countries" in woocommerce general settings.
4. Edit a product to see your new options


== Changelog ==

= 1.9 =
* Added functionality to allow HTML code in Default message.

= 1.8.2 =
* Change design of Default message.

= 1.8.1 =
* Set design of Default message.

= 1.8 =
* WordPress Compatibility added upto 5.1.1
* Update plugin name
* WC Compatibility added

= 1.7 =
* Big fixed - Fatal error:  Call to a member function get_shipping_country() in customize.

= 1.6 =
* Big fixed - Fatal error:  Call to a member function get_shipping_country() in cronjob.

= 1.5 =
* Hebrew, Hindi, German, Russian, french language support added.

= 1.4 =
* Big fixed - Product single page
* Added fnctionality for determine WooCommerce shipping country

= 1.3 =
* added feature for message position.

= 1.2 =
* improvement.

= 1.1 =
* bug fix
* added WooCommerce and WordPress compatibility.

= 1.0 =
* Initial version.