��    $      <      \      \     ]  
   o     z     �     �     �     �     �     �     �  F   �  A   F     �     �     �     �     �  #   �          #     4  >   Q     �  3   �     �     �     �  4     G   K  D   �  v   �  [   O  -   �  =   �  *     �  B     	  
   &	     1	     A	     R	     l	     |	     �	  )   �	     �	  J   �	  V   !
  !   x
     �
     �
     �
     �
  8   �
     +     A     V  A   s     �  ?   �     
     "     8  9   R  b   �  ]   �  �   M  r   �  2   i  D   �  5   �   After add to cart After meta After price After sharing After short description After title All countries Before title Default : After add to cart Default message Default message : Sorry, this product is not available in your country Error message to display when a product is restricted by country. Excluding selected countries Geographic availability HERE Hide catalog visibility Hide completely Leave blank for default message: %s Make non-purchasable Message Position Product Country Restrictions Product Country Restrictions requires WooCommerce 3.0 or newer Product Visibility Restrict WooCommerce products in specific countries Restriction message Selected countries Selected countries only Sorry, this product is not available in your country This message will show on product page when product is not purchasable. This will hide selected products completely (including direct link). This will hide selected products in shop and search results. However product still will be accessible via direct link. This will make selected products non-purchasable (i.e. product can't be added to the cart). WooCommerce Product Country Base Restrictions You need to setup shipping locations in WooCommerce settings  before you can choose country restrictions Project-Id-Version: WooCommerce Product Country Base Restrictions
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-03-26 08:43+0000
PO-Revision-Date: 2019-03-26 08:45+0000
Last-Translator: kuldip <navadiyakuldip21@gmail.com>
Language-Team: German
Language: de_DE
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.2.1; wp-5.1.1 Nach dem Warenkorb
 Nach meta
 Nach dem Preis
 Nach dem teilen
 Nach kurzer Beschreibung
 Nach dem Titel
 Alle Länder
 Vor dem Titel
 Standard: Nach dem Kauf in den Warenkorb
 Standardnachricht
 Standardmeldung: Dieses Produkt ist in Ihrem Land leider nicht verfügbar
 Fehlermeldung, die angezeigt wird, wenn ein Produkt nach Ländern eingeschränkt ist.
 Ausgewählte Länder ausgenommen
 Geografische Verfügbarkeit
 HIER
 Katalogsichtbarkeit ausblenden
 Komplett verstecken
 Lassen Sie das Feld für die Standardnachricht leer:% s
 Nicht kaufbar machen
 Nachrichtenposition
 Produktlandeinschränkungen
 Produktlandeinschränkungen erfordern WooCommerce 3.0 oder neuer
 Produktsichtbarkeit
 Schränken Sie WooCommerce-Produkte in bestimmten Ländern ein
 Einschränkungsmeldung
 Ausgewählte Länder
 Nur ausgewählte Länder
 Leider ist dieses Produkt in Ihrem Land nicht verfügbar
 Diese Nachricht wird auf der Produktseite angezeigt, wenn das Produkt nicht erworben werden kann.
 Dadurch werden ausgewählte Produkte vollständig ausgeblendet (einschließlich Direktlink).
 Dadurch werden ausgewählte Produkte im Shop und in den Suchergebnissen ausgeblendet. Das Produkt wird jedoch weiterhin über eine direkte Verbindung zugänglich sein.
 Dadurch werden ausgewählte Produkte nicht gekauft (d. H. Das Produkt kann nicht in den Warenkorb gelegt werden).
 Einschränkungen für das WooCommerce-Produktland
 Sie müssen Versandorte in den WooCommerce-Einstellungen einrichten
 bevor Sie Ländereinschränkungen auswählen können
 