msgid ""
msgstr ""
"Project-Id-Version: WooCommerce Product Country Base Restrictions\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-26 08:43+0000\n"
"PO-Revision-Date: 2019-03-26 08:45+0000\n"
"Last-Translator: kuldip <navadiyakuldip21@gmail.com>\n"
"Language-Team: हिन्दी\n"
"Language: hi_IN\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.2.1; wp-5.1.1"

#: woocommerce-product-country-base-restrictions.php:77
msgid "Product Country Restrictions"
msgstr ""
"उत्पाद देश प्रतिबंध\n"

#: woocommerce-product-country-base-restrictions.php:80
msgid "Restriction message"
msgstr ""
"प्रतिबंध संदेश\n"

#: woocommerce-product-country-base-restrictions.php:81
msgid "Error message to display when a product is restricted by country."
msgstr ""
"उत्पाद द्वारा देश में प्रतिबंधित किए जाने पर प्रदर्शित करने के लिए त्रुटि "
"संदेश।\n"

#: woocommerce-product-country-base-restrictions.php:87
#, php-format
msgid "Leave blank for default message: %s"
msgstr ""
"डिफ़ॉल्ट संदेश के लिए रिक्त छोड़ें:% s\n"

#: woocommerce-product-country-base-restrictions.php:96
msgid "Product Country Restrictions requires WooCommerce 3.0 or newer"
msgstr ""
"उत्पाद देश प्रतिबंधों को WooCommerce 3.0 या नए की आवश्यकता है\n"

#: woocommerce-product-country-base-restrictions.php:111
#: woocommerce-product-country-base-restrictions.php:155
msgid "Geographic availability"
msgstr ""
"भौगोलिक उपलब्धता\n"

#: woocommerce-product-country-base-restrictions.php:115
#: woocommerce-product-country-base-restrictions.php:160
msgid "All countries"
msgstr ""
"सभी देश\n"

#: woocommerce-product-country-base-restrictions.php:116
#: woocommerce-product-country-base-restrictions.php:161
msgid "Selected countries only"
msgstr ""
"चुनिंदा देश ही\n"

#: woocommerce-product-country-base-restrictions.php:117
#: woocommerce-product-country-base-restrictions.php:162
msgid "Excluding selected countries"
msgstr ""
"चुनिंदा देशों को छोड़कर\n"

#: woocommerce-product-country-base-restrictions.php:129
#: woocommerce-product-country-base-restrictions.php:175
msgid "Selected countries"
msgstr ""
"चुनिंदा देश\n"

#: woocommerce-product-country-base-restrictions.php:144
msgid "You need to setup shipping locations in WooCommerce settings "
msgstr ""
"आपको WooCommerce सेटिंग्स में शिपिंग स्थानों को सेटअप करने की आवश्यकता है"

#: woocommerce-product-country-base-restrictions.php:144
msgid "HERE"
msgstr ""
"यहाँ\n"

#: woocommerce-product-country-base-restrictions.php:144
msgid "before you can choose country restrictions"
msgstr ""
"इससे पहले कि आप देश प्रतिबंधों का चयन कर सकें\n"

#: woocommerce-product-country-base-restrictions.php:271
msgid "Sorry, this product is not available in your country"
msgstr ""
"क्षमा करें, यह उत्पाद आपके देश में उपलब्ध नहीं है\n"

#. Name of the plugin
#: include/settings.php:54 include/settings.php:74
msgid "WooCommerce Product Country Base Restrictions"
msgstr ""
"WooCommerce उत्पाद देश आधार प्रतिबंध\n"

#: include/settings.php:59
msgid "Product Visibility"
msgstr "उत्पाद दृश्यता"

#: include/settings.php:64
msgid "Hide catalog visibility"
msgstr ""
"सूची दृश्यता छिपाएँ\n"

#: include/settings.php:64
msgid ""
"This will hide selected products in shop and search results. However product "
"still will be accessible via direct link."
msgstr ""
"यह दुकान और खोज परिणामों में चयनित उत्पादों को छिपाएगा। हालांकि उत्पाद अभी "
"भी प्रत्यक्ष लिंक के माध्यम से सुलभ होगा।\n"

#: include/settings.php:65
msgid "Hide completely"
msgstr ""
"पूरी तरह से छिपाओ\n"

#: include/settings.php:65
msgid "This will hide selected products completely (including direct link)."
msgstr ""
"यह चयनित उत्पादों को पूरी तरह से छिपा देगा (प्रत्यक्ष लिंक सहित)।\n"

#: include/settings.php:80
msgid "Make non-purchasable"
msgstr ""
"गैर-क्रय योग्य बनायें\n"

#: include/settings.php:85
msgid ""
"This will make selected products non-purchasable (i.e. product can't be "
"added to the cart)."
msgstr ""
"यह चयनित उत्पादों को गैर-खरीद योग्य बना देगा (अर्थात उत्पाद को कार्ट में "
"नहीं जोड़ा जा सकता है)।\n"

#: include/settings.php:88
msgid "Default message"
msgstr ""
"डिफ़ॉल्ट संदेश\n"

#: include/settings.php:89
msgid "Default message : Sorry, this product is not available in your country"
msgstr "डिफ़ॉल्ट संदेश: क्षमा करें, यह उत्पाद आपके देश में उपलब्ध नहीं है"

#: include/settings.php:90 include/settings.php:97
msgid "This message will show on product page when product is not purchasable."
msgstr ""
"यह संदेश उत्पाद पृष्ठ पर तब दिखाई देगा जब उत्पाद खरीदने योग्य नहीं होगा।\n"

#: include/settings.php:95
msgid "Message Position"
msgstr ""
"संदेश की स्थिति\n"

#: include/settings.php:96
msgid "Default : After add to cart"
msgstr ""
"डिफ़ॉल्ट: कार्ट में जोड़ने के बाद\n"

#: include/settings.php:102
msgid "Before title"
msgstr "शीर्षक से पहले"

#: include/settings.php:103
msgid "After title"
msgstr "शीर्षक के बाद"

#: include/settings.php:104
msgid "After price"
msgstr ""
"कीमत के बाद\n"

#: include/settings.php:105
msgid "After short description"
msgstr "संक्षिप्त विवरण के बाद"

#: include/settings.php:106
msgid "After add to cart"
msgstr ""
"कार्ट में जोड़ने के बाद\n"

#: include/settings.php:107
msgid "After meta"
msgstr ""
"मेटा के बाद\n"

#: include/settings.php:108
msgid "After sharing"
msgstr "शेयर करने के बाद"

#. Description of the plugin
msgid "Restrict WooCommerce products in specific countries"
msgstr ""
"विशिष्ट देशों में WooCommerce उत्पादों को प्रतिबंधित करें\n"
