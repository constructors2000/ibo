��    $      <      \      \     ]  
   o     z     �     �     �     �     �     �     �  F   �  A   F     �     �     �     �     �  #   �          #     4  >   Q     �  3   �     �     �     �  4     G   K  D   �  v   �  [   O  -   �  =   �  *     �  B     	     4	     B	     O	     b	     �	     �	     �	  &   �	     �	  P   �	  E   >
     �
     �
     �
  $   �
     �
  -   �
     ,     B  !   W  ^   y     �  @   �     1     I     ^  :   }  \   �  T     �   j  i   	  4   s  W   �  2       After add to cart After meta After price After sharing After short description After title All countries Before title Default : After add to cart Default message Default message : Sorry, this product is not available in your country Error message to display when a product is restricted by country. Excluding selected countries Geographic availability HERE Hide catalog visibility Hide completely Leave blank for default message: %s Make non-purchasable Message Position Product Country Restrictions Product Country Restrictions requires WooCommerce 3.0 or newer Product Visibility Restrict WooCommerce products in specific countries Restriction message Selected countries Selected countries only Sorry, this product is not available in your country This message will show on product page when product is not purchasable. This will hide selected products completely (including direct link). This will hide selected products in shop and search results. However product still will be accessible via direct link. This will make selected products non-purchasable (i.e. product can't be added to the cart). WooCommerce Product Country Base Restrictions You need to setup shipping locations in WooCommerce settings  before you can choose country restrictions Project-Id-Version: WooCommerce Product Country Base Restrictions
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-03-26 08:43+0000
PO-Revision-Date: 2019-03-26 08:45+0000
Last-Translator: kuldip <navadiyakuldip21@gmail.com>
Language-Team: French (France)
Language: fr_FR
Plural-Forms: nplurals=2; plural=n > 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.2.1; wp-5.1.1 Après ajouter au panier
 Après méta
 Après prix
 Après le partage
 Après une courte description
 Après le titre
 Tous les pays
 Avant le titre
 Par défaut: après ajouter au panier
 Message par défaut
 Message par défaut: Désolé, ce produit n'est pas disponible dans votre pays.
 Message d'erreur à afficher lorsqu'un produit est limité par pays.
 Hors pays sélectionnés
 Disponibilité géographique
 ICI Masquer la visibilité du catalogue
 Se cacher complètement
 Laissez vide pour le message par défaut:% s
 Rendre non-achetable
 Position du message
 Restrictions par pays de produit
 Les restrictions par pays du produit nécessitent WooCommerce 3.0 ou une version ultérieure.
 Visibilité du produit
 Restreindre les produits WooCommerce dans des pays spécifiques
 Message de restriction
 Pays sélectionnés
 Pays sélectionnés seulement
 Désolé, ce produit n'est pas disponible dans votre pays
 Ce message s'affichera sur la page du produit lorsque le produit ne peut pas être acheté.
 Cela masquera complètement les produits sélectionnés (y compris le lien direct).
 Cela masquera les produits sélectionnés dans la boutique et les résultats de recherche. Toutefois, le produit sera toujours accessible via un lien direct.
 Cela rendra les produits sélectionnés non achetables (le produit ne peut pas être ajouté au panier).
 Restrictions de base du pays du produit WooCommerce
 Vous devez configurer les emplacements d'expédition dans les paramètres WooCommerce.
 avant de pouvoir choisir les restrictions de pays
 