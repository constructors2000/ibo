��    $      <      \      \     ]  
   o     z     �     �     �     �     �     �     �  F   �  A   F     �     �     �     �     �  #   �          #     4  >   Q     �  3   �     �     �     �  4     G   K  D   �  v   �  [   O  -   �  =   �  *     �  B     	     2	     C	     X	     m	     �	     �	     �	  4   �	     �	  `   
  V   �
  &   �
      �
       &   &     M  >   f     �     �  !   �  O   �     J  E   a     �     �  !   �  @   �  a   :  k   �  �     �   �  '   d  P   �  7   �   After add to cart After meta After price After sharing After short description After title All countries Before title Default : After add to cart Default message Default message : Sorry, this product is not available in your country Error message to display when a product is restricted by country. Excluding selected countries Geographic availability HERE Hide catalog visibility Hide completely Leave blank for default message: %s Make non-purchasable Message Position Product Country Restrictions Product Country Restrictions requires WooCommerce 3.0 or newer Product Visibility Restrict WooCommerce products in specific countries Restriction message Selected countries Selected countries only Sorry, this product is not available in your country This message will show on product page when product is not purchasable. This will hide selected products completely (including direct link). This will hide selected products in shop and search results. However product still will be accessible via direct link. This will make selected products non-purchasable (i.e. product can't be added to the cart). WooCommerce Product Country Base Restrictions You need to setup shipping locations in WooCommerce settings  before you can choose country restrictions Project-Id-Version: WooCommerce Product Country Base Restrictions
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-03-26 08:43+0000
PO-Revision-Date: 2019-03-26 08:44+0000
Last-Translator: kuldip <navadiyakuldip21@gmail.com>
Language-Team: Hebrew
Language: he_IL
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.2.1; wp-5.1.1 לאחר הוספה לעגלה
 לאחר מטא
 אחרי המחיר
 לאחר שיתוף
 לאחר תיאור קצר
 אחרי הכותרת
 כל הארצות
 לפני הכותרת
 ברירת מחדל: לאחר הוספה לעגלה
 הודעת ברירת מחדל
 הודעת ברירת מחדל: מצטערים, מוצר זה אינו זמין בארץ שלך
 הודעת שגיאה להצגה כאשר מוצר מוגבל על ידי מדינה.
 לא כולל ארצות שנבחרו
 זמינות גיאוגרפית
 כאן הסתר את חשיפת הקטלוג
 הסתר לחלוטין
 השאר ריק עבור הודעת ברירת המחדל:% s
 הפוך ללא רכישה
 הודעה מיקום
 הגבלות מדינה מוצר
 הגבלות ארץ מוצר דורשות WooCommerce 3.0 או חדש יותר
 חשיפה למוצר
 הגבל מוצרים של WooCommerce בארצות ספציפיות
 הודעת הגבלה
 ארצות נבחרות
 ארצות נבחרות בלבד
 מצטערים, מוצר זה אינו זמין בארץ שלך
 הודעה זו תופיע בדף המוצר כאשר המוצר אינו ניתן לרכישה.
 זה יהיה להסתיר את המוצרים שנבחרו לחלוטין (כולל קישור ישיר).
 פעולה זו תסתיר מוצרים שנבחרו בתוצאות החנות ובתוצאות החיפוש. עם זאת המוצר עדיין יהיה נגיש באמצעות קישור ישיר.
 פעולה זו תגרום למוצרים נבחרים שאינם לרכישה (כלומר, לא ניתן להוסיף את המוצר לעגלה).
 מגבלות מוצר של WooCommerce
 עליך להגדיר את מיקומי המשלוח בהגדרות WooCommerce
 לפני שתוכל לבחור הגבלות מדינה
 