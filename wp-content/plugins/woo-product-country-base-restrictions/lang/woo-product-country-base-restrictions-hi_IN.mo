��    $      <      \      \     ]  
   o     z     �     �     �     �     �     �     �  F   �  A   F     �     �     �     �     �  #   �          #     4  >   Q     �  3   �     �     �     �  4     G   K  D   �  v   �  [   O  -   �  =   �  *     �  B  >   	     ]	     |	  *   �	  <   �	  #   
     '
  &   <
  X   c
  )   �
  �   �
  �   �  @   h  /   �     �  6   �  .     a   M  :   �  *   �  6     �   L  (   �  �   �  )   �      �  '   �  �   �  �   {  �   ;  5  �  �     O     �   f  v      After add to cart After meta After price After sharing After short description After title All countries Before title Default : After add to cart Default message Default message : Sorry, this product is not available in your country Error message to display when a product is restricted by country. Excluding selected countries Geographic availability HERE Hide catalog visibility Hide completely Leave blank for default message: %s Make non-purchasable Message Position Product Country Restrictions Product Country Restrictions requires WooCommerce 3.0 or newer Product Visibility Restrict WooCommerce products in specific countries Restriction message Selected countries Selected countries only Sorry, this product is not available in your country This message will show on product page when product is not purchasable. This will hide selected products completely (including direct link). This will hide selected products in shop and search results. However product still will be accessible via direct link. This will make selected products non-purchasable (i.e. product can't be added to the cart). WooCommerce Product Country Base Restrictions You need to setup shipping locations in WooCommerce settings  before you can choose country restrictions Project-Id-Version: WooCommerce Product Country Base Restrictions
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-03-26 08:43+0000
PO-Revision-Date: 2019-03-26 08:45+0000
Last-Translator: kuldip <navadiyakuldip21@gmail.com>
Language-Team: हिन्दी
Language: hi_IN
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.2.1; wp-5.1.1 कार्ट में जोड़ने के बाद
 मेटा के बाद
 कीमत के बाद
 शेयर करने के बाद संक्षिप्त विवरण के बाद शीर्षक के बाद सभी देश
 शीर्षक से पहले डिफ़ॉल्ट: कार्ट में जोड़ने के बाद
 डिफ़ॉल्ट संदेश
 डिफ़ॉल्ट संदेश: क्षमा करें, यह उत्पाद आपके देश में उपलब्ध नहीं है उत्पाद द्वारा देश में प्रतिबंधित किए जाने पर प्रदर्शित करने के लिए त्रुटि संदेश।
 चुनिंदा देशों को छोड़कर
 भौगोलिक उपलब्धता
 यहाँ
 सूची दृश्यता छिपाएँ
 पूरी तरह से छिपाओ
 डिफ़ॉल्ट संदेश के लिए रिक्त छोड़ें:% s
 गैर-क्रय योग्य बनायें
 संदेश की स्थिति
 उत्पाद देश प्रतिबंध
 उत्पाद देश प्रतिबंधों को WooCommerce 3.0 या नए की आवश्यकता है
 उत्पाद दृश्यता विशिष्ट देशों में WooCommerce उत्पादों को प्रतिबंधित करें
 प्रतिबंध संदेश
 चुनिंदा देश
 चुनिंदा देश ही
 क्षमा करें, यह उत्पाद आपके देश में उपलब्ध नहीं है
 यह संदेश उत्पाद पृष्ठ पर तब दिखाई देगा जब उत्पाद खरीदने योग्य नहीं होगा।
 यह चयनित उत्पादों को पूरी तरह से छिपा देगा (प्रत्यक्ष लिंक सहित)।
 यह दुकान और खोज परिणामों में चयनित उत्पादों को छिपाएगा। हालांकि उत्पाद अभी भी प्रत्यक्ष लिंक के माध्यम से सुलभ होगा।
 यह चयनित उत्पादों को गैर-खरीद योग्य बना देगा (अर्थात उत्पाद को कार्ट में नहीं जोड़ा जा सकता है)।
 WooCommerce उत्पाद देश आधार प्रतिबंध
 आपको WooCommerce सेटिंग्स में शिपिंग स्थानों को सेटअप करने की आवश्यकता है इससे पहले कि आप देश प्रतिबंधों का चयन कर सकें
 