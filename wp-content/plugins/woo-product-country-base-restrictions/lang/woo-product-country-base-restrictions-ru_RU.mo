��    $      <      \      \     ]  
   o     z     �     �     �     �     �     �     �  F   �  A   F     �     �     �     �     �  #   �          #     4  >   Q     �  3   �     �     �     �  4     G   K  D   �  v   �  [   O  -   �  =   �  *       B  2   _	     �	     �	     �	  -   �	     
     "
     7
  K   V
  +   �
  �   �
  �   U  1   �  4   	     >  1   F  (   x  T   �  )   �  "      9   C  r   }  $   �  ]     /   s      �  -   �  Z   �  �   M  �   �  �   w  �   T  E     e   X  [   �   After add to cart After meta After price After sharing After short description After title All countries Before title Default : After add to cart Default message Default message : Sorry, this product is not available in your country Error message to display when a product is restricted by country. Excluding selected countries Geographic availability HERE Hide catalog visibility Hide completely Leave blank for default message: %s Make non-purchasable Message Position Product Country Restrictions Product Country Restrictions requires WooCommerce 3.0 or newer Product Visibility Restrict WooCommerce products in specific countries Restriction message Selected countries Selected countries only Sorry, this product is not available in your country This message will show on product page when product is not purchasable. This will hide selected products completely (including direct link). This will hide selected products in shop and search results. However product still will be accessible via direct link. This will make selected products non-purchasable (i.e. product can't be added to the cart). WooCommerce Product Country Base Restrictions You need to setup shipping locations in WooCommerce settings  before you can choose country restrictions Project-Id-Version: WooCommerce Product Country Base Restrictions
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-03-26 08:43+0000
PO-Revision-Date: 2019-03-26 08:46+0000
Last-Translator: kuldip <navadiyakuldip21@gmail.com>
Language-Team: Russian
Language: ru_RU
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.2.1; wp-5.1.1 После добавления в корзину
 После мета
 После цены
 После обмена
 После краткого описания
 После заголовка
 Все страны
 Перед названием
 По умолчанию: после добавления в корзину
 Сообщение по умолчанию
 Сообщение по умолчанию: Извините, этот продукт недоступен в вашей стране
 Сообщение об ошибке для отображения, когда продукт ограничен страной.
 Исключая выбранные страны
 Географическая доступность
 ВОТ
 Скрыть видимость каталога
 Полностью спрятаться
 Оставьте пустым для сообщения по умолчанию:% s
 Сделать не покупаемым
 Позиция сообщения
 Ограничения на товары в стране
 Ограничения по стране для продукта требуют WooCommerce 3.0 или новее
 Видимость продукта
 Ограничить продукты WooCommerce в определенных странах
 Сообщение об ограничении
 Выбранные страны
 Только выбранные страны
 Извините, этот продукт недоступен в вашей стране
 Это сообщение будет отображаться на странице товара, когда товар недоступен для покупки.
 Это позволит полностью скрыть выбранные товары (включая прямую ссылку).
 Это скроет выбранные товары в магазине и результаты поиска. Однако продукт по-прежнему будет доступен по прямой ссылке.
 Это сделает выбранные товары недоступными для покупки (то есть товар не может быть добавлен в корзину).
 WooCommerce продукт страновые ограничения
 Вам нужно настроить места доставки в настройках WooCommerce
 прежде чем вы сможете выбрать ограничения страны
 