msgid ""
msgstr ""
"Project-Id-Version: WooCommerce Product Country Base Restrictions\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-26 08:43+0000\n"
"PO-Revision-Date: 2019-03-26 08:45+0000\n"
"Last-Translator: kuldip <navadiyakuldip21@gmail.com>\n"
"Language-Team: French (France)\n"
"Language: fr_FR\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.2.1; wp-5.1.1"

#: woocommerce-product-country-base-restrictions.php:77
msgid "Product Country Restrictions"
msgstr ""
"Restrictions par pays de produit\n"

#: woocommerce-product-country-base-restrictions.php:80
msgid "Restriction message"
msgstr ""
"Message de restriction\n"

#: woocommerce-product-country-base-restrictions.php:81
msgid "Error message to display when a product is restricted by country."
msgstr ""
"Message d'erreur à afficher lorsqu'un produit est limité par pays.\n"

#: woocommerce-product-country-base-restrictions.php:87
#, php-format
msgid "Leave blank for default message: %s"
msgstr ""
"Laissez vide pour le message par défaut:% s\n"

#: woocommerce-product-country-base-restrictions.php:96
msgid "Product Country Restrictions requires WooCommerce 3.0 or newer"
msgstr ""
"Les restrictions par pays du produit nécessitent WooCommerce 3.0 ou une "
"version ultérieure.\n"

#: woocommerce-product-country-base-restrictions.php:111
#: woocommerce-product-country-base-restrictions.php:155
msgid "Geographic availability"
msgstr ""
"Disponibilité géographique\n"

#: woocommerce-product-country-base-restrictions.php:115
#: woocommerce-product-country-base-restrictions.php:160
msgid "All countries"
msgstr ""
"Tous les pays\n"

#: woocommerce-product-country-base-restrictions.php:116
#: woocommerce-product-country-base-restrictions.php:161
msgid "Selected countries only"
msgstr ""
"Pays sélectionnés seulement\n"

#: woocommerce-product-country-base-restrictions.php:117
#: woocommerce-product-country-base-restrictions.php:162
msgid "Excluding selected countries"
msgstr ""
"Hors pays sélectionnés\n"

#: woocommerce-product-country-base-restrictions.php:129
#: woocommerce-product-country-base-restrictions.php:175
msgid "Selected countries"
msgstr ""
"Pays sélectionnés\n"

#: woocommerce-product-country-base-restrictions.php:144
msgid "You need to setup shipping locations in WooCommerce settings "
msgstr ""
"Vous devez configurer les emplacements d'expédition dans les paramètres "
"WooCommerce.\n"

#: woocommerce-product-country-base-restrictions.php:144
msgid "HERE"
msgstr "ICI"

#: woocommerce-product-country-base-restrictions.php:144
msgid "before you can choose country restrictions"
msgstr ""
"avant de pouvoir choisir les restrictions de pays\n"

#: woocommerce-product-country-base-restrictions.php:271
msgid "Sorry, this product is not available in your country"
msgstr ""
"Désolé, ce produit n'est pas disponible dans votre pays\n"

#. Name of the plugin
#: include/settings.php:54 include/settings.php:74
msgid "WooCommerce Product Country Base Restrictions"
msgstr ""
"Restrictions de base du pays du produit WooCommerce\n"

#: include/settings.php:59
msgid "Product Visibility"
msgstr ""
"Visibilité du produit\n"

#: include/settings.php:64
msgid "Hide catalog visibility"
msgstr ""
"Masquer la visibilité du catalogue\n"

#: include/settings.php:64
msgid ""
"This will hide selected products in shop and search results. However product "
"still will be accessible via direct link."
msgstr ""
"Cela masquera les produits sélectionnés dans la boutique et les résultats de "
"recherche. Toutefois, le produit sera toujours accessible via un lien direct."
"\n"

#: include/settings.php:65
msgid "Hide completely"
msgstr ""
"Se cacher complètement\n"

#: include/settings.php:65
msgid "This will hide selected products completely (including direct link)."
msgstr ""
"Cela masquera complètement les produits sélectionnés (y compris le lien "
"direct).\n"

#: include/settings.php:80
msgid "Make non-purchasable"
msgstr ""
"Rendre non-achetable\n"

#: include/settings.php:85
msgid ""
"This will make selected products non-purchasable (i.e. product can't be "
"added to the cart)."
msgstr ""
"Cela rendra les produits sélectionnés non achetables (le produit ne peut pas "
"être ajouté au panier).\n"

#: include/settings.php:88
msgid "Default message"
msgstr ""
"Message par défaut\n"

#: include/settings.php:89
msgid "Default message : Sorry, this product is not available in your country"
msgstr ""
"Message par défaut: Désolé, ce produit n'est pas disponible dans votre pays."
"\n"

#: include/settings.php:90 include/settings.php:97
msgid "This message will show on product page when product is not purchasable."
msgstr ""
"Ce message s'affichera sur la page du produit lorsque le produit ne peut pas "
"être acheté.\n"

#: include/settings.php:95
msgid "Message Position"
msgstr ""
"Position du message\n"

#: include/settings.php:96
msgid "Default : After add to cart"
msgstr ""
"Par défaut: après ajouter au panier\n"

#: include/settings.php:102
msgid "Before title"
msgstr ""
"Avant le titre\n"

#: include/settings.php:103
msgid "After title"
msgstr ""
"Après le titre\n"

#: include/settings.php:104
msgid "After price"
msgstr ""
"Après prix\n"

#: include/settings.php:105
msgid "After short description"
msgstr ""
"Après une courte description\n"

#: include/settings.php:106
msgid "After add to cart"
msgstr ""
"Après ajouter au panier\n"

#: include/settings.php:107
msgid "After meta"
msgstr ""
"Après méta\n"

#: include/settings.php:108
msgid "After sharing"
msgstr ""
"Après le partage\n"

#. Description of the plugin
msgid "Restrict WooCommerce products in specific countries"
msgstr ""
"Restreindre les produits WooCommerce dans des pays spécifiques\n"
