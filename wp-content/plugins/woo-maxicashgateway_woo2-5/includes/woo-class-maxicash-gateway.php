<?php
class WC_Gateway_MaxiCashGateway extends WC_Payment_Gateway {
	
			/** The single instance of the class. */
		protected static $_instance = null;
		
		/**
		 * Returns the *Singleton* instance of this class.
		 * @return Singleton The *Singleton* instance.
		 */
		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}
		
		/**
		 * @return string
		 */
		public function __toString() {
			return strtolower( get_class( $this ) );
		}
		/**
		 * Constructor for the gateway.
		 */
		public function __construct() {
	  
	  
	  
	  
	  
			$this->id                 = 'maxicashgateway';
			$this->icon               = $this->get_plugin_url() . '/images/logo.png';
			$this->has_fields         = false;
			$this->method_title       = __( 'MaxiCash Gateway', 'woo-maxicash-gateway' );
			$this->method_description = __( 'Allows MaxiCash Gateway Payment. After payment successfully then Orders are marked as "<strong>Processing</strong>".', 'woo-maxicash-gateway' );

			// Load the settings.
			$this->init_form_fields();
			$this->init_settings();
			
			if($this->get_option('gateway_mode')=='live') 
			    $this->gateway_url='https://api.maxicashapp.com/PayEntryPost';
			else
			    $this->gateway_url='https://api-testbed.maxicashapp.com/PayEntryPost';
		  
			// Define user set variables
			$this->title        = $this->get_option( 'title' );
			$this->description  = $this->get_option( 'description' );
			$this->instructions = $this->get_option( 'instructions', $this->description );

			// Actions
						
			add_action('woocommerce_update_options_payment_gateways_' . $this->id, array( &$this, 'process_admin_options' ) );
			//add_action('woocommerce_update_options_payment_gateways', array(&$this, 'process_admin_options'));
			
			add_action( 'woocommerce_receipt_'. $this->id, array( $this, 'receipt_page' ));
			
		    add_action( 'woocommerce_thankyou_' . $this->id, array( $this, 'thankyou_page' ) );
		  
			// Customer Emails
			add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );

		}
		/*** Receipt Page **/
		public function receipt_page($order){ 
				echo '<p>'.__('Thank you for your order, please click the button below to pay with MaxiCash Gateway.', 'woo-maxicash-gateway').'</p>';
				echo $this->generate_maxicashgateway_form($order);
			}
		/**
		 * Generate payu button link
		 **/
		public function generate_maxicashgateway_form($order_id){
			
		   global $woocommerce;

			$order = new WC_Order( $order_id );
			
	
			//$accepturl=get_permalink($this->redirect_page_id)."?st=done&order_id=".$order_id;
			//$cancelurl=get_permalink($this->redirect_page_id)."?st=cancel&order_id=".$order_id;
			//$declineurl=get_permalink($this->redirect_page_id)."?st=decline&order_id=".$order_id;
			
			//$accepturl=get_permalink($this->redirect_page_id);
			
			$accepturl = get_permalink( $this->get_option('thankyou_page_id') );
			
			$cancelurl=get_permalink($this->redirect_page_id);
			$declineurl=get_permalink($this->redirect_page_id);
			$currency = get_option('woocommerce_currency');
			
			
			$params=array(
			 'MerchantID'=>$this->get_option('merchant_id'),
			 'MerchantPassword'=>$this->get_option('merchant_pass'),
			 'Amount' =>($order->order_total * 100),
			 'Currency' =>$currency,
			 'Language' =>$this->get_option('language_mode'),
			 'PayType' =>'MaxiCash',
			 'Telephone' =>'',
			 'Reference' =>$order_id,
			 'accepturl' =>$accepturl,
			 'cancelurl' =>$cancelurl,
			 'declineurl' =>$declineurl
			);
			
			

					
			$payu_args_array = array();
			foreach($params as $key => $value){
			  $payu_args_array[] = "<input type='hidden' name='$key' value='$value'/>";
			}
			return '<form action="'.$this->gateway_url.'" method="post" id="maxicashgateway_payment_form">
				' . implode('', $payu_args_array) . '
				<input type="submit" class="button-alt" id="submit_maxicashgateway__payment_form" value="'.__('Pay with MaxiCash Gateway', 'woo-maxicash-gateway').'" /> <a class="button cancel" href="'.$order->get_cancel_order_url().'">'.__('Cancel order and restore cart', 'woo-maxicash-gateway').'</a>
				<script type="text/javascript">
	jQuery(function(){
	jQuery("body").block(
			{
				message: "<img src=\"'.plugins_url( '../images/loading.gif' , __FILE__ ).'\" alt=\"Redirecting…\" style=\"float:left; margin-right: 10px;\" />'.__('Thank you for your order. We are now redirecting you to MaxiCash Gateway to make payment.', 'woo-maxicash-gateway').'",
					overlayCSS:
			{
				background: "#fff",
					opacity: 0.6
		},
		css: {
			padding:        20,
				textAlign:      "center",
				color:          "#555",
				border:         "3px solid #aaa",
				backgroundColor:"#fff",
				cursor:         "wait",
				lineHeight:"32px"
		}
		});
		jQuery("#submit_maxicashgateway__payment_form").click();});</script>
				</form>';
	
	
		}
		/**
		 * Initialize Gateway Settings Form Fields
		 */
		public function init_form_fields() {
	  
			$this->form_fields = apply_filters( 'wc_maxicashgateway_form_fields', array(
		  
				'enabled' => array(
					'title'   => __( 'Enable/Disable', 'woo-maxicash-gateway' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable MaxiCash Gateway', 'woo-maxicash-gateway' ),
					'default' => 'yes'
				),
				
				'title' => array(
					'title'       => __( 'Title', 'woo-maxicash-gateway' ),
					'type'        => 'text',
					'description' => __( 'This controls the title for the payment method the customer sees during checkout.', 'woo-maxicash-gateway' ),
					'default'     => __( 'MaxiCash Gateway', 'woo-maxicash-gateway' ),
					'desc_tip'    => true,
				),
				'merchant_id' => array(
					'title'       => __( 'Merchant Id', 'woo-maxicash-gateway' ),
					'type'        => 'text',
					'description' => __( 'You have to get merchant id from payment merchant account and add here.', 'woo-maxicash-gateway' ),
					'default'     => __( '', 'woo-maxicash-gateway' ),
					'desc_tip'    => true,
				),
				'merchant_pass' => array(
					'title'       => __( 'Merchant Password', 'woo-maxicash-gateway' ),
					'type'        => 'text',
					'description' => __( 'You have to get merchant password from payment merchant account and add here.', 'woo-maxicash-gateway' ),
					'default'     => __( '', 'woo-maxicash-gateway' ),
					'desc_tip'    => true,
				),
				
				'description' => array(
					'title'       => __( 'Description', 'woo-maxicash-gateway' ),
					'type'        => 'textarea',
					'description' => __( 'Payment method description that the customer will see on your checkout.', 'woo-maxicash-gateway' ),
					'default'     => __( 'Payer par VISA, MasterCard, Paypal, MaxiCash et Pepele Mobile', 'woo-maxicash-gateway' ),
					'desc_tip'    => true,
				),
				 'gateway_mode' => array(
                    'title' => __('Mode'),
                    'type' => 'select',
                    'options' => array('test'=>'Test','live'=>'Live'),
                    'description' => ''
                ),
				'language_mode' => array(
                    'title' => __('Language'),
                    'type' => 'select',
                    'options' => array('fr'=>'Francais','en'=>'English'),
                    'description' => ''
                ),
				 'thankyou_page_id' => array(
                    'title' => __('Thank you Page'),
                    'type' => 'select',
                    'options' => $this->get_pages('Select Page'),
                    'description' => "URL of thank you page. For create new thank you page <a href='post-new.php?post_type=page' target='_blank' >click here</a> "
                )
			) );
		}
	  
	         // get all pages
	   public function get_pages($title = false, $indent = true) {
			$wp_pages = get_pages('sort_column=menu_order');
			$page_list = array();
			if ($title) $page_list[] = $title;
			foreach ($wp_pages as $page) {
				$prefix = '';
				// show indented child pages?
				if ($indent) {
					$has_parent = $page->post_parent;
					while($has_parent) {
						$prefix .=  ' - ';
						$next_page = get_page($has_parent);
						$has_parent = $next_page->post_parent;
					}
				}
				// add to page list array array
				$page_list[$page->ID] = $prefix . $page->post_title;
			}
			
			return $page_list;
		} 
		/**
		 * Output for the order received page.
		 */
		public function thankyou_page() {
			if ( $this->instructions ) {
				
				wpautop( wptexturize( $this->instructions ) );
			}
		}
	
	
		/**
		 * Add content to the WC emails.
		 *
		 * @access public
		 * @param WC_Order $order
		 * @param bool $sent_to_admin
		 * @param bool $plain_text
		 */
		public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {
		
			if ( $this->instructions && ! $sent_to_admin && $this->id === $order->payment_method && $order->has_status( 'on-hold' ) ) {
				echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
			}
		}
	
	
		function get_plugin_url(){
			if(isset($this->plugin_url)){
				return $this->plugin_url;
			}

			if(is_ssl()){
				return $this->plugin_url = str_replace('http://', 'https://', WP_PLUGIN_URL) . "/" . plugin_basename(dirname(dirname(__FILE__)));
			} else {
				return $this->plugin_url = WP_PLUGIN_URL . "/" . plugin_basename(dirname(dirname(__FILE__)));
			}
		}

		/**
		 * Process the payment and return the result
		 *
		 * @param int $order_id
		 * @return array
		 */
		public function process_payment( $order_id ) {
	
			$order = wc_get_order( $order_id );

			 return array('result' => 'success', 'redirect' => add_query_arg('order',
			$order->id, add_query_arg('key', $order->order_key, $order->get_checkout_payment_url( $on_checkout = true )))
       
        );

			// Mark as on-hold (we're awaiting the payment)
			$order->update_status( 'on-hold', __( 'Awaiting offline payment', 'woo-maxicash-gateway' ) );
			
			// Reduce stock levels
			$order->reduce_order_stock();
			
			// Remove cart
			WC()->cart->empty_cart();
			
			// Return thankyou redirect
			return array(
				'result' 	=> 'success',
				'redirect'	=> $order->get_checkout_order_received_url()
			);
		}
	
  } // end \WC_Gateway_Offline class
?>