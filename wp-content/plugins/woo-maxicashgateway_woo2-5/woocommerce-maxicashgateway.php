<?php
defined( 'ABSPATH' ) || exit; // Exit if accessed directly

/*
Plugin Name: Woo MaxiCash Gateway 
Description: Extends WooCommerce with MaxiCash Gateway.
Version:     2.4
Author:      Pluritone
Text Domain: woo-maxicash-gateway
*/


/** @class WC_MaxiCashGateway */
final class WC_MaxiCashGateway {
	 
	  /** @var WC_Logger Logger instance */
	public static $log = false;
	
	/** The single instance of the class. */
	protected static $_instance = null;
	
	/**
	 * Notices (array)
	 * @var array
	 */
	public $notices = array();
	
	
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	/**
	 * Constructor for the gateway.
	 */
	protected function __construct() {
		if ( ! defined( 'WC_MAXICASHGATEWAY_PLUGIN_FILE' ) ) {
			define( 'WC_MAXICASHGATEWAY_PLUGIN_FILE', __FILE__ );
		}
		
		add_action( 'admin_init', array( $this, 'check_environment' ) );
		add_action( 'admin_notices', array( $this, 'admin_notices' ), 15 );
		add_action( 'plugins_loaded', array( $this, 'init' ) );
		///add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'plugin_action_links' ) );
	}

	/**
	 * Display any notices collected.
	 *
	 * @return void
	 */
	public function admin_notices() {
		foreach ( (array) $this->notices as $notice ) {
			echo '<div class="' . esc_attr( $notice['class'] ) . '"><p><b>' . __( 'Woo MaxiCash Gateway', 'woo-maxicash-gateway' ) . ': </b>';
			echo wp_kses( $notice['message'], array( 'a' => array( 'href' => array() ) ) );
			echo '</p></div>';
		}
	}
	/**
	 * Checks the environment for compatibility problems.
	 *
	 * @return void
	 */
	public function check_environment() {
		if ( is_admin() && current_user_can( 'activate_plugins' ) && ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			$class = 'notice notice-error is-dismissible';
			$message = __( 'This plugin requires <a href="https://wordpress.org/plugins/woocommerce/">WooCommerce</a> to be active.', 'woo-maxicash-gateway' );
			$this->add_admin_notice( $class, $message );
			// Deactivate the plugin
			deactivate_plugins( __FILE__ );
			return;
		}
		
		$php_version = phpversion();
		$required_php_version = '5.3';
		if ( version_compare( $required_php_version, $php_version, '>' ) ) {
			$class = 'notice notice-warning is-dismissible';
			$message = sprintf( __( 'Your server is running PHP version %1$s but some features requires at least %2$s.', 'woo-maxicash-gateway' ), $php_version, $required_php_version );
			$this->add_admin_notice( $class, $message );
		}
		
		if ( ! in_array( get_woocommerce_currency(), apply_filters( 'wc_pelecard_gateway_allowed_currencies', array( 'ILS', 'USD', 'EUR' ) ) ) ) {
			$class = 'notice notice-error is-dismissible';
			$message = __( 'No support for your store currency.', 'woo-maxicash-gateway' );
			$this->add_admin_notice( $class, $message );
		}
	}
	/**
	 * Add admin notices.
	 *
	 * @param string $class
	 * @param string $message
	 */
	public function add_admin_notice( $class, $message ) {
		$this->notices[] = array(
			'class'   => $class,
			'message' => $message
		);
	}
	
	/**
	 * Logging method.
	 *
	 * @param string $message
	 */
	public static function log( $message ) {
		if ( empty( self::$log ) ) {
			self::$log = new WC_Logger();
		}
		self::$log->add( 'wc_maxicash_gateway', $message );
	}
	
	/**
	 * Init the plugin after plugins_loaded so environment variables are set.
	 *
	 * @return void
	 */
	public function init() {
		// bail
		if ( ! class_exists( 'WC_Payment_Gateway' ) ) {
			return;
		}	
		// Includes
		include_once( dirname( __FILE__ ) . '/includes/woo-class-maxicash-gateway.php' );
			
		// Init metabox
		//WC_Pelecard_Metabox::instance();
		
		// Init the gateway itself
		$this->init_gateways();
		
		// Check gateways response
		add_action( 'wp', array( $this, 'handle_api_requests' ), 10 );
	}
	
	public function handle_api_requests()
	{
		if(!isset($_REQUEST['wc-ajax']) && $_REQUEST['reference']!='')
		{
		$status=$_REQUEST['status'];
		$order_id=$_REQUEST['reference'];
		include_once( dirname( __FILE__ ) . '/includes/woo-class-maxicash-gateway.php' );
		$obj=new WC_Gateway_MaxiCashGateway; 
		$thank_p=get_permalink($obj->get_option( 'thankyou_page_id' ));
	
		  try{
			        $order = new WC_Order( $order_id );
					
                   	if($status=="success" && $order_id!='')
		            { 
						
						
						$order->payment_complete();
						
						// Mark as on-hold (we're awaiting the payment)
						$order->update_status( 'processing', __( 'Processing', 'woo-maxicash-gateway' ) );
						
						// Reduce stock levels
						$order->reduce_order_stock();
						
						// Remove cart
						WC()->cart->empty_cart();
						
						echo "<script>location.href='". $order->get_checkout_order_received_url() ."'</script>";   
						
						exit;
				    }else
					{
						$this->msg['message']= __( 'Error ! payment has been failed', 'woo-maxicash-gateway' );
						$order->update_status('failed');
						//$order->add_order_note('Failed');
						$order->add_order_note($this->msg['message']);
						add_action('the_content', array(&$this, 'showMessage'));
					}
				
			}catch(Exception $e){
				// $errorOccurred = true;
				echo $msg = "Error :".$e; exit; 
			} 	
		}
		
	}
	public function showMessage($content){
            return '<div class="woocommerce-NoticeGroup woocommerce-NoticeGroup-checkout"><ul class="woocommerce-error">
			<li><strong>'.$this->msg['message'].'</strong></li></ul></div>'.$content;
        }
	/**
	 * Initialize the gateway.
	 *
	 * @return void
	 */
	public function init_gateways() {
		load_plugin_textdomain( 'woo-maxicash-gateway' );
		add_filter( 'woocommerce_payment_gateways', array( $this, 'woocommerce_add_maxicash_gateway' ) );
	}
	/**
	 * Add the gateways to WooCommerce.
	 *
	 * @param array $methods
	 */
	public function woocommerce_add_maxicash_gateway( $gateways ) {
		$gateways[] = 'WC_Gateway_MaxiCashGateway';
	    return $gateways;
	}	

}
/**
 * @return WC_Pelecard
 */
function WC_MaxiCashGateway() {
	return WC_MaxiCashGateway::instance();
}
// Global for backwards compatibility.
$GLOBALS['wc_maxicashgateway'] = WC_MaxiCashGateway();