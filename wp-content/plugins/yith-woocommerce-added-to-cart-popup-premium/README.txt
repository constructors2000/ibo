== Changelog ==

= 1.4.1 = Released: on Sep 11, 2018

* Fix: Mini cart not updated after removing of the last item and closing the popup.
* Fix: Mini cart not updated after "add to cart" action in single product page.

= 1.4.0 = Released: on Sep 10, 2018

* New: Support to WooCommerce 3.4.5.
* New: Support to WordPress 4.9.8.
* New: Floating mini Cart feature.
* New: Compatibility with YITH WooCommerce Frequently Bought Together.
* New: Template yith-wacp-popup-cart-info.php for cart info in popup ( total, shipping etc.. ).
* Update: Plugin core.
* Update: Language files.
* Fix: Fully support to grouped product in "Added Product" popup content.
* Fix: Trigger "wc_fragment_refresh" when a product is removed from the popup.
* Fix: Add same product in different languages using WPML.
* Dev: Refactoring of Ajax "add to cart" in single product page.
* Dev: Template yith-wacp-popup-cart.php to version 1.4.0.
* Dev: Template yith-wacp-popup-product.php to version 1.4.0.

= 1.3.6 = Released: on Jul 02, 2018

* Fix: Ajax add to cart in single product page.

= 1.3.5 = Released: on Jun 28, 2018

* New: Support to WooCommerce 3.4.2.
* Fix: prevent plugin from use on external products.
* Fix: variation details showed also when the option "Show product variations" is disabled.

= 1.3.4 = Released: on May 30, 2018

* New: Support to WooCommerce 3.4.1.
* New: Support to WordPress 4.9.6.
* Update: Italian language.
* Update: Spanish language.
* Fix: Compatibility issue with YITH WooCommerce Deposits and Down Payments.

= 1.3.3 = Released on Apr 19, 2018

* Fix: WooCommerce cart widget was not updating automatically changing product quantity inside the popup

= 1.3.2 = Released on Apr 18, 2018

* Update: Plugin Core.
* Fix: Update product quantity in quote table.

= 1.3.1 = Released on Apr 11, 2018

* New: Support to WooCommerce 3.3.5.
* New: Support to WordPress 4.9.5.
* Update: Plugin Core.
* Fix: AJAX submit for add to cart form with product add-ons.
* Fix: Issue with latest version FB Pixel Plugin.
* Fix: YIT_Upgrade class missing.
* Fix: Compatiblity issue with YITH WooCommerce Request a Quote

= 1.3.0 = Released on Jan 31, 2018

* New: Support to WooCommerce 3.3.0.
* New: Support to WordPress 4.9.2.
* New: Dutch translation.
* New: Update product's quantity directly from the popup (available only for cart layout).
* Update: Plugin Core.
* Update: Languages Files.
* Fix: Responsive style for suggested products.

= 1.2.8 = Released on Dec 05, 2017

* New: Support to WooCommerce 3.2.5.
* New: Support to WordPress 4.9.1.
* New: Compatibility with YITH WooCommerce Request A Quote.
* New: Compatibility with WooCommerce Product Bundles.
* Update: Plugin Core.
* Update: Languages Files.
* Update: Perfect-Scrollbar library.
* Fix: Shipping costs always visible on table.

= 1.2.7 = Released on Nov 07, 2017

* New: Support to WooCommerce 3.2.3.
* New: Support to WordPress 4.8.3.
* New: Compatibility with YITH WooCommerce Gift Cards.
* Update: Plugin Core.

= 1.2.6 = Released on Oct 12, 2017

* New: Support to WooCommerce 3.2.0.
* Update: Plugin Core.

= 1.2.5 = Released on Sep 26, 2017

* New: Support to WooCommerce 3.1.2.
* New: Support to WordPress 4.8.2.
* Update: Spanish translation files.

= 1.2.4 = Released on Sep 07, 2017

* New: Support to WooCommerce 3.1.2.
* Fix: Scroll problem with Firefox browser.

= 1.2.3 = Released on Jul 03, 2017

* New: Support to WooCommerce 3.1.0.
* New: Compatibility with YITH WooCommerce Quick View Premium.
* Update: Plugin Core.
* Update: Language files.
* Fix: Compatibility issue with YITH WooCommerce Dynamic Pricing and Discount.
* Fix: Auto close popup if cart is empty.
* Fix: Hide shipping cost when no shipping cost are calculated.
* Tweak: Destroy PerfectScrollbar after popup close, to avoid problems with next opening.

= 1.2.2 = Released on May 04, 2017

* New: Support to WooCommerce 3.0.5.
* Update: Plugin Core.
* Fix: Get correct attributes on popup for variable products.
* Fix: Ajax add to cart from single product page for simple products.
* Fix: Get up-sells for variable product instead of single variation.
* Fix: Visibility tax query for related products and latest WooCommerce version.

= 1.2.1 = Released on Apr 07, 2017

* New: Support to WooCommerce 3.0.1.
* Update: Plugin Core.
* Fix: The items data for variable products on popup are now shown.

= 1.2.0 = Released on Mar 22, 2017

* New: Support to WooCommerce 3.0.0 RC1.
* New: Support to WordPress 4.7.3.
* New: Compatibility with FB Pixel Plugin.
* New: Compatibility with YITH WooCommerce Product Bundles Premium.
* New: Norwegian translation file .po.
* New: Template for popup button 'yith-wacp-popup-action.php'
* Update: Language files.
* Update: Plugin Core.
* Fix: Post Status condition for Related Products on popup.
* Dev: Add filter 'yith_wacp_check_is_admin' for method is_admin of class YITH_WACP.
* Dev: Add param WC Product $product to filter 'yith_wacp_popup_related_args'.

= 1.1.0 = Released on Oct 12, 2016

* New: Support to WooCommerce Google Analytics Pro plugin.
* New: Support to WooCommerce 2.6.4.
* New: Show tax amount on cart summary inside popup.
* New: Integration with YITH WooCommerce Cart Messages Premium.
* New: Integration with YITH WooCommerce Color and Label Variations Premium.
* New: Option to show "add to cart" for suggested products in popup.
* New: Option to select suggestes products type.
* Update: Change text domain from yith-wacp to yith-woocommerce-added-to-cart-popup.
* Update: Language files.
* Update: Plugin Core.

= 1.0.7 = Released on Jun 10, 2016

* New: Support to WooCommerce 2.6 RC1.
* Update: Plugin Core

= 1.0.6 = Released on May 27, 2016

* New: Spanish translation.
* Update: Plugin Core

= 1.0.5 = Released on May 23, 2016

* New: Support to WooCommerce 2.6 Beta2
* New: Italian translation file .po
* Update: Plugin Core
* Update: Language file

= 1.0.4 = Released on Apr 13, 2016

* New: Added minimized js files. Plugin loads full files version if the constant "SCRIPT_DEBUG" is defined and is true.
* New: Compatibility with YITH WooCommerce Waiting List
* New: Compatibility with WordPress 4.5
* Update: Change all frontend ajax call from admin-ajax to wc-ajax.
* Update: Plugin Core
* Update: Language file
* Fix: Option "Enable Popup" for archive page.
* Fix: Responsive style.

= 1.0.3 = Released on Dec 28, 2015

* New: Compatibility with Wordpress 4.4
* New: Compatibility with WooCommerce 2.5 BETA 3
* New: Option to show selected variation on popup
* New: XML config file for WPML
* New: Compatibility with Gravity Form Product Addons Plugin
* Update: Plugin Core
* Update: Language file
* Fix: Popup dimension on mobile device

= 1.0.2 = Released on Aug 13, 2015

* New: Compatibility with WooCommerce 2.4
* Update: Plugin Core
* Update: Language file

= 1.0.1 = Released on Jul 27, 2015

* Initial release