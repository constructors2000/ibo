<?php 

class ABZER_Networkonline extends WC_Payment_Gateway {

	

	// Setup our Gateway's id, description and other values

	function __construct() {

		// The global ID for this Payment method

		$this->id = "abzer_networkonline";

		// The Title shown on the top of the Payment Gateways Page next to all the other Payment Gateways

		$this->method_title = __( "Network Online Payment Gateway", 'abzer_networkonline' );

		// The description for this Payment Gateway, shown on the actual Payment options page on the backend

		$this->method_description = __( "<strong>Network Online Payment Gateway Plug-in for WooCommerce</strong>", 'abzer_networkonline' );

		// The title to be used for the vertical tabs that can be ordered top to bottom

		$this->title = __( "Network Online Payment Gateway", 'abzer_networkonline' );

		// If you want to show an image next to the gateway's name on the frontend, enter a URL to an image.

		$this->icon = null;

		// Bool. Can be set to true if you want payment fields to show on the checkout 

		// if doing a direct integration, which we are doing in this case

		$this->has_fields = false;

		

		// Load the form fields.

		$this->init_form_fields();

			

		// Include the Javascript for the selection of (Multi Currency & Single Currency ) 

		//wp_register_script('neo.js', plugin_dir_url( __FILE__ ) . 'assets/neo.js', array('jquery'), '' );

		//wp_enqueue_script('neo.js' );

		

		// Load the settings

		$this->init_settings();

		

		// Define Network Online set variables

		$this->title 			= $this->get_option('title');

		$this->merchant_id		= $this->get_option('merchant_id');

		$this->description 		= $this->get_option('description');

		$this->merchantid 		= $this->get_option('merchantid');

		$this->merchantkey 		= $this->get_option('merchantkey');

		$this->transactionmode  = $this->get_option('transactionmode');

		$this->transactiontype  = $this->get_option('transactiontype');

		// Bit Map settings 

		$this->billingdatablock = $this->get_option('billingdatablock');

		$this->shippingdatablock= $this->get_option('shippingdatablock');

		$this->merchantdatablock= $this->get_option('merchantdatablock');

		$this->otherdatablock	= $this->get_option('otherdatablock');

		

		// Set the Neo Variable 

		$this->neo = '';

		

		// To Add values to the database 

		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

		// Actions

		add_action('woocommerce_receipt_abzer_networkonline', array( $this, 'receipt_page'));

		// Response From Network Online 

		//add_action( 'woocommerce_thankyou' , array( $this, 'networkonline_response' ) );

		// Response From Network Online

		add_action('woocommerce_api_abzer_networkonline', array( $this, 'check_authorize_response') );

		// To display the DCC infromation if needed 

		add_action( 'woocommerce_order_details_after_order_table', array( $this,'networkonline_success_page_dcc'),10, 1);

		// Add the Dcc Infomation in Order Success page : 	

		add_action( 'woocommerce_email_order_meta', array( $this, 'filter_woocommerce_email_order_items_table' ), 10, 3 );

		// Display the error message : For the failed & Cancled orders 

		//add_action( 'woocommerce_thankyou',array( $this, 'networkonline_failure_response' ));

		

		// Add the multicurrency Values to setting page 

		if(isset($_POST['woocommerce_abzer_networkonline_multicurrency'])){

			if($_POST['woocommerce_abzer_networkonline_multicurrency']=='YES'){

				//$_POST['woocommerce_abzer_networkonline_merchantids_array'] = 'nothing';

				$merchantids_array = array();

				$_POST['woocommerce_abzer_networkonline_currency']= array_filter($_POST['woocommerce_abzer_networkonline_currency']);

				$_POST['woocommerce_abzer_networkonline_MID'] = array_filter($_POST['woocommerce_abzer_networkonline_MID']);

				$_POST['woocommerce_abzer_networkonline_KEY'] = array_filter($_POST['woocommerce_abzer_networkonline_KEY']);

				for($i=0;$i<count($_POST['woocommerce_abzer_networkonline_currency']);$i++){

					$merchantids_array[$_POST['woocommerce_abzer_networkonline_currency'][$i]] = array(

							$_POST['woocommerce_abzer_networkonline_currency'][$i],

							$_POST['woocommerce_abzer_networkonline_MID'][$i],

							$_POST['woocommerce_abzer_networkonline_KEY'][$i]

						);

				}

				$_POST['woocommerce_abzer_networkonline_merchantids_array'] 	  = serialize($merchantids_array);

				$_POST['woocommerce_abzer_networkonline_merchantid']  			  = '';

				$_POST['woocommerce_abzer_networkonline_merchantkey'] 			  = '';

			}else{

				$_POST['woocommerce_abzer_networkonline_merchantids_array'] = '';

			}

			

		}

	}

	/*

	// Display Error Messages for the failed and cancled orders. 

	public function networkonline_failure_response( $order_id ){

		

		global $woocommerce;

		global $wpdb;

		

		$order = new WC_Order($order_id);

		

			$sql 	= "SELECT * FROM {$wpdb->prefix}woocommerce_networkonline where order_id = '{$order->get_id()}'";

			$rows   = $wpdb->get_results( $sql ,'ARRAY_A' );

			

			if (count($rows)>0){

				$ary_neodata   = $rows[0];

					  if($ary_neodata['response']!=''){

						$reponseArray  = explode("||",$ary_neodata['response']);

						$blockEI 	   = $reponseArray[1]; // It has to contains Seven indicators

						$bitmapString  = str_split($blockEI);

						$blockEIArrayKey= array(

												'Transaction_Response', 			   //Same as Request 

												'Transaction_related_information',    // Transaction related information 

												'Transaction_Status_information',    //  Transaction Status information 

												'Merchant_Information',    			//   Merchant Information 

												'Fraud_Block',    			       //    Fraud Block 

												'DCC_Block',    			      //     DCC Block 

												'Additional'    			     //      Additional Block Like Card Mask 

											);

						$bit 		  = 0;

						$blockEIArray = array();



						foreach($blockEIArrayKey as $blockValues){

							$blockEIArray[$blockValues] = $bitmapString[$bit];

							$bit++;

						}

						$blockEIArray = array_filter($blockEIArray);

						// Remove the first and second element from Array to map with the bit map values 

						array_shift($reponseArray);

						array_shift($reponseArray);

						

						$resposeAssignedArray = array();

						$res 				  = 0;

						foreach($blockEIArray as $key => $value){

								$resposeAssignedArray[$key] =  $reponseArray[$res];

							$res++;

						}

								$TransactionResposeValue['DataBlockBitmap']	= $blockEI;

						foreach($blockEIArrayKey as $key => $value){

								if(isset($resposeAssignedArray[$value]))

									$TransactionResposeValue[$value] = $resposeAssignedArray[$value];

								else

									$TransactionResposeValue[$value] = 'NULL';

						}

					  }

			    }	



			if($TransactionResposeValue['Transaction_Status_information']!='NULL'){

					$Transaction_Status_information = explode('|',$TransactionResposeValue['Transaction_Status_information']);

			}	

			 

			if ( $order->has_status( 'failed' ) ) {

				if($Transaction_Status_information[3]) {

					

					echo '<style type="text/css">';

						echo '.error_neo { background-color:#ef0101 !important; }';

						echo '.woocommerce-notice { display:none !important; }';

					echo '</style>';

					

					$html_error = '<p class="woocommerce-store-notice error_neo">';

					$html_error .= 'Network Online Payment Gateway Declined <b>'.$Transaction_Status_information[3].'</b>';	

					$html_error .= '</p>';

					echo $html_error;

				}	

			}

			

			if ( $order->has_status( 'cancelled' ) ) {

				

					echo '<style type="text/css">';

						echo '.error_neo { background-color:#ef0101 !important; }';

						echo '.woocommerce-notice { display:none !important; }';

					echo '</style>';

					

					$html_error = '<p class="woocommerce-store-notice error_neo">';

					$html_error .= 'Customer Canceled the order from Network Online Payment Gateway';

					$html_error .= '</p>';

					echo $html_error;

			}	

			

	}

	*/

	

	// Add the DCC information to email template for the customer opted for DCC Payment.

	public function filter_woocommerce_email_order_items_table( $order ){

		

		global $wpdb;

		

		 if($order->get_id()){	

		 

			$sql 	= "SELECT * FROM {$wpdb->prefix}woocommerce_networkonline where order_id = '{$order->get_id()}'";

			$rows   = $wpdb->get_results( $sql ,'ARRAY_A' );

			

			if (count($rows)>0){

				

				$ary_neodata   = $rows[0];

				

			  if($ary_neodata['response']!=''){

				  

				$reponseArray  = explode("||",$ary_neodata['response']);

				$blockEI 	   = $reponseArray[1]; // It has to contains Seven indicators

				$bitmapString  = str_split($blockEI);

				$blockEIArrayKey= array(

										'Transaction_Response', 			   //Same as Request 

										'Transaction_related_information',    // Transaction related information 

										'Transaction_Status_information',    //  Transaction Status information 

										'Merchant_Information',    			//   Merchant Information 

										'Fraud_Block',    			       //    Fraud Block 

										'DCC_Block',    			      //     DCC Block 

										'Additional'    			     //      Additional Block Like Card Mask 

									);

				$bit 		  = 0;

				$blockEIArray = array();



				foreach($blockEIArrayKey as $blockValues){

					$blockEIArray[$blockValues] = $bitmapString[$bit];

					$bit++;

				}

				$blockEIArray = array_filter($blockEIArray);

				// Remove the first and second element from Array to map with the bit map values 

				array_shift($reponseArray);

				array_shift($reponseArray);

				

				$resposeAssignedArray = array();

				$res 				  = 0;

				foreach($blockEIArray as $key => $value){

						$resposeAssignedArray[$key] =  $reponseArray[$res];

					$res++;

				}

						$TransactionResposeValue['DataBlockBitmap']	= $blockEI;

				foreach($blockEIArrayKey as $key => $value){

						if(isset($resposeAssignedArray[$value]))

							$TransactionResposeValue[$value] = $resposeAssignedArray[$value];

						else

							$TransactionResposeValue[$value] = 'NULL';

				}

				

				if($TransactionResposeValue['DCC_Block']!='NULL'){

					

					$responseDCC      = $TransactionResposeValue['DCC_Block'];

					$TransactionDCC   = explode('|',$responseDCC);

					

					$transaction_Response = $TransactionResposeValue['Transaction_Response'];

					$TransactionResponse  = explode('|',$transaction_Response);

					// Built the DCC Html values 

						if($TransactionDCC[1]=='YES'){

							$data['{card_type}'] 	= $TransactionResponse[5];

							$data['{payment_type}'] = $TransactionResponse[4]; 

							$data['{local_currency_amount}'] 	   = '<strong>'.$TransactionResponse[3].' '.$TransactionResponse[2].'</strong>'; 

							$data['{transcation_currency_amount}'] = '<strong>'.$TransactionDCC[2].' '.$TransactionDCC[3].'</strong>';

							$data['{exchnage_rate}']  			   = '<strong> 1 '.$TransactionResponse[2].' = '.$TransactionDCC[5] .' '.$TransactionDCC[3].'</strong>'; 

							$data['{conversion_fee}'] 			   = '<strong>'.$TransactionDCC[4].'%</strong>'; 

							$dcc_information = '<table cellpadding="0" cellspacing="0" border="0">

												<tr>

													<td class="box" style="border:1px solid #D6D4D4;background-color:#f8f8f8;padding:7px 0">

														<table class="table" style="width:100%">

															<tr>

																<td width="10" style="padding:7px 0">&nbsp;</td>

																<td style="padding:7px 0">

																	<font size="2" face="Open-sans, sans-serif" color="#555454">

																		<span style="color:#777">

																			<span style="color:#333"><strong>Card Type : </strong></span>'.$data['{card_type}'].'<br /><br />

																			<span style="color:#333"><strong>Payment Type : </strong></span>'.$data['{payment_type}'].'<br /><br />

																			<span style="color:#333"><strong>Local Currency & Amount : </strong></span> '.$data['{local_currency_amount}'].'<br /><br />

																			<span style="color:#333"><strong>Transaction Currency & Amount : </strong></span> '.$data['{transcation_currency_amount}'].' <br /><br />

																			<span style="color:#333"><strong>Exchange Rate : </strong></span>'.$data['{exchnage_rate}'].'<br /><br />

																			<span style="color:#333"><strong>Currency conversion fee : </strong></span> '.$data['{conversion_fee}'].'<br /><br />

																		</span>

																	</font>

																</td>

																<td width="10" style="padding:7px 0">&nbsp;</td>

															</tr>

														</table>

													</td>

												</tr>

												<tr>

													<td class="linkbelow" style="padding:7px 0">

														<font size="2" face="Open-sans, sans-serif" color="#555454">

															<span>You were offered a choice to pay in the merchant pricing currency or in the currency of your card and you chose to pay in the currency of your card.</span>

														</font>

													</td>

												</tr>

												</table>';

						}else{

							$dcc_information = '';

						}		

				}else{

					$dcc_information = '';

				}

				

			  }else{

				   $dcc_information = '';

			  }		

				

			}else{	

				$dcc_information = '';

			}

			

			if($dcc_information){

				

				$dccHtml   = '<section class=\"woocommerce-order-details\">';

				$dccHtml  .= '<h2 class=\"woocommerce-order-details__title\">DCC Information</h2>';

				$dccHtml  .= $dcc_information;

				$dccHtml  .= '</section>';

			

				echo $dccHtml;

			}

		 }	

	}

	

	// Add the DCC information On Success page for the DCC opted payment.  

	public function networkonline_success_page_dcc( $order ){

		

		global $wpdb;

		

		if($order){

			$sql 	= "SELECT * FROM {$wpdb->prefix}woocommerce_networkonline where order_id = '{$order->get_order_number()}'";

			$rows   = $wpdb->get_results( $sql ,'ARRAY_A' );

			

			if (count($rows)>0){

				$ary_neodata   = $rows[0];

				$reponseArray  = explode("||",$ary_neodata['response']);

				$blockEI 	   = $reponseArray[1]; // It has to contains Seven indicators

				$bitmapString  = str_split($blockEI);

				$blockEIArrayKey= array(

										'Transaction_Response', 			   //Same as Request 

										'Transaction_related_information',    // Transaction related information 

										'Transaction_Status_information',    //  Transaction Status information 

										'Merchant_Information',    			//   Merchant Information 

										'Fraud_Block',    			       //    Fraud Block 

										'DCC_Block',    			      //     DCC Block 

										'Additional'    			     //      Additional Block Like Card Mask 

									);

				$bit 		  = 0;

				$blockEIArray = array();



				foreach($blockEIArrayKey as $blockValues){

					$blockEIArray[$blockValues] = $bitmapString[$bit];

					$bit++;

				}

				$blockEIArray = array_filter($blockEIArray);

				// Remove the first and second element from Array to map with the bit map values 

				array_shift($reponseArray);

				array_shift($reponseArray);

				

				$resposeAssignedArray = array();

				$res 				  = 0;

				foreach($blockEIArray as $key => $value){

						$resposeAssignedArray[$key] =  $reponseArray[$res];

					$res++;

				}

						$TransactionResposeValue['DataBlockBitmap']	= $blockEI;

				foreach($blockEIArrayKey as $key => $value){

						if(isset($resposeAssignedArray[$value]))

							$TransactionResposeValue[$value] = $resposeAssignedArray[$value];

						else

							$TransactionResposeValue[$value] = 'NULL';

				}

				

				if($TransactionResposeValue['DCC_Block']!='NULL'){

					

					$responseDCC      = $TransactionResposeValue['DCC_Block'];

					$TransactionDCC   = explode('|',$responseDCC);

					

					$transaction_Response = $TransactionResposeValue['Transaction_Response'];

					$TransactionResponse  = explode('|',$transaction_Response);

					// Built the DCC Html values 

						if($TransactionDCC[1]=='YES'){

							$data['{card_type}'] 	= $TransactionResponse[5];

							$data['{payment_type}'] = $TransactionResponse[4]; 

							$data['{local_currency_amount}'] 	   = '<strong>'.$TransactionResponse[3].' '.$TransactionResponse[2].'</strong>'; 

							$data['{transcation_currency_amount}'] = '<strong>'.$TransactionDCC[2].' '.$TransactionDCC[3].'</strong>';

							$data['{exchnage_rate}']  			   = '<strong> 1 '.$TransactionResponse[2].' = '.$TransactionDCC[5] .' '.$TransactionDCC[3].'</strong>'; 

							$data['{conversion_fee}'] 			   = '<strong>'.$TransactionDCC[4].'%</strong>'; 

							$dcc_information_sucess = '<section class="woocommerce-order-details">

											<h2 class="woocommerce-order-details__title">DCC Infromation</h2>

											<table class="woocommerce-table woocommerce-table--order-details shop_table order_details">

												<thead>

													<tr>

														<th class="woocommerce-table__product-name product-name">DCC Labels</th>

														<th class="woocommerce-table__product-table product-total">Values</th>

													</tr>

												</thead>

												<tbody>

													<tr class="woocommerce-table__line-item order_item">

														<td class="woocommerce-table__product-name product-name">

															<strong class="product-quantity">Card Type :</strong>

														</td>

														<td class="woocommerce-table__product-total product-total">

															<span class="woocommerce-Price-currencySymbol">'.$data['{card_type}'].'</span>

														</td>

													</tr>

													<tr class="woocommerce-table__line-item order_item">

														<td class="woocommerce-table__product-name product-name">

															<strong class="product-quantity">Payment Type :</strong>

														</td>

														<td class="woocommerce-table__product-total product-total">

															<span class="woocommerce-Price-currencySymbol">'.$data['{payment_type}'].'</span>

														</td>

													</tr>

													<tr class="woocommerce-table__line-item order_item">

														<td class="woocommerce-table__product-name product-name">

															<strong class="product-quantity">Local Currency & Amount :</strong>

														</td>

														<td class="woocommerce-table__product-total product-total">

															<span class="woocommerce-Price-currencySymbol">'.$data['{local_currency_amount}'].'</span>

														</td>

													</tr>

													<tr class="woocommerce-table__line-item order_item">

														<td class="woocommerce-table__product-name product-name">

															<strong class="product-quantity">Transaction Currency & Amount :</strong>

														</td>

														<td class="woocommerce-table__product-total product-total">

															<span class="woocommerce-Price-currencySymbol">'.$data['{transcation_currency_amount}'].'</span>

														</td>

													</tr>

													<tr class="woocommerce-table__line-item order_item">

														<td class="woocommerce-table__product-name product-name">

															<strong class="product-quantity">Exchange Rate :</strong>

														</td>

														<td class="woocommerce-table__product-total product-total">

															<span class="woocommerce-Price-currencySymbol">'.$data['{exchnage_rate}'].'</span>

														</td>

													</tr>

													<tr class="woocommerce-table__line-item order_item">

														<td class="woocommerce-table__product-name product-name">

															<strong class="product-quantity">Currency Conversion Fee :</strong>

														</td>

														<td class="woocommerce-table__product-total product-total">

															<span class="woocommerce-Price-currencySymbol">'.$data['{conversion_fee}'].'</span>

														</td>

													</tr>

													<tr class="woocommerce-table__line-item order_item">

														<td colspan="2">You were offered a choice to pay in the merchant pricing currency or in the currency of your card and you chose to pay in the currency of your card.</td>

													</tr>

												</tbody>

											</table>

										</section>';

						}else{

							$dcc_information_sucess = '';

						}		

				}else{

					$dcc_information_sucess = '';

				}		

				

				

			}else{	

				$dcc_information_sucess = '';

			}

				

		}else{

						

				$dcc_information_sucess = '';

		}		

		

		echo $dcc_information_sucess;

	}

		

	/**

	 * Admin Options.

	 *

	 * Setup the email settings screen.

	 * Override this in your email.

	 *

	 * @since 1.0.0

	 */

	public function admin_options() {

            

        ?>

            <h3><?php _e('Network Online ', 'abzer_networkonline'); ?></h3>

            <p><?php _e('Network Online works by sending the user to Networkonline to enter their payment information.', 'abzer_networkonline'); ?></p>

            <table class="form-table">

			<?php

			

			// Generate the HTML For the settings form.

				$this->generate_settings_html();

			?>

			</table><!--/.form-table-->

            <script>

                    jQuery( function ( $ ) {

                        

                         $('select#woocommerce_abzer_networkonline_multicurrency' ).change( function() {

                            if($(this).val()==='YES'){

                                $('select#woocommerce_abzer_networkonline_multicurrency').closest('tr').next('tr').hide();

                                $('select#woocommerce_abzer_networkonline_multicurrency').closest('tr').next('tr').next('tr').hide();

								

								// Here we need to add another TR and provide the options to add multiple MID`s & KEY`s 

								var data = {};

								var ajaxurl = '<?php echo plugin_dir_url( __FILE__ );?>'+'assets/view/multicurrency.php';

								$.ajax({

										url:  ajaxurl,

										type: 'POST',

										data: data,

										beforeSend: function () {

												$('#woocommerce_abzer_networkonline_merchantids').html('loading...');

										},

										success: function (response) {

												$('#woocommerce_abzer_networkonline_merchantids').html(response);

										

												var add_button  = $("#add_field_button"); //Fields wrapper

												var wrapper     = $("#input_fields_wrap"); //Fields wrapper

												var x 			= 1; //initlal text box count

												$(add_button).click(function(e){ //on add input button click

													e.preventDefault();

													$(wrapper).append('<div class="woocommerce_attribute_data wc-metabox-content"><table cellpadding="0" cellspacing="0"><tr><td class="attribute_name"><div><label class="titledesc">Currency</label></div><div><input type="text" class="regular-input" name="woocommerce_abzer_networkonline_currency[]" value=""></div></td><td class="attribute_name"><div><label class="titledesc">Merchant Id</label></div><div><input type="text" class="regular-input" name="woocommerce_abzer_networkonline_MID[]" value=""></div></td><td class="attribute_name"><div><label class="titledesc">Merchant Key</label></div><div><input type="text" class="regular-input" style="width:380px !important;" name="woocommerce_abzer_networkonline_KEY[]" value=""></div></td></table><button type="button" class="button add_attribute button-remove remove_field">Remove</button></div>');

												});

												// Remove the Empty row 

												$(wrapper).on("click",".remove_field", function(e){ //user click on remove text

													e.preventDefault(); $(this).parent('div').remove(); x--;

												})		

												// Remove the row with Values

												$('.removeRow').click(function(event){

													event.preventDefault();

													$(this).closest('tr').remove();

												});	

												

										}	

								});	

								

								

                            }else{

                                $('select#woocommerce_abzer_networkonline_multicurrency').closest('tr').next('tr').show();

                                $('select#woocommerce_abzer_networkonline_multicurrency').closest('tr').next('tr').next('tr').show();

									// Here we need to remove the multiple MID`s & KEY`s 

								$('#woocommerce_abzer_networkonline_merchantids').html('');

							}

                        });

                        

						var multicurrency = '<?php echo $this->settings['multicurrency'];?>';

						if(multicurrency=='YES'){

							

							$('select#woocommerce_abzer_networkonline_multicurrency').closest('tr').next('tr').hide();

                            $('select#woocommerce_abzer_networkonline_multicurrency').closest('tr').next('tr').next('tr').hide();	

						

							var data = {};

								var ajaxurl = '<?php echo plugin_dir_url( __FILE__ );?>'+'assets/view/multicurrency.php';

								$.ajax({

										url:  ajaxurl,

										type: 'POST',

										data: data,

										beforeSend: function () {

												$('#woocommerce_abzer_networkonline_merchantids').html('loading...');

										},

										success: function (response) {

												$('#woocommerce_abzer_networkonline_merchantids').html(response);

												$('.removeRow').click(function(event){

													event.preventDefault();

													$(this).closest('tr').remove();

												});	

												

												var add_button  = $("#add_field_button"); //Fields wrapper

												var wrapper     = $("#input_fields_wrap"); //Fields wrapper

												var x 			= 1; //initlal text box count

												$(add_button).click(function(e){ //on add input button click

													e.preventDefault();

													$(wrapper).append('<div class="woocommerce_attribute_data wc-metabox-content"><table cellpadding="0" cellspacing="0"><tr><td class="attribute_name"><div><label class="titledesc">Currency</label></div><div><input type="text" class="regular-input" name="woocommerce_abzer_networkonline_currency[]" value=""></div></td><td class="attribute_name"><div><label class="titledesc">Merchant Id</label></div><div><input type="text" class="regular-input" name="woocommerce_abzer_networkonline_MID[]" value=""></div></td><td class="attribute_name"><div><label class="titledesc">Merchant Key</label></div><div><input type="text" class="regular-input" style="width:380px !important;" name="woocommerce_abzer_networkonline_KEY[]" value=""></div></td></table><button type="button" class="button add_attribute button-remove remove_field">Remove</button></div>');

												});

												

												$(wrapper).on("click",".remove_field", function(e){ //user click on remove text

													e.preventDefault(); $(this).parent('div').remove(); x--;

												})		

												

										}

								});

								

							}

							// For the displaying the Currency Values 

							$('body').on('click', '#refer', function (e){

								e.preventDefault();

								$(".container" ).toggle("slow");

							});

                    });

					

					

					

            </script>

        <?php 

        } 

	/**

	 * networkonline Gateway Settings Form Fields

	 *

	 * 

	 */

	public function init_form_fields() {

		$this->form_fields = array(

                            'enabled' => array(

                                            'title'       => __( 'Network Online', 'abzer_networkonline' ),

                                            'type' 	  => 'checkbox',

                                            'description' => __( 'This controls enables Network Online payment during checkout.', 'abzer_networkonline' ),

                                            'label'   	  => __( 'Enable Networkonline ', 'abzer_networkonline' ),

                                            'default'     => 'no',

                                            'desc_tip'    => true

                                    ),

                            'title' => array(

                                            'title'       => __( 'Title', 'abzer_networkonline' ),

                                            'type' 	  => 'text',

                                            'description' => __( 'This controls the title which the user see during checkout.', 'abzer_networkonline' ),

                                            'default'     => __( 'Networkonline', 'abzer_networkonline' ),

                                            'desc_tip'    => true,

                                    ),

							 'description' => array(

											'title' 	  => __( 'Description', 'abzer_networkonline' ),

											'type' 		  => 'textarea',

											'description' => __( 'This controls the description which the user sees during checkout.', 'abzer_networkonline' ),

											'default'     => __('NetworkOnline, the secure way to pay', 'abzer_networkonline'),

									),		

                             'multicurrency' =>array(

                                            'title' 	  => __( 'Multi Currency', 'abzer_networkonline' ),

                                            'type'        => 'select',

                                            'description' => __( 'This controls the ability to add multiple MID & KEYS for different currencies', 'abzer_networkonline' ),

                                            'default'     => 'NO',

                                            'desc_tip'    => true,

                                            'options'     => array(

                                                                    'NO'   => __( 'NO', 'abzer_networkonline' ),

                                                                    'YES'  => __( 'YES','abzer_networkonline' )

                                                                  )

                                    ),

                              'merchantid'		=>array(

                                            'title' 	  => __( 'Merchant ID', 'abzer_networkonline' ),

                                            'type'        => 'text',

                                            'description' => __( 'Please enter your Merchant ID', 'abzer_networkonline' ),

                                            'desc_tip'    => true

                                    ),

                              'merchantkey'	=>array(

                                            'title' 	  => __( 'Merchant KEY', 'abzer_networkonline' ),

                                            'type'        => 'text',

                                            'description' => __( 'Please enter your Merchant KEY', 'abzer_networkonline' ),

                                            'desc_tip'    => true

                                    ),

							   			

							   'merchantids' =>array(

											'title'       => __( '', 'abzer_networkonline' ),

											'type'        => 'title'

									),	

							   'merchantids_array' =>array(

										'title'       => __( '', 'abzer_networkonline' ),

										'type'        => 'hidden'

								),	

									

							   // Transaction Mode		

							   'transactionmode' =>array(

                                            'title' 	  => __( 'Transaction Mode', 'abzer_networkonline' ),

                                            'type'        => 'select',

                                            'description' => __( 'Please select Transaction Mode : TEST/LIVE', 'abzer_networkonline' ),

                                            'desc_tip'    => true,

											'default'     => 'TEST',

											'options'     => array(

                                                                    'TEST'  => __( 'TEST','abzer_networkonline' ),

                                                                    'LIVE'  => __( 'LIVE','abzer_networkonline' )

																)

									),

									// Transaction Type

								'transactiontype' =>array(

                                            'title' 	  => __( 'Transaction Type', 'abzer_networkonline' ),

                                            'type'        => 'select',

                                            'description' => __( 'Please select Transaction Type : SALES/AUTHROIZATION', 'abzer_networkonline' ),

                                            'desc_tip'    => true,

											'default'     => '01',

											'options'     => array(

                                                                    '01'  => __( 'SALES','abzer_networkonline' ),

                                                                    '02'  => __( 'AUTHROIZATION','abzer_networkonline' )

																)

									),

							    //Network Online Bit Map Settings Options

								'bitmapsetting'	 => array(

											'title' 	  => __( 'Network Online Bit Map Settings Options', 'abzer_networkonline' ),

											'type'        => 'title'

														),

								// Billing Data Block

								'billingdatablock'	=> array(

                                            'title' 	  => __( 'Billing Data Block', 'abzer_networkonline' ),

                                            'type'        => 'select',

                                            'description' => __( 'If you enable Billing Data Block, it will pass the billing information data to Network Online.', 'abzer_networkonline' ),

                                            'desc_tip'    => true,

											'default'     => '1',

											'options'     => array(

                                                                    '1'  => __( 'YES','abzer_networkonline' ),

                                                                    '0'  => __( 'NO','abzer_networkonline' )

																)

										),

								// Shipping Data Block

								'shippingdatablock'	=> array(

                                            'title' 	  => __( 'Shipping Data Block', 'abzer_networkonline' ),

                                            'type'        => 'select',

                                            'description' => __( 'If you enable Shipping Data Block it will pass the shipping information data to Network Online.', 'abzer_networkonline' ),

                                            'desc_tip'    => true,

											'default'     => '1',

											'options'     => array(

                                                                    '1'  => __( 'YES','abzer_networkonline' ),

                                                                    '0'  => __( 'NO','abzer_networkonline' )

																)

										),

								// Merchant Data Block

								'merchantdatablock'	=> array(

                                            'title' 	  => __( 'Merchant Data Block', 'abzer_networkonline' ),

                                            'type'        => 'select',

                                            'description' => __( 'If you enable Merchant Data Block it will pass the merchant information data to Network Online eg: Client IP Address , as of now we are only passing IP address.', 'abzer_networkonline' ),

                                            'desc_tip'    => true,

											'default'     => '1',

											'options'     => array(

                                                                    '1'  => __( 'YES','abzer_networkonline' ),

                                                                    '0'  => __( 'NO','abzer_networkonline' )

																)

										),

								// Other Data Block

								'otherdatablock'	=> array(

                                            'title' 	  => __( 'Other Data Block', 'abzer_networkonline' ),

                                            'type'        => 'select',

                                            'description' => __( 'If you enable Other Data Block it will pass the other information data to Network Online eg : ProductInfo , Category , ItemTotal.', 'abzer_networkonline' ),

                                            'desc_tip'    => true,

											'default'     => '1',

											'options'     => array(

                                                                    '1'  => __( 'YES','abzer_networkonline' ),

                                                                    '0'  => __( 'NO','abzer_networkonline' )

																)

										),

										

							);

	}

	

	/**

		Process the payment 

	**/

	public function process_payment($order_id){

		

			$order = new WC_Order($order_id);

			

			return array('result' => 'success', 'redirect' => $order->get_checkout_payment_url( true ));

			

	}

	/**

		generate networkonline form

	**/

	public function generate_networkonline_form( $order ) {


		    //echo '<pre>';
		//	print_r($this);
			//echo '</pre>';
		//	exit;
			
			global $woocommerce;

			global $wpdb; 

			

			$order = new WC_Order( $order );

			

			//error_log(print_r($order,true));

			

			$item_loop = 0;

			$items = $order->get_items();

			

			if (sizeof($items)>0) 

			{ 

				$item_name = '';

				foreach ($items as $item) 

				{	

					

					if ($item['qty']) {

						

						$item_loop++;



						$product 	 = $order->get_product_from_item( $item );

						$item_name 	.= ', '.$item['name'];

									

					}

				} 

			}

			// Need to check the MultiCurrency Option is been opted or not by merchant 

			if($this->settings['multicurrency']=='YES'){

				$merchantids_array = unserialize($this->settings['merchantids_array']);

				// Get the Order Currency Code 

				$currency_code     = $order->get_currency();

				// Check the array with the Order currency 

				if (array_key_exists($currency_code, $merchantids_array)) {

					$nolpaymerchantids = $merchantids_array[$currency_code];

				}

				if(isset($nolpaymerchantids)){

					$this->neo['merchant_id'] = $nolpaymerchantids[1];

					$this->neo['merchant_key']= $nolpaymerchantids[2];

				}		

			}
			
			if($this->settings['multicurrency']=='NO'){

				$this->neo['merchant_id']  	  = $this->merchantid;
				$this->neo['merchant_key'] 	  = $this->merchantkey;
				
				$this->neo = array("merchant_id"=>$this->merchantid, "merchant_key"=>$this->merchantkey);

			}

			
			// Set the MerchantKey in Session. 

			WC()->session->set('merchant_key', $this->neo['merchant_key']);


			$this->neo['transaction_mode']= $this->transactionmode;

			$this->neo['transaction_type']= $this->transactiontype;

			

			// Get the Bit Map option values 

			$this->neo['billing_option']  = $this->billingdatablock;

			$this->neo['shipping_option'] = $this->shippingdatablock;

			$this->neo['merchant_option'] = $this->merchantdatablock;

			$this->neo['other_option']    = $this->otherdatablock;

			

			$this->neo['neoPostErrors']   = array();

			//Validate the required fileds added from the admin side or not 

			if(($this->neo['merchant_id']=='')){

				$this->neo['neoPostErrors'][]  = __('Please Enter Merchant ID Through Admin Side.');

			}

			if(($this->neo['merchant_key']=='')){

				$this->neo['neoPostErrors'][]  = __('Please Enter Merchant Key Through Admin Side.');

			}

			if(($this->neo['transaction_mode']=='')){

				$this->neo['neoPostErrors'][]  = __('Please Select Transaction Mode Through Admin Side.');

			}

			if(($this->neo['transaction_type']=='')){

				$this->neo['neoPostErrors'][]  = __('Please Select Transaction Type Through Admin Side.');

			}

		

			// Validate the required fileds added from the admin for Bitmap Settings

			if(($this->neo['billing_option']=='')||($this->neo['billing_option']==0)){

				$this->neo['neoPostErrors'][]  = __('Please Enable Billing Option To Yes Through Admin Side .');

			}

			if(($this->neo['shipping_option']=='')||($this->neo['shipping_option']==0)){

				$this->neo['neoPostErrors'][]  = __('Please Enable Shipping Option To Yes Through Admin Side .');

			}

			if(($this->neo['merchant_option']=='')||($this->neo['merchant_option']==0)){

				$this->neo['neoPostErrors'][]  = __('Please Enable Merchant Option To Yes Through Admin Side .');

			}

			if(($this->neo['other_option']=='')||($this->neo['other_option']==0)){

				$this->neo['neoPostErrors'][]  = __('Please Enable Other Option To Yes Through Admin Side .');

			}

			

			// Here we need to Validate and display the error message : if exist 

			

			

		

			// Build the Request String 

			// Set the Neo ( Network Online Payment Integration ) variables 

			$this->neo['transactionDataBlock']= true;   																			// Transaction Data Block  ==> This is mandatory block  , 1

			$this->neo['billingDataBlock']    = $this->neo['billing_option'] ? $this->neo['billing_option'] : false;  		   	  // Billing Data Block      ==> This is an optional block ,0 

			$this->neo['shippingDataBlock']   = $this->neo['shipping_option']? $this->neo['shipping_option']: false; 		     // Shipping Data Block     ==> This is an optional block ,0 

			$this->neo['paymentDataBlock']    = false;  											 							// Payment Data Block      ==> This is mandatory block , 1

			$this->neo['merchantDataBlock']   = $this->neo['merchant_option']? $this->neo['merchant_option'] : false; 	       // Merchant Data Block     ==> This is an optional block ,0 

			$this->neo['otherDataBlock']      = $this->neo['other_option'] ? $this->neo['other_option'] : false ;   		  // Other Details Data Block==> This is an optional block ,0 

			$this->neo['DCCDataBlock']        = false;  										                             // DCC Data Block          ==> This is an optional block ,0			

			

			$this->neo['collaboratorId']      = 'NI';

			$this->neo['iv']      			  = '0123456789abcdef';

			

			$this->neo['encryptMethod']       = MCRYPT_RIJNDAEL_128;

			$this->neo['encryptMode']      	  = MCRYPT_MODE_CBC;

			$this->neo['payModeTypeCard']     = array('CC','DC','DD');

			$this->neo['payModeTypeNetBank']  = array('NB');

			

			if($this->neo['transaction_mode']=='TEST')

				$this->neo['url'] 			  = 'https://uat-NeO.network.ae/direcpay/secure/PaymentTxnServlet';

			else

				$this->neo['url'] 			  = 'https://NeO.network.ae/direcpay/secure/PaymentTxnServlet';

		

			// Block block Existence Indicator 

			$this->neo['blockEI'] 			  = $this->setblockExistenceIndicator();

			// • Transaction Block Variables   	

			$this->neo['merchantOrderNumber'] =  $order->get_id();

			$this->neo['amount']  		   	  =  number_format($order->get_total(), 2, '.', '');

			//$this->neo['amount']  		 	  =  '4004.00';

			$this->neo['successUrl']     	  =  site_url('/').'wc-api/abzer_networkonline/';

			$this->neo['failureUrl']          =  site_url('/').'wc-api/abzer_networkonline/';

			$this->neo['transactionMode']     =  'INTERNET';

			$this->neo['payModeType']         =  'CC';

			$this->neo['transactionType']     =  $this->neo['transaction_type'];

			$this->neo['currency']            =  $order->get_currency(); 	

			

			$this->neo['transactionArray']    =  array(

												$this->neo['merchantOrderNumber'],

												$this->neo['amount'],

												$this->neo['successUrl'],

												$this->neo['failureUrl'],

												$this->neo['transactionMode'],

												$this->neo['payModeType'],

												$this->neo['transactionType'],

												$this->neo['currency'],

											);

			$this->neo['TransactionFEI']   	   = $this->setTransactionFieldExistenceIndicator();									

			$this->neo['TransactionData']      = join('|',$this->neo['transactionArray']);

			$this->neo['TransactionDataBlock1']= $this->neo['TransactionFEI'].'|'.$this->neo['TransactionData'];

			

			// • Billing Block Variables 

			$this->neo['billToFirstName']  		= $order->get_billing_first_name() ? substr($order->get_billing_first_name(),0,50):'';	

			$this->neo['billToLastName']  		= $order->get_billing_last_name() ? substr($order->get_billing_last_name(),0,50):'';	

			$this->neo['billToStreet1']  		= $order->get_billing_address_1() ? substr($order->get_billing_address_1(),0,40): 'Dubai' ;	

			$this->neo['billToStreet2']  		= $order->get_billing_address_2() ? substr($order->get_billing_address_2(),0,40): 'Dubai' ;	

			$this->neo['billToCity']  			= $order->get_billing_city() ? substr($order->get_billing_city(),0,50): '' ;	

			$this->neo['billToState']  			= $order->get_billing_state() ? substr($order->get_billing_state(),0,20): '' ;	

			$this->neo['billtoPostalCode']  	= $order->get_billing_postcode() ? substr($order->get_billing_postcode(),0,9): '456789' ;	

			$this->neo['billToCountry']  		= $order->get_billing_country() ? substr($order->get_billing_country(),0,2): '' ;	

			$this->neo['billToEmail']  			= $order->get_billing_email() ? substr($order->get_billing_email(),0,100):'';	

			$this->neo['billToMobileNumber']  	= $order->get_billing_phone() ? substr($order->get_billing_phone(),0,15): '' ;	

			$this->neo['billToPhoneNumber1']  	= $order->get_billing_phone() ? substr($order->get_billing_phone(),0,3): '' ;	

			$this->neo['billToPhoneNumber2']  	= $order->get_billing_phone() ? substr($order->get_billing_phone(),3,3): '' ;	

			$this->neo['billToPhoneNumber3']  	= $order->get_billing_phone() ? substr($order->get_billing_phone(),6,4): '' ;	

			$this->neo['billingArray']          = array(

														 $this->neo['billToFirstName'],

														 $this->neo['billToLastName'],

														 $this->neo['billToStreet1'],

														 $this->neo['billToStreet2'],	

														 $this->neo['billToCity'],	

														 $this->neo['billToState'],	

														 $this->neo['billtoPostalCode'],	

														 $this->neo['billToCountry'],	

														 $this->neo['billToEmail'],	

														 $this->neo['billToMobileNumber'],	

														 $this->neo['billToPhoneNumber1'],	

														 $this->neo['billToPhoneNumber2'],	

														 $this->neo['billToPhoneNumber3']	

														);

			$this->neo['billingArray'] 			= $this->neo['billingDataBlock'] ? $this->neo['billingArray'] : array();

		

			$this->neo['BillingFEI']   	    	= $this->setBillingFieldExistenceIndicator();

			$this->neo['BillingData']      		= join('|',array_filter($this->neo['billingArray']));

			$this->neo['BillingDataBlock2']     = $this->neo['billingDataBlock'] ? $this->neo['BillingFEI'].'|'.$this->neo['BillingData'] : '';

			

			

			// • Shipping Data Block Variables 

			$this->neo['shipToFirstName']       = $order->get_shipping_first_name() ? substr($order->get_shipping_first_name(),0,50):substr($order->get_billing_first_name(),0,50);

			$this->neo['shipToLastName']        = $order->get_shipping_last_name() ? substr($order->get_shipping_last_name(),0,50):'';

			$this->neo['shipToStreet1']         = $order->get_shipping_address_1() ? substr($order->get_shipping_address_1(),0,40):'';

			$this->neo['shipToStreet2']         = $order->get_shipping_address_2() ? substr($order->get_shipping_address_2(),0,40):'';

			$this->neo['shipToCity']            = $order->get_shipping_city() ? substr($order->get_shipping_city(),0,50): '' ;

			$this->neo['shipToState']           = $order->get_shipping_state() ? substr($order->get_shipping_state(),0,20): '';

			$this->neo['shipToPostalCode']      = $order->get_shipping_postcode() ? substr($order->get_shipping_postcode(),0,9): '' ;

			$this->neo['shipToCountry']         = $order->get_shipping_country() ? substr($order->get_shipping_country(),0,2): '' ;

			$this->neo['shipToPhoneNumber1']    = '';

			$this->neo['shipToPhoneNumber2']    = '';

			$this->neo['shipToPhoneNumber3']    = '';

			$this->neo['shipToMobileNumber']    = '';

			$this->neo['shippingArray']         = array(

														 $this->neo['shipToFirstName'],

														 $this->neo['shipToLastName'],

														 $this->neo['shipToStreet1'],

														 $this->neo['shipToStreet2'],	

														 $this->neo['shipToCity'],	

														 $this->neo['shipToState'],	

														 $this->neo['shipToPostalCode'],	

														 $this->neo['shipToCountry'],	

														 $this->neo['shipToPhoneNumber1'],	

														 $this->neo['shipToPhoneNumber2'],	

														 $this->neo['shipToPhoneNumber3'],	

														 $this->neo['shipToMobileNumber']	

														);

			$this->neo['shippingArray']         = $this->neo['shippingDataBlock'] ? $this->neo['shippingArray'] : array();

			

			$this->neo['ShippingFEI']   	    = $this->setShippingFieldExistenceIndicator();											

			$this->neo['ShippingData']      	= join('|',array_filter($this->neo['shippingArray']));											

			$this->neo['ShippingDataBlock3']    = $this->neo['shippingDataBlock'] ? $this->neo['ShippingFEI'].'|'.$this->neo['ShippingData'] : '';

			

			// • Payment Data Block Variables

			// This Block is mandatory only if we are using Merchant hosted payment 

			// As of now we are using PSP , Payment Service Provider hosting , So it is an Optional parameter 

			$this->neo['cardNumber']  	        = ''; // 1. Card Number  

			$this->neo['expMonth']  		    = ''; // 2. Expiry Month 

			$this->neo['expYear']  		        = ''; // 3. Expiry Year

			$this->neo['CVV']  			        = ''; // 4. CVV  

			$this->neo['cardHolderName']        = ''; // 5. Card Holder Name 

			$this->neo['cardType']  		    = ''; // 6. Card Type

			$this->neo['custMobileNumber']      = ''; // 7. Customer Mobile Number

			$this->neo['paymentID']  		    = ''; // 8. Payment ID 

			$this->neo['OTP']  			        = ''; // 9. OTP  

			$this->neo['gatewayID']  		    = ''; // 10.Gateway ID 

			$this->neo['cardToken']   	        = ''; // 11.Card Token 

			$this->neo['paymentArray']          = array(

														$this->neo['cardNumber'],

														$this->neo['expMonth'],

														$this->neo['expYear'],

														$this->neo['CVV'],

														$this->neo['cardHolderName'],

														$this->neo['cardType'],

														$this->neo['custMobileNumber'],

														$this->neo['paymentID'],

														$this->neo['OTP'],

														$this->neo['gatewayID'],

														$this->neo['cardToken']

														);

			$this->neo['paymentArray'] 		    = $this->neo['paymentDataBlock'] ? $this->neo['paymentArray'] : array();	

			

			$this->neo['PaymentFEI']   	    	= $this->setPaymentFieldExistenceIndicator();

			$this->neo['PaymentData']      	    = join('|',array_filter($this->neo['paymentArray']));

			$this->neo['PaymentDataBlock4']     = $this->neo['paymentDataBlock'] ? $this->neo['PaymentFEI'].'|'.$this->neo['PaymentData'] : '';



			// • Merchant Data Block Variables

			$this->neo['UDF1']   				= $order->get_customer_ip_address() ? $order->get_customer_ip_address()  : $_SERVER['REMOTE_ADDR'];  // This is a ‘user-defined field’ .

			$this->neo['UDF2']   				= '';  // This is a ‘user-defined field’ .

			$this->neo['UDF3']   				= '';  // This is a ‘user-defined field’ .

			$this->neo['UDF4']   				= '';  // This is a ‘user-defined field’ . 

			$this->neo['UDF5']   				= '';  // This is a ‘user-defined field’ .

			$this->neo['UDF6']   				= '';  // This is a ‘user-defined field’ .

			$this->neo['UDF7']   				= '';  // This is a ‘user-defined field’ .

			$this->neo['UDF8']   				= '';  // This is a ‘user-defined field’ .

			$this->neo['UDF9']   				= '';  // This is a ‘user-defined field’ .

			$this->neo['UDF10']  				= '';  // This is a ‘user-defined field’ .	

			$this->neo['merchantArray']  		= array(

														$this->neo['UDF1'],

														$this->neo['UDF2'],

														$this->neo['UDF3'],

														$this->neo['UDF4'],

														$this->neo['UDF5'],

														$this->neo['UDF6'],

														$this->neo['UDF7'],

														$this->neo['UDF8'],

														$this->neo['UDF9'],

														$this->neo['UDF10']

														);

			$this->neo['merchantArray'] 		= $this->neo['merchantDataBlock'] ? $this->neo['merchantArray'] : array();

			

			$this->neo['MerchantFEI']   	    = $this->setMerchantFieldExistenceIndicator();

			$this->neo['MerchantData']      	= join('|',array_filter($this->neo['merchantArray']));

			$this->neo['MerchantDataBlock5']    = $this->neo['merchantDataBlock'] ? $this->neo['MerchantFEI'].'|'.$this->neo['MerchantData'] : '';

			

			// • Other Details Data Block Variables

			$this->neo['custID']			     = $order->get_customer_id() ? $order->get_customer_id() : '';    // The ID used to identify your customer when their profile was created. 

			$this->neo['transactionSource']      = 'Web';  										   // This is used to identify a channel, Acceptable values are: Web, IVR, or Mobile. 					

			$this->neo['productInfo']            = substr( wp_specialchars_decode(get_option('blogname'), ENT_QUOTES)." ". str_replace(',','',$item_name) ,0,100 ) ;  // This field is used to send details about the product and related information.						

			$this->neo['isUserLoggedIn']         = $order->get_customer_id() ? 'Y':'N';  				 // Indicates whether a customer has signed in to your website, or has Guest Values: 'Y' & 'N’ 	 						

			$this->neo['itemTotal']              = $order->get_total() ? substr($order->get_total(),0,100) :'';   // Example: If the order has two items of value 500 and 1000, send the value as '500.00, 1000.00'. Up to 2 decimal places are allowed in the value.						

			$this->neo['itemCategory']           = '';  									   // This is a comma-separated list of the categories of all the items in the order.							

			$this->neo['ignoreValidationResult'] = 'FALSE';  								  // Indicates whether you want to proceed for Authorization irrespective of the 3DSecure/Authentication result, or not. TRUE/FALSE

			$this->neo['otherDataArray']         = array(

														$this->neo['custID'],	

														$this->neo['transactionSource'],	

														$this->neo['productInfo'],	

														$this->neo['isUserLoggedIn'],	

														$this->neo['itemTotal'],	

														$this->neo['itemCategory'],	

														$this->neo['ignoreValidationResult']	

														);

			$this->neo['otherDataArray'] 		= $this->neo['otherDataBlock'] ? $this->neo['otherDataArray'] : array();

			

			$this->neo['OtherFEI']   	    	= $this->setOtherDetailsFieldExistenceIndicator();

			$this->neo['OtherData']      		= join('|',array_filter($this->neo['otherDataArray']));

			$this->neo['OtherDataBlock6']       = $this->neo['otherDataBlock'] ? $this->neo['OtherFEI'].'|'.$this->neo['OtherData'] : '';

			

			// • DCC Data Block Variables

			$this->neo['DCCReferenceNumber'] 	= ''; // DCC Reference Number

			$this->neo['foreignAmount']	   		= ''; // Foreign Amount

			$this->neo['ForeignCurrency']    	= ''; // Foreign Currency

			$this->neo['DCCArray']        		= array(

														$this->neo['DCCReferenceNumber'],

														$this->neo['foreignAmount'],

														$this->neo['ForeignCurrency']

														);

			$this->neo['DCCArray'] 				= $this->neo['DCCDataBlock'] ? $this->neo['DCCArray'] : array();

			

			$this->neo['DCCFEI']  				= $this->setDCCFieldExistenceIndicator();

			$this->neo['DCCData']      			= join('|',array_filter($this->neo['DCCArray']));

			$this->neo['DCCDataBlock7']       	= $this->neo['DCCDataBlock'] ? $this->neo['DCCFEI'].'|'.$this->neo['DCCData'] : '';

			

			// Block_Existence_Indicator Bit map and DataBlock`s Array

			$this->neo['DataBlocksArray']       = array(

														'DataBlockBitmap' => $this->neo['blockEI'], 

														'DataBlock1' => $this->neo['TransactionDataBlock1'], 

														'DataBlock2' => $this->neo['billingDataBlock'] ? $this->neo['BillingDataBlock2']:'',

														'DataBlock3' => $this->neo['shippingDataBlock'] ? $this->neo['ShippingDataBlock3']:'',

														'DataBlock4' => $this->neo['paymentDataBlock'] ? $this->neo['PaymentDataBlock4']:'',

														'DataBlock5' => $this->neo['merchantDataBlock'] ? $this->neo['MerchantDataBlock5']:'',

														'DataBlock6' => $this->neo['otherDataBlock'] ? $this->neo['OtherDataBlock6']:'',

														'DataBlock7' => $this->neo['DCCDataBlock'] ? $this->neo['DCCDataBlock7']:''

														);

			$this->neo['beforeEncryptionString'] = join('||',array_filter($this->neo['DataBlocksArray']));
			
		   
		
	
			
			$this->neo['EncryptedString']	     = $this->encryptData($this->neo['beforeEncryptionString'],$this->neo['merchant_key'],$this->neo['iv']);

			$this->neo['postString']             = $this->neo['merchant_id'].'||'.$this->neo['collaboratorId'].'||'.$this->neo['EncryptedString'];


			$text = $this->neo['merchant_id'].'||'.$this->neo['collaboratorId'].'||'.$this->neo['beforeEncryptionString'];

	
			// Display the error before going to Network Online

			if(count($this->neo['neoPostErrors']) > 0 ){

				return wc_add_notice(join("<br/>",$this->neo['neoPostErrors']),  $notice_type = 'error' );

			}	

			

			// Before redirecting to Network Online Payment Gateway add the request string to Network Online table (woocommerce_networkonline)

			$networkonlinesql = "( '".esc_sql($order->get_order_key()). "' ,'".$order->get_id()."', '".esc_sql($text)."', 'D', 'wc-pending', '".date("Y-m-d H:i:s")."' )";

			$wpdb->query( "

						INSERT IGNORE INTO {$wpdb->prefix}woocommerce_networkonline 

						( order_key, order_id, request, type , status , created_on ) VALUES $networkonlinesql;

						" );

			// Add Order Note Customer Redirecting to Network Online Site 

			$order->add_order_note('Customer was redirected to Network Online Site');

		

			return '

			<html>

				<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>	

				<script>

					setTimeout ( "dp_woo()" , 1 );

					function dp_woo(){

						document.networkonline_checkout.submit();

					}

				</script>

				<p class="message success">

					Redirtecing to <strong>Network Online Payment Gateway</strong>, please wait.

				</p>

				<form action="'.$this->neo['url'].'" method="post" id="networkonline_checkout" name="networkonline_checkout">

					<input type="hidden" name="requestParameter" value="'.$this->neo['postString'].'">

					<input type="submit" class="button wc-forward" value="Pay via NetworkOnline" name="suba" id="sub">

				</form>

			</html>';

			

			

			

			/*

			wc_enqueue_js('

				$.blockUI({

						message: "' . esc_js( __( 'Thank you for your order. We are now redirecting you to Networkonline to make payment.', 'abzer_networkonline' ) ) . '",

						baseZ: 99999,

						overlayCSS: { 

								background: "#fff", 

								opacity: 0.6 

							},

						css: { 

							padding:        "20px",

							zindex:         "9999999",

							textAlign:      "center", 

							color:          "#555", 

							border:         "3px solid #aaa", 

							backgroundColor:"#fff", 

							cursor:         "wait",

							lineHeight:		"32px",

							width:          "750px",

							margin: 		"-55px"

							} 

							});

							');

			*/				

	}



	

	

	

	/**

	* receipt_page

	**/

	public function receipt_page( $order ){

			
			echo '<p>'.__('Thank you for your order .', 'abzer_networkonline').'</p>';

			echo $this->generate_networkonline_form( $order );

	}

	/*

	  ** Network Online Response 

	  ** Using CallBack Function wc-api/abzer_networkonline/

	*/

	public function check_authorize_response(){

		

			global $woocommerce;

			global $wpdb; 

			

			// If there is no response from Network Online , Please notify the Administrator 

			if(!$_REQUEST['responseParameter'])

				return; 

			// Decrypt the response in to desired array 

			$responseArray = $this->decryptResponse($_REQUEST['responseParameter']);


			if($responseArray){

				try {

					// Check if the payment is SUCCESS or FAILURE 

					if($responseArray['Transaction_Status_information']!='NULL'){

						

							$Transaction_Status_information = explode('|',$responseArray['Transaction_Status_information']);

							// Get the Order Details from the Response 

					

							if($responseArray['Transaction_Response']!='NULL'){

								

								$Transaction_Response = explode('|',$responseArray['Transaction_Response']);

							//print_r($Transaction_Response);
								//exit;

								$order = new WC_Order( $Transaction_Response[1] );

									

								// Get the Transaction_related_information

								if($responseArray['Transaction_related_information']!='NULL'){

									$Transaction_related_information = explode('|',$responseArray['Transaction_related_information']);

									$transactionId 	= (string)$Transaction_related_information[1];

								}else{

									$transactionId  = '';

								}

								// For the SUCCESS Case : 

								if(($Transaction_Status_information[1]=='SUCCESS')&&($Transaction_Status_information[2]=='00000')&&($Transaction_Status_information[3]=='No Error.')){

										
							
				
		
										// Update the Network Online table (woocommerce_networkonline)

										$network_onlineTable = array("response"   => $responseArray['text'], 

																	 "auth_code"  => $Transaction_related_information[6],

																	 "status"     => 'wc-processing',

																	 "error_code" => $Transaction_Status_information[2]

														);

										$where = array('order_id' => $Transaction_Response[1]);

										$wpdb->update( $wpdb->prefix.'woocommerce_networkonline', $network_onlineTable, $where );

										

										// Add the transactionId to the order 

										$order->payment_complete($transactionId);

										$order->add_order_note('Network Online payment successful. <br/> Customer Successfully Completed the payment from Network Online Site Ref Number/Transaction ID: '.$transactionId);

										$woocommerce->cart->empty_cart();

										

										// Redirect to thank You page 

										$resurl=$this->get_return_url( $order );

										wp_redirect($resurl);

										

								// For the FAILURE Case : 		

								}elseif($Transaction_Status_information[1]=='FAILURE'){	

									  // Here we are having two conditions 

										// Case 1 : When customer Cancels the Order from Network Onlie Site 

										// Case 2 : When the payment is failed : Like Fraud Card , Insufficient fund , Invalid OPT , Declined Stolen or lost card 

										// @ Case 1 : 10087 Error code 

										if($Transaction_Status_information[2]=='10087'){ // Canceled Case 

										

											// Update the Network Online table (woocommerce_networkonline)

											$network_onlineTable = array("response"   => $responseArray['text'], 

																		 "auth_code"  => isset($Transaction_related_information[6]) ? $Transaction_related_information[6] : '',

																		 "status"     => 'wc-cancelled',

																		 "error_code" => $Transaction_Status_information[2]

															);

											$where = array('order_id' => $Transaction_Response[1]);

											$wpdb->update( $wpdb->prefix.'woocommerce_networkonline', $network_onlineTable, $where );

											

											// Change the status of the order & and add the order note as well 

											$order->add_order_note('Customer Canceled the order from Network Online Payment Gateway');

											$order->update_status('wc-cancelled');

											$woocommerce->cart->empty_cart();

											

											// Redirect to thank You page 

											//$resurl=$this->get_return_url( $order );
											$resurl = $order->get_cancel_order_url();

											wc_add_notice( __( '<p style="color:#aa0000;font-weight:bold">Customer Canceled the order from Network Online Payment Gateway.</p>', 'woocommerce' ), 'notice' );

											wp_redirect($resurl);

											

										// @ Case 2 : Remaning all Error code 	

										}else{

											// Update the Network Online table (woocommerce_networkonline)

											$network_onlineTable = array("response"   => $responseArray['text'], 

																		 "auth_code"  => isset($Transaction_related_information[6]) ? $Transaction_related_information[6] : '',

																		 "status"     => 'wc-failed',

																		 "error_code" => $Transaction_Status_information[2]

															);

											$where = array('order_id' => $Transaction_Response[1]);

											$wpdb->update( $wpdb->prefix.'woocommerce_networkonline', $network_onlineTable, $where );

											

											// Change the status of the order & and add the order note as well 

											$order->add_order_note('Network Online Payment Gateway Declined '.$Transaction_Status_information[3]);

											$order->update_status('wc-failed');

											$woocommerce->cart->empty_cart();

											

											// Redirect to thank You page 

											$resurl=$this->get_return_url( $order );

											wc_add_notice( __( '<p style="color:#d0c21f;font-weight:bold">Network Online Payment Gateway Declined Your Transaction Reason :<i>'.$Transaction_Status_information[3].'</i></p>', 'woocommerce' ), 'notice' );

											wp_redirect($resurl);

										}	

								}

							}

					}		

				}catch (Exception $e) {

					echo 'Caught exception: ',  $e->getMessage(), "\n";

				}

		}	

		

	}

	

	/**

	* Encrypts data with required encryption algorithm

	* @param string $data string which needs to be encrypted 			

	* @param string $key  key to encrypt data

	* @param string $iv   initializes CBC encryption

	* @return string 	  encrypted string

	*/

	public function encryptData( $data, $key, $iv ){

/*			$enc  				= $this->neo['encryptMethod'];        
			$mode 				= $this->neo['encryptMode'];
			$size 				= mcrypt_get_block_size( $enc, $mode );
			$pad  				= $size - ( strlen( $data ) % $size );
			$padtext 			= $data . str_repeat( chr( $pad ), $pad );
			$crypt				= mcrypt_encrypt( $enc, base64_decode( $key ), $padtext, $mode, $iv );      
			$data    			= base64_encode( $crypt ); 	
			return $data;*/
		
		
		
		$enc_method			= 'AES-256-CBC';       
		$base64_decode_key  = base64_decode( $key );	
		$options            = 0;
		$crypt				= openssl_encrypt( $data, $enc_method ,$base64_decode_key, $options, $iv );    

		
	return $crypt;

	} 

	/**

	* Decrypts data with decryption algorithm

	* @param string $responseParameter   Encrypted Response from Network Online

	* @return string 	  encrypted string

	*/	

	public function decryptResponse( $responseParameter ){
		
		

		
		if($responseParameter){
			
			list($merchantId,$encryptString) = explode("||", $responseParameter);
			
			/*
			$enc   	 	  = $this->encryptMethod;
			$mode  	      = $this->encryptMode;
			$iv    	      = $iv;
			$encrypt_key  = $key;
		
			$EncText 	  = base64_decode($encryptString);
			$padtext      = mcrypt_decrypt($enc, base64_decode($encrypt_key), $EncText, $mode, $iv);
			$pad 	      = ord($padtext{strlen($padtext) - 1});	
			*/
			$encrypt_key  = WC()->session->get('merchant_key');
			$enc_method   		= 'AES-256-CBC';
			$base64_decode_key  = base64_decode( $encrypt_key );
			$options            = 0;
			$iv    	   = "0123456789abcdef";
			$text      			= openssl_decrypt( $encryptString, $enc_method, $base64_decode_key, $options, $iv );
			$reponseArray 	  	 = explode("||",$text);
				
				$blockEI 			 = $reponseArray[0]; // It has to contains Seven indicators

				$bitmapString        = str_split($blockEI);

				$blockEIArrayKey     = array(

												'Transaction_Response', 			   //Same as Request 

												'Transaction_related_information',    // Transaction related information 

												'Transaction_Status_information',    //  Transaction Status information 

												'Merchant_Information',    			//   Merchant Information 

												'Fraud_Block',    			       //    Fraud Block 

												'DCC_Block',    			      //     DCC Block 

												'Additional'    			     //      Additional Block Like Card Mask 

											);	

				//

				$bit 		  = 0;

				$blockEIArray = array();



				foreach($blockEIArrayKey as $blockValues){

					$blockEIArray[$blockValues] = $bitmapString[$bit];

					$bit++;

				}

				$blockEIArray = array_filter($blockEIArray);

				// Remove the first element from Array to map with the bit map values 

				array_shift($reponseArray);

				$resposeAssignedArray = array();

				$res 				  = 0;

				foreach($blockEIArray as $key => $value){

						$resposeAssignedArray[$key] =  $reponseArray[$res];

					$res++;

				}

						$TransactionResposeValue['text']		    = $merchantId.'||'.$text;

						$TransactionResposeValue['merchantId']		= $merchantId;

						$TransactionResposeValue['DataBlockBitmap']	= $blockEI;

				foreach($blockEIArrayKey as $key => $value){

						if(isset($resposeAssignedArray[$value]))

							$TransactionResposeValue[$value] = $resposeAssignedArray[$value];

						else

							$TransactionResposeValue[$value] = 'NULL';

				}


		   return $TransactionResposeValue;			

		}else{

			return false;

		}


	}

	/** 

	 * setblockExistenceIndicator Function which is used to return the Block Existence Indicator Bitmap  

	 * Function PARAMETERS : NULL 	

	 * returns : The Block Existence Indicator Bitmap : String 

     */	

	public function setblockExistenceIndicator(){

		

			$blockExistenceIndicator  = '';

			// DataBlock 1 transactionDataBlock

			$blockExistenceIndicator .= $this->neo['transactionDataBlock'] ? '1' : '0';

			// DataBlock 2 billingDataBlock

			$blockExistenceIndicator .= $this->neo['billingDataBlock'] ? '1' : '0';

			// DataBlock 3 ShippingDataBlock

			$blockExistenceIndicator .= $this->neo['shippingDataBlock'] ? '1' : '0';

			// DataBlock 4 PaymentDataBlock

			$blockExistenceIndicator .= $this->neo['paymentDataBlock'] ? '1' : '0';

			// DataBlock 5 MerchantDataBlock

			$blockExistenceIndicator .= $this->neo['merchantDataBlock'] ? '1' : '0';

			// DataBlock 6 OtherDataBlock

			$blockExistenceIndicator .= $this->neo['otherDataBlock'] ? '1' : '0';

			// DataBlock 7 DCCDataBlock

			$blockExistenceIndicator .= $this->neo['DCCDataBlock'] ? '1' : '0';

			

		return $blockExistenceIndicator;

	}

	public function setTransactionFieldExistenceIndicator(){

		

		if($this->neo['transactionDataBlock']){

			

			$transactionFieldEI = '';

				// Filed 1 Merchant Order Number For Transaction Data Block 1

				$transactionFieldEI .= $this->neo['merchantOrderNumber'] ? '1':'0';

				// Filed 2 Amount For Transaction Data Block 1

				$transactionFieldEI .= $this->neo['amount'] ? '1':'0';

				// Filed 3 SuccessUrl For Transaction Data Block 1

				$transactionFieldEI .= $this->neo['successUrl'] ? '1':'0';

				// Filed 4 FailureUrl For Transaction Data Block 1

				$transactionFieldEI .= $this->neo['failureUrl'] ? '1':'0';

				// Filed 5 TransactionMode For Transaction Data Block 1

				$transactionFieldEI .= $this->neo['transactionMode'] ? '1':'0';

				// Filed 6 PayModeType For Transaction Data Block 1

				$transactionFieldEI .= $this->neo['payModeType'] ? '1':'0';

				// Filed 7 TransactionType For Transaction Data Block 1

				$transactionFieldEI .= $this->neo['transactionType'] ? '1':'0';

				// Filed 8 Currency For Transaction Data Block 1

				$transactionFieldEI .= $this->neo['currency'] ? '1':'0';

			

			return trim($transactionFieldEI); 	

		}	

	}

	/*

	 * setBillingFieldExistenceIndicator 

	 * Get all the required property values form the constructor to generate the Bitmap value 

	   for the Billing Block Data, Check if the billingDataBlock is set to true then generate the Bitmap values 

	 * @returns Bitmap value for the Billing Block Data 

	 * 

   */	

	public function setBillingFieldExistenceIndicator(){

		

		if($this->neo['billingDataBlock']){

			

				$billingFieldEI  = '';

				// Filed 1 BillTo FirstName For Billing Data Block 2

				$billingFieldEI .= $this->neo['billToFirstName'] ? '1':'0';

				// Filed 2 BillTo billToLastName For Billing Data Block 2

				$billingFieldEI .= $this->neo['billToLastName'] ? '1':'0';

				// Filed 3 BillTo billToStreet1 For Billing Data Block 2

				$billingFieldEI .= $this->neo['billToStreet1'] ? '1':'0';

				// Filed 4 BillTo billToStreet2 For Billing Data Block 2

				$billingFieldEI .= $this->neo['billToStreet2'] ? '1':'0';

				// Filed 5 BillTo billToCity For Billing Data Block 2

				$billingFieldEI .= $this->neo['billToCity'] ? '1':'0';

				// Filed 6 BillTo billToState For Billing Data Block 2

				$billingFieldEI .= $this->neo['billToState'] ? '1':'0';

				// Filed 7 BillTo billtoPostalCode For Billing Data Block 2

				$billingFieldEI .= $this->neo['billtoPostalCode'] ? '1':'0';

				// Filed 8 BillTo billToCountry For Billing Data Block 2

				$billingFieldEI .= $this->neo['billToCountry'] ? '1':'0';

				// Filed 9 BillTo billToEmail For Billing Data Block 2

				$billingFieldEI .= $this->neo['billToEmail'] ? '1':'0';

				// Filed 10 BillTo billToMobileNumber For Billing Data Block 2

				$billingFieldEI .= $this->neo['billToMobileNumber'] ? '1':'0';

				// Filed 11 BillTo billToPhoneNumber1 For Billing Data Block 2

				$billingFieldEI .= $this->neo['billToPhoneNumber1'] ? '1':'0';

				// Filed 12 BillTo billToPhoneNumber2 For Billing Data Block 2

				$billingFieldEI .= $this->neo['billToPhoneNumber2'] ? '1':'0';

				// Filed 13 BillTo billToPhoneNumber3 For Billing Data Block 2

				$billingFieldEI .= $this->neo['billToPhoneNumber3'] ? '1':'0';

				

			return $billingFieldEI;

		}else{

			return '0000000000000';

		}	

	}

	// Function setShippingFieldExistenceIndicator Filed Existence Indicator (BEI) 

	// for Shipping Block Data 

	/*

	 * setShippingFieldExistenceIndicator 

	 * Get all the required property values form the constructor to generate the Bitmap value 

	   for the Shipping Block Data, Check if the shippingDataBlock is set to true then generate the Bitmap values 

	 * @returns Bitmap value for the Shipping Block Data 

	 * 

	 */		

	public function setShippingFieldExistenceIndicator(){

		

		if($this->neo['shippingDataBlock']){

			

				$shippingFieldEI  = '';	

				// Filed 1 shipToFirstName For Shipping Data Block 3

				$shippingFieldEI .= $this->neo['shipToFirstName'] ? '1':'0';

				// Filed 2 shipToLastName For Shipping Data Block 3

				$shippingFieldEI .= $this->neo['shipToLastName'] ? '1':'0';

				// Filed 3 shipToStreet1 For Shipping Data Block 3

				$shippingFieldEI .= $this->neo['shipToStreet1'] ? '1':'0';

				// Filed 4 shipToStreet2 For Shipping Data Block 3

				$shippingFieldEI .= $this->neo['shipToStreet2'] ? '1':'0';

				// Filed 5 shipToCity For Shipping Data Block 3

				$shippingFieldEI .= $this->neo['shipToCity'] ? '1':'0';

				// Filed 6 shipToState For Shipping Data Block 3

				$shippingFieldEI .= $this->neo['shipToState'] ? '1':'0';

				// Filed 7 shipToPostalCode For Shipping Data Block 3

				$shippingFieldEI .= $this->neo['shipToPostalCode'] ? '1':'0';

				// Filed 8 shipToCountry For Shipping Data Block 3

				$shippingFieldEI .= $this->neo['shipToCountry'] ? '1':'0';

				// Filed 9 shipToPhoneNumber1 For Shipping Data Block 3

				$shippingFieldEI .= $this->neo['shipToPhoneNumber1'] ? '1':'0';

				// Filed 10 shipToPhoneNumber2 For Shipping Data Block 3

				$shippingFieldEI .= $this->neo['shipToPhoneNumber2'] ? '1':'0';

				// Filed 11 shipToPhoneNumber3 For Shipping Data Block 3

				$shippingFieldEI .= $this->neo['shipToPhoneNumber3'] ? '1':'0';

				// Filed 12 shipToMobileNumber For Shipping Data Block 3

				$shippingFieldEI .= $this->neo['shipToMobileNumber'] ? '1':'0';

				

			return $shippingFieldEI;

				

		}else{

		   return '000000000000';

		}

		

	}

	/*

	 * setPaymentFieldExistenceIndicator 

	 * Get all the required property values form the constructor to generate the Bitmap value 

	   for the Payment Block Data, Check if the paymentDataBlock is set to true then generate the Bitmap values 

	 * @returns Bitmap value for the Payment Block Data 

	 * 

	*/			

	public function setPaymentFieldExistenceIndicator(){

		

		if($this->neo['paymentDataBlock']){

			// Here we are having three options 

				// 1 ) Credit Card transaction // (CC)-Credit Card, (DC)-Debit Card, (DD)-Direct Debit // BitMap : 11111111111

				// 2 ) (NB)-Net Banking // BitMap : 00000000010 

				// 3 ) IMPS-Hope so (PAYPAL)-PayPal //  BitMap : 00000011100

			if(in_array($this->neo['payModeType'], $this->neo['payModeTypeCard'])){

						$paymentFieldEI = '';

						// Filed 1 cardNumber For Payment Data Block 4

						$paymentFieldEI .= $this->neo['cardNumber'] ? '1':'0';

						// Filed 2 expMonth For Payment Data Block 4

						$paymentFieldEI .= $this->neo['expMonth'] ? '1':'0';

						// Filed 3 expYear For Payment Data Block 4

						$paymentFieldEI .= $this->neo['expYear'] ? '1':'0';

						// Filed 4 CVV For Payment Data Block 4

						$paymentFieldEI .= $this->neo['CVV'] ? '1':'0';

						// Filed 5 cardHolderName For Payment Data Block 4

						$paymentFieldEI .= $this->neo['cardHolderName'] ? '1':'0';

						// Filed 6 cardType For Payment Data Block 4

						$paymentFieldEI .= $this->neo['cardType'] ? '1':'0';

						// Filed 7 custMobileNumber For Payment Data Block 4

						$paymentFieldEI .= $this->neo['custMobileNumber'] ? '1':'0';

						// Filed 8 paymentID For Payment Data Block 4

						$paymentFieldEI .= $this->neo['paymentID'] ? '1':'0';

						// Filed 9 OTP For Payment Data Block 4

						$paymentFieldEI .= $this->neo['OTP'] ? '1':'0';

						// Filed 10 gatewayID For Payment Data Block 4

						$paymentFieldEI .= $this->neo['gatewayID'] ? '1':'0';

						// Filed 11 cardToken For Payment Data Block 4

						$paymentFieldEI .= $this->neo['cardToken'] ? '1':'0';

					

					return $paymentFieldEI;

					

			}elseif(in_array($this->neo['payModeType'], $this->neo['payModeTypeNetBank'])){

						$paymentFieldEI = '00000000010';

					return $paymentFieldEI;

			}else{

						$paymentFieldEI = '00000011100';

					return $paymentFieldEI;

			}		

		}else{

						$paymentFieldEI = '00000000000';

					return $paymentFieldEI;

		}	

	}

	// Function setMerchantFieldExistenceIndicator Filed Existence Indicator (BEI) 

	// for Merchant Block Data 

	/*

	 * setMerchantFieldExistenceIndicator 

	 * Get all the required property values form the constructor to generate the Bitmap value 

	   for the Merchant Block Data, Check if the merchantDataBlock is set to true then generate the Bitmap values 

	 * @returns Bitmap value for the Merchant Block Data 

	 * 

	*/		

	public function setMerchantFieldExistenceIndicator(){

		

		if($this->neo['merchantDataBlock']){

			$merchantFieldEI  = '';

			// Filed 1 UDF1 For Merchant Data Block 5

			$merchantFieldEI .= $this->neo['UDF1'] ? '1':'0';

			// Filed 2 UDF2 For Merchant Data Block 5

			$merchantFieldEI .= $this->neo['UDF2'] ? '1':'0';

			// Filed 3 UDF3 For Merchant Data Block 5

			$merchantFieldEI .= $this->neo['UDF3'] ? '1':'0';

			// Filed 4 UDF4 For Merchant Data Block 5

			$merchantFieldEI .= $this->neo['UDF4'] ? '1':'0';

			// Filed 5 UDF5 For Merchant Data Block 5

			$merchantFieldEI .= $this->neo['UDF5'] ? '1':'0';

			// Filed 6 UDF6 For Merchant Data Block 5

			$merchantFieldEI .= $this->neo['UDF6'] ? '1':'0';

			// Filed 7 UDF7 For Merchant Data Block 5

			$merchantFieldEI .= $this->neo['UDF7'] ? '1':'0';

			// Filed 8 UDF8 For Merchant Data Block 5

			$merchantFieldEI .= $this->neo['UDF8'] ? '1':'0';

			// Filed 9 UDF9 For Merchant Data Block 5

			$merchantFieldEI .= $this->neo['UDF9'] ? '1':'0';

			// Filed 10 UDF10 For Merchant Data Block 5

			$merchantFieldEI .= $this->neo['UDF10'] ? '1':'0';

			

			return $merchantFieldEI;

			

		}else{

			return '0000000000';

		}

		

	}

	// Function setOtherDetailsFieldExistenceIndicator Filed Existence Indicator (BEI) 

	// for Other Details Block Data 

	/*

	 * setOtherDetailsFieldExistenceIndicator 

	 * Get all the required property values form the constructor to generate the Bitmap value 

	   for the Other Details Block Data, Check if the otherDataBlock is set to true then generate the Bitmap values 

	 * @returns Bitmap value for the Other Details Block Data 

	 * 

	*/		

	public function setOtherDetailsFieldExistenceIndicator(){

		

		if($this->neo['otherDataBlock']){

			

				$otherFieldEI  = '';

				// Filed 1 custID For OtherDeatils Data Block 6

				$otherFieldEI .= $this->neo['custID'] ? '1':'0';

				// Filed 2 transactionSource For OtherDeatils Data Block 6

				$otherFieldEI .= $this->neo['transactionSource'] ? '1':'0';

				// Filed 3 productInfo For OtherDeatils Data Block 6

				$otherFieldEI .= $this->neo['productInfo'] ? '1':'0';

				// Filed 4 isUserLoggedIn For OtherDeatils Data Block 6

				$otherFieldEI .= $this->neo['isUserLoggedIn'] ? '1':'0';

				// Filed 5 itemTotal For OtherDeatils Data Block 6

				$otherFieldEI .= $this->neo['itemTotal'] ? '1':'0';

				// Filed 6 itemCategory For OtherDeatils Data Block 6

				$otherFieldEI .= $this->neo['itemCategory'] ? '1':'0';

				// Filed 7 ignoreValidationResult For OtherDeatils Data Block 6

				$otherFieldEI .= $this->neo['ignoreValidationResult'] ? '1':'0';

				

			return $otherFieldEI;

			

		}else{

			return '0000000';

		}	

	}

	// Function setDCCFieldExistenceIndicator Filed Existence Indicator (BEI) 

	// for DCC Block Data 

	/*

	 * setDCCFieldExistenceIndicator 

	 * Get all the required property values form the constructor to generate the Bitmap value 

	   for the DCC Block Data, Check if the DCCDataBlock is set to true then generate the Bitmap values 

	 * @returns Bitmap value for the DCC Block Data 

	 * 

	*/	

	public function setDCCFieldExistenceIndicator(){

		

		if($this->neo['DCCDataBlock']){

				$DCCFieldEI  = '';

				// Filed 1 DCCReferenceNumber For DCC Data Block 7

				$DCCFieldEI .= $this->neo['DCCReferenceNumber'] ? '1':'0';

				// Filed 1 foreignAmount For DCC Data Block 7

				$DCCFieldEI .= $this->neo['foreignAmount'] ? '1':'0';

				// Filed 1 ForeignCurrency For DCC Data Block 7

				$DCCFieldEI .= $this->neo['ForeignCurrency'] ? '1':'0';

				

			return $DCCFieldEI;

			

		}else{

			return '000';

		}

		

	}	

}



	

	