<?php
/**
 * Network Online Logs page
 *
 * @author      Rajeevan
 * @category    Admin
 * @package     WooCommerce/Admin/Order System Status ( Network Online Logs) 
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
/**
 * Abzer_Networkonline_Logs Class.
 */
class Abzer_Networkonline_Logs extends WP_List_Table {
	
	
	
	/** Class constructor */
	public function __construct() {
		
		parent::__construct( [
			'singular' => __( 'Networkonlinelog', 'sp' ), //singular name of the listed records
			'plural'   => __( 'Networkonlinelogs', 'sp' ), //plural name of the listed records
			'ajax'     => false //should this table support ajax?
		] );
	}
	/**
	 * Retrieve customer’s data from the database
	 *
	 * @param int $per_page
	 * @param int $page_number
	 *
	 * @return mixed
	 */
		public static function get_networkonlinelogs( $per_page = 5, $page_number = 1 ) {

		  global $wpdb;
			
		  $sql    = "SELECT * FROM {$wpdb->prefix}woocommerce_networkonline";
		  
		  $status = isset( $_REQUEST['status'] ) ? $_REQUEST['status'] : '';	
		  if($status=='all')
			  $status = '';
			  
          if ( ! empty( $status ) ) {
			   $sql .= ' WHERE `status` = \'wc-'.$status."'";
		  }
			
		  if ( ! empty( $_REQUEST['orderby'] ) ) {
			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
		  }else{
			$sql .= ' ORDER BY order_id DESC ';  
		  }
			
		  $sql .= " LIMIT $per_page";

		  $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;
			
		  $result = $wpdb->get_results( $sql, 'ARRAY_A' );
		
		  return $result;
		}
		
		protected function get_views() { 
		
				$processing_count = $this->get_status_count('processing');
				$cancelled_count  = $this->get_status_count('cancelled');
				$failed_count  	  = $this->get_status_count('failed');
				$pending_count    = $this->get_status_count('pending');
				$total_count      = $this->get_status_count('');
				
				// Get Status Type 
				$status = isset( $_REQUEST['status'] ) ? $_REQUEST['status'] : 'all';
				
				$status_links = array();
				
				$class = $status == 'all' ? ' class="current"' : '';
				$status_links['all'] 		= "<a href='admin.php?page=abzer-networkonline-logs&status=all'$class>" . sprintf( _nx( 'All <span class="count">(%s)</span>', 'All <span class="count">(%s)</span>', $total_count, 'users' ), number_format_i18n( $total_count ) ) . '</a>';
				
				$class = $status == 'processing' ? ' class="current"' : '';
				$status_links['processing'] = "<a href='admin.php?page=abzer-networkonline-logs&status=processing'$class>" . sprintf( _nx( 'Processing <span class="count">(%s)</span>', 'Processing <span class="count">(%s)</span>', $processing_count, 'users' ), number_format_i18n( $processing_count ) ) . '</a>';
				
				$class = $status == 'cancelled' ? ' class="current"' : '';
				$status_links['cancelled'] = "<a href='admin.php?page=abzer-networkonline-logs&status=cancelled'$class>" . sprintf( _nx( 'Cancelled <span class="count">(%s)</span>', 'Cancelled <span class="count">(%s)</span>', $cancelled_count, 'users' ), number_format_i18n( $cancelled_count ) ) . '</a>';
				
				$class = $status == 'failed' ? ' class="current"' : '';
				$status_links['failed'] = "<a href='admin.php?page=abzer-networkonline-logs&status=failed'$class>" . sprintf( _nx( 'Failed <span class="count">(%s)</span>', 'Failed <span class="count">(%s)</span>', $failed_count, 'users' ), number_format_i18n( $failed_count ) ) . '</a>';
				
				$class = $status == 'pending' ? ' class="current"' : '';
				$status_links['pending'] = "<a href='admin.php?page=abzer-networkonline-logs&status=pending'$class>" . sprintf( _nx( 'Failed <span class="count">(%s)</span>', 'Pending <span class="count">(%s)</span>', $pending_count, 'users' ), number_format_i18n( $pending_count ) ) . '</a>';
				
				
				
			return $status_links;	
		}
	/*
		Public function get Status Count 
		
	*/	
		public function get_status_count($status){

		   global $wpdb;	
		  
		  $sql = "SELECT COUNT(*) FROM {$wpdb->prefix}woocommerce_networkonline";
		
          if ( ! empty( $status ) ) {
			   $sql .= ' WHERE `status` = \'wc-'.$status."'";
		  }
		
		  return $wpdb->get_var( $sql );
		  
		}	
	
	/**
	 * Delete a customer record.
	 *
	 * @param int $id customer ID
	*/
		public static function delete_networkonlinlog( $id ) {
			
		  global $wpdb;

		  $wpdb->delete(
			"{$wpdb->prefix}woocommerce_networkonline",
			[ 'neo_id' => $id ],
			[ '%d' ]
		  );
		}	
	/**
	 * Returns the count of records in the database.
	 *
	 * @return null|string
	 */
		public static function record_count() {
			
		  global $wpdb;
		  	
		  $sql = "SELECT COUNT(*) FROM {$wpdb->prefix}woocommerce_networkonline";
		
		  $status = isset( $_REQUEST['status'] ) ? $_REQUEST['status'] : '';	
		  if($status=='all')
			  $status = '';
			  
          if ( ! empty( $status ) ) {
			   $sql .= ' WHERE `status` = \'wc-'.$status."'";
		  }
		
		  return $wpdb->get_var( $sql );
		}
	/** Text displayed when no customer data is available */
		public function no_items() {
		  _e( 'No records avaliable.', 'sp' );
		}	
	
	/**
	 * Render a column when no column specific method exists.
	 *
	 * @param array $item
	 * @param string $column_name
	 *
	 * @return mixed
	*/
	public function column_default( $item, $column_name ) {
		
	  switch ( $column_name ) {
		case 'order_id':
		case 'order_key':
		case 'request':
		case 'response':
		//case 'cb':
		case 'auth_code':
		case 'amount':
		case 'currency':
		case 'exchange_rate':
		case 'created_on':
		case 'status':
		  return $item[ $column_name ];
		default:
		  return print_r( $item, true ); //Show the whole array for troubleshooting purposes
	  }
	}
	/**
	 *  Associative array of columns
	 *
	 * @return array
	*/
	function get_columns() {
	  $columns = [
		//'cb'       => '<input type="checkbox" />',
		'order_key'    => __( 'Order Key', 'sp' ),
		'order_id' 	   => __( 'Order ID', 'sp' ),
		'request'      => __( 'Request (OrderID,Amount,Currency) ', 'sp' ),
		'response'     => __( 'Response', 'sp' ),
		'auth_code'    => __( 'Auth Code', 'sp' ),
		'amount'       => __( 'Amount', 'sp' ),
		'currency'     => __( 'Currency', 'sp' ),
		'exchange_rate'=> __( 'Exchange Rate', 'sp' ),
		'created_on'   => __( 'Created', 'sp' ),
		'status'	   => __( 'Status', 'sp' )
	  ];

	  return $columns;
	}
	/**
	 * Columns to make sortable.
	 *
	 * @return array
	*/
	public function get_sortable_columns() {
	  $sortable_columns = array(
		'order_key' => array( 'order_key', false ),
		'order_id' => array( 'order_id', false )
	  );

	  return $sortable_columns;
	}
	/**
	 * Render the bulk edit checkbox
	 *
	 * @param array $item
	 *
	 * @return string
	*/
	/*
	function column_cb( $item ) {
	  return sprintf(
		'<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['cb']
	  );
	}*/
	/**
	 * Returns an associative array containing the bulk action
	 *
	 * @return array
	*/
	public function get_bulk_actions() {
	  $actions = [
		'bulk-delete' => 'Delete'
	  ];

	  return $actions;
	}
	/**
	 * Handles data query and filter, sorting, and pagination.
	*/
	public function prepare_items() {

	  $this->_column_headers = $this->get_column_info();

	  /** Process bulk action */
	  $this->process_bulk_action();

	  $per_page     = $this->get_items_per_page( 'customers_per_page', 5 );
	  $current_page = $this->get_pagenum();
	  $total_items  = self::record_count();

	  $this->set_pagination_args( [
		'total_items' => $total_items, //WE have to calculate the total number of items
		'per_page'    => $per_page    //WE have to determine how many items to show on a page
	  ] );


	 // $this->items = self::get_networkonlinelogs( $per_page, $current_page );
	  $networklogs = self::get_networkonlinelogs( $per_page, $current_page );
	  
	   if ( ! empty( $networklogs )  ){
		   
			 foreach ( $networklogs as $item ) {
				 
				// Set the Request Entity for the corresponding value 
				 if($item['request']!=''){
					 $request              = $item['request'];
					 $explode_bit_map 	   = explode('||',$request);
					 $transaction_response = explode('|',$explode_bit_map[3]);
					 $transaction_array	   = array(
													isset($transaction_response[1])?$transaction_response[1]:'',
													isset($transaction_response[2])?$transaction_response[2]:'',
													isset($transaction_response[8])?$transaction_response[8]:''
												);
					 $requestString        = join(',',$transaction_array);							
				 }else{
					 $requestString        = '';
				 }		
				 
				 if($item['type']=='D'){
					 $responseArray 	   = $this->convertRequestArray($item['response']);
					 if($responseArray['Transaction_Status_information']!='NULL'){
						$Transaction_Status_information  = explode('|',$responseArray['Transaction_Status_information']);
						$Transaction_related_information = $this->getTransactionRelatedInformation($responseArray['Transaction_related_information']);
							if($Transaction_Status_information[1]=='SUCCESS'){
								$responsArray = array(	
														isset($Transaction_related_information['Reference_Number']) ? $Transaction_related_information['Reference_Number'] : '',
														isset($Transaction_related_information['Gateway_Trace_Number']) ? $Transaction_related_information['Gateway_Trace_Number'] : '',
													);
							}elseif($Transaction_Status_information[1]=='FAILURE'){
								$responsArray = array(	
														isset($Transaction_related_information['Reference_Number']) ? $Transaction_related_information['Reference_Number'] : '',
														isset($Transaction_related_information['Gateway_Trace_Number']) ? $Transaction_related_information['Gateway_Trace_Number'] : '',
													);
							}else{
								$responsArray = array();
							}
							$responsString = join(',',$responsArray);
					}else{
							$responsString = '';
					}
					
					// Amount & Currency 
					if($responseArray['DCC_Block']!='NULL'){
						$DCC_Block = explode('|',$responseArray['DCC_Block']);
						if($DCC_Block[1]=='YES'){
							$amount       = number_format($DCC_Block[2],2,'.','');
							$currency     = $DCC_Block[3];
							$exchangeRate = $DCC_Block[5];
						}else{
							if($responseArray['Transaction_Response']!= 'NULL'){
								$Transaction_Response = explode('|',$responseArray['Transaction_Response']);
								$amount 	= number_format($Transaction_Response[3],2,'.','');
								$currency   = $Transaction_Response[2];
								$exchangeRate= '';
							}else{
								$amount 	= '';
								$currency   = '';
								$exchangeRate= '';
							}
						}
					}else{
						if($responseArray['Transaction_Response']!= 'NULL'){
							$Transaction_Response = explode('|',$responseArray['Transaction_Response']);
							$exchangeRate= '';
							if(isset($Transaction_Response[3]))
								$amount = number_format(str_replace(',','',$Transaction_Response[3]),2,'.','');
							else
								$amount='';	
							
							if(isset($Transaction_Response[2]))
								$currency=$Transaction_Response[2];
							else
								$currency= '';
						}else{
								$amount 	= '';
								$currency   = '';
								$exchangeRate= '';
						}
							  
					}
					
				 }else{
						$responseArray 	 = $this->convertRequestArrayCron($item['response']);
						if($responseArray['Status']!='NULL'){
							$Status 	 = explode('|',$responseArray['Status']);
							
							$sucessStatusArray = array('Capture/Sale Success',
															'Auth Success',
															'Settled'
														   );
							
							//if($Status[1]=='SUCCESS'){
							if(in_array($Status[1],$sucessStatusArray)){	
								// Request :
								$Transaction_Details = explode('|',$responseArray['Transaction_Details']);
								$responsString 		 = $Transaction_Details[1];
								
								// Auth Code : Will be Null 
								// Amount : Check if the DCC Blcok is set  
								if($responseArray['DCC_Block']!='NULL'){
									$DCC_Block = explode('|',$responseArray['DCC_Block']);
									if($DCC_Block[1]=='YES'){
										// Amount : Will Change 
										$amount   = number_format($DCC_Block[2],2,'.','');
										$currency = $DCC_Block[3];
										$exchangeRate = '0'.$DCC_Block[4];
									}else{
										if($responseArray['Amount_Block']!='NULL'){
											$Amount_Block = explode('|',$responseArray['Amount_Block']);
											$amount   = number_format($Amount_Block[1],2,'.','');
											$currency = $Amount_Block[2];
											$exchangeRate = '';
										}
									}
								}else{
									if($responseArray['Amount_Block']!='NULL'){
										$Amount_Block = explode('|',$responseArray['Amount_Block']);
										$amount   =  number_format($Amount_Block[1],2,'.','');
										$currency = $Amount_Block[2];
										$exchangeRate = '';
									}else{
										$amount 	= '';
										$currency   = '';
										$exchangeRate= '';
									}	
								}
							}else{
								// Request : 
								$responsString = 'No Record Found';
								// Amount : Check if the DCC Blcok is set  
								if($responseArray['DCC_Block']!='NULL'){
									$DCC_Block = explode('|',$responseArray['DCC_Block']);
									if($DCC_Block[1]=='YES'){
										// Amount : Will Change 
										$amount   = number_format($DCC_Block[2],2,'.','');
										$currency = $DCC_Block[3];
										$exchangeRate = ('0'.$DCC_Block[4]);
									}else{
										if($responseArray['Amount_Block']!='NULL'){
											$Amount_Block = explode('|',$responseArray['Amount_Block']);
											$amount	  =  number_format($Amount_Block[1],2,'.','');
											$currency =  $Amount_Block[2];
											$exchangeRate = '';
										}
									}
								}else{
									if($responseArray['Amount_Block']!='NULL'){
										$Amount_Block = explode('|',$responseArray['Amount_Block']);
										$amount		  = number_format($Amount_Block[1],2,'.','');
										$currency     = $Amount_Block[2];
										$exchangeRate = '';
									}else{
										$amount 	= '';
										$currency   = '';
										$exchangeRate= '';
									}	
								}
							}
						}	
				 }		
				 
				 $data[] = array(
									//'cb'      	      => $item['neo_id'],
									'order_key'       => $item['order_key'],
									'order_id'        => $item['order_id'],
									'request'         => $requestString,
									'response'        => $responsString ? $responsString : '',
									'amount'          => $amount,
									'auth_code'       => $item['auth_code'],
									'currency'        => $currency,
									'exchange_rate'   => $exchangeRate,
									'created_on'      => $item['created_on'],
									'status'          => $item['status']
									);

			 }
		}
	  if(isset($data))	
		$this->items  =  $data;
	 // exit;
	  
	}
	
	public function process_bulk_action() {
			
			
		  //Detect when a bulk action is being triggered...
		  if ( 'delete' === $this->current_action() ) {

			// In our file that handles the request, verify the nonce.
			$nonce = esc_attr( $_REQUEST['_wpnonce'] );

			if ( ! wp_verify_nonce( $nonce, 'sp_delete_networkonlinlog' ) ) {
			  die( 'Go get a life script kiddies' );
			}
			else {
			  self::delete_networkonlinlog( absint( $_GET['customer'] ) );

			  wp_redirect( esc_url( add_query_arg() ) );
			  exit;
			}

		  }

		  // If the delete bulk action is triggered
		  if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' )
			   || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )
		  ) {

			$delete_ids = esc_sql( $_POST['bulk-delete'] );

			// loop over the array of record IDs and delete them
			foreach ( $delete_ids as $id ) {
			  self::delete_networkonlinlog( $id );

			}

			wp_redirect( esc_url( add_query_arg() ) );
			exit;
		  }
		}
		
	public function convertRequestArray($text){
		
		if($text){
			$reponseArray 	  	 = explode("||",$text);
			$reponseGetMerchantId= explode("||",$text);
			
			$blockEI 			 = $reponseArray[1]; // It has to contains Seven indicators
			$bitmapString        = str_split($blockEI);
			$blockEIArrayKey     = array(
											'Transaction_Response', 			   //Same as Request 
											'Transaction_related_information',    // Transaction related information 
											'Transaction_Status_information',    //  Transaction Status information 
											'Merchant_Information',    			//   Merchant Information 
											'Fraud_Block',    			       //    Fraud Block 
											'DCC_Block',    			      //     DCC Block 
											'Additional'    			     //      Additional Block Like Card Mask 
										);	
			//
			$bit 		  = 0;
			$blockEIArray = array();

			foreach($blockEIArrayKey as $blockValues){
				if(isset($bitmapString[$bit]))
					$blockEIArray[$blockValues] = $bitmapString[$bit];
				$bit++;
			}
			$blockEIArray = array_filter($blockEIArray);
			// Remove the first element from Array to map with the bit map values 
			array_shift($reponseArray);
			array_shift($reponseArray);
			$resposeAssignedArray = array();
			$res 				  = 0;
			foreach($blockEIArray as $key => $value){
					$resposeAssignedArray[$key] =  $reponseArray[$res];
				$res++;
			}
					$merchantId                                 = $reponseGetMerchantId[0];
					$TransactionResposeValue['text']		    = $text;
					$TransactionResposeValue['merchantId']		= $merchantId;
					$TransactionResposeValue['DataBlockBitmap']	= $blockEI;
			foreach($blockEIArrayKey as $key => $value){
					if(isset($resposeAssignedArray[$value]))
						$TransactionResposeValue[$value] = $resposeAssignedArray[$value];
					else
						$TransactionResposeValue[$value] = 'NULL';
			}
			
		   return $TransactionResposeValue;
		}else{
		   return array(
						'text'  =>'NULL',
						'merchantId'  =>'NULL',
						'DataBlockBitmap'  =>'NULL',
						'Transaction_Response' =>'NULL',		       //Same as Request 
						'Transaction_related_information'=>'NULL',    // Transaction related information 
						'Transaction_Status_information'=>'NULL',	 //  Transaction Status information 
						'Merchant_Information'=>'NULL',    			//   Merchant Information 
						'Fraud_Block'=>'NULL',     				   //    Fraud Block 
						'DCC_Block'=>'NULL',	    			  //     DCC Block 
						'Additional' =>'NULL'	   			     //      Additional Block Like Card Mask 
						);	
		   
		}	
		
	}
	public function getTransactionRelatedInformation( $transactionRelatedString ){
		
		$trans_related_info_array = array(
									'Reference_Number',
									'Transaction_Date',
									'Card_Enrollment_Response',
									'ECI_Indicator',
									'Gateway_Trace_Number',
									'Gateway_Identifier',
									'Auth_Code'
								 );
		
		$TransactionRelated 	  = explode('|',$transactionRelatedString);	
		$TransactionRelatedFEI    = $TransactionRelated[0];
		$TransactionRelatedbitmapString  = str_split($TransactionRelatedFEI);
		
		if(count($TransactionRelatedbitmapString)==7){
			
			$trifei = 0;
			foreach( $trans_related_info_array as $key => $value){
				$TransactionAssignedArray[$value] = $TransactionRelatedbitmapString[$trifei];
				$trifei++;
			}
			$TransactionAssignedArray = array_filter($TransactionAssignedArray);
			// Remove the first element from Array to map with the bit map values 
			array_shift($TransactionRelated);

			$trc = 0 ;
			$TransactionRelatedValuesArray = array();
			foreach($TransactionAssignedArray as $key => $value){
					$TransactionRelatedValuesArray[$key] = $TransactionRelated[$trc];
				$trc++ ;
			}
			$trans_related_values_array = array();
			foreach( $trans_related_info_array as $key => $value){
				
					if(isset($TransactionRelatedValuesArray[$value]))
						$trans_related_values_array[$value] = $TransactionRelatedValuesArray[$value];
					else
						$trans_related_values_array[$value] = 'NULL';
							
						
			}
		
			return $trans_related_values_array;
		
		}else{
			
			return array(
						'Reference_Number'        =>'NULL',
						'Transaction_Date'        =>'NULL',
						'Card_Enrollment_Response'=>'NULL',
						'ECI_Indicator'           =>'NULL',
						'Gateway_Trace_Number'    =>'NULL',
						'Gateway_Identifier'      =>'NULL',
						'Auth_Code'               =>'NULL'
						);
		}
		
	}
	public function convertRequestArrayCron( $text ){
		
		if($text){
			$reponseArray 	  	 = explode("||",$text);
			$reponseGetMerchantId= explode("||",$text);
			$blockEI 			 = $reponseArray[1]; // It has to contains Seven indicators
			$bitmapString        = str_split($blockEI);
			$blockEIArrayKey     = array(
											'Transaction_Details', 			   //Same as Request 
											'Amount_Block',    // Transaction related information 
											'Status',    //  Transaction Status information 
											'Card_related_information',    			//   Merchant Information 
											'Fraud_Block',    			       //    Fraud Block 
											'DCC_Block'    			      //     DCC Block 
										);	
			$bit 		  = 0;
			$blockEIArray = array();

			foreach($blockEIArrayKey as $blockValues){
				$blockEIArray[$blockValues] = $bitmapString[$bit];
				$bit++;
			}
			$blockEIArray = array_filter($blockEIArray);
			// Remove the first element from Array to map with the bit map values 
			array_shift($reponseArray);
			array_shift($reponseArray);
			$resposeAssignedArray = array();
			$res 				  = 0;
			foreach($blockEIArray as $key => $value){
					$resposeAssignedArray[$key] =  $reponseArray[$res];
				$res++;
			}
					$merchantId                                 = $reponseGetMerchantId[0];
					$TransactionResposeValue['text']		    = $text;
					$TransactionResposeValue['merchantId']		= $merchantId;
					$TransactionResposeValue['DataBlockBitmap']	= $blockEI;
			foreach($blockEIArrayKey as $key => $value){
					if(isset($resposeAssignedArray[$value]))
						$TransactionResposeValue[$value] = $resposeAssignedArray[$value];
					else
						$TransactionResposeValue[$value] = 'NULL';
			}
		  return $TransactionResposeValue;								
		}else{
			
		}	
	}	
}

class SP_Plugin {

	// class instance
	static $instance;

	// customer WP_List_Table object
	public $customers_obj;

	// class constructor
	public function __construct() {
		add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
		add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
	}


	public static function set_screen( $status, $option, $value ) {
		return $value;
	}

	public function plugin_menu() {

		$hook = add_submenu_page(
			'woocommerce',
			__( 'Network Online Logs', 'woocommerce' ),
			__( 'Network Online Logs', 'woocommerce' ),
			'manage_options',
			'abzer-networkonline-logs',
			[ $this, 'plugin_settings_page' ]
		);

		add_action( "load-$hook", [ $this, 'screen_option' ] );

	}


	/**
	 * Plugin settings page
	 */
	public function plugin_settings_page() {
		?>
		
		<script>
			jQuery( function ( $ ) {
				//$(".column-order_id").css("background-color", "yellow");
				//$(".column-order_id").css("width", "7%");
				$(".bulkactions").hide();
				
				$('.networkonlinelogs .column-status').each(function(){
					  console.log($(this).html());
					  if($(this).html()=='wc-processing')
						$(this).html('<span style="color:#73a724 !important; font-weight:bold;">'+ucfirst($(this).html().replace('wc-',''),true)+'</span>');
					  else if($(this).html()=='wc-cancelled')
						$(this).html('<span style="color:#aa0000 !important; font-weight:bold;">'+ucfirst($(this).html().replace('wc-',''),true)+'</span>');  
					  else if($(this).html()=='wc-failed')
						$(this).html('<span style="color:#d0c21f !important; font-weight:bold;">'+ucfirst($(this).html().replace('wc-',''),true)+'</span>');
					  else if($(this).html()=='wc-pending')
						$(this).html('<span style="color:#ffba00 !important; font-weight:bold;">'+ucfirst($(this).html().replace('wc-',''),true)+'</span>');  
					  else
						$(this).html($(this).html());	
						
				});
				
			});
			function ucfirst(str,force){
				  str=force ? str.toLowerCase() : str;
				  return str.replace(/(\b)([a-zA-Z])/,
						   function(firstLetter){
							  return   firstLetter.toUpperCase();
						   });
			}
		</script>				
		
		<div class="wrap">
			<h2>Network Online Logs</h2>
			<div id="poststuff">
				<div id="post-body" class="metabox-holder columns-3">
					<div id="post-body-content">
						<div class="meta-box-sortables ui-sortable"> 
							<?php $this->customers_obj->views(); ?>
							<form method="post">
								<?php
								$this->customers_obj->prepare_items();
								//$this->customers_obj->search_box('Search', 'search');
								$this->customers_obj->display(); ?>
							</form>
						</div>
					</div>
				</div>
				<br class="clear">
			</div>
		</div>
	<?php
	}

	/**
	 * Screen options
	 */
	public function screen_option() {

		$option = 'per_page';
		$args   = [
			'label'   => 'Network Online Logs',
			'default' => 5,
			'option'  => 'customers_per_page'
		];

		add_screen_option( $option, $args );

		$this->customers_obj = new Abzer_Networkonline_Logs();
	}


	/** Singleton instance */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

}
