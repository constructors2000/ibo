<?php 
include_once('../../../../../wp-config.php');
include_once('../../../../../wp-load.php');
include_once('../../../../../wp-includes/wp-db.php');

if(get_site_option('woocommerce_abzer_networkonline_settings')){
	$settings 		   = get_site_option('woocommerce_abzer_networkonline_settings');
	if($settings['merchantids_array'])
		$merchantids_array =  unserialize($settings['merchantids_array']);
}else{
	$merchantids_array = array();
}	

$Country_array = array('AFN' => '/*Afghani*/' ,
						'ALL' => '/*Albanian Lek*/' ,
						'AZN' => '/*Azerbaijanian Manat*/' ,
						'DZD' => '/*Algerian Dinar*/' ,
						'AOA' => '/*Angolan Kwanza*/' ,
						'ARS' => '/*Argentine Peso*/' ,
						'AMD' => '/*Armenian Dram*/' ,
						'AWG' => '/*Aruban Florin*/' ,
						'AUD' => '/*Australian Dollar*/' ,
						'BSD' => '/*Bahamian Dollar*/' ,
						'BHD' => '/*Bahraini Dinar*/' ,
						'BDT' => '/*Bangladesh Taka*/' ,
						'BBD' => '/*Barbados Dollar*/' ,
						'BYR' => '/*Belarussian Ruble*/' ,
						'BZD' => '/*Belize Dollar*/' ,
						'BMD' => '/*Bermudan Dollar*/' ,
						'BTN' => '/*Bhutan Ngultrum*/' ,
						'BOB' => '/*Boliviano*/' ,
						'BAM' => '/*Bosnia-Herzegovina Convertible Mark*/' ,
						'BWP' => '/*Botswanan Pula*/' ,
						'BRL' => '/*Brazilian Real*/' ,
						'GBP' => '/*British Pound Sterling*/' ,
						'BND' => '/*Brunei Dollar*/' ,
						'BGN' => '/*Bulgarian New Lev*/' ,
						'BUK' => '/*Burmese Kyat*/' ,
						'BIF' => '/*Burundi Franc*/' ,
						'KHR' => '/*Cambodian Riel*/' ,
						'CAD' => '/*Canadian Dollar*/' ,
						'CVE' => '/*Cape Verde Escudo*/' ,
						'CZK' => '/*Czech Republic Koruna*/' ,
						'KYD' => '/*Cayman Islands Dollar*/' ,
						'GQE' => '/*Central African CFA Franc*/' ,
						'CLP' => '/*Chilean Peso*/' ,
						'CNY' => '/*Chinese Yuan Renminbi*/' ,
						'COP' => '/*Colombian Peso*/' ,
						'KMF' => '/*Comoro Franc*/' ,
						'CDF' => '/*Congolese Franc Congolais*/' ,
						'CRC' => '/*Costa Rican Colon*/' ,
						'HRK' => '/*Croatian Kuna*/' ,
						'CUP' => '/*Cuban Peso*/' ,
						'DKK' => '/*Danish Krone*/' ,
						'DJF' => '/*Djibouti Franc*/' ,
						'DOP' => '/*Dominican Peso*/' ,
						'XCD' => '/*East Caribbean Dollar*/' ,
						'EGP' => '/*Egyptian Pound*/' ,
						'SVC' => '/*El Salvador Colon*/' ,
						'ERN' => '/*Eritrean Nakfa*/' ,
						'EEK' => '/*Estonian Kroon*/' ,
						'ETB' => '/*Ethiopian Birr*/' ,
						'EUR' => '/*Euro*/' ,
						'FKP' => '/*Falkland Islands Pound*/' ,
						'FJD' => '/*Fiji Dollar*/' ,
						'GMD' => '/*Gambia Dalasi*/' ,
						'GEK' => '/*Georgian Kupon Larit*/' ,
						'GEL' => '/*Georgian Lari*/' ,
						'GHS' => '/*Ghana Cedi*/' ,
						'GIP' => '/*Gibraltar Pound*/' ,
						'GTQ' => '/*Guatemala Quetzal*/' ,
						'GNF' => '/*Guinea Franc*/' ,
						'GYD' => '/*Guyana Dollar*/' ,
						'HTG' => '/*Haitian Gourde*/' ,
						'HNL' => '/*Honduras Lempira*/' ,
						'HKD' => '/*Hong Kong Dollar*/' ,
						'HUF' => '/*Hungarian Forint*/' ,
						'ISK' => '/*Icelandic Krona*/' ,
						'INR' => '/*Indian Rupee*/' ,
						'IDR' => '/*Indonesian Rupiah*/' ,
						'IRR' => '/*Iranian Rial*/' ,
						'IQD' => '/*Iraqi Dinar*/' ,
						'ILS' => '/*Israeli New Sheqel*/' ,
						'JMD' => '/*Jamaican Dollar*/' ,
						'JPY' => '/*Japanese Yen*/' ,
						'JOD' => '/*Jordanian Dinar*/' ,
						'KZT' => '/*Kazakhstan Tenge*/' ,
						'KES' => '/*Kenyan Shilling*/' ,
						'KWD' => '/*Kuwaiti Dinar*/' ,
						'KGS' => '/*Kyrgystan Som*/' ,
						'LAK' => '/*Laotian Kip*/' ,
						'LVL' => '/*Latvian Lats*/' ,
						'LBP' => '/*Lebanese Pound*/' ,
						'LSL' => '/*Lesotho Loti*/' ,
						'LRD' => '/*Liberian Dollar*/' ,
						'LYD' => '/*Libyan Dinar*/' ,
						'LTL' => '/*Lithuanian Lita*/' ,
						'MOP' => '/*Macao Pataca*/' ,
						'MKD' => '/*Macedonian Denar*/' ,
						'MGA' => '/*Malagasy Ariary*/' ,
						'MWK' => '/*Malawi Kwacha*/' ,
						'MYR' => '/*Malaysian Ringgit*/' ,
						'MVR' => '/*Maldive Islands Rufiyaa*/' ,
						'LSM' => '/*Maloti*/' ,
						'MRO' => '/*Mauritania Ouguiya*/' ,
						'MUR' => '/*Mauritius Rupee*/' ,
						'MXN' => '/*Mexican Peso*/' ,
						'MDL' => '/*Moldovan Leu*/' ,
						'MNT' => '/*Mongolian*/' ,
						'MAD' => '/*Moroccan Dirham*/' ,
						'MZN' => '/*Mozambique Metical*/' ,
						'MMK' => '/*Myanmar Kyat*/' ,
						'NAD' => '/*Namibia Dollar*/' ,
						'NPR' => '/*Nepalese Rupee*/' ,
						'ANG' => '/*Netherlands Antillan Guilder*/' ,
						'YTL' => '/*New Turkish Lira*/' ,
						'NZD' => '/*New Zealand Dollar*/' ,
						'NIC' => '/*Nicaraguan Cordoba*/' ,
						'NGN' => '/*Nigerian Naira*/' ,
						'KPW' => '/*North Korean Won*/' ,
						'NOK' => '/*Norwegian Krone*/' ,
						'OMR' => '/*Oman Rial*/' ,
						'PKR' => '/*Pakistan Rupee*/' ,
						'PAB' => '/*Panamanian Balboa*/' ,
						'PGK' => '/*Papua New Guinea Kina*/' ,
						'PYG' => '/*Paraguay Guarani*/' ,
						'PEN' => '/*Peruvian Nuevo Sol*/' ,
						'PHP' => '/*Philippine Peso*/' ,
						'PLN' => '/*Polish Zloty*/' ,
						'QAR' => '/*Qatari Rial*/' ,
						'RHD' => '/*Rhodesian Dollar*/' ,
						'RON' => '/*Romanian Leu*/' ,
						'RUB' => '/*Russian Ruble*/' ,
						'RWF' => '/*Rwandan Franc*/' ,
						'SHP' => '/*Saint Helena Pound*/' ,
						'STD' => '/*Sao Tome Dobra*/' ,
						'SAR' => '/*Saudi Riyal*/' ,
						'RSD' => '/*Serbian Dinar*/' ,
						'SCR' => '/*Seychelles Rupee*/' ,
						'SLL' => '/*Sierra Leone Leone*/' ,
						'SGD' => '/*Singapore Dollar*/' ,
						'SKK' => '/*Slovak Koruna*/' ,
						'SBD' => '/*Solomon Islands Dollar*/' ,
						'SOS' => '/*Somali Shilling*/' ,
						'ZAR' => '/*South African Rand*/' ,
						'KRW' => '/*South Korean Won*/' ,
						'LKR' => '/*Sri Lanka Rupee*/' ,
						'SDG' => '/*Sudanese Pound*/' ,
						'SRD' => '/*Surinam Dollar*/' ,
						'SZL' => '/*Swaziland Lilangeni*/' ,
						'SEK' => '/*Swedish Krona*/' ,
						'CHF' => '/*Swiss Franc*/' ,
						'SYP' => '/*Syrian Pound*/' ,
						'TWD' => '/*Taiwan New Dollar*/' ,
						'TJS' => '/*Tajikistan Somoni*/' ,
						'TZS' => '/*Tanzanian Shilling*/' ,
						'THB' => '/*Thai Baht*/' ,
						'TOP' => '/*Tonga Pa?anga*/' ,
						'TTD' => '/*Trinidad and Tobago Dollar*/' ,
						'TND' => '/*Tunisian Dinar*/' ,
						'TMM' => '/*Turkmenistan Manat*/' ,
						'USD' => '/*US Dollar*/' ,
						'UGX' => '/*Ugandan Shilling*/' ,
						'UAH' => '/*Ukrainian Hryvnia*/' ,
						'AED' => '/*United Arab Emirates Dirham*/' ,
						'UYU' => '/*Uruguay Peso Uruguayo*/' ,
						'UZS' => '/*Uzbekistan Sum*/' ,
						'VUV' => '/*Vanuatu Vatu*/' ,
						'VEB' => '/*Venezuelan Bolivar*/' ,
						'VEF' => '/*Venezuelan bolívar fuerte*/' ,
						'VND' => '/*Vietnamese Dong*/' ,
						'CHE' => '/*WIR Euro*/' ,
						'CHW' => '/*WIR Franc*/' ,
						'XOF' => '/*West African CFA franc*/' ,
						'WST' => '/*Western Samoa Tala*/' ,
						'YER' => '/*Yemeni Rial*/' ,
						'ZMK' => '/*Zambian Kwacha*/' ,
						'ZWD' => '/*Zimbabwe Dollar*/' ,
						'TRY' => '/*Turkish Lira*/' ,
						'AZM' => '/*Azerbaijani Manat (1993-2006)*/' ,
						'ROL' => '/*Old Romanian Leu*/' ,
						'TRL' => '/*Old Turkish Lira*/' ,
						'XPF' => '/*CFP Franc*/'
);

?>

<style type="text/css">
.container:after { /*clear float*/
    content: "";
    display: table;
    clear: both;
}
.container > div {
    float: left;
    width: 33.33%;
	height: 25px;
    border: 1px solid #555;
    box-sizing: border-box;
    text-align: left;
	font-size: 14px; !important;font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif;
}
@media (max-width: 480px) { /*breakpoint*/
    .container > div {
        float: none;
        width: 100%;
    }
}
</style>

<div style="padding-left:230px; padding-bottom:10px;" id="referdiv">
	<a href="void(0);"; style="text-decoration:none;" id='refer'>Please refer Currency Code </a> For eg : United Arab Emirates Dirham Currency is <i>AED</i>
</div>

<div style="padding-left:230px; padding-bottom:10px; display:none;" class="container">
	 <?php foreach($Country_array as $key => $value) { ?>
		<div><b><i><?php echo $key; ?></i></b> <span style='padding-left:5px;'> <?php echo str_replace(array('/*',"*/"),'',$value); ?></span></div>
	<?php } ?>	
</div>

<!-- Here we need to display the currenicies of the SHOP -->
<div class="toolbar toolbar-top" style="padding-left:230px;">
	<button type="button" class="button add_attribute button-add" id="add_field_button">Add</button>
	<div class="woocommerce_attribute_data wc-metabox-content" id="input_fields_wrap">
		<table cellpadding="0" cellspacing="0">
			<?php if(count($merchantids_array) > 0) { 
				   foreach($merchantids_array as $merchantids ){	
			?>
			<tr>
				<td class="titledesc">
					<div><label class="titledesc">Currency</label></div>
					<div><input type="text" class="regular-input" name="woocommerce_abzer_networkonline_currency[]" value="<?php echo $merchantids[0]; ?>"></div>
				</td>
				<td class="titledesc">
					<div><label class="titledesc">Merchant Id</label></div>
					<div><input type="text" class="regular-input" name="woocommerce_abzer_networkonline_MID[]" value="<?php echo $merchantids[1]; ?>"></div>
				</td>
				<td class="titledesc">
					<div><label class="titledesc">Merchant Key</label></div>
					<div><input type="text" class="regular-input" style="width:380px !important;" name="woocommerce_abzer_networkonline_KEY[]" value="<?php echo $merchantids[2]; ?>"></div>
				</td>
				<td class="titledesc">
					<div style="margin:18px 0 0 7px;"><button type="button" class="button add_attribute button-remove removeRow">Remove</button></div>
				</td>
			</tr>
			
		   <?php }} ?>
			<!--<tr>
				<td class="titledesc">
					<div><label class="titledesc">Currency</label></div>
					<div><input type="text" class="regular-input" name="woocommerce_abzer_networkonline_currency[]" value=""></div>
				</td>
				<td class="titledesc">
					<div><label>Merchant Id</label></div>
					<div><input type="text" class="regular-input" name="woocommerce_abzer_networkonline_MID[]" value=""></div>
				</td>
				<td class="titledesc">
					<div><label>Merchant Key</label></div>
					<div><input type="text" class="regular-input" name="woocommerce_abzer_networkonline_KEY[]" value=""></div>
				</td>
			</tr>-->
		</table>
	</div>
</div>