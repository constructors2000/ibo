<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/*
Plugin Name: Network Online Payment Gateway - WooCommerce
Plugin URI:  https://www.abzer.com/
Description: Extends WooCommerce by Adding the Network Online Payment Gateway.
Version: 	 1.0.0
Author:      Rajeevan , ABZER Technology Solutions  
Author URI:  https://www.abzer.com/
*/
//abzer-networkonline-gateway.php

// Include our Gateway Class and Register Payment Gateway with WooCommerce
add_action( 'plugins_loaded', 'abzer_networkonline_gateway_init', 0 );

	function abzer_networkonline_gateway_init() {
		
		// If the parent WC_Payment_Gateway class doesn't exist
		// it means WooCommerce is not installed on the site
		// so do nothing
		if ( ! class_exists( 'WC_Payment_Gateway' ) ) return;
		
		// If we made it this far, then include our Gateway Class
		include_once( 'abzer-networkonline.php' );
		include_once( 'abzer-networkonline-logs.php' );
		
		// Now that we have successfully included our class,
		// Lets add it too WooCommerce
		add_filter( 'woocommerce_payment_gateways', 'abzer_networkonline_gateway' );
		function abzer_networkonline_gateway( $methods ) {
			$methods[] = 'ABZER_Networkonline';
			return $methods;
		}
	}

// Add custom action links
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'abzer_networkonline_gateway_action_links' );
	function abzer_networkonline_gateway_action_links( $links ) {
		$plugin_links = array(
			'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=abzer_networkonline' ) . '">' . __( 'Settings', 'abzer_networkonline_gateway' ) . '</a>',
			'<a href="' . admin_url( 'admin.php?page=abzer-networkonline-logs' ) . '">' . __( 'Network Online Logs', 'abzer_networkonline_gateway' ) . '</a>'
		);
		// Merge our new link with the default ones
		return array_merge( $plugin_links, $links );	
	}

// run the install scripts upon plugin activation , CREATE woocommerce_networkonline TABLE 
register_activation_hook(__FILE__,'abzer_networkonline_gateway_options_install');

	function abzer_networkonline_gateway_options_install() {
		
		 global $wpdb;
		 $table_name = $wpdb->prefix."woocommerce_networkonline";//Network Online Table Name 
		 
		 $collate    = '';
		 if ( $wpdb->has_cap( 'collation' ) ) {
				$collate = $wpdb->get_charset_collate();
		 }
		 
		 if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
			 
			   $sql = "DROP TABLE IF EXISTS `{$table_name}`;	
			   CREATE TABLE `{$table_name}` (
				  `neo_id` int(10) NOT NULL auto_increment,
				  `order_key` varchar(200) NOT NULL,
				  `order_id` varchar(50) NOT NULL,
				  `request` text NOT NULL,
				  `response` text NOT NULL,
				  `auth_code` varchar(50) NOT NULL,
				  `type` varchar(10) NOT NULL,
				  `status` varchar(50) NOT NULL,
				  `error_code` int(10) NOT NULL,
				  `created_on` datetime NOT NULL,
				   PRIMARY KEY  (neo_id),
				   UNIQUE KEY order_id (order_id)
				) $collate;";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($sql);	
		 }
		
	}
// Display the Network Online logs , in Grid @Admin side 
add_action( 'plugins_loaded', function () {SP_Plugin::get_instance();} );

// CSS @ Grid column width change 
add_action( 'admin_head', 'style_my_table' );

	function style_my_table() {
		  echo '<style type="text/css">';
		  echo '.column-cb       { width: 5% !important; }';
		  echo '.column-order_id { width: 7% !important; }';
		  echo '.column-order_key{ width: 10% !important; }';
		  echo '.column-request  { width: 13% !important; }';
		  echo '.column-response { width: 30% !important; }';
		  echo '.column-auth_code{ width: 10% !important; }';
		  echo '.column-amount	 { width: 7% !important; }';
		  echo '.button-add	 { color:black !important; background-color:#73a724 !important; }';
		  echo '.button-remove	 { color:black !important; background-color:#ef0101 !important; }';
		  echo '.regular-input	 { width: 220px; padding: 6px 0px; margin: 1px 5px; box-sizing: border-box; border: 2px solid #555}';
		  echo 'label.titledesc  { margin: 0 0 0 3px !important; color: #444 !important; font-size: 14px; !important;font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif;}';
		  echo '.remove_field  { margin: 5px 0 0 5px !important;}';
		  echo '</style>';
	}

// ---- Schedule cron event for Network Online Query API .
add_action('init', 'networkonline_query_api');

	function networkonline_query_api() {
		if (wp_next_scheduled('networkonline_query_api_action') == false) {
			wp_schedule_event(time(), 'hourly', 'networkonline_query_api_action');
		}
		add_action('networkonline_query_api_action', 'query_api_function');
	}

	// ---- Execute custom plugin function.
	function query_api_function() {
		// Get the database Object 
		global $wpdb;
		
		// Load the XML files for Query API 
		$xml_path = plugin_dir_url( __FILE__ ) . 'assets/view/';
		
		// Include the Network Online Settings Class 	
		include_once( 'abzer-networkonline.php' );
		$neoObject = new ABZER_Networkonline();
		
		// set the Networ Online Query API Settings 
		$neoapi['collaboratorId']      = 'NI';
		$neoapi['iv']      			   = '0123456789abcdef';
		$neoapi['encryptMethod']       = MCRYPT_RIJNDAEL_128;
		$neoapi['encryptMode']         = MCRYPT_MODE_CBC;		
		$neoapi['transactionDataBlock']= true; 
		$neoapi['blockEI'] 			   = setblockExistenceIndicator($neoapi);
		
		if($neoObject->enabled=='yes'){
				
				// Set the Webservice XML file 
				if($neoObject->settings['transactionmode']=='TEST')
					$neoapi['webServiceUrl'] 	 = $xml_path.'InvokePMTBitMapWebService_test.xml';
				else
					$neoapi['webServiceUrl']     = $xml_path.'InvokePMTBitMapWebService_live.xml';
				
				// Build the query 
				$query_api_sql = "SELECT
									  NEO.order_key
									, NEO.order_id
									, NEO.status
									, NEO.request
									, NEO.response
									, PO.ID as post_id
									, PO.post_status
									, PM.meta_key
									, PM.meta_value
									FROM ".$wpdb->prefix."posts AS PO 
									LEFT JOIN ".$wpdb->prefix."woocommerce_networkonline AS NEO ON (NEO.order_id = PO.ID)
									LEFT JOIN ".$wpdb->prefix."postmeta AS PM ON (PM.post_id = PO.ID)
									WHERE ((PO.post_status = 'wc-pending') OR (PO.post_status ='wc-pending') OR (DATE_ADD(PO.post_date, interval 60 MINUTE) < NOW()))
										AND PM.meta_key = '_order_currency'
										AND NEO.status = 'wc-pending'
										AND PO.ID <> ''
										AND PO.post_status <> 'wc-cancelled'
										ORDER BY NEO.order_id DESC ";
									
				 //$query_api_sql .= " LIMIT 1 ";	
				 
				 echo $query_api_sql;
				 
				 $neoapi['query_api_sql'] = $query_api_sql;
				 
				 $results = $wpdb->get_results( $query_api_sql, 'ARRAY_A' );
				 // Check the results and Loop the Quering Array 
				 if($results) {
					 
					foreach( $results as $neoArray ){
						// Here we need to check the Multi Currency 
						$getOrderCurrencyCode	=  $neoArray['meta_value'];
						
						// Check if the MultiCurrency is been enabled or not 
						if($neoObject->settings['multicurrency']=='YES'){
							$merchantids_array = unserialize($neoObject->settings['merchantids_array']);
							$currency_code     = $getOrderCurrencyCode;
							
							// Check the array with the Order currency 
							if (array_key_exists($currency_code, $merchantids_array)) {
								$nolpaymerchantids = $merchantids_array[$currency_code];
							}
							if(isset($nolpaymerchantids)){
								$neoapi['merchantId'] = $nolpaymerchantids[1];
								$neoapi['merchantKey']= $nolpaymerchantids[2];
							}		
						}else{
							$neoapi['merchantId']     =  $neoObject->settings['merchantid'];     
							$neoapi['merchantKey']    =  $neoObject->settings['merchantkey'];
						}
						
						// Since we are having two correct values 
						$neoapi['transactionArray'] = array(  'ReferenceID' 		=> '',
														  'merchantOrderNumber' => $neoArray['order_id'],
														  'transactionType' 	=> ''
													);
						$TransactionFieldExistenceIndicator = setTransactionFieldExistenceIndicator($neoapi);
						$TransactionData   	   = join('|',array_filter($neoapi['transactionArray']));
						$TransactionDataBlock1 = $TransactionFieldExistenceIndicator.'|'.$TransactionData;
						
						$DataBlocksArray       = array('DataBlockBitmap'  => $neoapi['blockEI'],
													   'DataBlock1' 	  => $TransactionDataBlock1
													   );
						$neoapi['DataBlocksArray']= $DataBlocksArray;
						
						$beforeEncryptionString   = join('||',array_filter($DataBlocksArray));
						$neoapi['beforeEncryptionString']= $beforeEncryptionString;
						
						$EncryptedString 		  = encryptData($beforeEncryptionString,$neoapi['merchantKey'],$neoapi['iv']);
						$neoapi['EncryptedString']= $EncryptedString;
						
						$NeoPostData      		  = $neoapi['merchantId'].'||'.$neoapi['collaboratorId'].'||'.$EncryptedString;	
						$neoapi['NeoPostData']	  = $NeoPostData;
						
						// Get the Response From Network Online using SOAP Request 
						$neoResonse 			  = getQueryAPIRespose($neoapi);
						$neoapi['return']         = $neoResonse;
						
						if($neoResonse['return']!='')
							$responseParameter  = decryptResponse($neoapi);
						else
							echo 'No Response from Netowrk Online'; // Send mail to the administrator
						
						$neoapi['responseDec']  = $responseParameter;
						$neoapi['responseSet']  = setValidateOrderQueryAPI( $responseParameter );
						
					}
					error_log(print_r($neoapi,true));
				 }	

		}
	}
	
	/** 
	 * setblockExistenceIndicator Function which is used to return the Block Existence Indicator Bitmap  
	 * Function PARAMETERS : NULL 	
	 * returns : The Block Existence Indicator Bitmap : String 
     */	
	function setblockExistenceIndicator($neoapi){
		
			$blockExistenceIndicator  = '';
			// DataBlock 1 transactionDataBlock
			$blockExistenceIndicator .= $neoapi['transactionDataBlock'] ? '1' : '0';
			
		return $blockExistenceIndicator;
	}
	// Function setTransactionFieldExistenceIndicator Filed Existence Indicator (BEI) 
	// for Transaction Block Data 
	/*
	 * setTransactionFieldExistenceIndicator 
	 * Get all the required property values form the constructor to generate the Bitmap value and the POSTING data 
	   for the Transaction Block Data, Check if the transactionDataBlock is set to true then generate the Bitmap values 
	 * @returns Bitmap value for the Transaction Block Data 
	 * 
	 */
	function setTransactionFieldExistenceIndicator($transactionArray){
		
		if($transactionArray['transactionDataBlock']){
			if(is_array($transactionArray)){
					$transactionFieldEI = '';
				$transactionFieldEI .= $transactionArray['transactionArray']['ReferenceID'] ? '1':'0';	
				$transactionFieldEI .= $transactionArray['transactionArray']['merchantOrderNumber'] ? '1':'0';	
				$transactionFieldEI .= $transactionArray['transactionArray']['transactionType'] ? '1':'0';	
			 return $transactionFieldEI;	
			}
		}else{
			return '';
		}
		
	}
	/**
	* Encrypts data with required encryption algorithm
	* @param string $data string which needs to be encrypted 			
	* @param string $key  key to encrypt data
	* @param string $iv   initializes CBC encryption
	* @return string 	  encrypted string
	*/
	function encryptData( $data, $key, $iv ){				$enc_method			= 'AES-256-CBC';       		$base64_decode_key  = base64_decode( $key );			$options            = 0;		$crypt				= openssl_encrypt( $data, $enc_method ,$base64_decode_key, $options, $iv );    		return $crypt;	
	}
	
	function getQueryAPIRespose($NeoPostData){
			
			if($NeoPostData){
				
				$webServiceUrl 		= $NeoPostData['webServiceUrl'];
				$context 		    = stream_context_create(array(
											'ssl' => array('verify_peer' => false,
														'verify_peer_name' => false,
														'allow_self_signed' => true)
											));					
				$client 			= new SoapClient( $webServiceUrl, 
														array( 'stream_context' => $context, 'trace' => 1 ) 
													 );
				$response 			= $client->__soapCall('invokeQueryAPI', 
												array(
														'requestparameters' => 
														array('requestparameters'=>$NeoPostData['NeoPostData'])) 
													);
				$array 				= (array) $response;
				
				return $array;
		}
	}
	
	function decryptResponse( $responseParameter ){
		
		if($responseParameter){
			$encrypt_key  = $responseParameter['merchantKey'];			$merchantId   = $responseParameter['merchantId'];									$enc_method   		= 'AES-256-CBC';			$base64_decode_key  = base64_decode( $encrypt_key );			$options            = 0;			$iv    	   = "0123456789abcdef";			$text      			= openssl_decrypt( $encryptString, $enc_method, $base64_decode_key, $options, $iv );
				$reponseArray 	  	 = explode("||",$text);
				$blockEI 			 = $reponseArray[0]; // It has to contains Seven indicators
				$bitmapString        = str_split($blockEI);
				$blockEIArrayKey     = array(
												'Transaction_Details', 			   		//Same as Request 
												'Amount_Block',    						// Transaction related information 
												'Status',    							//  Transaction Status information 
												'Card_related_information',    			//   Merchant Information 
												'Fraud_Block',    			       //    Fraud Block 
												'DCC_Block',    			      //     DCC Block 
											);	
				$bit 		  = 0;
				$blockEIArray = array();

				foreach($blockEIArrayKey as $blockValues){
					$blockEIArray[$blockValues] = $bitmapString[$bit];
					$bit++;
				}
				$blockEIArray = array_filter($blockEIArray);
				// Remove the first element from Array to map with the bit map values 
				array_shift($reponseArray);
				$resposeAssignedArray = array();
				$res 				  = 0;
				foreach($blockEIArray as $key => $value){
						$resposeAssignedArray[$key] =  $reponseArray[$res];
					$res++;
				}
						$TransactionResposeValue['text']		    = $merchantId.'||'.$text;;
						$TransactionResposeValue['merchantId']		= $merchantId;
						$TransactionResposeValue['DataBlockBitmap']	= $blockEI;
				foreach($blockEIArrayKey as $key => $value){
						if(isset($resposeAssignedArray[$value]))
							$TransactionResposeValue[$value] = $resposeAssignedArray[$value];
						else
							$TransactionResposeValue[$value] = 'NULL';
				}
		   return $TransactionResposeValue;			
			
		}else{
			return false;
		}
		
	}
	// Update the order Status & Transcation ID , update the Network Online table 
	function setValidateOrderQueryAPI( $responseArray ){
		
			global $woocommerce;
			global $wpdb; 
			
		if($responseArray['Status']!='NULL'){
			// Get the Transaction_related_information
			if($responseArray['Transaction_Details']!='NULL'){
				$Transaction_Details = explode('|',$responseArray['Transaction_Details']);
				if($Transaction_Details[0]=='01'){
					$Transaction_Details[2] = $Transaction_Details[1];
					$Transaction_Details[1] = '';
				}
				if(isset($Transaction_Details[1]))
					$transactionId 		 = (string)$Transaction_Details[1];
				else
					$transactionId  	 = '';
			}else{
				$transactionId  		 = '';
			}
			// Get the order details passing the order id 
			$order  = new WC_Order( $Transaction_Details[2] );
			// Get the Status 
			$Status = explode('|',$responseArray['Status']);
			
			// Get Card Related infromation 
			if($responseArray['Card_related_information']!='NULL'){
				$Cardrelated_Details = convertCardreleatedDetailsArray($responseArray['Card_related_information']);
				$card_brand      	 = $Cardrelated_Details['CardType'];
				$card_number     	 = $Cardrelated_Details['Card_Number'];
				$auth_code     	 	 = $Cardrelated_Details['Auth_Code'];
			}else{
				$card_brand      	 = '';
				$card_number     	 = '';
				$auth_code       	 = '';
			}
			
			$sucessStatusArray = array('Capture/Sale Success',
													'Auth Success',
													'Settled'
												   );
			if(($Status[2]=='00000')&&($Status[3]=='No Error.')&&(in_array($Status[1],$sucessStatusArray))){
					
					// Add the transactionId to the order 
					$order->payment_complete($transactionId);
					$order->add_order_note('Customer Successfully Completed the payment from Network Online Site , updated by Query API Cron Ref Number/Transaction ID: '.$transactionId);
					//Reduce stock levels , Reduce the stock 
					//wc_reduce_stock_levels($Transaction_Details[2]);
					//$order->add_order_note('Order item stock reduced successfully');
					
					// Update the Network Online table (woocommerce_networkonline)
					$network_onlineTable = array("response"   => $responseArray['text'], 
												 "auth_code"  => isset($auth_code) ? $auth_code : '',
												 "status"     => 'wc-processing',
												 "type"  	  => 'C',
												 "error_code" => $Status[2]
									);
					$where = array('order_id' => $Transaction_Details[2]);
					$wpdb->update( $wpdb->prefix.'woocommerce_networkonline', $network_onlineTable, $where );


			}elseif(($Status[2]=='10099') && ($Status[1]=='FAILURE') && ($Status[3]=='No Record Found')){
				// Update the Network Online table (woocommerce_networkonline)
					$network_onlineTable = array("response"   => $responseArray['text'], 
												 "auth_code"  => isset($auth_code) ? $auth_code : '',
												 "status"     => 'wc-cancelled',
												 "type"  	  => 'C',
												 "error_code" => $Status[2]
									);
					$where = array('order_id' => $Transaction_Details[2]);
					$wpdb->update( $wpdb->prefix.'woocommerce_networkonline', $network_onlineTable, $where );
					
					// Change the status of the order & and add the order note as well 
					$order->add_order_note('Network Online Payment Error '.$Status[3]);
					$order->update_status('wc-cancelled');
			}else{
					// Update the Network Online table (woocommerce_networkonline)
					$network_onlineTable = array("response"   => $responseArray['text'], 
												 "auth_code"  => isset($auth_code) ? $auth_code : '',
												 "status"     => 'wc-failed',
												 "type"  	  => 'C',
												 "error_code" => $Status[2]
									);
					$where = array('order_id' => $Transaction_Details[2]);
					$wpdb->update( $wpdb->prefix.'woocommerce_networkonline', $network_onlineTable, $where );
					
					// Change the status of the order & and add the order note as well 
					$order->add_order_note('Network Online Payment Declined '.$Status[3]);
					$order->update_status('wc-failed');
			}		
			
			
		}else{
			return;
		}		
		
	}
	
	function convertCardreleatedDetailsArray($responseParameter){
		
		if($responseParameter!='NULL'){
			//echo $responseParameter.'<br/>';
			$transactionDetails = explode('|',$responseParameter);
			$blockEI 			= $transactionDetails[0]; // It has to Two indicators
			$bitmapString       = str_split($blockEI);
			$blockEIArrayKey    = array('PayModeType', 			   		 
										'CardType',    		 
										'CardEnrollmentResponse',    		 
										'ECI_Values',    		 
										'Card_Number',
										'Auth_Code'	
									   );
			$bit 		  = 0;
			$blockEIArray = array();
			foreach($blockEIArrayKey as $blockValues){
				if(isset($bitmapString[$bit]))
					$blockEIArray[$blockValues] = $bitmapString[$bit];
				$bit++;
			}
			$blockEIArray = array_filter($blockEIArray);
			// Remove the first element from Array to map with the bit map values 
			array_shift($transactionDetails);			
			$resposeAssignedArray = array();
			$res 				  = 0;
			foreach($blockEIArray as $key => $value){
					$resposeAssignedArray[$key] =  $transactionDetails[$res];
				$res++;
			}	
			foreach($blockEIArrayKey as $key => $value){
						if(isset($resposeAssignedArray[$value]))
							$TransactionResposeValue[$value] = $resposeAssignedArray[$value];
						else
							$TransactionResposeValue[$value] = 'NULL';
			}
		  return $TransactionResposeValue;
		}else{
		  return array('PayModeType' => '', 			   		 
						 'CardType'=>'',    		 
						 'CardEnrollmentResponse' => '',    		 
						 'ECI_Values'=>'',   		 
						 'Card_Number' =>'',
						 'Auth_Code' => ''	
						);
		}
		
	}	
	
	
	
/*
// Add custom action links
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'abzer_networkonline_action_links' );
function abzer_networkonline_action_links( $links ) {}
*/
?>