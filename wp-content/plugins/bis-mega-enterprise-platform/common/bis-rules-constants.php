<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

// Plugin version constant
define("BIS_RULES_ENGINE_VERSION_CONST", "BIS_RULES_ENGINE_VERSION");
define("BIS_RULES_ENGINE_PLATFORM_DIR", "BIS_RULES_ENGINE_PLATFORM_DIR");
define("BIS_GEO_NAME_USER", "BIS_GEO_NAME_USER");
define("BIS_RULES_ENGINE_ALLOWABLE_TAGS_CONST", "BIS_RULES_ENGINE_ALLOWABLE_TAGS_CONST");
define("BIS_RULES_ENGINE_ALLOWABLE_TAGS", "a:20:{i:0;s:1:\"a\";i:1;s:5:\"audio\";i:2;s:1:\"b\";i:3;s:10:\"blockquote\";i:4;s:2:\"br\";i:5;:\"h1\";i:6;s:2:\"h2\";i:7;s:2:\"h3\";i:8;s:2:\"h4\";i:9;s:2:\"h5\";i:10;s:2:\"h6\";i:11;s:2:\"hr\";i:12;s:1:\"i\";i:13;s:3:\"img\";i:14;s:2:\"li\";i:15;s:1:\"p\";i:16;s:4:\"span\";i:17;s:6:\"strong\";i:18;s:2:\"ul\";i:19;s:5:\"video\";}");
define("BIS_RULES_ENGINE_DELETE_DB", "BIS_RULES_ENGINE_DELETE_DB");
define("BIS_RULES_ENGINE_CACHE_INSTALLED", "BIS_RULES_ENGINE_CACHE_INSTALLED");
define("BIS_REDIRECT_META_TEMPLATE", "BIS_REDIRECT_META_TEMPLATE");
define("BIS_POPUP_META_TEMPLATE", "BIS_POPUP_META_TEMPLATE");
define("BIS_REDIRECT_POPUP_TEMPLATE", "BIS_REDIRECT_POPUP_TEMPLATE");
define("BIS_PRODUCT_POPUP_TEMPLATE", "BIS_PRODUCT_POPUP_TEMPLATE");
define("BIS_POPUP_VO", "BIS_POPUP_VO");
define("BIS_POPUP_TEMPLATE", "BIS_POPUP_TEMPLATE");
define("BIS_CONTINENT_COUNTRY_POPUP_TEMPLATE", "BIS_CONTINENT_COUNTRY_POPUP_TEMPLATE");
define("BIS_CONTINENT_POPUP_TEMPLATE", "BIS_CONTINENT_POPUP_TEMPLATE");
define("BIS_COUNTRY_FLAGS_POPUP_TEMPLATE", "BIS_COUNTRY_FLAGS_POPUP_TEMPLATE");
define("BIS_COUNTRY_POPUP_TEMPLATE", "BIS_COUNTRY_POPUP_TEMPLATE");
define("BIS_REGION_POPUP_TEMPLATE", "BIS_REGION_POPUP_TEMPLATE");
define("BIS_CITY_POPUP_TEMPLATE", "BIS_CITY_POPUP_TEMPLATE");
define("BIS_PAGE_POPUP_TEMPLATE", "BIS_PAGE_POPUP_TEMPLATE");
define("BIS_POST_POPUP_TEMPLATE", "BIS_POST_POPUP_TEMPLATE");
define("BIS_RULES_ENGINE_PLUGIN_FORCE_DELETE", "BIS_RULES_ENGINE_PLUGIN_FORCE_DELETE");
define("BIS_RULES_ENGINE_SITE", "http://docs.megaedzee.com/");
define("BIS_RULES_ENGINE_WIKI", "http://docs.megaedzee.com/");
define("BIS_WHERE_CAN_I_FIND_PURCHASE_CODE", "https://help.market.envato.com/hc/en-us/articles/202822600-Where-can-I-find-my-Purchase-Code-S");
define("BIS_MEGA_DASHBOARD_HELP", "http://docs.megaedzee.com/docs/mega-enterprise-platform/dashboard/");
define("BIS_NPROGRESS_BAR_COLOR_CONST","BIS_NPROGRESS_BAR_COLOR_CONST");
define("BIS_NPROGRESS_BAR_COLOR","#727cde");
define("BIS_RULES_DEBUG_MODE_CONT","BIS_RULES_DEBUG_MODE_CONT");
define("BIS_RULES_DEBUG_MODE","No");
define("BIS_RULES_EVALUATION_CONT", "BIS_RULES_EVALUATION_CONT");
define("BIS_RULES_EVALUATION", "No");
define("BIS_MEGA_PLATFORM_PLUGIN_ID", "bis_mega_platform");
define("BIS_MEGA_COUNTRY_DROPDOWN", "No");
define("BIS_MEGA_SEL_IPS", "bis_mega_sel_ips");
define("BIS_MEGA_BLACK_IPS", "bis_mega_black_ips");
define("BIS_MEGA_BLACK_RED", "bis_mega_black_red");

define("BIS_RULES_CRITERIA_ROWS_COUNT", 25);
define("BIS_RULES_ARRAY", "bis_rules_array");
define("BIS_REDIRECT_SESSION_RULES_ARRAY", "bis_redirect_session_rules_array");
define("BIS_REQUEST_RULES_ARRAY", "bis_request_rules_array");
define("BIS_REDIRECT_REQUEST_RULES_ARRAY", "bis_redirect_request_rules_array");
define("BIS_REQUEST_RULES", "bis_request_rules_");
define("BIS_EXCLUDE_PAGES", "bis_exclude_page");
define("BIS_APPEND_TO_PAGES", "bis_append_to_page");
define("BIS_APPEND_TO_POSTS", "bis_append_to_posts");
define("BIS_EXCLUDE_POSTS", "bis_exclude_posts");
define("BIS_EXCLUDE_WIDGETS", "bis_exclude_widgets");
define("BIS_POST_CAT_WIDGET_EXCLUDE", "bis_post_cat_widget_exclude");
define("BIS_POST_TAG_WIDGET_EXCLUDE", "bis_post_tag_widget_include");
define("BIS_PROD_CAT_WIDGET_EXCLUDE", "bis_prod_cat_widget_exclude");
define("BIS_PROD_TAG_WIDGET_EXCLUDE", "bis_prod_tag_widget_include");
define("BIS_EXCLUDE_CATEGORIES", "bis_exclude_categories");
define("BIS_REDIRECT_RULES", "bis_redirect_rules");
define("BIS_LOAD_RULE_THEME", "bis_load_rule_theme");
define("BIS_DEFAULT_THEME_DETAILS", "bis_default_theme_details");
define("BIS_IMPORT_RULE_SATUS", "BIS_IMPORT_RULE_SATUS");

define("BIS_REDIRECT_RULES_CACHE_KEY", "bis_redirect_rules_cache_key");

// DB Transaction constants
define("BIS_DB_START_TRANSACTION", "start transaction");
define("BIS_DB_COMMIT", "commit");
define("BIS_DB_ROLLBACK", "rollback");

// DB Error codes.
define("BIS_DUPLICATE_ENTRY", "duplicate_entry");
define("BIS_DUPLICATE_ENTRY_SQL_MESSAGE", "Duplicate entry");
define("BIS_GENERIC_DATABASE_ERROR", "generic_database_error");

// Message constants
define("BIS_MESSAGE_LOGICAL_RULE_DELETE_FAILED", "Delete failed, Please remove child rules before removing parent rules.");
define("BIS_MESSAGE_NO_RECORD_FOUND", "Logical rules not found");
define("BIS_MESSAGE_PAGE_RECORD_DELETE", "Error occurred while deleting page rules");


// Response results constants
define("BIS_STATUS", "status");
define("BIS_DATA", "data");
define("BIS_GEOLOCATION_DATA", "geo_data");
define("BIS_GEOLOCATION_WRAPPER", "geo_plugin_wrapper");

define("BIS_POPUP_DATA", "popup_data");
define("BIS_SUCCESS", "success");
define("BIS_MESSAGE_KEY", "message_key");
define("BIS_SUCCESS_WITH_NO_DATA", "success_with_no_data");
define("BIS_ERROR", "error");
define("BIS_INVALID_DATABASE_FILE", "bis_invalid_database_file");
define("BIS_NO_RECORDS_FOUND", "no_records_found");
define("BIS_NO_METHOD_FOUND", "no_method_found");
define("BIS_NO_SHORTCODE_FOUND", "no_shortcode_found");
define("BIS_INVALID_SHORTCODE", "invalid_shortcode");


// Rule type constants
define("BIS_PAGE_TYPE_RULE", 1);
define("BIS_REDIRECT_TYPE_RULE", 2);
define("BIS_POST_TYPE_RULE", 3);
define("BIS_WIDGET_TYPE_RULE", 4);
define("BIS_THEME_TYPE_RULE", 5);
define("BIS_CATEGORY_TYPE_RULE", 6);
define("BIS_LANGUAGE_TYPE_RULE", 7);
define("BIS_WOO_PRODUCT_TYPE_RULE", 8);
define("BIS_POPUP_TYPE_RULE", 9);
define("BIS_WOO_POPUP_TYPE_RULE", 10);
define("BIS_WOO_DISCOUNT_TYPE_RULE", 11);
define("BIS_RULE_TYPE_CONST", "bis_rule_type_const_");
define("BIS_RULE_CRITERIA_TRANSIENT_CONST", "bis_rule_criteria_transient_const");
define("BIS_RULE_RESET_TRANSIENT_CONST", "bis_rule_reset_transient_const");
define("BIS_RULE_COUNTRY_TRANSIENT_CONST", "bis_rule_country_transient_const");
define("BIS_RULE_RESET_SESSION_CONST", "bis_rule_reset_session_const");
define("BIS_RULE_LOGICAL_RULES_ACTIVE_TRANSIENT_CONST", "bis_rule_logical_rules_transient_const");
define("BIS_REDIRECT_LOGICAL_RULES_ACTIVE_TRANSIENT_CONST", "bis_redirect_logical_rules_transient_const");
define("BIS_RULE_RESET_TRANSIENT_CONST_CONST", "bis_rule_reset_transient_const_const");
//define("BIS_LOGICAL_POPUP_RULES", "BIS_LOGICAL_POPUP_RULES");
define("BIS_PAGE_POPUP_RULES", "BIS_PAGE_POPUP_RULES");
define("BIS_POST_POPUP_RULES", "BIS_POST_POPUP_RULES");
define("BIS_PRODUCT_POPUP_RULES", "BIS_PRODUCT_POPUP_RULES");


// Rule eval constants.
define("BIS_EVAL_SESSION_TYPE", 1);
define("BIS_EVAL_REQUEST_TYPE", 2);

// Values Type constants
define("BIS_PAGE_TYPE_VALUE", 1);
define("BIS_POST_TYPE_VALUE", 2);
define("BIS_WIDGET_TYPE_VALUE", 3);
define("BIS_SIDEBAR_TYPE_VALUE", 4);

// Dynamic expression for request rules
define("BIS_RULE_CATEGORY_EXPRESSION_APPEND", "XCG$");
define("BIS_RULE_PAGE_EXPRESSION_APPEND", "XPG$");
define("BIS_RULE_WOO_PRODUCT_TAG_EXPRESSION_APPEND", "XWPTAG$");
define("BIS_RULE_WP_POST_TAG_EXPRESSION_APPEND", "XWPPOSTTAG$");
define("BIS_RULE_WOO_PRODUCT_ATTRIBUTE_EXPRESSION_APPEND", "XWPATTR$");
define("BIS_RULE_PARAM_EXPRESSION_APPEND", "XPARAM$");
define("BIS_RULE_FORM_DATA_EXPRESSION_APPEND", "XFORMDATA$");
define("BIS_RULE_STATUS_EXPRESSION_APPEND", "XSTATUS$");
define("BIS_RULE_POST_EXPRESSION_APPEND", "XPT$");
define("BIS_RULE_REFERRAL_PATH_EXPRESSION_APPEND", "XRP$");
define("BIS_RULE_REFERRED_PATH_EXPRESSION_APPEND", "XRFDP$");
define("BIS_RULE_COOKIE_EXPRESSION_APPEND", "XCOOKIE$");
define("BIS_RULE_TEMP_EXPRESSION_APPEND", "XTEMP$");
define("BIS_RULE_SEARCH_EXPRESSION_APPEND", "XSEARCH$");

define("BIS_EXCLUDE_REQUEST_RULE_PAGES", "bis_exclude_request_rule_pages");
define("BIS_EXCLUDE_REQUEST_RULE_POSTS", "bis_exclude_request_rule_posts");

// Search constants
define("BIS_SEARCH_BY", "bis_re_search_by");
define("BIS_SEARCH_STATUS", "bis_re_status");


define("BIS_PAGE_RULE_EVALUATED", "bis_page_rule_evaluated_");
define("BIS_STATUS_EVALUATED", "bis_status_evaluated_");
define("BIS_CATEGORY_RULE_EVALUATED", "bis_category_rule_evaluated_");
define("BIS_EXCLUDE_CATEGORY_IDS", "bis_exclude_category_ids");
define("BIS_PAGE_ACTION_ID", 1000);
define("BIS_PAGE_CONTENT_POSITION", 1001);
define("BIS_CONTENT_IMAGE_SIZE", 1002);

define("BIS_POST_ACTION_ID", 1003);
define("BIS_POST_CONTENT_POSITION", 1004);

// Constant for text domain
define("BIS_RULES_ENGINE_TEXT_DOMAIN", "rulesengine");
define("BIS_GEOLOCATION_VO", "bis_geolocation_vo");

define("BIS_LOGICAL_RULE_RESET", "bis_logical_rule_reset");
define("BIS_LOGICAL_RULE_INIT_TIME", "bis_logical_rule_init_time");
define("BIS_PAGE_RULE_RESET", "bis_page_rule_reset");
define("BIS_POST_RULE_RESET", "bis_post_rule_reset");
define("BIS_PRODUCT_RULE_RESET", "bis_product_rule_reset");
define("BIS_APPLY_LANGUAGE", "bis_apply_language");

// Constants for dashboard.

// Language
define("BIS_PLATFORM_LANGUAGE_PLUGIN_ID", "bis_language_plugin");
define("BIS_PLATFORM_LANGUAGE_API_KEY", "cbxdwidktyirelyz7yoigxotsg7dmqqw");
define("BIS_PLATFORM_LANGUAGE_PLUGIN_DISPLAY_NAME", "Language Controller");
define("BIS_PLATFORM_LANGUAGE_CSS_CLASS", "language-rule-icon");
define("BIS_PLATFORM_LANGUAGE_PLUGIN_PATH", "admin.php?page=languagerules");
define("BIS_PLATFORM_LANGUAGE_PLUGIN_ABSPATH", "bis-language-switcher-addon/language-rules-index.php");
define("BIS_PLATFORM_LANGUAGE_PLUGIN_DESCRIPTION", "Define rules for switching languages.");

// Pages
define("BIS_PLATFORM_PAGE_PLUGIN_ID", "bis_page_plugin");
define("BIS_PLATFORM_PAGE_API_KEY", "0cjbnb0tud6yqrd99oe7vp58dbgb0hj9");
define("BIS_PLATFORM_PAGE_PLUGIN_DISPLAY_NAME", "Page Controller");
define("BIS_PLATFORM_PAGE_CSS_CLASS", "page-rule-icon");
define("BIS_PLATFORM_PAGE_PLUGIN_PATH", "admin.php?page=pagerules");
define("BIS_PLATFORM_PAGE_PLUGIN_ABSPATH", "bis-page-rules-addon/page-rules-index.php");
define("BIS_PLATFORM_PAGE_PLUGIN_DESCRIPTION", "Define rules for hide, show, append, replace pages.");

// Posts
define("BIS_PLATFORM_POST_PLUGIN_ID", "bis_posts_plugin");
define("BIS_PLATFORM_POST_API_KEY", "cs5nm273hbj6v9uyk2j1i2pd32el4tbr");
define("BIS_PLATFORM_POST_PLUGIN_DISPLAY_NAME", "Post Controller");
define("BIS_PLATFORM_POST_CSS_CLASS", "post-rule-icon");
define("BIS_PLATFORM_POST_PLUGIN_PATH", "admin.php?page=postrules");
define("BIS_PLATFORM_POST_PLUGIN_ABSPATH", "bis-post-rules-addon/post-rules-index.php");
define("BIS_PLATFORM_POST_PLUGIN_DESCRIPTION", "Define rules for hide, show, append, replace posts.");

// Products
define("BIS_PLATFORM_PRODUCT_PLUGIN_ID", "bis_products_plugin");
define("BIS_PLATFORM_PRODUCT_API_KEY", "cs5nm273hbj6v9uyk2j1i2pd32el4tbr");
define("BIS_PLATFORM_PRODUCT_PLUGIN_DISPLAY_NAME", "Product Controller");
define("BIS_PLATFORM_PRODUCT_CSS_CLASS", "product-rule-icon");
define("BIS_PLATFORM_PRODUCT_PLUGIN_PATH", "admin.php?page=bis_pg_productrules");
define("BIS_PLATFORM_PRODUCT_PLUGIN_ABSPATH", "bis-product-rules-addon/product-rules-index.php");
define("BIS_PLATFORM_PRODUCT_PLUGIN_DESCRIPTION", "Define rules for hide, show, append, replace products.");
define("BIS_PROD_GEN_FEILD_LOGICAL_ID", "bis_prod_gen_feild_logical_id");

// Discounts
define("BIS_PLATFORM_DISCOUNT_PLUGIN_ID", "bis_discount_plugin");
define("BIS_PLATFORM_DISCOUNT_API_KEY", "cs5nm273hbj6v9uyk2j1i2pd32el4tbr");
define("BIS_PLATFORM_DISCOUNT_PLUGIN_DISPLAY_NAME", "Discount Controller");
define("BIS_PLATFORM_DISCOUNT_CSS_CLASS", "discount-rule-icon");
define("BIS_PLATFORM_DISCOUNT_PLUGIN_PATH", "admin.php?page=bis_pg_discountrules");
define("BIS_PLATFORM_DISCOUNT_PLUGIN_ABSPATH", "bis-discount-rules-addon/discount-rules-index.php");
define("BIS_PLATFORM_DISCOUNT_PLUGIN_DESCRIPTION", "Define rules for controlling the woo product price discounts.");

// Categories
define("BIS_PLATFORM_CATEGORY_PLUGIN_ID", "bis_category_plugin");
define("BIS_PLATFORM_CATEGORY_API_KEY", "u7bg58n38p4jz4qlq674f9wfkx4p9fb5");
define("BIS_PLATFORM_CATEGORY_PLUGIN_DISPLAY_NAME", "Category Controller");
define("BIS_PLATFORM_CATEGORY_CSS_CLASS", "category-rule-icon");
define("BIS_PLATFORM_CATEGORY_PLUGIN_PATH", "admin.php?page=categoryrules");
define("BIS_PLATFORM_CATEGORY_PLUGIN_ABSPATH", "category-rules-addon/category-rules-index.php");
define("BIS_PLATFORM_CATEGORY_PLUGIN_DESCRIPTION", "Define rules for show or hide categories.");

// Widgets
define("BIS_PLATFORM_WIDGET_PLUGIN_ID", "bis_widget_plugin");
define("BIS_PLATFORM_WIDGET_API_KEY", "4vh9fgil1sfoz58zizzdtx72gq24ftk9");
define("BIS_PLATFORM_WIDGET_PLUGIN_DISPLAY_NAME", "Sidebar and Widget Controller");
define("BIS_PLATFORM_WIDGET_CSS_CLASS", "widget-rule-icon");
define("BIS_PLATFORM_WIDGET_PLUGIN_PATH", "admin.php?page=widgetrules");
define("BIS_PLATFORM_WIDGET_PLUGIN_ABSPATH", "widget-rules-addon/widget-rules-index.php");
define("BIS_PLATFORM_WIDGET_PLUGIN_DESCRIPTION", "Define rules for show or hide Sidebar & widgets.");
define("BIS_WIDGET_LOGICAL_RULES", "bis_widget_logical_rules");

// Themes
define("BIS_PLATFORM_THEME_PLUGIN_ID", "bis_theme_plugin");
define("BIS_PLATFORM_THEME_API_KEY", "o5uk6m5ahwxwz4o11bt0d2z7zhe1gq48");
define("BIS_PLATFORM_THEME_PLUGIN_DISPLAY_NAME", "Theme Controller");
define("BIS_PLATFORM_THEME_CSS_CLASS", "theme-rule-icon");
define("BIS_PLATFORM_THEME_PLUGIN_PATH", "admin.php?page=themerules");
define("BIS_PLATFORM_THEME_PLUGIN_ABSPATH", "theme-rules-addon/theme-rules-index.php");
define("BIS_PLATFORM_THEME_PLUGIN_DESCRIPTION", "Define rules for switching themes.");

// Redirect
define("BIS_PLATFORM_REDIRECT_PLUGIN_ID", "bis_redirect_plugin");
define("BIS_PLATFORM_REDIRECT_API_KEY", "g1kugxr2385sf9z5bjsblhc8z9gcfqej");
define("BIS_PLATFORM_REDIRECT_PLUGIN_DISPLAY_NAME", "Redirect Controller");
define("BIS_PLATFORM_REDIRECT_CSS_CLASS", "redirect-rule-icon");
define("BIS_PLATFORM_REDIRECT_PLUGIN_PATH", "admin.php?page=redirectrules");
define("BIS_PLATFORM_REDIRECT_PLUGIN_ABSPATH", "redirect-rules-addon/redirect-rules-index.php");
define("BIS_PLATFORM_REDIRECT_PLUGIN_DESCRIPTION", "Define rules for URL Redirection.");

// Google analytics
define("BIS_PLATFORM_GOOGLE_ANALYTICS_PLUGIN_ID", "bis_google_analytics_plugin");
define("BIS_PLATFORM_GOOGLE_ANALYTICS_API_KEY", "g1kugxr2385sf9z5bjsblhc8z9gcfqej");
define("BIS_PLATFORM_GOOGLE_ANALYTICS_PLUGIN_DISPLAY_NAME", "Google Analytics");
define("BIS_PLATFORM_GOOGLE_ANALYTICS_CSS_CLASS", "google-analytics-icon");
define("BIS_PLATFORM_GOOGLE_ANALYTICS_PLUGIN_PATH", "admin.php?page=bis_pg_dash_analytics");
define("BIS_PLATFORM_GOOGLE_ANALYTICS_PLUGIN_ABSPATH", "google-analytics-addon/google-analytics-index.php");
define("BIS_PLATFORM_GOOGLE_ANALYTICS_PLUGIN_DESCRIPTION", "Google analytics data.");

// Popups
define("BIS_PLATFORM_POPUP_PLUGIN_ID", "bis_popup_plugin");
define("BIS_PLATFORM_POPUP_API_KEY", "4vh9fgil1sfoz58zizzdtx72gq24ftk9");
define("BIS_PLATFORM_POPUP_PLUGIN_DISPLAY_NAME", "Popup Controller");
define("BIS_PLATFORM_POPUP_CSS_CLASS", "popup-rule-icon");
define("BIS_PLATFORM_POPUP_PLUGIN_PATH", "admin.php?page=popuprules");
define("BIS_PLATFORM_POPUP_PLUGIN_ABSPATH", "popup-rules-addon/popup-rules-index.php");
define("BIS_PLATFORM_POPUP_PLUGIN_DESCRIPTION", "Define rules for controlling of Popups.");
define("BIS_POPUP_LOGICAL_RULES", "bis_popup_logical_rules");

//Google analytics
define("BIS_ANALYTICS_SETTINGS", "bis_analytics_settings");

define("BIS_RULES_ENGINE", "http://docs.megaedzee.com/");

// Const for no redirect condition
define("BIS_NO_REDIRECT", "bis_nrd");
define("BIS_AUDIT_INFO", "bis_audit_vo");

define("RULES_ENGINE_MAIL", "rules4wp@gmail.com");

define("BIS_PUR_CODE", "BIS_PUR_CODE_");
define("BIS_SESSION_INITIATED", "initiated");
define("BIS_SESSION_RULEVO", "bis_session_rulevo");
define("BIS_CHILD_FILE_PATH", "bis_redirect_child_rule_path");
define("BIS_CHILD_RULE_ID", "bis_child_rule_id");
define("BIS_SESSION_LOGICAL_RULE_STATUS", "bis_session_logical_rule_status");

define("BIS_REPORT_CURRENT_MONTH", "current_month");
define("BIS_CAPTURE_ANALYTICS_DATA", "BIS_CAPTURE_ANALYTICS_DATA");
define("BIS_COUNTRY_SELECT", "bis_country");
define("BIS_SEARCH_BOX_VALUE", "bis_search_box_value");
define("BIS_COUNTRY_DROPDOWN_VALUE", "bis_country_dropdown_value");
define("BIS_COOKIE_EXPIRE_TIME", 2592000); // 30 DAYS
define("BIS_COOKIE_ONE_DAY_EXPIRE_TIME", 86400); // 1 DAYS
define("BIS_GEO_DETAILS", "BIS_GEO_DETAILS"); 
define("BIS_RESET_RULE_PARAM", "bis_reset_rule");
define("TRANSLATOR_DROPDOWN_COOKIE_KEY", "translator-dropdown-translator-dropdown-jquery-to");


define("BIS_LOGICAL_RULE_ID", "bis_logical_rule_id");

//1 = MaxMind DB 2= Geolocation plugin
define("BIS_GEO_LOOKUP_TYPE", "BIS_GEO_LOOKUP_TYPE");
define("BIS_GEO_IP2LOCATION_LOOKUP_TYPE", "BIS_GEO_IP2LOCATION_LOOKUP_TYPE");
define("BIS_GEO_IP2LOCATION_CSV_LOOKUP_TYPE", "BIS_GEO_IP2LOCATION_CSV_LOOKUP_TYPE");
define("BIS_GEO_IP2LOCATION_DB", "IP2LOCATION-LITE-DB11.BIN");
define("BIS_GEO_IP2LOCATION_CSV_DB", "IP2LOCATION-LITE-DB9.CSV");
define("BIS_GEO_LOOKUP_WEBSERVICE_TYPE", "BIS_GEO_LOOKUP_WEBSERVICE_TYPE");
define("BIS_GEO_MAXMIND_DB_FILE", "BIS_GEO_MAXMIND_DB_FILE");
define("BIS_GEO_MAXMIND_DB_FILE_NAME", "GeoLite2-Country.mmdb");
define("BIS_SOFT_EXCLUDE_CATEGORY_IDS", "bis_soft_exclude_category_ids");
define("BIS_USER_COUNTRY_DROPDOWN", "bis_user_country_dropdown");

define("BIS_REDIRECT_COOKIE", "bis_red_cookie");
define("BIS_REDIRECT_COOKIE_REDIRECT", "redirect");
define("BIS_REDIRECT_COOKIE_CANCEL", "cancel");
define("BIS_UPLOAD_DIRECTORY", "bis_rulesengine_uploads");
define("BIS_POST_RULE_DIRECTORY", "bis_postrules");
define("BIS_PAGE_RULE_DIRECTORY", "bis_pagerules");
define("BIS_PRODUCT_RULE_DIRECTORY", "bis_productrules");
//define("BIS_LOGICAL_RULE_DIRECTORY", "bis_logicalrules");
define("BIS_REDIRECT_RULE_DIRECTORY", "bis_redirectrules");
define("BIS_PAGE_SIZE", 10);
define("BIS_ROW_COUNT", "bis_row_count");
define("BIS_SEARCH_REQUEST", "bis_search_request");
define("BIS_ACTION", "bis_action");
define("BIS_POPUP_SHOWN_ONCE", "bis_popup_shown_once");
define("BIS_SESSION_APPLIED_RULES", "bis_session_applied_rules");
define("BIS_REDIRECT_SESSION_APPLIED_RULES", "bis_redirect_session_applied_rules");
define("BIS_REDIRECT_REQUEST_APPLIED_RULES", "bis_redirect_request_applied_rules");
define("BIS_REQUEST_APPLIED_RULES", "bis_request_applied_rules");
define("BIS_REQUEST_RULES_KEY", "bis_request_rules_key");
define("BIS_REDIRECT_REQUEST_RULES_KEY", "bis_redirect_request_rules_key");
define("BIS_IS_AJAX_REQUEST", "bis_is_ajax_request");
define("BIS_PLUGIN_TYPE_VALUE", "bis_plugin_type_value");

// Plugin constants:
define("BIS_PLF_THEME_PLUGIN_ID", "bis_theme_plugin");
define("BIS_PLF_PAGE_PLUGIN_ID", "bis_page_plugin");
define("BIS_PLF_POST_PLUGIN_ID", "bis_posts_plugin");
define("BIS_PLF_PRODUCT_PLUGIN_ID", "bis_products_plugin");
define("BIS_PLF_DISCOUNT_PLUGIN_ID", "bis_discount_plugin");
define("BIS_PLF_REDIRECT_PLUGIN_ID", "bis_redirect_plugin");
define("BIS_PLF_LANGUAGE_PLUGIN_ID", "bis_language_plugin");
define("BIS_PLF_WIDGET_PLUGIN_ID", "bis_widget_plugin");
define("BIS_PLF_CATEGORY_PLUGIN_ID", "bis_category_plugin");
define("BIS_PLF_PROD_GEN_FIELD_RULE", "bis_prod_gen_field_rule");
define("BIS_PLF_WIDGET_ADMIN_RULE", "bis_widget_admin_rule");
define("BIS_PLF_POPUP_ADMIN_RULE", "bis_popup_plugin");

define("BIS_LOGICAL_CACHE_REFRESH_ID", "BIS_LOGICAL_CACHE_REFRESH_ID");

define("BIS_CHECK_IF_SHORTCODE", "bis_check_if_shortcode");

//Download constants
define("BIS_DOWNLOAD_CITY_DB", "http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.tar.gz");
define("BIS_DOWNLOAD_CONTRY_DB", "http://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.tar.gz");

//Queryfilter constants
define("BIS_GET_ALL_LOGICALRULE_VALUES", "bis_get_all_logicalrule_values");
define("BIS_GET_ALL_LOGICALRULE_CONDITIONS", "bis_get_all_logicalrule_conditions");
define("BIS_GET_QUERY_FOR_FILTER", "bis_get_query_for_filter");

define("BIS_TIME_PICKER", "timepicker");
define("BIS_DATE_PICKER", "datepicker");
define("BIS_TIME_DTAE_FORMAT", "format");
define("BIS_CLOSE_ON_DATE_TIME_SELECT", "closeOnDateSelect");
define("BIS_DEMO_SITE", "megaedzee.in");

define("BIS_RULES_KEY", "bis_rules");
define("BIS_POPUP_KEY", "bis_popup_key");
define("BIS_PAGE_RULES_KEY", "bis_page_rules");
define("BIS_REDIRECT_RULES_KEY", "bis_redirect_rules");
define("BIS_POST_RULES_KEY", "bis_post_rules");
define("BIS_WIDGET_RULES_KEY", "bis_widget_rules");
define("BIS_CATEGORY_RULES_KEY", "bis_category_rules");
define("BIS_WOO_PRODUCT_RULES_KEY", "bis_woo_product_rules");

define("BIS_MEGA_CACHE_KEY", "bis_mega_cache");
define("BIS_MEGA_PCODE_KEY", "bis_mega_pcode");
define("BIS_MEGA_CITY_KEY", "bis_mega_city");
define("BIS_MEGA_STATE_KEY", "bis_mega_state");
define("BIS_MEGA_COUNTRY_KEY", "bis_mega_country");
define("BIS_MEGA_CONTINENT_KEY", "bis_mega_continent");

//These are for extending browser execution time and memory.
define("SET_MAX_EXECUTION_TIME",1000);
define("SET_MAX_EXECUTION_MEMORY", '600M');

//popup constants 
define("BIS_POPUP_MAIL_INTEGRATION_DATA", 'BIS_POPUP_MAIL_INTEGRATION_DATA');

//all continents
define("BIS_ALL_CONTINENTS", 'BIS_ALL_CONTINENTS');
