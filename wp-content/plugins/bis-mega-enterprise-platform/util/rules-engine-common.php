<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use bis\repf\action\RulesEngine;
use bis\repf\common\RulesEngineCacheWrapper;
use bis\repf\common\BISSessionWrapper;
use bis\repf\model\LogicalRulesEngineModel;

class RulesEngineCommon {

    /**
     *
     * This function returns the true if rule is valid.
     *
     * @param $rule_name
     * @return bool
     */
    public static function is_rule_valid($rule_name) {
        
        $rules_engine = new RulesEngine();
        $bis_session_rules = array();
        $bis_request_rules = array();

        // Get the valid session
        $bis_session_rules_temp = RulesEngineUtil::get_applied_session_rules($rules_engine);

        if(!empty($bis_session_rules_temp)) {
            $bis_session_rules = $bis_session_rules_temp;
        }
        
        // Get the valid request rules

        $bis_request_rules_temp = $rules_engine->get_request_rules();
        
        if (!empty($bis_request_rules_temp)) {
            $bis_request_rules = $bis_request_rules_temp;
        }

        $succsess_array = array_merge($bis_session_rules, $bis_request_rules);
        
        if ($succsess_array != null && !empty($succsess_array)){
            
            foreach ($succsess_array as $valid_rule) {
                if (RulesEngineUtil::isEqual($valid_rule->name, $rule_name)) {
                    return true;
                }
            }
        }

        return false;
    }
    
    /**
     * Updates WP Rocket .htaccess rules and regenerates config file.
     *
     * @return bool
     */
    public static function wp_rocket_edd_cookie__housekeeping() {

        if (!function_exists('flush_rocket_htaccess'))
            return false;

        // Update WP Rocket .htaccess rules.
        flush_rocket_htaccess();

        // Regenerate WP Rocket config file.
        rocket_generate_config_file();

        // Return a value for testing.
        return true;
    }
    
    public static function get_geo_location_details(){
        return RulesEngineCacheWrapper::get_session_attribute(BIS_GEOLOCATION_VO);
    }
    
    public static function get_geo_location_details_by_ip($ipAddress){
        $logical_rules_model = new LogicalRulesEngineModel();
        return $logical_rules_model->get_geo_location($ipAddress);
    }
    
    public static function get_city($lat, $lng, $distance, $unit = 'mi') {
        global $wpdb;
        $limit = 1;
        $base_prefix = $wpdb->base_prefix;
        $geo_radius = RulesEngineCommon::get_geo_radius($lat, $lng, $distance, $unit);
        $query = $wpdb->prepare("SELECT city FROM (SELECT city, longitude, latitude FROM " . $base_prefix . "ip2location_db2 
                    WHERE latitude >= %f AND latitude <= %f ) AS mytable WHERE longitude >= %f AND longitude <= %f LIMIT %d", $geo_radius['minLat'], $geo_radius['maxLat'], $geo_radius['minLng'], $geo_radius['maxLng'], $limit);
        $city = $wpdb->get_results($query);
        return $city;
    }
    
    private static function get_geo_radius($lat, $lng, $distance, $unit = 'mi') {
        if ($unit == 'km') {
            $radius = 6371.009; // in kilometers
        } elseif ($unit == 'mi') {
            $radius = 3958.761; // in miles
        }

        $maxLat = (float) $lat + rad2deg($distance / $radius);
        $minLat = (float) $lat - rad2deg($distance / $radius);

        // longitude boundaries (longitude gets smaller when latitude increases)
        $maxLng = (float) $lng + rad2deg($distance / $radius / cos(deg2rad((float) $lat)));
        $minLng = (float) $lng - rad2deg($distance / $radius / cos(deg2rad((float) $lat)));
        
        $geo_radius_map = array(
            'maxLat' => $maxLat,
            'minLat' => $minLat,
            'maxLng' => $maxLng,
            'minLng' => $minLng,
        );
        
        return $geo_radius_map;
    }

    public static function get_near_by_cities($lat = null , $lng = null , $distance = 50, $unit = 'mi', $limit = 50) {
        
        if($lat == null || $lng == null){
            $sessionWrapper = new BISSessionWrapper();
            $geoPlugin = $sessionWrapper->getGeoPlugin();
            $lat = $geoPlugin->getLatitude();
            $lng = $geoPlugin->getLongitude();
        }
        global $wpdb;
        $base_prefix = $wpdb->base_prefix;
        
        $geo_radius = RulesEngineCommon::get_geo_radius($lat, $lng, $distance, $unit);

        $query = $wpdb->prepare("SELECT DISTINCT city FROM (SELECT city, longitude FROM " . $base_prefix . "ip2location_db2 
                    WHERE latitude >= %f AND latitude <= %f ) AS mytable WHERE longitude >= %f AND longitude <= %f LIMIT %d", $geo_radius['minLat'], $geo_radius['maxLat'], $geo_radius['minLng'], $geo_radius['maxLng'], $limit);
        $nearby = $wpdb->get_results($query);

        return $nearby;
    }
}