<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

use bis\repf\common\RulesEngineCacheWrapper;
use bis\repf\action\RulesEngine;
use bis\repf\vo\LabelValueVO;

class RulesEngineUtil {
 
    private function __construct() {
       
    }

    public static function get_url_content($url) {
       
        $response = wp_remote_get($url, array('timeout' => 120, 'httpversion' => '1.1'));
        $body = null;
        if (is_array($response)) {
            $body = $response['body']; // use the content
        }

        return $body;

    }

    public static function getIncludesDirPath() {   

        $pluginPath = RulesEngineUtil::getPluginAbsPath() . "/includes/";

        return $pluginPath;
    }

    public static function getPluginAbsPath() {

        $dirName = plugin_dir_path(__FILE__);
        $pluginPath = realpath($dirName);
        $path_array = explode("util", $pluginPath);

        return $path_array[0];
    }

    /**
     * This method is used to parse the json value and evaluat the values.
     *
     * @param unknown $tokenInput jsonToken value
     * @param unknown $arg2
     * @param unknown $condId
     * @return boolean
     */
    public static function evaluateTokenInputRule($tokenInput, $arg2, $condId) {

        $json_tokens = json_decode($tokenInput);
        $final_eval = false;
        $eval = false;
        if (($json_tokens != null) && (!empty($json_tokens))) {
            foreach ($json_tokens as $token) {
                $eval = RulesEngineUtil::evaluateStringTypeRule($token->id, $arg2, $condId);
                if ($eval && !($condId == 14 || $condId == 12)) {
                    $eval = true;
                    break;
                } else if ($condId == 14 && !$eval) { // Does not contain any of
                    $eval = false;
                    break;          
                } else if ($condId == 12 && $eval) { // Contains any of
                    $eval = true;
                    break;
                }
            }
        }
        return $eval;
    }

    /**
     * 
     * @param type $arg1 is an container
     * @param type $arg2 value to be checked
     * @param type $condId
     * @return type
     */
    public static function evaluateStringTypeRule($arg1, $arg2, $condId) {

        $eval_value = false;
        
        switch ($condId) {
            case 1 : // Equal
                $eval_value = RulesEngineUtil::isEqual($arg1, $arg2);
                break;

            case 2 : // Not Equal
                $eval_value = !RulesEngineUtil::isEqual($arg1, $arg2);
                break;

            case 3 : // Starts With
                $eval_value = RulesEngineUtil::startsWith($arg1, $arg2);
                break;

            case 4 : // Contains
                $eval_value = RulesEngineUtil::isContains($arg1, $arg2);
                break;

            case 5 : // Does not contain
                $eval_value = !RulesEngineUtil::isContains($arg1, $arg2);
                break;

            case 8 : // Domain is
                $eval_value = RulesEngineUtil::isDomain($arg1, $arg2);
                break;

            case 9 : // Ends With
                $eval_value = RulesEngineUtil::endsWith($arg1, $arg2);
                break;
            
            case 10 : // is Set
                $eval_value = !RulesEngineUtil::isNullOrEmptyString($arg1, $arg2);
                break;
            
            case 11 : // is Not Set
                $eval_value = RulesEngineUtil::isNullOrEmptyString($arg1, $arg2);
                break;
            
            case 12 : // Contains any of
                $eval_value = RulesEngineUtil::isContainsAnyOf($arg1, $arg2);
                break;
            
            case 14 : // Does not Contains any of
                $eval_value = !RulesEngineUtil::isContainsAnyOf($arg1, $arg2);
                break;
        }
        return $eval_value;
    }
    
    /**
     * This method checks if a value is contained in the comma seperated list.
     * 
     * @param type $needle is the value to be find
     * @param type $haystack comma seperated value
     * @return boolean
     */
    public static function isContainsAnyOf($haystack, $needle) {

        $heyStackList = explode(",", $haystack);
        foreach ($heyStackList as $value) {
            if (strcasecmp(trim($value), trim($needle)) == 0) {
                return true;
            }
        }
        return false;
    }

    public static function isDomain($arg1, $domain) {
        $email_domain = explode("@", $arg1);
        return RulesEngineUtil::isEqual($email_domain[1], $domain);
    }

    public static function isEqual($arg1, $arg2) {
        $isEqual = false;

        if (strcasecmp($arg1, $arg2) == 0) {
            $isEqual = true;
        }

        return $isEqual;
    }

    /**
     * This method is used to 
     * 
     * @param type $haystack is the container.
     * @param type $needle is the value to be checked
     * @return type
     */
    public static function startsWith($haystack, $needle) {
        // search backwards starting from haystack length characters from the end
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }
   
    public static function endsWith($haystack, $needle) {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }
    
    /**
     * Check whether $find in contained in $container.
     *
     * @param unknown $container value container
     * @param unknown $find value to find.
     * @return boolean
     */
    public static function isContains($container, $find) {
        $isContains = false;

        if ($find != null && (stripos($container, $find) !== false)) {
            $isContains = true;
        }

        return $isContains;
    }

    public static function isURLEquals($target_url, $current_url) {
        $eval = false;
        
        if($current_url == null) {
            return $eval;
        }
        
        if(!RulesEngineUtil::endsWith($target_url, "/")) {
            $target_url = $target_url ."/";
        }
        
        foreach ($current_url as $current_path) {
            $eval = RulesEngineUtil::isEqual($current_path, $target_url);
            if ($eval) {
                break;
            }
        }
        return $eval;
    }
    
    public static function evaluateURLTypeRule($cpath_array, $rule_path, $condId,
                                                $applied_rule=NULL) {

        $eval = false;
        if(($condId == 1 || $condId == 2)
                && !RulesEngineUtil::endsWith($rule_path, "/")) {
            $rule_path = $rule_path ."/";
        }
        
        switch ($condId) {
            case 1 : 
                foreach ($cpath_array as $current_path) {
                    $eval = RulesEngineUtil::isEqual($current_path, $rule_path);
                    if($eval) {
                        break;
                    }    
                }
                break;

            case 2 :
                
                foreach ($cpath_array as $current_path) {                
                    $eval = RulesEngineUtil::isEqual($current_path, $rule_path);
                    if ($eval) {
                        break;
                    }
                }
                
                     $eval = !$eval;
                break;

            case 4 :
                 foreach ($cpath_array as $current_path) {
                    $eval = RulesEngineUtil::isContains($current_path, $rule_path);
                    if ($eval) {
                        break;
                    }
                }    
                break;

            case 5 :
                foreach ($cpath_array as $current_path) {
                   
                    $eval = RulesEngineUtil::isContains($current_path, $rule_path);
                    
                    if ($eval) {
                        break;
                    }
                }
                $eval = !$eval;
                break;
                
            case 13 : // pattern match
               
                    $rule_values = explode("/**", $rule_path);
                    $rule_value = $rule_values[0];

                    foreach ($cpath_array as $current_path) {
                        if (!RulesEngineUtil::isContains($current_path, "bis_prd=1")) {
                            $eval = RulesEngineUtil::startsWith($current_path, $rule_value);

                            if ($eval) {
                                $applied_rule->condId = $condId;
                                $redirectPaths = explode($rule_value, $current_path);
                                $applied_rule->patternRedirect = $redirectPaths[1];
                                break;
                            }
                        }    
                    }
                break;
        }

        return $eval;
    }

      
    /**
     * Get the filter url
     *
     * @param $rule_path
     * @return string
     */
    public static function get_filter_url($rule_path) {

        /*if (!(substr($rule_path, 0, 7) === "http://" || substr($rule_path, 0, 8) === "https://")) {
            $rule_path = "http://" . $rule_path;
        }

        if (substr($rule_path, 0, 11) === "http://www.") {
            $rule_path_ar = explode("http://www.", $rule_path);
            $rule_path = "http://" . $rule_path_ar[1];
        } elseif (substr($rule_path, 0, 12) === "https://www.") {
            $rule_path_ar = explode("https://www.", $rule_path);
            $rule_path = "https://" . $rule_path_ar[1];
        }*/

        return $rule_path;
    }

    public static function evaluateParameterRules($value_type, $condId, $isAjaxRequest) {
        
        $query_string = $_SERVER['QUERY_STRING'];
        $param_values = explode('=', $value_type);
        $param_value = $param_values[1];
        $query_param = $param_values[0];
        
        parse_str($query_string, $query_param_array);
        
        if($isAjaxRequest) {
            if (array_key_exists("bis_re_cache_page_url", $query_param_array)) {
                $parts = parse_url($query_param_array['bis_re_cache_page_url']);
                if ($parts['query']) {
                    parse_str($parts['query'], $query_param_array);
                }
            } else {
                if (isset($query_param_array[$query_param])) {
                    $param1 = $query_param_array[$query_param];
                }
            }
        }
        
        $param1 = false;
        
        if(isset($query_param_array[$query_param])) {
            $param1 = $query_param_array[$query_param];
        }
        
        $eval = RulesEngineUtil::evaluateStringTypeRule($param1, $param_value, $condId);        
        
        return $eval;
    }
    
    public static function convertToSQLString($value) {
        $value = str_replace(",", "','", $value);
        $value = "'" . $value . "'";
        return $value;
    }
    
    public static function evaluateFormDataRules($value_type, $condId) {
        
        $eval = false;
        $form_param_value = '';
        $req_form_value = '';

        if(RulesEngineUtil::isContains($value_type, "=")) {
            $form_data = explode("=", $value_type); 
            $form_param =  $form_data[0];
            $form_param_value = $form_data[1];
        } else {
            $form_param = $value_type;
        }
        
        if(isset($_POST[$form_param])) {
            $req_form_value = $_POST[$form_param];
        } else {
            // No data for form submitted. Considered as form not submited.
            return $eval;
        }
        
        $eval = RulesEngineUtil::evaluateStringTypeRule($req_form_value, $form_param_value, $condId);
        
        return $eval;
    }
    
    public static function evaluateSearchFormDataRules($value_type, $condId) {

        $eval = false;
        $req_form_value = '';
        $selected_session_country_value = RulesEngineCacheWrapper::get_session_attribute(BIS_COUNTRY_DROPDOWN_VALUE);
        $selected_session_autocomplete_value = RulesEngineCacheWrapper::get_session_attribute(BIS_SEARCH_BOX_VALUE);
        if((isset($_POST["bis_autocomplete"]) && isset($_POST["bis_autocomplete_google_city"]) && isset($_POST["bis_autocomplete_google_state"]) 
                    && isset($_POST["bis_autocomplete_google_country"])) || ($selected_session_autocomplete_value !== null && $selected_session_autocomplete_value !== "")) {
            if (isset($_POST["bis_autocomplete_google_city"]) && $_POST["bis_autocomplete_google_city"] != "" && $_POST["bis_autocomplete_google_state"] != ""
                        && $_POST["bis_autocomplete_google_country"] != "") {
                $req_form_value = $_POST["bis_autocomplete_google_city"];
            } else if (isset ($_POST["bis_autocomplete_google_country"]) && $_POST["bis_autocomplete_google_city"] == "" && $_POST["bis_autocomplete_google_state"] == ""
                        && $_POST["bis_autocomplete_google_country"] != "") {
                $req_form_value = $_POST["bis_country_code"];
            } else if(isset ($_POST["bis_autocomplete_google_state"]) && $_POST["bis_autocomplete_google_city"] == "" && $_POST["bis_autocomplete_google_state"] != "" 
                        && $_POST["bis_autocomplete_google_country"] != "") {
                $req_form_value = $_POST["bis_autocomplete_google_state"];
            } elseif ($selected_session_autocomplete_value !== null && $selected_session_autocomplete_value !== "") {
                $req_form_value = $selected_session_autocomplete_value;
            } else {
                // No data for form submitted. Considered as form not submited.
                return $eval;
            }
            RulesEngineCacheWrapper::set_session_attribute(BIS_SEARCH_BOX_VALUE, $req_form_value);
                        
        } elseif (isset($_POST["bis_fea_country_dropdown_select"])) {
            $req_form_value = $_POST["bis_fea_country_dropdown_select"];
            RulesEngineCacheWrapper::set_session_attribute(BIS_COUNTRY_DROPDOWN_VALUE, $_POST["bis_fea_country_dropdown_select"]);
        } elseif (isset($_POST["bis_fea_country_dropdown_select_widget"])) {
            $req_form_value = $_POST["bis_fea_country_dropdown_select_widget"];
            RulesEngineCacheWrapper::set_session_attribute(BIS_COUNTRY_DROPDOWN_VALUE, $_POST["bis_fea_country_dropdown_select_widget"]);
        }elseif ($selected_session_country_value !== null && $selected_session_country_value !== "") {
            $req_form_value = $selected_session_country_value;
            RulesEngineCacheWrapper::set_session_attribute(BIS_COUNTRY_DROPDOWN_VALUE, $selected_session_country_value);
        }

        $eval = RulesEngineUtil::evaluateStringTypeRule($req_form_value, $value_type, $condId);

        return $eval;
    }

    public static function get_dynamic_rule_expression($value_type, $tokenInput, $condId) {

        $json_tokens = json_decode($tokenInput);
        $dynamic_value = "";

        $token_count = count($json_tokens);

        if ((($token_count - 1) > 0) && ($json_tokens != null)) {
            foreach ($json_tokens as $key => $token) {

                if ($key == 0) {
                    $dynamic_value = "( ";
                }

                if ($key < $token_count - 1) {
                    if ($key < $token_count - 1 && $condId == 14) {
                        $dynamic_value = $dynamic_value . $value_type . $token->id . "$" . $condId . " + ";
                    } else {
                        $dynamic_value = $dynamic_value . $value_type . $token->id . "$" . $condId . " - ";
                    }
                } else {
                    $dynamic_value = $dynamic_value . $value_type . $token->id . "$" . $condId . " )";
                }
            }
        } else if ($token_count == 1) {
            $dynamic_value = $value_type . $json_tokens[0]->id . "$" . $condId;
        }

        return $dynamic_value;
    }

    public static function get_dynamic_rule_expression_val_type($value_type, $tokenInput, $condId) {
        return $value_type . $tokenInput . "$" . $condId;
    }

    public static function evaluateIntTypeRule($arg1, $arg2, $condId) {
        $condId = (int) $condId;

        switch ($condId) {

            case 1: // Equals
            case 12: // Contains AnyOf
                if ($arg1 == $arg2) {
                    return true;
                }
                break;
            case 2: // Not Equals
            case 14: // Does not contains AnyOf
                if ($arg1 != $arg2) {
                    return true;
                }
                break;

            case 6: // Greaterthan
                if ($arg1 > $arg2) {
                    return true;
                }
                if ($arg1 == $arg2) {
                    return false;
                }
                break;

            case 7: //Lessthan
                if ($arg1 < $arg2) {
                    return true;
                }
                if($arg1 == $arg2){
                    return false;              
                }
                break;
        }

        return false;
    }

    public static function evaluateDateTypeRule($arg1, $arg2, $condId) {

        $condId = (int) $condId;

        switch ($condId) {
            case 1 :
                return RulesEngineUtil::isDatesEqual($arg1, $arg2);
                break;
            case 2 :
                return !RulesEngineUtil::isDatesEqual($arg1, $arg2);
                break;
            case 6 :
                return RulesEngineUtil::isDateGreater($arg1, $arg2, $condId);
                break;
            case 7 :
                return !RulesEngineUtil::isDateGreater($arg1, $arg2, $condId);
                break;
        }
    }

    public static function isDatesEqual($arg1, $arg2) {

        $date1 = date_create($arg1);
        $date2 = date_create($arg2);

        $isEqual = false;

        if ($date1 == $date2) {
            $isEqual = true;
        }

        return $isEqual;
    }

    public static function isDateGreater($arg1, $arg2, $condId) {

        $date1 = date_create($arg1);
        $date2 = date_create($arg2);
        $isGreater = false;

        if ($date1 > $date2) {
            $isGreater = true;
        }
        if($condId == 7){
            if ($date1 == $date2) {
                $isGreater = true;
            }
        }

        return $isGreater;
    }

    public static function get_applied_session_rules($rules_engine=null) {
        
        if($rules_engine == null) {
            $rules_engine = new RulesEngine();
        }
       
        $applied_rules = $rules_engine->get_applied_logical_rules();

        if ($applied_rules == null) {
            $rules_engine->bis_start_session();
            $applied_rules = $rules_engine->get_applied_logical_rules();
        }

        return $applied_rules;
    }

    public static function getSessionValue($key) {
        $value = null;

        if (RulesEngineCacheWrapper::is_session_attribute_set($key)) {
            $value = RulesEngineCacheWrapper::get_session_attribute($key);
        }

        return $value;
    }

    public static function evaluate_browser_rule($rule_value, $condId) {

        $client_browser = RulesEngineUtil::get_client_browser();

        return RulesEngineUtil::evaluateStringTypeRule($rule_value, $client_browser, $condId);
    }
    
    public static function evaluate_os_rule($rule_value, $condId) {
        $browser = browser_detection("full_assoc");
        $client_os = $browser['os'];
        return RulesEngineUtil::evaluateStringTypeRule($rule_value, $client_os, $condId);
    }

    public static function get_client_browser() {
        
        $browser = browser_detection("full_assoc");
        $browser_name = "none";

        if ($browser["browser_working"] == "ie") {
            $browser_name = "ie";
        } else if ($browser["browser_working"] == "moz" && $browser["moz_data"][0] == "firefox") {
            $browser_name = "firefox";
        } else if ($browser["browser_working"] == "blink") {
            $browser_name = "opera";
        } else if ($browser["browser_working"] == "webkit") {
            if ($browser["webkit_data"][0] == "safari") {
                $browser_name = "safari";
            } else if ($browser["webkit_data"][0] == "chrome") {
                $browser_name = "chrome";
            }
        }

        return $browser_name;
    }

    public static function get_applied_rule_ids($logical_rules) {

        $logical_rule_id_array = array();
        if ($logical_rules != null && !empty($logical_rules)) {
            foreach ($logical_rules as $logical_rule) {

                // For request rules
                //If the rules contains expression then expression must be true
                if ($logical_rule->expression != null) {
                    if (isset($logical_rule->eval) && ($logical_rule->eval)) {
                        array_push($logical_rule_id_array, $logical_rule->ruleId);
                    }
                } else {
                    // For session rules
                    array_push($logical_rule_id_array, $logical_rule->ruleId);
                }
            }
        }

        return implode(",", $logical_rule_id_array);
    }

    public static function get_applied_rules($logical_rules) {

        $logical_rule_id_array = array();
        if ($logical_rules != null && !empty($logical_rules)) {
            foreach ($logical_rules as $logical_rule) {
                // For request rules
                //If the rules contains expression then expression must be true
                if (isset($logical_rule->expression)) {
                    if (isset($logical_rule->eval) && ($logical_rule->eval === true) &&
                            (!in_array($logical_rule->ruleId, $logical_rule_id_array))) {
                        array_push($logical_rule_id_array, $logical_rule->ruleId);
                    }
                } else {
                    // For session rules
                    if((isset($logical_rule->ruleId)) && 
                        (!in_array($logical_rule->ruleId, $logical_rule_id_array))) {
                        array_push($logical_rule_id_array, $logical_rule->ruleId);
                    }
                }
            }
        }
        
        return $logical_rule_id_array;
    }

    public static function get_applied_page_rule_ids($page_rules) {

        $page_rule_id_array = array();

        if ($page_rules != null) {
            foreach ($page_rules as $page_rule) {
                array_push($page_rule_id_array, $page_rule->parent_id);
            }
        }

        return implode(",", $page_rule_id_array);
    }

    public static function get_applied_post_rule_ids($post_rules) {

        $post_rule_id_array = array();

        if ($post_rules != null && !empty($post_rules)) {
            $post_rule_id_array = $post_rules;
        }       
        return $post_rule_id_array;
    }
    
    public static function cache_child_rule(RulesVO $rules_vo, $child_rule_path, $plugin_id) {
        RulesEngineCacheWrapper::set_session_attribute(BIS_SESSION_RULEVO, $rules_vo);
        RulesEngineCacheWrapper::set_session_attribute(BIS_CHILD_FILE_PATH, $child_rule_path);
        RulesEngineCacheWrapper::set_session_attribute(BIS_CHILD_RULE_ID, $plugin_id);
    }

    public static function clear_cached_child_rule() {
        RulesEngineCacheWrapper::remove_session_attribute(BIS_CHILD_FILE_PATH);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_SESSION_RULEVO);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_CHILD_RULE_ID);
    }

    public static function generate_json_response($results_map, $action=null) {

        $status = $results_map[BIS_STATUS];

        if ($status == BIS_SUCCESS) {

            if (isset($results_map[BIS_DATA]) && !empty($results_map[BIS_DATA])) {
                if(isset($results_map[BIS_ROW_COUNT])){
                    $data = array("status" => BIS_SUCCESS, "data" => $results_map[BIS_DATA],
                    "total_records" => $results_map[BIS_ROW_COUNT]);
                } else {
                    $data = array("status" => BIS_SUCCESS, "data" => $results_map[BIS_DATA]);
                }
            } else {
                $data = array("status" => BIS_SUCCESS);
            }
        } else if ($status == BIS_SUCCESS_WITH_NO_DATA) {

            $data = array("status" => BIS_SUCCESS_WITH_NO_DATA);
        } else {
            if (isset($results_map[BIS_DATA]) && !empty($results_map[BIS_DATA])) {
                $data = array("status" => BIS_ERROR, "data" => $results_map[BIS_DATA]);
            } else {
                $data = array("status" => BIS_ERROR);
            }
        }

        if (isset($results_map[BIS_MESSAGE_KEY])) {
            $data[BIS_MESSAGE_KEY] = $results_map[BIS_MESSAGE_KEY];
        }
        
        if (isset($action)) {
            $data[BIS_ACTION] = $action;
        }

        wp_send_json($data);
    }

    /**
     * Convert the json string value to array of values.
     *
     * @param unknown $json_value
     * @return multitype:
     */
    public static function get_values_array($json_value) {

        $rule_values = json_decode(stripslashes($json_value));

        $values = array();

        if (($rule_values != null) && (!empty($rule_values))) {
            foreach ($rule_values as $rule_value) {
                array_push($values, $rule_value->id);
            }
        }

        return $values;
    }

    public static function get_exclude_page_id($exclude_pages, $applied_rule_id) {
        $exclude_page_id = "";

        if (($exclude_pages != null) && !empty(($exclude_pages))) {
            foreach ($exclude_pages as $exclude_page) {
                if ($exclude_page->lrId == $applied_rule_id && $exclude_page->action == "hide_page") {
                    $exclude_page_id = $exclude_page->parent_id . "," . $exclude_page_id;
                }
            }
        }

        return $exclude_page_id;
    }

    public static function set_request_rules($eval_request_rules) {
        RulesEngineCacheWrapper::set_session_attribute(BIS_REQUEST_RULES . session_id(), $eval_request_rules);
    }

    /**
     * This method return the style name for the theme.
     * @param $template_name
     * @return string
     */
    public static function get_theme_style($template) {
        $stylesheet = "";
        $themes = wp_get_themes();

        foreach ($themes as $theme) {
            if ($theme->template == $template) {
                $stylesheet = $theme->stylesheet;
            }
        }

        return $stylesheet;
    }

    /**
     * This method is used to build current request url with protocol.
     *
     * @return string
     */
    public static function get_current_url($url = null) {
   
        $url_array = null;
        
        if($url == null && !RulesEngineUtil::is_ajax_request()) {
            $url = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
        }
        
        // If the url is an ajax then return no need to verify
        if($url != null) {            
            $url_array = array();
            if (is_ssl()) {
                array_push($url_array, "https://" . $url);
                array_push($url_array, "https://www." . $url);  
                
                if (!RulesEngineUtil::endsWith($url, "/")) {
                    array_push($url_array, "https://" . $url . "/");
                    array_push($url_array, "https://www." . $url . "/");
                }
            } else {
                array_push($url_array, "http://" . $url);
                array_push($url_array, "http://www." . $url);
                
                if(!RulesEngineUtil::endsWith($url, "/")) {
                    array_push($url_array, "http://" . $url."/");
                    array_push($url_array, "http://www." . $url."/");
                }
            }
        }
        return $url_array;
    }

    /**
     * This method will display the request forgery error message.
     */
    public static function handle_request_forgery_error() {
        die("<h1><span style='color: red;'>Oops! Request forgery error, please reload page.</span></h1>");
    }

    public static function isNullOrEmptyString($value) {
        return (!isset($value) || trim($value) === '');
    }

    public function convert_to_text($text) {
        if ($text == 1)
            return 'true';
        else
            return 'false';
    }

    public static function get_json_ids($json_values) {
        $values = json_decode($json_values);
        $value_ids = array();

        if ($values != null) {
            foreach ($values as $value) {
                array_push($value_ids, array("id" => intval($value->id)));
            }
        }

        return json_encode($value_ids);
    }

    public static function is_valid_shortcode($shortcode) {
        $content = trim($shortcode);
        $first_char = substr($content, 0, 1);
        $last_char = substr($content, strlen($content) - 1, 1);

        if (!($first_char === "[" && $last_char === "]")) {
            $results_map = array();
            $results_map[BIS_MESSAGE_KEY] = BIS_INVALID_SHORTCODE;
            $results_map[BIS_STATUS] = BIS_ERROR;
            RulesEngineUtil::generate_json_response($results_map);
        }
        return true;
    }

    public static function is_woocommerce_installed() {

        if (class_exists('WooCommerce')) {
            return true;
        }

        return false;
    }
    
    public static function is_redirect_plugin_installed() {

        if (class_exists('RedirectRulesEngine')) {
            return true;
        }

        return false;
    }

    /**
     * Get the mobile device manufacture details, if not mobile return false.
     * If not in list returns other.
     * 
     * @return string|boolean
     */
    public static function get_mobile($uagent_info) {


        if ($uagent_info->DetectMobileQuick() ||
                $uagent_info->DetectSmartphone()) {

            //"iPhone"
            $eVal = $uagent_info->DetectIphone();
            if ($eVal) {
                return "iPhone";
            }

            // windows Phone
            $eVal = $uagent_info->DetectWindowsPhone();
            if ($eVal) {
                return "winPhone";
            }

            //"Andriod_Phone"
            $eVal = $uagent_info->DetectAndroidPhone();
            if ($eVal) {
                return "andriodPhone";
            }

            //"BlackBerry_Phone"
            $eVal = $uagent_info->DetectBlackBerry10Phone();
            if ($eVal) {
                return "blackBerry";
            }

            return "other";
        } // End of if

        return null;
    }

    /**
     * This method is used to get the table details from the list.
     * If not found in the returns other.
     * If not table return false.
     * 
     * @param type $uagent_info
     * @return string|boolean
     */
    public static function get_tablet($uagent_info) {

        // iPad
        $eVal = $uagent_info->DetectIpad();
        if ($eVal) {
            return "iPad";
        }

        //"Andriod_Tablet"
        $eVal = $uagent_info->DetectAndroidTablet();
        if ($eVal) {
            return "andriodTablet";
        }

        $eVal = $uagent_info->DetectBlackBerryTablet();
        if ($eVal) {
            return "blackBerryTablet";
        }

        //"HP_TouchPad":
        $eVal = $uagent_info->DetectWebOSTablet();
        if ($eVal) {
            return "hpTouchPad";
        }

        //"Tablet":
        $eVal = $uagent_info->DetectTierTablet();
        if ($eVal) {
            return "other";
        }

        return null;
    }
    
    /**
     * This method is used to if no redirect param is available or not.
     * 
     * @return boolean
     */
    public static function is_redirect() {
        $is_no_trd = 0;
        $is_no_redirect = false;
        
        if (isset($_GET[BIS_NO_REDIRECT])) {
            $is_no_trd = $_GET[BIS_NO_REDIRECT];

            if ($is_no_trd === 1 || $is_no_trd === '1') {
                // set to session check from session
                RulesEngineCacheWrapper::set_session_attribute(BIS_NO_REDIRECT, true);
            } else if ($is_no_trd === 0 || $is_no_trd === '0') { // explict redirect
                // set to session check from session
                RulesEngineCacheWrapper::remove_session_attribute(BIS_NO_REDIRECT);
                return true;
            }
        }
        
        if(RulesEngineCacheWrapper::is_session_attribute_set(BIS_NO_REDIRECT)) {
            if(RulesEngineCacheWrapper::get_session_attribute(BIS_NO_REDIRECT) === true
                    || RulesEngineCacheWrapper::get_session_attribute(BIS_NO_REDIRECT) === 'true') {
                return false;
            }
        }

        return true;
    }
    
    public static function get_audit_report_data_id() {
        $auditVO = RulesEngineCacheWrapper::get_session_attribute(BIS_AUDIT_INFO);
        
        if($auditVO == null) {
            return null;
        }
        return $auditVO->getId();
    }
    
    public static function get_purchase_code($product_id) {
        $bis_re_pur_key = BIS_PUR_CODE. $product_id;
        $bis_re_prd_vrf = RulesEngineUtil::get_option($bis_re_pur_key);

        if ($bis_re_prd_vrf == false) {
            return ""; 
        }
        
        return $bis_re_prd_vrf;
    } 
    
    public static function add_option($option, $value, $net_opt = false, $option_site = true) {      
        if (is_multisite() && $option_site) {
            add_site_option($option, $value);
        } else {
            add_option($option, $value);
        }
    }
    
    public static function get_option($option, $net_opt = false, $option_site = true) {       
        if (is_multisite() && $option_site) { // Sets in network options if multisite
           return get_site_option($option);
        } else {
           return get_option($option);
        }       
    }
    
    public static function update_rule_cache_id($cacheId) {    
        RulesEngineUtil::update_option($cacheId, mt_rand());
    }
    
    public static function add_rule_cache_id($cacheId) {
        RulesEngineUtil::add_option($cacheId, mt_rand());
    }
    
    public static function get_rule_cache_id($cacheId) {    
        return RulesEngineUtil::get_option($cacheId);
    }
    
    public static function is_rules_updated() {
        $rule_updated = false;
        $ses_ran_val = RulesEngineCacheWrapper::get_session_attribute(BIS_LOGICAL_CACHE_REFRESH_ID);
        $opt_rand_val = RulesEngineUtil::get_rule_cache_id(BIS_LOGICAL_CACHE_REFRESH_ID);
        
        if ($ses_ran_val != $opt_rand_val) {
            $rule_updated = true;
        } 
        return $rule_updated;
    }  
    
    public static function update_option($option, $value, $net_opt = false, $option_site = true) {
        if (is_multisite() && $option_site) {
            update_site_option($option, $value);
        } else {
            update_option($option, $value);
        }
    }

    public static function delete_option($option, $net_opt = false, $option_site = true) {
        if (is_multisite() && $option_site) {
            delete_site_option($option);
        } else {
            delete_option($option);
        }
    }

    public static function is_bis_re_plugin_page() {

        $is_plugin_page = false; 
       
        
        // Logic to check for dashboard
        /*if(RulesEngineUtil::is_wp_dashboard()) {
            return true;
        }*/

        if(isset($_GET["page"])) { 
            $bis_cpage = $_GET["page"];
            $bis_pages = array("bis_pg_dashboard", "advanced_bulk_edit", "bis_pg_rulesengine", "pagerules",
                "postrules", "categoryrules", "widgetrules", "themerules", "popuprules",
            "redirectrules", "languagerules", "bis_pg_global_settings", "bis_pg_analytics", "bis_pg_",
                );
            $is_plugin_page = in_array($bis_cpage, $bis_pages);

            if(!$is_plugin_page) {
                $is_plugin_page = RulesEngineUtil::isContains($bis_cpage, "bis_pg_");
            }
        }
        
        return $is_plugin_page;
    }

    public static function is_wp_dashboard() {
        
        if(!isset($_GET["page"])) {
            $url = $_SERVER['REQUEST_URI'];
            if (substr($url, -9) == "/wp-admin" || substr($url, -10) == "/wp-admin/" 
                    || substr($url, -19) == "/wp-admin/index.php") {
                return true;
            }
        }
        
        return false;
    }

    public static function getCommaSeperatedCategories($categories) {
        $catStr = "";
        
        if(!empty($categories)) {
            $catArray = array();

            foreach ($categories as $cat) {
                array_push($catArray, $cat->term_id);
            }
            if(!empty($catArray)) {
                $catStr = implode(',', $catArray);
            }
        }
        return $catStr;
    }
    
    public static function evaluateCategoryArrayTypeRule($array, $value, $condId, $isWooCat = false, $isAjaxRequest) {
        
        $eval = false;

        if(!empty($array)) {
            $eval = false;
            if (!$isWooCat) {
                if($isAjaxRequest) {
                    foreach ($array as $cat) {
                        if ($cat == $value) {
                            $eval = true;
                            break;
                        }
                    }
                } else {
                    foreach ($array as $cat) {
                        if (isset($cat->term_id) && $cat->term_id == $value) {
                            $eval = true;
                            break;
                        }
                    }
                }
            } else {
                $eval = in_array($value, $array);
            }
        }
        switch ($condId) {
            case 1:
            case 12:
                //Do nothing.
                //$eval = $eval;
                break;

            case 2:
            case 14:
                $eval = !$eval;
                break;
        }

        return $eval;
    }
    
    public static function is_reset_rule() {
        if (isset($_POST[BIS_RESET_RULE_PARAM]) ||
                isset($_GET[BIS_RESET_RULE_PARAM])) {
            if ($_POST[BIS_RESET_RULE_PARAM] == true || $_GET[BIS_RESET_RULE_PARAM] == true) {
                return true;
            }
        }
    }
    
    public static function replace_geo_placeholders($content, $geoLocVO) {
        
        if ($content !== "" && false !== strpos($content, '[')) {
            $content = str_replace('[bis_country_name]', $geoLocVO->getCountryName(), $content);
            $content = str_replace('[bis_city_name]', $geoLocVO->getCity(), $content);
            $content = str_replace('[bis_region_name]', $geoLocVO->getRegion(), $content);
        }
        
        return $content;
    }
    
    public static function get_merged_arrays($array1, $array2) {
        if (!empty($array1)) {
            if (empty($array2)) {
                $array2 = array();
            }
            $array2 = array_merge($array2, $array1);
        }

        return $array2;
    }
    
    public static function is_ajax_request() {
        if (((!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && 
                strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')) || 
                (!empty($_SERVER['X-Requested-With']) && 
                strtolower($_SERVER['X-Requested-With']) == 'xmlhttprequest' )) {
            
            return true;
        }
        return false;
    }
    
    /**
     * This method is used to return the file uplad directory.
     * 
     * @return string
     */
    public static function get_file_upload_path() {
        $upload_dirname = trailingslashit(WP_CONTENT_DIR) . 'uploads/' . BIS_UPLOAD_DIRECTORY . '/';
        return $upload_dirname;
    }
    
    /**
     * This method is used to return the file uplad directory.
     * 
     * @return string
     */
    public static function get_templates_file_path() {
        $template_dirname = BIS_RULES_ENGINE_PLATFORM_PATH . '/template/popup-templates';
        return $template_dirname;
    }

    /**
     * This method is used to delete directory and files recurrsively.
     * 
     * @param type $dir
     * @return boolean
     */
    public static function delete_directory($dir) {
        if (!file_exists($dir)) {
            return true;
        }
        $files = glob($dir."/*"); // get all file names
        
        foreach ($files as $file) { // iterate files           
            if (is_dir($file)) {
                self::delete_directory($file);
            } else {
                unlink($file);
            }   
        }

        return rmdir($dir);
    }
    
    /**
     * This method is used to getting start page index in pagination.
     * 
     * @return int
     */
    public static function get_start_page_index() {
        $page_start_index = 0;

        if (isset($_POST['searchData']) && isset($_POST['pg_st_index'])) {
            $page_start_index = $_POST["pg_st_index"];
        } else if (isset($_GET['pg_st_index'])) {
            $page_start_index = $_GET["pg_st_index"];
        }

        if ($page_start_index > 1) {
            $page_start_index = (($page_start_index - 1) * BIS_PAGE_SIZE);
        } else {
            $page_start_index = 0;
        }

        return $page_start_index;
    }

    public static function set_search_request() {
        RulesEngineCacheWrapper::set_session_attribute(BIS_SEARCH_REQUEST, TRUE);
    }
    
    public static function remove_search_request() {
        RulesEngineCacheWrapper::remove_session_attribute(BIS_SEARCH_REQUEST);
    }
	
	 /**
     * 
     * 
     * @param type $rule_value
     * @param type $condId
     * @return type
     */
    public static function evaluateLanguageRule($rule_value, $condId) {

        $isBrowserLang = false;
        $langs = RulesEngineUtil::getBrowserLanguages();
        // look through sorted list and use first one that matches our languages
        foreach ($langs as $lang => $val) {
            if (strpos($rule_value, $lang) === 0) {
                $isBrowserLang = true;
                break;
            } elseif (strpos($lang, '-') !== false) {
                $lang = str_replace('-', '_', $lang);
                if ($lang == $rule_value) {
                    $isBrowserLang = true;
                    break;
                }
            }
        }

        if ($condId != 1) {
            $isBrowserLang = !$isBrowserLang;
        }

        return $isBrowserLang;
    }

    /**
     * This method is used to get the browser language list.
     * 
     */
    public static function getBrowserLanguages() {
        $langs = array();

        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            // break up string into pieces (languages and q factors)
            preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);

            if (count($lang_parse[1])) {
                // create a list like "en" => 0.8
                $langs = array_combine($lang_parse[1], $lang_parse[4]);

                // set default to 1 for any without q factor
                foreach ($langs as $lang => $val) {
                    if ($val === '')
                        $langs[$lang] = 1;
                }

                // sort list based on value	
                arsort($langs, SORT_NUMERIC);
            }
        }

        return $langs;
    }
    
    /**
     * Define constant only if not already set.
     *
     * @param  string $name
     * @param  string|bool $value
     */
    public static function define($name, $value) {
        if (!defined($name)) {
            define($name, $value);
        }
    }

    /**
     * What type of request is this?
     *
     * @param  string $type admin, ajax, cron or frontend.
     * @return bool
     */
    public static function is_request($type) {
        switch ($type) {
            case 'admin' :
                return is_admin();
            case 'ajax' :
                return defined('DOING_AJAX');
            case 'cron' :
                return defined('DOING_CRON');
            case 'frontend' :
                return (!is_admin() || defined('DOING_AJAX') ) && !defined('DOING_CRON');
        }
    }
    
    public static function get_current_page_url() {
        global $wp;
        if(isset($wp)) {
            return home_url(add_query_arg(array(), $wp->request));
        } else {
            return false;
        }
    }
    
    public static function evaluateArrayTypeRule($present_post_tag_ids, $rule_appalied_tag, $condId){
        
        $eval = false;
          
        if(!empty($present_post_tag_ids)){
            if (in_array($rule_appalied_tag, $present_post_tag_ids)) {
                $eval = true;
            }
        }
        switch ($condId) {
            case 1:
            case 12:
                $eval = $eval;
                break;
            case 2:
            case 14:
                $eval = !$eval;
                break;
        }
        return $eval;
    }
    
    /**
     * This method is used to check whether the request is from mega cache request.
     * @return boolean
     */
    public static function is_mega_rules_ajax_request() {
        $is_mega_rules_ajax = FALSE;
        
        if (isset($_GET['bis_re_action']) &&
                ($_GET['bis_re_action'] === 'bis_re_mega_rules')) {
            $is_mega_rules_ajax = TRUE;
        }
        
        return $is_mega_rules_ajax;
    }
    
    public static function get_auto_redirect_times() {
        $times = Array();

        $value1 = new LabelValueVO(0, "Never");
        array_push($times, $value1);

        $value2 = new LabelValueVO(2000, "2");
        array_push($times, $value2);

        $value3 = new LabelValueVO(4000, "4");
        array_push($times, $value3);

        $value4 = new LabelValueVO(5000, "5");
        array_push($times, $value4);

        $value5 = new LabelValueVO(10000, "10");
        array_push($times, $value5);

        $value6 = new LabelValueVO(20000, "20");
        array_push($times, $value6);

        $value7 = new LabelValueVO(30000, "30");
        array_push($times, $value7);

        $value8 = new LabelValueVO(60000, "60");
        array_push($times, $value8);

        return $times;
    }
    
    public static function bis_get_curl_data($api_key){

        $data = array('fields' => 'lists');
        $url = 'https://' . substr($api_key, strpos($api_key, '-') + 1) . '.api.mailchimp.com/3.0/lists/';

        $curl_data_json = RulesEngineUtil::bis_rudr_mailchimp_curl_connect($url, 'GET', $api_key, $data);
        $curl_data = json_decode($curl_data_json);
        if (!empty($curl_data->lists)) {
            $status = BIS_SUCCESS;
        } else {
            $status = BIS_ERROR;
        }
        $results_map = array();
        $results_map[BIS_DATA] = $curl_data;
        $results_map[BIS_STATUS] = $status;
        return $results_map;
    }

    public static function bis_rudr_mailchimp_curl_connect($url, $request_type, $api_key, $data = array()){
        
        if ($request_type == 'GET')
            $url .= '?' . http_build_query($data);

        $mch = curl_init();
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Basic ' . base64_encode('user:' . $api_key)
        );
        curl_setopt($mch, CURLOPT_URL, $url);
        curl_setopt($mch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($mch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($mch, CURLOPT_CUSTOMREQUEST, $request_type);
        curl_setopt($mch, CURLOPT_TIMEOUT, 10);
        curl_setopt($mch, CURLOPT_SSL_VERIFYPEER, false);

        if ($request_type != 'GET') {
            curl_setopt($mch, CURLOPT_POST, true);
            curl_setopt($mch, CURLOPT_POSTFIELDS, json_encode($data));
        }

        return curl_exec($mch);
    }
    
    public static function bis_get_continents() {
        if (class_exists('WooCommerce')) {
            $continents = RulesEngineCacheWrapper::get_session_attribute(BIS_ALL_CONTINENTS);
            if ($continents === null && empty($continents)) {
                $continents = apply_filters('woocommerce_continents', include WC()->plugin_path() . '/i18n/continents.php');
                RulesEngineCacheWrapper::set_session_attribute(BIS_ALL_CONTINENTS, $continents);
            }
            return $continents;
        } else {
            return null;
        }
    }

    public static function bis_get_continent_code_for_country($cc) {
        $cc = trim(strtoupper($cc));
        $continents = RulesEngineUtil::bis_get_continents();
        if($continents !== null){
            $continents_and_ccs = wp_list_pluck($continents, 'countries');
            foreach ($continents_and_ccs as $continent_code => $countries) {
                if (false !== array_search($cc, $countries, true)) {
                    return $continent_code;
                }
            }
        }
        return '';
    }

}