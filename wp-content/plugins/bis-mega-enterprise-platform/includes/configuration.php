<?php
$bis_plugin_force_delete = RulesEngineUtil::get_option(BIS_RULES_ENGINE_PLUGIN_FORCE_DELETE);

$folder = RulesEngineUtil::get_file_upload_path();
$city_file = file_exists($folder . "GeoLite2-City.mmdb");
$country_file = file_exists($folder . "GeoLite2-Country.mmdb");

$db_download_text = "Download DB";
if ($country_file == "1" && $city_file == "1") {
    $db_download_text = "Update DB";
}

$plugin_force_delete_checked = '';

if ($bis_plugin_force_delete === "true") {
    $plugin_force_delete_checked = "checked";
}
?>

<div class="container-fluid">
    <div class="row">
        <div>
            <div class="panel panel-default">
                <div class="panel-heading  bis-panel-geo-configuration">
                    <h3 class="panel-title ">
                        <label id="bis_re_basic_add_rule"><?php _e("Geolocation Configuration", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="geo-location col-sm-6 pl-0">
                        <div class="di-block">
                            <div class="form-group">
                                <label>
                                    <?php
                                    $bis_geo_plugin_checked = "";
                                    $bis_geo_max_checked = "";
                                    $bis_geo_db_text_style = "display:none";
                                    $bis_ip2_location_checked = "";
                                    $bis_ip2_location_csv_checked = "";
                                    $bis_ip2_location_db_text_style = "display:none";

                                    if ($bis_geo_lookup_webservice_type == 1) {
                                        $bis_geo_plugin_checked = "checked=\"checked\"";
                                    }
                                    if ($bis_geo_lookup_type == 1) {
                                        $bis_geo_max_checked = "checked=\"checked\"";
                                        $bis_geo_db_text_style = "display:block";
                                    }
                                    if ($bis_ip2location_lookup_type == 1) {
                                        $bis_ip2_location_db_text_style = "display:block";
                                        $bis_ip2_location_checked = "checked=\"checked\"";
                                    }
                                    if ($bis_ip2location_csv_lookup_type == 1) {
                                        $bis_ip2_location_db_text_style = "display:block";
                                        $bis_ip2_location_csv_checked = "checked=\"checked\"";
                                    }
                                    if ($bis_ip2_location_db_file !== null) {
                                        $bis_ip2_location_db_file = $bis_ip2_location_db_file;
                                    } else {
                                        $bis_ip2_location_db_file = "";
                                    }
                                    if ($bis_ip2_location_csv_db_file !== null) {
                                         $bis_ip2_location_csv_db_file = $bis_ip2_location_csv_db_file;
                                    } else {
                                        $bis_ip2_location_csv_db_file = "";
                                    }
                                    ?>
                                    <input <?php echo $bis_geo_plugin_checked; ?> type="checkbox" name="bis_geolocation_ws" id="bis_geo_geoloc" value="1">
                                    <?php _e("Use Geoplugin webservice", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>                      
                                </label>
                            </div>
                        </div>
                        <div>
                            <div class="form-group di-block">
                                <label>
                                    <input type="radio" <?php echo $bis_geo_max_checked; ?> name="bis_ip2location_db" id="bis_geo_maxmind" value="1"> 
                                    <?php _e("Use MaxMind database", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                                </label>
                                <select class="localdb-select d-block bis-latest-db" id="bis_city_country_dropdown" style="display: none;">

                                </select>
                            </div>
                            <div style="padding-bottom: 20px">
                                <button id="bis_download_city_contry_db" class="btn btn-primary"><i class="glyphicon glyphicon-save bis-glyphi-position"></i><?php echo $db_download_text; ?></button>
                                <button class="btn btn-primary"  id="bis_downloading_city_contry_db" style="display: none"><i class="fa fa-spinner fa-spin"></i><?php _e("Downloading latest DB please wait...", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></button>
                            </div>
                            <div class="alert alert-info bis-alert-information" style="margin-bottom: 10px">
                                <?php _e("This product includes GeoLite2 data created by MaxMind, available from", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                                <a href="http://www.maxmind.com">http://www.maxmind.com</a>.
                            </div>
                        </div>
                        <!--                        <div class="form-group di-block ">
                                                    <div class="di-block  bis-geo-location-or" >
                                                        OR
                                                    </div>
                                                </div>-->
                        <div>
                            <div class="form-group di-block ">
                                <label class="mb-10">
                                    <input type="radio" <?php echo $bis_ip2_location_checked; ?> name="bis_ip2location_db" id="bis_ip2_location" value="1"> 
                                    <?php _e("Use Ip2location database", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                                </label>
                                <input <?php echo $bis_ip2_location_db_text_style; ?> value="<?php echo $bis_ip2_location_db_file ?>" class="localdb-select d-block bis-latest-db"  size="45px" type="text" id="bis_ip2_location_db_file" style="border-radius: 5px;"
                                                                                      name="bis_ip2_loction_db_file" placeholder='<?php _e("Enter Ip2location database file name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'>
                            </div>
                            <!--                            <div>
                                                        <div class="form-group di-block ">
                                                            <div class="di-block  bis-geo-location-or" >
                                                                OR
                                                            </div>
                                                        </div>
                                                        </div>-->
                            <div class="form-group di-block ">
                                <label class="mb-10">
                                    <input type="radio" <?php echo $bis_ip2_location_csv_checked; ?> name="bis_ip2location_db" id="bis_ip2_location_csv" value="1"> 
                                    <?php _e("Create IP2Location Local DB", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                                </label>
                                <input <?php echo $bis_ip2_location_db_text_style; ?> value="<?php echo $bis_ip2_location_csv_db_file  ?>" class="localdb-select d-block bis-latest-db"  size="45px" type="text" id="bis_ip2_location_db_csv_file" style="border-radius: 5px;"
                                                                                      name="bis_ip2_location_db_csv_file" placeholder='<?php _e("Enter Ip2location database csv file name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'>
                                <div class="alert alert-info bis-alert-information" id="tip_csv_db" style="background-color: yellow;color: black;margin-top: 5px;">
                                    <?php _e("This installation may take more then 5 minutes please wait after clicking Install DB button.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                                </div>
                            </div>
                           
                          
                            <div style="padding-bottom: 20px">
                                <button id="bis_import_ip2location_db" class="btn btn-primary"><i class="glyphicon glyphicon-save bis-glyphi-position"></i><?php _e("Install DB", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></button>
                                <button class="btn btn-primary"  id="bis_installing_ip2_location_db" style="display: none"><i class="fa fa-spinner fa-spin"></i><?php _e(" Installing latest DB please wait...", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></button>
                            </div>
                        </div>
                        <div class="alert alert-info bis-alert-information">
                            <?php _e("This site or product includes IP2Location LITE data, available from", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                            <a href="http://lite.ip2location.com">http://lite.ip2location.com</a>.
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label for="description"><?php _e("Geonames User Name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                        <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                        <strong>
                            <input value="<?php echo $bis_geoname_user ?>" class="form-control" type="text" id="geonameuser" 
                                   name="geonameuser" placeholder='<?php _e("Enter user name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'>
                        </strong>
                        <div class="alert alert-info bis-alert-information mt-10 col-sm-3 mb-10">
                            <strong><?php _e("Credits", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></strong>  <a target="_blank" href="http://www.geonames.org/">geonames.org</a>
                        </div>
                        <div class="allowed-tags clearfix">
                            <label><?php _e("Allowed tags", BIS_RULES_ENGINE_TEXT_DOMAIN); ?><span class="ml-10 notes">(<?php _e("Click this to highlight the multiple select element", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>)</span></label>
                            <select id="bis_allowed_tags" multiple="multiple">
                                <option><?php _e("a", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("audio", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("b", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("blockquote", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("br", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("h1", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("h2", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("h3", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("h4", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("h5", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("h6", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("hr", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("i", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("img", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("li", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("p", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("span", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("strong", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("ul", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option><?php _e("video", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                            </select>
                        </div>
                        <div class="form-group mt-20">
                            <input <?php echo $checked ?> type="checkbox" name="bis_re_delete_db" id="bis_re_delete_db"/>
                            <label style ="margin-left: 20px; margin-top: -20px;"><?php _e("Delete plugin-in tables on plugin uninstall or delete. Uncheck checkbox before plugin-in upgrade.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                        </div>
                        <div class="form-group mb-0">
                            <input <?php echo $plugin_force_delete_checked ?> type="checkbox" name="bis_re_del_plugin" id="bis_re_del_plugin"/>
                            <label><?php _e("Force delete Mega Platform plug-in.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                            <a class="popoverData" target="_blank" href="<?php echo BIS_RULES_ENGINE ?>"
                               data-content='<?php _e("Plug-in will be deleted without checking any dependent child plug-ins. Use only in rare cases.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>' rel="popover"
                               data-placement="top" data-trigger="hover">
                                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-10 pull-left">
        <button id="bis_re_save_settings" class="btn btn-primary">
            <i class="glyphicon glyphicon-ok-sign bis-glyphi-position"></i><?php _e("Save Settings", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
        </button>
    </div>
</div>
<script>
    jQuery(document).ready(function () {
       jQuery('#bis_import_ip2location_db,#tip_csv_db').hide();
      if( jQuery('#bis_ip2_location_csv').is(':checked')){
         jQuery('#bis_import_ip2location_db,#tip_csv_db').show(); 
      }
        bis_set_allowable_tags();
        var cityFile = '<?php echo $city_file ?>';
        var contryFile = '<?php echo $country_file ?>';
       /*var ip2LocationFile = '<?php echo $bis_ip2_location_db_file ?>';
        if (ip2LocationFile !== "") {
            jQuery('#bis_ip2_location_db_file').val('<?php echo $bis_ip2_location_db_file ?>');
        } else {
            jQuery('#bis_ip2_location_db_file').val('');
        }
          var ip2LocationcsvFile = '<?php echo $bis_ip2_location_csv_db_file ?>';
        if (ip2LocationcsvFile !== "") {
            jQuery('#bis_ip2_location_db_csv_file').val('<?php echo $bis_ip2_location_csv_db_file ?>');
        } else {
            jQuery('#bis_ip2_location_db_csv_file').val('');
        }
        
        jQuery("#bis_geo_maxmind").change(function () {
            
                if (jQuery("#bis_geo_maxmind").is(':checked')) {
                    if(cityFile === '1' || contryFile === '1') {
                    jQuery("#bis_ip2_location").prop("checked", false);
                   
                    jQuery("#bis_geo_maxmind").prop("checked", true);
                } else {
                    jQuery("#bis_ip2_location").prop("checked", true);
                }
            } else {
                jQuery("#bis_geo_maxmind").prop("checked", false);
                bis_showErrorMessage('<?php _e("Please Download db and then select this option", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
            }
            
        });
        jQuery("#bis_ip2_location").change(function () {
            if (jQuery("#bis_ip2_location").is(':checked')) {
                jQuery("#bis_geo_maxmind").prop("checked", false);
              
            } else {
                if(cityFile === '1' || contryFile === '1'){
                    jQuery("#bis_geo_maxmind").prop("checked", true);
                }
            }
        });*/

        if (cityFile === '1' || contryFile === '1') {
            jQuery('#bis_downloading_city_contry_db').hide();
            jQuery("#bis_city_country_dropdown").show();
            //jQuery('#bis_download_city_contry_db').hide();
            if (contryFile === '1') {
                jQuery('#bis_city_country_dropdown').append(jQuery('<option>', {value: "GeoLite2-Country.mmdb"}).text("GeoLite2-Country.mmdb"));
            }
            if (cityFile === '1') {
                jQuery('#bis_city_country_dropdown').append(jQuery('<option>', {value: "GeoLite2-City.mmdb"}).text("GeoLite2-City.mmdb"));
            }
            jQuery('#bis_city_country_dropdown').find('option[value="<?php echo $bis_geo_maxmind_db_file ?>"]').prop('selected', true);
            jQuery("#bis_city_country_dropdown").select2({

            });
        } else {
            jQuery('#bis_downloading_city_contry_db').hide();
            jQuery("#bis_city_country_dropdown").hide();
            jQuery('#bis_download_city_contry_db').show();
            jQuery("#bis_geo_maxmind").prop("checked", false);
        }
        function bis_set_allowable_tags() {
            var jqXHR = jQuery.get(
                    BISAjax.ajaxurl,
                    {
                        action: "bis_get_allowable_tags",
                    });
            jqXHR.done(function (response) {
                var tags = response["data"]
                jQuery("#bis_allowed_tags").val(tags);
                jQuery("#bis_allowed_tags").select2({
                    tags: true
                });
            });
        }
        jQuery('#bis_download_city_contry_db').click(function () {
            jQuery('#bis_download_city_contry_db').hide();
            jQuery('#bis_downloading_city_contry_db').show();
            if (cityFile === '1' && contryFile === '1') {
                jQuery("#bis_downloading_city_contry_db").text("Updating latest DB please wait...");
            }
            var jqXHR = jQuery.get(
                    BISAjax.ajaxurl,
                    {
                        action: "bis_dowload_city_countries_db",
                    });
            jqXHR.done(function (response) {

                if (response.city === "success" && response.country === "success") {
                    jQuery('#bis_downloading_city_contry_db').hide();
                    jQuery("#bis_download_city_contry_db").show();
                    jQuery("#bis_download_city_contry_db").text("Update DB");
                    if (cityFile !== '1') {
                        jQuery('#bis_city_country_dropdown').append(jQuery('<option>', {value: "GeoLite2-City.mmdb"}).text("GeoLite2-City.mmdb"));
                    }
                    if (contryFile !== '1') {
                        jQuery('#bis_city_country_dropdown').append(jQuery('<option>', {value: "GeoLite2-Country.mmdb"}).text("GeoLite2-Country.mmdb"));
                    }

                } else if (response.city === "error" || response.country === "error") {
                    if (response.city === "error") {
                        bis_showSuccessMessage('<?php _e("Error accured while city DB downloading", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                    }
                    if (response.country === "error") {
                        bis_showSuccessMessage('<?php _e("Error accured while country DB downloading", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                    }
                } else {
                    if (response.city === "success") {
                        jQuery('#bis_city_country_dropdown').append(jQuery('<option>', {value: "GeoLite2-City.mmdb"}).text("GeoLite2-Country.mmdb"));
                    }
                    if (response.country === "success") {
                        jQuery('#bis_city_country_dropdown').append(jQuery('<option>', {value: "GeoLite2-Country.mmdb"}).text("GeoLite2-Country.mmdb"));
                    }
                    if (response.city === "error") {
                        bis_showSuccessMessage('<?php _e("Error accured while city DB downloading", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                    }
                    if (response.country === "error") {
                        bis_showSuccessMessage('<?php _e("Error accured while country DB downloading", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                    }
                }
                jQuery("#bis_city_country_dropdown").select2({

                });

            });
        });


        jQuery('input[name*="bis_ip2location_db"]').change(function () {
            if (jQuery('#bis_ip2_location_csv').is(':checked')) {
                jQuery('#bis_import_ip2location_db,#tip_csv_db').show();
            } else {
                jQuery('#bis_import_ip2location_db,#tip_csv_db').hide();
            }

        });
        jQuery("#bis_import_ip2location_db").click(function () {
            var csv_file_name = jQuery("#bis_ip2_location_db_csv_file").val();
            var ext = csv_file_name.slice(csv_file_name.length - 4, csv_file_name.length).toLowerCase();
            if (ext == ".csv") {
                jQuery("#bis_import_ip2location_db").hide();
                jQuery("#bis_installing_ip2_location_db").show();
                jQuery.get(ajaxurl,
                        {
                            action: "bis_ip2location_db_import",
                            file_name: jQuery("#bis_ip2_location_db_csv_file").val(),
                            bis_nonce: BISAjax.bis_rules_engine_nonce
                        },
                        function (response) {
                            jQuery("#bis_import_ip2location_db").show();
                            jQuery("#bis_installing_ip2_location_db").hide();

                            if (response == 0) {
                                //bis_alert(breCV.not_exist_csv);
                                bis_showErrorMessage('<?php _e("Entered ip2location CSV file does not found....!", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                            }

                        }
                );
            } else {
//                bis_alert(breCV.valid_csv);
                //alert('Please Enter Valid  CSV File....!');
                 bis_showErrorMessage('<?php _e("Please enter valid ip2location CSV file....!", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
            }
        });

        jQuery('#bis_re_save_settings').click(function () {

            var maxmind = false;
            var geoService = false;
            var cacheEnable = false;
            var deleteEnable = false;
            var ip2LOactionCheckbox = false;
            var ip2location_csv=false;
            var selectedFileNmae = null;
            var selectedTags = null;
            var ip2locationfile = null;
            var ip2locationcsvfile = null;
            var ip2lFile = null;
            var deletePlugin = null;
            var geonameuser = jQuery("#geonameuser").val();
            selectedFileNmae = jQuery("#bis_city_country_dropdown").val();
            ip2locationfile = jQuery('#bis_ip2_location_db_file').val();
            ip2locationcsvfile = jQuery('#bis_ip2_location_db_csv_file').val();
            selectedTags = jQuery("#bis_allowed_tags").val();
            if (jQuery("#bis_geo_maxmind").is(':checked')) {
                maxmind = true;
            }


            if (jQuery("#bis_geo_geoloc").is(':checked')) {
                geoService = true;
            }
            if (jQuery("#bis_re_delete_db").is(':checked')) {
                deleteEnable = true;
            }
            if (jQuery("#bis_re_del_plugin").is(':checked')) {
                deletePlugin = true;
            }
            if (jQuery("#bis_ip2_location_csv").is(':checked')) {
                
                 var csv_file_name =ip2locationcsvfile;
                var ext = csv_file_name.slice(csv_file_name.length - 4, csv_file_name.length).toLowerCase();
                if (ext == ".csv") {
                    ip2location_csv = true;
                } else {
                    // alert("Enter valid csv file...!");
                   // bis_alert(breCV.valid_csv);
                    bis_showErrorMessage('<?php _e("Please enter ip2location CSV  file....!", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                    return false;
                }
            }

            if (jQuery("#bis_ip2_location").is(':checked')) {
                var bin_file_name =ip2locationfile;
                var ext = bin_file_name.slice(bin_file_name.length - 4, bin_file_name.length).toLowerCase();
                if (ext == ".bin") {
                    ip2LOactionCheckbox = true;
                    ip2lFile = ip2locationfile;
                } else {
                    // alert("Enter valid BIN file...!");
                   // bis_alert(breCV.valid_bin);
                    bis_showErrorMessage('<?php _e("Please enter ip2location BIN  file....!", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                    return false;
                }
            }




            var jqXHR = jQuery.post(
                    BISAjax.ajaxurl,
                    {
                        action: 'bis_check_db_name',
                        ip2lFile: ip2lFile,
                        ip2LocationCheckbox: ip2LOactionCheckbox
                    }
            );
            jqXHR.done(function (response) {
                if ((response["status"] === "success" && response["data"] === "Maxmind DB") || response["status"] === "success") {

                    var jqXHR = jQuery.post(
                            BISAjax.ajaxurl,
                            {
                                action: 'bis_re_save_settings',
                                geonameuser: geonameuser,
                                selectedFileNmae: selectedFileNmae,
                                ip2locationfile: ip2locationfile,
                                ip2locationcsvfile: ip2locationcsvfile,
                                maxmind: maxmind,
                                geoService: geoService,
                                ip2LOactionCheckbox: ip2LOactionCheckbox,
                                deleteEnable: deleteEnable,
                                selectedTags: selectedTags,
                                deletePlugin: deletePlugin,
                                ip2location_csv:ip2location_csv,
                                bis_nonce: BISAjax.bis_rules_engine_nonce
                            }
                    );
                    jqXHR.done(function (response) {
                        if (response["status"] === "success") {
                            if (response["data"] === "true") {
                                jQuery("#bis_re_delete_db").attr("checked", "checked");
                            } else {
                                jQuery("#bis_re_delete_db").removeAttr("checked")
                            }
                            bis_showSuccessMessage('<?php _e("Plugin settings saved successfully", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                        } else {
                            if (response['message_key'] === "bis_invalid_database_file") {
                                if (ip2LOactionCheckbox === true) {
                                    bis_showErrorMessage('<?php _e("Invalid IP2location local database file", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                                }
                                if (maxmind === true) {
                                    bis_showErrorMessage('<?php _e("Invalid MaxMind local database file", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                                }
                            } else {
                                bis_showErrorMessage('<?php _e("Error occured while saving Plugin settings", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                            }
                        }
                    });
                } else {
                    bis_showErrorMessage('<?php _e("Invalid Ip2location DB file name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                }
            });
        });

        jQuery("#bis_allowed_tags").select2({
            tags: true
        });
    });
</script>