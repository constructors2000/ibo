<?php
use bis\repf\common\RulesEngineCacheWrapper;
use bis\repf\common\RulesEngineLocalization;

$logical_rule_engine_modal = new bis\repf\model\LogicalRulesEngineModel();

$ruleId = RulesEngineCacheWrapper::get_session_attribute(BIS_LOGICAL_RULE_ID);

// Get the rule details.
$bis_rule = $logical_rule_engine_modal->get_rule($ruleId);

// Ge the rule details
$rule = $bis_rule["rule"];

$city_radius_data = array();
/*$bis_longitude = '';
$bis_latitude = '';
$bis_city_radius = '20';
$bis_radius_unit = 'mi';

if($rule->near_by_cities != null){
    $near_by_cities = json_decode($rule->near_by_cities);
    $bis_longitude = $near_by_cities->longitude;
    $bis_latitude = $near_by_cities->latitude;
    $bis_city_radius = $near_by_cities->city_radius;
    $bis_radius_unit = $near_by_cities->radius_unit;
}*/
// Get the criteria array from map
$rule_criteria_array = $bis_rule["rule_criteria"];
$plugin_type_value = RulesEngineCacheWrapper::get_session_attribute(BIS_PLUGIN_TYPE_VALUE);
//RulesEngineCacheWrapper::remove_session_attribute(BIS_PLUGIN_TYPE_VALUE);
$bis_re_rule_options = $logical_rule_engine_modal->get_rules_options($plugin_type_value);
$bis_re_editable_roles = get_editable_roles();

$bis_attr_taxonomies = null;

if (RulesEngineUtil::is_woocommerce_installed()) {
    $bis_attr_taxonomies = $logical_rule_engine_modal->get_woo_attribute_taxonomies();
}

?>


<input type="hidden" name="bis_re_rId" id="bis_re_rId" value='<?php echo $rule->rId; ?>'/>
        <div class="panel panel-default future-box ">
           <!--   <div class="panel-heading">
                <div class="container-fluid">
                  <div class="row">
                        <div class="col-md-12">
                            <span class="panel-title"><?php _e("Edit Criteria", BIS_RULES_ENGINE_TEXT_DOMAIN);?></span>
                            <a class="popoverData"
                               target="_blank" href="<?php echo BIS_RULES_ENGINE_SITE ?>" data-content="<?php _e("All fields are required except operator field.
                               Operator field is used to define more complex rules. Click to know more about how to define
                               simple and complex rules.", BIS_RULES_ENGINE_TEXT_DOMAIN);?>" rel="popover" data-placement="bottom" data-trigger="hover">
                                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>-->
            <div class="panel-body">
                <table class="table table-hover table-bordered table-striped table-bottom">
                    <thead>
                    <tr>
                        <th width="20%"><?php _e("Criteria", BIS_RULES_ENGINE_TEXT_DOMAIN);?></th>
                        <th width="13%"><?php _e("Condition", BIS_RULES_ENGINE_TEXT_DOMAIN);?></th>
                        <th><?php _e("Value", BIS_RULES_ENGINE_TEXT_DOMAIN);?></th>
                        <th width="7%"></th>
                     </tr>
                    </thead>
                    <tbody>
                    <?php
                        $rows_count = BIS_RULES_CRITERIA_ROWS_COUNT;
                        $criteria_count = count($rule_criteria_array);
                        for ($i = 0; $i < $rows_count; $i++) {
                            $style = "";
                            $rule_criteria = null;
                           
                            // Hide other rows
                            if ($i > $criteria_count - 1) {
                                $style = "display:none";
                            } elseif ($i < $criteria_count) {
                                $rule_criteria = $rule_criteria_array[$i];
                                $oper_ar[$rule_criteria->operId] = "selected=\"selected\"";
                            }
                            ?>

                        <tr class="rule_criteria_row" id="rule_criteria_<?php echo $i ?>" style='<?php echo $style ?>'>
                            <td>
                                    <div class="input-group btn-group">
                                        <select class="bis-multiselect bis_re_sub_option" name="bis_re_sub_option[]"
                                                id="bis_re_sub_option_<?php echo $i ?>" size="2">
                                            <?php
                                            foreach ($bis_re_rule_options as $row) {
                                                $bis_sub_options = $logical_rule_engine_modal->get_rules_sub_options($row->id, $plugin_type_value);
                                                ?>
                                                <optgroup label="<?php echo __($row->name, 'rulesengine'); ?>">
                                                    <?php foreach ($bis_sub_options as $sub_row) { 
                                                        $selected = "";
                                                        if ($sub_row != null && $rule_criteria != null && $sub_row->id == $rule_criteria->subOptId) {
                                                            $selected = "selected=\"selected\"";
                                                        }
                                                        if ($sub_row->id !== '2000') {
                                                            
                                                            ?>
                                                            <option <?php echo $selected ?> value="<?php echo $row->id . '_' . $sub_row->id ?>"
                                                                                            id="<?php echo $sub_row->id ?>"><?php echo __($sub_row->name, 'rulesengine'); ?>
                                                            </option>
                                                        <?php
                                                        } else if (!empty($bis_attr_taxonomies['data'])) {
                                                            $bis_attr_list = $bis_attr_taxonomies['data'];
                                                            foreach ($bis_attr_list as $bis_list) {
                                                                $selected = "";
                                                                $cri_value = $row->id . '_' . $sub_row->id . '_' . $bis_list->id . '_' . $bis_list->slug;
                                                                if ($sub_row != null && $rule_criteria != null && $sub_row->id == $rule_criteria->subOptId
                                                                        && $rule_criteria->gencol1 == $cri_value) {
                                                                    $selected = "selected=\"selected\"";
                                                                }
                ?>    
                                                            <option <?php echo $selected ?> value="<?php echo $cri_value ?>"
                                                                id="<?php echo $sub_row->id ?>"><?php echo ucfirst($bis_list->name) ?>
                                                            </option>    
                                                        <?php
                                                        }
                                                        }
                                                       } ?>        
                                                </optgroup>
                                                <?php } ?>          
                                        </select>
                                    </div>
                                </td>
                            <td>
                                <span class="bis_re_condition_span" id="bis_re_condition_span_<?php echo $i ?>">
                                <?php if ($i < $criteria_count) {
                                    $rconditions = $logical_rule_engine_modal->get_rules_conditions($rule_criteria->subOptId);
                                    $rules_conditions = RulesEngineLocalization::get_localized_values($rconditions["RuleConditions"]);
                                    ?>
                                    <select size="4" id="bis_re_condition_<?php echo $i ?>" name="bis_re_condition[]" class="bis-multiselect">
                                        <?php foreach ($rules_conditions as $rule_cond) {

                                            $selected = "";
                                            if($rule_criteria != null && $rule_cond->id == $rule_criteria->condId) {
                                                $selected =  "selected=\"selected\"";
                                            }
                                            ?>
                                        <option <?php echo $selected ?> value="<?php echo $rule_cond->id ?>">
                                                <?php echo $rule_cond->name ?>
                                            </option>
                                        <?php } ?>

                                    </select>
                                <?php } ?>
                               </span>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class="col-sm-10 pl-0">
                                        <span id="bis_re_rule_value_span_<?php echo $i ?>">
                                        <?php if ($i < $criteria_count) {

                                            $value_type_id = (int)$rule_criteria->valueTypeId;
                                            
                                            array_push($city_radius_data, $rule_criteria->gencol1);
                                            
                                            switch ($value_type_id) {

                                                case 1: // Input Token
                                                    if($rule_criteria->subOptId === "21" || $rule_criteria->subOptId === "22") {
                                                        $pages_json = $logical_rule_engine_modal->get_posts_by_ids(json_decode($rule_criteria->value));
                                                        $rule_criteria->value = json_encode($pages_json);
                                                    } else if($rule_criteria->subOptId === "17") {
                                                        $categories_json = $logical_rule_engine_modal->get_wp_categories_by_ids(json_decode($rule_criteria->value));
                                                        $rule_criteria->value = json_encode($categories_json);
                                                    } else if($rule_criteria->subOptId === "25") {
                                                        $categories_json = $logical_rule_engine_modal->get_woocommerce_categories_by_ids(json_decode($rule_criteria->value));
                                                        $rule_criteria->value = json_encode($categories_json);
                                                    }
                                                    ?>
                                                    <input type="text" class="bis_re_rule_value" autocomplete="off" class="form-control"
                                                           id="bis_re_rule_value_<?php echo $i ?>"
                                                           name="bis_re_rule_value[]"
                                                           placeholder="Enter rule value"/>

                                                    <?php break;
                                                case 2: //SelectBox
                                                     $values = $logical_rule_engine_modal->get_rule_values($rule_criteria->subOptId, true, false, false, null, $rule_criteria->value);
                                                     ?>

                                                    <select class="form-control bis_re_rule_value select" name="bis_re_rule_value[]"
                                                            id="bis_re_rule_value_<?php echo $i ?>" size="2">
                                                        <?php foreach ($values as $value) {
                                                            $selected = "";

                                                            if ($value->id == $rule_criteria->value) {
                                                                $selected = "selected=selected";
                                                            }
                                                            ?>

                                                            <option  <?php echo $selected ?> value="<?php echo $value->id ?>"
                                                                                             id="<?php echo $value->id ?>">
                                                                <?php echo $value->name ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>

                                                    <?php break;
                                                case 3: // Date
                                                    ?>

                                                    <input type="text" autocomplete="off" class="form-control bis_re_rule_value"
                                                           id="bis_re_rule_value_<?php echo $i ?>"
                                                           name="bis_re_rule_value[]"
                                                           placeholder="Enter rule value" value="<?php echo $rule_criteria->value; ?>"/>

                                                    <?php break;
                                                case 4: // Textbox
                                                    $rule_value = $rule_criteria->value;
                                                    
                                                    
                                                    if ($rule_criteria->optId == 3 || $rule_criteria->subOptId == 32) {
                                                        $rule_value = json_decode($rule_value);
                                                        $rule_value = $rule_value[0]->id;
                                                    }
                                                    if($rule_criteria->subOptId == 44){ ?>
                                                    <input type="text" style='width: 66px; height: 34px;border-radius: 4px; position: absolute; right: -59px; top: 0px;' id='bis_distance_units_<?php echo $i ?>_value_1' name='bis_distance_units_<?php echo $i ?>_value_1' placeholder='mi or km' value="<?php echo $rule_criteria->gencol2; ?>">
                                                 <?php }
                                                    ?>

                                                    <input type="text" autocomplete="off" class="form-control"
                                                           id="bis_re_rule_value_<?php echo $i ?>"
                                                           name="bis_re_rule_value[]"
                                                           placeholder="<?php _e("Enter rule value", BIS_RULES_ENGINE_TEXT_DOMAIN);?>" value="<?php echo $rule_value; ?>"/>


                                                    <?php break;
                                            }
                                            } ?>
                                        </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <span class="bis_re_value_type_id_span"
                                          id="bis_re_value_type_id_span_<?php echo $i ?>"> 
                                </span>
                                 <?php 
                                    $bis_re_log_style = "";
                                    if ($i < ($criteria_count - 1)) { 
                                        $bis_re_log_style = "style=display:none;";
                                    }   
                                ?>
                                <span>
                                    <a <?php echo $bis_re_log_style; ?> href="javascript:void(0)" class="logical-operator"
                                           id="bis_add_criteria_<?php echo $i ?>" title="Add criteria">
                                            <span class="glyphicon glyphicon-plus-sign bis-add-criteria-icon-blue" aria-hidden="true">
                                            </span>
                                        </a>
                                </span>
                                <span>
                                       <?php if ($i != 0) { ?>
                                        <a href="javascript:void(0)" class="bis-remove-icon bis_remove_criteria"
                                            id="bis_remove_criteria_<?php echo $i ?>" title="Remove criteria">
                                            <span class="glyphicon glyphicon-remove bis-delete-criteria-icon-red"
                                                aria-hidden="true">
                                            </span>   
                                         </a>
                                        <?php } ?> 
                                </span>
                            </td>
                            <?php
                            if($rule_criteria != null) {
                                                                                            
                                if($rule_criteria->valueTypeId === "1") {
                                   $rule_criteria->value = stripslashes($rule_criteria->value);
                                }
                                ?>
                                <input type="hidden" name="bis_re_rcId[]" id="bis_re_rcId_<?php echo $i ?>"
                                       value="<?php echo $rule_criteria->rcId; ?>"/>
                                <input type="hidden" id="bis_re_tokenInput_<?php echo $i ?>" name="bis_re_tokenInput[]"
                                       value="<?php echo $rule_criteria->valueTypeId; ?>"/>
                                
                                <input type="hidden" class="bis_re_rule_value_json" name="bis_re_rule_value_json[]"
                                       value='<?php echo $rule_criteria->value; ?>'
                                       id="bis_re_rule_value_json_<?php echo $i ?>"/>
                                <input type="hidden" class="bis_re_sub_opt_type_id"
                                       id="bis_re_sub_opt_type_id_<?php echo $i ?>"
                                       name="bis_re_sub_opt_type_id[]" value="<?php echo $value_type_id; ?>"/>
                                <input type="hidden" class="bis_re_delete"
                                       id="bis_re_delete_<?php echo $i ?>"
                                       name="bis_re_delete[]" />
                          <?php  } ?>

                        </tr>
                       <?php } ?>
                    </tbody>
                </table>
            </div>
            <span id="bis_near_by_popup" style="display: none">
                <?php require_once 'bis-near-by-popup.php'; ?>
            </span>
            <input type="hidden" id="bis_re_name" name="bis_re_name" value="<?php echo $rule->name ?>">
            <input type="hidden" id="bis_re_description" name="bis_re_description" value="<?php echo $rule->description ?>">
            <input type="hidden" id="bis_re_hook" name="bis_re_hook" value="">
            <input type="hidden" id="bis_re_eval_type" name="bis_re_eval_type" value="1">
            <input type="hidden" id="bis_re_status" name="bis_re_status" value="<?php echo $rule->status ?>">
            <input type="hidden" id="bis_re_city_lat" name="bis_re_city_lat" value="">
            <input type="hidden" id="bis_re_city_lng" name="bis_re_city_lng" value="">
        </div>
        <div class="form-group container-fluid" id="bis_city_radius_div" style="display: none; background: #dad7d7; border-radius: 4px; width: 97%;">
            <div class="form-group bis-city-radius-popup" style="position: relative; top: 7px; font-size: 18px;">
                <a href="javascript:void(0)" id="bis_city_radius_popup"><i class="fa fa-hand-o-right" style="padding-right: 10px;" aria-hidden="true"></i><?php _e("Check near by cities.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></a>
            </div>
        </div>
<div class="form-group container-fluid bis-postcode-csv-file" style="display: none; background: #dad7d7; border-radius: 4px;">
    <div class="form-group col-md-3 pl-0 bis-postcode-csv-file">
        <label><?php _e("Upload post/zip code csv", BIS_RULES_ENGINE_TEXT_DOMAIN); ?> </label>
        <a class="popoverData" target="_blank" href="<?php echo BIS_RULES_ENGINE_SITE ?>"
           data-content='<?php _e("If you have a post/zip code in a .csv format, you may add it by uploading it here..", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'
           rel="popover"
           data-placement="bottom" data-trigger="hover">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
        </a>
        <input type="file" class="form-control mega-file-upload" id="bis_re_post_code_file" name="bis_re_post_code_file">

    </div>
    <div class="mega-btn-upload col-md-2 bis-postcode-csv-file">
        <button id="bis_re_post_code_upload" type="button" class="btn btn-primary">
            <i class=""></i>Upload</button>
    </div>
</div>
<style>
    .mega-btn-upload {
        margin-top: 22px;
    }
    .mega-file-upload {
        height: 30px;
    }
</style>
<!--<div class="panel-body" id="bis_city_radius_div" style="display: none">
    <div class="panel-body post-rules-container-edit dashboard-panel">
        <div>
            <div class="row">
                <div class="form-group col-md-2">
                    <label><?php _e("City Radius", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                    <a class="popoverData" target="_blank" href="<?php //echo BIS_RULES_ENGINE_SITE ?>"
                       data-content="<?php _e("City radius will get cities within specified radius.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>"
                       rel="popover"
                       data-placement="bottom" data-trigger="hover">
                        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                    </a>
                    <input type="text" class="form-control bis-near-radius"
                           id="bis_re_city_radius" name="bis_re_city_radius"
                           value="20">
                    <div class="can-toggle clearfix can-toggle--size-small mt-10 bis_re_mi_km">
                        <input id="bis_re_mi_km" name="bis_re_mi_km" checked type="checkbox">
                        <label for="bis_re_mi_km">
                            <div class="can-toggle__switch toggle_switch_bgclr" data-checked="mi" data-unchecked="km"></div>
                        </label>
                    </div>
                </div>
                <div class="form-group bis-city-radius-popup col-md-2" style="position: relative; top: 25px; font-size: 19px;">
                    <a href="javascript:void(0)" id="bis_city_radius_popup"><i class="fa fa-hand-o-right" aria-hidden="true"><?php _e("Near Cities", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></i></a>
                </div>
            </div>
        </div>
    </div>
</div>-->
<script>

    // wait for the DOM to be loaded
    jQuery(document).ready(function () {

        var selectedValueContainer = null;
        jQuery("#wpfooter").css("position", "relative");
        jQuery('.popoverData').popover();
        subCriteria = 0;
        ssubCriteria = 0;
        
        jQuery(".input-group-remove").click(function () {
            jQuery(this.parentElement).remove();
        });

        jQuery('.select').multiselect({
            nonSelectedText: '<?php _e("Select Criteria", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>',
            enableCaseInsensitiveFiltering: true,
            maxHeight: 220,
            buttonWidth: '350px'
        });  
        
        jQuery('.bis_re_sub_option').multiselect({
            nonSelectedText: '<?php _e("Select Criteria", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>',
            enableCaseInsensitiveFiltering: true,
            maxHeight: 220,
            buttonWidth: '350px',
            onChange: function (element, checked) {
                var subCrVal = element[0].value.split("_")[1];
                var rowIndex = jQuery(element[0]).closest("tr").index();
                var ssubCrVal = element[0].value.split("_")[3];
                subCriteria = subCrVal;
                ssubCriteria = ssubCrVal;
                addSubOptionChangeEvent(subCrVal, ssubCrVal, rowIndex);

                //Remove child dependent values
                var siblings = element.closest("td").nextAll();
                jQuery(siblings[0].children).html('<?php _e("None Selected", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                jQuery(siblings[1].children).children().children().html("");
                addDropdownDeleteEvent();
                
            }

        });


        jQuery('.bis-multiselect').multiselect({
        });

        function showResponse(responseText, statusText, xhr, $form) {

            if (responseText["status"] === "success") {
                bis_showLogicalRulesList();
            } else {
                if (responseText["status"] === "error") {
                    if (responseText["message_key"] === "duplicate_entry") {
                        bis_showErrorMessage('<?php _e("Duplicate rule name, Name should be unique.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                    } else if (responseText["message_key"] === "no_method_found") {
                        bis_showErrorMessage('<?php _e("Action hook method does not exist, Please define method with name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'
                                + " \"" + jQuery("#bis_re_hook").val() + "\".");
                    } else {
                        bis_showErrorMessage("<?php _e("Error occurred while creating rule.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>");
                    }
                }
            }

        }

        // Adds dropdown delete event
        function addDropdownDeleteEvent() {
            jQuery(".input-group-remove").on("click", function (evt) {
                evt.stopPropagation();
                evt.preventDefault();

                var childCond = jQuery(this).closest("td").nextAll();

                for (var i = 0; i < childCond.length; i++) {
                    jQuery(jQuery(childCond)[i]).find(".btn-group").remove();
                }

                jQuery(this.parentElement).remove();
            });

        }


        /**
         * This method is used to get the sub options of rules based on the optionId
         */
        function addSubOptionChangeEvent(subCrVal, ssubCrVal, rowIndex) {

                // Componenent Id should be dynamic
                jQuery.post(
                        BISAjax.ajaxurl,
                        {
                            action: 'bis_get_conditions',
                            optionId: subCrVal,
                            ssubOptionId : ssubCrVal
                        },
                function (response) {

                    var data = {
                        compId: 'bis_re_condition_' + rowIndex,
                        compName: 'bis_re_condition[]',
                        suboptions: response["RuleConditions"]
                    };

                    var valueTypeId = response["ValueTypeId"];
                    var source = jQuery("#bis-selectComponent").html();
                    var template = Handlebars.compile(source);
                    var logicalRulesContent = template(data);
                    
                    jQuery("#bis_re_condition_span_" + rowIndex).html(logicalRulesContent);
                    
                    jQuery('.bis-multiselect').multiselect({
                        nonSelectedText: '<?php _e("None Selected", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'
                    });

                    var dataTypeId = {
                        id: 'bis_re_sub_opt_type_id_' + rowIndex,
                        name: 'bis_re_sub_opt_type_id[]',
                        value: valueTypeId
                    };

                    source = jQuery("#bis-logical-hidden-component").html();
                    template = Handlebars.compile(source);
                    var valueTypeIdContent = template(dataTypeId);

                    jQuery("#bis_re_value_type_id_span_" + rowIndex).html(valueTypeIdContent);

                    addDropdownDeleteEvent();
                    addConditionChangeEvent(jQuery("#" + data.compId), valueTypeId);    
                    
                }
                );
        }

        function addSelectBox(rowIndex) {

            var jqXHR = jQuery.get(ajaxurl,
                    {
                    action: "bis_re_get_rule_values",
                    bis_nonce: BISAjax.bis_rules_engine_nonce,
                    bis_sub_criteria: subCriteria,
                    bis_ssubCriteria: ssubCriteria
                });

            jqXHR.done(function (response) {

                var data = {
                    compId: 'bis_re_rule_value_' + rowIndex,
                    compName: 'bis_re_rule_value[]',
                    suboptions: response
                };

                var source = jQuery("#bis-selectComponent").html();

                var template = Handlebars.compile(source);
                var logicalRulesContent = template(data);
                var subOptObj = jQuery("#bis_re_rule_value_span_" + rowIndex);

                subOptObj.html(logicalRulesContent);

                jQuery('.bis-multiselect').multiselect({
                    nonSelectedText: '<?php _e("None Selected", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>',
                    maxHeight: 200,
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering:true
                });
            });

        }


        function renderTextBox(data, rowIndex) {
            
            if((subCriteria === 32 || subCriteria === 26 || subCriteria === 27)
                    && (condition === 1 || condition === 2)) {
                data.placeholder = '<?php _e("Enter a value, format name=value", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>';
            }
            if(subCriteria === 46) {
                data.placeholder = '<?php _e("Enter hours hr:mi Ex(24:00 or 00:30 or 01:05)", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>';
            }
            var source = jQuery("#bis-logical-textbox-component").html();
            var template = Handlebars.compile(source);
            var textbox = template(data);
            var textboxId = jQuery("#bis_re_rule_value_span_" + rowIndex);
            textboxId.html(textbox);
            var ids_exists = jQuery.find("#bis_distance_units_" + rowIndex + "_value_1").length;
            if(subCriteria === 44 && ids_exists === 0) {
                jQuery("#bis_re_rule_value_span_" + rowIndex).append("<input type='text' style='width: 66px; height: 34px;border-radius: 4px; position: absolute; right: -59px; top: 0px;' id='bis_distance_units_" + rowIndex + "_value_1' name='bis_distance_units_" + rowIndex + "_value_1' placeholder='mi or km'>");
            } else if(ids_exists > 1) {
                jQuery("#bis_distance_units_" + rowIndex + "_value_1").remove();
            }
            if(subCriteria != 44) {
                jQuery("#bis_distance_units_" + rowIndex + "_value_1").remove();
            }
            
            if(subCriteria === 35 && (condition === 12 || condition === 14)){
               jQuery('.bis-postcode-csv-file').show();
                var pcods = '';
                jQuery('#bis_re_post_code_upload').click(function () {
                   jQuery("#bis_re_rule_value_" + rowIndex).val('');
                    var csv = jQuery('#bis_re_post_code_file');
                    var csvFile = csv[0].files[0];
                    var ext = csv.val().split(".").pop().toLowerCase();
                    if (jQuery.inArray(ext, ["csv"]) === -1) {
                        alert('upload csv');
                        return false;
                    }
                    if (csvFile !== 'undefined') {
                        reader = new FileReader();
                        reader.onload = function (e) {
                            rows = e.target.result.split(/\r|\n|\r\n/);
                            if (rows.length > 0) {
                                var first_Row_Cells = splitCSVtoCells(rows[0], ","); //Taking Headings
                                var jsonArray = new Array();
                                for (var i = 1; i < rows.length; i++) {
                                    var cells = splitCSVtoCells(rows[i], ",");
                                    var obj = {};
                                    for (var j = 0; j < cells.length; j++){
                                        obj[first_Row_Cells[j]] = cells[j];
                                    }
                                    jsonArray.push(obj);
                                }
                                pcods = '';
                                jQuery.each(jsonArray, function(key,val){
                                    if(val.hasOwnProperty('Pincode')){
                                        var pinStr = val.Pincode;
                                        if(pcods !== '' && pinStr !== ''){
                                            pcods = pcods+','+pinStr;
                                        } else if (pinStr !== '') {
                                            pcods = pinStr;
                                        }
                                    }
                                });
                                if(pcods !== ''){
                                    jQuery("#bis_re_rule_value_" + rowIndex).val(pcods);
                                }
                            }
                        };
                        reader.readAsText(csvFile);
                    }
                });
            } else {
                jQuery('.bis-postcode-csv-file').hide();
            }
        }
        
        function splitCSVtoCells(row, separator) {
            var sliptedRow = row.split(separator);
            jQuery.each(sliptedRow, function(key, val){
                sliptedRow[key] = val.replace(/\"/g, "");
            });
            return sliptedRow;
        }
        
        function addConditionChangeEvent(condOptObj, valueTypeId) {

            condOptObj.on("change", function () {

                condition = Number(jQuery.trim(this.value));
                var rowIndex = jQuery(this).closest("tr").index();
                jQuery("#bis_re_rule_value_" + rowIndex).prev().remove();

                subCriteria = Number(subCriteria);
                valueTypeId = Number(valueTypeId);

                switch (valueTypeId) {

                    case 1: // Token Input
                        addTokenInput(rowIndex);

                        switch (condition) {
                            case 1: // Equal
                            case 2: // Not Equal
                            case 12:
                            case 14:
                            case 15: 
                                if(condition == 15) {
                                    jQuery("#bis_city_radius_div").show();
                                    jQuery("#bis_re_rule_value_span_" + rowIndex).append("<input type='text' style='width: 70px; position: absolute; right: -30px; top: 0px;' id='bis_city_radius_" + rowIndex + "_value_1' name='bis_city_radius_" + rowIndex + "_value_1' placeholder='Radius'><select style='width: 55px; position: absolute; right: -90px; top: 0px;' id='bis_city_radius_units_" + rowIndex + "_value_2' name='bis_city_radius_units_" + rowIndex + "_value_2'><option val='mi'>mi</option><option val='km'>km</option></select><input type='hidden' id='bis_city_latitude_" + rowIndex + "_value_3' name='bis_city_latitude_" + rowIndex + "_value_3'><input type='hidden' id='bis_city_longitude_" + rowIndex + "_value_4' name='bis_city_longitude_" + rowIndex + "_value_4'>");
                                } else {
                                    jQuery("#bis_city_radius_div").hide();
                                    jQuery("#bis_city_radius_" + rowIndex + "_value_1").remove();
                                    jQuery("#bis_city_radius_units_" + rowIndex + "_value_2").remove();
                                    jQuery("#bis_city_latitude_" + rowIndex + "_value_3").remove();
                                    jQuery("#bis_city_longitude_" + rowIndex + "_value_4").remove();
                                }
                                addTokenInput(rowIndex);
                                break;
                            default:
                                var data = {
                                    id: 'bis_re_rule_value_' + rowIndex,
                                    name: 'bis_re_rule_value[]',
                                    placeholder: '<?php _e("Enter a value", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'
                                };

                                renderTextBox(data, rowIndex);

                        }
                        break;

                    case 2:  // Select Option
                        addSelectBox(rowIndex);
                        break;

                    case 3: // Calendar Date
                        var data = {
                            id: 'bis_re_rule_value_' + rowIndex,
                            name: 'bis_re_rule_value[]',
                            placeholder: '<?php _e("Enter date", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'
                        };

                        // Default only date
                        tpicker = false;
                        dformat = 'Y-m-d';
                        dpicker = true;

                        if (subCriteria === 14) { // Date & Time
                            tpicker = true;
                            dformat = 'Y-m-d H:i';
                        } else if (subCriteria === 10) {  // Only Time
                            dpicker = false;
                            dformat = 'H:i';
                            tpicker = true;
                        }

                        renderTextBox(data, rowIndex);

                        jQuery("#bis_re_rule_value_" + rowIndex).datetimepicker({
                            timepicker: tpicker,
                            format: dformat,
                            datepicker: dpicker,
                            closeOnDateSelect: true,
                            mask: true
                        });

                        break;

                    case 4: // Text box
                        var data = {
                            id: 'bis_re_rule_value_' + rowIndex,
                            name: 'bis_re_rule_value[]',
                            placeholder: '<?php _e("Enter a value", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'
                        };

                        if(condition === 12 || condition === 14) {
                          data.placeholder = '<?php _e("Enter values with comma separation (i.e value1, value2, value3)", "rulesengine"); ?>';
                        } else if(condition === 13) {
                          data.placeholder = '<?php _e("Enter URL with /** pattern (ex: http://rulesengine.in/mypath/**)", "rulesengine"); ?>';
                        }  
                        renderTextBox(data, rowIndex);
                        break;
                }
            });
        }


        /**
         Add the tokenInput component with values.
         */
        function addTokenInput(rowIndex) {

            var bis_nonce = BISAjax.bis_rules_engine_nonce;
            jQuery("#bis_re_rule_value_" + rowIndex).prev().remove();

            var data = {
                id: 'bis_re_rule_value_' + rowIndex,
                name: 'bis_re_rule_value[]'
            };

            var source = jQuery("#bis-logical-hidden-component").html();
            var template = Handlebars.compile(source);
            var textbox = template(data);
            var textboxId = jQuery("#bis_re_rule_value_span_" + rowIndex);
            textboxId.html(textbox);
            var tokenLimit = 1;
            if((condition === 12)||(condition === 14)){
                tokenLimit = null;
            }

            jQuery("#bis_re_rule_value_" + rowIndex).tokenInput(ajaxurl + "?action=bis_re_get_value&bis_nonce=" + bis_nonce +
                    "&subcriteria=" + subCriteria + "&condition=" + condition, {
                        theme: "facebook",
                        minChars: 2,
                        method: "POST",
                        tokenLimit: tokenLimit,
                        onAdd: function (item) {
                            jQuery("#bis_re_rule_value_" + rowIndex).val(JSON.stringify(this.tokenInput("get")));
                            jQuery("#bis_re_city_lng").val(item.lng);
                            jQuery("#bis_re_city_lat").val(item.lat);
                            if (condition === 15) {
                                selectedValueContainer = jQuery.parseJSON(jQuery("#bis_re_rule_value_" + rowIndex).val());
                                jQuery("#bis_city_latitude_" + rowIndex + "_value_3").val(item.lat);
                                jQuery("#bis_city_longitude_" + rowIndex + "_value_4").val(item.lng);
                            } 
                            if (condition !== 15) {
                                jQuery("#bis_city_radius_" + rowIndex + "_value_1").remove();
                                jQuery("#bis_city_radius_units_" + rowIndex + "_value_2").remove();
                                jQuery("#bis_city_latitude_" + rowIndex + "_value_3").remove();
                                jQuery("#bis_city_longitude_" + rowIndex + "_value_4").remove();
                            }
                        },
                        onDelete: function (item) {
                            jQuery(this).val(JSON.stringify(this.tokenInput("get")));
                        }
                    });
            if (condition === 15) {
                jQuery("#bis_re_rule_value_span_" + rowIndex).append("<input type='text' style='width: 70px; position: absolute; right: -30px; top: 0px;' id='bis_city_radius_" + i + "_value_1' name='bis_city_radius_" + i + "_value_1' placeholder='Radius'><select style='width: 55px; position: absolute; right: -90px; top: 0px;' id='bis_city_radius_units_" + i + "_value_2' name='bis_city_radius_units_" + i + "_value_2'><option val='mi'>mi</option><option val='km'>km</option></select><input type='hidden' id='bis_city_latitude_" + i + "_value_3' name='bis_city_latitude_" + i + "_value_3'><input type='hidden' id='bis_city_longitude_" + i + "_value_4' name='bis_city_longitude_" + i + "_value_4'>");
                jQuery("#bis_city_radius_div").show();
            } 
            if (condition !== 15) {
                jQuery("#bis_city_radius_" + rowIndex + "_value_1").remove();
                jQuery("#bis_city_radius_units_" + rowIndex + "_value_2").remove();
                jQuery("#bis_city_latitude_" + rowIndex + "_value_3").remove();
                jQuery("#bis_city_longitude_" + rowIndex + "_value_4").remove();
                jQuery("#bis_city_radius_div").hide();
                jQuery("#bis_re_city_lng").val('');
                jQuery("#bis_re_city_lat").val('');
            }

        }

        jQuery(".bis_remove_criteria").click(function (event) {
            event.preventDefault();
            var ctr = jQuery(this).closest("tr").filter(":visible");
            var pretr = ctr.prevUntil().filter(":visible").attr("id");
            var preIndex = getRowIndex(pretr);
            var siblings = ctr.children();
            var crowlen = Number(jQuery(".rule_criteria_row").filter(":visible").length);

            // Removes the dropdown logical operation value.
            crowlen = crowlen - 1;
            var cIndex = Number(preIndex) + 1;

            if (crowlen === cIndex) {
                jQuery("#bis_add_criteria_" + preIndex).show();
            }
            
            // Remove the values before hiding the criteria
            jQuery("#bis_re_condition_span_"+cIndex).html('<?php _e("None Selected", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
            jQuery("#bis_re_rule_value_span_"+cIndex).html("");

            var cSubOption =  jQuery("#bis_re_sub_option_"+cIndex);
            cSubOption.multiselect("deselect", cSubOption.val());
            // Hide the next row
            jQuery(this).closest("tr").hide();
            
        });

        /**
         This method will show and hide the next criteria based on Operator value.
         */
        jQuery(".logical-operator").click(function (event) {
            event.preventDefault();
            jQuery(this).closest("tr").next().show();
            var preIndex = getRowIndex(jQuery(this).attr("id"));
            jQuery(this).hide();
            var crowlen = Number(jQuery(".rule_criteria_row").filter(":visible").length);

            // Removes the dropdown logical operation value.
            crowlen = crowlen - 1;
            var nextIndex = Number(preIndex) + 1;
            jQuery("#bis_add_criteria_" + nextIndex).show();
            
        });


        function split(val) {
            return val.split(/,\s*/);
        }

        function extractLast(term) {
            return split(term).pop();
        }

        function getSubCriteria(val) {
            var ele = jQuery(val).closest("td").prevAll();
            subCriteria = jQuery(ele[1]).children().children().children()[1].value;
            return subCriteria;
        }

        function getCondition(val) {

            var ele = jQuery(val).closest("td").prevAll();
            return jQuery(ele[0]).children().children().children()[1].value;
        }

        jQuery("#bis_back_rules_list").click(function () {
            bis_showLogicalRulesList();
        });


        // From edit page

        jQuery("#wpfooter").css("position","relative");
        jQuery('.popoverData').popover();
        jQuery('.bis-multiselect').multiselect();


        jQuery("#bis_re_status").change(function (event) {

            if (jQuery("#bis_re_status").val() === "0") {
                bis_warn('<?php _e("Dependent child rules will become inactive, if logical rule is deactivated", BIS_RULES_ENGINE_TEXT_DOMAIN);?>');
            }
        });
        
        var rule_criteria = <?php echo json_encode($city_radius_data); ?>;
        var rule_values = jQuery(".bis_re_rule_value");
        var bis_nonce = BISAjax.bis_rules_engine_nonce;
        var visible_rows  = jQuery(".rule_criteria_row").filter(":visible").length;
       
        if(visible_rows == 1){
            var catSubOptId = jQuery("#bis_re_sub_option_"+0).val().split("_");
            var subOptId = Number(catSubOptId[1]);        
            var conditionId = Number(jQuery("#bis_re_condition_"+0).val());
            if(subOptId == 35 &&(conditionId == 12 || conditionId == 14)){
                jQuery('.bis-postcode-csv-file').show();
                var pcods = '';
                jQuery('#bis_re_post_code_upload').click(function () {
                    jQuery("#bis_re_rule_value_" + 0).val('');
                    var csv = jQuery('#bis_re_post_code_file');
                    var csvFile = csv[0].files[0];
                    var ext = csv.val().split(".").pop().toLowerCase();
                    if (jQuery.inArray(ext, ["csv"]) === -1) {
                        alert('upload csv');
                        return false;
                    }
                    if (csvFile !== 'undefined') {
                        reader = new FileReader();
                        reader.onload = function (e) {
                            rows = e.target.result.split(/\r|\n|\r\n/);
                            if (rows.length > 0) {
                                var first_Row_Cells = splitCSVtoCells(rows[0], ","); //Taking Headings
                                var jsonArray = new Array();
                                for (var i = 1; i < rows.length; i++) {
                                    var cells = splitCSVtoCells(rows[i], ",");
                                    var obj = {};
                                    for (var j = 0; j < cells.length; j++) {
                                        obj[first_Row_Cells[j]] = cells[j];
                                    }
                                    jsonArray.push(obj);
                                }
                                pcods = '';
                                jQuery.each(jsonArray, function (key, val) {
                                    if (val.hasOwnProperty('Pincode')) {
                                        var pinStr = val.Pincode;
                                        if (pcods !== '' && pinStr !== '') {
                                            pcods = pcods + ',' + pinStr;
                                        } else if (pinStr !== '') {
                                            pcods = pinStr;
                                        }
                                    }
                                });
                                if (pcods !== '') {
                                    jQuery("#bis_re_rule_value_" + 0).val(pcods);
                                }
                            }
                        };
                        reader.readAsText(csvFile);
                    }
                });
            }
        }
        
        for (var i = 0; i < rule_values.length; i++) {

            var rule_value = rule_values[i];
            var arr = rule_value.id.split("_");
            var uId = arr[arr.length-1];
            var ruleIdObj = jQuery("#" + rule_value.id);
            var optId = jQuery("#bis_re_rule_option_"+uId).val();
            var catSubOptId = jQuery("#bis_re_sub_option_"+uId).val().split("_");
            var optId = Number(catSubOptId[0]);
            var subOptId = Number(catSubOptId[1]);        
            var conditionId = Number(jQuery("#bis_re_condition_"+uId).val());
            var rule_value_json = jQuery("#bis_re_rule_value_json_"+uId).val();
            var rule_type_id = Number(jQuery("#bis_re_sub_opt_type_id_"+uId).val());
            var tokenLimit = 1;
            if((conditionId === 12)||(conditionId === 14)){
                tokenLimit = null;
            }
            
            switch (rule_type_id) {
                case 1: // Input Token
                    var params = "?action=bis_re_get_value&bis_nonce=" + bis_nonce + "&criteria=" + optId + "&subcriteria=" + subOptId + "&condition=" + conditionId;

                    if (conditionId === 1 || conditionId === 2 || conditionId === 14 || conditionId === 12 || conditionId === 15 ) {
                        var $tokenInput = jQuery("#" + rule_value.id).tokenInput(ajaxurl + params,
                            {
                                theme: "facebook",
                                prePopulate: jQuery.parseJSON(rule_value_json),
                                minChars: 2,
                                method: "POST",
                                tokenLimit: tokenLimit,
                                onAdd: function (item) {
                                    jQuery(this).val(JSON.stringify(this.tokenInput("get")));
                                    jQuery("#bis_re_city_lng").val(item.lng);
                                    jQuery("#bis_re_city_lat").val(item.lat);
                                    if (conditionId === 15) {
                                        selectedValueContainer = jQuery.parseJSON(rule_value_json);
                                        jQuery("#bis_city_latitude_" + uId + "_value_3").val(item.lat);
                                        jQuery("#bis_city_longitude_" + uId + "_value_4").val(item.lng);
                                    } 
                                    if (conditionId !== 15) {
                                        jQuery("#bis_city_radius_" + uId + "_value_1").remove();
                                        jQuery("#bis_city_radius_units_" + uId + "_value_2").remove();
                                        jQuery("#bis_city_latitude_" + uId + "_value_3").remove();
                                        jQuery("#bis_city_longitude_" + uId + "_value_4").remove();
                                    }
                                },
                                onDelete: function (item) {
                                    jQuery(this).val(JSON.stringify(this.tokenInput("get")));
                                }
                            });
                        $tokenInput.val(rule_value_json);
                    } else {
                        jQuery(rule_value).val(rule_value_json);
                    }
                    if (conditionId === 15) {
                        selectedValueContainer = jQuery.parseJSON(rule_value_json);
                        var city_radius = JSON.parse(rule_criteria[i]);
                        jQuery("#bis_re_rule_value_span_" + i).append("<input type='text' style='width: 70px; position: absolute; right: -30px; top: 0px;' id='bis_city_radius_" + i + "_value_1' name='bis_city_radius_" + i + "_value_1' placeholder='Radius'><select style='width: 55px; position: absolute; right: -90px; top: 0px;' id='bis_city_radius_units_" + i + "_value_2' name='bis_city_radius_units_" + i + "_value_2'><option val='mi'>mi</option><option val='km'>km</option></select><input type='hidden' id='bis_city_latitude_" + i + "_value_3' name='bis_city_latitude_" + i + "_value_3'><input type='hidden' id='bis_city_longitude_" + i + "_value_4' name='bis_city_longitude_" + i + "_value_4'>");
                        jQuery("#bis_city_radius_" + i + "_value_1").val(city_radius["city_radius"]);
                        jQuery("#bis_city_radius_units_" + i + "_value_2").val(city_radius["radius_unit"]);
                        jQuery("#bis_city_latitude_" + i + "_value_3").val(city_radius["latitude"]);
                        jQuery("#bis_city_longitude_" + i + "_value_4").val(city_radius["longitude"]);
                        jQuery("#bis_city_radius_div").show();
                    } 
                    if (conditionId !== 15) {
                        jQuery("#bis_city_radius_" + i + "_value_1").remove();
                        jQuery("#bis_city_radius_units_" + i + "_value_2").remove();
                        jQuery("#bis_city_latitude_" + i + "_value_3").remove();
                        jQuery("#bis_city_longitude_" + i + "_value_4").remove();
                        jQuery("#bis_city_radius_div").hide();
                        jQuery("#bis_re_city_lng").val('');
                        jQuery("#bis_re_city_lat").val('');
                    }
                    break;

                case 2: // Select Box
                    jQuery(rule_value).multiselect();
                    break;

                case 3:// Date

                    // Default only date
                    tpicker = false;
                    dformat = 'Y-m-d';
                    dpicker = true;

                    if (subOptId === 14) { // Date & Time
                        tpicker = true;
                        dformat = 'Y-m-d H:i';
                    } else if (subOptId === 10) {  // Only Time
                        dpicker = false;
                        dformat = 'H:i';
                        tpicker = true;
                    }

                    jQuery(rule_value).datetimepicker({
                        timepicker: tpicker,
                        format: dformat,
                        datepicker: dpicker,
                        closeOnDateSelect: true,
                        mask: true
                    });

                    break;
            }
        }

        var options = {
            success: showResponse,
            url: BISAjax.ajaxurl,
            beforeSubmit: bis_validateAddRulesForm,

            data: {
                action: 'bis_re_update_rule_new',
                bis_nonce: BISAjax.bis_rules_engine_nonce
            }
        };

        jQuery('#bis-editlogicalruleform').ajaxForm(options);

        function showResponse(responseText, statusText, xhr, $form) {
             if (responseText["status"] === "success") {
                var callback = jQuery("#bis_child_list_callback").val();
                window[callback]();
            } else {
                if (responseText["status"] === "error") {
                    if (responseText["message_key"] === "duplicate_entry") {
                        bis_showErrorMessage('<?php _e("Duplicate rule name, Name should be unique.", BIS_RULES_ENGINE_TEXT_DOMAIN);?>');
                    } else if (responseText["message_key"] === "no_method_found") {
                        bis_showErrorMessage('<?php _e("Action hook method does not exist, Please define method with name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'
                        +" \""+ jQuery("#bis_re_hook").val() + "\".");
                    } else {
                        bis_showErrorMessage('<?php _e("Error occurred while updating rule.", BIS_RULES_ENGINE_TEXT_DOMAIN);?>');
                    }
                }
            }
        }

        jQuery("#bis_re_show_logical_rules").click(function () {
            bis_showLogicalRulesList();
        });
        jQuery(".bis_re_condition_span").change(function(){
            bis_tokenInput(this);
        });
        
        jQuery("#bis_city_radius_popup").click(function () {
           bis_cities_popup();
        });
        jQuery("#bis_re_mi_km").change(function(){
            bis_radius_unit();
        });
        
        //bis_radius_unit_changer('bis_re_mi_km','<?php //echo $bis_radius_unit ?>');
    });
</script>