<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use bis\repf\common\RulesEngineCacheWrapper;

if (isset($_POST['bis_import_submit'])) {
    $file_name = $_FILES['bis_import_file']['name'];
    $info = new SplFileInfo($file_name);
    $status = 'fail';
    $message = "Don't upload empty file";

    // Check if the file is csv
    if ($info->getExtension() === 'csv') {
        $f = $_FILES['bis_import_file']['tmp_name'];
        $fp = fopen($f, 'r');
        $remove_header = true;
        $letter = chr(rand(97, 122));
        $database_status = 'fail';

        global $wpdb;
        while ($r = fgetcsv($fp)) {
           $database_status = 'fail';
            if ($remove_header === true) {
                $remove_header = false;
                continue;
            }

            // Logical Rules
            $table_prefix = $wpdb->prefix;
            $table = $table_prefix . "bis_re_logical_rules";
            $i = 1;

            $ruleName = $r[$i] . "Product";
            $description = $r[$i + 1];
            $site_id = get_current_blog_id();
            $data = array(
                'name' => $ruleName,
                'description' => $r[$i++],
                'site_id' => $site_id,
                'add_rule_type' => 1,
            );
            $status = $wpdb->insert($table, $data, array("%s", "%s", "%d", "%d"));
         
            // Logical Rules Criteria
            if ($status != false) {
                $logical_rules_id = $wpdb->insert_id;
                $table = $table_prefix . "bis_re_logical_rules_criteria";
                $data = array(
                    'option_id' => 7,
                    'sub_option_id' => 9,
                    'condition_id' => 6,
                    'value' => '2019-02-01',
                    'logical_rule_id' => $logical_rules_id
                );
          

            $status = $wpdb->insert($table, $data, array("%d", "%d", "%d", "%s", "%d"));

            // Rule details
            $products = "[".$r[5]."]";

                if ($status != false) {
                    $table = $table_prefix . "bis_re_rule_details";
                    $data = array(
                        'name' => $r[0],
                        'description' => $r[1],
                        'action' => $r[4],
                        'rule_type_id' => 8,
                        'logical_rule_id' => $logical_rules_id,
                        'parent_type_value' => 'product',
                        'general_col1' => $r[6],
                        'general_col2' => $r[7],
                        'general_col3' => $products
                    );
                    $status = $wpdb->insert($table, $data, array("%s", "%s", "%s", "%d", "%d", "%s", "%s", "%s"));


                    // BIS Rules
                    if ($status != false) {
                        $rule_detail_id = $wpdb->insert_id;
                        $table = $table_prefix . "bis_re_rules";
                        $rules_details_id = $wpdb->insert_id;
                        $data = array(
                            'parent_id' => $r[3],
                            'rule_details_id' => $rule_detail_id,
                        );
                        $status = $wpdb->insert($table, $data, array("%d", "%d"));
                        
                        if ($status != false) {
                            $database_status = 'success';
                        }
                    }
                }
            }

            if ($database_status === 'fail') {
                $status = 'fail';
                $message = "There is error while importing rules";
                break;
            }
        }
        if ($database_status === 'success') {
            $status = 'success';
            $message = "Product Rules imported successfully";
        }
    } else { // End of valid csv file check
        $status = 'fail';
        $message = "Please upload the vaild CSV file ";
    }
    $import_data = array("status" => $status, "message" => $message);
    RulesEngineCacheWrapper::set_session_attribute(BIS_IMPORT_RULE_SATUS, $import_data);
}
?>
<html>
    <head></head>
    <body>
        <form method="post"  enctype="multipart/form-data">
            <div class="col-md-8">
                <input type="file" name="bis_import_file" id="bis_import_file">
            </div>
            <div class="col-md-4" style="margin-top: 6px;">
                <input type="submit" name="bis_import_submit" id="bis_import_submit" class="button button-primary" value="Import rules"   disabled>
            </div>
        </form>
        <style> 
            input#bis_import_file {
                border: 1px solid #ccc;
                box-sizing: border-box;
                padding: 6px;
                border-radius: 4px;
                width: 74%;
            }
        </style> 
    </body>
</html>	
<script>

    jQuery(document).ready(function () {
        jQuery('#bis_import_file').live('click', function () {
            if (jQuery('#bis_import_file').val()) {
                jQuery('#bis_import_submit').attr('disabled', true);
            } else {
                jQuery('#bis_import_submit').attr('disabled', false);
            }
        });
    });

</script>

