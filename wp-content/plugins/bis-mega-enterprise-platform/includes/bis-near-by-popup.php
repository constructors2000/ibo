<div  id="PopUp-config" class="modal-dialog" style="">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"><?php _e("Near by cites", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h4>
            <span id="bis_re_nearby_close" class="close"  style="position:absolute;top:16px;right:13px;font-size:20px;">&times;</span>
        </div>
        <div class="modal-body dashboard-panel bg-white bis-nearby-content-body1">
            <div class="row">
                <div class="form-group col-md-3">
                    <label><?php _e("City", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                    <div class="input_text">
                        <span id="bis_re_near_box_span"></span>

                    </div>    
                </div>
            </div>
            <div class="row">    
                <div class="form-group col-md-3">
                    <label><?php _e("Radius", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                    <div class="input_text">
                        <input type="text" class="form-control bis-near-radius" value="" maxlength="50" id="bis_nearby_radius" name="bis_nearby_radius" placeholder="Enter radius">
                    </div>
                    <div class="can-toggle clearfix can-toggle--size-small mt-10 bis_re_mi_km" style="height:28px !important;">
                        <input id="bis_re_mi_km_popup" name="bis_re_mi_km_popup" checked type="checkbox" value="mi">
                        <label for="bis_re_mi_km_popup">
                            <div class="can-toggle__switch toggle_switch_bgclr" data-checked="mi" data-unchecked="km"></div>
                        </label>
                    </div>
                </div> 
                <div class="form-group col-md-3">
                    <div style="padding-top: 25px">
                        <button type="button" id="bis_near_go_button" class="btn btn-primary bis-nearby-go-button">
                            <?php _e("Go", "rulesengine"); ?>
                        </button>                                          
                    </div>  
                </div>   
            </div>    
            <div class="row" id="bis_re_cities_parent_div">                           
                <div class="form-group col-md-12" id="bis_re_cities_list_div">
                    <div class="loader"></div>
                    <div id="bis_re_cities_list">
                    </div>
                </div>
                <div class="push-down"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" id="bis_near_by_popup_close" class="btn btn-primary savesettings" data-dismiss="modal">Close</button>
        </div>
    </div>

</div>
<script>

    jQuery(document).ready(function () {
        
        jQuery("#bis_near_go_button").click(function () {
            
            var bis_re_city_radius = jQuery("#bis_nearby_radius").val();
            var bis_longitude = jQuery("#bis_re_city_lng").val();
            var bis_latitude = jQuery("#bis_re_city_lat").val();
            var bis_radius_unit = jQuery("#bis_re_mi_km_popup").val();
            bis_cities_table_list(bis_re_city_radius, bis_longitude, bis_latitude, bis_radius_unit);
        });
        
        jQuery("#bis_re_nearby_close").click(function () {
            jQuery("#bis_near_by_popup").hide();
        });
        
        jQuery("#bis_near_by_popup_close").click(function () {
            jQuery("#bis_near_by_popup").hide();
        });
        
        jQuery("#bis_re_mi_km_popup").change(function(){
            if (jQuery("#bis_re_mi_km_popup").is(':checked')) {
                jQuery("#bis_re_mi_km_popup").val('mi');
            } else {
                jQuery("#bis_re_mi_km_popup").val('km');
            }
        });
    });
</script>