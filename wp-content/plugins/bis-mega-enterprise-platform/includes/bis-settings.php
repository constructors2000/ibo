<?php
/*$bis_capture_analytics = RulesEngineUtil::get_option(BIS_CAPTURE_ANALYTICS_DATA);

$bis_re_enable_analytics = '';

if ($bis_capture_analytics === "true") {
    $bis_re_enable_analytics = "checked";
  } */

use bis\repf\common\RulesEngineCacheWrapper;

$bis_progress_bar_color = RulesEngineUtil::get_option(BIS_NPROGRESS_BAR_COLOR_CONST, false, false);
$bis_cache_installed = RulesEngineUtil::get_option(BIS_RULES_ENGINE_CACHE_INSTALLED, false, false);
$bis_rules_debug_switch = RulesEngineUtil::get_option(BIS_RULES_DEBUG_MODE_CONT, false, false);
$bis_rules_evaluation_switch = RulesEngineUtil::get_option(BIS_RULES_EVALUATION_CONT, false, false);
$bis_country_dropdown_switch = RulesEngineUtil::get_option(BIS_MEGA_COUNTRY_DROPDOWN, false, false);
$bis_sel_ips = RulesEngineUtil::get_option(BIS_MEGA_SEL_IPS, false, false);
$bis_black_ips = RulesEngineUtil::get_option(BIS_MEGA_BLACK_IPS, false, false);
$bis_black_red = RulesEngineUtil::get_option(BIS_MEGA_BLACK_RED, false, false);

$mail_data_str = RulesEngineUtil::get_option(BIS_POPUP_MAIL_INTEGRATION_DATA, false, false);
$mail_data = json_decode($mail_data_str);
$intg_list = "";
$saved_intg_list = "";
if (isset($mail_data->apikey)) { 
    $api_key = $mail_data->apikey;
    $saved_intg_list = $mail_data->maillist;
    $list_data = RulesEngineUtil::bis_get_curl_data($api_key);
    if($list_data['status'] === "success"){
        $intg_list = $list_data['data']->lists;
    }
} else {
    $api_key = "";
}

$bis_cache_enable = "";
$bis_rules_debug_switch_enable = "";
$bis_country_dropdown = "";
if ($bis_cache_installed == "true") {
    $bis_cache_enable = "checked=\"checked\"";
}

if ($bis_rules_debug_switch == "Yes") {
    $bis_rules_debug_switch_enable = "checked=\"checked\"";
}

if ($bis_country_dropdown_switch == "Yes") {
    $bis_country_dropdown = "checked=\"checked\"";
}
?>
<div class="row">
    <div id="bis_migrate_std_to_query">
        <div class="col-sm-12">
            <div class="alert alert-success alert-dismissable bis-message-position mega-custom-alert" 
                 id="bis_std_query_banner">
                <button class="close" id="std_query_banner_close" type="button"
                        aria-hidden="true">&times;</button>
                <strong><span id="std_query_banner_open"> </span></strong>
            </div>
            <div class="alert alert-success alert-dismissable bis-message-position mega-custom-alert" 
                 id="bis_import_rules_status">
                <button class="close" id="bis_import_rules_status_close" type="button"
                        aria-hidden="true">&times;</button>
                <strong><span id="bis_import_rules_status_open"></span></strong>
            </div>
            <div class="alert alert-warning alert-dismissable bis-message-position mega-custom-alert" 
                 id="bis_import_rules_error" style="display:none">
                <button class="close" id="bis_import_rules_error_close" type="button"
                        aria-hidden="true">&times;</button>
                <strong><span id="bis_import_rules_error_open"></span></strong>
            </div>
            
            <div class="alert alert-success alert-dismissable bis-message-position mega-custom-alert" 
                 id="bis_clear_cache_banner">
                <button class="close" id="clear_cache_banner_close" type="button"
                        aria-hidden="true">&times;</button>
                <strong><span id="clear_cache_banner_open"> </span></strong>
            </div> 
            <div class="alert alert-success alert-dismissable bis-message-position mega-custom-alert" 
                 id="bis_delete_rules_banner" >
                <button class="close" id="bis_delete_banner_close" type="button"
                        aria-hidden="true">&times;</button>
                <strong><span id="bis_delete_banner_open"> </span></strong>
            </div>
            <div class="alert alert-success alert-dismissable bis-message-position mega-custom-alert" 
                 id="bis_delete_rules_engine_platform_banner" >
                <button class="close" id="bis_delete_rules_engine_platform_banner_close" type="button"
                        aria-hidden="true">&times;</button>
                <strong><span id="bis_delete_rules_engin_platform_banner_open"> </span></strong>
            </div>
            <div class="alert alert-success alert-dismissable bis-message-position mega-custom-alert" 
                 id="bis_save_caches_debugs_nprc_banner" >
                <button class="close" id="bis_save_caches_debugs_nprc_banner_close" type="button"
                        aria-hidden="true">&times;</button>
                <strong><span id="bis_save_caches_debugs_nprc_banner_open"> </span></strong>
            </div>
            <div class="alert alert-success alert-dismissable bis-message-position mega-custom-alert" 
                 id="bis_save_mail_integration_banner" >
                <button class="close" id="bis_save_mail_integration_close" type="button"
                        aria-hidden="true">&times;</button>
                <strong><span id="bis_save_mail_integration_banner_open"> </span></strong>
            </div>
        </div>
        <!--<div class="loader"></div>
        <div class="col-sm-4">
            <div class="mega-clear-box">
                <h5 class="mega-clear-box-tittle"><?php _e("Migrate Standard Rule to QueryBuilder Rule", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h5>
        
                <div id="bis_cache_clear">
                    <h5><?php _e("Click on the button to convert all standard rules to querybuilder rules", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h5>
                </div>
                <div class="bis-verify-lic-div">
                    <button onclick="bis_std_to_query()" class="btn btn-primary pcode-button">
                        <i class ="glyphicon glyphicon-retweet bis-glyphi-position"></i><?php _e("Migrate", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                    </button>
                </div> 
            </div> 
        </div>-->
        <div class="col-sm-12">
            <div class="mega-clear-box">
                <h5 class="mega-clear-box-tittle"><?php _e("Site configurations", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h5>
                <div class="can-toggle clearfix can-toggle--size-small mt-10">
                    <input <?php echo $bis_cache_enable ?> id="bis_cache_enable_switch" type="checkbox">
                    <label for="bis_cache_enable_switch">
                        <div class="can-toggle__switch" data-checked="Yes" data-unchecked="No"></div>
                        <div class="can-toggle__label-text"><?php _e("Cache enabled for this site.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></div>
                    </label>
                </div>
                <!--<div class="can-toggle clearfix can-toggle--size-small mt-10">
                    <input <?php //echo $bis_rules_evaluation_switch_enable     ?> id="bis_rules_evaluation_switch" type="checkbox">
                    <label for="bis_rules_evaluation_switch">
                        <div class="can-toggle__switch" data-checked="Yes" data-unchecked="No"></div>
                        <div class="can-toggle__label-text"><?php _e("Enable client side rules evaluation.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></div>
                    </label>
                </div>-->
                <div class="can-toggle clearfix can-toggle--size-small mt-10">
                    <input <?php echo $bis_rules_debug_switch_enable ?> id="bis_rules_debug_switch" type="checkbox">                   
                    <label for="bis_rules_debug_switch">
                        <div class="can-toggle__switch" data-checked="Yes" data-unchecked="No"></div>
                        <div class="can-toggle__label-text"><?php _e("Enable debug mode.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></div>
                    </label>
                    <span class="bis-logical-rule-status-short-code"><?php _e("Add [bis_logical_rules_status] shortcode to a page to know the status of the defined rules.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></span>
                </div>
                <div class="can-toggle clearfix can-toggle--size-small" style="margin-top: -10px;">
                    <input <?php echo $bis_country_dropdown ?> id="bis_country_dropdown_switch" type="checkbox">
                    <label for="bis_country_dropdown_switch">
                        <div class="can-toggle__switch" data-checked="Yes" data-unchecked="No"></div>
                        <div class="can-toggle__label-text"><?php _e("Enable for Mega shortcodes.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></div>
                    </label>
                </div>
                <!--<div class="can-toggle clearfix can-toggle--size-small mt-10">
                    <input value='<?php //echo $bis_progress_bar_color;     ?>' class="form-control" type="color" id="bis_customize_nprg_color"
                           style="height: 28px; width: 90px; border-radius: 5px; border-color: #737cdd" placeholder='<?php _e("Enter progress bar color (ex: #80EEEE).", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'>
                    <label for="bis_customize_nprg_color">
                        <div class="can-toggle__label-text" style="padding-left: 105px; margin-top: -30px"><?php _e("Customize progress bar color.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></div>
                    </label>
                </div>-->
                <div class="bis-verify-lic-div">
                    <button onclick="save_configurations()" class="btn btn-primary pcode-button mega-clear-box-button-top-margin">
                        <i class="glyphicon glyphicon-ok-sign bis-glyphi-position"></i><?php _e("Save", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                    </button>
                </div> 
            </div>
        </div>
        <div class="col-sm-6">
            <div class="mega-clear-box">
                <h5 class="mega-clear-box-tittle"><?php _e("Clear Transient Cache", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h5>
            <div id="bis_cache_clear">
                <h5><?php _e("Click on the button to clear the cache", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h5>
            </div>
            <div class="bis-verify-lic-div">
                <button onclick="bis_clear_cache()" class="btn btn-primary pcode-button mega-clear-box-button-top-margin">
                    <i class="glyphicon glyphicon-trash bis-glyphi-position"></i><?php _e("Clear Cache", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                </button>
            </div> 
            </div> 
        </div>
        <div class="col-sm-6">
            <div class="mega-clear-box">
                <h5 class="mega-clear-box-tittle"><?php _e("Delete all rules from DB", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h5>
                <div id="bis_delete_rules_status">
                    <h5><?php _e("Click on the button to delete all rules", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h5>
                </div>
                <div class="bis-verify-lic-div">
                    <button onclick="bis_delete_rules()" class="btn btn-primary pcode-button mega-clear-box-button-top-margin">
                        <i class="glyphicon glyphicon-trash bis-glyphi-position"></i><?php _e("Delete Rules", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                    </button>
                </div> 
            </div>
        </div>
        
        <div class="col-sm-12">
            <div class="mega-clear-box clearfix">
                <h5 class="mega-clear-box-tittle"><?php _e("Mailing list configuration", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h5>
                <div class="col-md-12 clearfix" style="padding-bottom:15px;">
                    <div class="col-md-5">
                            Select integrator:
                    </div> 
                    <div class="col-md-7">
                        <select name="bis_select_mail_integrator" id="bis_select_mail_integrator">
                           <option value="mailchimp">Mailchimp</option>
                        </select>
                    </div>    
                </div>
                <div class="col-md-12 clearfix" style="padding-bottom:15px;">
                    <div class="col-md-5"> 
                        API Key :
                    </div>    
                    <div class="col-md-7">
                        <input type="text" id="bis_mail_integrator_api_key" name="bis_mail_integrator_api_key" value="<?php echo $api_key; ?>">&nbsp;&nbsp;
                        <a href="https://mailchimp.com/help/about-api-keys/" target="blank">Where can i find my api key?</a>
                    </div>
                </div>
                <div class="col-md-12 clearfix" style="padding-bottom:15px;">
                    <div class="col-md-5">Mail List:</div>
                        <div class="col-md-7">
                            <select name="bis_integrator_ist" id="bis_integrator_ist">
                                <?php
                                    if(is_array($intg_list)){
                                        foreach ($intg_list as $list) { ?>
                                        <option value="<?php echo $list->id; ?>"><?php echo $list->name; ?></option>
                                        <?php }
                                    }
                                ?>
                            </select>
                            <input class="btn btn-primary" type="button" id="bis_get_mailing_list" value="Get List">
                        </div>   
                </div>
                <div class="bis-verify-lic-div">
                    <button onclick="save_mail_integrtion_sett()" class="btn btn-primary pcsave_mail_integrtion_settode-button mega-clear-box-button-top-margin">
                        <i class="glyphicon glyphicon-ok-sign bis-glyphi-position"></i><?php _e("Save Mail integrator", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                    </button>
                </div> 
                
            </div>
        </div>
        
        <?php if (class_exists('BIS_Redirect_Controller')) { ?>
        <div class="col-sm-6">
            <div class="mega-clear-box">
                <h5 class="mega-clear-box-tittle"><?php _e("Whitelist IP Address", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h5>
                <div id="bis_whitelist_div">
                    <label for="bis_wlist_selected_ips">
                        <h5 style="font-weight: bold"><?php _e("IP Address", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h5>
                    </label>
                    <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                    <a class="popoverData" target="_blank" href="<?php echo BIS_RULES_ENGINE ?>"
                       data-content='<?php _e("Enter whitelist ip with comma(,) seperated.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>' rel="popover"
                       data-placement="bottom" data-trigger="hover">
                        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                    </a>
                    <input class="form-control" id="bis_wlist_selected_ips" value="<?php echo $bis_sel_ips; ?>" type="text" placeholder="<?php _e("Enter whitelist ip with comma(,) seperated.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">
                </div>
                <div class="bis-verify-lic-div">
                    <button onclick="bis_whitelist_ips()" class="btn btn-primary pcode-button mega-clear-box-button-top-margin">
                        <i class="glyphicon glyphicon-ok-sign bis-glyphi-position"></i><?php _e("Save", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                    </button>
                </div> 
            </div> 
        </div>
        <div class="col-sm-6">
            <div class="mega-clear-box">
                <h5 class="mega-clear-box-tittle"><?php _e("Blacklist IP Address", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h5>
                <div id="bis_blacklist_div">
                    <label for="bis_blist_selected_ips">
                        <h5 style="font-weight: bold"><?php _e("IP Address", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h5>
                    </label>
                    <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                    <a class="popoverData" target="_blank" href="<?php echo BIS_RULES_ENGINE ?>"
                       data-content='<?php _e("Enter blacklist ip with comma(,) seperated.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>' rel="popover"
                       data-placement="bottom" data-trigger="hover">
                        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                    </a>
                    <input class="form-control" id="bis_blist_selected_ips" value="<?php echo $bis_black_ips; ?>" type="text" placeholder="<?php _e("Enter blacklist ip with comma(,) seperated.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">
                    <label for="bis_blist_red_url">
                        <h5 style="font-weight: bold"><?php _e("Redirect to", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h5>
                    </label>
                    <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                    <a class="popoverData" target="_blank" href="<?php echo BIS_RULES_ENGINE ?>"
                       data-content='<?php _e("Redirect users to this url from the above blacklist ip address.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>' rel="popover"
                       data-placement="bottom" data-trigger="hover">
                        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                    </a>
                    <input class="form-control" id="bis_blist_selected_ips_red" value="<?php echo $bis_black_red; ?>" type="text" placeholder="<?php _e("Enter redirect url.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">
                </div>
                <div class="bis-verify-lic-div">
                    <button onclick="bis_blacklist_ips()" class="btn btn-primary pcode-button mega-clear-box-button-top-margin">
                        <i class="glyphicon glyphicon-ok-sign bis-glyphi-position"></i><?php _e("Save", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                    </button>
                </div> 
            </div> 
        </div>
        <?php } ?>
        <div class="col-md-6">
            <div class="mega-clear-box">
                <h5 class="mega-clear-box-tittle"><?php _e("Import Product Rules", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h5> 
                <div class="row" style="margin-top: 6px;">
            <?php include('bis-product-import.php'); ?>
    </div> 
</div>
            <?php
            $import_data = RulesEngineCacheWrapper::get_session_attribute(BIS_IMPORT_RULE_SATUS);
            RulesEngineCacheWrapper::remove_session_attribute(BIS_IMPORT_RULE_SATUS);
            $import_rule_status = $import_data['status'];
            $import_message = $import_data['message'];
            ?>
           </div>
    </div>
</div> 

<script>
    jQuery(document).ready(function () {
        
        /*jQuery("#bis_rules_evaluation_switch").click(function(){
            if (jQuery("#bis_rules_evaluation_switch").is(':checked')) {
                jQuery('#bis_rules_evaluation_switch').attr('checked', true);
                jQuery('#bis_cache_enable_switch').attr('checked', false); 
            }
        });*/
        jQuery("#bis_import_rules_error").hide();
        jQuery('#bis_active_plugins').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            maxHeight: 300,
            selectedClass: null,
            buttonWidth: '250px'
        });
        jQuery('#bis_active_plugins').live('change', function () {
            jQuery('#export_active_plugin').val(jQuery('#bis_active_plugins').val());
        });
       
        var status_set = null;
        var import_status = '<?php echo $import_rule_status; ?>';
        var import_message = "";
      
        if (import_status === 'fail' && import_status != null) {
            import_message = "<?php echo $import_message; ?>";
            status_set = false;
        }
        if (import_status === 'success' && import_status != null) {
            import_message = "<?php echo $import_message; ?>";
            status_set = true;
        }
        if (status_set === true && import_status != null) {
            jQuery("#bis_import_rules_status_open").text(import_message)
            jQuery("#bis_import_rules_status").show();
        }    
        
        if (status_set === false && import_status != null) {
            jQuery("#bis_import_rules_error_open").text(import_message)
            jQuery("#bis_import_rules_error").show();
        }
        
        jQuery('#bis_import_rules_status_close').click(function () {
            jQuery('#bis_import_rules_status').hide();
        });
        
        jQuery('#bis_import_rules_error_close').click(function () {
            jQuery('#bis_import_rules_error').hide();
        });
        jQuery("#bis_cache_enable_switch").click(function () {
            if (jQuery("#bis_cache_enable_switch").is(':checked')) {
                //jQuery('#bis_rules_evaluation_switch').attr('checked', false);
                jQuery('#bis_cache_enable_switch').attr('checked', true); 
            } else {
                jQuery('#bis_cache_enable_switch').attr('checked', false); 
            }
        });
        
        jQuery("#bis_get_mailing_list").click(function(){
            var apiKey = jQuery('#bis_mail_integrator_api_key').val();
            var jqXHR = jQuery.post(
                BISAjax.ajaxurl,
                {
                    action: 'bis_get_mailing_list',
                    apiKey: apiKey
                });
            jqXHR.done(function (response) {
                if(response['status'] === "success"){
                    jQuery.each(response['data']['lists'], function(key, val){
                        jQuery('#bis_integrator_ist').append(jQuery('<option>', {value: val.id}).text(val.name));
                    });
                }
            });
        });
        jQuery("#bis_integrator_ist").val("<?php echo $saved_intg_list ?>");
    });
</script>