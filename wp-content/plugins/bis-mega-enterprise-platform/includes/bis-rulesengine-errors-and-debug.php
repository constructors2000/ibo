<div class="panel panel-default">
    <div class=" panel-heading dashboard-panel-heading">
        <div>
            <div class="row">
                <div class="col-md-4">
                    <h3 class="panel-title"><label><?php _e("Site Settings", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label></h3>
                </div>
                <div class="col-md-8">
                    <strong class="panel-help"><label style="float:right;"><a id="bis_help_url" target="_blank" href="http://docs.megaedzee.com/docs/mega-enterprise-platform/site-settings/">Help &nbsp;<span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label></strong>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body dashboard-panel">
        <ul role="tablist" class="nav nav-tabs" id="myTabs">
            <li class="active" role="presentation">
                <a aria-expanded="false" aria-controls="clearcache_deleterule" data-toggle="tab" 
                   role="tab" id="clearcache_deleterule-tab" href="#clearcache_deleterule"><?php _e("Configuration", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></a>
            </li>
            <li class="deactive" role="presentation">
                <a aria-expanded="false" aria-controls="trouble-shoot" data-toggle="tab" 
                   role="tab" id="trouble-shoot-tab" href="#trouble-shoot"><?php _e("Trouble Shoot", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></a>
            </li>  
            <li class="deactive" role="presentation">
                <a aria-expanded="false" aria-controls="mega-popups" data-toggle="tab" 
                   role="tab" id="mega-popups-tab" href="#mega-popups"><?php _e("Site Popup", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></a>
            </li>  
        </ul>
        <div class="tab-content" id="myTabContent" style="margin-top: 20px;">
            <div aria-labelledby="clearcache_deleterule-tab" id="clearcache_deleterule" 
                 class="tab-pane fade in active" role="tabpanel">
                     <?php include 'bis-settings.php'; ?>
            </div>
            <div aria-labelledby="trouble-shoot-tab" id="trouble-shoot" 
                 class="tab-pane fade" role="tabpanel">
                <div class="row" style="margin-top: 20px;">
                    <?php include 'bis-troubleshoot.php'; ?>
                </div>
            </div>
            <div aria-labelledby="mega-popups-tab" id="mega-popups" 
                 class="tab-pane fade" role="tabpanel">
                <div class="row" style="margin-top: 20px;">
                    <?php include 'bis-mega-popups-configuration.php'; ?>
                </div>
            </div>
        </div>              
    </div>
</div>    