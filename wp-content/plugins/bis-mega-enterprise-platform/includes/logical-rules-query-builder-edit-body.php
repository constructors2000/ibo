<?php

use bis\repf\common\RulesEngineCacheWrapper;
use bis\repf\model\QueryBuilderModel;

$logical_rule_engine_model = new bis\repf\model\LogicalRulesEngineModel();

$ruleId = RulesEngineCacheWrapper::get_session_attribute(BIS_LOGICAL_RULE_ID);

// Get the rule details.
$bis_rule = $logical_rule_engine_model->get_rule($ruleId);

// Ge the rule details
$rule = $bis_rule["rule"];

// Get the criteria array from map
$rule_criteria_array = $bis_rule["rule_criteria"];
$querybuilder_model = new QueryBuilderModel();
$query_rule = $querybuilder_model->get_query_builder_rule($ruleId);

/*$bis_longitude = '';
$bis_latitude = '';
$bis_city_radius = '20';
$bis_radius_unit = 'mi';

if ($query_rule[0]->near_by_cities != null) {
    $near_by_cities = json_decode($query_rule[0]->near_by_cities);
    $bis_longitude = $near_by_cities->longitude;
    $bis_latitude = $near_by_cities->latitude;
    $bis_city_radius = $near_by_cities->city_radius;
    $bis_radius_unit = $near_by_cities->radius_unit;
}*/
$plugin_type_value = RulesEngineCacheWrapper::get_session_attribute(BIS_PLUGIN_TYPE_VALUE);
//RulesEngineCacheWrapper::remove_session_attribute(BIS_PLUGIN_TYPE_VALUE);
?>
    <input type="hidden" name="bis_re_rId" id="bis_re_rId" value='<?php echo $rule->rId; ?>'/>
    <div id="bis-rule-builder"></div>
    <div class="query-builder-change-rule" id="bis_city_radius_div" style="display: none">
        <div class="form-group bis-city-radius-popup" style="position: relative; top: 5px; font-size: 18px;">
            <a href="javascript:void(0)" id="bis_city_radius_popup"><i class="fa fa-hand-o-right" style="padding-right: 10px;" aria-hidden="true"></i><?php _e("Check near by cities.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></a>
        </div>
    </div>
    <!--<div class="query-builder-change-rule" id="bis_city_radius_div" style="display: none">
        <div class="panel-body post-rules-container-edit dashboard-panel">
            <div>
                <div class="row">
                    <div class="form-group col-md-2">
                        <label><?php _e("City Radius", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                        <a class="popoverData" target="_blank" href="<?php //echo BIS_RULES_ENGINE_SITE ?>"
                           data-content="<?php _e("City radius will get cities within specified radius.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>"
                           rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                        <input type="text" class="form-control bis-near-radius"
                               id="bis_re_city_radius" name="bis_re_city_radius"
                               value="20">
                        <div class="can-toggle clearfix can-toggle--size-small mt-10 bis_re_mi_km">
                            <input id="bis_re_mi_km" name="bis_re_mi_km" checked type="checkbox">
                            <label for="bis_re_mi_km">
                                <div class="can-toggle__switch toggle_switch_bgclr" data-checked="mi" data-unchecked="km"></div>
                            </label>
                        </div>
                    </div>
                    <div class="form-group bis-city-radius-popup col-md-2" style="position: relative; top: 25px; font-size: 19px;">
                        <a href="javascript:void(0)" id="bis_city_radius_popup"><i class="fa fa-hand-o-right" aria-hidden="true"><?php _e("Near Cities", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group bis-city-radius-popup" style="position: relative; top: 5px; font-size: 18px;">
            <a href="javascript:void(0)" id="bis_city_radius_popup"><i class="fa fa-hand-o-right" style="padding-right: 10px;" aria-hidden="true"></i><?php _e("Check near by cities.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></a>
        </div>
    </div>-->
    <div class="panel panel-default">
        <div class="panel-body post-rules-container-edit dashboard-panel">
            <div >
                <div class="row">
                    <div class="form-group col-md-2 pr-0">
                        <label><?php _e("Evaluation Type", "rulesengine"); ?></label>
                        <a class="popoverData" target="_blank" href=""
                           data-content='<?php
                           _e("Evaluation Type is Atleast Once means even if the rule is satisfied atleast once, "
                                   . "the rule will be considered as satisfied for the entire session. Useful for Requested URL subcategory. "
                                   . "Default is Always.", "rulesengine");
                           ?>'
                           rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                        <div>
                            <select class="form-control bis-multiselect" name="bis_re_eval_type" 
                                    id="bis_re_eval_type" style="width:20%">
                                <?php
                                if ($rule->eval_type == 1) {
                                    ?>
                                    <option selected="selected" value="1"><?php _e("Always", "rulesengine"); ?></option>
                                    <option value="2"><?php _e("Atleast Once", "rulesengine"); ?></option>
                                <?php } else {
                                    ?>
                                    <option value="1"><?php _e("Always", "rulesengine"); ?></option>
                                    <option selected="selected" value="2"><?php _e("Atleast Once", "rulesengine"); ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-5 pl-0">
                        <label><?php _e("Action Hook", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                        <a class="popoverData" target="_blank" href="<?php echo BIS_RULES_ENGINE_SITE ?>"
                           data-content="<?php _e("Hook will be called if rule criteria is satisfied. Before entering action hook
                               please define a function with action hook.Action hook should not contain any spaces.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>"
                           rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                        <input type="text" class="form-control"
                               id="bis_re_hook" name="bis_re_hook"
                               value="<?php echo $rule->action_hook ?>"
                               placeholder="<?php _e("Enter action hook", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">
                    </div>
                    <div class="form-group col-md-3 pl-0 bis-postcode-csv-file" style="display:none">
                        <label><?php _e("Upload post/zip code csv", BIS_RULES_ENGINE_TEXT_DOMAIN); ?> </label>
                        <a class="popoverData" target="_blank" href="<?php echo BIS_RULES_ENGINE_SITE ?>"
                           data-content='<?php _e("If you have a post/zip code in a .csv format, you may add it by uploading it here..", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'
                           rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                        <input type="file" class="form-control mega-file-upload" id="bis_re_post_code_file" name="bis_re_post_code_file">

                    </div>
                    <div class="mega-btn-upload col-md-2 bis-postcode-csv-file" style="display:none">
                        <button id="bis_re_post_code_upload" type="button" class="btn btn-primary">
                            <i class=""></i> Upload</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
    .mega-btn-upload {
        margin-top: 22px;
    }
    .mega-file-upload {
        height: 30px;
    }
    </style>
    <span id="bis_near_by_popup" style="display: none">
        <?php require_once 'bis-near-by-popup.php'; ?>
    </span>
    <input type="hidden" id="bis_jquery_result" name="bis_jquery_result" value="">
    <input type="hidden" id="bis_re_right_bracket" name="bis_re_right_bracket" value="0">
    <input type="hidden" id="bis_re_left_bracket" name="bis_re_left_bracket" value="0">
    <input type="hidden" id="bis_re_name" name="bis_re_name" value="<?php echo $rule->name ?>">
    <input type="hidden" id="bis_re_description" name="bis_re_description" value="<?php echo $rule->description ?>">
    <input type="hidden" id="bis_re_status" name="bis_re_status" value="<?php echo $rule->status ?>">
    <input type="hidden" id="bis_re_city_lat" name="bis_re_city_lat" value="">
    <input type="hidden" id="bis_re_city_lng" name="bis_re_city_lng" value="">

<script>
    
    jQuery(document).ready(function () {
        
        var plugInType = "<?php echo $plugin_type_value ?>";
        var rules_basic = <?php echo $query_rule[0]->rule_query ?> ;
        var bis_nonce = BISAjax.bis_rules_engine_nonce;
        var allFilters = null;
        var selectedValueContainer = null;
        var jqXHR = jQuery.get(
                BISAjax.ajaxurl,
                {
                    action: 'bis_get_all_query_data',
                    plugInType:plugInType,
                    bis_nonce:bis_nonce
                });
        jqXHR.done(function (respose) {
            
            allFilters = respose["data"];
            var jqueryProm = jQuery('#bis-rule-builder').queryBuilder({
                operators: jQuery.fn.queryBuilder.constructor.DEFAULTS.operators.concat([
                        { type: 'is_set',  nb_inputs: 1, multiple: false, apply_to: ['string'] },
                        { type: 'is_not_set', nb_inputs: 1, multiple: false, apply_to: ['string'] },
                        { type: 'pattern_match', nb_inputs: 1, multiple: false, apply_to: ['string'] },
                        { type: 'domain_is', nb_inputs: 1, multiple: false, apply_to: ['string'] },
                        { type: 'contains_only', nb_inputs: 1, multiple: false, apply_to: ['string'] },
                        { type: 'does_not_contains', nb_inputs: 1, multiple: false, apply_to: ['string'] },
                        { type: 'does_not_contains_any_of', nb_inputs: 1, multiple: false, apply_to: ['string'] },
                        { type: 'contains_any_of', nb_inputs: 1, multiple: false, apply_to: ['string'] },
                        { type: 'equal_to', nb_inputs: 2, multiple: false, apply_to: ['string'] },
                        { type: 'less_than', nb_inputs: 1, multiple: false, apply_to: ['string'] },
                        { type: 'greater_than', nb_inputs: 1, multiple: false, apply_to: ['string'] },
                        { type: 'near_by', nb_inputs: 5, multiple: false, apply_to: ['string'] },
                        { type: 'les_than', nb_inputs: 2, multiple: false, apply_to: ['string'] },
                        { type: 'gtr_than', nb_inputs: 2, multiple: false, apply_to: ['string'] }
                ]),
                lang: {
                    operators: {
                            is_set: 'is set',
                            is_not_set: 'is not set',
                            pattern_match: 'pattern match',
                            domain_is: 'domain is',
                            contains_only: 'contains',
                            does_not_contains: 'does not contains',
                            contains_any_of: 'contains any of',
                            does_not_contains_any_of: 'does not contains any of',
                            less_than: 'less than',
                            greater_than: 'greater than',
                            near_by: 'near by',
                            equal_to: 'equal to',
                            les_than: 'Less than',
                            gtr_than: 'Greater than',
                            }
                        },
                        plugins: ['sortable'],
                        display_empty_filter: false,
                        autocomplete:false,
                        inputs_separator: '',
                        select_placeholder: "Select criteria",
                        filters: allFilters,
                        rules: rules_basic
                    });
                
                jqueryProm.promise().done(function() {

                    if (jQuery("select[name*='_filter']").length > 0) {
                        jQuery(".rules-group-container select").multiselect({
                            nonSelectedText: '<?php _e("None Selected", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>',
                            maxHeight: 200,
                            enableCaseInsensitiveFiltering: true,
                            enableFiltering: true
                        });
                    }
                    
                    jQuery("select[name*='_value_']").multiselect('rebuild');
                    
                    var inputObj = jQuery("input[name*='_value_']");
                    var inEleLen = inputObj.length;
                     
                    for(i=0; i<inEleLen; i++) {
                   
                        var ele = inputObj[i];
                        var val = ele.value;
                        var siblings = jQuery(ele).closest("div").siblings();
                        var catsub = jQuery(siblings[3]).find("select").val().split("_");
                        var cat = catsub[0];
                        var subcat = catsub[1];
                        var valTypeId = catsub[3];
                        var ssubCriteria= catsub[4];
                        var cond = jQuery(siblings[4]).find("select").val();
                        var row_value = (ele.name).split("_");
                        var rowIndex = row_value[2];
                        
                        if(valTypeId === "1" && (cond === "does_not_contains_any_of" || cond === "contains_any_of" || cond === "equal" || cond === "not_equal" || cond === "near_by")) {
                            if(row_value[4] == "0") {
                                var jsonVal = jQuery.parseJSON(val);
                                if(typeof jsonVal == 'object'){
                                    var tokenLimit = 1;
                                    if(cond === "does_not_contains_any_of" || cond === "contains_any_of"){
                                        tokenLimit = null;
                                    }
                                    var params = "?action=bis_re_get_value&bis_nonce=" + bis_nonce + "&criteria=" + cat + "&subcriteria=" + subcat + "&ssubCriteria="+ssubCriteria+"&condition=" + cond;
                                    if (cond === "does_not_contains_any_of" || cond === "contains_any_of" || cond === "equal" || cond === "not_equal" || cond === "near_by") {
                                        var $tokenInput = jQuery(ele).tokenInput(ajaxurl + params,
                                        {
                                            theme: "facebook",
                                            prePopulate: jsonVal,
                                            minChars: 2,
                                            method: "POST",
                                            tokenLimit: tokenLimit,
                                            onAdd: function (item) {
                                                jQuery(this).val(JSON.stringify(this.tokenInput("get")));
                                                jQuery("#bis_re_city_lng").val(item.lng);
                                                jQuery("#bis_re_city_lat").val(item.lat);
                                                jQuery("#bis_city_latitude_" + rowIndex + "_value_3").val(item.lat);
                                                jQuery("#bis_city_longitude_" + rowIndex + "_value_4").val(item.lng);
                                            },
                                            onDelete: function (item) {
                                                jQuery(this).val(JSON.stringify(this.tokenInput("get")));
                                            }
                                        });
                                        $tokenInput.val(jsonVal);
                                    } else {
                                        jQuery(ele).val(jsonVal);
                                    }
                                }
                            }
                            
                        } else if(valTypeId === "4"){
                            var text;
                            switch(cond){
                                case "pattern_match" :
                                    text = 'Enter URL with /** pattern (ex: http://rulesengine.in/mypath/**)';
                                    break;
                                default:
                                    text = 'Enter a value';
                            }
                            if(subcat === "26" || subcat === "27" || (subcat === "32" && (cond === "equal" || cond === "not_equal"))){
                                text = 'Enter a value, formate name=value'
                            }
                            if(subcat === "15" && (cond === "contains_any_of" || cond === "does_not_contains_any_of")) {
                                text = 'Enter a value for Ip address, (ex:14.141.244.99 , 66.249.79.122)';
                            }
                            if(subcat === "35" && (cond === "contains_any_of" || cond === "does_not_contains_any_of")) {
                                text = 'Enter a value for zip code, (ex:500033 , 518222)';
                            }
                            jQuery(ele).attr('placeholder',text);
                            jQuery(ele).width(500);
                        }

                        if (cond === "near_by") {
                            selectedValueContainer = jsonVal;
                            jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_1]").attr('id', 'bis_city_radius_' + rowIndex + '_value_1');
                            jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_2]").attr('id', 'bis_city_radius_units_' + rowIndex + '_value_2');
                            jQuery("#bis_city_radius_" + rowIndex + "_value_1").attr('style','width: 80px; position: absolute; right: 260px; top: 5px;');
                            jQuery("#bis_city_radius_units_" + rowIndex + "_value_2").attr('style','width: 80px; position: absolute; right: 350px; top: 5px;');
                            jQuery("#bis_city_radius_" + rowIndex + "_value_1").attr('placeholder', 'Radius');
                            jQuery("#bis_city_radius_units_" + rowIndex + "_value_2").attr('placeholder', 'mi or km');
                            jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_3]").attr('id', 'bis_city_latitude_' + rowIndex + '_value_3');
                            jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_4]").attr('id', 'bis_city_longitude_' + rowIndex + '_value_4');
                            jQuery("#bis_city_latitude_" + rowIndex + "_value_3").hide();
                            jQuery("#bis_city_longitude_" + rowIndex + "_value_4").hide();
                            jQuery("#bis_city_radius_div").show();
                        }
                        if (cond !== "near_by") {
                            jQuery("#bis_city_radius_div").hide();
                            jQuery("#bis_re_city_lng").val('');
                            jQuery("#bis_re_city_lat").val('');
                        }
                        if(subcat === "46"){
                            jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_0]").attr('placeholder','Enter hr:mi Ex(24:00 or 00:30 or 01:05)');
                        }
                        if(subcat === "44"){
                            jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_1]").attr('id', 'bis_distance_units_' + rowIndex + '_value_1');
                            jQuery("#bis_distance_units_" + rowIndex + "_value_1").attr('style','width: 80px; position: absolute; right: 200px; top: 5px;');
                            jQuery("#bis_distance_units_" + rowIndex + "_value_1").attr('placeholder', 'mi or km');
                            jQuery("#bis_distance_units_" + rowIndex + "_value_1").show();
                            jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_0]").attr('style','width: 490px;');
                        } else {
                            jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_1]").hide();
                            jQuery(".drag-handle").attr('style','padding-right: 0px;');
                        }
                        
                        if (subcat === "35" && (cond === 'contains_any_of' || cond === 'does_not_contains_any_of')) {
                            jQuery('.bis-postcode-csv-file').show();
                            var pcods = '';
                            jQuery('#bis_re_post_code_upload').click(function () {
                                jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_0]").val('');
                                var csv = jQuery('#bis_re_post_code_file');
                                var csvFile = csv[0].files[0];
                                var ext = csv.val().split(".").pop().toLowerCase();
                                if (jQuery.inArray(ext, ["csv"]) === -1) {
                                    alert('upload csv');
                                    return false;
                                }
                                if (csvFile !== 'undefined') {
                                    reader = new FileReader();
                                    reader.onload = function (e) {
                                        rows = e.target.result.split(/\r|\n|\r\n/);
                                        if (rows.length > 0) {
                                            var first_Row_Cells = splitCSVtoCells(rows[0], ","); //Taking Headings
                                            var jsonArray = new Array();
                                            for (var i = 1; i < rows.length; i++) {
                                                var cells = splitCSVtoCells(rows[i], ",");
                                                var obj = {};
                                                for (var j = 0; j < cells.length; j++){
                                                    obj[first_Row_Cells[j]] = cells[j];
                                                }
                                                jsonArray.push(obj);
                                            }
                                            pcods = '';
                                            jQuery.each(jsonArray, function(key,val){
                                                if(val.hasOwnProperty('Pincode')){
                                                    var pinStr = val.Pincode;
                                                    if(pcods !== '' && pinStr !== ''){
                                                        pcods = pcods+','+pinStr;
                                                    } else if (pinStr !== '') {
                                                        pcods = pinStr;
                                                    }
                                                }
                                            });
                                            if(pcods !== ''){
                                                jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_0]").val(pcods);
                                            }
                                        }
                                    };
                                    reader.readAsText(csvFile);
                                }
                            });
                        } else {
                            jQuery('.bis-postcode-csv-file').hide();
                        }
                    }
                });
                
                jQuery(document).on('change', '.rules-group-container select', function () {
                    
                    jQuery(".rules-group-container select").multiselect({
                        nonSelectedText: '<?php _e("None Selected", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>',
                        maxHeight: 200,
                        enableCaseInsensitiveFiltering: true,
                        enableFiltering: true
                    });

                    if (this.name.indexOf("bis-rule-builder_rule_") > -1) {

                        var catsub = this.value.split("_");
                        var cat = catsub[0];
                        var subcat = catsub[1];
                        var valTypeId = catsub[3];
                        var ssubcat = catsub[4];
                        var rowIndex = this.name.split("_")[2];
                        var operatorId = "bis-rule-builder_rule_" + rowIndex + "_operator";

                        if (valTypeId !== "1") {
                            var presentFilter = jQuery("select[name=bis-rule-builder_rule_" + rowIndex + "_filter]").val();
                            var presentFilterValues = presentFilter.split("_");
                            cat = presentFilterValues[0];
                            subcat = presentFilterValues[1];
                            valTypeId = presentFilterValues[3];
                            ssubcat = presentFilterValues[4];
                        }
                        var cond = document.getElementsByName(operatorId)[0].value;
                        var valueContainer = "bis-rule-builder_rule_" + rowIndex + "_value_0";
                        var textElement = document.getElementsByName(valueContainer)[0];
                        if (valTypeId === "1"){
                            if(cond === "contains_only" || cond === "does_not_contains" || cond === "domain_is"){
                                jQuery(textElement).attr("value", "");
                                var has_class = jQuery(jQuery("input[name=bis-rule-builder_rule_"+rowIndex+"_value_0]").siblings()[0]).hasClass('token-input-list-facebook');
                            if(has_class === true){
                                var ulId ="bis_"+rowIndex+"_value_0"
                                jQuery(jQuery("input[name=bis-rule-builder_rule_"+rowIndex+"_value_0]").siblings()[0]).attr("id",ulId);
                                jQuery("#"+ulId).hide();
                            }
                                //jQuery(".token-input-list-facebook").css("display", "none");
                                jQuery("input[name="+valueContainer+"]").css("display", "block");
                                jQuery(textElement).attr('placeholder','Enter a value');
                                jQuery(textElement).width(500);
                                if(cond === "domain_is"){
                                    jQuery(textElement).attr('placeholder', 'Enter a value (ex: gmail.com (or) yahoo.com)');
                                }
                            } else {
                                addTokenInput(textElement, subcat, ssubcat, cond, rowIndex);
                            }
                        }else if(valTypeId === "4"){
                            var text;
                            switch(cond){
                                case "pattern_match" :
                                    text = 'Enter URL with /** pattern (ex: http://rulesengine.in/mypath/**)';
                                    break;
                                default:
                                    text = 'Enter a value';
                            }
                            if(subcat === "26" || subcat === "27" || (subcat === "32" && (cond === "equal" || cond === "not_equal"))){
                                text = 'Enter a value, formate name=value'
                            }
                            if(subcat === "15" && (cond === "contains_any_of" || cond === "does_not_contains_any_of")) {
                                text = 'Enter a value for Ip address, (ex:14.141.244.99 , 66.249.79.122)';
                            }
                            if(subcat === "35" && (cond === "contains_any_of" || cond === "does_not_contains_any_of")) {
                                text = 'Enter a value for zip code, (ex:500033 , 518222)';
                            }
                            jQuery(textElement).attr('placeholder',text);
                            jQuery(textElement).width(500); 
                        }
                        
                        if (cond === "near_by") {
                            selectedValueContainer = valueContainer;
                            jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_1]").attr('id', 'bis_city_radius_' + rowIndex + '_value_1');
                            jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_2]").attr('id', 'bis_city_radius_units_' + rowIndex + '_value_2');
                            jQuery("#bis_city_radius_" + rowIndex + "_value_1").attr('style','width: 80px; position: absolute; right: 260px; top: 5px;');
                            jQuery("#bis_city_radius_units_" + rowIndex + "_value_2").attr('style','width: 80px; position: absolute; right: 350px; top: 5px;');
                            jQuery("#bis_city_radius_" + rowIndex + "_value_1").attr('placeholder', 'Radius');
                            jQuery("#bis_city_radius_units_" + rowIndex + "_value_2").attr('placeholder', 'mi or km');
                            jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_3]").attr('id', 'bis_city_latitude_' + rowIndex + '_value_3');
                            jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_4]").attr('id', 'bis_city_longitude_' + rowIndex + '_value_4');
                            jQuery("#bis_city_latitude_" + rowIndex + "_value_3").hide();
                            jQuery("#bis_city_longitude_" + rowIndex + "_value_4").hide();
                            jQuery("#bis_city_radius_div").show();
                        }
                        if (cond !== "near_by") {
                            jQuery("#bis_city_radius_div").hide();
                            jQuery("#bis_re_city_lng").val('');
                            jQuery("#bis_re_city_lat").val('');
                        }
                        if(subcat === "46"){
                            jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_0]").attr('placeholder','Enter hr:mi Ex(24:00 or 00:30 or 01:05)');
                        }   
                        if(subcat === "44"){
                            jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_1]").attr('id', 'bis_distance_units_' + rowIndex + '_value_1');
                            jQuery("#bis_distance_units_" + rowIndex + "_value_1").attr('style','width: 80px; position: absolute; right: 200px; top: 5px;');
                            jQuery("#bis_distance_units_" + rowIndex + "_value_1").attr('placeholder', 'mi or km');
                            jQuery("#bis_distance_units_" + rowIndex + "_value_1").show();
                            jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_0]").attr('style','width: 490px;');
                        } else {
                            jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_1]").hide();
                            jQuery(".drag-handle").attr('style','padding-right: 0px;');
                        }
                        if (subcat === "35" && (cond === 'contains_any_of' || cond === 'does_not_contains_any_of')) {
                            jQuery('.bis-postcode-csv-file').show();
                            var pcods = '';
                            jQuery('#bis_re_post_code_upload').click(function () {
                                jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_0]").val('');
                                var csv = jQuery('#bis_re_post_code_file');
                                var csvFile = csv[0].files[0];
                                var ext = csv.val().split(".").pop().toLowerCase();
                                if (jQuery.inArray(ext, ["csv"]) === -1) {
                                    alert('upload csv');
                                    return false;
                                }
                                if (csvFile !== 'undefined') {
                                    reader = new FileReader();
                                    reader.onload = function (e) {
                                        rows = e.target.result.split(/\r|\n|\r\n/);
                                        if (rows.length > 0) {
                                            var first_Row_Cells = splitCSVtoCells(rows[0], ","); //Taking Headings
                                            var jsonArray = new Array();
                                            for (var i = 1; i < rows.length; i++) {
                                                var cells = splitCSVtoCells(rows[i], ",");
                                                var obj = {};
                                                for (var j = 0; j < cells.length; j++){
                                                    obj[first_Row_Cells[j]] = cells[j];
                                                }
                                                jsonArray.push(obj);
                                            }
                                            pcods = '';
                                            jQuery.each(jsonArray, function(key,val){
                                                if(val.hasOwnProperty('Pincode')){
                                                    var pinStr = val.Pincode;
                                                    if(pcods !== '' && pinStr !== ''){
                                                        pcods = pcods+','+pinStr;
                                                    } else if (pinStr !== '') {
                                                        pcods = pinStr;
                                                    }
                                                }
                                            });
                                            if(pcods !== ''){
                                                jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_0]").val(pcods);
                                            }
                                        }
                                    };
                                    reader.readAsText(csvFile);
                                }
                            });
                        } else {
                            jQuery('.bis-postcode-csv-file').hide();
                        }
                    }
                });
        });
        
        jQuery(document).on('click', "button[data-add='rule']", function () {
            jQuery(".rules-group-container select").multiselect({
                nonSelectedText: '<?php _e("None Selected", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>',
                maxHeight: 200,
                enableCaseInsensitiveFiltering: true,
                enableFiltering: true
            });
        });

        jQuery(document).on('click', "button[data-add='group']", function () {
            jQuery(".rules-group-container select").multiselect({
                nonSelectedText: '<?php _e("None Selected", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>',
                maxHeight: 200,
                enableCaseInsensitiveFiltering: true,
                enableFiltering: true
            });
        });
        
         jQuery("#bis_re_eval_type").multiselect({
            buttonWidth: '150px'
        });
        
        jQuery('.popoverData').popover();

        /*
         * Add the tokenInput component with values.
         */
        function addTokenInput(eleObj, subCriteria, ssubCriteria, condition, rowIndex) {
        
            jQuery(eleObj).prev().remove();
            var tokenLimit = 1;
            if (condition === "does_not_contains_any_of" || condition === "contains_any_of") {
                tokenLimit = null;
            }
            var bis_nonce = BISAjax.bis_rules_engine_nonce;
            
            var date = new Date();
            var millisec = date.getMilliseconds();
            jQuery(eleObj).tokenInput(ajaxurl + "?action=bis_re_get_value&bis_nonce=" + bis_nonce +
                    "&subcriteria=" + subCriteria + "&ssubCriteria=" + ssubCriteria + "&condition=" + condition
                    + "milsec=" + millisec, {
                        theme: "facebook",
                        minChars: 2,
                        method: "POST",
                        tokenLimit: tokenLimit,
                        onAdd: function (item) {
                            jQuery(eleObj).val(JSON.stringify(this.tokenInput("get")));
                            jQuery("#bis_re_city_lng").val(item.lng);
                            jQuery("#bis_re_city_lat").val(item.lat);
                            jQuery("#bis_city_latitude_" + rowIndex + "_value_3").val(item.lat);
                            jQuery("#bis_city_longitude_" + rowIndex + "_value_4").val(item.lng);
                        },
                        onDelete: function (item) {
                            jQuery(this).val(JSON.stringify(this.tokenInput("get")));
                        }
            });

        }
        jQuery("#bis_city_radius_popup").click(function () {
           bis_cities_popup();
        });
        jQuery("#bis_re_mi_km").change(function(){
            bis_radius_unit();
        });
        
        //bis_radius_unit_changer('bis_re_mi_km','<?php //echo $bis_radius_unit ?>');
    });

</script>