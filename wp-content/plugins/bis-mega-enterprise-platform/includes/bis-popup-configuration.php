<?php
$popUpVO = $popup_data;
$file_path = RulesEngineUtil::get_file_upload_path(). $plugin_rule_dir;
$scanned_directory = array_diff(scandir($file_path), array('..', '.'));
?>
<div class="row" id="bis_common_popup_pannel">
    <div class="form-group col-md-12">
        <div class="panel panel-default bis-popup-configure">
            <div class="panel-heading bis-css-popup-customization-tittle">
                <div class="container-fluid">
                    <div class="row">
                        <h3 class="panel-title"><label><?php _e("PopUp configuration", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label></h3>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="description"><?php _e("Title", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                        </label>
                        <input type="text"  class="form-control" maxlength="100"
                               id="bis_re_common_popup_title" name="bis_re_common_popup_title" placeholder="Enter PopUp title">    
                    </div>
                    <div class="form-group col-md-4">
                        <label><?php _e("Popup close Time", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                        <a class="popoverData" target="_blank" 
                           data-content='<?php _e("Show popup in seconds, Never indicate no auto redirect and user action is must.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>' rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                        <div>
                            <?php
                            $time_selected = "0";

                            if ($popUpVO != null) {
                                $time_selected = $popUpVO->autoCloseTime;
                            }

                            $redirect_time = RulesEngineUtil::get_auto_redirect_times();
                            ?>
                            <select name="bis_re_common_auto_red" class="form-control" id="bis_re_common_auto_red" style="width:353px!important;">
                                <?php
                                foreach ($redirect_time as $value) {
                                    $time_sel_attr = "";

                                    if ($time_selected == $value->id) {
                                        $time_sel_attr = "selected=selected";
                                    }
                                    ?>
                                    <option <?php echo $time_sel_attr ?> value="<?php echo $value->id ?>"><?php echo $value->label ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="description"><?php _e("Show PopUp", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                        <a class="popoverData" target="_blank" 
                           data-content='<?php _e("Shows PopUp always or one time, default is always.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>' rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                        <div>
                            <select name="bis_re_common_popup_occur" class="form-control" id="bis_re_common_popup_occur">
                                <?php
                                $show_always_sel = "selected=selected";
                                $show_once_sel = "";

                                if (isset($popUpVO) && isset($popUpVO->popUpOccurr) && $popUpVO->popUpOccurr == 1) {
                                    $show_always_sel = "";
                                    $show_once_sel = "selected=selected";
                                }
                                ?>
                                <option <?php echo $show_always_sel ?> value="0"><?php _e("Always", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option <?php echo $show_once_sel ?> value="1"><?php _e("Once", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="description"><?php _e("Heading", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                        <input type="text"  class="form-control" maxlength="500"
                               id="bis_re_common_heading" name="bis_re_common_heading" placeholder="Enter heading">    
                    </div>
                    <div class="form-group col-md-4">
                        <label for="description"><?php _e("Sub heading", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                        <input type="text"  class="form-control" maxlength="500"
                               id="bis_re_common_sub_heading" name="bis_re_common_sub_heading" placeholder="Enter sub heading">    
                    </div>
                    <div class="form-group col-md-4">
                        <label for="description"><?php _e("Open Page In", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                        <a class="popoverData" target="_blank" 
                           data-content='<?php _e("Redirect to target URL in the same browser window or new browser window.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>' rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                        <div>
                            <select name="bis_re_common_red_window" class="form-control" id="bis_re_common_red_window" maxlength="500">
                                <?php
                                $same_window_sel = "selected=selected";
                                $new_window_sel = "";
                                if (isset($popUpVO) && isset($popUpVO->redirectWindow) && $popUpVO->redirectWindow == 1) {
                                    $same_window_sel = "";
                                    $new_window_sel = "selected=selected";
                                }
                                ?>
                                <option <?php echo $same_window_sel ?> value="0"><?php _e("Same Window", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option <?php echo $new_window_sel ?> value="1"><?php _e("New Window", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="description"><?php _e("Button-1 label", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                        </label>
                        <input type="text"  class="form-control" maxlength="100"
                               id="bis_re_btn1_label" name="bis_re_btn1_label" placeholder="<?php _e("Enter button 1 label", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                    </div>
                    <div class="form-group col-md-4">
                        <label for="description"><?php _e("Button-1 url", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                        <a class="popoverData" target="_blank"
                           data-content='<?php _e("Enter the url to redirect the any site. This url will be used for opening to the specified website.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>' rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                        <input type="text" class="form-control" maxlength="100" 
                               id="bis_re_but1_url" name="bis_re_but1_url" placeholder="<?php _e("Enter button 1 url", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                    </div>
                    <div class="form-group col-md-4">
                        <label for="description"><?php _e("Select Template", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                        <select name="bis_re_tempalte" id="bis_re_tempalte">
                            <?php 
                                    $defaul_sel = ""; 
                                    if(isset($popUpVO) && isset($popUpVO->tempalteFileName) && $popUpVO->tempalteFileName == "bis-popup-template.html"){
                                    $defaul_sel = "selected=selected";
                            }?>
                            <option  <?php echo $defaul_sel ?> value="<?php echo "bis-popup-template.html" ?>"><?php echo "Default Template" ?></option>
                            <?php foreach ($scanned_directory as $file_name) {
                                    if(RulesEngineUtil::isContains($file_name, ".html")){
                                            $tempalte_sel = ""; 
                                            if(isset($popUpVO) && isset($popUpVO->tempalteFileName) && $popUpVO->tempalteFileName == $file_name){
                                                $tempalte_sel = "selected=selected";
                                            }
                                            ?>
                            <option  <?php echo $tempalte_sel ?> value="<?php echo $file_name ?>"><?php echo $file_name; ?></option>
                                   <?php }
                                }
                            ?>
                        </select> 
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="description"><?php _e("Button-2 label", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                        </label>
                        <input type="text"  class="form-control" maxlength="100"
                               id="bis_re_but2_label" name="bis_re_but2_label" placeholder="<?php _e("Enter button 2 label", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                    </div>
                    <div class="form-group col-md-4">
                        <label for="description"><?php _e("Button-2 url", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                        <a class="popoverData" target="_blank"
                           data-content='<?php _e("Enter the url to redirect the any site. This url will be used for opening to the specified website.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>' rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>

                        <input type="text" class="form-control" maxlength="100" 
                               id="bis_re_but2_url" name="bis_re_but2_url" placeholder="<?php _e("Enter button 2 url", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                    </div>
                </div>                 
                <div class="col-sm-9 pl-0" style="padding-right: 100px;">
                    <div class="panel panel-default ">
                        <h4 class="panel-tittle bis-css-popup-customization-tittle">Css Customization</h4>
                        <div class="panel-body" id="bis_cust_popup_panel_body" >
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="description"><?php _e("PopUp Background color", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                                    <input type="text"  class="form-control" maxlength="100"
                                           id="bis_re_common_popup_color" name="bis_re_common_popup_color" placeholder="<?php _e("Enter PopUp background in Hex Color Codes (ex: #80EEEE).", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="popup title"><?php _e("Title css class", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                                    <input type="text"  class="form-control" maxlength="100"
                                           id="bis_re_common_popup_title_class" name="bis_re_common_popup_title_class" placeholder="<?php _e("Enter PopUp title css class name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="description"><?php _e("Heading css class", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                                    <input type="text"  class="form-control" maxlength="100"
                                           id="bis_re_common_heading_class" name="bis_re_common_heading_class" placeholder="<?php _e("Enter heading css class name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="description"><?php _e("Sub Heading css class", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                                    <input type="text"  class="form-control" maxlength="100"
                                           id="bis_re_common_sub_heading_class" name="bis_re_common_sub_heading_class" placeholder="<?php _e("Enter sub heading css class name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="description"><?php _e("Button-1 css class", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                                    <input type="text"  class="form-control" maxlength="100"
                                           id="bis_re_btn1_class" name="bis_re_btn1_class" placeholder="<?php _e("Enter button 1 css class name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="description"><?php _e("Button-2 css class", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                                    <input type="text"  class="form-control" maxlength="100"
                                           id="bis_re_but2_class" name="bis_re_but2_class" placeholder="<?php _e("Enter button 2 class name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
</div>

<script>
    // wait for the DOM to be loaded
    jQuery(document).ready(function () {

        jQuery('#bis_re_common_auto_red').multiselect({
            buttonWidth: '350px'
        });

        jQuery('#bis_re_common_red_window').multiselect({
            buttonWidth: '250px'
        });

        jQuery('#bis_re_common_popup_occur').multiselect({
            buttonWidth: '250px'
        });
        
        jQuery('#bis_re_tempalte').multiselect({
            maxHeight: 300,
            nonSelectedText: '<?php _e("Select Template", "rulesengine"); ?>',
            buttonWidth: '250px'
        });
        
    <?php
    $popUpOccurr = "0";
    $redirectWindow = "0";

    if ($popUpVO != null) {
        if (isset($popUpVO->popUpOccurr)) {
            $popUpOccurr = $popUpVO->popUpOccurr;
        }

        if (isset($popUpVO->redirectWindow)) {
            $redirectWindow = $popUpVO->redirectWindow;
        }
    }
    ?>
          
            jQuery("#bis_re_common_popup_occur option:selected").removeAttr("selected");
            jQuery("#bis_re_common_popup_occur").multiselect('select', '<?php echo $popUpOccurr ?>');
            jQuery("#bis_re_common_red_window option:selected").removeAttr("selected");
            jQuery("#bis_re_common_red_window").multiselect('select', '<?php echo $redirectWindow ?>');
            jQuery("#bis_re_common_popup_title").val('<?php echo $popUpVO ? $popUpVO->title : "" ?>');
            jQuery("#bis_re_common_popup_color").val('<?php echo $popUpVO ? $popUpVO->popUpBackgroundColor : "" ?>');
            jQuery("#bis_re_common_heading").val('<?php echo $popUpVO ? $popUpVO->headingOne : "" ?>');
            jQuery("#bis_re_common_heading_class").val('<?php echo $popUpVO ? $popUpVO->headingOneClass : "" ?>');
            jQuery("#bis_re_common_sub_heading").val('<?php echo $popUpVO ? $popUpVO->headingTwo : "" ?>');
            jQuery("#bis_re_common_sub_heading_class").val('<?php echo $popUpVO ? $popUpVO->headingTwoClass : "" ?>');
            jQuery("#bis_re_btn1_label").val('<?php echo $popUpVO ? $popUpVO->buttonLabelOne : "" ?>');
            jQuery("#bis_re_btn1_class").val('<?php echo $popUpVO ? $popUpVO->buttonOneClass : "" ?>');
            jQuery("#bis_re_but2_label").val('<?php echo $popUpVO ? $popUpVO->buttonLabelTwo : "" ?>');
            jQuery("#bis_re_but2_class").val('<?php echo $popUpVO ? $popUpVO->buttonTwoClass : "" ?>');
            jQuery("#bis_re_common_popup_title_class").val('<?php echo $popUpVO ? $popUpVO->titleClass : "" ?>');
            jQuery("#bis_re_but1_url").val('<?php echo $popUpVO ? $popUpVO->buttonOneUrl : "" ?>');
            jQuery("#bis_re_but2_url").val('<?php echo $popUpVO ? $popUpVO->buttonTwoUrl : "" ?>');
     
    });
</script>    