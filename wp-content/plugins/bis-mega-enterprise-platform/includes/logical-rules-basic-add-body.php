<?php

use bis\repf\common\RulesEngineLocalization;
use bis\repf\common\RulesEngineCacheWrapper;

$logical_rule_engine_modal = new bis\repf\model\LogicalRulesEngineModel();
$plugin_type_value = RulesEngineCacheWrapper::get_session_attribute(BIS_PLUGIN_TYPE_VALUE);
//RulesEngineCacheWrapper::remove_session_attribute(BIS_PLUGIN_TYPE_VALUE);
$bis_re_rule_options = $logical_rule_engine_modal->get_rules_options($plugin_type_value);

$bis_attr_taxonomies = null;

if (RulesEngineUtil::is_woocommerce_installed()) {
    $bis_attr_taxonomies = $logical_rule_engine_modal->get_woo_attribute_taxonomies();
}
$bis_re_editable_roles = get_editable_roles();
?>
<div id="rulecollapse">
    <table class="table table-hover table-bordered table-striped dashboard-panel post-rules-container-edit">
        <thead>
            <tr>
                <th width="25%"><?php _e("Criteria", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></th>
                <th width="15%"><?php _e("Condition", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></th>
                <th><?php _e("Value", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></th>
                <th width="10%"></th>
            </tr>
        </thead>
        <tbody>

            <?php
            $rows_count = BIS_RULES_CRITERIA_ROWS_COUNT;
            for ($i = 0; $i < $rows_count; $i++) {
                $style = "";

                // Hide other rows
                if ($i > 0) {
                    $style = "display:none";
                }
                ?>

                <tr class="rule_criteria_row" id="criteria_<?php echo $i ?>" style='<?php echo $style ?>'>
                    <td>
                        <div class="input-group btn-group">
                            <select class="bis-multiselect bis_re_sub_option" name="bis_re_sub_option[]"
                                    id="bis_re_sub_option_<?php echo $i ?>" size="2">
                                        <?php
                                        foreach ($bis_re_rule_options as $row) {
                                            $bis_sub_options = $logical_rule_engine_modal->get_rules_sub_options($row->id, $plugin_type_value);
                                            $bis_sub_options = RulesEngineLocalization::get_localized_values($bis_sub_options);
                                            ?>
                                    <optgroup label="<?php echo __($row->name, 'rulesengine'); ?>">
                                        <?php foreach ($bis_sub_options as $sub_row) { ?>
                                            <?php if ($sub_row->id !== '2000') { ?>
                                                <option value="<?php echo $row->id . '_' . $sub_row->id ?>"
                                                    id="<?php echo $sub_row->id ?>"> <?php echo __($sub_row->name, 'rulesengine'); ?>
                                                </option>
                                            <?php
                                            } else if (!empty($bis_attr_taxonomies['data'])) {
                                                $bis_attr_list = $bis_attr_taxonomies['data'];
                                                foreach ($bis_attr_list as $bis_list) {
                                                    ?>    
                                                    <option value="<?php echo $row->id . '_' . $sub_row->id . '_' . $bis_list->id . '_' . $bis_list->slug ?>"
                                                            id="<?php echo $sub_row->id ?>"><?php echo ucfirst($bis_list->name) ?>
                                                    </option>    
                                                <?php }
                                            }
                                        }
                                        ?>        
                                    </optgroup>
                                <?php } ?>          
                            </select>
                        </div>
                    </td>
                    <td>
                        <span class="bis_re_condition_span" id="bis_re_condition_span_<?php echo $i ?>"></span>
                    </td>
                    <td>
                        <div class="form-group">
                            <div class="col-sm-10 pl-0">
                                <span id="bis_re_rule_value_span_<?php echo $i ?>"></span>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="input-group btn-group">
                            <span class="bis_re_value_type_id_span"
                                id="bis_re_value_type_id_span_<?php echo $i ?>"> 
                            </span>
                            <span>
                                <a href="javascript:void(0)" class="logical-operator"
                                   id="bis_add_criteria_<?php echo $i ?>" title="Add criteria">
                                    <span class="glyphicon glyphicon-plus-sign bis-add-criteria-icon-blue" aria-hidden="true">
                                    </span>
                                </a>
                            </span>
                            <span>
                            <?php if ($i != 0) { ?>
                                <a href="javascript:void(0)" class="bis-remove-icon bis_remove_criteria"
                                    accesskey=""id="bis_remove_criteria_<?php echo $i ?>" title="Remove criteria">
                                    <span
                                        class="glyphicon glyphicon-remove bis-delete-criteria-icon-red"
                                        aria-hidden="true">
                                    </span>
                                </a>
                            <?php } ?>
                            </span>
                        </div>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <span id="bis_near_by_popup" style="display: none">
        <?php require_once 'bis-near-by-popup.php'; ?>
    </span>
    <input type="hidden" id="bis_re_name" name="bis_re_name">
    <input type="hidden" id="bis_re_hook" name="bis_re_hook" value="">
    <input type="hidden" id="bis_re_eval_type" name="bis_re_eval_type" value="1">
    <input type="hidden" id="bis_re_right_bracket" name="bis_re_right_bracket" value="0">
    <input type="hidden" id="bis_re_left_bracket" name="bis_re_left_bracket" value="0">
    <input type="hidden" id="bis_re_description" name="bis_re_description">
    <input type="hidden" id="bis_re_city_lat" name="bis_re_city_lat" value="">
    <input type="hidden" id="bis_re_city_lng" name="bis_re_city_lng" value="">
</div>
<div class="form-group container-fluid" id="bis_city_radius_div" style="display: none; background: #dad7d7; border-radius: 4px;">
    <div class="form-group bis-city-radius-popup" style="position: relative; top: 5px; font-size: 18px; margin-bottom: 11px;">
        <a href="javascript:void(0)" id="bis_city_radius_popup"><i class="fa fa-hand-o-right" style="padding-right: 10px;" aria-hidden="true"></i><?php _e("Check near by cities.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></a>
    </div>
</div>
<div class="form-group container-fluid bis-postcode-csv-file" style="display: none; background: #dad7d7; border-radius: 4px;">
    <div class="form-group col-md-3 pl-0 bis-postcode-csv-file">
        <label><?php _e("Upload post/zip code csv", BIS_RULES_ENGINE_TEXT_DOMAIN); ?> </label>
        <a class="popoverData" target="_blank" href="<?php echo BIS_RULES_ENGINE_SITE ?>"
           data-content='<?php _e("If you have a post/zip code in a .csv format, you may add it by uploading it here..", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'
           rel="popover"
           data-placement="bottom" data-trigger="hover">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
        </a>
        <input type="file" class="form-control mega-file-upload" id="bis_re_post_code_file" name="bis_re_post_code_file">

    </div>
    <div class="mega-btn-upload col-md-2 bis-postcode-csv-file" style="display:block">
        <button id="bis_re_post_code_upload" type="button" class="btn btn-primary">
            <i class=""></i>Upload</button>
    </div>
</div>
<style>
    .mega-btn-upload {
        margin-top: 22px;
    }
    .mega-file-upload {
        height: 30px;
    }
</style>
<!--<div class="query-builder-change-rule" id="bis_city_radius_div" style="display: none; padding-left: 15px;">
    <div class="panel-body post-rules-container-edit dashboard-panel">
        <div>
            <div class="row">
                <div class="form-group col-md-2">
                    <label><?php _e("City Radius", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                    <a class="popoverData" target="_blank" href="<?php //echo BIS_RULES_ENGINE_SITE ?>"
                       data-content="<?php _e("City radius will get cities within specified radius.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>"
                       rel="popover"
                       data-placement="bottom" data-trigger="hover">
                        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                    </a>
                    <input type="text" class="form-control bis-near-radius"
                           id="bis_re_city_radius" name="bis_re_city_radius"
                           value="20">
                    <div class="can-toggle clearfix can-toggle--size-small mt-10 bis_re_mi_km">
                        <input id="bis_re_mi_km" name="bis_re_mi_km" checked type="checkbox">
                        <label for="bis_re_mi_km">
                            <div class="can-toggle__switch toggle_switch_bgclr" data-checked="mi" data-unchecked="km"></div>
                        </label>
                    </div>
                </div>
                <div class="form-group bis-city-radius-popup col-md-2" style="position: relative; top: 25px; font-size: 19px;">
                    <a href="javascript:void(0)" id="bis_city_radius_popup"><i class="fa fa-hand-o-right" aria-hidden="true"><?php _e("Near Cities", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group bis-city-radius-popup" style="position: relative; top: 5px; font-size: 18px;">
        <a href="javascript:void(0)" id="bis_city_radius_popup"><i class="fa fa-hand-o-right" style="padding-right: 10px;" aria-hidden="true"></i><?php _e("Check near by cities.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></a>
    </div>
</div>-->
<script>

    // wait for the DOM to be loaded
    jQuery(document).ready(function () {
        
        var selectedValueContainer = null;
        jQuery("#wpfooter").css("position", "relative");
        jQuery('.popoverData').popover();
        subCriteria = 0;
        ssubCriteria = 0;

        jQuery(".input-group-remove").click(function () {
            jQuery(this.parentElement).remove();
        });

        jQuery('.bis_re_sub_option').multiselect({
            nonSelectedText: '<?php _e("Select Criteria", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>',
            enableCaseInsensitiveFiltering: true,
            maxHeight: 220,
            buttonWidth: '350px',
            onChange: function (element, checked) {
                var subCrVal = element[0].value.split("_")[1];
                var ssubCrVal = element[0].value.split("_")[3];
                var rowIndex = jQuery(element[0]).closest("tr").index();
                subCriteria = subCrVal;
                ssubCriteria = ssubCrVal;
                
                addSubOptionChangeEvent(subCrVal, ssubCrVal, rowIndex);

                //Remove child dependent values
                var siblings = element.closest("td").nextAll();
                jQuery(siblings[0].children).html('<?php _e("None Selected", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                jQuery(siblings[1].children).children().children().html("");
                addDropdownDeleteEvent();
                    
                } 

        });

        jQuery('#bis_re_status').multiselect();

        jQuery('.bis-multiselect').multiselect({
        });

        function showResponse(responseText, statusText, xhr, $form) {

            if (responseText["status"] === "success") {
                bis_showLogicalRulesList();
            } else {
                if (responseText["status"] === "error") {
                    if (responseText["message_key"] === "duplicate_entry") {
                        bis_showErrorMessage('<?php _e("Duplicate rule name, Name should be unique.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                    } else if (responseText["message_key"] === "no_method_found") {
                        bis_showErrorMessage('<?php _e("Action hook method does not exist, Please define method with name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'
                                + " \"" + jQuery("#bis_re_hook").val() + "\".");
                    } else {
                        bis_showErrorMessage("<?php _e("Error occurred while creating rule.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>");
                    }
                }
            }

        }

        // Adds dropdown delete event
        function addDropdownDeleteEvent() {
            jQuery(".input-group-remove").on("click", function (evt) {
                evt.stopPropagation();
                evt.preventDefault();

                var childCond = jQuery(this).closest("td").nextAll();

                for (var i = 0; i < childCond.length; i++) {
                    jQuery(jQuery(childCond)[i]).find(".btn-group").remove();
                }

                jQuery(this.parentElement).remove();
            });

        }


        /**
         * This method is used to get the sub options of rules based on the optionId
         */
        function addSubOptionChangeEvent(subCrVal, ssubCrVal, rowIndex) {

                // Componenent Id should be dynamic
                jQuery.post(
                    BISAjax.ajaxurl,
                    {
                        action: 'bis_get_conditions',
                        optionId: subCrVal,
                        ssubOptionId : ssubCrVal
                    },
                    function (response) {

                        var data = {
                            compId: 'bis_re_condition_' + rowIndex,
                            compName: 'bis_re_condition[]',
                            suboptions: response["RuleConditions"]
                        };

                        var valueTypeId = response["ValueTypeId"];
                        var source = jQuery("#bis-selectComponent").html();
                        var template = Handlebars.compile(source);
                        var logicalRulesContent = template(data);

                        jQuery("#bis_re_condition_span_" + rowIndex).html(logicalRulesContent);

                        jQuery('.bis-multiselect').multiselect({
                            nonSelectedText: '<?php _e("None Selected", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'
                        });

                        var dataTypeId = {
                            id: 'bis_re_sub_opt_type_id_' + rowIndex,
                            name: 'bis_re_sub_opt_type_id[]',
                            value: valueTypeId
                        };

                        source = jQuery("#bis-logical-hidden-component").html();
                        template = Handlebars.compile(source);
                        var valueTypeIdContent = template(dataTypeId);

                        jQuery("#bis_re_value_type_id_span_" + rowIndex).html(valueTypeIdContent);

                        addDropdownDeleteEvent();
                        addConditionChangeEvent(jQuery("#" + data.compId), valueTypeId);    

                    }
                );
        }

        function addSelectBox(rowIndex) {

            var jqXHR = jQuery.get(ajaxurl,
                    {
                        action: "bis_re_get_rule_values",
                        bis_nonce: BISAjax.bis_rules_engine_nonce,
                        bis_sub_criteria: subCriteria,
                        bis_ssubCriteria: ssubCriteria

                    });

            jqXHR.done(function (response) {

                var data = {
                    compId: 'bis_re_rule_value_' + rowIndex,
                    compName: 'bis_re_rule_value[]',
                    suboptions: response
                };

                var source = jQuery("#bis-selectComponent").html();

                var template = Handlebars.compile(source);
                var logicalRulesContent = template(data);
                var subOptObj = jQuery("#bis_re_rule_value_span_" + rowIndex);

                subOptObj.html(logicalRulesContent);

                jQuery('.bis-multiselect').multiselect({
                    nonSelectedText: '<?php _e("None Selected", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>',
                    maxHeight: 200,
                    enableCaseInsensitiveFiltering:true,
                    enableFiltering: true
                });
            });

        }

        function renderTextBox(data, rowIndex) {
            
            if((subCriteria === 32 || subCriteria === 26 || subCriteria === 27)
                    && (condition === 1 || condition === 2)) {
                data.placeholder = '<?php _e("Enter a value, format name=value", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>';
            }
            
            if(subCriteria === 46) {
                data.placeholder = '<?php _e("Enter hours hr:mi Ex(24:00 or 00:30 or 01:05)", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>';
            }
            
            var source = jQuery("#bis-logical-textbox-component").html();
            var template = Handlebars.compile(source);
            var textbox = template(data);
            var textboxId = jQuery("#bis_re_rule_value_span_" + rowIndex);
            textboxId.html(textbox);
            var ids_exists = jQuery.find("#bis_distance_units_" + rowIndex + "_value_1").length;
            if(subCriteria === 44 && ids_exists === 0) {
                jQuery("#bis_re_rule_value_span_" + rowIndex).append("<input type='text' style='width: 66px; height: 34px;border-radius: 4px; position: absolute; right: -59px; top: 0px;' id='bis_distance_units_" + rowIndex + "_value_1' name='bis_distance_units_" + rowIndex + "_value_1' placeholder='mi or km'>");
            } else if(ids_exists > 1) {
                jQuery("#bis_distance_units_" + rowIndex + "_value_1").remove();
            }
            if(subCriteria !== 44) {
                jQuery("#bis_distance_units_" + rowIndex + "_value_1").remove();
            }
            
            if(subCriteria === 35 && (condition === 12 || condition === 14)){
               jQuery('.bis-postcode-csv-file').show();
                var pcods = '';
                jQuery('#bis_re_post_code_upload').click(function () {
                    jQuery("#bis_re_rule_value_" + rowIndex).val('');
                    //jQuery("input[name=bis-rule-builder_rule_" + rowIndex + "_value_0]").attr('id', 'bis_postcodes' + rowIndex + '_value_0');
                    var csv = jQuery('#bis_re_post_code_file');
                    var csvFile = csv[0].files[0];
                    var ext = csv.val().split(".").pop().toLowerCase();
                    if (jQuery.inArray(ext, ["csv"]) === -1) {
                        alert('upload csv');
                        return false;
                    }
                    if (csvFile !== 'undefined') {
                        reader = new FileReader();
                        reader.onload = function (e) {
                            rows = e.target.result.split(/\r|\n|\r\n/);
                            if (rows.length > 0) {
                                var first_Row_Cells = splitCSVtoCells(rows[0], ","); //Taking Headings
                                var jsonArray = new Array();
                                for (var i = 1; i < rows.length; i++) {
                                    var cells = splitCSVtoCells(rows[i], ",");
                                    var obj = {};
                                    for (var j = 0; j < cells.length; j++){
                                        obj[first_Row_Cells[j]] = cells[j];
                                    }
                                    jsonArray.push(obj);
                                }
                                pcods = '';
                                jQuery.each(jsonArray, function(key,val){
                                    if(val.hasOwnProperty('Pincode')){
                                        var pinStr = val.Pincode;
                                        if(pcods !== '' && pinStr !== ''){
                                            pcods = pcods+','+pinStr;
                                        } else if (pinStr !== '') {
                                            pcods = pinStr;
                                        }
                                    }
                                });
                                if(pcods !== ''){
                                    jQuery("#bis_re_rule_value_" + rowIndex).val(pcods);
                                }
                            }
                        };
                        reader.readAsText(csvFile);
                    }
                });
            } else {
                jQuery('.bis-postcode-csv-file').hide();
            }
        }
         
        function addConditionChangeEvent(condOptObj, valueTypeId) {
        
            condOptObj.on("change", function () {

                condition = Number(jQuery.trim(this.value));
                var rowIndex = jQuery(this).closest("tr").index();
                jQuery("#bis_re_rule_value_" + rowIndex).prev().remove();

                subCriteria = Number(subCriteria);
                valueTypeId = Number(valueTypeId);
                
                switch (valueTypeId) {
                    
                    case 1: // Token Input
                        addTokenInput(rowIndex);

                        switch (condition) {
                            case 1: // Equal
                            case 2: // Not Equal
                            case 12:
                            case 14:
                            case 15:
                                var ids_exist = jQuery.find("#bis_city_radius_" + rowIndex + "_value_1").length;
                                if(condition === 15 && ids_exist === 0) {
                                    jQuery("#bis_re_rule_value_span_" + rowIndex).append("<input type='text' style='width: 70px; position: absolute; right: -30px; top: 0px;' id='bis_city_radius_" + rowIndex + "_value_1' name='bis_city_radius_" + rowIndex + "_value_1' placeholder='Radius'><select style='width: 55px; position: absolute; right: -90px; top: 0px;' id='bis_city_radius_units_" + rowIndex + "_value_2' name='bis_city_radius_units_" + rowIndex + "_value_2'><option val='mi'>mi</option><option val='km'>km</option></select><input type='hidden' id='bis_city_latitude_" + rowIndex + "_value_3' name='bis_city_latitude_" + rowIndex + "_value_3'><input type='hidden' id='bis_city_longitude_" + rowIndex + "_value_4' name='bis_city_longitude_" + rowIndex + "_value_4'>");
                                } else if(ids_exist > 1) {
                                    jQuery("#bis_city_radius_" + rowIndex + "_value_1").remove();
                                    jQuery("#bis_city_radius_units_" + rowIndex + "_value_2").remove();
                                    jQuery("#bis_city_latitude_" + rowIndex + "_value_3").remove();
                                    jQuery("#bis_city_longitude_" + rowIndex + "_value_4").remove();
                                }
                                if(condition != 15) {
                                    jQuery("#bis_city_radius_" + rowIndex + "_value_1").remove();
                                    jQuery("#bis_city_radius_units_" + rowIndex + "_value_2").remove();
                                    jQuery("#bis_city_latitude_" + rowIndex + "_value_3").remove();
                                    jQuery("#bis_city_longitude_" + rowIndex + "_value_4").remove();
                                }
                                break;
                                addTokenInput(rowIndex);
                            default:
                                var data = {
                                    id: 'bis_re_rule_value_' + rowIndex,
                                    name: 'bis_re_rule_value[]',
                                    placeholder: '<?php _e("Enter a value", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'
                                };

                                renderTextBox(data, rowIndex);

                        }
                        break;

                    case 2:  // Select Option
                        addSelectBox(rowIndex);
                        break;

                    case 3: // Calendar Date
                        var data = {
                            id: 'bis_re_rule_value_' + rowIndex,
                            name: 'bis_re_rule_value[]',
                            placeholder: '<?php _e("Enter date", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'
                        };

                        // Default only date
                        tpicker = false;
                        dformat = 'Y-m-d';
                        dpicker = true;

                        if (subCriteria === 14) { // Date & Time
                            tpicker = true;
                            dformat = 'Y-m-d H:i';
                        } else if (subCriteria === 10) {  // Only Time
                            dpicker = false;
                            dformat = 'H:i';
                            tpicker = true;
                        }

                        renderTextBox(data, rowIndex);

                        jQuery("#bis_re_rule_value_" + rowIndex).datetimepicker({
                            timepicker: tpicker,
                            format: dformat,
                            datepicker: dpicker,
                            closeOnDateSelect: true,
                            mask: true
                        });

                        break;

                    case 4: // Text box
                        var data = {
                            id: 'bis_re_rule_value_' + rowIndex,
                            name: 'bis_re_rule_value[]',
                            placeholder: '<?php _e("Enter a value", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'
                        };
                        
                        if(condition === 12 || condition === 14) {
                          data.placeholder = '<?php _e("Enter values with comma separation (i.e value1, value2, value3)", "rulesengine"); ?>';
                        } else if(condition === 13) {
                          data.placeholder = '<?php _e("Enter URL with /** pattern (ex: http://rulesengine.in/mypath/**)", "rulesengine"); ?>';
                        }  
                        renderTextBox(data, rowIndex);
                        break;
                }
            });
        }

        /**
         Add the tokenInput component with values.
         */
        function addTokenInput(rowIndex) {

            var bis_nonce = BISAjax.bis_rules_engine_nonce;
            jQuery("#bis_re_rule_value_" + rowIndex).prev().remove();

            var data = {
                id: 'bis_re_rule_value_' + rowIndex,
                name: 'bis_re_rule_value[]'
            };

            var source = jQuery("#bis-logical-hidden-component").html();
            var template = Handlebars.compile(source);
            var textbox = template(data);
            var textboxId = jQuery("#bis_re_rule_value_span_" + rowIndex);
            textboxId.html(textbox);
            var tokenLimit = 1;
            
            if((condition === 12)||(condition === 14)){
                tokenLimit = null;
            }

            jQuery("#bis_re_rule_value_" + rowIndex).tokenInput(ajaxurl + "?action=bis_re_get_value&bis_nonce=" + bis_nonce +
                    "&subcriteria=" + subCriteria + "&ssubCriteria="+ssubCriteria+"&condition=" + condition, {
                        theme: "facebook",
                        minChars: 2,
                        method: "POST",
                        tokenLimit: tokenLimit, 
                        onAdd: function (item) {
                            jQuery("#bis_re_rule_value_" + rowIndex).val(JSON.stringify(this.tokenInput("get")));
                            jQuery("#bis_re_city_lng").val(item.lng);
                            jQuery("#bis_re_city_lat").val(item.lat);
                            if (condition === 15) {
                                selectedValueContainer = jQuery.parseJSON(jQuery("#bis_re_rule_value_" + rowIndex).val());
                                jQuery("#bis_city_latitude_" + rowIndex + "_value_3").val(item.lat);
                                jQuery("#bis_city_longitude_" + rowIndex + "_value_4").val(item.lng);
                            } 
                            if (condition !== 15) {
                                jQuery("#bis_city_radius_" + rowIndex + "_value_1").remove();
                                jQuery("#bis_city_radius_units_" + rowIndex + "_value_2").remove();
                                jQuery("#bis_city_latitude_" + rowIndex + "_value_3").remove();
                                jQuery("#bis_city_longitude_" + rowIndex + "_value_4").remove();
                            }
                        },
                        onDelete: function (item) {
                            jQuery(this).val(JSON.stringify(this.tokenInput("get")));
                        }
                    });
            if (condition === 15) {
                jQuery("#bis_re_rule_value_span_" + rowIndex).append("<input type='text' style='width: 70px; position: absolute; right: -30px; top: 0px;' id='bis_city_radius_" + rowIndex + "_value_1' name='bis_city_radius_" + rowIndex + "_value_1' placeholder='Radius'><select style='width: 55px; position: absolute; right: -90px; top: 0px;' id='bis_city_radius_units_" + rowIndex + "_value_2' name='bis_city_radius_units_" + rowIndex + "_value_2'><option val='mi'>mi</option><option val='km'>km</option></select><input type='hidden' id='bis_city_latitude_" + rowIndex + "_value_3' name='bis_city_latitude_" + rowIndex + "_value_3'><input type='hidden' id='bis_city_longitude_" + rowIndex + "_value_4' name='bis_city_longitude_" + rowIndex + "_value_4'>");
                jQuery("#bis_city_radius_div").show();
            } 
            if (condition !== 15) {
                jQuery("#bis_city_radius_" + rowIndex + "_value_1").remove();
                jQuery("#bis_city_radius_units_" + rowIndex + "_value_2").remove();
                jQuery("#bis_city_latitude_" + rowIndex + "_value_3").remove();
                jQuery("#bis_city_longitude_" + rowIndex + "_value_4").remove();
                jQuery("#bis_city_radius_div").hide();
                jQuery("#bis_re_city_lng").val('');
                jQuery("#bis_re_city_lat").val('');
            }

        }

        jQuery(".bis_remove_criteria").click(function (event) {
            event.preventDefault();
            var ctr = jQuery(this).closest("tr").filter(":visible");
            var pretr = ctr.prevUntil().filter(":visible").attr("id");
            var preIndex = getRowIndex(pretr);
            var siblings = ctr.children();
            var crowlen = Number(jQuery(".rule_criteria_row").filter(":visible").length);

            // Removes the dropdown logical operation value.
            crowlen = crowlen - 1;
            var cIndex = Number(preIndex) + 1;

            if (crowlen === cIndex) {
                jQuery("#bis_add_criteria_" + preIndex).show();
            }
            
            // Remove the values before hiding the criteria
            jQuery("#bis_re_condition_span_"+cIndex).html('<?php _e("None Selected", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
            jQuery("#bis_re_rule_value_span_"+cIndex).html("");

            var cSubOption =  jQuery("#bis_re_sub_option_"+cIndex);
            cSubOption.multiselect("deselect", cSubOption.val());
           
            // Hide the next row
            jQuery(this).closest("tr").hide();
        });

        /**
         This method will show and hide the next criteria based on Operator value.
         */
        jQuery(".logical-operator").click(function (event) {
            event.preventDefault();
            jQuery(this).closest("tr").next().show();
            var preIndex = getRowIndex(jQuery(this).attr("id"));
            jQuery(this).hide();
            var crowlen = Number(jQuery(".rule_criteria_row").filter(":visible").length);

            // Removes the dropdown logical operation value.
            crowlen = crowlen - 1;
            var nextIndex = Number(preIndex) + 1;
            jQuery("#bis_add_criteria_" + nextIndex).show();
        });


        function split(val) {
            return val.split(/,\s*/);
        }

        function extractLast(term) {
            return split(term).pop();
        }

        function getSubCriteria(val) {
            var ele = jQuery(val).closest("td").prevAll();
            subCriteria = jQuery(ele[1]).children().children().children()[1].value;
            return subCriteria;
        }

        function getCondition(val) {
            var ele = jQuery(val).closest("td").prevAll();
            return jQuery(ele[0]).children().children().children()[1].value;
        }

        jQuery("#bis_back_rules_list").click(function () {
            bis_showLogicalRulesList();
        });
        
        jQuery("#bis_city_radius_popup").click(function () {
           bis_cities_popup(selectedValueContainer);
        });
        jQuery("#bis_re_mi_km").change(function(){
            bis_radius_unit();
        });
    });
</script>