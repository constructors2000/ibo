<div class="panel panel-default">
    <div class="panel-heading dashboard-panel-heading">
        <div>
            <div class="row">
                <div class="col-md-6">
                    <h3 class="panel-title"><label><?php _e("Dashboard", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label></h3>
               </div>
                <div class="col-md-6">
                    <strong class="panel-help"><label style="float:right;"><a id="bis_help_url" target="_blank" href='<?php echo BIS_MEGA_DASHBOARD_HELP; ?>'><?php _e("Help", BIS_RULES_ENGINE_TEXT_DOMAIN); ?> &nbsp;<span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label></strong>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body dashboard-panel">
        <div>
            <div>
                <ul role="tablist" class="nav nav-tabs" id="myTabs">
                    <?php if (!is_network_admin()) {?>
                        <li class="active" role="presentation">
                            <a aria-expanded="false" aria-controls="installed" data-toggle="tab" 
                               role="tab" id="home-tab" href="#installed">
                                   <?php _e("Active", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                            </a>
                        </li>
                        <li class="deactive" role="presentation">
                                <a aria-expanded="false" aria-controls="inactive" data-toggle="tab" 
                                   role="tab" id="inactive-tab" href="#inactive">
                                       <?php _e("Inactive", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                                </a>
                        </li>
                        <li role="presentation" class="">
                                <a aria-controls="available" data-toggle="tab" id="profile-tab" role="tab" 
                                   href="#available" aria-expanded="true">
                                       <?php _e("Available", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                                </a>
                         </li>
                 <?php }?>
                    <li role="presentation" class="">
                        <a aria-controls="settings" data-toggle="tab" id="profile-tab" role="tab" 
                           href="#settings" aria-expanded="true"> 
                               <?php _e("Settings", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div aria-labelledby="home-tab" id="installed" class="tab-pane fade active in" role="tabpanel">
                        <div class="row" style="margin-top: 20px;">
                            <!--<a href="admin.php?page=bis_pg_analytics">
                                <div class="col-md-3">
                                    <div class="icon-placeholder">
                                        <span class="analytic-rule-icon"></span>
                                        <h3><?php _e("Analytics", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
                                        <p><?php _e("Analysis about sites.", BIS_RULES_ENGINE_TEXT_DOMAIN) ?></P>
                                    </div>
                                </div>
                            </a>!-->
                        <?php
                            
                            $bis_addons = RulesEngineUtil::get_option(BIS_RULESENGINE_ADDONS_CONST);

                            if ($bis_addons != null) {
                                $bis_active_rules_json = json_decode($bis_addons);
                                foreach ($bis_active_rules_json as $ac_rule) {
                                    if ($ac_rule->status == '1') {

                                        if (!is_network_admin()) {
                                            ?>
                                            <a href="<?php echo $ac_rule->path ?>">
                                                <div class="col-md-3">
                                                    <div class="icon-placeholder">    
                                                        <span class="<?php echo $ac_rule->cssClass ?>"></span>
                                                        <h3><?php _e($ac_rule->displayName, BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
                                                        <p><?php _e($ac_rule->description, BIS_RULES_ENGINE_TEXT_DOMAIN); ?></P>
                                                    </div>
                                                </div>
                                            </a>
                                            <?php } else { // End of Network Admin If 
                                            ?>
                                            <!--<div class="col-md-3">
                                                <div class="icon-placeholder">    
                                                    <span class="<?php echo $ac_rule->cssClass ?>"></span>
                                                    <h3><?php _e($ac_rule->displayName, BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
                                                    <p><?php _e($ac_rule->description, BIS_RULES_ENGINE_TEXT_DOMAIN); ?></P>
                                                </div>
                                            </div>-->
                                        <?php
                                        } // End of Network admin else 
                                    }// If condition                    
                                } // For loop
                            } // Bis Addons
                            ?>
                            <?php if (!is_network_admin()) { ?>
                                <a href="admin.php?page=bis_pg_rulesengine">
                                    <div class="col-md-3">
                                        <div class="icon-placeholder">
                                            <span class="logical-rule-icon"></span>
                                            <h3><?php _e("Logical Rules", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
                                            <p><?php _e("Define your business rules.", BIS_RULES_ENGINE_TEXT_DOMAIN) ?></P>
                                        </div>
                                    </div>
                                </a>
                            <?php } else { ?>
                                <!--<div class="col-md-3">
                                    <div class="icon-placeholder">
                                        <span class="logical-rule-icon"></span>
                                        <h3><?php _e("Logical Rules", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
                                        <p><?php _e("Define your business rules.", BIS_RULES_ENGINE_TEXT_DOMAIN) ?></P>
                                    </div>
                                </div>-->
                            <?php } ?>

                        </div> 
                    </div>
                    <div aria-labelledby="inactive-tab" id="inactive" class="tab-pane fade" role="tabpanel">
                        <div class="row" style="margin-top: 20px;">
                            <?php
                             $bis_addons = RulesEngineUtil::get_option(BIS_RULESENGINE_ADDONS_CONST);
                            $no_inactive = true;

                            if ($bis_addons != null && !is_network_admin()) {
                                $bis_active_rules_json = json_decode($bis_addons);
                                foreach ($bis_active_rules_json as $ac_rule) {
                                    if ($ac_rule->status == '0') {
                                        $no_inactive = false;
                                        ?>
                                        <div class="col-md-3">
                                            <div class="icon-placeholder">    
                                                <span class="<?php echo $ac_rule->cssClass ?>"></span>
                                                <h3><?php _e($ac_rule->displayName, BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
                                                <p><?php _e($ac_rule->description, BIS_RULES_ENGINE_TEXT_DOMAIN); ?></P>
                                            </div>
                                        </div>
                                        <?php
                                    } // If condition                    
                                } // For loop
                            } // Bis Addons
                              
                            if ($no_inactive) {
                                ?>
                            <div class="no-active-plugins">
                                <div class="alert alert-info text-center" role="alert">No inactive plugins.</div></div>
                        <?php } ?>

                        </div> 
                    </div>
                    <div aria-labelledby="available-tab" id="available" class="tab-pane fade" role="tabpanel">
                        <?php if (!is_network_admin()) { ?>
                            <div class="row" style="margin-top: 20px;">
                                <!-- <a target="_blank" href="javascript:void(0)">
                                    <div class="col-md-3">
                                        <div class="icon-placeholder">
                                            <i class="icon64 mb-5 di-block icon-analytics"></i>
                                            <h3><?php _e("Analytics", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
                                            <p><?php _e("Analysis about sites.", BIS_RULES_ENGINE_TEXT_DOMAIN) ?></P>
                                        </div>
                                    </div>
                                </a>!-->
                                <a target="_blank" href="http://docs.megaedzee.com/docs/page-controller/">
                                    <div class="col-md-3">
                                        <div class="icon-placeholder">    
                                            <i class="icon48 mb-5 di-block icon-page-rules" style="margin-bottom: 15px;"></i>
                                            <h3><?php _e("Page Controller", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
                                            <p><?php _e("Define rules for hiding, appending and replacing content for pages.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></P>
                                        </div>
                                    </div>
                                </a>

                                <a target="_blank" href="http://docs.megaedzee.com/docs/post-controller/">
                                    <div class="col-md-3">
                                        <div class="icon-placeholder">   
                                            <i class="icon48 mb-5 di-block icon-post-rules" style="margin-bottom: 15px;"></i>
                                            <h3><?php _e("Post Controller", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
                                            <p><?php _e("Define rules for hiding, appending and replacing content for posts.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></P>
                                        </div>
                                    </div>
                                </a>
                                <a target="_blank" href="http://docs.megaedzee.com/docs/category-controller/">
                                    <div class="col-md-3">
                                        <div class="icon-placeholder">
                                            <i class="icon48 mb-5 di-block icon-category-rules" style="margin-bottom: 15px;"></i>
                                            <h3><?php _e("Category Controller", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
                                            <p><?php _e("Define rules for show or hide categories.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></P>
                                        </div>
                                    </div>
                                </a>
                                <a target="_blank" href="http://docs.megaedzee.com/docs/widget-controller/" >
                                    <div class="col-md-3">
                                        <div class="icon-placeholder">
                                            <i class="icon48 mb-5 di-block icon-widget-rules" style="margin-bottom: 15px;"></i>
                                            <h3><?php _e("Advanced Sidebar and Widget Restrictions", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
                                            <p><?php _e("Define rules for show or hide widgets.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></P>
                                        </div>
                                    </div>
                                </a>
                                <a target="_blank" href="http://docs.megaedzee.com/docs/theme-controller/">
                                    <div class="col-md-3">
                                        <div class="icon-placeholder">
                                            <i class="icon48 mb-5 di-block icon-theme-rules" style="margin-bottom: 15px;"></i>
                                            <h3><?php _e("Theme Controller", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
                                            <p><?php _e("Define rules for switching themes.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></P>
                                        </div>
                                    </div>
                                </a>
                                <a target="_blank" href="http://docs.megaedzee.com/docs/language-controller/">
                                    <div class="col-md-3">
                                        <div class="icon-placeholder">
                                            <i class="icon48 mb-5 di-block icon-language-rules" style="margin-bottom: 15px;"></i>
                                            <h3><?php _e("Language Controller", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
                                            <p><?php _e("Define rules for switching languages.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></P>
                                        </div>
                                    </div>
                                </a>            
                                <a target="_blank" href="http://docs.megaedzee.com/docs/redirect-controller/" >
                                    <div class="col-md-3">
                                        <div class="icon-placeholder">
                                            <i class="icon48 mb-5 di-block icon-url-redirection" style="margin-bottom: 15px;"></i>
                                            <h3><?php _e("Redirect Controller", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
                                            <p><?php _e("Define rules for URL Redirection.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></P>
                                        </div>
                                    </div>
                                </a>
                                <a target="_blank" href="http://docs.megaedzee.com/docs/product-controller/" >
                                    <div class="col-md-3">
                                        <div class="icon-placeholder">
                                            <i class="icon48 mb-5 di-block icon-woo-product-rules" style="margin-bottom: 15px;"></i>
                                            <h3><?php _e("Product Controller", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
                                            <p><?php _e("Define rules for hiding, appending and replacing content for products.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></P>
                                        </div>
                                    </div>
                                </a>
                                <a target="_blank" href="http://docs.megaedzee.com/docs/google-analytics/" >
                                    <div class="col-md-3">
                                        <div class="icon-placeholder">
                                            <i class="icon48 mb-5 di-block icon-google-analytics" style="margin-bottom: 15px;"></i>
                                            <h3><?php _e("Google Analytics", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
                                            <p><?php _e("Google analytics data.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></P>
                                        </div>
                                    </div>
                                </a>
                                <a target="_blank" href="http://docs.megaedzee.com/docs/mega-enterprise-platform/logical-rules/" >
                                    <div class="col-md-3">
                                        <div class="icon-placeholder">
                                            <i class="icon48 mb-5 di-block icon-logical-rules" style="margin-bottom: 15px;"></i>
                                            <h3><?php _e("Logical Rules", "rulesengine"); ?></h3>
                                            <p><?php _e("Define your business rules.", "rulesengine") ?></P>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                    <?php 
                        if (is_network_admin()) {
                            $setting_tab = "tab-pane fade active in";
                        } else {
                            $setting_tab = "tab-pane fade";
                        }
                    ?>
                    <div aria-labelledby="settings-tab" id="settings" class="<?php echo $setting_tab ?>" role="tabpanel">
                        <div class="row" style="margin-top: 20px;">
                            <?php if ((!is_multisite()) || (is_multisite() && is_network_admin())) { ?>
                                <a href="admin.php?page=bis_pg_global_settings">
                                    <div class="col-md-3">
                                        <div class="icon-placeholder">
                                            <i class="icon48 mb-5 di-block icon-settings" style="margin-bottom: 15px;"></i>
                                            <h3><?php _e("Global Settings", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
                                            <p><?php _e("Configure settings for Rules Engine.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></P>
                                        </div>
                                    </div>
                                </a>
                            <?php } ?>
                            <?php if ((!is_multisite()) || (is_multisite() && !is_network_admin())) { ?>
                                <a href="admin.php?page=bis_pg_site_settings">
                                    <div class="col-md-3">
                                        <div class="icon-placeholder">
                                            <i class="icon48 mb-5 di-block icon-site-settings" style="margin-bottom: 15px;"></i>
                                            <h3><?php _e("Site Settings", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
                                            <p><?php _e("Configure settings for Rules Engine.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></P>
                                        </div>
                                    </div>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>





