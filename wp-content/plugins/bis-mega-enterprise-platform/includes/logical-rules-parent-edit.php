<?php
use bis\repf\common\RulesEngineCacheWrapper;

$logical_rule_engine_modal = new bis\repf\model\LogicalRulesEngineModel();

$ruleIdParent = RulesEngineCacheWrapper::get_session_attribute(BIS_LOGICAL_RULE_ID);
// Get the rule details.
$bis_rule_parent = $logical_rule_engine_modal->get_logical_rule($ruleIdParent);

$add_rule_type = $bis_rule_parent['data']->add_rule_type;
?>
<div id="bis_editlogicalrule">
    <div class="panel panel-default">
        <div class="panel-heading dashboard-panel-heading">
            <h3 class="panel-title">
                <label id="bis_re_basic_add_rule">Edit Rule </label>
            </h3>
            <a id='bis_distance_untis_msg_edit' class='popoverData' target='_blank' href='<?php echo BIS_RULES_ENGINE ?>' data-content='<?php _e('Distance criteria should not apply with other criterias.', 'redirectrules'); ?>' rel='popover' data-placement='bottom' data-trigger='hover'>
                <span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span>
            </a>
        </div>
        <div id="bis_re_basic_lr_body" class="panel-body">
            <div class="query-builder-change-rule">
                <label class="radio-inline">
                    <input type="radio" name="bis_add_rule_type" id="bis_add_rule_type_1" value="1"> 
                    <?php _e("Standard Rule", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                </label>
                <label class="radio-inline">
                    <input type="radio" class="bis-query-builder" name="bis_add_rule_type" id="bis_add_rule_type_4" value="4">
                    <?php _e("Advanced Rule", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                </label>
                <!--<label class="radio-inline">
                    <input type="radio" name="bis_add_rule_type" id="bis_add_rule_type_2" value="2">
                <?php _e("Advanced Rule", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                </label>
                <label class="radio-inline">
                    <input type="radio" name="bis_add_rule_type" id="bis_add_rule_type_3" value="3">
                <?php _e("Use Existing Rule", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                </label>-->
            </div>  
            <div id="bis-logical-rule-define">
                <?php
                $pluginPath = RulesEngineUtil::getIncludesDirPath();
                switch ($add_rule_type) {
                    case 1:
                        require_once $pluginPath . "logical-rules-basic-edit-body.php";
                        break;

                    /* case 2:
                      require_once $pluginPath . "logical-rules-advance-edit-body.php";
                      break */

                    /* case 3:
                      require_once $pluginPath . "bis-use-existing-rule.php";
                      break; */
                    case 4:
                        require_once $pluginPath . "logical-rules-query-builder-edit-body.php";
                        break;
                }
                ?>
            </div>
        </div>
    </div>
</div>

<script>

    // wait for the DOM to be loaded
    jQuery(document).ready(function () {
        
        jQuery(".query-builder-change-rule").hide();
        
        var selectedOption = '<?php echo $add_rule_type ?>';
        jQuery("#bis_add_rule_type_"+selectedOption).attr("checked", "checked");
        var req_action = "bis_re_advance_rule_include";
        
        /*jQuery('[name = "bis_add_rule_type"]').click(function() {
            var loadPage = true;
            switch(this.value) {
                case  '1' :
                        req_action = 'bis_re_basic_rule_edit_include';
                        options.data.action = 'bis_re_update_rule_new';  
                        if(selectedOption === '2') {
                           loadPage = false;
                           jQuery("#bis_add_rule_type_"+selectedOption).attr("checked", "checked");
                           bis_confirm("<h4><?php _e('By converting an advance rule to a standard rule you may loose some conditions. Do you want to continue ?') ?></h4>", function (result) {
                                if (result) {
                                   jQuery("#bis_add_rule_type_1").attr("checked", "checked");
                                   bis_showLogicalRuleCriteria();
                                }    
                            }); 
                        }
                        break;
                case  '2' : 
                        req_action = 'bis_re_advance_rule_edit_include';
                        options.data.action = 'bis_re_update_rule_new';
                        break;
                case  '3' : 
                        req_action = 'bis_re_exising_rule_include';
                        options.data.action = 'bis_re_use_exising_rule';
                        break;       
                case  '4' : 
                       req_action = 'bis_re_query_builder_rule_edit_include';
                       options.data.action = 'bis_update_query_builder_logical_rule_new';
                       if(selectedOption === '2') {
                           loadPage = false;
                           jQuery("#bis_add_rule_type_"+selectedOption).attr("checked", "checked");
                           bis_confirm("<h4><?php _e('By converting an advance rule to a standard rule you may loose some conditions. Do you want to continue ?') ?></h4>", function (result) {
                                if (result) {
                                   jQuery("#bis_add_rule_type_1").attr("checked", "checked");
                                   bis_showLogicalRuleCriteria();
                                }    
                            }); 
                        }
                       break;       
            }
           
            if(loadPage) {
              bis_showLogicalRuleCriteria();
            }
        });*/
        
        function bis_showLogicalRuleCriteria() {
            
                var jqXHR = jQuery.get(ajaxurl,
                    {
                        action: req_action,
                        bis_nonce: BISAjax.bis_rules_engine_nonce
                    });

                jqXHR.done(function (data) {
                        jQuery("#bis-logical-rule-define").html(data);
                });
        }
        
       
        
        var options = {
            success: showResponse,
            url: BISAjax.ajaxurl,
            beforeSubmit: bis_validateAddRulesForm,

            data: {
                action: 'bis_update_query_builder_logical_rule_new',
                bis_nonce: BISAjax.bis_rules_engine_nonce
            }
        };

        switch(selectedOption){
            case '1' :
            case '2' :
                options.data.action = 'bis_re_update_rule_new';
                jQuery("#bis_add_rule_type_4").attr("disabled", "disabled");
                break;
            case '3' :
                options.data.action = 'bis_re_use_exising_rule';
                jQuery("#bis_add_rule_type_4").attr("disabled", "disabled");
                break;
            case '4' :
                options.data.action = 'bis_update_query_builder_logical_rule_new';
                jQuery("#bis_add_rule_type_1").attr("disabled", "disabled");
                jQuery("#bis_add_rule_type_2").attr("disabled", "disabled");
                jQuery("#bis_add_rule_type_3").attr("disabled", "disabled");
                break;
        }
        
        function showResponse(responseText, statusText, xhr, $form) {
            if (responseText["status"] === "success") {
                var callback = jQuery("#bis_child_list_callback").val();
                window[callback]();
            } else {
                if (responseText["status"] === "error") {
                    if (responseText["message_key"] === "duplicate_entry") {
                        bis_showErrorMessage('<?php _e("Duplicate rule name, Name should be unique.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                    } else if (responseText["message_key"] === "no_method_found") {
                        bis_showErrorMessage('<?php _e("Action hook method does not exist, Please define method with name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'
                        +" \""+ jQuery("#bis_re_hook").val() + "\".");
                    } else {
                        bis_showErrorMessage('<?php _e("Error occurred while updating rule.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                    }
                }
            }
        }
        jQuery('#bis-editlogicalruleform').ajaxForm(options);

    });
</script>