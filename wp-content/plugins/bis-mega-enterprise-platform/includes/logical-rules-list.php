<span id="logical_rules_list_content">
    <form name="logicalRuleForm" id="logicalRuleForm" method="POST">

        <!-- Search Panel -->
        <div class="panel panel-default">
            <div class="panel-heading dashboard-panel-heading">
                <div>
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="panel-title"><label><?php _e("Logical Rules", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label></h3>
                        </div>
                        <div class="col-md-6">
                                     <strong class="panel-help"><label style="float:right;"><a id="bis_help_url" target="_blank" href="http://docs.megaedzee.com/docs/mega-enterprise-platform/logical-rules/"><?php _e("Help", BIS_RULES_ENGINE_TEXT_DOMAIN); ?> &nbsp;<span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label></strong>
                        </div>    
                    </div>
                    <div class="bis-searh-box-main-container"><a class="search-box-a" href="javascript:void(0)"><i class="glyphicon glyphicon-menu-down bis-glyphi-position"></i><?php _e("Search", "rulesengine"); ?></a></div>
                </div>
            </div>
            <div class="panel-body dashboard-panel post-rules-container">
                <div class="container-fluid search-container">
                    <div>
                        <div class="col-sm-6 pl-0 pr-0">
                            <div class="input-group search-group-postrules">
                                <div class="input-group-btn">
                                    <label><?php _e("Search by ", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>:</label>
                                    <select name="bis_re_search_by" size="2" id="bis_re_search_by">
                                        <option value="name" selected="selected"><?php _e("Name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                        <option value="description"><?php _e("Description", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                    </select>
                                </div>
                                <input type="text" id="bis_re_search_value" name="bis_re_search_value" placeholder='<?php _e("Enter search criteria", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>' class="form-control bis-search-post-rules">
                            </div>
                        </div>
                        <div class="col-sm-4 pr-0">
                            <label><?php _e("Status", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>:</label>
                            <select name="bis_re_search_status" size="2" id="bis_re_search_status">
                                <option selected="selected" value="all"><?php _e("All", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option value="1"><?php _e("Active", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                <option value="0"><?php _e("Deactive", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                            </select>
                            <button class="form-control btn btn-primary bis-main-search-button" type="submit"><?php _e("Go", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                            </button>
                        </div>
                        <div class="pull-right pl-0 pr-0">
                           
                        </div>
                    </div> 
                </div>

                <!-- End of Search Panel -->
                <!-- Start of Results Panel -->
                <div class="bis-table-scroll">
                    <!-- Message span -->
                    <div id="bis-logical-no-result" class="bis-no-results-message alert alert-info text-center"><center></center></div>
                    <div class="col-md-3 pl-0 mb-10">
                        <select name="bis_select_action" size="2" id="bis_select_action" data-placeholder="<?php _e("Bulk Actions", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">
                            <option value="activate"><?php _e("Activate", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                            <option value="deactivate"><?php _e("Deactivate", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                            <option value="delete"><?php _e("Delete", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                        </select>
                    </div>
                    <div class="pull-right">
                        <button id="bis_addlogicalrule" type="button" class="btn btn-primary">
                            <i class="glyphicon glyphicon-plus bis-glyphi-position"></i> <?php _e("Add Rule", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                        </button>
                    </div>
                    <table id="bis-logical-rules-table" style="display: none"
                           class="table table-hover table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="2%"><input type="checkbox" id="bis_check_all" name="checked_id[]" class="bis-checkbox" value=""/></th>
                                <th width="40%"><?php _e("Name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></th>
                                <th><?php _e("Description", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></th>
                                <th width="10%"><?php _e("Status", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></th>
                            </tr>
                        </thead>
                        <tbody id="bis-logicalRulesContent"></tbody>
                    </table>
                </div>
                <!-- End of Results Pannel -->
            </div>
        </div>
        <div>
            <div class="bis-pagination-top col-md-7 pull-left">
                <span class="pagination"></span>
                <input id="searchPagination" type="hidden" value="">
            </div>
            <div class="col-md-2 pull-right">
                <label class="bis-rows-count"><?php _e("Number of Rules", BIS_RULES_ENGINE_TEXT_DOMAIN); ?> </label><span class="badge bis-rows-no">0</span>
            </div>
        </div>
    </form>
</span>
<span id="logical_rules_child_content"> </span>

<script>
    jQuery(document).ready(function () {

        bis_setLogicalSearchButtonWidth();
        bis_logicalResetFields();
        
        var options = {
            success: function(response){
                if(response["status"] === "success") {
                    var row_count = response["total_records"];
                    var total_pages = Math.ceil(row_count/10);
                    var action = response["bis_action"];
                    jQuery('.pagination').show();
                    jQuery('#searchPagination').val(action);
                    bis_logicalBootpages(total_pages);
                    jQuery('.pagination').bootpag({page: 1});
                    jQuery('#bis_select_action').multiselect("disable");
                } else {
                    jQuery('.pagination').hide();
                    jQuery('#bis_select_action').next().hide();
                }
                showResponse(response);
                return false;
            },
            url: BISAjax.ajaxurl,

            data: {
                action: 'bis_re_search_rule',
                bis_nonce: BISAjax.bis_rules_engine_nonce
            }
        };

        jQuery('#logicalRuleForm').ajaxForm(options);

        jQuery("#bis_addlogicalrule").click(function () {

            var jqXHR = jQuery.get(ajaxurl,
                {
                    action: "bis_re_new_rule",
                    bis_nonce: BISAjax.bis_rules_engine_nonce
                });

            jqXHR.done(function (data) {
                jQuery("#logical_rules_child_content").html(data);
                jQuery("#logical_rules_list_content").hide();
                jQuery("#logical_rules_child_content").show();
                jQuery(".pagination").bootpag({page: 1});
            });

        });
        jQuery("#bis_check_all").change(function(){
            jQuery("input:checkbox").prop('checked',jQuery(this).prop("checked"));
        });
        jQuery("#bis_select_action").change(function(){
                bis_logicalRuleAction();
        });
        jQuery("#bis_check_all").click(function() {
            if (jQuery(this).is(':checked')) {
                jQuery('#bis_select_action').multiselect("enable");
            }else{
                jQuery('#bis_select_action').multiselect("disable");
            }
        });
        
        bis_getLogicalPageCount(0);
        
        jQuery('.search-container').hide();
        jQuery(".bis-searh-box-main-container").click(function() {
        jQuery(this).find('i').toggleClass("glyphicon-menu-down glyphicon-menu-up bis-search-position");
           jQuery('.search-container').slideToggle('fast');
        });

    });
</script>
