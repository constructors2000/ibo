<?php
$geoLocation = new bis\repf\util\GeoPluginWrapper();
?>
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading dashboard-panel-heading ">
            <h3 class="panel-title"><?php _e("Geolocation", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h3>
        </div>
        <div class="panel-body">
            <?php

            $clientIP = $geoLocation->getIPAddress();
            if ($clientIP === null || '::1' === $clientIP || $clientIP === "" || $clientIP === "127.0.0.1") {
            ?>  
            <div class="alert alert-danger mega-custom-alert" role="alert"><?php _e("Unable to resolve client IP Address, contact your hosting service provider.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></div>
            <?php } else { ?>
            <div class="alert alert-success alert-dismissible mega-custom-alert" role="alert">
                        <?php _e("Resolved client IP Address", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></div>
            <?php } ?>
            <div class="row form-group">
                <label  class="control-label col-md-2" for="description">
                    <?php _e("IP Address", BIS_RULES_ENGINE_TEXT_DOMAIN); ?> :
                </label>

                <label class="col-md-10">
                    <?php echo $clientIP; ?>
                </label> 
            </div>

            <div class="row form-group">
                <label  class="control-label col-md-2" for="description">
                    <?php _e("Continent", BIS_RULES_ENGINE_TEXT_DOMAIN); ?> :
                </label>
                <label class="col-md-10">
                    <?php echo $geoLocation->getContinentName(); ?>
                </label> 
            </div>
            <div class="row form-group">
                <label  class="control-label col-md-2" for="description">
                    <?php _e("Country", BIS_RULES_ENGINE_TEXT_DOMAIN); ?> :
                </label>
                <label class="col-md-10">
                    
                    <?php 
                    $logical_rule_engine_modal = new bis\repf\model\LogicalRulesEngineModel();
                    echo $logical_rule_engine_modal->get_display_name_by_value($geoLocation->getCountryCode());
                    ?>
                </label> 
            </div>
            <div class="row form-group">
                <label  class="control-label col-md-2" for="description">
                    <?php _e("Currency", BIS_RULES_ENGINE_TEXT_DOMAIN); ?> :
                </label>
                <label class="col-md-10">
                    <?php echo $geoLocation->getCurrencyCode(); ?>
                </label> 
            </div>
            
            <div class="row form-group">
                <label  class="control-label col-md-2" for="description">
                    <?php _e("Region", BIS_RULES_ENGINE_TEXT_DOMAIN); ?> :
                </label>
                <label class="col-md-10">
                    <?php echo $geoLocation->getRegion(); ?>
                </label> 
            </div>

            <div class="row form-group">
                <label  class="control-label col-md-2" for="description">
                    <?php _e("City", BIS_RULES_ENGINE_TEXT_DOMAIN); ?> :
                </label>
                <label class="col-md-10">
                    <?php 
                    
                    if("Other" === $geoLocation->getCity()) { ?>
                    <span style="font-size: 15px;" class="label label-warning">
                            <?php _e("Cities database is not configured", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                    </span>
                    <?php } else {
                        echo $geoLocation->getCity();
                    }
                    
                    ?>
                </label> 
            </div>
            <div class="row form-group">
                <label  class="control-label col-md-2" for="description">
                    <?php _e("Postal Code", BIS_RULES_ENGINE_TEXT_DOMAIN); ?> :
                </label>
                <label class="col-md-10">
                    <?php echo $geoLocation->getPostalCode(); ?>
                </label> 
            </div>
            <div class="row form-group">
                <label  class="control-label col-md-2" for="description">
                    <?php _e("Time Zone", BIS_RULES_ENGINE_TEXT_DOMAIN); ?> :
                </label>
                <label class="col-md-10">
                    <?php echo date_default_timezone_get(). $geoLocation->getTimeZone(); ?>
                </label> 
            </div>
            <div class="row form-group">
                <label  class="control-label col-md-2" for="description">
                    <?php _e("Current Time", BIS_RULES_ENGINE_TEXT_DOMAIN); ?> :
                </label>
                <label class="col-md-10">
                    <?php echo current_time('mysql');?>
                </label> 
            </div>
            <div class="row form-group">
                <label  class="control-label col-md-2" for="description">
                    <?php _e("Geolocation API", BIS_RULES_ENGINE_TEXT_DOMAIN); ?> :
                </label>

                <label class="col-md-10">
                    <?php 
                    if (RulesEngineUtil::get_option(BIS_GEO_LOOKUP_TYPE) == 1) {
                        echo "MaxMind local database";
                        if (RulesEngineUtil::get_option(BIS_GEO_LOOKUP_WEBSERVICE_TYPE) == 1) {
                            echo " & ";
                        }
                    } 
                    if (RulesEngineUtil::get_option(BIS_GEO_IP2LOCATION_CSV_LOOKUP_TYPE) == 1) {
                        echo "IP2Location local CSV database";
                        if (RulesEngineUtil::get_option(BIS_GEO_LOOKUP_WEBSERVICE_TYPE) == 1) {
                            echo " & ";
                        }
                    } 
                    if (RulesEngineUtil::get_option(BIS_GEO_IP2LOCATION_LOOKUP_TYPE) == 1) {
                        echo "IP2Location local BIN database";
                        if (RulesEngineUtil::get_option(BIS_GEO_LOOKUP_WEBSERVICE_TYPE) == 1) {
                            echo " & ";
                        }
                    } 
                    if (RulesEngineUtil::get_option(BIS_GEO_LOOKUP_WEBSERVICE_TYPE) == 1) {
                        echo "Geo plugin webservice";
                    }?>
                </label> 
            </div>
            <div class="row form-group">
                <label  class="control-label col-md-2" for="description">
                    <?php _e("Client IP Look up", BIS_RULES_ENGINE_TEXT_DOMAIN); ?> :
                </label>

                <label class="col-md-10">
                    <?php echo $geoLocation->getIPLookUp(); ?>
                </label> 
            </div>
        </div>
    </div>
</div>