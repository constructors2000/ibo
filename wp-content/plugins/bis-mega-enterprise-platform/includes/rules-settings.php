<?php
$bis_delete_db = RulesEngineUtil::get_option(BIS_RULES_ENGINE_DELETE_DB);
$bis_capture_analytics = RulesEngineUtil::get_option(BIS_CAPTURE_ANALYTICS_DATA);
$installed_version = RulesEngineUtil::get_option(BIS_RULES_ENGINE_VERSION_CONST);
$cat_addon_installed_version = RulesEngineUtil::get_option("BIS_CATEGORY_CONTROLLER_VERSION");
$bis_geoname_user = RulesEngineUtil::get_option(BIS_GEO_NAME_USER);
$bis_config_tab = "active in";

$bis_activate_plugins = "";
$bis_config_ac_tab = "active";
$bis_plugin_ac_tab = "deactive";

$bis_geo_lookup_type = RulesEngineUtil::get_option(BIS_GEO_LOOKUP_TYPE);
$bis_geo_lookup_webservice_type = RulesEngineUtil::get_option(BIS_GEO_LOOKUP_WEBSERVICE_TYPE);
$bis_geo_maxmind_db_file = RulesEngineUtil::get_option(BIS_GEO_MAXMIND_DB_FILE);
$bis_ip2_location_db_file = RulesEngineUtil::get_option(BIS_GEO_IP2LOCATION_DB);
$bis_ip2_location_csv_db_file = RulesEngineUtil::get_option(BIS_GEO_IP2LOCATION_CSV_DB);
$bis_ip2location_lookup_type = RulesEngineUtil::get_option(BIS_GEO_IP2LOCATION_LOOKUP_TYPE);
$bis_ip2location_csv_lookup_type = RulesEngineUtil::get_option(BIS_GEO_IP2LOCATION_CSV_LOOKUP_TYPE);
if (isset($_GET["stab"])) {
    $bis_activate_plugins = "active in";
    $bis_config_tab = "";
    $bis_config_ac_tab = "deactive";
    $bis_plugin_ac_tab = "active";
}

$checked = '';
if ($bis_delete_db === "true") {
    $checked = "checked";
}

?>
<span id="bis_setting_rule_tab">
    <span id="setting-rules-list-content">
        <!-- Search Panel -->
        <div class="panel panel-default">
            <div class="panel-heading dashboard-panel-heading">
                <div>
                    <div class="row">
                        <div class="col-md-4">
                            <h3 class="panel-title"><label><?php _e("Global Settings", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label></h3>
                        </div>
                        <div class="col-md-8">
                            <strong class="panel-help"><label style="float:right;"><a id="bis_help_url" target="_blank" href="http://docs.megaedzee.com/docs/mega-enterprise-platform/global-settings/">Help &nbsp;<span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label></strong>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body dashboard-panel">
                <ul role="tablist" class="nav nav-tabs" id="myTabs">
                    <li class="<?php echo $bis_config_ac_tab ?>" role="presentation">
                        <a aria-expanded="false" aria-controls="installed" data-toggle="tab" 
                           role="tab" id="config-tab" href="#installed"><?php _e("Configuration", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></a>
                    </li>
                    <li class="<?php echo $bis_plugin_ac_tab ?>" role="presentation">
                        <a aria-expanded="false" aria-controls="active-plugins" data-toggle="tab" 
                           role="tab" id="active-plugins-tab" href="#active-plugins"><?php _e("Activate Plugins", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent" style="margin-top: 20px;">
                    <div aria-labelledby="config-tab" id="installed" 
                         class="tab-pane fade <?php echo $bis_config_tab; ?>" role="tabpanel">
                             <?php include 'configuration.php'; ?>
                    </div>
                    <div aria-labelledby="active-plugins-tab" id="active-plugins" 
                         class="tab-pane fade <?php echo $bis_activate_plugins ?>" role="tabpanel">
                        <div class="row" style="margin-top: 20px;">
                            <?php include 'plugin-versions.php'; ?>
                        </div>
                    </div> 
                </div>              
            </div>
        </div>
</span>

