<?php
use bis\repf\model\LogicalRulesEngineModel;
use bis\repf\util\GeoPluginWrapper;

$popUpVO = json_decode(RulesEngineUtil::get_option(BIS_POPUP_VO, false, false));
if($popUpVO == null) {
    $popUpVO = new bis\repf\vo\PopUpVO();
}

$plugin_rule_dir = BIS_PRODUCT_RULE_DIRECTORY;
$logical_rule_engine_modal = new LogicalRulesEngineModel();
$images_list = $logical_rule_engine_modal->get_images_from_media_library();
$pos_img_sizes = $logical_rule_engine_modal->get_rule_values(BIS_CONTENT_IMAGE_SIZE);
$file_path = RulesEngineUtil::get_templates_file_path();
$scanned_directory = array_diff(scandir($file_path), array('..', '.'));
?>

<form name="bis_re_addpopupconfigform" id="bis_re_addpopupconfigform" method="POST">
<div class="container-fluid">
    <div class="alert alert-success alert-dismissable bis-message-position mega-custom-alert" 
         id="bis_save_mega_popup_config_banner" >
        <button class="close mega-close" id="bis_save_mega_popup_config_banner_close" type="button"
                aria-hidden="true">&times;</button>
        <strong><span id="bis_save_mega_popup_config_banner_open"> </span></strong>
    </div>
    <div class="row" id="bis_common_popup_pannel">
        <div class="form-group col-md-12">
            <div class="panel panel-default bis-popup-configure">
                <div class="panel-heading bis-css-popup-customization-tittle">
                    <div class="container-fluid">
                        <div class="row">
                            <h3 class="panel-title"><label><?php _e("PopUp Configuration", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label></h3>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="description"><?php _e("Select Template", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                            <select name="bis_re_template" id="bis_re_template">
                                <?php
                                $defaul_sel = "";
                                if (isset($popUpVO) && isset($popUpVO->tempalteFileName) && $popUpVO->tempalteFileName == "bis-popup-template.html") {
                                    $defaul_sel = "selected=selected";
                                }
                                ?>
                                <option  <?php echo $defaul_sel ?> value="<?php echo "bis-popup-template.html" ?>"><?php echo "Default Template" ?></option>
                                <?php
                                foreach ($scanned_directory as $file_name) {
                                    if (RulesEngineUtil::isContains($file_name, ".html")) {
                                        $tempalte_sel = "";
                                        if (isset($popUpVO) && isset($popUpVO->tempalteFileName) && $popUpVO->tempalteFileName == $file_name) {
                                            $tempalte_sel = "selected=selected";
                                        }
                                        ?>
                                        <option  <?php echo $tempalte_sel ?> value="<?php echo $file_name ?>"><?php echo $file_name; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select> 
                        </div>
                        <div class="form-group col-md-3">
                            <label><?php _e("Popup Close Time", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                            <a class="popoverData" target="_blank" 
                               data-content='<?php _e("Show popup in seconds, Never indicate no auto redirect and user action is must.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>' rel="popover"
                               data-placement="bottom" data-trigger="hover">
                                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                            </a>
                            <div>
                                <?php
                                $time_selected = 0;

                                if ($popUpVO != null) {
                                    $time_selected = $popUpVO->autoCloseTime;
                                }

                                $redirect_time = RulesEngineUtil::get_auto_redirect_times();
                                ?>
                                <select name="bis_re_common_auto_red" class="form-control" id="bis_re_common_auto_red" style="width:353px!important;">
                                    <?php
                                    foreach ($redirect_time as $value) {
                                        $time_sel_attr = "";

                                        if ($time_selected == $value->id) {
                                            $time_sel_attr = "selected=selected";
                                        }
                                        ?>
                                        <option <?php echo $time_sel_attr ?> value="<?php echo $value->id ?>"><?php echo $value->label ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="description"><?php _e("Show PopUp", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                            <a class="popoverData" target="_blank" 
                               data-content='<?php _e("Shows PopUp always or one time, default is always.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>' rel="popover"
                               data-placement="bottom" data-trigger="hover">
                                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                            </a>
                            <div>
                                <select name="bis_re_common_popup_occur" class="form-control" id="bis_re_common_popup_occur">
                                    <?php
                                    $show_always_sel = "selected=selected";
                                    $show_once_sel = "";

                                    if (isset($popUpVO) && isset($popUpVO->popUpOccurr) && $popUpVO->popUpOccurr == 1) {
                                        $show_always_sel = "";
                                        $show_once_sel = "selected=selected";
                                    }
                                    ?>
                                    <option <?php echo $show_always_sel ?> value="0"><?php _e("Always", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                    <option <?php echo $show_once_sel ?> value="1"><?php _e("Once", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-3 pull-right">
                            <label><?php _e("Status", "rulesengine"); ?></label>
                            <div>
                                <select name="bis_re_rule_status" id="bis_re_rule_status">
                                    <option value="1"><?php _e("Active", "rulesengine"); ?></option>
                                    <option value="0"><?php _e("Deactive", "rulesengine"); ?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="bis_select_country_div" class="form-group col-md-4" style="display: none;">
                            <label for="description"><?php _e("Select Country", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                            <?php
                            $all_countries = $logical_rule_engine_modal->get_all_countries();
                            ?>
                            <div>
                                <select name="bis_re_select_country[]" multiple="multiple" id="bis_re_select_country"
                                        size="2">
                                    <?php foreach ($all_countries as $country) { ?>

                                        <option value="<?php echo $country->value ?>">
                                        <?php echo $country->display_name; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div id="bis_select_continent_div" class="form-group col-md-4" style="display: none;">
                            <label for="description"><?php _e("Select Continent", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                            <?php
                            $geo_wrapper = new GeoPluginWrapper();
                            $continents = $geo_wrapper->getContinentMap();
                            ?>
                            <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                            <div>
                                <select name="bis_re_select_continent[]" multiple="multiple" id="bis_re_select_continent"
                                        size="2">
                                    <?php foreach ($continents as $key => $continent) { ?>
                                        <option value="<?php echo $key ?>"><?php echo $continent; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="description"><?php _e("Title", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                            </label>
                            <input type="text"  class="form-control" maxlength="100"
                                   id="bis_re_common_popup_title" name="bis_re_common_popup_title" placeholder="Enter PopUp title">    
                        </div>
                        <div class="form-group col-md-4 pull-right">
                            <label><?php _e("Select Image", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                            <div>
                                <select name="bis_re_image_id" id="bis_re_image_id" size="2">
                                    <?php foreach ($images_list as $image) { ?>
                                        <option value="<?php echo $image->id ?>"
                                                id="<?php echo $image->id ?>"><?php echo $image->name ?></option>
                                            <?php } ?>
                                </select>
                            </div>    
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="description"><?php _e("Heading", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                            <input type="text"  class="form-control" maxlength="500"
                                   id="bis_re_common_heading" name="bis_re_common_heading" placeholder="Enter heading">    
                        </div>
                        <div class="form-group col-md-4">
                            <label for="description"><?php _e("Sub Heading", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                            <input type="text"  class="form-control" maxlength="500"
                                   id="bis_re_common_sub_heading" name="bis_re_common_sub_heading" placeholder="Enter sub heading">    
                        </div>
                        <div class="form-group col-md-4" id="bis_image_size" style="display: none;">
                            <label><?php _e("Image Size", "rulesengine"); ?></label>
                            <a class="popoverData" target="_blank"
                               data-content='<?php _e("It is recommended to use Thumbnail or Medium images, Some Full or Large images may not fit in the PopUp.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>' rel="popover"
                               data-placement="bottom" data-trigger="hover">
                                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                            </a>
                            <div>
                                <select name="bis_re_cont_img_size" id="bis_re_cont_img_size" size="2">
                                    <?php foreach ($pos_img_sizes as $img_size) { ?>
                                        <option value="<?php echo $img_size->value ?>"
                                                id="<?php echo $img_size->value ?>"><?php echo $img_size->name ?></option>
                                            <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="bis_popup_buttons_div">
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="description"><?php _e("Button-1 Label", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                                </label>
                                <input type="text"  class="form-control" maxlength="100"
                                       id="bis_re_btn1_label" name="bis_re_btn1_label" placeholder="<?php _e("Enter button 1 label", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                            </div>
                            <div class="form-group col-md-4">
                                <label for="description"><?php _e("Button-1 URL", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                                <a class="popoverData" target="_blank"
                                   data-content='<?php _e("Enter the url to redirect the any site. This url will be used for opening to the specified website.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>' rel="popover"
                                   data-placement="bottom" data-trigger="hover">
                                    <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                                </a>
                                <input type="text" class="form-control" maxlength="100" 
                                       id="bis_re_but1_url" name="bis_re_but1_url" placeholder="<?php _e("Enter button 1 url", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="description"><?php _e("Button-2 Label", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
                                </label>
                                <input type="text"  class="form-control" maxlength="100"
                                       id="bis_re_but2_label" name="bis_re_but2_label" placeholder="<?php _e("Enter button 2 label", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                            </div>
                            <div class="form-group col-md-4">
                                <label for="description"><?php _e("Button-2 URL", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                                <a class="popoverData" target="_blank"
                                   data-content='<?php _e("Enter the url to redirect the any site. This url will be used for opening to the specified website.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>' rel="popover"
                                   data-placement="bottom" data-trigger="hover">
                                    <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                                </a>

                                <input type="text" class="form-control" maxlength="100" 
                                       id="bis_re_but2_url" name="bis_re_but2_url" placeholder="<?php _e("Enter button 2 url", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                            </div>
                        </div>   
                    </div>
                    <div class="col-sm-9 pl-0" style="padding-right: 100px;">
                        <div class="panel panel-default ">
                            <h4 class="panel-tittle bis-css-popup-customization-tittle"><?php _e("CSS Customization", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></h4>
                            <div class="panel-body" id="bis_cust_popup_panel_body" >
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="description"><?php _e("PopUp Background Color", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                                        <input type="text"  class="form-control" maxlength="100"
                                               id="bis_re_common_popup_color" name="bis_re_common_popup_color" placeholder="<?php _e("Enter PopUp background in Hex Color Codes (ex: #80EEEE).", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="popup title"><?php _e("Title CSS Class", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                                        <input type="text"  class="form-control" maxlength="100"
                                               id="bis_re_common_popup_title_class" name="bis_re_common_popup_title_class" placeholder="<?php _e("Enter PopUp title css class name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="description"><?php _e("Heading CSS Class", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                                        <input type="text"  class="form-control" maxlength="100"
                                               id="bis_re_common_heading_class" name="bis_re_common_heading_class" placeholder="<?php _e("Enter heading css class name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="description"><?php _e("Sub Heading CSS Class", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                                        <input type="text"  class="form-control" maxlength="100"
                                               id="bis_re_common_sub_heading_class" name="bis_re_common_sub_heading_class" placeholder="<?php _e("Enter sub heading css class name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                                    </div>
                                </div>
                                <div id="bis_popup_buttons_css_div" class="row">
                                    <div class="form-group col-md-6">
                                        <label for="description"><?php _e("Button-1 CSS Class", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                                        <input type="text"  class="form-control" maxlength="100"
                                               id="bis_re_btn1_class" name="bis_re_btn1_class" placeholder="<?php _e("Enter button 1 css class name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="description"><?php _e("Button-2 CSS Class", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></label>
                                        <input type="text"  class="form-control" maxlength="100"
                                               id="bis_re_but2_class" name="bis_re_but2_class" placeholder="<?php _e("Enter button 2 class name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">    
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="row">
        <div class="col-md-10 pull-left">
            <button id="bis_re_save_popup_settings" class="btn btn-primary">
                <i class="glyphicon glyphicon-ok-sign bis-glyphi-position"></i><?php _e("Save Popup", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>
            </button>
        </div>
    </div>
</div>
</form>
    
<script>
    // wait for the DOM to be loaded
    jQuery(document).ready(function () {

        jQuery('#bis_re_common_auto_red').multiselect({
            buttonWidth: '350px'
        });

        jQuery('#bis_re_rule_status').multiselect({
            buttonWidth: '150px'
        });

        jQuery('#bis_re_common_popup_occur').multiselect({
            buttonWidth: '250px'
        });

        jQuery('#bis_re_template').multiselect({
            maxHeight: 300,
            nonSelectedText: '<?php _e("Select Template", "rulesengine"); ?>',
            buttonWidth: '250px'
        });

        jQuery('#bis_re_select_country').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            maxHeight: 300,
            nonSelectedText: '<?php _e("Select Country", "rulesengine"); ?>',
            buttonWidth: '350px'
        });
        
        jQuery('#bis_re_select_continent').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            maxHeight: 300,
            nonSelectedText: '<?php _e("Select Continent", "rulesengine"); ?>',
            buttonWidth: '300px'
        });
        
        jQuery('#bis_re_cont_img_size').multiselect({
            buttonWidth: '250px'
        });
        
        jQuery("#bis_re_template").change(function(){
            var template = jQuery("#bis_re_template").val();
            if(template.indexOf("bis-popup") >=0) {
                jQuery("#bis_popup_buttons_div").show();
                jQuery("#bis_popup_buttons_css_div").show();
                jQuery("#bis_re_select_country").multiselect('deselect', jQuery("#bis_re_select_country").val());
                jQuery("#bis_re_select_continent").multiselect('deselect', jQuery("#bis_re_select_continent").val());
                jQuery("#bis_re_image_id").multiselect('deselect', jQuery("#bis_re_image_id").val());
                jQuery("#bis_re_cont_img_size").multiselect('deselect', jQuery("#bis_re_cont_img_size").val());
                jQuery("#bis_select_country_div").hide();
                jQuery("#bis_select_continent_div").hide();
            } else if(template.indexOf("country-flags") >= 0) {
                jQuery("#bis_select_country_div").show();
                jQuery("#bis_re_select_country").multiselect('deselect', jQuery("#bis_re_select_country").val());
                jQuery("#bis_re_select_continent").multiselect('deselect', jQuery("#bis_re_select_continent").val());
                jQuery("#bis_re_image_id").multiselect('deselect', jQuery("#bis_re_image_id").val());
                jQuery("#bis_re_cont_img_size").multiselect('deselect', jQuery("#bis_re_cont_img_size").val());
                jQuery("#bis_popup_buttons_div").hide();
                jQuery("#bis_popup_buttons_css_div").hide();
                jQuery("#bis_select_continent_div").hide();
            } else if(template.indexOf("continent-country") >= 0) {
                jQuery("#bis_select_country_div").show();
                jQuery("#bis_re_select_country").multiselect('deselect', jQuery("#bis_re_select_country").val());
                jQuery("#bis_re_select_continent").multiselect('deselect', jQuery("#bis_re_select_continent").val());
                jQuery("#bis_re_image_id").multiselect('deselect', jQuery("#bis_re_image_id").val());
                jQuery("#bis_re_cont_img_size").multiselect('deselect', jQuery("#bis_re_cont_img_size").val());
                jQuery("#bis_select_continent_div").hide();
                jQuery("#bis_popup_buttons_div").hide();
                jQuery("#bis_popup_buttons_css_div").hide();
            } else if(template.indexOf("country") >= 0) {
                jQuery("#bis_select_country_div").show();
                jQuery("#bis_re_select_country").multiselect('deselect', jQuery("#bis_re_select_country").val());
                jQuery("#bis_re_select_continent").multiselect('deselect', jQuery("#bis_re_select_continent").val());
                jQuery("#bis_re_image_id").multiselect('deselect', jQuery("#bis_re_image_id").val());
                jQuery("#bis_re_cont_img_size").multiselect('deselect', jQuery("#bis_re_cont_img_size").val());
                jQuery("#bis_popup_buttons_div").hide();
                jQuery("#bis_popup_buttons_css_div").hide();
                jQuery("#bis_select_continent_div").hide();
            } else if(template.indexOf("continent") >= 0) {
                jQuery("#bis_select_continent_div").show();
                jQuery("#bis_re_select_country").multiselect('deselect', jQuery("#bis_re_select_country").val());
                jQuery("#bis_re_select_continent").multiselect('deselect', jQuery("#bis_re_select_continent").val());
                jQuery("#bis_re_image_id").multiselect('deselect', jQuery("#bis_re_image_id").val());
                jQuery("#bis_re_cont_img_size").multiselect('deselect', jQuery("#bis_re_cont_img_size").val());
                jQuery("#bis_select_country_div").hide();
                jQuery("#bis_popup_buttons_div").hide();
                jQuery("#bis_popup_buttons_css_div").hide();
            } else if(template.indexOf("city") >= 0) {
                jQuery("#bis_select_continent_div").hide();
                jQuery("#bis_re_select_country").multiselect('deselect', jQuery("#bis_re_select_country").val());
                jQuery("#bis_re_select_continent").multiselect('deselect', jQuery("#bis_re_select_continent").val());
                jQuery("#bis_re_image_id").multiselect('deselect', jQuery("#bis_re_image_id").val());
                jQuery("#bis_re_cont_img_size").multiselect('deselect', jQuery("#bis_re_cont_img_size").val());
                jQuery("#bis_select_country_div").hide();
                jQuery("#bis_popup_buttons_div").hide();
                jQuery("#bis_popup_buttons_css_div").hide();
            } else if(template.indexOf("state") >= 0) {
                jQuery("#bis_select_continent_div").hide();
                jQuery("#bis_re_select_country").multiselect('deselect', jQuery("#bis_re_select_country").val());
                jQuery("#bis_re_select_continent").multiselect('deselect', jQuery("#bis_re_select_continent").val());
                jQuery("#bis_re_image_id").multiselect('deselect', jQuery("#bis_re_image_id").val());
                jQuery("#bis_re_cont_img_size").multiselect('deselect', jQuery("#bis_re_cont_img_size").val());
                jQuery("#bis_select_country_div").hide();
                jQuery("#bis_popup_buttons_div").hide();
                jQuery("#bis_popup_buttons_css_div").hide();
            }
        });
        
        jQuery('#bis_re_image_id').multiselect({
            enableCaseInsensitiveFiltering: true,
            maxHeight: 250,
            buttonWidth: '250px',
            enableHTML      : true,
            optionLabel     : function(element) {
                //return '<img height="30px" width="30px" src="'+jQuery(element).attr('data-img')+'"> '+jQuery(element).text();
                return jQuery(element).text();
            },
            onChange: function (element, checked) {
                jQuery("#bis_image_size").show();
            }
        });

<?php
$popUpOccurr = "0";
//$redirectWindow = "0";

if ($popUpVO != null) {
    if (isset($popUpVO->popUpOccurr)) {
        $popUpOccurr = $popUpVO->popUpOccurr;
    }
    
    if (isset($popUpVO->status)) {
        $status = $popUpVO->status;
    }

    /*if (isset($popUpVO->redirectWindow)) {
        $redirectWindow = $popUpVO->redirectWindow;
    }*/
}
?> ;
        
        var options = {
            success: showPopupResponse,
            url: BISAjax.ajaxurl,
            data: {
                action: 'bis_re_save_popup_settings',
                bis_nonce: BISAjax.bis_rules_engine_nonce
            }
        };

        jQuery('#bis_re_addpopupconfigform').ajaxForm(options);
        
        function showPopupResponse() {
            jQuery("#bis_save_mega_popup_config_banner_open").html(breCV.bis_save_mega_popup_config);
            jQuery("#bis_save_mega_popup_config_banner").show();

            jQuery("#bis_save_mega_popup_config_banner_close").click(function () {
                jQuery("#bis_save_mega_popup_config_banner").hide();
            });
        }
        

        var selectedTemplate = '<?php echo $popUpVO ? $popUpVO->tempalteFileName : "" ?>';
        if(selectedTemplate.indexOf("bis-popup") >=0) {
            jQuery("#bis_popup_buttons_div").show();
            jQuery("#bis_popup_buttons_css_div").show();
            jQuery("#bis_re_select_country").multiselect('deselect', jQuery("#bis_re_select_country").val());
            jQuery("#bis_re_select_continent").multiselect('deselect', jQuery("#bis_re_select_continent").val());
            jQuery("#bis_re_image_id").multiselect('deselect', jQuery("#bis_re_image_id").val());
            jQuery("#bis_re_cont_img_size").multiselect('deselect', jQuery("#bis_re_cont_img_size").val());
            jQuery("#bis_select_country_div").hide();
            jQuery("#bis_select_continent_div").hide();
        } else if(selectedTemplate.indexOf("country-flags") >= 0) {
            jQuery("#bis_select_country_div").show();
            jQuery("#bis_re_select_country").multiselect('deselect', jQuery("#bis_re_select_country").val());
            jQuery("#bis_re_select_continent").multiselect('deselect', jQuery("#bis_re_select_continent").val());
            jQuery("#bis_re_image_id").multiselect('deselect', jQuery("#bis_re_image_id").val());
            jQuery("#bis_re_cont_img_size").multiselect('deselect', jQuery("#bis_re_cont_img_size").val());
            jQuery("#bis_popup_buttons_div").hide();
            jQuery("#bis_popup_buttons_css_div").hide();
            jQuery("#bis_select_continent_div").hide();
            var selected_countries = jQuery.parseJSON('<?php echo json_encode($popUpVO->selectedCountries); ?>');
            jQuery.each(selected_countries, function (index, data) {
                jQuery("#bis_re_select_country").multiselect('select', index);
            });
        } else if(selectedTemplate.indexOf("continent-country") >= 0) {
            jQuery("#bis_select_country_div").show();
            jQuery("#bis_re_select_country").multiselect('deselect', jQuery("#bis_re_select_country").val());
            jQuery("#bis_re_select_continent").multiselect('deselect', jQuery("#bis_re_select_continent").val());
            jQuery("#bis_re_image_id").multiselect('deselect', jQuery("#bis_re_image_id").val());
            jQuery("#bis_re_cont_img_size").multiselect('deselect', jQuery("#bis_re_cont_img_size").val());
            jQuery("#bis_select_continent_div").hide();
            jQuery("#bis_popup_buttons_div").hide();
            jQuery("#bis_popup_buttons_css_div").hide();
            var selected_countries = jQuery.parseJSON('<?php echo json_encode($popUpVO->selectedCountries); ?>');
            jQuery.each(selected_countries, function (index, data) {
                jQuery("#bis_re_select_country").multiselect('select', data);
            });
        } else if(selectedTemplate.indexOf("country") >= 0) {
            jQuery("#bis_select_country_div").show();
            jQuery("#bis_re_select_country").multiselect('deselect', jQuery("#bis_re_select_country").val());
            jQuery("#bis_re_select_continent").multiselect('deselect', jQuery("#bis_re_select_continent").val());
            jQuery("#bis_re_image_id").multiselect('deselect', jQuery("#bis_re_image_id").val());
            jQuery("#bis_re_cont_img_size").multiselect('deselect', jQuery("#bis_re_cont_img_size").val());
            jQuery("#bis_popup_buttons_div").hide();
            jQuery("#bis_popup_buttons_css_div").hide();
            jQuery("#bis_select_continent_div").hide();
            var selected_countries = jQuery.parseJSON('<?php echo json_encode($popUpVO->selectedCountries); ?>');
            jQuery.each(selected_countries, function (index, data) {
                jQuery("#bis_re_select_country").multiselect('select', index);
            });
        } else if(selectedTemplate.indexOf("continent") >= 0) {
            jQuery("#bis_select_continent_div").show();
            jQuery("#bis_re_select_country").multiselect('deselect', jQuery("#bis_re_select_country").val());
            jQuery("#bis_re_select_continent").multiselect('deselect', jQuery("#bis_re_select_continent").val());
            jQuery("#bis_re_image_id").multiselect('deselect', jQuery("#bis_re_image_id").val());
            jQuery("#bis_re_cont_img_size").multiselect('deselect', jQuery("#bis_re_cont_img_size").val());
            jQuery("#bis_select_country_div").hide();
            jQuery("#bis_popup_buttons_div").hide();
            jQuery("#bis_popup_buttons_css_div").hide();
            var selected_continents = jQuery.parseJSON('<?php echo json_encode($popUpVO->selectedCountries); ?>');
            jQuery.each(selected_continents, function (index, data) {
                jQuery("#bis_re_select_continent").multiselect('select', index);
            });
        } else if(selectedTemplate.indexOf("city") >= 0) {
            jQuery("#bis_select_continent_div").hide();
            jQuery("#bis_re_select_country").multiselect('deselect', jQuery("#bis_re_select_country").val());
            jQuery("#bis_re_select_continent").multiselect('deselect', jQuery("#bis_re_select_continent").val());
            jQuery("#bis_re_image_id").multiselect('deselect', jQuery("#bis_re_image_id").val());
            jQuery("#bis_re_cont_img_size").multiselect('deselect', jQuery("#bis_re_cont_img_size").val());
            jQuery("#bis_select_country_div").hide();
            jQuery("#bis_popup_buttons_div").hide();
            jQuery("#bis_popup_buttons_css_div").hide();
        } else if(selectedTemplate.indexOf("state") >= 0) {
            jQuery("#bis_select_continent_div").hide();
            jQuery("#bis_re_select_country").multiselect('deselect', jQuery("#bis_re_select_country").val());
            jQuery("#bis_re_select_continent").multiselect('deselect', jQuery("#bis_re_select_continent").val());
            jQuery("#bis_re_image_id").multiselect('deselect', jQuery("#bis_re_image_id").val());
            jQuery("#bis_re_cont_img_size").multiselect('deselect', jQuery("#bis_re_cont_img_size").val());
            jQuery("#bis_select_country_div").hide();
            jQuery("#bis_popup_buttons_div").hide();
            jQuery("#bis_popup_buttons_css_div").hide();
        }
        
        var image = '<?php echo $popUpVO ? $popUpVO->imageOneId : "" ?>';
        
        if(image !== "") {
            jQuery("#bis_re_image_id").multiselect('select', '<?php echo $popUpVO ? $popUpVO->imageOneId : "" ?>');
            jQuery("#bis_re_cont_img_size").multiselect('select', '<?php echo $popUpVO ? $popUpVO->imageOneSize : "" ?>');
            jQuery("#bis_image_size").show();
        }

        jQuery("#bis_re_rule_status").multiselect('deselect', jQuery("#bis_re_rule_status").val());
        jQuery("#bis_re_rule_status").multiselect('select', '<?php echo $popUpVO ? $popUpVO->status : 1 ?>');
        jQuery("#bis_re_common_popup_occur option:selected").removeAttr("selected");
        jQuery("#bis_re_common_popup_occur").multiselect('select', '<?php echo $popUpOccurr ?>');
        //jQuery("#bis_re_common_red_window option:selected").removeAttr("selected");
        //jQuery("#bis_re_common_red_window").multiselect('select', '<?php //echo $redirectWindow ?>');
        jQuery("#bis_re_common_popup_title").val('<?php echo $popUpVO ? $popUpVO->title : "" ?>');
        jQuery("#bis_re_common_popup_color").val('<?php echo $popUpVO ? $popUpVO->popUpBackgroundColor : "" ?>');
        jQuery("#bis_re_common_heading").val('<?php echo $popUpVO ? $popUpVO->headingOne : "" ?>');
        jQuery("#bis_re_common_heading_class").val('<?php echo $popUpVO ? $popUpVO->headingOneClass : "" ?>');
        jQuery("#bis_re_common_sub_heading").val('<?php echo $popUpVO ? $popUpVO->headingTwo : "" ?>');
        jQuery("#bis_re_common_sub_heading_class").val('<?php echo $popUpVO ? $popUpVO->headingTwoClass : "" ?>');
        jQuery("#bis_re_btn1_label").val('<?php echo $popUpVO ? $popUpVO->buttonLabelOne : "" ?>');
        jQuery("#bis_re_btn1_class").val('<?php echo $popUpVO ? $popUpVO->buttonOneClass : "" ?>');
        jQuery("#bis_re_but2_label").val('<?php echo $popUpVO ? $popUpVO->buttonLabelTwo : "" ?>');
        jQuery("#bis_re_but2_class").val('<?php echo $popUpVO ? $popUpVO->buttonTwoClass : "" ?>');
        jQuery("#bis_re_common_popup_title_class").val('<?php echo $popUpVO ? $popUpVO->titleClass : "" ?>');
        jQuery("#bis_re_but1_url").val('<?php echo $popUpVO ? $popUpVO->buttonOneUrl : "" ?>');
        jQuery("#bis_re_but2_url").val('<?php echo $popUpVO ? $popUpVO->buttonTwoUrl : "" ?>');

    });
</script>    