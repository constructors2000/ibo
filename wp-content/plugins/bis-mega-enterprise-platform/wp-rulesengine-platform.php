<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

/*
 * Plugin Name: Mega Enterprise Platform
 * Plugin URI: http://megaedzeetechnologies.com/
 * Description: Mega Enterprise Platform is designed for defining logical rules using User Profile, User Role, Geolocation, Date and Time etc .</strong> To know more read documentation or watch quick start <a target="_blank"  href="http://rulesengine.in/">video tutorials </a>.
 * Version: 1.15
 * Author: MegaEdzee Technologies
 * Author URI:  http://megaedzeetechnologies.com/
 * Text Domain: rulesengine
 * Domain Path: /languages/
 * License: Commercial
 *
 */

use bis\repf\common\BISSession;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (!class_exists('BISRE_Professional_Platform')) {

    final class BISRE_Professional_Platform {
         
        public $version = '1.15';
        private static $_instance = null;

        public static function instance() {

            if (is_null(self::$_instance)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        public function __clone() {
            _doing_it_wrong(__FUNCTION__, __('You cannot clone Mega Enterprise Platform', BIS_RULES_ENGINE_TEXT_DOMAIN), '1.15');
        }

        public function __wakeup() {
            _doing_it_wrong(__FUNCTION__, __('You cannot wakeup Mega Enterprise Platform', BIS_RULES_ENGINE_TEXT_DOMAIN), '1.15');
        }

        private function __construct() {
            $this->define_constants();
            $this->includes();
            $this->init_hooks();
            //do_action('rulesengineplatform_loaded');
        }

        private function init_hooks() {
            $bis_common_platform = new BISRE_Common_Platform();
            $bis_common_platform->includes();
            $bis_common_platform->init_hooks();
            BISSession::getInstance();
            $bisre_plugin_install = new bis\repf\install\BISRE_Plugin_Install();

            register_activation_hook(__FILE__, array($bisre_plugin_install, 'bis_rules_engine_activation'));
            register_deactivation_hook(__FILE__, array($bisre_plugin_install, 'bis_rules_engine_deactivate'));
            register_uninstall_hook(__FILE__, 'BISRE_Common_Platform::bis_rules_engine_uninstall');
            
            if (is_multisite()) {
                //$current_site = get_current_site()->domain;
                add_action('network_admin_menu', array(&$this, 'bis_show_re_network_menu'));
                //if (!(\RulesEngineUtil::isContains($current_site, BIS_DEMO_SITE))) {
                    add_action('wpmu_new_blog', array(&$bisre_plugin_install, 'bis_on_site_create'), 10, 6);
                    add_filter('wpmu_drop_tables', array(&$bisre_plugin_install, 'bis_on_site_delete'));
                //}
            } else {
                add_filter('plugin_action_links_' . plugin_basename(__FILE__), array(&$this, 'bis_plugin_add_settings_link'));
            }
        }
        
        /**
         * Define Platform Constants.
         */
        private function define_constants() {
            $upload_dir = wp_upload_dir();

            $this->define("BIS_RULES_ENGINE_VERSION", $this->version);
            $this->define("BIS_RULES_ENGINE_PLATFORM_TYPE", "BIS_RE_PROF_EDITION");

            $dirName = dirname(__FILE__);
            $realPath = realpath($dirName);
            
            $this->define("BIS_RULES_ENGINE_PLATFORM_PATH", $realPath);
            $this->define('BIS_RULES_ENGINE_PLUGIN_FILE', __FILE__);
            $this->define('BIS_RULES_ENGINE_PLUGIN_BASENAME', plugin_basename(__FILE__));
            $this->define('BIS_RULES_ENGINE_LOG_DIR', $upload_dir['basedir'] . '/bis-rulesengine-logs/');
            $this->define('BIS_RULES_ENGINE_SESSION_CACHE_GROUP', 'bis_rulesengine_session_id');
        }

        /**
         * Define constant names if not already set.
         *
         * @param  string $name
         * @param  string|bool $value
         */
        private function define($name, $value) {
            if (!defined($name)) {
                define($name, $value);
            }
        }
        
        /**
         * This method is used for adding Settings link in plugins page.
         * 
         * @param type $links
         * @return type
         */
        function bis_plugin_add_settings_link($links) {
            unset($links['edit']);
            unset($links[0]);
            $settings_link = '<a href="' . admin_url('admin.php?page=bis_pg_global_settings') . '">' . __("Settings", BIS_RULES_ENGINE_TEXT_DOMAIN) . '</a>';
            array_unshift($links, $settings_link);
            return $links;
        }

        /**
         * This menu is shown for network dashboard.
         */
        function bis_show_re_network_menu() {
            $bis_common_platform = new BISRE_Common_Platform();
            add_menu_page('Mega Platform', __("Mega Platform", BIS_RULES_ENGINE_TEXT_DOMAIN), 'manage_options', 'bis_pg_dashboard', array($bis_common_platform, 'show_rules_engine_config'), plugins_url('/image/rules-engine-small-icon.png', __FILE__));
            add_submenu_page('bis_pg_dashboard', 'Dashboard', __('Dashboard', BIS_RULES_ENGINE_TEXT_DOMAIN), 'manage_options', 'bis_pg_dashboard', array($bis_common_platform, 'show_rules_engine_config'));
            add_submenu_page('bis_pg_dashboard', 'Global Settings', __('Global Settings', BIS_RULES_ENGINE_TEXT_DOMAIN), 'manage_options', 'bis_pg_global_settings', array($bis_common_platform, 'show_rules_engine_config'));
            //add_submenu_page('bis_pg_dashboard', 'Site Settings', __('Errors & Debug', BIS_RULES_ENGINE_TEXT_DOMAIN), 'manage_options', 'bis_pg_site_settings', array($bis_common_platform, 'show_rules_engine_config'));
        }

        public function includes() {

            $realPath = BIS_RULES_ENGINE_PLATFORM_PATH;
            
            require_once($realPath . "/bis/repf/common/BISLogger.php");
            require_once($realPath . "/bis-platfom-common-include.php");
            require_once($realPath . "/bis/repf/install/bis-rules-plugin-install.php");
            require_once($realPath . "/bis/repf/util/bis-analytics-util.php");

            require_once($realPath . '/bis/repf/ajax/logical-rules-ajax.php');
            require_once($realPath . '/bis/repf/ajax/rules-settings-ajax.php');
            require_once($realPath . '/bis/repf/ajax/bis-analytics-ajax.php');
            require_once($realPath . '/bis/repf/ajax/cached-site-ajax.php');
            require_once($realPath . '/bis/repf/ajax/mail-integration-ajax.php');
        }
        
    }

}

function rulesengine_platform_init() {
    
    if (!(isset($_POST['action']) && $_POST['action'] === "heartbeat")) {
        return BISRE_Professional_Platform::instance();
    }  
}

//add_action('plugins_loaded', 'rulesengine_platform_init');

// Global for backwards compatibility.
$GLOBALS['rulesengineplatform'] = rulesengine_platform_init();