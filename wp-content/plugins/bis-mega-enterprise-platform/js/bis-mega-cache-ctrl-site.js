jQuery(document).ready(function () {
    
    var is404 = false;
    if (jQuery(".error404").length) {
        is404 = true;
    }
    
    jQuery.ajax({
        type: "GET",
        dataType: "json",
        url: jQuery("#bis_re_site_url").val(),
        data: {
            bis_re_action: "bis_re_mega_rules",
            bis_re_cache_page_url: jQuery("#bis_re_cache_page_url").val(),
            bis_re_cache_post_id: jQuery("#bis_re_cache_post_id").val(),
            bis_re_cache_post_tag_id: jQuery("#bis_re_cache_post_tag_id").val(),
            bis_re_cache_product_tag_id: jQuery("#bis_re_cache_product_tag_id").val(),
            bis_re_cache_cat_id: jQuery("#bis_re_cache_cat_id").val(),
            bis_re_cache_reffer_path: jQuery("#bis_re_cache_reffer_path").val(),
            bis_re_cache_404: is404,
            bis_nonce: BISAjax.bis_rules_engine_nonce
        },
        success: function (response) {
            if (response.data && response.status !== "success_with_no_data") {
                var data = response.data;
                if (data.bis_page_rules) {
                    var bis_page_rules = new bisPageCtrl(data.bis_page_rules);
                    bis_page_rules.execute_page_rules();
                }
                if (data.bis_redirect_rules) {
                    var bis_redirect_rules = new bisRedirectCtrl(data.bis_redirect_rules);
                    showPage = bis_redirect_rules.execute_redirect_rules();
                }
                if (data.bis_post_rules) {
                    var bis_post_rules = new bisPostCtrl(data.bis_post_rules);
                    bis_post_rules.execute_post_rules();
                }
                /*if (data.bis_widget_rules) {
                    var bis_widget_rules = new bisWidgetCtrl(data.bis_widget_rules);
                    bis_widget_rules.execute_widget_rules();
                }*/
                if (data.bis_category_rules) {
                    var bis_category_rules = new bisCategoryCtrl(data.bis_category_rules);
                    bis_category_rules.execute_category_rules();
                }
                if(data.bis_woo_product_rules){
                    var bis_woo_product_rules = new bisWooProductCtrl(data.bis_woo_product_rules);
                    bis_woo_product_rules.execute_woo_product_rules();
                }
                if(data.bis_popup_key) {
                    var bis_popup_ctrl = new bisPopupCtrl(data.bis_popup_key);
                    bis_popup_ctrl.execute_popup_vo();
                }
            }
        }
    });
});