/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

bis_tiny_mce_button(tinymce);

function bis_tiny_mce_button(tinymce) {
    "use strict";
    tinymce.PluginManager.add('mega_rules_engine_icon', function (editor, url) {
        editor.addButton('mega_rules_engine_icon', {
            title: 'Logical Rule Shortcodes',
            type: 'menubutton',
            icon: 'mega-rules-engine-icon',
            menu: [{
                    text: 'If',
                    value: 'if',
                    onclick: function () {
                        editor.windowManager.open({
                            title: 'Logical Rule : If',
                            width: 500,
                            height: 100,
                            body: [{
                                    type: 'listbox',
                                    name: 'bis_logical_rules',
                                    label: 'Select Rule : ',
                                    values: JSON.parse(localStorage.getItem('bis-log-rules'))
                                }],
                            onsubmit: function (e) {
                                var count = 1;
                                localStorage.setItem('bis-if-count', count);
                                var name;
                                var rules = JSON.parse(localStorage.getItem('bis-log-rules'));
                                tinymce.each(rules, function (value, key) {
                                    if (e.data.bis_logical_rules === value['value']) {
                                        name = value['text'];
                                        var selected_text = editor.selection.getContent({format: 'text'});
                                        editor.insertContent('[bis-rules-if rule="' + name + '"]' + selected_text + '[/bis-rules-if]');
                                    }
                                });
                            }
                        });
                    }
                },
                {
                    text: 'Else',
                    value: 'else',
                    onclick: function () {
                        var count = localStorage.getItem('bis-if-count');
                        if(count == "1") {
                            localStorage.removeItem('bis-if-count');
                            var selected_text = editor.selection.getContent({format: 'text'});
                            editor.insertContent('[bis-rules-else]' + selected_text + '[/bis-rules-else]');                
                        } else {
                            alert('Without If you cannot select Else condition.');
                        }
                    }
                }]
        });
    });
}