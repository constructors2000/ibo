function bis_verify_pcode(productId, apikey) {

    jQuery("#bis_re_pc_" + productId).html("<i class=\"glyphicon glyphicon-time bis-glyphi-position\"></i>" + breCV.pc_button_please_wait);
    jQuery("#bis_re_pc_" + productId).attr("disabled", "true");
    jQuery("#pc_" + productId).attr("readonly", "true");

    var m_status;
    var pCode = jQuery("#pc_" + productId).val();
    var mpURL = "https://api.envato.com/v3/market/author/sale?code=" + pCode;

    // Request with custom header
    jQuery.ajax({
        url: mpURL,
        headers: {'Authorization': 'Bearer mQSWIs1yQv3LVptiCJ7jiHJRPNpJu2G0'},
        success: function (r_data) {
            m_status = bis_license_verify(r_data, pCode, productId);

            if (m_status == "not_verified") {
                bis_licver_showErrorMessage(productId, breCV.invalid_purchase_code);
                bis_save_fail_ver(productId, pCode);
            }
        },
        error: function (error) {
            jQuery.ajax({
                url: mpURL,
                headers: {'Authorization': 'Bearer EJPoveysHmBIaEqrnMMfPnqF7UirZXXo'},
                success: function (r_data) {
                    m_status = bis_license_verify(r_data, pCode, productId);

                    if (m_status == "not_verified") {
                        bis_licver_showErrorMessage(productId, breCV.invalid_purchase_code);
                        bis_save_fail_ver(productId, pCode);
                    }
                },
                error: function (error) {
                    bis_licver_showErrorMessage(productId, breCV.error_verify_pcode);
                    bis_save_fail_ver(productId, pCode);
                }
            });
        }
    });
}

function bis_license_verify(data, pCode, productId) {
    var licDivWidth = jQuery(".bis-verify-lic-div").width() - 52;

    jQuery("#" + productId + "_sId").width(licDivWidth);

    if (data != null) {
        bis_save_success_ver(data, productId, pCode);
        return "verified";
    } else {
        return "not_verified";
    }
}

function bis_reset_activate_button(productId) {

    jQuery("#bis_re_pc_" + productId).html("<i class=\"glyphicon glyphicon-ok-sign bis-glyphi-position\"></i>" + breCV.pc_button_activate);
    jQuery("#bis_re_pc_" + productId).removeAttr("disabled");
    jQuery("#pc_" + productId).removeAttr("readonly");

}

function bis_validate_dates() {
    var fromDate = jQuery.trim(jQuery("#bis_re_from_date").val());
    var toDate = jQuery.trim(jQuery("#bis_re_to_date").val());

    if (fromDate === "") {
        bis_alert(breCV.bis_select_from_date);
        return false;
    }

    if (toDate === "") {
        bis_alert(breCV.bis_select_to_date);
        return false;
    }

    if (fromDate > toDate) {
        bis_alert(breCV.bis_from_greater_to_date);
        return false;
    }
}

function bis_save_success_ver(vData, productId, pCode) {

    jQuery.post(
            BISAjax.ajaxurl,
            {
                action: 'bis_re_activate_plugin',
                bis_nonce: BISAjax.bis_rules_engine_nonce,
                item_name: vData.item.name,
                item_id: vData.item.id,
                buyer: vData.buyer,
                licence_type: vData.license,
                product_id: productId,
                purchase_date: vData.sold_at,
                pur_code: pCode
            },
            function (data) {
                jQuery("#" + productId + "_smessage").html(breCV.pc_plugin_act);
                jQuery("#" + productId + "_sId").show();

                jQuery("#" + productId + "_close_sId").click(function () {
                    jQuery("#" + productId + "_sId").hide();
                });

            }).always(function () {
        bis_reset_activate_button(productId);
    });

}

function bis_save_fail_ver(productId, pCode) {
    jQuery.post(
            BISAjax.ajaxurl,
            {action: 'bis_re_acplg_error',
                bis_nonce: BISAjax.bis_rules_engine_nonce,
                product_id: productId,
                pur_code: pCode
            },
            function (data) {
                // do nothing
            }).always(function () {
        bis_reset_activate_button(productId);
    });

}

function bis_licver_showErrorMessage(productId, message) {
    var licDivWidth = jQuery(".bis-verify-lic-div").width() - 52;
    jQuery("#" + productId + "_fId").width(licDivWidth);
    jQuery("#" + productId + "_fmessage").html(message);
    jQuery("#" + productId + "_fId").show();

    jQuery("#" + productId + "_close_fId").click(function () {
        jQuery("#" + productId + "_fId").hide();
    });
}
function bis_clear_cache() {
    var jqXHR = jQuery.get(
            BISAjax.ajaxurl,
            {
                action: 'bis_clear_transiant_cache'
            });
    jqXHR.done(function (respose) {
        jQuery("#clear_cache_banner_open").html(breCV.cache_clear);
        jQuery("#bis_clear_cache_banner").show();

        jQuery("#clear_cache_banner_close").click(function () {
            jQuery("#bis_clear_cache_banner").hide();
        });
    });
}

function bis_delete_rules() {

    bis_confirm("<h4>Do you want delete all rules from DB</h4>", function (result) {
        if (result) {
            var jqXHR = jQuery.get(
                    BISAjax.ajaxurl,
                    {
                        action: 'bis_delete_all_rules'
                    });
            jqXHR.done(function (response) {
                jQuery("#bis_delete_banner_open").html(breCV.delete_rules);
                jQuery("#bis_delete_rules_banner").show();

                jQuery("#bis_delete_banner_close").click(function () {
                    jQuery("#bis_delete_rules_banner").hide();
                });
            });
        }
    });
}

function bis_whitelist_ips() {
    var selIPs = jQuery("#bis_wlist_selected_ips").val();
    if (bis_validateIPaddress(jQuery.trim(selIPs)) === true) {
        var jqXHR = jQuery.get(
                BISAjax.ajaxurl,
                {
                    action: 'bis_save_whitelist_ips',
                    selIPs: selIPs
                });
        jqXHR.done(function (respose) {
            jQuery("#bis_save_caches_debugs_nprc_banner_open").html(breCV.bis_whitelist_ips);
            jQuery("#bis_save_caches_debugs_nprc_banner").show();

            jQuery("#bis_save_caches_debugs_nprc_banner_close").click(function () {
                jQuery("#bis_save_caches_debugs_nprc_banner").hide();
            });
        });
    }
}

function bis_blacklist_ips() {
    var blIPs = jQuery("#bis_blist_selected_ips").val();
    if (bis_validateIPaddress(jQuery.trim(blIPs)) === false) {
        return false;
    }
    var blRed = jQuery("#bis_blist_selected_ips_red").val();
    if (bis_validate_url(blRed) === false) {
        return false;
    }
    var jqXHR = jQuery.get(
            BISAjax.ajaxurl,
            {
                action: 'bis_save_blacklist_ips',
                blIPs: blIPs,
                blRed: blRed
            });
    jqXHR.done(function (respose) {
        jQuery("#bis_save_caches_debugs_nprc_banner_open").html(breCV.bis_blacklist_ips);
        jQuery("#bis_save_caches_debugs_nprc_banner").show();

        jQuery("#bis_save_caches_debugs_nprc_banner_close").click(function () {
            jQuery("#bis_save_caches_debugs_nprc_banner").hide();
        });
    });
}

function bis_std_to_query() {
    bis_confirm("<h4>Do you want convert all standard rules to Querybuilder rules</h4>", function (result) {
        if (result) {
            var jqXHR = jQuery.get(
                    BISAjax.ajaxurl,
                    {
                        action: 'bis_std_to_query'
                    }
            );
            var element = jQuery("#bis_migrate_std_to_query");
            jQuery(element).find('.loader').show();
            jqXHR.done(function (response) {
                if (response === true) {
                    var display_msg = breCV.bis_std_query;
                } else {
                    var display_msg = breCV.bis_no_ids_for_con;
                }
                jQuery(element).find('.loader').hide();
                jQuery("#std_query_banner_open").html(display_msg);
                jQuery("#bis_std_query_banner").show();

                jQuery("#std_query_banner_close").click(function () {
                    jQuery("#bis_std_query_banner").hide();
                });
            });
        }
    });
}

function bis_delete_rules_engin_platform() {
    var deletePlugin = false;
    var enablAnalytics = false;
    if (jQuery("#bis_re_del_plugin").is(':checked')) {
        deletePlugin = true;
    }
    if (jQuery("#bis_re_enable_analytics").is(':checked')) {
        enablAnalytics = true;
    }
    var jqXHR = jQuery.get(
            BISAjax.ajaxurl,
            {
                action: 'bis_delete_rules_engin_platform',
                deletePlugin: deletePlugin,
                enablAnalytics: enablAnalytics
            });
    jqXHR.done(function (response) {
        jQuery("#bis_delete_rules_engin_platform_banner_open").html(breCV.platform_plug_in_option);
        jQuery("#bis_delete_rules_engine_platform_banner").show();

        jQuery("#bis_delete_rules_engine_platform_banner_close").click(function () {
            jQuery("#bis_delete_rules_engine_platform_banner").hide();
        });
    });
}

function save_configurations() {

    //var progressBarColor = jQuery("#bis_customize_nprg_color").val();
    if (jQuery("#bis_rules_debug_switch").is(':checked')) {
        var rulesDebugSwitch = "Yes";
    } else {
        rulesDebugSwitch = "No";
    }
    if (jQuery("#bis_cache_enable_switch").is(':checked')) {
        var cacheEnableSwitch = "Yes";
    } else {
        cacheEnableSwitch = "No";
    }
    if (jQuery("#bis_country_dropdown_switch").is(':checked')) {
        var countryDropdownSwitch = "Yes";
    } else {
        countryDropdownSwitch = "No";
    }
    /*if (jQuery("#bis_rules_evaluation_switch").is(':checked')) {
     var ruleEvaluationSwitch = "Yes";
     } else {
     ruleEvaluationSwitch = "No";
     }*/
    var jqXHR = jQuery.get(
            BISAjax.ajaxurl,
            {
                action: 'bis_save_configurations',
                cacheEnableSwitch: cacheEnableSwitch,
                rulesDebugSwitch: rulesDebugSwitch,
                countryDropdownSwitch: countryDropdownSwitch
                        //progressBarColor: progressBarColor,
                        //ruleEvaluationSwitch:ruleEvaluationSwitch
            });
    jqXHR.done(function (response) {
        jQuery("#bis_save_caches_debugs_nprc_banner_open").html(breCV.bis_save_caches_debugs_nprc);
        jQuery("#bis_save_caches_debugs_nprc_banner").show();

        jQuery("#bis_save_caches_debugs_nprc_banner_close").click(function () {
            jQuery("#bis_save_caches_debugs_nprc_banner").hide();
        });
    });
}

function save_mail_integrtion_sett(){
    var integrator = jQuery("#bis_select_mail_integrator").val();
    var apiKey = jQuery("#bis_mail_integrator_api_key").val();
    var mailList = jQuery("#bis_integrator_ist").val();
    
    var jqXHR = jQuery.post(
            BISAjax.ajaxurl,
            {
                action: 'bis_save_mail_integrtion_sett',
                integrator: integrator,
                apiKey: apiKey,
                mailList:mailList
            });
    jqXHR.done(function (response) {
        jQuery("#bis_save_mail_integration_banner_open").html(breCV.bis_save_mail_int);
        jQuery("#bis_save_mail_integration_banner").show();

        jQuery("#bis_save_mail_integration_close").click(function () {
            jQuery("#bis_save_mail_integration_banner").hide();
       });
    });
}