function bis_setLogicalSearchButtonWidth() {
    jQuery('#bis_re_search_by').multiselect({

    });

    jQuery('#bis_re_search_status').multiselect({

    });
    jQuery('#bis_select_action').multiselect({

    });
}

function bis_logicalResetFields() {

    jQuery('#bis_re_search_status').multiselect('select', 'all');
    jQuery('#bis_re_search_status').multiselect('deselect', ['1', '0']);
    jQuery('#bis_re_search_by').multiselect('select', 'name');
    jQuery('#bis_re_search_by').multiselect('deselect', 'description');
    jQuery('#bis_select_action').val("");
    jQuery("#bis_re_search_value").val("");
    jQuery("#bis_check_all").attr('checked',false); 
    jQuery('#bis_select_action').multiselect("disable");
}

function bis_showLogicalRulesList(pageIndex) {

    jQuery.get(
            BISAjax.ajaxurl,
            {
                action: 'bis_get_logical_rules',
                pg_st_index: pageIndex
            },
            function (response) {

                bis_setLogicalSearchButtonWidth();
                bis_showLogicalRules(response);
                bis_logicalResetFields();
                bis_request_complete();

            }
    );

}

function bis_showLogicalRules(response) {

    jQuery("#logical_rules_list_content").show();
    jQuery("#logical_rules_child_content").html("");

    if (response["status"] === "success") {

        var data = {
            logicalrules: response["data"]
        };

        var source = jQuery("#bis-logicalRulesTemplate").html();
        var template = Handlebars.compile(source);
        var logicalRulesContent = template(data);

        jQuery(".bis-rows-no").html(response["total_records"]);
        jQuery('.pagination').show();
        jQuery('#bis_select_action').next().show();
        jQuery('.bis-select-action').show();
        jQuery("#bis-logicalRulesContent").html(logicalRulesContent);

        jQuery(".bis-rule-edit").on("click", bis_edit_rule);
        jQuery(".bis-checkbox").on("change", bis_uncheckCheckbox);
        jQuery(".bis-checkbox").on("change", bis_actionButton);
        jQuery("#bis-logical-rules-table").show();
        jQuery("#bis-logical-no-result").hide();

    } else {
        jQuery("#bis-logical-rules-table").hide();
        jQuery(".bis-rows-no").html(0);
        jQuery("#bis-logical-no-result").children().html('Logical rules not found');
        jQuery("#bis-logical-no-result").show();
        jQuery('.pagination').hide();
        jQuery('#bis_select_action').next().hide();
        jQuery('.bis-select-action').hide();
    }
    // stop loading icon
    bis_request_complete();

}

function bis_searchResultsLogicalRulesList(pageIndex) {

    jQuery.post(
            BISAjax.ajaxurl,
            {
                action: 'bis_re_search_rule',
                pg_st_index: pageIndex,
                searchData: { search_by: jQuery("#bis_re_search_by").val(),
                    search_value: jQuery("#bis_re_search_value").val(),
                    search_status: jQuery("#bis_re_search_status").val()
                },
                bis_nonce: BISAjax.bis_rules_engine_nonce
            },
            function (response) {
                bis_showLogicalRules(response);
                bis_request_complete();
                return false;
            }
    );
}

function bis_getLogicalPageCount(pageIndex) {

    jQuery.get(
            BISAjax.ajaxurl,
            {
                action: 'bis_get_logical_rules',
                pg_st_index: pageIndex
            },
            function (response) {
                var row_count = response["total_records"];
                var total_pages = Math.ceil(row_count / 10);
                bis_logicalBootpages(total_pages);
                bis_showLogicalRules(response);
                bis_request_complete();
            }
    );
}

function bis_logicalBootpages(total_pages) {
    jQuery('.pagination').bootpag({
        total: total_pages,
        maxVisible: 10,
        firstLastUse: true,
        first: '<span aria-hidden="true">First</span>',
        last: '<span aria-hidden="true">Last</span>',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
    }).on("page", function (event, num) {
        event.preventDefault();
        var search = jQuery('#searchPagination').val();
        if (search) {
            bis_searchResultsLogicalRulesList(num);
            jQuery("#bis_check_all").attr('checked', false);
            jQuery('#bis_select_action').multiselect("disable");
        } else {
            bis_showLogicalRulesList(num);
            jQuery("#bis_check_all").attr('checked', false);
            jQuery('#bis_select_action').multiselect("disable");
        }
        return false;
    });
}

function bis_edit_rule() {

    var ruleId = jQuery(this).closest("div").children()[0].value;

    var jqXHR = jQuery.get(ajaxurl,
            {
                action: "bis_re_edit_rule",
                ruleId: ruleId,
                bis_nonce: BISAjax.bis_rules_engine_nonce
            });

    jqXHR.done(function (data) {
        jQuery("#logical_rules_child_content").html(data);
        jQuery("#logical_rules_list_content").hide();
        jQuery("#logical_rules_child_content").show();
        jQuery(".pagination").bootpag({page: 1});
        jQuery("#bis_check_all").attr('checked', false);
        jQuery('#bis_select_action').multiselect("disable");
    });

}

function bis_logicalRuleAction() {

    var actionIds = [];
    var i = 0;
    var actionNames = " ";
    var selectedAction = jQuery("#bis_select_action option:selected").val();
    jQuery('input[name="delete_rule"]:checked').each(function() {

        actionIds[i++] = this.value;
        if(actionIds.length > 1){
            actionNames = jQuery("#delete_logical_rule_"+this.value).text()+", "+actionNames;
        }
        else{
            actionNames = jQuery("#delete_logical_rule_"+this.value).text()+actionNames;
        }
    });

    bis_confirm("<h4>Do want to " + selectedAction + " rule \"" + actionNames + "\" ?</h4>", function (result) {
        if (result) {

            var jqXHR = jQuery.get(ajaxurl,
                    {
                        action: "bis_re_logical_rule_action",
                        ruleId: actionIds,
                        selectedAction: selectedAction,
                        bis_nonce: BISAjax.bis_rules_engine_nonce,
                        beforeSend: bis_before_request_send
                    });

            jqXHR.done(function (data) {
                if (data["status"] !== "error") {
                    bis_showLogicalRules(data["data"]);
                    jQuery(".bis-rows-no").html(data["data"]["bis_row_count"]);
                } else {
                    bis_showErrorMessage(data["data"]);
                }
                bis_getLogicalPageCount(0);
                jQuery(".pagination").bootpag({page: 1});
                jQuery("#bis_check_all").attr('checked', false);
                jQuery("#bis_select_action").multiselect('deselect', selectedAction);
                jQuery('#bis_select_action').multiselect("disable");
                bis_request_complete();
            });
        }
    });
}


function showResponse(responseText, statusText, xhr, $form) {
    bis_showLogicalRules(responseText);
}

function bis_uncheckCheckbox() {
    var check = jQuery('#bis_checkbox:not(:checked)').length;
    if (check <= 0) {
        jQuery("#bis_check_all").attr('checked', true);
    } else {
        jQuery("#bis_check_all").attr('checked', false);
    }
}

function bis_actionButton() {
    if (jQuery(".bis-checkbox").is(':checked')) {
        jQuery('#bis_select_action').multiselect("enable");
    } else {
        jQuery('#bis_select_action').multiselect("disable");
    }
}

function bis_tokenInput(that){
    var rowIndex = jQuery(that).closest("tr").index();
    jQuery("#bis_re_rule_value_" + rowIndex).prev().remove();
    var bis_nonce = BISAjax.bis_rules_engine_nonce;
    var subCriteria =null;
    var subCriteriaValue = jQuery("#bis_re_sub_option_" + rowIndex + " option:selected").val();
    var arr = subCriteriaValue.split('_');

    if(arr.length == 1){
        subCriteria = subCriteriaValue;
    }else{
        subCriteria = arr[1];
    }
    var condition = jQuery("#bis_re_condition_" + rowIndex + " option:selected").val();
    var tokenLimit = 1;

    if (condition == 12 || condition == 14) {
        tokenLimit = null;
    }
    
    var ids_exist = jQuery.find("#bis_city_radius_" + rowIndex + "_value_1").length;
    
    if(condition == 15 && ids_exist == 0) {
        jQuery("#bis_city_radius_div").show();
        jQuery("#bis_re_rule_value_span_" + rowIndex).append("<input type='text' style='width: 70px; position: absolute; right: -30px; top: 0px;' id='bis_city_radius_" + rowIndex + "_value_1' name='bis_city_radius_" + rowIndex + "_value_1' placeholder='Radius'><select style='width: 55px; position: absolute; right: -90px; top: 0px;' id='bis_city_radius_units_" + rowIndex + "_value_2' name='bis_city_radius_units_" + rowIndex + "_value_2'><option val='mi'>mi</option><option val='km'>km</option></select><input type='hidden' id='bis_city_latitude_" + rowIndex + "_value_3' name='bis_city_latitude_" + rowIndex + "_value_3'><input type='hidden' id='bis_city_longitude_" + rowIndex + "_value_4' name='bis_city_longitude_" + rowIndex + "_value_4'>");
    } else if(ids_exist > 1) {
        jQuery("#bis_city_radius_div").show();
        jQuery("#bis_city_radius_" + rowIndex + "_value_1").remove();
        jQuery("#bis_city_radius_units_" + rowIndex + "_value_2").remove();
        jQuery("#bis_city_latitude_" + rowIndex + "_value_3").remove();
        jQuery("#bis_city_longitude_" + rowIndex + "_value_4").remove();
    } else if(condition != 15){
        jQuery("#bis_city_radius_div").hide();
        jQuery("#bis_city_radius_" + rowIndex + "_value_1").remove();
        jQuery("#bis_city_radius_units_" + rowIndex + "_value_2").remove();
        jQuery("#bis_city_latitude_" + rowIndex + "_value_3").remove();
        jQuery("#bis_city_longitude_" + rowIndex + "_value_4").remove();
    }

    var data = {
        id: 'bis_re_rule_value_' + rowIndex,
        name: 'bis_re_rule_value[]',
        placeholder: 'Enter a value'
    };

    switch(subCriteria){

        case "17":
        case "18":
        case "23":
        case "24":
        case "29":
        case "4":
        case "5":
        case "20":
        case "30":
        case "21":
        case "1":
        case "25":
        case "22":
        case "2":
        case "34":
        case "36":
        case "2000":
            if(condition == 12 || condition == 14 || condition == 15 || condition == 1 || condition == 2){
                jQuery("#bis_re_rule_value_" + rowIndex).tokenInput(ajaxurl + "?action=bis_re_get_value&bis_nonce=" + bis_nonce +
                        "&subcriteria=" + subCriteria + "&condition=" + condition, {
                            theme: "facebook",
                            minChars: 2,
                            method: "POST",
                            tokenLimit: tokenLimit,
                            onAdd: function (item) {
                                jQuery("#bis_re_rule_value_" + rowIndex).val(JSON.stringify(this.tokenInput("get")));
                                if (condition == 15) {
                                    jQuery("#bis_city_latitude_" + rowIndex + "_value_3").val(item.lat);
                                    jQuery("#bis_city_longitude_" + rowIndex + "_value_4").val(item.lng);
                                } 
                                if (condition != 15) {
                                    jQuery("#bis_city_radius_" + rowIndex + "_value_1").remove();
                                    jQuery("#bis_city_radius_units_" + rowIndex + "_value_2").remove();
                                    jQuery("#bis_city_latitude_" + rowIndex + "_value_3").remove();
                                    jQuery("#bis_city_longitude_" + rowIndex + "_value_4").remove();
                                }
                            }
                        });
            } else{
                renderTextBox(data, rowIndex);
            }
            break;
        case "15":

            if (condition === "12" || condition == "14") {
                data.placeholder = 'Enter values with comma separation (i.e value1, value2, value3)';
            }

            renderTextBox(data, rowIndex);
            break;
        case "6":

            if (condition === "13") {
                data.placeholder = 'Enter URL with /** pattern (ex: http://rulesengine.in/mypath/**';
            }

            renderTextBox(data, rowIndex);
            break;
    }
    if (subCriteria === "35" && (condition === "12" || condition == "14")) {
        jQuery('.bis-postcode-csv-file').show();
        var pcods = '';
        jQuery('#bis_re_post_code_upload').click(function () {
            jQuery("#bis_re_rule_value_" + rowIndex).val('');
            var csv = jQuery('#bis_re_post_code_file');
            var csvFile = csv[0].files[0];
            var ext = csv.val().split(".").pop().toLowerCase();
            if (jQuery.inArray(ext, ["csv"]) === -1) {
                alert('upload csv');
                return false;
            }
            if (csvFile !== 'undefined') {
                reader = new FileReader();
                reader.onload = function (e) {
                    rows = e.target.result.split(/\r|\n|\r\n/);
                    if (rows.length > 0) {
                        var first_Row_Cells = splitCSVtoCells(rows[0], ","); //Taking Headings
                        var jsonArray = new Array();
                        for (var i = 1; i < rows.length; i++) {
                            var cells = splitCSVtoCells(rows[i], ",");
                            var obj = {};
                            for (var j = 0; j < cells.length; j++) {
                                obj[first_Row_Cells[j]] = cells[j];
                            }
                            jsonArray.push(obj);
                        }
                        pcods = '';
                        jQuery.each(jsonArray, function (key, val) {
                            if (val.hasOwnProperty('Pincode')) {
                                var pinStr = val.Pincode;
                                if (pcods !== '' && pinStr !== '') {
                                    pcods = pcods + ',' + pinStr;
                                } else if (pinStr !== '') {
                                    pcods = pinStr;
                                }
                            }
                        });
                        if (pcods !== '') {
                            jQuery("#bis_re_rule_value_" + rowIndex).val(pcods);
                        }
                    }
                };
                reader.readAsText(csvFile);
            }
        });
    } else {
        jQuery('.bis-postcode-csv-file').hide();
        jQuery("#bis_re_rule_value_" + rowIndex).val('');
    }
}

function splitCSVtoCells(row, separator) {
    var sliptedRow = row.split(separator);
    jQuery.each(sliptedRow, function (key, val) {
        sliptedRow[key] = val.replace(/\"/g, "");
    });
    return sliptedRow;
}

function renderTextBox(data, rowIndex) {

    var source = jQuery("#bis-logical-textbox-component").html();
    var template = Handlebars.compile(source);
    var textbox = template(data);
    var textboxId = jQuery("#bis_re_rule_value_span_" + rowIndex);
    textboxId.html(textbox);

}

function bis_cities_popup(){

    /*if (typeof selectedValueContainer == 'object') {
        var selectedCity = selectedValueContainer;
        var jsonVal = selectedCity;
    } else {
        selectedCity = jQuery("input[name=" + selectedValueContainer + "]").val();
        jsonVal = jQuery.parseJSON(selectedCity);
    }
    var bis_re_city_radius = jQuery("#bis_re_city_radius").val();
    var bis_longitude = jQuery("#bis_re_city_lng").val();
    var bis_latitude = jQuery("#bis_re_city_lat").val();
    var bis_radius_unit = jQuery("#bis_re_mi_km").val();
    jQuery("#bis_nearby_radius").val(bis_re_city_radius);
    jQuery("#bis_re_mi_km_popup").val(bis_radius_unit);*/
    jQuery("#bis_near_by_popup").show();
    jQuery("#bis_re_cities_list_div").hide();
    bis_radius_unit_changer('bis_re_mi_km_popup', bis_radius_unit)
    var bis_nonce = BISAjax.bis_rules_engine_nonce;
    jQuery("#bis_re_near_box_span_value").prev().remove();

    var data = {
        id: 'bis_re_near_box_span_value',
        name: 'bis_re_near_box_span_value[]'
    };
    var source = jQuery("#bis-logical-hidden-component").html();
    var template = Handlebars.compile(source);
    var textbox = template(data);
    var textboxId = jQuery("#bis_re_near_box_span");
    textboxId.html(textbox);
    var subCriteria = 29;
    var $tokenInput = jQuery("#bis_re_near_box_span_value").tokenInput(ajaxurl + "?action=bis_re_get_value&bis_nonce=" + bis_nonce +
        "&subcriteria=" + subCriteria,
        {
            theme: "facebook",
            minChars: 2,
            method: "POST",
            tokenLimit: 1,
            onAdd: function (item) {
                jQuery("#bis_re_near_box_span_value").val(JSON.stringify(this.tokenInput("get")));
                jQuery("#bis_re_city_lng").val(item.lng);
                jQuery("#bis_re_city_lat").val(item.lat);
            },
            onDelete: function (item) {
                jQuery(this).val(JSON.stringify(this.tokenInput("get")));
            }
        });
    //$tokenInput.val(jsonVal);
    //bis_cities_table_list(bis_re_city_radius, bis_longitude, bis_latitude, bis_radius_unit);    
}

function bis_cities_table_list(bis_re_city_radius, bis_longitude, bis_latitude, bis_radius_unit) {
    
    var element = jQuery("#bis_re_cities_list_div");
    jQuery(element).find('.loader').show();
    var jqXHR = jQuery.get(
            BISAjax.ajaxurl,
            {
                action: "bis_get_near_cities",
                bis_re_city_radius: bis_re_city_radius,
                bis_longitude: bis_longitude,
                bis_latitude: bis_latitude,
                bis_radius_unit: bis_radius_unit
            });
    jqXHR.done(function (response) {

        jQuery('#bis_re_cities_list').html("");
        var content = "";
        if(response.length > 0){
            content = "<table class='table table - hover table - bordered table - striped'>"
            content += "<thead><tr><th>City Name</th></tr></thead>"
            var i = 1;
            jQuery.each(response, function (key, city) {
                content += '<tr class="assetRow"><td><div class="content-rule-name" >' + i + '.' + city["city"] + '</div></td></tr>';
                i++;
            });
            content += "</table>";
            
        } else {
            content = "<table class='table table - hover table - bordered table - striped'>\n\
                                <thead>\n\
                                    <tr><th>City Name</th></tr>\n\
                                </thead>\n\
                                <tr class='assetRow'>\n\
                                    <td><div class='content-rule-name'></div>No Cities found with in this "+bis_re_city_radius+" "+bis_radius_unit+" radius</td>\n\
                                </tr>\n\
                           </table>";
        }
        jQuery('#bis_re_cities_list').append(content);
        jQuery(element).find('.loader').hide();
        jQuery("#bis_re_cities_list_div").show();
    });
}

function bis_radius_unit() {
    if (jQuery("#bis_re_mi_km").is(':checked')) {
        jQuery("#bis_re_mi_km").val('mi');
    } else {
        jQuery("#bis_re_mi_km").val('km');
    }
}

function bis_radius_unit_changer(radius_unit_id, radius_unit){
    if (radius_unit == "mi") {
        jQuery('#'+radius_unit_id).attr('checked', true); 
    } else {
        jQuery('#'+radius_unit_id).attr('checked', false); 
    }
}