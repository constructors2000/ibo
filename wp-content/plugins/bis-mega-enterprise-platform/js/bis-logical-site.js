// wait for the DOM to be loaded
/*jQuery(document).ready(function () {

    if (BISAjax.bis_cached_site === "false") {
        jQuery.ajax({
            type: "GET",
            dataType: "json",
            url: jQuery("#bis_re_site_url").val(),
            data: {
                bis_re_action: "bis_re_popup_show",
                bis_nonce: BISAjax.bis_rules_engine_nonce
            },
            success: function (response) {
                var bis_popup = new bisPopupCtrl(response);
                bis_popup.show_popup();
            }
        });
    }

    window.bisPopupCtrl = function (options) { 

        this.popUp = null;
        this.show_popup = function () {

            var showPage = true;

            if (options.data && options.status !== "success_with_no_data") {

                var bis_popup_once = jQuery.cookie("bis_rd_popup_occur");

                if (options.data.popup_data) {
                    this.popUp = options.data.popup_data;
                    if (bis_popup_once !== '1') {
                        this.bis_show_popup(options);
                    }
                }
            }

            return showPage;
        };
        
        this.bis_show_popup = function (response) {
            var templateContent = response.data.popup_data.popupTemplate;
            jQuery("body").append('<div id="bis_re_modal_div" style="text-align:center"></div>');
            jQuery("body").append(templateContent);

            var logicalPopUp = tmpl("bis_dailog_popup_template", response['data']);

            var popUp = response['data']['popup_data'];

            jQuery("#bis_re_modal_div").html(logicalPopUp);

            var options = {
                closeOnOutsideClick: true,
                closeOnEscape: true,
                hashTracking: false
            };

            var bis_remodal = jQuery('#bis_re_dailog_modal').remodal(options);
            bis_remodal.open();

            var secInterval;

            // Set cookie
            jQuery.cookie("bis_rd_popup_occur", 0);

            if (popUp.popUpOccurr === '1') {
                jQuery.cookie("bis_rd_popup_occur", 1);
            }

            if (popUp.autoCloseTime !== 0) {
                var counter = Number(popUp.autoCloseTime) / 1000;
                jQuery("#bis_stimer").text(counter);

                secInterval = setInterval(function () {
                    counter = counter - 1;
                    if (counter <= 0) {
                        clearInterval(secInterval);
                        bis_remodal.close();
                    }
                }, 1000);
            }
        };
    };

    if (BISAjax.bis_cached_site === "false") {
        jQuery.ajax({
            type: "GET",
            dataType: "json",
            url: jQuery("#bis_re_site_url").val(),
            data: {
                bis_re_action: "bis_re_logical_popup",
                bis_nonce: BISAjax.bis_rules_engine_nonce
            },
            success: function (response) {
                var bis_logical_rules = new bisLogicalCtrl(response);
                bis_logical_rules.show_logical_popup();
            }
        });
    }
    
    window.bisLogicalCtrl = function (options) {

        this.logical_rules = options;
        this.popUp = null;
        var that = this;
        this.execute_logical_rules = function () {
            var post_id = jQuery("#bis_re_cache_post_id").val();
            var tag_id = jQuery("#bis_re_cache_product_tag_id").val();
            var cat_id = jQuery("#bis_re_cache_cat_id").val();
            for (i = 0; i < this.logical_rules.length; i++) {
                switch (this.logical_rules[i].action) {
                    case "show_as_dialog":
                        bisLogicalAction(this.logical_rules[i]);
                        break;
                }
            }
        };

        this.show_logical_popup = function () {

            var showPage = true;

            if (options.data && options.status !== "success_with_no_data") {

                var bis_popup_once = jQuery.cookie("bis_rd_popup_occur");

                if (options.data.popup_data) {
                    this.popUp = options.data.popup_data;
                    if (bis_popup_once !== '1') {
                        this.bis_show_logical_popup(options);
                    }
                }
            }

            return showPage;
        };

        this.bis_show_logical_popup = function (response) {

            var templateContent = response.data.popup_data.popupTemplate;
            jQuery("body").append('<div id="bis_re_modal_div" style="text-align:center"></div>');
            jQuery("body").append(templateContent);

            var logicalPopUp = tmpl("bis_dailog_popup_template", response['data']);

            var popUp = response['data']['popup_data'];

            jQuery("#bis_re_modal_div").html(logicalPopUp);

            var options = {
                closeOnOutsideClick: true,
                closeOnEscape: true,
                hashTracking: false
            };

            var bis_remodal = jQuery('#bis_re_dailog_modal').remodal(options);
            bis_remodal.open();

            var secInterval;

            // Set cookie
            jQuery.cookie("bis_rd_popup_occur", 0);

            if (popUp.popUpOccurr === '1') {
                jQuery.cookie("bis_rd_popup_occur", 1);
            }

            if (popUp.autoCloseTime !== 0) {
                var counter = Number(popUp.autoCloseTime) / 1000;
                jQuery("#bis_stimer").text(counter);

                secInterval = setInterval(function () {
                    counter = counter - 1;
                    if (counter <= 0) {
                        clearInterval(secInterval);
                        bis_remodal.close();
                    }
                }, 1000);
            }            
        };
    };

    function bisLogicalAction(logical_rule) {
        var position = jQuery.parseJSON(logical_rule.gencol2);
        switch (position['content_position']) {
            case "show_as_dialog":
                show_logical_popup_in_cache_sites(logical_rule.popup_data);
                break;
        }
    }

    function show_logical_popup_in_cache_sites(options) {

        var showPage = true;

        if (options.data && options.status !== "success_with_no_data") {

            var bis_popup_once = jQuery.cookie("bis_rd_popup_occur");

            if (options.data.popup_data) {
                this.popUp = options.data.popup_data;
                if (bis_popup_once !== '1') {
                    bis_logical_popup_show(options);
                }
            }

        }

        return showPage;

    }

    function bis_logical_popup_show(response) {

        var templateContent = response.data.popup_data.popupTemplate;
        jQuery("body").append('<div id="bis_re_modal_div" style="text-align:center"></div>');
        jQuery("body").append(templateContent);

        var logicalPopUp = tmpl("bis_dailog_popup_template", response['data']);

        var popUp = response['data']['popup_data'];

        jQuery("#bis_re_modal_div").html(logicalPopUp);

        var options = {
            closeOnOutsideClick: false,
            closeOnEscape: false,
            hashTracking: false
        };

        var bis_remodal = jQuery('#bis_re_dailog_modal').remodal(options);
        bis_remodal.open();

        var secInterval;

        // Set cookie
        jQuery.cookie("bis_rd_popup_occur", 0);

        if (popUp.popUpOccurr === '1') {
            jQuery.cookie("bis_rd_popup_occur", 1);
        }

        if (popUp.autoCloseTime !== 0) {
            var counter = Number(popUp.autoCloseTime) / 1000;
            jQuery("#bis_stimer").text(counter);

            secInterval = setInterval(function () {
                counter = counter - 1;
                if (counter <= 0) {
                    clearInterval(secInterval);
                    bis_remodal.close();
                }
            }, 1000);
        }
    }

});*/