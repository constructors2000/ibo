<?php
/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

namespace bis\repf\model;

use bis\repf\vo\RulesVO;
use bis\repf\vo\LabelValueVO;
use bis\repf\util\GeoPluginWrapper;

/**
 * This class is a Model for Post Rules.
 *
 */
class PostRulesEngineModel extends BaseRulesEngineModel
{


    /**
     * Save Post rules.
     *
     * @param RulesVO $rules_vo
     * @return array
     */
    public function save_post_rule(RulesVO $rules_vo)
    {

        return $this->save_child_rule($rules_vo);

    }

    /**
     * Save Post rules.
     *
     * @param RulesVO $rules_vo
     * @return array
     */
    public function updatePostRule(RulesVO $rules_vo)
    {
        
        return $this->update_child_rule($rules_vo);
    }

    /**
     * This method is used to get the applied post rules.
     *
     * @param $logical_rules
     * @internal param \unknown $applied_rules
     * @return string
     */
    function get_applied_post_rules($logical_rules)
    {
        $applied_rules = array();
        $applied_post_rules =  $this->get_applied_rules($logical_rules, BIS_POST_TYPE_RULE);
        $applied_product_rules =  $this->get_applied_rules($logical_rules, BIS_WOO_PRODUCT_TYPE_RULE);
        if (is_array($applied_post_rules)) {
            if(is_array($applied_product_rules)){
                $applied_post_rules = array_merge($applied_post_rules, $applied_product_rules);
            }
            return $applied_post_rules;
        } else {
            return $applied_product_rules;
        }
    }

    /**
     * Delete post rule using ruleId.
     *
     * @param  $ruleId .
     * @return \multitype
     */
    public function post_rule_action($ruleId, $selectedAction)
    {

        $status = $this->child_rule_action($ruleId, $selectedAction);
        $results_map = $this->get_post_rules();

        if (!$status) {
            $results_map[BIS_STATUS] = BIS_ERROR;
        }

        return $results_map;
    }
    
    /**
     * This method is used to get all post rules.
     *
     * @return multitype:
     */
    public function get_post_rules($page_start_index = 0)
    {
        return $this->get_child_rules(BIS_POST_TYPE_RULE, $page_start_index);
    }
    
    /**
     * This method is used to get the Post Rule based on ruleId.
     * @param unknown $ruleId
     * @return NULL
     */
    public function get_post_rule($ruleId) {

        global $wpdb;
        $table_prefix = $wpdb->prefix;

        $rd_query = $wpdb->prepare("SELECT  rrd.id as post_detail_id, rrd.action_hook, rrd.name as post_rule, rrd.description, rrd.status, rrd.action,
				   rlr.name AS rule_name, rlr.id AS rule_id, rrd.general_col1 as pd_title, rrd.parent_type_value as type_id, rrd.general_col2 as pd_body,
                                   rrd.general_col3 as gencol3,  rrd.general_col4 as gencol4, rrd.general_col5 as gencol5  FROM " . $table_prefix . "bis_re_rule_details rrd
				   JOIN " . $table_prefix . "bis_re_logical_rules rlr ON rlr.id = rrd.logical_rule_id WHERE rrd.id = %d", $ruleId);

        $posts_table = $wpdb->prefix . "posts";


        $row = $wpdb->get_row($rd_query);
        
        $rules_vo = null;
        $selected_posts = array();
        
        $rules_vo = new RulesVO();
        $rules_vo->set_name($row->post_rule);
        $rules_vo->set_description($row->description);
        $rules_vo->set_rule_name($row->rule_name);
        $rules_vo->set_action($row->action);
        $rules_vo->set_status($row->status);
        $rules_vo->set_id($row->post_detail_id);
        $rules_vo->set_rule_type_id(BIS_POST_TYPE_RULE);
        $rules_vo->set_action_hook($row->action_hook);
        $rules_vo->set_rule_id($row->rule_id);
        $rules_vo->set_general_col1($row->pd_title);
        $rules_vo->set_general_col2($row->pd_body);
        $rules_vo->set_general_col3($row->gencol3);
        $rules_vo->set_general_col4($row->gencol4);
        $rules_vo->set_general_col5($row->gencol5);
        $rules_vo->set_parent_type_value($row->type_id);

        if ($row->type_id == "attributes" || $row->type_id == "custom_taxo") {
            $slug = json_decode($row->gencol4)[0];
            $actual_terms = $this->get_term_list_by_slug($slug, $row->type_id);
            $saved_terms = $wpdb->get_results("SELECT parent_id FROM " . $table_prefix . "bis_re_rules WHERE rule_details_id = " . $row->post_detail_id);
            foreach ($saved_terms as $saved_term) {
                foreach ($actual_terms["data"] as $actual_term) {
                    if ($saved_term->parent_id == $actual_term->id) {
                        $label_value_vo = new LabelValueVO();
                        $label_value_vo->set_label($actual_term->name);
                        $label_value_vo->set_value($actual_term->id);
                        array_push($selected_posts, $label_value_vo);
                    }
                }
            }

            $rules_vo->set_rule_type_value(json_decode($row->gencol4));
            $rules_vo->setAll_terms($selected_posts);
        } elseif ($row->type_id == "woo-tags" || $row->type_id == "wp-tags") {
            $wpp_query = "SELECT parent_id FROM ". $table_prefix ."bis_re_rules WHERE rule_details_id =%d";
            $product_tags = $wpdb->get_results($wpdb->prepare($wpp_query, $row->post_detail_id));
            foreach ($product_tags as $product_tag) {
                $label_value_vo = new LabelValueVO();
                $label_value_vo->set_label($product_tag->parent_id);
                $label_value_vo->set_value($product_tag->parent_id);
                array_push($selected_posts, $label_value_vo);
            }
            $rules_vo->set_rule_type_value($selected_posts);
        } else {
            $wpp_query = "SELECT wpp.post_title as post_name, wpp.id as post_id FROM " . $table_prefix . "bis_re_rules rr  JOIN $posts_table wpp ON wpp.id = rr.parent_id WHERE rule_details_id = %d";
            $post_rows = $wpdb->get_results($wpdb->prepare($wpp_query, $row->post_detail_id));
            if (!empty($post_rows)) {
                foreach ($post_rows as $post_row) {
                    $label_value_vo = new LabelValueVO();
                    $label_value_vo->set_label($post_row->post_name);
                    $label_value_vo->set_value($post_row->post_id);
                    array_push($selected_posts, $label_value_vo);
                }
            }
            $rules_vo->set_rule_type_value($selected_posts);
        }
        return $rules_vo;
    }

    /**
     * This method is used to get all redirect rules.
     *
     * @return multitype:
     */
    public function is_post_rule_exists($rule_name, $rdetail_id = 0) {

        return $this->is_child_rule_exists($rule_name, $rdetail_id, BIS_POST_TYPE_RULE);
    }
    
    /**
     * This method is used to get all post types.
     *
     * @return array of post types:
     */
    public function get_all_post_list($type_id) {
        
        $post_types = $type_id;
        $posts = array();
        $results_map = array();
        
        if ($type_id == "wp-tags") {
            $wp_tags = get_terms(array(
                'hide_empty' => false, // only if you want to hide false
                'taxonomy' => 'post_tag',
                )
            );
            foreach ($wp_tags as $wp_tag) {
                $post_tag = new LabelValueVO();
                $post_tag->set_id($wp_tag->slug);
                $post_tag->set_name($wp_tag->name);
                $post_tag->set_value($wp_tag->slug);
                array_push($posts, $post_tag);
            }
        } elseif ($type_id == "wp-categories") {

            $args = array(
                'taxonomy' => 'category',
                'orderby' => 'name',
                'show_count' => 0,
                'pad_counts' => 0,
                'hierarchical' => 1,
                'title_li' => '',
                'hide_empty' => 0
            );

            $all_categories = get_categories($args);
            foreach ($all_categories as $cat) {
                $post_cat = new LabelValueVO();
                $post_cat->set_id((int) $cat->term_id);
                $post_cat->set_name($cat->name);
                $post_cat->set_value($cat->term_id);
                array_push($posts, $post_cat);
            }
        } elseif (\RulesEngineUtil::isContains($type_id, "product_variation")){
            
            $parent_id = explode("_", $type_id)[2];
            if(\RulesEngineUtil::isContains($parent_id, ",")){
                $variations_arr = array();
                $parent_ids = explode(",", $parent_id);
                foreach ($parent_ids as $id) {
                    $variations = get_posts(array(
                        'offset' => 0,
                        'orderby' => 'post_date',
                        'order' => 'DESC',
                        'post_type' => 'product_variation',
                        'post_status' => 'publish',
                        'suppress_filters' => true,
                        'posts_per_page' => -1,
                        'post_parent' => $id,
                    ));
                    $variations_arr = array_merge($variations_arr, $variations);
                }
                $rows = $variations_arr;
            } else {
                $args = array(
                 'offset' => 0,
                 'orderby' => 'post_date',
                 'order' => 'DESC',
                 'post_type' => 'product_variation',
                 'post_status' => 'publish',
                 'suppress_filters' => true,
                 'posts_per_page' => -1,
                 'post_parent' => $parent_id,
                );
                $rows = get_posts($args);
            }


            if (($rows != null) && (count($rows) > 0)) {
                foreach ($rows as $post) {
                    $images = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
                    $post_s = new LabelValueVO();
                    $post_s->set_image($images[0]);
                    $post_s->set_id($post->ID);
                    $post_s->set_name($post->post_title);
                    array_push($posts, $post_s);
                }
            }
        } elseif ($type_id == "grouped_product" || $type_id == "external_product" || $type_id == "variable_product") {
            
            if($type_id == "grouped_product"){
                // Get grouped products.
                global $wpdb;
                $table_prefix = $wpdb->prefix;
                $query = "SELECT post_id FROM " . $table_prefix . "postmeta WHERE meta_key = '_children'";
                $products = $wpdb->get_results($query);
                
            } elseif ($type_id == "external_product") {
                // Get external products.
                $args = array(
                    'type' => 'external',
                );
                $products = wc_get_products($args);
            } elseif ($type_id == "variable_product") {
                $args = array(
                    'product_type' => 'variable',
                );
                $products = wc_get_products($args);
            }
            if (($products != null) && (!empty($products))) {
                foreach ($products as $product) {
                    if($type_id == "grouped_product"){
                         $content_post = get_post($product->post_id);
                    } else {
                        $content_post = get_post($product->get_id());
                    }
                    if($content_post->post_status == 'publish'){
                        /*$images = wp_get_attachment_image_src(get_post_thumbnail_id($content_post->ID), 'single-post-thumbnail');
                        $post_s = new LabelValueVO();
                        $post_s->set_image($images[0]);*/
                        $post_s->set_id($content_post->ID);
                        $post_s->set_name($content_post->post_title);
                        array_push($posts, $post_s);
                    }
                }
            }
        } else {
            $args = array(
                'offset' => 0,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_type' => $post_types,
                'post_status' => 'publish',
                'suppress_filters' => true,
                'posts_per_page' => -1
            );

            $rows = get_posts($args);            

            if (($rows != null) && (!empty($rows))) {
                foreach ($rows as $post) {
                    $images = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
                    $post_s = new LabelValueVO();
                    $post_s->set_image($images[0]);
                    $post_s->set_id($post->ID);
                    $post_s->set_name($post->post_title);
                    array_push($posts, $post_s);
                }
            }
        }
            
        $results_map[BIS_DATA] = $posts;
        $results_map[BIS_STATUS] = BIS_SUCCESS;
        $results_map[BIS_ROW_COUNT] = null;

        return $results_map;
    }
    
    public function bis_get_all_post_rules($type) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $type_id = BIS_POST_TYPE_RULE;
        if($type == 'product'){
            $type_id = BIS_WOO_PRODUCT_TYPE_RULE;
        }
        if (isset($type)) {
            $query = $wpdb->prepare("SELECT * FROM " . $table_prefix . "bis_re_rule_details WHERE parent_type_value = %s AND status = 1 ORDER BY name", $type);
        } else {
            $query = $wpdb->prepare("SELECT * FROM " . $table_prefix . "bis_re_rule_details WHERE rule_type_id = %d AND status = 1 ORDER BY name", $type_id);
        }
        $rows = $wpdb->get_results($query);
        if (!empty($rows)) {
            $results_map = array("status" => "success", "data" => $rows);
        } else {
            $results_map = array("status" => "no_data", "data" => BIS_MESSAGE_NO_RECORD_FOUND);
        } return $results_map;
    }
    
    /**
     * This method used to apply post rule from wp post list page
     * 
     * @global type $wpdb
     * @param type $postids
     * @param type $postrule
     * @return int
     */
    public function applying_post_rule($postids, $postrule, $post_type) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $rules_table = $table_prefix . "bis_re_rules";
        $status = 0;
        $typeid = BIS_POST_TYPE_RULE;
        if($post_type == 'product'){
             $typeid = BIS_WOO_PRODUCT_TYPE_RULE;
        }
        $pquery = $wpdb->prepare("SELECT * FROM " . $table_prefix . "bis_re_rules WHERE rule_details_id=%d", $postrule);
        $rows = $wpdb->get_results($pquery);
        $array = array();
        foreach ($rows as $row){
            array_push($array, $row->parent_id);
        }
        $ids = array_diff($postids, $array);
        if($ids != null){
            foreach ($ids as $id) {
                $data = array('parent_id' => $id, 'rule_details_id' => $postrule,
                    'parent_type_id' => $typeid);
                $status = $wpdb->insert($rules_table, $data, array("%s", "%d", "%d"));
                if ($status != 1) {
                    $status = 0;
                    break;
                }
            }
        }
        return $status;
    }
    
    public function get_post_rule_values($post_type) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $rule_type_id = BIS_POST_TYPE_RULE;
        if($post_type == 'product'){
            $rule_type_id = BIS_WOO_PRODUCT_TYPE_RULE;
        }
        $pquery = $wpdb->prepare("SELECT brrd.id, brrd.name, brr.parent_id FROM " . $table_prefix . "bis_re_rule_details brrd 
		INNER JOIN " . $table_prefix . "bis_re_rules brr ON brr.rule_details_id = brrd.id AND brrd.rule_type_id = %d WHERE brrd.status = 1", $rule_type_id);
        $rows = $wpdb->get_results($pquery);

        return $rows;
    }
    
    public function get_post_types() {
        $post_array1 = array();
        $posts = array();
        $c_args = array(
            'public' => true,
            '_builtin' => false
        );
        $w_args = array(
            'name' => 'post'
        );
        $popup_args = array(
            'name' => 'popup'
        );

        array_push($post_array1, $c_args);
        array_push($post_array1, $w_args);
        array_push($post_array1, $popup_args);

        foreach ($post_array1 as $key => $value) {
            $post_types1 = get_post_types($value);
            foreach ($post_types1 as $key => $value1) {
                $post = new LabelValueVO();
                $post->set_name($value1);
                array_push($posts, $post);
            }
        }
        return $posts;
    }
    
    public function get_all_product_taxonomies($type_id){
        
        $product_taxonomies = array();
        
        if($type_id == "attributes"){
            
            $all_attributes = $this->get_woo_attribute_taxonomies();
            if(!empty($all_attributes)) {
                $attr_list = $all_attributes['data'];
                foreach ($attr_list as $attr) {
                    $product_attr = new LabelValueVO();
                    $product_attr->set_id($attr->slug);
                    $product_attr->set_label($attr->name);
                    array_push($product_taxonomies, $product_attr);
                }
            }
        } elseif ($type_id == "categories") {
           
            $args = array(
                'taxonomy' => 'product_cat',
                'orderby' => 'name',
                'show_count' => 0,
                'pad_counts' => 0,
                'hierarchical' => 1,
                'title_li' => '',
                'hide_empty' => 0
            );
            
            $all_categories = get_categories($args);
            foreach ($all_categories as $cat) {
                $product_cat = new LabelValueVO();
                $product_cat->set_id((int)$cat->term_id);
                $product_cat->set_name($cat->name);
                $product_cat->set_value($cat->term_id);
                array_push($product_taxonomies, $product_cat);
            }
        } elseif ($type_id == "woo-tags") {
            $woo_tags = get_terms(array(
                'hide_empty' => false, // only if you want to hide false
                'taxonomy' => 'product_tag',
                    )
            );
            foreach ($woo_tags as $woo_tag) {
                $product_tag = new LabelValueVO();
                $product_tag->set_id($woo_tag->slug);
                $product_tag->set_name($woo_tag->name);
                $product_tag->set_value($woo_tag->slug);
                array_push($product_taxonomies, $product_tag);
            }
        } elseif ($type_id == "custom_taxo") {
            $args = array('public' => true, '_builtin' => false);
            $taxonomies = get_taxonomies($args);
            foreach ($taxonomies as $taxonomy) {
                $cust_tax = new LabelValueVO();
                $cust_tax->set_id($taxonomy);
                $cust_tax->set_name($taxonomy);
                $cust_tax->set_value($taxonomy);
                array_push($product_taxonomies, $cust_tax);
            }
        }
        
        $results_map[BIS_DATA] = $product_taxonomies;
        $results_map[BIS_STATUS] = BIS_SUCCESS;
        $results_map[BIS_ROW_COUNT] = null;

        return $results_map;
    }
    
    public function get_term_list_by_slug($ssubOptionId, $product_type){
        
        if($product_type == "custom_taxo"){
            $ssubOptionId = $ssubOptionId;
        } else {
            // Add pa to indicate product attributes
            $ssubOptionId = 'pa_' . $ssubOptionId;
        }
        $terms = get_terms(array(
            'taxonomy' => $ssubOptionId,
            'hide_empty' => false
        ));

        $results_map = array();
        $product_terms = array();

        if (!empty($terms) && !is_wp_error($terms)) {
            foreach ($terms as $term) {
                $product_term = new LabelValueVO();
                $product_term->set_id($term->slug);
                $product_term->set_label($term->name);
                array_push($product_terms, $product_term);
            }
        }

        $results_map[BIS_DATA] = $product_terms;
        $results_map[BIS_STATUS] = BIS_SUCCESS;
        $results_map[BIS_ROW_COUNT] = null;
        
        return $results_map;
    }
    
    public function get_post_or_product_show_rules($type_id) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;

        $query = "SELECT rrd.id, rrd.action_hook, rrd.name, rrd.description, rrd.status,
                    rrd.child_sub_rule, rrd.action, rrd.parent_type_value, rrd.general_col1 AS gencol1,
                    rrd.general_col2 AS location, rrd.general_col4 AS slug, rr.parent_id
                FROM " . $table_prefix . "bis_re_rule_details rrd 
                JOIN " . $table_prefix . "bis_re_rules rr ON rr.rule_details_id = rrd.id 
                WHERE rrd.rule_type_id = " . $type_id . " AND rrd.action ='show_post' AND rrd.status=1;";

        $rows = $wpdb->get_results($query);

        return $rows;
    }
    
    /*public function get_post_or_product_show_only_rules($type_id) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;

        $query = "SELECT rrd.id, rrd.action_hook, rrd.name, rrd.description, rrd.status,
                    rrd.child_sub_rule, rrd.action, rrd.parent_type_value, rrd.general_col1 AS gencol1,
                    rrd.general_col2 AS location, rrd.general_col4 AS slug, rr.parent_id
                FROM " . $table_prefix . "bis_re_rule_details rrd 
                JOIN " . $table_prefix . "bis_re_rules rr ON rr.rule_details_id = rrd.id 
                WHERE rrd.rule_type_id = " . $type_id . " AND rrd.action ='show_only_post' AND rrd.status=1;";

        $rows = $wpdb->get_results($query);

        return $rows;
    }*/
    
    public function get_slug_by_id($id, $taxonomy){
        $term = get_term($id, $taxonomy);
        if(!empty($term)){
            return $slug = $term->slug;
        } else {
            return $slug = "";
        }
    }

    public function create_post_rule_based_on_woo_order($cat_ids, $post_duration, $data){
       
        $rules_vo = new RulesVO();
        $geoLocation = new GeoPluginWrapper();
        $current_ip = $geoLocation->getIPAddress();
        
        $rule_name = $data["billing_first_name"]. $data["billing_last_name"];
        $rules_vo->set_description("");
        $rules_vo->set_action("show_post");
        $rules_vo->set_status("1");
        $rules_vo->set_parent_type_value("custom_taxo");
        $rules_vo->set_rule_type_id(BIS_POST_TYPE_RULE);
        $rules_vo->set_reset_rule_key(BIS_POST_RULE_RESET);
        $rules_vo->set_general_col4(json_encode(array("portfolio_cat")));
        $bis_re_sub_option = array("7_46", "6_15");
        $bis_re_condition = array("6", "1");
        $bis_re_rcId = array();
        $logical_rule_id = 0;
        $bis_criteria = array(
            "bis_re_sub_option" => $bis_re_sub_option,
            "bis_re_condition" => $bis_re_condition,
            "bis_re_left_bracket" => "0",
            "bis_re_right_bracket" => "0",
            "bis_re_sub_opt_type_id" => array("1", "1"),
            "bis_re_eval_type" => "1",
            "bis_add_rule_type" => "1",
            "bis_re_rule_status" => "1",
            "bis_re_rcId" => $bis_re_rcId,
            "bis_re_rId" => $logical_rule_id
        );

        foreach ($cat_ids as $key => $cat_id) {
            $rules_vo->set_name($rule_name.rand(0, 1000000));
            $rules_vo->set_rule_type_value(array($cat_id));
            $bis_criteria = array_merge($bis_criteria, array(
                        "bis_re_rule_value" => array($post_duration[$key], $current_ip)
                    ));
            bis_create_logical_rule_new($rules_vo, $bis_criteria, "user_based");
        }
    }
}