<?php


/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

namespace bis\repf\model;
use bis\repf\vo\QueryFilterVO;
use bis\repf\common\RulesEngineCacheWrapper;
use bis\repf\vo\LogicalRulesCriteriaVO;
use RulesEngineUtil;
use bis\repf\vo\StdToQueryVO;
use bis\repf\model\LogicalRulesEngineModel;

/**
 * Class QueryBuilderModel
 */
class QueryBuilderModel extends BaseRulesEngineModel {
    
    public $condtion_array = null;
    public $criteria_array = null;
    public $main_condition = null;
    public $grop_condition = null;
    public $rule_criteria_array = array();
    public $group_no = null;
    public $present_edit_criteria = null;
    public $val_and_con_array = null;
    
    private $condition_map  = array("1"=>"equal", "2"=>"not_equal", 
        "3" => "begins_with", "4" =>"contains_only", "5" => "does_not_contains", "6"=>"greater_than",
        "7"=>"less_than", "9"=>"ends_with", "12"=>"contains_any_of", "14"=>"does_not_contains_any_of",
        "8"=> "domain_is", "10"=>"is_set", "11"=>"is_not_set", "13"=>"pattern_match", "15"=>"near_by", 
        "16" => "equal_to", "17" => "gtr_than", "18" => "les_than",
    );
    private $condition_map_reverse  = array("equal"=> 1, "not_equal"=>2, 
         "begins_with"=>3,  "contains_only"=>4, "does_not_contains"=>5, "greater_than"=>6,
        "less_than"=>7, "ends_with"=>9, "contains_any_of"=>12, "does_not_contains_any_of"=>14,
        "domain_is"=>8,  "is_set"=>10, "is_not_set"=>11, "pattern_match"=>13, "near_by"=>15,
        "equal_to" => 16 , "gtr_than" => 17, "les_than" => 18,
    );
    private $condition = array("is equal to"=> "equal", "is not equal to"=>"not_equal", 
         "begins with"=>"begins_with",  "contains"=>"contains_only", "does not contain"=>"does_not_contains", "greater than"=>"greater_than",
        "less than"=>"less_than", "ends with"=>"ends_with", "contains any of"=>"contains_any_of", "does not contains any of"=>"does_not_contains_any_of",
        "domain is"=>"domain_is",  "is set"=>"is_set", "is not set"=>"is_not_set", "pattern match"=>"pattern_match", "near by"=>"near_by",
        "equal to" => "equal_to", "Greater than" => "gtr_than", "Less than" => "les_than"
    );
    
    private $operator_map = array("AND"=>1, "OR"=>2);
    
    public function get_all_query_builder_data($plugin_type) {
        
        $total_rows = $this->get_query_filter($plugin_type);
        
        $dataArray = array();
        $plugin_confing_date_time = array(
            BIS_DATE_PICKER => true,
            BIS_TIME_PICKER => true,
            BIS_TIME_DTAE_FORMAT => 'Y-m-d H:i',
            BIS_CLOSE_ON_DATE_TIME_SELECT => true
        );
        $plugin_confing_date = array(
            BIS_DATE_PICKER => true,
            BIS_TIME_PICKER => false,
            BIS_TIME_DTAE_FORMAT => 'Y-m-d',
            BIS_CLOSE_ON_DATE_TIME_SELECT => true
        );
        $plugin_confing_time = array(
            BIS_DATE_PICKER => false,
            BIS_TIME_PICKER => true,
            BIS_TIME_DTAE_FORMAT => 'H:i',
            BIS_CLOSE_ON_DATE_TIME_SELECT => true
        );

        foreach ($total_rows as $row) {
            
            $queryFilterVo = new QueryFilterVO();
            
            $queryFilterVo->setId($row->optId ."_".$row->subOptId."_".$row->subOptId."_".$row->value_type_id);
            $queryFilterVo->setLabel($row->subOptName);
            $queryFilterVo->setOptgroup($row->optName);

            switch ($row->optId) {
                case 3:
                case 11:
                case 6:
                    if($row->subOptId == 26 || $row->subOptId == 27 || $row->subOptId == 32){
                        $queryFilterVo->setPlaceholder("Enter a value, format name=value");
                    }
                    if ($row->subOptId == 6 || $row->subOptId == 31 || $row->subOptId == 15 || $row->subOptId == 35) {
                        $queryFilterVo->setPlaceholder("Enter a value");
                    }
                    break;
                case 7:
                case 2:
                    switch ($row->subOptId) {
                        case 9:
                        case 3:
                            $queryFilterVo->setPlugin_config($plugin_confing_date);
                            $queryFilterVo->setPlaceholder("____-__-__");
                            break;
                        case 10:
                            $queryFilterVo->setPlugin_config($plugin_confing_time);
                            $queryFilterVo->setPlaceholder("__:__");
                            break;
                        case 14:
                            $queryFilterVo->setPlugin_config($plugin_confing_date_time);
                            $queryFilterVo->setPlaceholder("____-__-__ __:__");
                            break;
                    }
                    break;
                case 14:
                    if ($row->subOptId == 2000 || $row->subOptId == "2000") {
                       
                        if (RulesEngineUtil::is_woocommerce_installed()) {
                            $bis_attr_taxonomies = $this->get_woo_attribute_taxonomies();

                        if (!empty($bis_attr_taxonomies['data'])) {
                                $bis_attr_list = $bis_attr_taxonomies['data'];
                                $row->value_type_id = 1;
                                foreach ($bis_attr_list as $bis_list) {
                                    $id = $row->optId . "_" . $row->subOptId . "_" . $bis_list->id . "_" . $row->value_type_id."_". $bis_list->slug;
                                    
                                    $queryFilterVo = new QueryFilterVO();
                                    $queryFilterVo->setId($id);
                                    $queryFilterVo->setOptgroup($row->optName);
                                    $queryFilterVo->setLabel($bis_list->name);
                                   
                                    $values = $this->get_values_based_on_suptionId($row->subOptId, $row->value_type_id);
                                    $queryFilterVo->setValues($values);
                                    $conditons = $this->get_conditions_based_on_suboptId($row->subOptId, $plugin_type);
                                    $queryFilterVo->setOperators($conditons);

                                    array_push($dataArray, $queryFilterVo);
                                }
                            }        

                        }
                        
                    }    
                    break;
            }
            
            switch ($row->value_type_id) {
                case 2:
                    $queryFilterVo->setType("integer");
                    $queryFilterVo->setInput("select");
                    break;
                case 3:
                    $queryFilterVo->setType("date");
                    $queryFilterVo->setPlugin("datetimepicker");
                    break;
                case 1:
                    break;
            }
            if ($row->subOptId != 2000) {
                $values = $this->get_values_based_on_suptionId($row->subOptId, $row->value_type_id);
                $queryFilterVo->setValues($values);
                $conditons = $this->get_conditions_based_on_suboptId($row->subOptId, $plugin_type);
                $queryFilterVo->setOperators($conditons);
                
                array_push($dataArray, $queryFilterVo);
            }
        }
       
        return $dataArray;
    }
    
    public function converting_to_query_rule($rule_criteria) {

        $dataArray = null;
        $logical_rule_engine_modal = new LogicalRulesEngineModel();
        foreach ($rule_criteria as $row) {

            $std_to_query_vo = new StdToQueryVO();

            $std_to_query_vo->setId($row->optId . "_" . $row->subOptId . "_" . $row->subOptId . "_" . $row->valueTypeId);
            $std_to_query_vo->setField($row->optId . "_" . $row->subOptId . "_" . $row->subOptId . "_" . $row->valueTypeId);
            $std_to_query_vo->setType("string");
            $std_to_query_vo->setInput("text");
            $std_to_query_vo->setOperator($this->condition[$row->ruleCondition]);
            
            switch ($row->valueTypeId) {
                case 1 : // token input
                    if ($row->subOptId === "21" || $row->subOptId === "22") {
                        $pages_json = $logical_rule_engine_modal->get_posts_by_ids(json_decode($row->value));
                        $row->value = json_encode($pages_json);
                    } else if ($row->subOptId === "17") {
                        $categories_json = $logical_rule_engine_modal->get_wp_categories_by_ids(json_decode($row->value));
                        $row->value = json_encode($categories_json);
                    } else if ($row->subOptId === "25") {
                        $categories_json = $logical_rule_engine_modal->get_woocommerce_categories_by_ids(json_decode($row->value));
                        $row->value = json_encode($categories_json);
                    }
                    $std_to_query_vo->setValue(trim(json_encode($row->value), '"'));
                    break;
                case 2 : // select box
                    $array_obj = $logical_rule_engine_modal->get_rule_values($row->subOptId, true, false, false, null, $row->value);
                    foreach ($array_obj as $obj) {
                        if ($obj->id == $row->value) {
                             $row->value = $obj->id;
                        }
                    }
                    $std_to_query_vo->setValue($row->value);
                case 4 : //text
                    $rule_value = $row->value;
                    if ($row->optId == 3 || $row->subOptId == 32) {
                        $rule_value = json_decode($rule_value);
                        $rule_value = $rule_value[0]->id;
                    }
                    $std_to_query_vo->setValue($rule_value);
            }
            $dataArray[] = $std_to_query_vo;
        }
        $total_array = array(
            "condition" => "AND",
            "rules" => $dataArray
        );
        return json_encode($total_array);
    }

    public function get_conditions_based_on_suboptId($suboptId, $plugin_type){
        
        $conditons = array();
        $total_conditions = $this->get_query_conditions($plugin_type);
        foreach ($total_conditions as $total_condition) {
           if ($total_condition->sub_option_id == $suboptId) {
                array_push($conditons, $this->condition_map[$total_condition->condid]);
            }
        }
        return $conditons;
    }
    
    public function get_values_based_on_suptionId($suboptId, $type){
        $values = array();
        if($type == 2){
            $total_values = $this->get_logical_rule_values();
            foreach ($total_values as $total_value) {
                if($total_value->parent_id == $suboptId){
                    if($suboptId == "7" || $suboptId == "33"){
                        $values[$total_value->VALUE] = $total_value->display_name;
                    } else {
                        $values[$total_value->id] = $total_value->display_name;
                    }
                }
            }
        } 
        return $values;
    }

    public function get_logical_rule_values() {

        global $wpdb;
        $base_prefix = $wpdb->base_prefix;

        if (RulesEngineCacheWrapper::get_transient(BIS_GET_ALL_LOGICALRULE_VALUES)) {
            return RulesEngineCacheWrapper::get_transient(BIS_GET_ALL_LOGICALRULE_VALUES);
        }

        $table_of_values = $wpdb->get_results("SELECT parent_id, id, VALUE, display_name FROM " . $base_prefix . "bis_re_logical_rule_value");

        RulesEngineCacheWrapper::set_transient(BIS_GET_ALL_LOGICALRULE_VALUES, $table_of_values);
        return $table_of_values;
    }

    public function get_query_conditions($plugin_type) {

        global $wpdb;
        $base_prefix = $wpdb->base_prefix;
        $plugin_type_value = $plugin_type;
        if ($plugin_type_value == BIS_PLF_REDIRECT_PLUGIN_ID) {
            $table_of_conditions = $wpdb->get_results("SELECT sub_option_id, cond.id as condid, cond.name  FROM " . $base_prefix . "bis_re_condition cond JOIN "
                    . $base_prefix . "bis_re_sub_option_condition suboptcon ON suboptcon.condition_id =  cond.id ORDER BY LENGTH(cond.name)");
            
        } else {
            $table_of_conditions = $wpdb->get_results("SELECT sub_option_id, cond.id as condid, cond.name  FROM " . $base_prefix . "bis_re_condition cond JOIN "
                    . $base_prefix . "bis_re_sub_option_condition suboptcon ON suboptcon.condition_id =  cond.id WHERE cond.id !=13  ORDER BY LENGTH(cond.name)");
        }

        return $table_of_conditions;
    }
                
    public function get_query_filter($plugin_type) {

        global $wpdb;
        $base_prefix = $wpdb->base_prefix;
        $plugin_type_value = $plugin_type;
        switch ($plugin_type_value) {
            
            case BIS_PLF_PAGE_PLUGIN_ID:
                $id = "(17, 16, 25, 22, 27 ,44)";
                break;
            case BIS_PLF_PRODUCT_PLUGIN_ID:
                $id = "(17, 16, 25, 21, 22, 27)";
                break;
            case BIS_PLF_POST_PLUGIN_ID:
            case BIS_PLF_DISCOUNT_PLUGIN_ID:
                $id = "(17, 16, 25, 21, 22, 27, 36, 34, 2000 ,44)";
                break;
            case BIS_PLF_LANGUAGE_PLUGIN_ID:
            case BIS_PLF_THEME_PLUGIN_ID:
                $id = "(17, 16, 25, 27, 26, 31, 6, 32, 36, 34, 2000 ,44)";
                break;
            case BIS_PLF_CATEGORY_PLUGIN_ID:
                $id = "(21, 22, 16, 27 ,44)";
                break;
        }
        if ($plugin_type_value == BIS_PLF_REDIRECT_PLUGIN_ID || $plugin_type_value == BIS_PLF_WIDGET_PLUGIN_ID || $plugin_type_value == BIS_LOGICAL_RULE_ID || $plugin_type_value == BIS_PLF_POPUP_ADMIN_RULE) {
            $filter = $wpdb->get_results("SELECT opt.id optId, opt.name optName, subopt.id subOptId, subopt.NAME subOptName, subopt.value_type_id
                    FROM " . $base_prefix . "bis_re_option opt JOIN " . $base_prefix . "bis_re_sub_option subopt ON opt.id = subopt.option_id WHERE subopt.id NOT IN (44) ORDER BY opt.name");
        } else {
            $filter = $wpdb->get_results("SELECT opt.id optId, opt.name optName, subopt.id subOptId, subopt.NAME subOptName, subopt.value_type_id
                                FROM " . $base_prefix . "bis_re_option opt JOIN " . $base_prefix . "bis_re_sub_option subopt ON opt.id = subopt.option_id WHERE subopt.id NOT IN ".$id. " ORDER BY opt.name");
        }

        return $filter;
    }
    
    public function bis_recursion_for_query($rules, $array_size, $codition, $parent_condition, $group_count) {
        
        $index = 1;
        $logical_rules_criteria_vo = null;
        $bis_re_rule_type = "option";

        foreach ($rules as $key2 => $value2) {
            
            if ($key2 !== "condition" && $key2 !== "rules" && !(array_key_exists("condition", $value2))) {
                $logical_rules_criteria_vo = new LogicalRulesCriteriaVO();
            }

            // Condition to check group.
            if (is_array($value2) && array_key_exists("condition", $value2)) {
                
                $condition = $value2['condition'];
                $group_count = 1;
                $this->bis_recursion_for_query($value2, sizeof($value2), $condition, $parent_condition, $group_count);
                $condition = $parent_condition;
                $logical_rules_criteria_vo_up = end($this->rule_criteria_array);
                $logical_rules_criteria_vo_up->set_rightBracket($logical_rules_criteria_vo_up->get_rightBracket() + 1);
            }

            // Single Object
            if ($key2 === "rules") {
                // Get condition from parent rules
                if (isset($rules['condition'])) {
                    $condition = $rules['condition'];
                }
                $this->bis_recursion_for_query($value2, sizeof($value2), $condition, $parent_condition, $group_count);
                $group_count = 0;
            }

            // Condition for building Criteria, key become index (0, 1) etc
            if ($key2 !== "condition" && $key2 !== "rules" && !(array_key_exists("condition", $value2))) {

                $bis_re_rule_value_str = $value2["value"];
                $options = explode("_", $value2["id"]);
                $value_type_id = $options[3];
                $option_id = (int) $options[0];
                $sub_option = (int) $options[1];
                $cond = $this->condition_map_reverse[$value2["operator"]];
                    
                // Check for Input Token
                if ($value_type_id == 1) {
                    // If condition = equal or condition = not equal
                    if (($cond == 1 || $cond == 2) ||
                            (($sub_option == 2 || $sub_option == 4 || $sub_option == 5 || $sub_option == 23 || $sub_option == 24 ||
                            $sub_option == 20 || $sub_option == 21 || $sub_option == 1 || $sub_option == 18 || $sub_option == 30 ||
                            $sub_option == 22 || $sub_option == 25 || $sub_option == 17 || $sub_option == 29) && ($cond == 12 || $cond == 14) || $cond == 15)) {
                        if(is_array($bis_re_rule_value_str)) {
                            $logical_rules_engine_modal = new LogicalRulesEngineModel();
                            $cities = $logical_rules_engine_modal->get_nearby_cities($bis_re_rule_value_str[3], $bis_re_rule_value_str[4], $bis_re_rule_value_str[1], $bis_re_rule_value_str[2]);
                            $near_by_cities = json_encode((array) $cities);
                            $logical_rules_criteria_vo->set_general_col2($near_by_cities);
                            $bis_re_rule_value_str = stripslashes($bis_re_rule_value_str[0]);
                        } else {
                            $bis_re_rule_value_str = stripslashes($bis_re_rule_value_str);
                        }
                    }
                } elseif (is_array($bis_re_rule_value_str) && $sub_option == 44) {
                    $logical_rules_criteria_vo->set_general_col2($bis_re_rule_value_str[1]);
                    $bis_re_rule_value_str = $bis_re_rule_value_str[0];
                }
                    
                if ($option_id == 9 || $option_id == 10 || $option_id == 3 || $option_id == 12 || $option_id == 11) {
                    $logical_rules_criteria_vo->set_evalType(BIS_EVAL_REQUEST_TYPE);
                }
                
                // Hours based condtion
                if ($option_id == 7 && $sub_option == 46) {
                    $current_date = date('Y-m-d H:i', current_time('timestamp', 0));
                    if(RulesEngineUtil::isContains($bis_re_rule_value_str, ":")){
                        $bis_hrs_mints = explode(":", $bis_re_rule_value_str);
                        $bis_hours = $bis_hrs_mints[0];
                        $bis_minutes = $bis_hrs_mints[1];
                    } else {
                        $bis_hours = 0;
                        $bis_minutes = 0;
                    }
                    $cenvertedTime = date('Y-m-d H:i:s', strtotime('+' . $bis_hours . ' hour +' . $bis_minutes . ' minutes', strtotime($current_date)));
                    $logical_rules_criteria_vo->set_general_col2($cenvertedTime);
                }
                
                // Option is Request
                if ($option_id == 3 || $sub_option == 32) {

                    // Filter the url
                    if ($sub_option == 6 && ($cond == 1 || $cond == 2)) {
                        $bis_re_rule_value_str = RulesEngineUtil::get_filter_url($bis_re_rule_value_str);
                    }
                    $bis_re_rule_value_str = json_encode(array(array("id" => $bis_re_rule_value_str)));
                }
                    
                // Save Only Ids for Posts and Pages
                if ($option_id == 9 || $option_id == 10 || $option_id == 12) {
                    $bis_re_rule_value_str = RulesEngineUtil::get_json_ids($bis_re_rule_value_str);
                }
                    
                $logical_rules_criteria_vo->set_value($bis_re_rule_value_str);
                $logical_rules_criteria_vo->set_optionId($option_id);
                $logical_rules_criteria_vo->set_subOptionId($sub_option);
                $logical_rules_criteria_vo->set_conditionId($cond);
                $logical_rules_criteria_vo->set_ruleType($bis_re_rule_type);

                // Set the value until last, Remove this logic, we can set out side
                if ($array_size > $index) {
                    $logical_rules_criteria_vo->set_operatorId($this->operator_map[$codition]);
                } else {
                    $logical_rules_criteria_vo->set_operatorId($this->operator_map[$parent_condition]);
                }
                if ($group_count > 0) {
                    if ($index == 1) {
                        $logical_rules_criteria_vo->set_leftBracket(1);
                    }
                }
                array_push($this->rule_criteria_array, $logical_rules_criteria_vo);
                
            }
            $index++;
        }
    }

    public function bis_quey_builder_function($array_query) {
        
        $group_count = 0;
        $this->main_condition = $array_query["condition"];
        $this->bis_recursion_for_query($array_query["rules"], sizeof($array_query["rules"]), $this->main_condition, $this->main_condition, $group_count);
        $logical_rules_criteria_vo_last = end($this->rule_criteria_array);
        $logical_rules_criteria_vo_last->set_operatorId(0);
        return $this->rule_criteria_array;
    }
    
    public function get_query_builder_rule($ruleId) {

        global $wpdb;
        $table_prefix = $wpdb->prefix;

        $query_builder_rule = $wpdb->get_results("SELECT rule_query FROM " . $table_prefix . "bis_re_logical_rules WHERE id =" . $ruleId);

        return $query_builder_rule;
    }

}