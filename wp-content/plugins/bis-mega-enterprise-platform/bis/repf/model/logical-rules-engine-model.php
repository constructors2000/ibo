<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

namespace bis\repf\model;

use bis\repf\vo\SearchVO;
use bis\repf\vo\LogicalRulesVO;
use bis\repf\common\RulesEngineCacheWrapper;
use RulesEngineUtil;
use bis\repf\model\QueryBuilderModel;

/**
 * This class is a Model for Logical Rules.
 *
 */
class LogicalRulesEngineModel extends BaseRulesEngineModel {

    /**
     * Empty constructor.
     */
    public function __construct() {
        
    }

    public function get_geo_location($ip_address) {
        global $wpdb;
        $base_prefix = $wpdb->base_prefix;

        $query = $wpdb->prepare("SELECT country_code, country_name, region_name, city FROM "
                . "(SELECT country_code, country_name, region_name, city, ip_from from " . $base_prefix . "ip2location_db2"
                . " WHERE ip_to >= INET_ATON (%s) LIMIT 1) AS ip_lookup WHERE ip_from <= INET_ATON(%s)", $ip_address, $ip_address);

        $rows = $wpdb->get_row($query);

        return $rows;
    }

    public static function get_nearby_cities($lat, $lng, $distance = 50, $unit = 'mi', $limit = 50) {
        global $wpdb;
        $base_prefix = $wpdb->base_prefix;
        if ($unit == 'km') {
            $radius = 6371.009; // in kilometers
        } elseif ($unit == 'mi') {
            $radius = 3958.761; // in miles
        }

        $maxLat = (float) $lat + rad2deg($distance / $radius);
        $minLat = (float) $lat - rad2deg($distance / $radius);

        // longitude boundaries (longitude gets smaller when latitude increases)
        $maxLng = (float) $lng + rad2deg($distance / $radius / cos(deg2rad((float) $lat)));
        $minLng = (float) $lng - rad2deg($distance / $radius / cos(deg2rad((float) $lat)));

        $query = $wpdb->prepare("SELECT DISTINCT city FROM (SELECT city, longitude FROM " . $base_prefix . "ip2location_db2 
                    WHERE latitude >= %f AND latitude <= %f ) AS mytable WHERE longitude >= %f AND longitude <= %f LIMIT %d", $minLat, $maxLat, $minLng, $maxLng, $limit);

        $nearby = $wpdb->get_results($query);

        return $nearby;
    }

    public function bis_ip2location_import_db($file_path) {
        global $wpdb;
        $prefix = $wpdb->base_prefix;
        if (file_exists($file_path)) {
            $drop_table_query = 'DROP TABLE IF EXISTS ' . $prefix . 'ip2location_db2';
            $status = $wpdb->query($drop_table_query);

            $create_table_query = "CREATE TABLE " . $prefix . "ip2location_db2 (
                `ip_from` int(10) unsigned zerofill NOT NULL DEFAULT '0000000000',
                `ip_to` int(10) unsigned zerofill NOT NULL DEFAULT '0000000000',
                `country_code` char(2) NOT NULL DEFAULT '',
                `country_name` varchar(255) NOT NULL DEFAULT ' ',
                `region_name` varchar(255) NOT NULL DEFAULT ' ',
                `city` varchar(255) NOT NULL DEFAULT ' ',
                `latitude` varchar(255) NOT NULL DEFAULT ' ',
                `longitude` varchar(255) NOT NULL DEFAULT ' ',
                `zipcode` varchar(255) NOT NULL DEFAULT ' ',
                PRIMARY KEY (`ip_to`) 
            ) ENGINE=InnoDB DEFAULT CHARSET=utf16";
            $status = $wpdb->query($create_table_query);
            $import_query = 'LOAD DATA LOCAL INFILE "' . $file_path . '"
                INTO TABLE ' . $prefix . 'ip2location_db2
                FIELDS TERMINATED by \',\'
                ENCLOSED BY \'"\'
                LINES TERMINATED BY \'\r\n\' IGNORE 1 ROWS';
            $import_status = $wpdb->query($import_query);
            $execution_time=ini_get("max_execution_time");
            ini_set("max_execution_time", SET_MAX_EXECUTION_TIME );
//            $alter_query = 'ALTER TABLE ' . $prefix . 'ip2location_db2 ENGINE=InnoDB';
//            $wpdb->query($alter_query);
            $alter_query = 'ALTER TABLE ' . $prefix . 'ip2location_db2 ADD KEY (ip_from)';
            $wpdb->query($alter_query);
            $alter_query = 'ALTER TABLE ' . $prefix . 'ip2location_db2 ADD KEY (city)';
            $wpdb->query($alter_query);
            $alter_query = 'ALTER TABLE ' . $prefix . 'ip2location_db2 ADD KEY (latitude)';
            $wpdb->query($alter_query);
            $alter_query = 'ALTER TABLE ' . $prefix . 'ip2location_db2 ADD KEY (longitude)';
            $wpdb->query($alter_query);
            ini_set("max_execution_time", $execution_time);
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * This method return the unassigned and active logical rules.
     *
     * @return list of logical rules
     */
    public function get_unassigned_logical_rulenames() {

        global $wpdb;
        $table_prefix = $wpdb->prefix;
        //$blog_id = get_current_blog_id();

        $query = "SELECT id, name, description FROM " . $table_prefix . "bis_re_logical_rules brlr WHERE brlr.id
        		NOT IN (SELECT logical_rule_id FROM " . $table_prefix . "bis_re_rule_details 
                        WHERE rule_type_id = 2)";

        $rows = $wpdb->get_results($query);

        return $rows;
    }

    /**
     * This method is used to get all the eligible logical rules.
     * @param $rule_detail_id
     * @return mixed
     */
    public function get_all_eligible_logical_rules($rule_detail_id) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        //$blog_id = get_current_blog_id();
        $query = $wpdb->prepare("SELECT id, name, description FROM " . $table_prefix . "bis_re_logical_rules brlr WHERE brlr.id
				NOT IN (SELECT logical_rule_id FROM " . $table_prefix . "bis_re_rule_details brrd WHERE brrd.rule_type_id = 2
				and brrd.id != %d )", $rule_detail_id);

        $rows = $wpdb->get_results($query);

        return $rows;
    }

    /**
     * Delete,Deactivate,Activate logical rule using ruleId.
     *
     * @param $logical_ruleId
     * @internal param \unknown $ruleId .
     * @return array
     */
    public function logical_rule_action($logical_ruleId, $selectedAction, $delete_previous_criteria = null) {

        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $query = "";
        $status_array = array();

        $rule_action_ids = $logical_ruleId;

        $action = $selectedAction;

        $childs_count = null;

        if (is_array($logical_ruleId)) {
            $rule_action_ids = implode(", ", $logical_ruleId);
        }
        if ($action === "delete") {
            if ($delete_previous_criteria == null) {
                $query = "SELECT COUNT(id) as child_count FROM " . $table_prefix . "bis_re_rule_details WHERE logical_rule_id IN($rule_action_ids) AND general_col5 = 'logical_rule'";
                $row = $wpdb->get_row($query);
                $childs_count = (int) $row->child_count;
            } else {
                $childs_count = 0;
            }
            // Delete rule only if no child rules exists
            if ($childs_count == 0) {
                $query = "delete from " . $table_prefix . "bis_re_logical_rules where id in ($rule_action_ids)";
            } else {
                $status_array["status"] = "childs_rules_exists";
            }
        } else if ($action == "deactivate") {
            $query = "UPDATE " . $table_prefix . "bis_re_logical_rules SET status = 0 WHERE id in ($rule_action_ids)";
        } else {
            $query = "UPDATE " . $table_prefix . "bis_re_logical_rules SET status = 1 WHERE id in ($rule_action_ids)";
        }
        $rows = $wpdb->query($query);

        if ($rows > 0) {
            $status_array["status"] = "success";
            $this->bis_cache_priority();
        } else {
            $status_array["status"] = "error";
        }
        if ($childs_count > 0) {
            $status_array["status"] = "childs_rules_exists";
        }
        $status_array["data"] = $this->get_logical_rules_only();
        return $status_array;
    }

    /**
     * This method is used to get all the logical rules.
     * @return array
     */
    public function get_logical_rules($page_start_index = 0) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $page_size = BIS_PAGE_SIZE;
        //$blog_id = get_current_blog_id();

        $pquery = $wpdb->prepare("SELECT lr.id AS rcId, lr.name, lr.description, "
                . "lr.action_hook, lr.status FROM " . $table_prefix . "bis_re_logical_rules lr WHERE ORDER BY lr.name limit %d, %d", $page_start_index, $page_size);

        $rows = $wpdb->get_results($pquery);
        $count_query = "SELECT COUNT(*) as logical_rules_count FROM " . $table_prefix . "bis_re_logical_rules";
        //$pre_count_query = $wpdb->prepare($count_query, $blog_id);
        $logical_rules_count_arr = $wpdb->get_results($count_query);
        $total_count = $logical_rules_count_arr[0]->logical_rules_count;

        if (!empty($rows)) {
            $results_map = array("status" => "success", "data" => $rows, BIS_ROW_COUNT => $total_count);
        } else {
            $results_map = array("status" => "no_data", "data" => BIS_MESSAGE_NO_RECORD_FOUND);
        }

        return $results_map;
    }

    /**
     * This method used for search rules.
     *
     * @param SearchVO $searchVO
     * @return array of rules
     */
    public function search_logical_rules(SearchVO $searchVO = null, $page_start_index) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $page_size = BIS_PAGE_SIZE;
        $search_value = $searchVO->get_search_value();
        $search_column = "";
        //$blog_id = get_current_blog_id();

        if ($search_value != "" || $search_value != "*") {
            if ($searchVO->get_search_by() === "description") {
                $search_column = "lr.description LIKE %s ";
            } else {
                $search_column = "lr.name LIKE %s ";
            }
        } else {
            $query = "SELECT lr.id AS rcId, lr.name, lr.description, lr.action_hook, lr.status, lr.site_id, lr.general_col1, lr.general_col2, lr.eval_type, lr.add_rule_type
                  FROM " . $table_prefix . "bis_re_logical_rules lr  WHERE ORDER BY lr.name limit %d, %d";

            $rows = $wpdb->get_results($wpdb->prepare($query, $page_start_index, $page_size));
            $count_query = "SELECT COUNT(*) AS logical_rules_count FROM " . $table_prefix . "bis_re_logical_rules lr WHERE lr.status = %d " . $search_column . " ORDER BY name";
            $logical_rules_count_arr = $wpdb->get_results($wpdb->prepare($count_query, $searchVO->get_status(), '%' . $wpdb->esc_like($search_value) . '%'));
            $total_records = $logical_rules_count_arr[0]->logical_rules_count;
        }


        if ($searchVO->get_status() != "all") {

            if ($search_column != "") {
                $search_column = " AND " . $search_column;
            }

            $query = "SELECT lr.id AS rcId, lr.name, lr.description, lr.action_hook, lr.status
                  FROM " . $table_prefix . "bis_re_logical_rules lr  WHERE lr.status = %d
                   " . $search_column . " ORDER BY lr.name limit %d, %d";

            $rows = $wpdb->get_results($wpdb->prepare($query, $searchVO->get_status(), '%' . $wpdb->esc_like($search_value) . '%', $page_start_index, $page_size));
            $count_query = "SELECT COUNT(*) AS logical_rules_count FROM " . $table_prefix . "bis_re_logical_rules lr WHERE lr.status = %d " . $search_column . " ORDER BY name";
            $logical_rules_count_arr = $wpdb->get_results($wpdb->prepare($count_query, $searchVO->get_status(), '%' . $wpdb->esc_like($search_value) . '%'));
            $total_records = $logical_rules_count_arr[0]->logical_rules_count;
        } else {

            if ($search_column != "") {
                $search_column = " WHERE " . $search_column;
            }

            $query = "SELECT lr.id AS rcId, lr.name, lr.description, lr.action_hook, lr.status
                  FROM " . $table_prefix . "bis_re_logical_rules lr  " . $search_column . " ORDER BY lr.name limit %d, %d";

            $rows = $wpdb->get_results($wpdb->prepare($query, '%' . $wpdb->esc_like($search_value) . '%', $page_start_index, $page_size));
            $count_query = "SELECT COUNT(*) AS logical_rules_count FROM " . $table_prefix . "bis_re_logical_rules lr " . $search_column . " ORDER BY name";
            $logical_rules_count_arr = $wpdb->get_results($wpdb->prepare($count_query, '%' . $wpdb->esc_like($search_value) . '%'));
            $total_records = $logical_rules_count_arr[0]->logical_rules_count;
        }

        if (!empty($rows)) {
            $results_map = array(BIS_STATUS => BIS_SUCCESS, BIS_DATA => $rows, BIS_ROW_COUNT => $total_records);
        } else {
            $results_map = array(BIS_STATUS => BIS_SUCCESS_WITH_NO_DATA);
        }

        return $results_map;
    }

    /**
     *
     * Delete the list of rules.
     *
     * @param unknown $rules .
     * @return array
     */
    public function delete_logical_rules($rules) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $table = $table_prefix . "bis_re_logical_rules";
        $table_criteria = $table_prefix . "bis_re_logical_rules_criteria";
        $data = array();
        $c_rows = 0;

        $wpdb->query(BIS_DB_START_TRANSACTION);

        foreach ($rules as $key => $ruleId) {

            $data["id"] = $key;
            $c_data = array();
            $c_data["logical_rule_id"] = $key;
            $rows = $wpdb->delete($table, $data, array("%d"));

            if ($rows > 0) {
                $c_rows = $wpdb->delete($table_criteria, $c_data, array("%d"));
            }

            if ($c_rows < 0) {
                break;
            }
        }

        if ($c_rows > 0) {
            $wpdb->query(BIS_DB_COMMIT);
        } else {
            $wpdb->query(BIS_DB_ROLLBACK);
        }

        return $this->get_logical_rules();
    }

    public function get_rules_options($plugin_type_value) {
        global $wpdb;
        $base_prefix = $wpdb->base_prefix;

        switch ($plugin_type_value) {
            case BIS_PLF_THEME_PLUGIN_ID :
                $query = "SELECT id, name FROM " . $base_prefix . "bis_re_option WHERE id IN (1,2,4,5,6,7,8,13) ORDER BY NAME; ";
                break;
            case BIS_PLF_WIDGET_PLUGIN_ID :
            case BIS_PLF_CATEGORY_PLUGIN_ID :
                $query = "SELECT id, name FROM " . $base_prefix . "bis_re_option WHERE id NOT IN (9,10) ORDER BY NAME; ";
                break;
            case BIS_PLF_PAGE_PLUGIN_ID :
                $query = "SELECT id, name FROM " . $base_prefix . "bis_re_option WHERE id NOT IN (10,12) ORDER BY NAME; ";
                break;
            case BIS_PLF_POST_PLUGIN_ID :
                $query = "SELECT id, name FROM " . $base_prefix . "bis_re_option WHERE id NOT IN (9,12,10) ORDER BY NAME; ";
                break;
            case BIS_PLF_LANGUAGE_PLUGIN_ID :
                $query = "SELECT id, name FROM " . $base_prefix . "bis_re_option WHERE id NOT IN (9,10,12) ORDER BY NAME; ";
                break;
            case BIS_PLF_PROD_GEN_FIELD_RULE :
                $query = "SELECT id, name FROM " . $base_prefix . "bis_re_option WHERE id NOT IN (8,9,10,14) ORDER BY NAME; ";
                break;
            case BIS_PLF_WIDGET_ADMIN_RULE :
                $query = "SELECT id, name FROM " . $base_prefix . "bis_re_option WHERE id NOT IN (8,14) ORDER BY NAME; ";
                break;
            default:
                $query = "SELECT id, name FROM " . $base_prefix . "bis_re_option ORDER BY NAME ";
        }
        $rows = $wpdb->get_results($query);
        return $rows;
    }

    /**
     * This method is used to get all suboptions.
     * @param $optionId
     * @return mixed
     */
    public function get_rules_sub_options($optionId, $plugin_type_value) {
        global $wpdb;
        $base_prefix = $wpdb->base_prefix;
        switch ($plugin_type_value) {
            case BIS_PLF_LANGUAGE_PLUGIN_ID :
                if ($optionId == 11 || $optionId == 9 || $optionId == 10) {
                    $query = $wpdb->prepare("SELECT id, name FROM " . $base_prefix . "bis_re_sub_option
                        where option_id = %d and id NOT IN (16,44) order by name", $optionId);
                } elseif ($optionId == 3) {
                    $query = $wpdb->prepare("SELECT id, name FROM " . $base_prefix . "bis_re_sub_option
                        where option_id = %d and id NOT IN (26,27,44) order by name", $optionId);
                } else {
                    $query = $wpdb->prepare("SELECT id, name FROM " . $base_prefix . "bis_re_sub_option
                        where option_id = %d and id NOT IN (44) order by name", $optionId);
                }
                break;
            case BIS_PLF_POST_PLUGIN_ID:
            case BIS_PLF_PAGE_PLUGIN_ID:
                if ($optionId == 11 || $optionId == 9 || $optionId == 10) {
                    $query = $wpdb->prepare("SELECT id, name FROM " . $base_prefix . "bis_re_sub_option
                        where option_id = %d and id NOT IN (16,44) order by name", $optionId);
                } elseif ($optionId == 3) {
                    $query = $wpdb->prepare("SELECT id, name FROM " . $base_prefix . "bis_re_sub_option
                        where option_id = %d and id NOT IN (27,44) order by name", $optionId);
                } else {
                    $query = $wpdb->prepare("SELECT id, name FROM " . $base_prefix . "bis_re_sub_option
                        where option_id = %d and id NOT IN (44) order by name", $optionId);
                }
                break;
            case BIS_PLF_PRODUCT_PLUGIN_ID:
                $query = $wpdb->prepare("SELECT id, name FROM " . $base_prefix . "bis_re_sub_option
                    where option_id = %d order by name", $optionId);
                break;
            default:
                $query = $wpdb->prepare("SELECT id, name FROM " . $base_prefix . "bis_re_sub_option
                    where option_id = %d and id NOT IN (44) order by name", $optionId);
        }
        $rows = $wpdb->get_results($query);
        return $rows;
    }

    /**
     * This method is used to get all conditions.
     *
     * @param $optionId
     * @return array
     */
    public function get_rules_conditions($optionId) {
        global $wpdb;
        $base_prefix = $wpdb->base_prefix;
        $pquery = $wpdb->prepare("SELECT id, name FROM " . $base_prefix . "bis_re_condition WHERE id IN
					(SELECT condition_id FROM " . $base_prefix . "bis_re_sub_option_condition WHERE sub_option_id = %d)", $optionId);

        /* $pquery = $wpdb->prepare("SELECT id, name FROM " . $base_prefix . "bis_re_condition WHERE id IN
          (SELECT condition_id FROM " . $base_prefix . "bis_re_sub_option_condition WHERE sub_option_id = %d)", $optionId); */

        $rows = $wpdb->get_results($pquery);

        $row = $wpdb->get_row($wpdb->prepare("SELECT value_type_id as valueTypeId FROM " . $base_prefix . "bis_re_sub_option WHERE id = %d", $optionId));

        $rules = array("RuleConditions" => $rows, "ValueTypeId" => $row->valueTypeId);

        return $rules;
    }

    /**
     * This method is used to get the criteria list.
     * @param $logical_rule_id
     * @return int
     */
    public function get_rule_criterias_by_ruleId($logical_rule_id) {

        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $row_count = $wpdb->get_row($wpdb->prepare("SELECT COUNT(id) AS count FROM " . $table_prefix . "bis_re_logical_rules_criteria
				WHERE logical_rule_id = %d", $logical_rule_id));

        return (int) $row_count->count;
    }

    /**
     * This method is used to save the logical rule.
     *
     * @param LogicalRulesVO $logical_rules_vo
     * @return unknown
     */
    public function save_rule(LogicalRulesVO $logical_rules_vo) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $results_map = array();
        $table = $table_prefix . "bis_re_logical_rules";
        //$blog_id = get_current_blog_id();
        $data = array('name' => $logical_rules_vo->get_name(), 'description' => $logical_rules_vo->get_description(),
            'action_hook' => $logical_rules_vo->get_actionHook(), 'status' => $logical_rules_vo->get_status(), 'eval_type' => $logical_rules_vo->getRuleEvalType(),
            'add_rule_type' => $logical_rules_vo->getAddRuleType(), 'rule_query' => $logical_rules_vo->getQuery_builder());

        // Start transaction
        $wpdb->query(BIS_DB_START_TRANSACTION);

        $status = $wpdb->insert($table, $data, array("%s", "%s", "%s", "%d", "%d", "%d", "%s"));

        if ($status > 0) {

            $rule_id = $wpdb->insert_id;
            $logical_rules_vo->set_id($rule_id);

            $table = $table_prefix . "bis_re_logical_rules_criteria";

            foreach ($logical_rules_vo->get_ruleCriteriaArray() as $rules_criteria_vo) {

                $data = array('option_id' => $rules_criteria_vo->get_optionId(), 'sub_option_id' => $rules_criteria_vo->get_subOptionId(),
                    'condition_id' => $rules_criteria_vo->get_conditionId(), 'value' => $rules_criteria_vo->get_value(), 'logical_rule_id' => $rule_id,
                    'operator_id' => $rules_criteria_vo->get_operatorId(), 'left_bracket' => $rules_criteria_vo->get_leftBracket(),
                    'right_bracket' => $rules_criteria_vo->get_rightBracket(), 'eval_type' => $rules_criteria_vo->get_evalType(),
                    'general_col1' => $rules_criteria_vo->get_general_col1(), 'general_col2' => $rules_criteria_vo->get_general_col2());

                $status = $wpdb->insert($table, $data, array("%d", "%d", "%d", "%s", "%d", "%d", "%d", "%d", "%d", "%s", "%s"));

                // No insert then exit loop
                if ($status != 1) {
                    $status = 0;
                    break;
                }
            }
        }

        if ($status == 0) {
            $results_map[BIS_STATUS] = BIS_ERROR;
            if (RulesEngineUtil::isContains($wpdb->last_error, BIS_DUPLICATE_ENTRY_SQL_MESSAGE)) {
                $results_map[BIS_MESSAGE_KEY] = BIS_DUPLICATE_ENTRY;
            } else {
                $results_map[BIS_MESSAGE_KEY] = BIS_GENERIC_DATABASE_ERROR;
            }
            $wpdb->query(BIS_DB_ROLLBACK);
        } else {
            $wpdb->query(BIS_DB_COMMIT);
            $results_map[BIS_STATUS] = BIS_SUCCESS;
            $this->bis_cache_priority();
        }

        return $results_map;
    }

    /**
     * This method is used to return the logical rule.
     * 
     * @global type $wpdb
     * @param type $ruleId
     * @return type
     */
    public function get_logical_rule($ruleId) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $rule_query = $wpdb->prepare("SELECT lr.id AS rId, lr.name, lr.description, 
                            lr.action_hook, lr.status, lr.eval_type, lr.add_rule_type FROM " . $table_prefix . "bis_re_logical_rules lr WHERE id = %d", $ruleId);


        $rows = $wpdb->get_row($rule_query);

        if (!empty($rows)) {
            $results_map = array(BIS_STATUS => "success", BIS_DATA => $rows, BIS_ROW_COUNT => null);
        } else {
            $results_map = array(BIS_STATUS => "no_data", BIS_DATA => BIS_MESSAGE_NO_RECORD_FOUND);
        }

        return $results_map;
    }

    /**
     * This method will return the rule details based on ruleId
     *
     * @param unknown $ruleId
     * @return multitype:unknown
     */
    public function get_rule($ruleId) {
        global $wpdb;

        $base_prefix = $wpdb->base_prefix;
        $table_prefix = $wpdb->prefix;

        $rule_query = $wpdb->prepare("SELECT lr.id AS rId, lr.name, lr.description, lr.action_hook, lr.status, lr.eval_type, lr.add_rule_type 
				FROM " . $table_prefix . "bis_re_logical_rules lr WHERE id = %d", $ruleId);

        $rule_criteria_query = $wpdb->prepare("SELECT rc.left_bracket lb, rc.right_bracket rb, op.id AS optId, sop.id AS subOptId, sop.value_type_id as valueTypeId, rcon.id AS condId,
				op.name AS criteria, sop.name AS subcriteria, rcon.name AS ruleCondition, rc.id AS rcId, rc.logical_rule_id AS rId,
				rc.value, rc.operator_id as operId, rc.general_col1 as gencol1 , rc.general_col2 as gencol2 FROM " . $table_prefix . "bis_re_logical_rules_criteria rc 
                                JOIN " . $base_prefix . "bis_re_option op ON rc.option_id = op.id 
				JOIN " . $base_prefix . "bis_re_sub_option sop ON sop.id = rc.sub_option_id 
				JOIN " . $base_prefix . "bis_re_condition rcon ON rcon.id = rc.condition_id AND logical_rule_id = %d", $ruleId);

        $rule = $wpdb->get_row($rule_query);


        $rule_criteria = $wpdb->get_results($rule_criteria_query);

        $rule_details = array("rule" => $rule, "rule_criteria" => $rule_criteria);

        return $rule_details;
    }

    public function get_user_emails() {
        return get_users();
    }

    public function get_userIds() {
        return get_users();
    }

    /**
     * This method is used to get pages.
     *
     * @param $page_ids
     * @return mixed
     */
    public function get_pages_by_ids($page_ids) {

        return $this->get_posts_by_ids($page_ids);
    }

    /**
     * This method is used to get the posts.
     *
     * @param $post_ids
     * @return mixed
     */
    public function get_posts_by_ids($post_ids) {
        global $wpdb;

        $post_id_arr = array();
        foreach ($post_ids as $post_id) {
            array_push($post_id_arr, $post_id->id);
        }

        $post_ids = implode(",", $post_id_arr);
        $table_name = $wpdb->prefix . 'posts';
        $query = "SELECT id, post_title as name FROM " . $table_name . " where id in ($post_ids)";
        $rows = $wpdb->get_results($query);
        return $rows;
    }

    /**
     * This method is used for getting the list of wordpress categories based on id.
     * 
     * @param type $categories_ids
     * @return type
     */
    public function get_wp_categories_by_ids($categories_ids) {
        $taxonomy = 'category';
        return $this->get_categories_by_ids($taxonomy, $categories_ids);
    }

    /**
     * This method is used for getting the list of woocommerce categories based on id.
     * 
     * @param type $categories_ids
     * @return type
     */
    public function get_woocommerce_categories_by_ids($categories_ids) {
        $taxonomy = 'product_cat';
        return $this->get_categories_by_ids($taxonomy, $categories_ids);
    }

    /**
     * This method is used to return the categories json representation based on
     * category ids.
     * 
     * @param type $taxonomy
     * @param type $categories_ids
     * @return type
     */
    public function get_categories_by_ids($taxonomy, $categories_ids) {

        $category_include = "";
        foreach ($categories_ids as $categories_id) {
            $category_include = $category_include . $categories_id->id . ",";
        }

        $args = array(
            'orderby' => 'name',
            'order' => 'ASC',
            'hide_empty' => FALSE,
            'include' => $category_include,
            'taxonomy' => $taxonomy
        );

        $categories = get_categories($args);

        $valArr = array();
        $count = 0;

        if (!empty($categories)) {
            foreach ($categories as $category) {
                $valArr[$count++] = array('id' => $category->term_id, 'name' => $category->name);
            }
        }

        return $valArr;
    }

    /**
     * This method is used to get the all authors.
     *
     * @return mixed
     */
    public function get_authors() {

        return get_users('orderby=display_name&role=author');
    }

    /**
     * This method is used to get contributors.
     *
     * @return mixed
     */
    public function get_contributor() {

        return get_users('orderby=display_name&role=contributor');
    }

    /**
     * This method is used to get the author Ids.
     *
     * @return mixed
     */
    public function get_authorsIds() {

        return get_users('orderby=user_login&role=author');
    }

    /**
     * This method is used to get the contributorIds.
     *
     * @return mixed
     */
    public function get_contributorIds() {

        return get_users('orderby=user_login&role=contributor');
    }

    /**
     *
     * This method is used to save
     *  the logical rules.
     *
     * @param LogicalRulesVO $logical_rules_vo
     * @return updated record count
     */
    public function update_rule(LogicalRulesVO $logical_rules_vo) {
        global $wpdb;

        $table_prefix = $wpdb->prefix;
        $results_map = array();

        $wpdb->query(BIS_DB_START_TRANSACTION);

        $table = $table_prefix . "bis_re_logical_rules_criteria";

        $data = array();
        $data["logical_rule_id"] = $logical_rules_vo->get_id();
        $success = $wpdb->delete($table, $data, array("%d"));

        if (!($success === false)) {

            $table = $table_prefix . "bis_re_logical_rules";
            $data = array('name' => $logical_rules_vo->get_name(), 'description' => $logical_rules_vo->get_description(),
                'action_hook' => $logical_rules_vo->get_actionHook(), 'eval_type' => $logical_rules_vo->getRuleEvalType(),
                'add_rule_type' => $logical_rules_vo->getAddRuleType(), 'status' => $logical_rules_vo->get_status(),
                'rule_query' => $logical_rules_vo->getQuery_builder());

            $where = array('id' => $logical_rules_vo->get_id());

            $success = $wpdb->update($table, $data, $where, array("%s", "%s", "%s", "%d", "%d", "%d", "%s"), array("%d"));


            if (!($success === false)) {

                $table = $table_prefix . "bis_re_logical_rules_criteria";

                foreach ($logical_rules_vo->get_ruleCriteriaArray() as $rules_criteria_vo) {

                    $data = array('option_id' => $rules_criteria_vo->get_optionId(), 'sub_option_id' => $rules_criteria_vo->get_subOptionId(),
                        'condition_id' => $rules_criteria_vo->get_conditionId(), 'value' => $rules_criteria_vo->get_value(), 'logical_rule_id' => $logical_rules_vo->get_id(),
                        'operator_id' => $rules_criteria_vo->get_operatorId(), 'left_bracket' => $rules_criteria_vo->get_leftBracket(),
                        'right_bracket' => $rules_criteria_vo->get_rightBracket(), 'eval_type' => $rules_criteria_vo->get_evalType(),
                        'general_col1' => $rules_criteria_vo->get_general_col1(), 'general_col2' => $rules_criteria_vo->get_general_col2());

                    $status = $wpdb->insert($table, $data, array("%d", "%d", "%d", "%s", "%d", "%d", "%d", "%d", "%d", "%s", "%s"));

                    // No insert then exit loop
                    if ($status != 1) {
                        $success = false;
                        break;
                    }
                }
            }
        }
        if (!($success === false)) {
            $wpdb->query(BIS_DB_COMMIT);
            $results_map[BIS_STATUS] = BIS_SUCCESS;
            $this->bis_cache_priority();
        } else {
            if (RulesEngineUtil::isContains($wpdb->last_error, BIS_DUPLICATE_ENTRY_SQL_MESSAGE)) {
                $results_map[BIS_MESSAGE_KEY] = BIS_DUPLICATE_ENTRY;
            } else {
                $results_map[BIS_MESSAGE_KEY] = BIS_GENERIC_DATABASE_ERROR;
            }
            $results_map[BIS_STATUS] = BIS_ERROR;
            $wpdb->query(BIS_DB_ROLLBACK);
        }

        return $results_map;
    }

    /**
     * This method is used to get the active logical rules.
     *
     * @return mixed
     */
    public function get_active_rules() {

        $key = BIS_RULE_LOGICAL_RULES_ACTIVE_TRANSIENT_CONST;

        if (RulesEngineCacheWrapper::get_transient($key)) {
            return RulesEngineCacheWrapper::get_transient($key);
        }

        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $rows = null;

        $val = $wpdb->get_results("SELECT 1 FROM " . $table_prefix . "bis_re_logical_rules LIMIT 1");

        if (!empty($val)) {
            $pquery = "SELECT id AS ruleId, NAME as name, action_hook, eval_type, description "
                    . "FROM " . $table_prefix . "bis_re_logical_rules WHERE STATUS = 1";

            $rows = $wpdb->get_results($pquery);
            RulesEngineCacheWrapper::set_transient($key, $rows);
        }

        return $rows;
    }

    /** This method is used to get the rule criteria using ruleId.
     * @param $logical_rule_id
     * @return mixed
     */
    public function get_rule_criteria_by_ruleId($logical_rule_id) {

        $key = BIS_RULE_CRITERIA_TRANSIENT_CONST . $logical_rule_id;

        // Get the rule criteria from session
        if (RulesEngineCacheWrapper::is_session_attribute_set($key)) {
            return RulesEngineCacheWrapper::get_session_attribute($key);
        }

        global $wpdb;

        $base_prefix = $wpdb->base_prefix;
        $table_prefix = $wpdb->prefix;

        $pquery = $wpdb->prepare("SELECT rc.id as rcId, rc.operator_id AS operId, rc.left_bracket AS lb, rc.right_bracket AS rb, lr.id AS lrId, op.id AS optId, sop.id AS subOptId, rcon.id AS condId, lr.name, lr.description, lr.action_hook, lr.status,
				op.name AS criteria, sop.name AS subcriteria, rcon.name AS ruleCondition,
				rc.value FROM " . $table_prefix . "bis_re_logical_rules_criteria rc 
                                JOIN " . $base_prefix . "bis_re_option op ON rc.option_id = op.id 
                                JOIN " . $base_prefix . "bis_re_sub_option sop ON sop.id = rc.sub_option_id 
                                JOIN " . $base_prefix . "bis_re_condition rcon ON rcon.id = rc.condition_id 
                                JOIN " . $table_prefix . "bis_re_logical_rules lr ON lr.id = rc.logical_rule_id AND lr.status = 1 where logical_rule_id = %d", $logical_rule_id);

        $rows = $wpdb->get_results($pquery);

        if ($rows != null && (count($rows) > 0)) {
            foreach ($rows as $row) {
                if ($row->subOptId == 8) {
                    $logical_rule_value = $this->get_logical_rule_value($row->value);
                    $row->value = $logical_rule_value;
                }
            }
        }

        // set the criteria values to session
        RulesEngineCacheWrapper::set_session_attribute($key, $rows);

        return $rows;
    }

    /**
     *
     * Get the rule values based on the sub option Id.
     *
     * @param $id
     * @internal param \unknown $suboption_id
     * @return unknown
     */
    public function get_logical_rule_value($id) {
        global $wpdb;

        $base_prefix = $wpdb->base_prefix;

        $pquery = $wpdb->prepare("SELECT value as rule_value FROM " . $base_prefix . "bis_re_logical_rule_value WHERE id = %d", $id);
        $row = $wpdb->get_row($pquery);

        return $row->rule_value;
    }

    /**
     * This method is used to get all the logical rules.
     * @return array
     */
    public function get_logical_rules_only($page_start_index = 0) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $page_size = BIS_PAGE_SIZE;
        //$blog_id = get_current_blog_id();

        $pquery = $wpdb->prepare("SELECT lr.id AS rcId, lr.name, lr.description, "
                . "lr.action_hook, lr.status FROM " . $table_prefix . "bis_re_logical_rules lr WHERE lr.id NOT IN(SELECT logical_rule_id FROM " . $table_prefix . "bis_re_rule_details WHERE general_col5 IS NULL OR  general_col5 <> 'logical_rule') ORDER BY lr.name limit %d, %d", $page_start_index, $page_size);

        $rows = $wpdb->get_results($pquery);
        $count_query = "SELECT COUNT(*) as logical_rules_count FROM " . $table_prefix . "bis_re_logical_rules WHERE id NOT IN(SELECT logical_rule_id FROM " . $table_prefix . "bis_re_rule_details WHERE general_col5 IS NULL OR  general_col5 <> 'logical_rule')";
        //$pre_count_query = $wpdb->prepare($count_query, $blog_id);
        $logical_rules_count_arr = $wpdb->get_results($count_query);
        $total_count = $logical_rules_count_arr[0]->logical_rules_count;

        if (!empty($rows)) {
            $results_map = array("status" => "success", "data" => $rows, BIS_ROW_COUNT => $total_count);
        } else {
            $results_map = array("status" => "no_data", "data" => BIS_MESSAGE_NO_RECORD_FOUND);
        }

        return $results_map;
    }

    /**
     * This method used for search rules.
     *
     * @param SearchVO $searchVO
     * @return array of rules
     */
    public function search_logical_rules_only(SearchVO $searchVO = null, $page_start_index) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $page_size = BIS_PAGE_SIZE;
        $search_value = $searchVO->get_search_value();
        $search_column = "";
        //$blog_id = get_current_blog_id();

        if ($search_value != "" || $search_value != "*") {
            if ($searchVO->get_search_by() === "description") {
                $search_column = "lr.description LIKE %s ";
            } else {
                $search_column = "lr.name LIKE %s ";
            }
        } else {
            $query = "SELECT lr.id AS rcId, lr.name, lr.description, lr.action_hook, lr.status, lr.site_id, lr.general_col1, lr.general_col2, lr.eval_type, lr.add_rule_type
            FROM " . $table_prefix . "bis_re_logical_rules lr  WHERE lr.id NOT IN(SELECT logical_rule_id FROM " . $table_prefix . "bis_re_rule_details WHERE general_col5 IS NULL OR  general_col5 <> 'logical_rule') ORDER BY lr.name limit %d, %d";

            $rows = $wpdb->get_results($wpdb->prepare($query, $page_start_index, $page_size));
            $count_query = "SELECT COUNT(*) AS logical_rules_count FROM " . $table_prefix . "bis_re_logical_rules lr WHERE lr.status = %d " . $search_column . " AND lr.id NOT IN(SELECT logical_rule_id FROM " . $table_prefix . "bis_re_rule_details WHERE general_col5 IS NULL OR  general_col5 <> 'logical_rule') ORDER BY name";
            $logical_rules_count_arr = $wpdb->get_results($wpdb->prepare($count_query, $searchVO->get_status(), '%' . $wpdb->esc_like($search_value) . '%'));
            $total_records = $logical_rules_count_arr[0]->logical_rules_count;
        }


        if ($searchVO->get_status() != "all") {

            if ($search_column != "") {
                $search_column = " AND " . $search_column;
            }

            $query = "SELECT lr.id AS rcId, lr.name, lr.description, lr.action_hook, lr.status
            FROM " . $table_prefix . "bis_re_logical_rules lr  WHERE lr.status = %d
            " . $search_column . " AND lr.id NOT IN(SELECT logical_rule_id FROM " . $table_prefix . "bis_re_rule_details WHERE general_col5 IS NULL OR  general_col5 <> 'logical_rule') ORDER BY lr.name limit %d, %d";

            $rows = $wpdb->get_results($wpdb->prepare($query, $searchVO->get_status(), '%' . $wpdb->esc_like($search_value) . '%', $page_start_index, $page_size));
            $count_query = "SELECT COUNT(*) AS logical_rules_count FROM " . $table_prefix . "bis_re_logical_rules lr WHERE lr.status = %d " . $search_column . " AND lr.id NOT IN(SELECT logical_rule_id FROM " . $table_prefix . "bis_re_rule_details WHERE general_col5 IS NULL OR  general_col5 <> 'logical_rule') ORDER BY name";
            $logical_rules_count_arr = $wpdb->get_results($wpdb->prepare($count_query, $searchVO->get_status(), '%' . $wpdb->esc_like($search_value) . '%'));
            $total_records = $logical_rules_count_arr[0]->logical_rules_count;
        } else {

            if ($search_column != "") {
                $search_column = " WHERE " . $search_column . "AND lr.id NOT IN(SELECT logical_rule_id FROM " . $table_prefix . "bis_re_rule_details WHERE general_col5 IS NULL OR  general_col5 <> 'logical_rule')";
            }

            $query = "SELECT lr.id AS rcId, lr.name, lr.description, lr.action_hook, lr.status
            FROM " . $table_prefix . "bis_re_logical_rules lr  " . $search_column . " ORDER BY lr.name limit %d, %d";

            $rows = $wpdb->get_results($wpdb->prepare($query, '%' . $wpdb->esc_like($search_value) . '%', $page_start_index, $page_size));
            $count_query = "SELECT COUNT(*) AS logical_rules_count FROM " . $table_prefix . "bis_re_logical_rules lr " . $search_column . " ORDER BY name";
            $logical_rules_count_arr = $wpdb->get_results($wpdb->prepare($count_query, '%' . $wpdb->esc_like($search_value) . '%'));
            $total_records = $logical_rules_count_arr[0]->logical_rules_count;
        }

        if (!empty($rows)) {
            $results_map = array(BIS_STATUS => BIS_SUCCESS, BIS_DATA => $rows, BIS_ROW_COUNT => $total_records);
        } else {
            $results_map = array(BIS_STATUS => BIS_SUCCESS_WITH_NO_DATA);
        }

        return $results_map;
    }

    /**
     * This method is used to save the logical rule.
     *
     * @param LogicalRulesVO $logical_rules_vo
     * @return unknown
     */
    public function save_rule_only(LogicalRulesVO $logical_rules_vo) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $results_map = array();
        $table = $table_prefix . "bis_re_logical_rules";
        //$blog_id = get_current_blog_id();
        $data = array('name' => $logical_rules_vo->get_name(), 'description' => $logical_rules_vo->get_description(),
            'action_hook' => $logical_rules_vo->get_actionHook(), 'status' => $logical_rules_vo->get_status(), 'eval_type' => $logical_rules_vo->getRuleEvalType(),
            'add_rule_type' => $logical_rules_vo->getAddRuleType(), 'rule_query' => $logical_rules_vo->getQuery_builder());

        // Start transaction
        $wpdb->query(BIS_DB_START_TRANSACTION);

        $status = $wpdb->insert($table, $data, array("%s", "%s", "%s", "%d", "%d", "%d", "%s"));

        if ($status > 0) {

            $rule_id = $wpdb->insert_id;
            $logical_rules_vo->set_id($rule_id);

            $table = $table_prefix . "bis_re_logical_rules_criteria";

            foreach ($logical_rules_vo->ruleCriteriaArray as $rules_criteria_vo) {

                $data = array('option_id' => $rules_criteria_vo->get_optionId(), 'sub_option_id' => $rules_criteria_vo->get_subOptionId(),
                    'condition_id' => $rules_criteria_vo->get_conditionId(), 'value' => $rules_criteria_vo->get_value(), 'logical_rule_id' => $rule_id,
                    'operator_id' => $rules_criteria_vo->get_operatorId(), 'left_bracket' => $rules_criteria_vo->get_leftBracket(),
                    'right_bracket' => $rules_criteria_vo->get_rightBracket(), 'eval_type' => $rules_criteria_vo->get_evalType(), 'general_col2' => $rules_criteria_vo->get_general_col2());

                $status = $wpdb->insert($table, $data, array("%d", "%d", "%d", "%s", "%d", "%d", "%d", "%d", "%d", "%s"));

                // No insert then exit loop
                if ($status != 1) {
                    $status = 0;
                    break;
                }
            }
        }

        if ($status == 0) {
            $results_map[BIS_STATUS] = BIS_ERROR;
            if (RulesEngineUtil::isContains($wpdb->last_error, BIS_DUPLICATE_ENTRY_SQL_MESSAGE)) {
                $results_map[BIS_MESSAGE_KEY] = BIS_DUPLICATE_ENTRY;
            } else {
                $results_map[BIS_MESSAGE_KEY] = BIS_GENERIC_DATABASE_ERROR;
            }
            $wpdb->query(BIS_DB_ROLLBACK);
        } else {
            $wpdb->query(BIS_DB_COMMIT);
            $results_map[BIS_STATUS] = BIS_SUCCESS;
        }

        return $results_map;
    }

    /**
     *
     * This method is used to update the logical rules.
     *
     * @param LogicalRulesVO $logical_rules_vo
     * @return updated record count
     */
    public function update_rule_only(LogicalRulesVO $logical_rules_vo) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $results_map = array();

        $wpdb->query(BIS_DB_START_TRANSACTION);

        $table = $table_prefix . "bis_re_logical_rules_criteria";

        $data = array();
        $data["logical_rule_id"] = $logical_rules_vo->get_id();
        $success = $wpdb->delete($table, $data, array("%d"));

        if (!($success === false)) {

            $table = $table_prefix . "bis_re_logical_rules";
            $data = array('name' => $logical_rules_vo->get_name(), 'description' => $logical_rules_vo->get_description(),
                'action_hook' => $logical_rules_vo->get_actionHook(), 'eval_type' => $logical_rules_vo->getRuleEvalType(),
                'add_rule_type' => $logical_rules_vo->getAddRuleType(), 'status' => $logical_rules_vo->get_status(), 'rule_query' => $logical_rules_vo->getQuery_builder());

            $where = array('id' => $logical_rules_vo->get_id());

            $success = $wpdb->update($table, $data, $where, array("%s", "%s", "%s", "%d", "%d", "%d", "%s"), array("%d"));


            if (!($success === false)) {

                $table = $table_prefix . "bis_re_logical_rules_criteria";

                foreach ($logical_rules_vo->ruleCriteriaArray as $rules_criteria_vo) {

                    $data = array('option_id' => $rules_criteria_vo->get_optionId(), 'sub_option_id' => $rules_criteria_vo->get_subOptionId(),
                        'condition_id' => $rules_criteria_vo->get_conditionId(), 'value' => $rules_criteria_vo->get_value(), 'logical_rule_id' => $logical_rules_vo->get_id(),
                        'operator_id' => $rules_criteria_vo->get_operatorId(), 'left_bracket' => $rules_criteria_vo->get_leftBracket(),
                        'right_bracket' => $rules_criteria_vo->get_rightBracket(), 'eval_type' => $rules_criteria_vo->get_evalType(), 'general_col2' => $rules_criteria_vo->get_general_col2());

                    $status = $wpdb->insert($table, $data, array("%d", "%d", "%d", "%s", "%d", "%d", "%d", "%d", "%d", "%s"));

                    // No insert then exit loop
                    if ($status != 1) {
                        $success = false;
                        break;
                    }
                }
            }
        }
        if (!($success === false)) {
            $wpdb->query(BIS_DB_COMMIT);
            $results_map[BIS_STATUS] = BIS_SUCCESS;
        } else {
            if (RulesEngineUtil::isContains($wpdb->last_error, BIS_DUPLICATE_ENTRY_SQL_MESSAGE)) {
                $results_map[BIS_MESSAGE_KEY] = BIS_DUPLICATE_ENTRY;
            } else {
                $results_map[BIS_MESSAGE_KEY] = BIS_GENERIC_DATABASE_ERROR;
            }
            $results_map[BIS_STATUS] = BIS_ERROR;
            $wpdb->query(BIS_DB_ROLLBACK);
        }

        return $results_map;
    }

    public function get_all_logical_rules_only() {
        global $wpdb;
        $rules = array();
        $table_prefix = $wpdb->prefix;
        $query = "SELECT lr.id AS rcId, lr.name, lr.description, lr.action_hook, lr.status, lr.site_id, lr.general_col1, lr.general_col2, lr.eval_type, lr.add_rule_type
                FROM " . $table_prefix . "bis_re_logical_rules lr  WHERE lr.status=1 AND lr.id NOT IN(SELECT logical_rule_id FROM " . $table_prefix . "bis_re_rule_details WHERE general_col5 IS NULL OR  general_col5 <> 'logical_rule') ORDER BY lr.name";
        $rows = $wpdb->get_results($query);
        foreach ($rows as $logical_rule) {
            $rule = array(
                'text' => $logical_rule->name,
                'value' => $logical_rule->rcId
            );
            array_push($rules, $rule);
        }
        return $rules;
    }

    public function std_to_query_conv() {

        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $query_builder = new QueryBuilderModel();
        $logical_ids = $wpdb->get_results("SELECT id FROM " . $table_prefix . "bis_re_logical_rules WHERE add_rule_type = 1");
        if (!empty($logical_ids)) {
            foreach ($logical_ids as $rule_id) {
                $bis_rule = $this->get_rule($rule_id->id);
                $rule_query = $query_builder->converting_to_query_rule($bis_rule["rule_criteria"]);
                $wpdb->get_results("UPDATE " . $table_prefix . "bis_re_logical_rules SET add_rule_type = 4 , rule_query ='" . $rule_query . "' WHERE ID =" . $rule_id->id);
            }
            return true;
        } else {
            return false;
        }
    }

    public function bis_cache_priority() {

        if (!function_exists('flush_rocket_htaccess')) {
            return false;
        }

        global $wpdb;
        $table_prefix = $wpdb->prefix;

        $query = "SELECT sub_option_id, COUNT(sub_option_id) AS rule_count FROM " . $table_prefix . "bis_re_logical_rules_criteria lrc JOIN  
                    " . $table_prefix . "bis_re_logical_rules blr ON blr.id = lrc.logical_rule_id WHERE blr.status = 1 AND sub_option_id IN (4,20,29,30,35) GROUP BY sub_option_id;";
        $rows = $wpdb->get_results($query);

        foreach ($rows as $logical_rule) {
            if ($logical_rule->sub_option_id == 35) {
                RulesEngineUtil::update_option(BIS_MEGA_PCODE_KEY, $logical_rule->rule_count, false, false);
            } else if ($logical_rule->sub_option_id == 29) {
                RulesEngineUtil::update_option(BIS_MEGA_CITY_KEY, $logical_rule->rule_count, false, false);
            } else if ($logical_rule->sub_option_id == 30) {
                RulesEngineUtil::update_option(BIS_MEGA_STATE_KEY, $logical_rule->rule_count, false, false);
            } else if ($logical_rule->sub_option_id == 4) {
                RulesEngineUtil::update_option(BIS_MEGA_COUNTRY_KEY, $logical_rule->rule_count, false, false);
            } else if ($logical_rule->sub_option_id == 20) {
                RulesEngineUtil::update_option(BIS_MEGA_CONTINENT_KEY, $logical_rule->rule_count, false, false);
            }
        }
    }

}
