<?php
/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

namespace bis\repf\model;

use bis\repf\vo\RulesVO;
use bis\repf\vo\LabelValueVO;

/**
 * This class is a Model for Page Rules.
 *
 */
class PageRulesEngineModel extends BaseRulesEngineModel
{


    /**
     * Save Page rules.
     *
     * @param RulesVO $rules_vo
     * @return array
     */
    public function savePageRule(RulesVO $rules_vo)
    {

        return $this->save_child_rule($rules_vo);

    }

    /**
     * update Page rules.
     *
     * @param RulesVO $rules_vo
     * @return array
     */
    public function updatePageRule(RulesVO $rules_vo)
    {

        return $this->update_child_rule($rules_vo);
    }

    /**
     * This method is used to get the applied page rules.
     *
     * @param $logical_rules
     * @return string
     */
    function get_applied_page_rules($logical_rules)
    {

        return $this->get_applied_rules($logical_rules, BIS_PAGE_TYPE_RULE);

    }

    /**
     * Delete,Deactivate and Activate page rule using ruleId.
     *
     * @param  $ruleId .
     * @return \multitype
     */
    public function page_rule_action($ruleId, $selectedAction)
    {

        $status = $this->child_rule_action($ruleId, $selectedAction);
        $results_map = $this->get_page_rules();

        if (!$status) {
            $results_map[BIS_STATUS] = BIS_ERROR;
        }

        return $results_map;
    }
    
    /**
     * This method is used to get all page rules.
     *
     * @return multitype:
     */
    public function get_page_rules($page_start_index = 0)
    {
        return $this->get_child_rules(BIS_PAGE_TYPE_RULE, $page_start_index);
    }

    /**
     * This method is used to get the Page Rule based on ruleId.
     * @param unknown $ruleId
     * @return NULL
     */
    public function get_page_rule($ruleId)
    {

        global $wpdb;
        $table_prefix = $wpdb->prefix;

        $rd_query = $wpdb->prepare("SELECT  rrd.id as page_detail_id, rrd.action_hook, rrd.name as page_rule,
                    rrd.description, rrd.parent_type_value, rrd.status, rrd.action, rlr.name AS rule_name, rlr.id AS rule_id,
                    rrd.general_col1 as gencol1, rrd.general_col2 as gencol2, rrd.general_col3 as gencol3,
                    rrd.general_col4 as gencol4, rrd.general_col5 as gencol5
                    FROM " . $table_prefix . "bis_re_rule_details rrd JOIN " . $table_prefix . "bis_re_logical_rules rlr ON rlr.id = rrd.logical_rule_id
                    WHERE rrd.id = %d", $ruleId);

        $posts_table = $wpdb->prefix . "posts";

        $wpp_query = "SELECT wpp.post_title as page_name, wpp.id as page_id FROM " . $table_prefix . "bis_re_rules rr  JOIN $posts_table wpp ON wpp.id = rr.parent_id WHERE rule_details_id = %d";
        
        $wp_menu_query = "SELECT t.term_id, t.name, t.slug FROM " . $table_prefix . "bis_re_rules rr  JOIN " . $table_prefix . "terms t ON t.slug = rr.parent_id WHERE rule_details_id = %d";

        $row = $wpdb->get_row($rd_query);
        $selected_pages = array();

        $rules_vo = new RulesVO();
        $rules_vo->set_name($row->page_rule);
        $rules_vo->set_description($row->description);
        $rules_vo->set_rule_name($row->rule_name);
        $rules_vo->set_action($row->action);
        $rules_vo->set_status($row->status);
        $rules_vo->set_id($row->page_detail_id);
        $rules_vo->set_rule_type_id(1);
        $rules_vo->set_action_hook($row->action_hook);
        $rules_vo->set_rule_id($row->rule_id);
        $rules_vo->set_general_col1($row->gencol1);
        $rules_vo->set_general_col2($row->gencol2);
        $rules_vo->set_general_col3($row->gencol3);
        $rules_vo->set_general_col4($row->gencol4);
        $rules_vo->set_general_col5($row->gencol5);

        if($row->parent_type_value == "menu") {
            $menu_row = $wpdb->get_results($wpdb->prepare($wp_menu_query, $row->page_detail_id));
            $label_value_vo = new LabelValueVO();
            $label_value_vo->set_label($menu_row[0]->name);
            $label_value_vo->set_value($menu_row[0]->slug);
            array_push($selected_pages, $label_value_vo);
            $rules_vo->set_rule_type_value($selected_pages);
            $rules_vo->set_parent_type_value($row->parent_type_value);
        } else {
            $page_rows = $wpdb->get_results($wpdb->prepare($wpp_query, $row->page_detail_id));
            if (!empty($page_rows)) {
                foreach ($page_rows as $page_row) {
                    $label_value_vo = new LabelValueVO();
                    $label_value_vo->set_label($page_row->page_name);
                    $label_value_vo->set_value($page_row->page_id);
                    array_push($selected_pages, $label_value_vo);
                }
                $rules_vo->set_rule_type_value($selected_pages);
                $rules_vo->set_parent_type_value($row->parent_type_value);
            }
        }
        
        return $rules_vo;
    }
    
    /**
     * This method is used to get all redirect rules.
     *
     * @return multitype:
     */
    public function is_page_rule_exists($rule_name, $rdetail_id = 0) {

        return $this->is_child_rule_exists($rule_name, $rdetail_id, BIS_PAGE_TYPE_RULE);
    }
    
    public function bis_get_all_page_rules() {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $typeid = BIS_PAGE_TYPE_RULE;
        $query = $wpdb->prepare("SELECT * FROM " . $table_prefix . "bis_re_rule_details WHERE rule_type_id = %d AND status = 1 ORDER BY name", $typeid );
        $rows = $wpdb->get_results($query);
        if (!empty($rows)) {
            $results_map = array("status" => "success", "data" => $rows);
        } else {
            $results_map = array("status" => "no_data", "data" => BIS_MESSAGE_NO_RECORD_FOUND);
        } return $results_map;
    }
    
    public function applying_page_rule($pageids, $pagerule) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $rules_table = $table_prefix . "bis_re_rules";
        $status = 0;
        $typeid = BIS_PAGE_TYPE_RULE;
            foreach ($pageids as $pageid) {
                $data = array('parent_id' => $pageid, 'rule_details_id' => $pagerule,
                    'parent_type_id' => $typeid);
                $status = $wpdb->insert($rules_table, $data, array("%s", "%d", "%d"));
                if ($status != 1) {
                    $status = 0;
                    break;
                }
            }

        return $status;
    }
    
    public function get_page_rule_values() {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $rule_type_id = BIS_PAGE_TYPE_RULE;
        $pquery = $wpdb->prepare("SELECT brrd.id, brrd.name, brr.parent_id FROM " . $table_prefix . "bis_re_rule_details brrd 	
		INNER JOIN " . $table_prefix . "bis_re_rules brr ON brr.rule_details_id = brrd.id AND brrd.rule_type_id = %d WHERE brrd.status = 1", $rule_type_id);
        $rows = $wpdb->get_results($pquery);

        return $rows;
    }

}