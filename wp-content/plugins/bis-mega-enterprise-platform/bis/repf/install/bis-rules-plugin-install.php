<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

namespace bis\repf\install;

use bis\repf\vo\PluginVO;
use RulesEngineUtil;
use bis\repf\common\RulesEngineCacheWrapper;

if (!class_exists('BISRE_Plugin_Install')) {

    class BISRE_Plugin_Install {

        /**
         * This function is used for setting up database table for rules engine.
         */
        function bis_rules_engine_activation($network_wide) {

            if (!is_network_admin() && is_multisite()) {
                die("<span style=\"color:red\">\"Mega Platform\" 
            should be activated from network admin. </span>"
                );
            }

            // Get the installed version of plugin
            $installed_version = RulesEngineUtil::get_option(BIS_RULES_ENGINE_VERSION_CONST, is_network_admin());

            // First time installation
            if ($installed_version == false) {
                $this->bis_create_rules_engine_database($network_wide);
                $this->bis_rules_engine_install_data($network_wide);
                $this->bis_rules_engine_configure_addons($network_wide);

                RulesEngineUtil::update_option(BIS_RULES_ENGINE_DELETE_DB, "true", is_network_admin());
                RulesEngineUtil::add_option(BIS_GEO_NAME_USER, "rules4wp", is_network_admin());
            } else {
                $this->bis_rules_engine_configure_addons($network_wide);
                if (floatval($installed_version) < floatval(BIS_RULES_ENGINE_VERSION)) { // logic to check if upgrade
                    $this->bis_upgrade_rules_engine();
                }
            }

            $re_plugin_dir = explode("/bis/repf/install", plugin_basename(__FILE__));

            $re_plugin_dir_opt = RulesEngineUtil::get_option(BIS_RULES_ENGINE_PLATFORM_DIR, is_network_admin());

            if ($re_plugin_dir_opt == false) {
                RulesEngineUtil::add_option(BIS_RULES_ENGINE_PLATFORM_DIR, $re_plugin_dir[0], is_network_admin());
            } else {
                RulesEngineUtil::update_option(BIS_RULES_ENGINE_PLATFORM_DIR, $re_plugin_dir[0], is_network_admin());
            }

            $this->copy_maxmind_db();
            //do_action('bis_mega_platform_activate');
        }

        function create_upload_directory() {
            $upload_dir = wp_upload_dir();
            $upload_dirname = $upload_dir['basedir'] . '/' . BIS_UPLOAD_DIRECTORY;
            if (!file_exists($upload_dirname)) {
                wp_mkdir_p($upload_dirname);
            }

            if (file_exists($upload_dirname)) {

                /* $logical_rule_upload_dirname = $upload_dir['basedir'] . '/' . BIS_UPLOAD_DIRECTORY . '/' . BIS_LOGICAL_RULE_DIRECTORY;
                  if (!file_exists($logical_rule_upload_dirname)) {
                  wp_mkdir_p($logical_rule_upload_dirname);
                  } */
                $post_rule_upload_dirname = $upload_dir['basedir'] . '/' . BIS_UPLOAD_DIRECTORY . '/' . BIS_POST_RULE_DIRECTORY;
                if (!file_exists($post_rule_upload_dirname)) {
                    wp_mkdir_p($post_rule_upload_dirname);
                }
                $page_rule_upload_dirname = $upload_dir['basedir'] . '/' . BIS_UPLOAD_DIRECTORY . '/' . BIS_PAGE_RULE_DIRECTORY;
                if (!file_exists($page_rule_upload_dirname)) {
                    wp_mkdir_p($page_rule_upload_dirname);
                }
                $product_rule_upload_dirname = $upload_dir['basedir'] . '/' . BIS_UPLOAD_DIRECTORY . '/' . BIS_PRODUCT_RULE_DIRECTORY;
                if (!file_exists($product_rule_upload_dirname)) {
                    wp_mkdir_p($product_rule_upload_dirname);
                }
                $redirect_rule_upload_dirname = $upload_dir['basedir'] . '/' . BIS_UPLOAD_DIRECTORY . '/' . BIS_REDIRECT_RULE_DIRECTORY;
                if (!file_exists($redirect_rule_upload_dirname)) {
                    wp_mkdir_p($redirect_rule_upload_dirname);
                }
            }
        }

        function copy_maxmind_db() {
            $this->create_upload_directory();
            $upload_dir = wp_upload_dir();
            $upload_dirname = $upload_dir['basedir'] . '/' . BIS_UPLOAD_DIRECTORY;
            $bis_geo_maxmind_db_file = RulesEngineUtil::get_option(BIS_GEO_MAXMIND_DB_FILE);

            // Check if file name exists in options.
            // File doesnot exists
            if (RulesEngineUtil::isNullOrEmptyString($bis_geo_maxmind_db_file) ||
                    $bis_geo_maxmind_db_file == false) {
                $from_filePath = RulesEngineUtil::getPluginAbsPath() . 'bis/repf/MaxMind/Db/' . BIS_GEO_MAXMIND_DB_FILE_NAME;
                $to_filePath = $upload_dirname . "/" . BIS_GEO_MAXMIND_DB_FILE_NAME;
                if (!copy($from_filePath, $to_filePath)) {
                    echo "failed to copy $from_filePath to $to_filePath...\n";
                }
            } else { // file exists
                $from_filePath = RulesEngineUtil::getPluginAbsPath() . 'bis/repf/MaxMind/Db/' . $bis_geo_maxmind_db_file;
                $to_filePath = $upload_dirname . "/" . $bis_geo_maxmind_db_file;
                if (!file_exists($to_filePath) && file_exists($from_filePath)) {
                    if (!copy($from_filePath, $to_filePath)) {
                        echo "failed to copy $from_filePath to $to_filePath...\n";
                    }
                }
                // File name does not exists in the source
                // This condition for upgrade case.
                if (!file_exists($from_filePath)) {
                    $from_filePath = RulesEngineUtil::getPluginAbsPath() . 'bis/repf/MaxMind/Db/' . BIS_GEO_MAXMIND_DB_FILE_NAME;
                    $to_filePath = $upload_dirname . "/" . BIS_GEO_MAXMIND_DB_FILE_NAME;
                    if (!copy($from_filePath, $to_filePath)) {
                        echo "failed to copy $from_filePath to $to_filePath...\n";
                    } else {
                        RulesEngineUtil::update_option(BIS_GEO_MAXMIND_DB_FILE, BIS_GEO_MAXMIND_DB_FILE_NAME);
                    }
                }
            }
        }

        /**
         * This method is used to show error message if any addon is active.
         * 
         */
        function bis_rules_engine_deactivate() {
            require_once( ABSPATH . 'wp-admin/includes/plugin.php' );

            $bis_plugin_force_delete = RulesEngineUtil::get_option(BIS_RULES_ENGINE_PLUGIN_FORCE_DELETE);

            if ($bis_plugin_force_delete !== "true") {

                $bis_addons = RulesEngineUtil::get_option(BIS_RULESENGINE_ADDONS_CONST);
                $bis_addons_active = FALSE;

                if ($bis_addons != null) {

                    $bis_active_rules_json = json_decode($bis_addons);
                    foreach ($bis_active_rules_json as $ac_rule) {
                        if ($ac_rule->status == '1') {
                            $bis_addons_active = TRUE;
                        }
                    }
                }

                if ($bis_addons_active) {
                    wp_redirect("admin.php?page=bis_pg_settings&deactive=false");
                    die();
                }
            }

            do_action('bis_mega_platform_deactivate');
        }

        /**
         * This method is used for future upgrades
         *
         */
        function bis_upgrade_rules_engine() {
            // Method for future use
            $this->create_upload_directory();
            $this->bis_rules_engine_update_data();
            RulesEngineUtil::update_option(BIS_RULES_ENGINE_VERSION_CONST, BIS_RULES_ENGINE_VERSION, is_network_admin());
        }

        function bis_rules_engine_update_data() {
            global $wpdb;
            $base_prefix = $wpdb->base_prefix;
            $table_prefix = $wpdb->prefix;

            // Get the installed version of plugin
            $installed_version = RulesEngineUtil::get_option(BIS_RULES_ENGINE_VERSION_CONST, is_network_admin());

            if (floatval($installed_version) < floatval(1.08)) {
                $wpdb->query("ALTER TABLE " . $base_prefix . "bis_re_logical_rules_criteria ADD general_col2 VARCHAR(5000);");
                $wpdb->query("INSERT INTO bis_re_sub_option_condition VALUES (4, 14), (4, 12), (23, 14), (23,12), (24,14), (24,12), (20,14), (20,12), (5,14), (5,12), (21,14), (21,12), (1,14), (1,12), (18,14),"
                        . " (18,12), (30,14), (30,12), (22,14), (22,12), (25,14), (25,12), (17,14), (17,12), (29,14), (29,12), (2,14), (2,12)");
            }

            if (floatval($installed_version) < floatval(1.0)) {
                $wpdb->query("INSERT INTO bis_re_sub_option_condition VALUES (35, 3), (35,9)");
            }

            if (floatval($installed_version) < floatval(1.01)) {
                $wpdb->query("ALTER TABLE " . $base_prefix . "bis_re_logical_rules_criteria ADD general_col2 VARCHAR(5000);");
            }

            if (floatval($installed_version) < floatval(1.08)) {
                $wpdb->query("ALTER TABLE bis_re_sub_option_condition RENAME TO " . $base_prefix . "bis_re_sub_option_condition");
                $wpdb->query("ALTER TABLE bis_re_sub_option RENAME TO " . $base_prefix . "bis_re_sub_option");
                $wpdb->query("ALTER TABLE bis_re_option RENAME TO " . $base_prefix . "bis_re_option");
                $wpdb->query("ALTER TABLE bis_re_rules RENAME TO " . $table_prefix . "bis_re_rules");
                $wpdb->query("ALTER TABLE bis_re_rule_details RENAME TO " . $table_prefix . "bis_re_rule_details");
                $wpdb->query("ALTER TABLE bis_re_condition RENAME TO " . $base_prefix . "bis_re_condition");
                $wpdb->query("ALTER TABLE bis_re_logical_rule_value RENAME TO " . $base_prefix . "bis_re_logical_rule_value");
                $wpdb->query("ALTER TABLE bis_re_logical_rules_criteria RENAME TO " . $table_prefix . "bis_re_logical_rules_criteria");
                $wpdb->query("ALTER TABLE bis_re_logical_rules RENAME TO " . $table_prefix . "bis_re_logical_rules");
                $wpdb->query("ALTER TABLE bis_re_report_auth RENAME TO " . $table_prefix . "bis_re_report_auth");
                $wpdb->query("ALTER TABLE bis_re_report_data RENAME TO " . $table_prefix . "bis_re_report_data");
                $wpdb->query("ALTER TABLE bis_re_report RENAME TO " . $table_prefix . "bis_re_report");

                // Alering table suite attributes
                $wpdb->query("ALTER TABLE " . $base_prefix . "bis_re_logical_rules_criteria ADD general_col1 VARCHAR(500);");
                $wpdb->query("ALTER TABLE " . $base_prefix . "bis_re_logical_rules_criteria ADD general_col2 VARCHAR(5000);");
                $wpdb->query("ALTER TABLE " . $table_prefix . "bis_re_rules ADD general_col1 VARCHAR(100);");
                $wpdb->query("ALTER TABLE " . $table_prefix . "bis_re_rules ADD general_col2 VARCHAR(100);");
                $wpdb->query("ALTER TABLE " . $table_prefix . "bis_re_rules ADD general_col3 VARCHAR(100);");
                $wpdb->query("ALTER TABLE " . $table_prefix . "bis_re_rules ADD general_col4 VARCHAR(100);");
                $wpdb->query("ALTER TABLE " . $table_prefix . "bis_re_rule_details MODIFY COLUMN general_col5 VARCHAR(1000);");
                $wpdb->query("ALTER TABLE " . $table_prefix . "bis_re_rule_details MODIFY COLUMN general_col2 VARCHAR(2500);");
                $wpdb->query("ALTER TABLE " . $table_prefix . "bis_re_rule_details MODIFY COLUMN general_col3 VARCHAR(1000);");

                $wpdb->query("INSERT INTO " . $base_prefix . "bis_re_condition(`id` , `name`) VALUES (15,'near by');");
                $wpdb->query("INSERT INTO " . $base_prefix . "bis_re_option(`id`,`name`) VALUES (16,'Templates Value');");
                $wpdb->query("INSERT INTO " . $base_prefix . "bis_re_sub_option(`id`,`name`,`option_id`,`value_type_id`) VALUES (37,'Continent Country Template',16,1),(38,'Country Flags Template',16,1),(39,'Continent Template',16,1),(40,'Country Template',16,1),(41,'Search Box',16,1),(42,'City Template',16,1),(43,'Region Template',16,1),(44,'Distance',6,4);");
                $wpdb->query("INSERT INTO " . $base_prefix . "bis_re_sub_option_condition(`sub_option_id`,`condition_id`) VALUES (29,15),(37,1),(37,2),(38,1),(38,2),(39,1),(39,2),(40,1),(40,2),(41,1),(41,2),(42,1),(42,2),(43,1),(43,2),(44,6),(44,7),(44,16);");
                $wpdb->query("INSERT INTO " . $base_prefix . "bis_re_logical_rule_value(`value`, `display_name`, `parent_id`) VALUES ('fa_AF','Dari - دری','7');");
            }

            if (floatval($installed_version) < floatval(1.10)) {
                $wpdb->query("INSERT INTO " . $base_prefix . "bis_re_condition(`id` , `name`) VALUES (16,'equal to'), (17,'Greater'), (18,'Less than');");
                $wpdb->query("INSERT INTO " . $base_prefix . "bis_re_sub_option(`id`,`name`,`option_id`,`value_type_id`) VALUES (44,'Distance',6,4);");
                $wpdb->query("INSERT INTO " . $base_prefix . "bis_re_sub_option_condition(`sub_option_id`,`condition_id`) VALUES (44,16),(44,17),(44,18);");
            }

            if (floatval($installed_version) < floatval(1.11)) {
                $wpdb->query("ALTER TABLE " . $table_prefix . "bis_re_logical_rules MODIFY COLUMN rule_query VARCHAR(20000);");
                $wpdb->query("ALTER TABLE " . $table_prefix . "bis_re_logical_rules_criteria MODIFY COLUMN value VARCHAR(20000);");
            }
            
            if (floatval($installed_version) < floatval(1.12)) {
                $wpdb->query("INSERT INTO bis_re_sub_option_condition VALUES (41, 12), (41,14)");
            }
            
            if (floatval($installed_version) < floatval(1.13)) {
                $wpdb->query("INSERT INTO " . $base_prefix . "bis_re_option(`id`,`name`) VALUES (17,'Cart');");
                $wpdb->query("INSERT INTO " . $base_prefix . "bis_re_sub_option(`id`,`name`,`option_id`,`value_type_id`) VALUES (45,'Subtotal',17,4);");
                $wpdb->query("INSERT INTO " . $base_prefix . "bis_re_sub_option_condition(`sub_option_id`,`condition_id`) VALUES (45,1),(45,2),(45,6),(45,7);");
                $wpdb->query("INSERT INTO " . $base_prefix . "bis_re_sub_option(`id`,`name`,`option_id`,`value_type_id`) VALUES (46,'Hours',7,4);");
                $wpdb->query("INSERT INTO " . $base_prefix . "bis_re_sub_option_condition(`sub_option_id`,`condition_id`) VALUES (46,6),(46,7);");
            }
            
            if (floatval($installed_version) <= floatval(1.15)) {
                $wpdb->query("ALTER TABLE " . $table_prefix . "bis_re_rule_details MODIFY COLUMN general_col1 VARCHAR(5000);");
            }
            // Get the installed version of plugin
            $metaTemplate = RulesEngineUtil::get_option(BIS_REDIRECT_META_TEMPLATE, is_network_admin());

            if ($metaTemplate == false || RulesEngineUtil::isNullOrEmptyString($metaTemplate)) {
                $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/bis-redirect-meta-template.html");
                RulesEngineUtil::add_option(BIS_REDIRECT_META_TEMPLATE, $dynamicContent, is_network_admin());
            }
        }

        function bis_create_site_analytics_tables() {

            global $wpdb;
            $table_prefix = $wpdb->prefix;

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

            $charset_collate = $wpdb->get_charset_collate();

            /* Table structure for table `bis_re_report` */
            $bis_re_report_query = "CREATE TABLE " . $table_prefix . "bis_re_report (
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `name` varchar(100) NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE = InnoDB " . $charset_collate . ";";

            dbDelta($bis_re_report_query);

            /* Table structure for table `bis_re_report_data` */
            $bis_re_report_data_query = "CREATE TABLE " . $table_prefix . "bis_re_report_data (
                `id` bigint(100) unsigned NOT NULL AUTO_INCREMENT,
                `country` varchar(100) NOT NULL,
                `region` varchar(100) DEFAULT NULL,
                `city` varchar(100) DEFAULT NULL,
                `ipaddress` varchar(100) NOT NULL,
                `browser` varchar(100) NOT NULL,
                `device` varchar(50) NOT NULL,
                `manufacturer` varchar(50) DEFAULT NULL,
                `response_code` int(10) NOT NULL DEFAULT '200',
                `request_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `report_type_id` int(10) unsigned NOT NULL DEFAULT '1',
                `parent_report_data_id` bigint(100) unsigned DEFAULT NULL,
                `ip_verified` tinyint(1) NOT NULL DEFAULT '0',
                `site_id` int(11) NOT NULL DEFAULT '1',
                `post_id` bigint(20) unsigned DEFAULT NULL,
                `term_taxonomy_id` bigint(20) unsigned DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY " . $table_prefix . "resp_status (`response_code`),
                KEY " . $table_prefix . "req_client (`browser`),
                KEY " . $table_prefix . "country (`country`),
                KEY " . $table_prefix . "req_time (`request_time`),
                KEY " . $table_prefix . "report_type_id (`report_type_id`),
                KEY " . $table_prefix . "parent_report_data_id (`parent_report_data_id`),
                KEY " . $table_prefix . "ip_verified (`ip_verified`),
                CONSTRAINT " . $table_prefix . "report_type_const FOREIGN KEY (`report_type_id`) REFERENCES " . $table_prefix . "bis_re_report (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE = InnoDB " . $charset_collate . ";";

            dbDelta($bis_re_report_data_query);

            /* Table structure for table `bis_re_report_auth` */
            $bis_re_report_auth_query = "CREATE TABLE " . $table_prefix . "bis_re_report_auth (
                `id` bigint(100) unsigned NOT NULL AUTO_INCREMENT,
                `userid` varchar(100) NOT NULL,
                `user_role` varchar(100) NOT NULL,
                `login` timestamp NULL DEFAULT NULL,
                `logout` timestamp NULL DEFAULT '0000-00-00 00:00:00',
                `report_data_id` bigint(100) unsigned NOT NULL,
                PRIMARY KEY (`id`),
                KEY " . $table_prefix . "report_data_id (`report_data_id`),
                CONSTRAINT " . $table_prefix . "bis_re_report_auth_ibfk_1 FOREIGN KEY (`report_data_id`) REFERENCES " . $table_prefix . "bis_re_report_data (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE = InnoDB " . $charset_collate . ";";

            dbDelta($bis_re_report_auth_query);
        }

        function bis_create_demo_site_analytics_tables() {

            global $wpdb;
            $table_prefix = $wpdb->prefix;

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

            $charset_collate = $wpdb->get_charset_collate();

            /* Table structure for table `bis_re_report` */
            $bis_re_report_query = "CREATE TABLE " . $table_prefix . "bis_re_report (
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `name` varchar(100) NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE = InnoDB " . $charset_collate . ";";

            dbDelta($bis_re_report_query);

            /* Table structure for table `bis_re_report_data` */
            $bis_re_report_data_query = "CREATE TABLE " . $table_prefix . "bis_re_report_data (
                `id` bigint(100) unsigned NOT NULL AUTO_INCREMENT,
                `country` varchar(100) NOT NULL,
                `region` varchar(100) DEFAULT NULL,
                `city` varchar(100) DEFAULT NULL,
                `ipaddress` varchar(100) NOT NULL,
                `browser` varchar(100) NOT NULL,
                `device` varchar(50) NOT NULL,
                `manufacturer` varchar(50) DEFAULT NULL,
                `response_code` int(10) NOT NULL DEFAULT '200',
                `request_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `report_type_id` int(10) unsigned NOT NULL DEFAULT '1',
                `parent_report_data_id` bigint(100) unsigned DEFAULT NULL,
                `ip_verified` tinyint(1) NOT NULL DEFAULT '0',
                `site_id` int(11) NOT NULL DEFAULT '1',
                `post_id` bigint(20) unsigned DEFAULT NULL,
                `term_taxonomy_id` bigint(20) unsigned DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY " . $table_prefix . "resp_status (`response_code`),
                KEY " . $table_prefix . "req_client (`browser`),
                KEY " . $table_prefix . "country (`country`),
                KEY " . $table_prefix . "req_time (`request_time`),
                KEY " . $table_prefix . "report_type_id (`report_type_id`),
                KEY " . $table_prefix . "parent_report_data_id (`parent_report_data_id`),
                KEY " . $table_prefix . "ip_verified (`ip_verified`)
            ) ENGINE = InnoDB " . $charset_collate . ";";

            dbDelta($bis_re_report_data_query);

            /* Table structure for table `bis_re_report_auth` */
            $bis_re_report_auth_query = "CREATE TABLE " . $table_prefix . "bis_re_report_auth (
                `id` bigint(100) unsigned NOT NULL AUTO_INCREMENT,
                `userid` varchar(100) NOT NULL,
                `user_role` varchar(100) NOT NULL,
                `login` timestamp NULL DEFAULT NULL,
                `logout` timestamp NULL DEFAULT '0000-00-00 00:00:00',
                `report_data_id` bigint(100) unsigned NOT NULL,
                PRIMARY KEY (`id`),
                KEY " . $table_prefix . "report_data_id (`report_data_id`)
            ) ENGINE = InnoDB " . $charset_collate . ";";

            dbDelta($bis_re_report_auth_query);
        }

        /**
         * Plugin db cration method
         */
        function bis_create_rules_engine_database($network_wide) {

            global $wpdb;

            $base_prefix = $wpdb->base_prefix;

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

            $charset_collate = $wpdb->get_charset_collate();

            /* Table structure for table `bis_re_condition` */
            $bis_re_condition_query = "CREATE TABLE " . $base_prefix . "bis_re_condition (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `name` varchar(100) NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE = InnoDB " . $charset_collate . ";";

            dbDelta($bis_re_condition_query);

            /* Data for the table `bis_re_condition` */
            $bis_re_logical_rule_value = "CREATE TABLE " . $base_prefix . "bis_re_logical_rule_value (
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `value` varchar(50) NOT NULL,
                `display_name` varchar(70) NOT NULL,
                `parent_id` int(11) DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY " . $base_prefix . "lg_rule_parent_id (`parent_id`),
                KEY " . $base_prefix . "lg_rule_value (`value`),
                KEY " . $base_prefix . "lg_display_name (`display_name`)
            ) ENGINE = InnoDB " . $charset_collate . ";";

            dbDelta($bis_re_logical_rule_value);

            /* Table structure for table `bis_re_option` */
            $bis_re_option = "CREATE TABLE " . $base_prefix . "bis_re_option (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `name` varchar(100) NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE = InnoDB " . $charset_collate . ";";

            dbDelta($bis_re_option);

            if (is_multisite() && $network_wide) {
                $current_site = get_current_site()->domain;
                if (\RulesEngineUtil::isContains($current_site, BIS_DEMO_SITE)) {

                    $blog_id = get_current_blog_id();
                    switch_to_blog($blog_id);
                    $this->bis_create_demo_site_specific_tables();
                    $this->bis_create_demo_site_analytics_tables();
                    restore_current_blog();

                    /* Table structure for table `bis_re_sub_option` */
                    //  value_type_id 1=InputToken; 2=selectbox; 3=date; 4=textbox;
                    $bis_re_sub_option = "CREATE TABLE " . $base_prefix . "bis_re_sub_option (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `name` varchar(100) NOT NULL,
                        `option_id` int(100) NOT NULL,
                        `value_type_id` int(10) NOT NULL DEFAULT '1',
                        PRIMARY KEY (`id`),
                        KEY " . $base_prefix . "Option_Index (`option_id`),
                        KEY " . $base_prefix . "value_type_id (`value_type_id`)
                    ) ENGINE = InnoDB " . $charset_collate . ";";

                    dbDelta($bis_re_sub_option);


                    /* Table structure for table `bis_re_sub_option_condition` */
                    $bis_re_sub_option_condition = "CREATE TABLE " . $base_prefix . "bis_re_sub_option_condition (
                        `sub_option_id` int(11) NOT NULL,
                        `condition_id` int(11) NOT NULL,
                        KEY " . $base_prefix . "sub_option (`sub_option_id`),
                        KEY " . $base_prefix . "condition (`condition_id`)
                    ) ENGINE = InnoDB " . $charset_collate . ";";

                    dbDelta($bis_re_sub_option_condition);
                } else {
                    // Get all blogs in the network and activate plugin on each one
                    $blog_ids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");
                    foreach ($blog_ids as $blog_id) {
                        switch_to_blog($blog_id);
                        $this->bis_create_site_specific_tables();
                        $this->bis_create_site_analytics_tables();
                        restore_current_blog();
                    }

                    /* Table structure for table `bis_re_sub_option` */
                    //  value_type_id 1=InputToken; 2=selectbox; 3=date; 4=textbox;
                    $bis_re_sub_option = "CREATE TABLE " . $base_prefix . "bis_re_sub_option (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `name` varchar(100) NOT NULL,
                        `option_id` int(100) NOT NULL,
                        `value_type_id` int(10) NOT NULL DEFAULT '1',
                        PRIMARY KEY (`id`),
                        KEY " . $base_prefix . "Option_Index (`option_id`),
                        KEY " . $base_prefix . "value_type_id (`value_type_id`),
                        CONSTRAINT " . $base_prefix . "option_constaint FOREIGN KEY (`option_id`) REFERENCES " . $base_prefix . "bis_re_option (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                    ) ENGINE = InnoDB " . $charset_collate . ";";

                    dbDelta($bis_re_sub_option);

                    /* Table structure for table `bis_re_sub_option_condition` */
                    $bis_re_sub_option_condition = "CREATE TABLE " . $base_prefix . "bis_re_sub_option_condition (
                        `sub_option_id` int(11) NOT NULL,
                        `condition_id` int(11) NOT NULL,
                        KEY " . $base_prefix . "sub_option (`sub_option_id`),
                        KEY " . $base_prefix . "condition (`condition_id`),
                        CONSTRAINT " . $base_prefix . "sub_condition FOREIGN KEY (`condition_id`) REFERENCES " . $base_prefix . "bis_re_condition (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                        CONSTRAINT " . $base_prefix . "sub_option FOREIGN KEY (`sub_option_id`) REFERENCES " . $base_prefix . "bis_re_sub_option (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                    ) ENGINE = InnoDB " . $charset_collate . ";";

                    dbDelta($bis_re_sub_option_condition);
                }
            } else {

                $this->bis_create_site_specific_tables();
                $this->bis_create_site_analytics_tables();

                /* Table structure for table `bis_re_sub_option` */
                //  value_type_id 1=InputToken; 2=selectbox; 3=date; 4=textbox;
                $bis_re_sub_option = "CREATE TABLE " . $base_prefix . "bis_re_sub_option (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `name` varchar(100) NOT NULL,
                    `option_id` int(100) NOT NULL,
                    `value_type_id` int(10) NOT NULL DEFAULT '1',
                    PRIMARY KEY (`id`),
                    KEY " . $base_prefix . "Option_Index (`option_id`),
                    KEY " . $base_prefix . "value_type_id (`value_type_id`),
                    CONSTRAINT " . $base_prefix . "option_constaint FOREIGN KEY (`option_id`) REFERENCES " . $base_prefix . "bis_re_option (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                ) ENGINE = InnoDB " . $charset_collate . ";";

                dbDelta($bis_re_sub_option);

                /* Table structure for table `bis_re_sub_option_condition` */
                $bis_re_sub_option_condition = "CREATE TABLE " . $base_prefix . "bis_re_sub_option_condition (
                    `sub_option_id` int(11) NOT NULL,
                    `condition_id` int(11) NOT NULL,
                    KEY " . $base_prefix . "sub_option (`sub_option_id`),
                    KEY " . $base_prefix . "condition (`condition_id`),
                    CONSTRAINT " . $base_prefix . "sub_condition FOREIGN KEY (`condition_id`) REFERENCES " . $base_prefix . "bis_re_condition (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                    CONSTRAINT " . $base_prefix . "sub_option FOREIGN KEY (`sub_option_id`) REFERENCES " . $base_prefix . "bis_re_sub_option (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                ) ENGINE = InnoDB " . $charset_collate . ";";

                dbDelta($bis_re_sub_option_condition);
            }

            RulesEngineUtil::add_option(BIS_RULES_ENGINE_VERSION_CONST, BIS_RULES_ENGINE_VERSION, is_network_admin());
            RulesEngineUtil::add_option(BIS_RULES_ENGINE_ALLOWABLE_TAGS_CONST, BIS_RULES_ENGINE_ALLOWABLE_TAGS, is_network_admin());
            RulesEngineUtil::add_option(BIS_CAPTURE_ANALYTICS_DATA, "false", is_network_admin());
            RulesEngineUtil::add_option(BIS_GEO_LOOKUP_TYPE, 1, is_network_admin());
            RulesEngineUtil::add_option(BIS_GEO_MAXMIND_DB_FILE, BIS_GEO_MAXMIND_DB_FILE_NAME, is_network_admin());
            RulesEngineUtil::add_option(BIS_RULES_DEBUG_MODE_CONT, BIS_RULES_DEBUG_MODE, false, false);
            RulesEngineUtil::add_option(BIS_RULES_ENGINE_CACHE_INSTALLED, "false", false, false);
            RulesEngineUtil::add_option(BIS_MEGA_COUNTRY_DROPDOWN, "No", false, false);
            //RulesEngineUtil::add_option(BIS_NPROGRESS_BAR_COLOR_CONST, BIS_NPROGRESS_BAR_COLOR, false, false);
            //RulesEngineUtil::add_option(BIS_RULES_EVALUATION_CONT, BIS_RULES_EVALUATION, false, false);
        }

        /**
         * This function is used for setting up initial data for rules engine.
         */
        function bis_rules_engine_install_data($network_wide) {
            global $wpdb;
            $base_prefix = $wpdb->base_prefix;

            $pre_query = "SELECT COUNT(id) as count FROM " . $wpdb->prefix . "bis_re_condition";
            $rows = $wpdb->get_row($pre_query);

            if ($rows->count === '0') {
                $wpdb->query("INSERT INTO " . $base_prefix . "bis_re_condition(`id` , `name`) VALUES (1,'is equal to'),(2,'is not equal to'),(3,'begins with'),(4,'contains'),(5,'does not contain'),(6,'greater than'),(7,'less than'),(8,'domain is'),(9, 'ends with'),(10,'is set'),(11,'is not set'),(12,'contains any of'),(13,'pattern match'),(14,'does not contains any of'),(15,'near by'),(16,'equal to'), (17,'Greater than'), (18,'Less than');");
                $wpdb->query("INSERT INTO " . $base_prefix . "bis_re_logical_rule_value(`id`,`value`,`display_name`,`parent_id`) VALUES (1,'IOS','IOS',13),(2,'Andriod','Andriod',13),(3,'Windows','Windows',13),(4,'iPhone','iPhone',11),(5,'Andriod_Phone','Andriod Phone',11),(6,'BlackBerry_Phone','BlackBerry Phone',11),(7,'iPad','iPad',12),(8,'Andriod_Tablet','Andriod Tablet',12),(9,'BlackBerry_Tablet','BlackBerry Tablet',12),(10,'HP_TouchPad','HP TouchPad',12),(11,'ie','Internet Explorer',8),(12,'chrome','Google Chrome',8),(13,'firefox','Firefox',8),(14,'safari','Safari',8),(16,'301','301 - Moved Permanently',15),(17,'500','500 - Internal Server Error',17),(21,'404','404 - Not Found',16),(23,'302','302 - Found',15),(24,'307','307 - Temporary Redirect',15),(25,'','Unregistered User',19),(26,'Mobile','Mobile',28),(27,'Tablet','Tablet',28),(28,'Smart_Phone','Smart Phone',28),(29,'Windows_Phone','Windows Phone',11),(30,'opera','Opera',8),(31,'AF','Afghanistan - ا�?غانستان ',4),(32,'AL','Albania - Shqipëria',4),(33,'DZ','Algeria - الجزائر‎',4),(34,'AS','American Samoa - Amerika S�?moa',4),(35,'AD','Andorra',4),(36,'AO','Angola',4),(37,'AI','Anguilla',4),(38,'AQ','Antarctica',4),(39,'AG','Antigua and Barbuda',4),(40,'AR','Argentina',4),(41,'AM','Armenia - Հայաստան',4),(42,'AW','Aruba',4),(43,'AU','Australia',4),(44,'AT','Austria - Österreich',4),(45,'AZ','Azerbaijan - Azərbaycan',4),(46,'BH','Bahrain - البحرين',4),(47,'BD','Bangladesh - বাংলাদেশ',4),(48,'BB','Barbados',4),(49,'BY','Belarus - Белару�?ь',4),(50,'BE','Belgium - België',4),(51,'BZ','Belize',4),(52,'BJ','Benin - Bénin',4),(53,'BM','Bermuda',4),(54,'BT','Bhutan - འབྲུག་ཡུལ',4),(55,'BO','Bolivia',4),(56,'BA','Bosnia and Herzegovina - Bosna i Hercegovina',4),(57,'BW','Botswana',4),(58,'BR','Brazil - Brasil',4),(59,'IO','British Indian Ocean Territory',4),(60,'VG','British Virgin Islands',4),(61,'BN','Brunei Darussalam - بروني',4),(62,'BG','Bulgaria',4),(63,'BF','Burkina Faso',4),(64,'MM','Burma - မြန်မာ',4),(65,'BI','Burundi',4),(66,'KH','Cambodia - កម្ពុជា',4),(67,'CM','Cameroon - Cameroun',4),(68,'CA','Canada',4),(69,'CV','Cape Verde - Cabo Verde',4),(70,'KY','Cayman Islands',4),(71,'CF','Central African Republic - République Centrafricaine',4),(72,'TD','Chad - Tchad',4),(73,'CL','Chile',4),(74,'CN','China - 中国',4),(75,'CX','Christmas Island',4),(76,'CC','Cocos (Keeling) Islands',4),(77,'CO','Colombia',4),(78,'KM','Comoros - Komori',4),(79,'CD','Democratic Republic of the Congo - République démocratique du Congo',4),(80,'CG','Republic of the Congo - République du Congo',4),(81,'CK','Cook Islands',4),(82,'CR','Costa Rica',4),(83,'HR','Croatia - Hrvatska',4),(84,'CU','Cuba',4),(85,'CY','Cyprus - Kypros',4),(86,'CZ','Czech Republic - Česká republika',4),(87,'DK','Denmark - Danmark',4),(88,'DJ','Djibouti - جيبوتي',4),(89,'DM','Dominica',4),(90,'DO','Dominican Republic - República Dominicana',4),(91,'EC','Ecuador',4),(92,'EG','Egypt - مصر',4),(93,'SV','El Salvador',4),(94,'GQ','Equatorial Guinea - Guinea Ecuatorial',4),(95,'ER','Eritrea - إرتريا',4),(96,'EE','Estonia - Eesti',4),(97,'ET','Ethiopia - ኢትዮጵያ',4),(98,'FK','Falkland Islands (Islas Malvinas)',4),(99,'FO','Faroe Islands - Færøerne',4),(100,'FJ','Fiji',4),(101,'FI','Finland - Suomi',4),(102,'FR','France',4),(103,'PF','French Polynesia - Polynésie française',4),(104,'GA','Gabon',4),(105,'GE','Georgia - ს�?ქ�?რთველ�?',4),(106,'DE','Germany - Deutschland',4),(107,'GH','Ghana',4),(108,'GI','Gibraltar',4),(109,'GR','Greece - Ελλάδα',4),(110,'GL','Greenland - Kalaallit Nunaat',4),(111,'GD','Grenada',4),(112,'GU','Guam - Guåhån',4),(113,'GT','Guatemala',4),(114,'GN','Guinea - Guinée',4),(115,'GW','Guinea Bissau - Guiné-Bissau',4),(116,'GY','Guyana',4),(117,'HT','Haiti - Haïti',4),(118,'VA','Holy See (Vatican City)',4),(119,'HN','Honduras',4),(120,'HK','Hong Kong (SAR)',4),(121,'HU','Hungary - Magyarország',4),(122,'IS','Iceland - �?sland',4),(123,'IN','India - भारत',4),(124,'ID','Indonesia',4),(125,'IR','Iran - ایران',4),(126,'IQ','Iraq - العراق',4),(127,'IE','Ireland - Éire',4),(128,'IL','Israel - ישר�?ל',4),(129,'IT','Italy - Italia',4),(130,'JM','Jamaica',4),(131,'JP','Japan - 日本',4),(132,'JE','Jersey',4),(133,'JO','Jordan - الأردن',4),(134,'KZ','Kazakhstan - Қазақ�?тан',4),(135,'KE','Kenya',4),(136,'KI','Kiribati',4),(137,'KP','Korea, North',4),(138,'KR','Korea, South',4),(139,'KW','Kuwait - دولة الكويت',4),(140,'KG','Kyrgyzstan',4),(141,'LA','Laos - ປະເທດລາວ',4),(142,'LV','Latvia - Latvija',4),(143,'LB','Lebanon - لبنان',4),(144,'LS','Lesotho',4),(145,'LR','Liberia',4),(146,'LY','Libya - ليبيا',4),(147,'LI','Liechtenstein - Liechtenstein',4),(148,'LT','Lithuania - Lietuva',4),(149,'LU','Luxembourg - Lëtzebuerg',4),(150,'MO','Macao',4),(151,'MK','Macedonia - Makedonija',4),(152,'MG','Madagascar - Madagasikara',4),(153,'MW','Malawi',4),(154,'MY','Malaysia - مليسيا',4),(155,'MV','Maldives - ދިވެހިރާއްޖެ',4),(156,'ML','Mali',4),(157,'MT','Malta',4),(158,'MH','Marshall Islands',4),(159,'MR','Mauritania',4),(160,'MU','Mauritius',4),(161,'YT','Mayotte',4),(162,'MX','Mexico - México',4),(163,'FM','Micronesia, Federated States of',4),(164,'MD','Moldova',4),(165,'MC','Monaco',4),(166,'MN','Mongolia - Монгол Ул�?',4),(167,'ME','Montenegro - Црна Гора',4),(168,'MS','Montserrat',4),(169,'MA','Morocco - المغرب',4),(170,'MZ','Mozambique - Moçambique',4),(171,'MM','Myanmar',4),(172,'NA','Namibia',4),(173,'NR','Nauru',4),(174,'NP','Nepal - नेपाल',4),(175,'NL','Netherlands - Nederlân',4),(176,'AN','Netherlands Antilles',4),(177,'NC','New Caledonia - Nouvelle-Calédonie',4),(178,'NZ','New Zealand - Aotearoa',4),(179,'NI','Nicaragua',4),(180,'NE','Niger',4),(181,'NG','Nigeria',4),(182,'NU','Niue - Niuē',4),(183,'MP','Northern Mariana Islands',4),(184,'NO','Norway - Norge',4),(185,'OM','Oman - ع�?مان',4),(186,'PK','Pakistan - پاکستان',4),(187,'PW','Palau - Belau',4),(188,'PA','Panama - Panamá',4),(189,'PG','Papua New Guinea',4),(190,'PY','Paraguay',4),(191,'PE','Peru - Perú',4),(192,'PH','Philippines - Pilipinas',4),(193,'PN','Pitcairn Islands',4),(194,'PL','Poland - Polska',4),(195,'PT','Portugal',4),(196,'PR','Puerto Rico',4),(197,'QA','Qatar -  قطر',4),(198,'RO','Romania - România',4),(199,'RU','Russia - Ро�?�?и�?',4),(200,'RW','Rwanda',4),(201,'SH','Saint Helena',4),(202,'KN','Saint Kitts and Nevis',4),(203,'LC','Saint Lucia',4),(204,'PM','Saint Pierre and Miquelon - Saint-Pierre et Miquelon',4),(205,'VC','Saint Vincent and the Grenadines',4),(206,'WS','Samoa',4),(207,'SM','San Marino',4),(208,'ST','Sao Tome and Principe - São Tomé e Príncipe',4),(209,'SA','Saudi Arabia - المملكة العربية السعودية',4),(210,'SN','Senegal - Sénégal',4),(211,'RS','Serbia - Srbija',4),(212,'SC','Seychelles - Sesel',4),(213,'SL','Sierra Leone',4),(214,'SG','Singapore - Singapura',4),(215,'SK','Slovakia - Slovensko',4),(216,'SI','Slovenia - Slovenija',4),(217,'SB','Solomon Islands',4),(218,'SO','Somalia - الصومال',4),(219,'ZA','South Africa',4),(220,'ES','Spain - España',4),(221,'LK','Sri Lanka - �?්�?රී ලංක�?ව',4),(222,'SD','Sudan - السودان',4),(223,'SR','Suriname',4),(224,'SJ','Svalbard',4),(225,'SZ','Swaziland',4),(226,'SE','Sweden - Sverige',4),(227,'CH','Switzerland - Schweiz',4),(228,'SY','Syria - سورية',4),(229,'TW','Taiwan - �?��?�',4),(230,'TJ','Tajikistan - Тоҷики�?тон',4),(231,'TZ','Tanzania',4),(232,'TH','Thailand - เมืองไทย',4),(233,'TT','Trinidad and Tobago',4),(234,'TN','Tunisia - تونس',4),(235,'TR','Turkey - Türkiye',4),(236,'TM','Turkmenistan - Türkmenistan',4),(237,'TC','Turks and Caicos Islands',4),(238,'TV','Tuvalu',4),(239,'UG','Uganda',4),(240,'UA','Ukraine - Україна',4),(241,'AE','United Arab Emirates - الإمارات العربيّة المتّحدة',4),(242,'GB','United Kingdom',4),(243,'US','United States',4),(244,'UY','Uruguay - República Oriental del Uruguay',4),(245,'UZ','Uzbekistan - Ўзбеки�?тон',4),(246,'VU','Vanuatu',4),(247,'VE','Venezuela',4),(248,'VN','Vietnam - Việt Nam',4),(249,'WF','Wallis and Futuna - Wallis-et-Futuna',4),(250,'EH','Western Sahara',4),(251,'YE','Yemen - اليمن',4),(252,'ZM','Zambia',4),(253,'ZW','Zimbabwe 22',4),(254,'INR','Indian Rupee',5),(255,'AUD','Australian Dollar',5),(256,'EUR','Euro',5),(257,'BRL','Brazilian Real',5),(258,'CAD','Canadian Dollar',5),(259,'CNY','Yuan Renminbi',5),(260,'DKK','Danish Krone',5),(261,'GBP','Pound Sterling',5),(262,'HKD','Hong Kong Dollar',5),(263,'IRR','Iranian Rial',5),(264,'ILS','Israeli New Shekel',5),(265,'JPY','Japanese Yen',5),(266,'KRW','	Korean Won',5),(267,'NZD','New Zealand Dollar',5),(268,'SGD','Singapore Dollar',5),(269,'ZAR','South African Rand',5),(270,'SEK','Swedish Krona',5),(271,'CHF','Swiss Franc',5),(272,'TWD','Taiwan Dollar',5),(273,'TRY','Turkish Lira',5),(274,'GBP','Pound Sterling',5),(275,'USD','US Dollar',5),(276,'AED','Arab Emirates Dirham',5),(277,'AF','Africa',20),(278,'AN','Antarctica',20),(279,'AS','Asia',20),(280,'EU','Europe',20),(281,'NA','North america',20),(282,'OC','Oceania',20),(283,'SA','South america',20),(285,'Sunday','Sunday',23),(286,'Monday','Monday',23),(287,'Tuesday','Tuesday',23),(288,'Wednesday','Wednesday',23),(289,'Thursday','Thursday',23),(290,'Friday','Friday',23),(291,'Saturday','Saturday',23),(292,'Jan','January',24),(293,'Feb','February',24),(294,'Mar','March',24),(295,'Apr','April',24),(296,'May','May',24),(297,'Jun','June',24),(298,'Jul','July',24),(299,'Aug','August',24),(300,'Sep','September',24),(301,'Oct','October',24),(302,'Nov','November',24),(303,'Dec','December',24),(304,'hide_page','Hide Page',1000),(305,'replace_page_content','Replace Page Content',1000),(306,'append_page_content','Append content to Page',1000),(308,'append_image_page','Append image to Page',1000),(309,'pos_top_page','Top of Page',1001),(310,'pos_bottom_page','Bottom of Page',1001),(311,'pos_dialog_page','Show as Dialog',1001),(312,'large','Large',1002),(313,'medium','Medium',1002),(314,'thumbnail','Thumbnail',1002),(315,'full','Full',1002),(316,'pos_cust_scode_page','Generate Short Code',1001),(317,'append_image_bg_page','Append content with image background',1000),(318,'hide_post','Hide Post',1003),(319,'replace_post_content','Replace Post Content',1003),(320,'append_post_content','Append content to Post',1003),(321,'append_image_post','Append image to Post',1003),(322,'pos_top_post','Top of Post',1004),(323,'pos_bottom_post','Bottom of Post',1004),(324,'pos_dialog_post','Show as Dialog',1004),(325,'pos_cust_scode_post','Generate Short Code',1004),(326,'append_image_bg_post','Append content with image background',1003),(327,'append_existing_scode_page','Append Third-Party Shortcode',1000),(328,'append_existing_scode_post','Append Third-Party Shortcode',1003),(329,'en_US','English (United States)',7),(330,'ar','Arabic - العربية',7),(331,'az','Azerbaijani - Azərbaycan dili',7),(332,'bg_BG','Bulgarian - Българ�?ки',7),(333,'bs_BA','Bosnian - Bosanski',7),(334,'ca','Catalan - Català',7),(335,'cy','Welsh - Cymraeg',7),(336,'da_DK','Danish - Dansk',7),(337,'de_CH','German (Switzerland) - Deutsch (Schweiz)',7),(338,'de_DE','German - Deutsch',7),(339,'el','Greek - Ελληνικά',7),(340,'en_CA','English (Canada) - English (Canada)',7),(341,'en_AU','English (Australia) - English (Australia)',7),(342,'en_GB','English (UK) - English (UK)',7),(343,'eo','Esperanto - Esperanto',7),(344,'es_PE','Spanish (Peru) - Español de Perú',7),(345,'es_MX','Spanish (Mexico) - Español de México',7),(346,'es_ES','Spanish (Spain) - Español',7),(347,'es_CL','Spanish (Chile) - Español de Chile',7),(348,'eu','Basque - Euskara',7),(349,'fa_IR','Persian - �?ارسی',7),(350,'fi','Finnish - Suomi',7),(351,'fr_FR','French (France) - Français',7),(352,'gd','Scottish Gaelic - Gàidhlig',7),(353,'gl_ES','Galician - Galego',7),(354,'haz','Hazaragi - هزاره گی',7),(355,'he_IL','Hebrew - עִבְרִית',7),(356,'hr','Croatian - Hrvatski',7),(357,'hu_HU','Hungarian - Magyar',7),(358,'id_ID','Indonesian - Bahasa Indonesia',7),(359,'is_IS','Icelandic - �?slenska',7),(360,'it_IT','Italian - Italiano',7),(361,'ja','Japanese - 日本語',7),(362,'ko_KR','Korean - 한국어',7),(363,'lt_LT','Lithuanian - Lietuvių kalba',7),(364,'my_MM','Myanmar (Burmese) - ဗမာစာ',7),(365,'nb_NO','Norwegian (Bokmål) - Norsk bokmål',7),(366,'nl_NL','Dutch - Nederlands',7),(367,'nn_NO','Norwegian (Nynorsk) - Norsk nynorsk',7),(368,'oci','Occitan - Occitan',7),(369,'pl_PL','Polish - Polski',7),(370,'ps','Pashto - پښتو',7),(371,'pt_BR','Portuguese (Brazil) - Português do Brasil',7),(372,'pt_PT','Portuguese (Portugal) - Português',7),(373,'ro_RO','Romanian - Română',7),(374,'ru_RU','Russian - Ру�?�?кий',7),(375,'sk_SK','Slovak - Sloven�?ina',7),(376,'sl_SI','Slovenian - Slovenš�?ina',7),(377,'sq','Albanian - Shqip',7),(378,'sr_RS','Serbian - Срп�?ки језик',7),(379,'sv_SE','Swedish - Svenska',7),(380,'th','Thai - ไทย',7),(381,'tr_TR','Turkish - Türkçe',7),(382,'ug_CN','Uighur - Uyƣurqə',7),(383,'uk','Ukrainian - Україн�?ька',7),(384,'zh_CN','Chinese (China) - 简体中文',7),(385,'zh_TW','Chinese (Taiwan) - �?體中文',7),(386,'soft_page_hide','Soft Page Hide',1000),(388,'nt','Windows',33),(389,'lin','Linux',33),(390,'mac','Macintosh',33),(391,'unix','Unix',33),(392,'show_post','Show Post',1003),(393,'fa_AF','Dari - دری','7');");
                $wpdb->query("INSERT INTO " . $base_prefix . "bis_re_option(`id`,`name`) VALUES (1,'User Role'),(2,'User Profile'),(3,'Request'),(4,'Mobile Device'),(5,'Language'),(6,'Geo Location'),(7,'Date and Time'),(8,'Browser'),(9,'Page'),(10,'Post'),(11,'Response'),(12,'Category'),(13,'Operating System'),(14,'Woo Attribute'),(15,'Tags'),(16,'Templates Value'),(17,'Cart');");
                $wpdb->query("INSERT INTO " . $base_prefix . "bis_re_sub_option(`id`,`name`,`option_id`,`value_type_id`) VALUES (1,'User Role Name',1,1),(2,'Email',2,1),(3,'Registered Date',2,3),(4,'Country',6,1),(5,'Currency',6,1),(6,'URL',3,4),(7,'Language Name',5,2),(8,'Browser Name',8,2),(9,'Date',7,3),(10,'Time',7,3),(11,'Mobile',4,2),(12,'Tablet',4,2),(13,'Mobile Operating System',4,2),(14,'Date and Time',7,3),(15,'IP Address',6,4),(16,'Status Code',11,2),(17,'WordPress',12,1),(18,'User Id',2,1),(19,'Unregistered(Not Logged In)',2,2),(20,'Continent',6,1),(21,'Page Title',9,1),(22,'Post Title',10,1),(23,'Day of Week',7,1),(24,'Month',7,1),(25,'WooCommerce',12,1),(26,'Parameter',3,4),(27,'Form Data',3,4),(28,'Mobile Device Type',4,2),(29,'City',6,1),(30,'State or Region',6,1),(31,'Referral URL',3,4),(32,'Cookie',11,4),(33,'OS Name',13,2),(34,'Woo Tags',15,1),(35,'Postal Code',6,4),(36,'WP Tags',15,1),(37,'Continent Country Template',16,1),(38,'Country Flags Template',16,1),(39,'Continent Template',16,1),(40,'Country Template',16,1),(41,'Search Box',16,1),(42,'City Template',16,1),(43,'Region Template',16,1),(44,'Distance',6,4),(45,'Subtotal',17,4),(46,'Hours',7,4),(2000,'Woo Attribute',14,1);");
                $wpdb->query("INSERT INTO " . $base_prefix . "bis_re_sub_option_condition(`sub_option_id`,`condition_id`) VALUES (1,1),(1,2),(1,12),(1,14),(2,1),(2,2),(2,4),(2,5),(2,8),(2,12),(2,14),(3,1),(3,2),(3,6),(3,7),(4,1),(4,2),(4,12),(4,14),(5,1),(5,2),(5,12),(5,14),(6,1),(6,2),(6,4),(6,5),(6,13),(7,1),(7,2),(8,1),(8,2),(9,1),(9,2),(9,6),(9,7),(10,1),(10,2),(10,6),(10,7),(11,1),(11,2),(12,1),(12,2),(13,1),(13,2),(14,1),(14,2),(14,6),(14,7),(15,1),(15,2),(15,3),(15,4),(15,5),(15,9),(15,12),(15,14),(16,1),(16,2),(17,1),(17,2),(17,12),(17,14),(18,1),(18,2),(18,12),(18,14),(19,1),(19,2),(20,1),(20,2),(20,12),(20,14),(21,1),(21,2),(21,12),(21,14),(22,1),(22,2),(22,12),(22,14),(23,1),(23,2),(23,12),(23,14),(24,1),(24,2),(24,12),(24,14),(25,1),(25,2),(25,12),(25,14),(26,1),(26,2),(26,4),(26,5),(27,1),(27,2),(28,1),(28,2),(29,1),(29,2),(29,12),(29,14),(29,15),(30,1),(30,2),(30,12),(30,14),(31,1),(31,2),(31,4),(31,5),(32,1),(32,2),(32,10),(32,11),(33,1),(33,2),(34,1),(34,2),(34,12),(34,14),(35,1),(35,2),(35, 3),(35,4),(35,5),(35,9),(35,12),(35,14),(36,1),(36,2),(36,12),(36,14),(37,1),(37,2),(38,1),(38,2),(39,1),(39,2),(40,1),(40,2),(41,1),(41,2),(42,1),(42,2),(43,1),(43,2),(44,18),(44,17),(44,16),(45,1),(45,2),(45,6),(45,7),(46,6),(46,7),(2000,1),(2000,2),(2000,12),(2000,14);");
                $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/bis-redirect-meta-template.html");
                RulesEngineUtil::add_option(BIS_REDIRECT_META_TEMPLATE, $dynamicContent, is_network_admin());
                $popup_vo = '{
                    "title": "Choose your country",
                    "titleClass": "",
                    "headingOne": "",
                    "headingTwo": "",
                    "headingOneClass": "",
                    "headingTwoClass": "",
                    "buttonLabelOne": "",
                    "buttonLabelTwo": "",
                    "buttonOneClass": "",
                    "buttonTwoClass": "",
                    "buttonActiveColor": null,
                    "buttonInActiveColor": null,
                    "buttonHoverColor": null,
                    "popUpBackgroundColor": "",
                    "autoCloseTime": 0,
                    "imageOneUrl": null,
                    "imageOneId": null,
                    "imageOneSize": null,
                    "imageTwoUrl": null,
                    "imageTwoId": null,
                    "imageTwoSize": null,
                    "buttonOneUrl": "",
                    "buttonTwoUrl": "",
                    "checkBoxOne": 0,
                    "redirectWindow": 0,
                    "popUpOccurr": 0,
                    "tempalteFileName": "bis-country-flags-popup-template.html",
                    "selectedCountries": {
                      "AU": "Australia",
                      "CA": "Canada",
                      "FR": "France",
                      "DE": "Germany ",
                      "IN": "India ",
                      "IT": "Italy ",
                      "NL": "Netherlands ",
                      "ES": "Spain ",
                      "GB": "United Kingdom",
                      "US": "United States"
                    },
                    "continentCountryMap": null,
                    "status": "0"
                }';
                RulesEngineUtil::add_option(BIS_POPUP_VO, $popup_vo, false, false);
            }
        }

        function bis_rules_engine_configure_addons($network_wide) {

            $addon_list = array();

            // PAGE
            $pgPluginVO = new PluginVO();
            $pgPluginVO->set_id(BIS_PLATFORM_PAGE_PLUGIN_ID);
            $pgPluginVO->set_apiKey(BIS_PLATFORM_PAGE_API_KEY);
            $pgPluginVO->set_display_name(BIS_PLATFORM_PAGE_PLUGIN_DISPLAY_NAME);
            $pgPluginVO->set_description(BIS_PLATFORM_PAGE_PLUGIN_DESCRIPTION);
            $pgPluginVO->set_css_class(BIS_PLATFORM_PAGE_CSS_CLASS);
            $pgPluginVO->set_path(BIS_PLATFORM_PAGE_PLUGIN_PATH);
            $pgPluginVO->set_abs_path(BIS_PLATFORM_PAGE_PLUGIN_ABSPATH);
            $pgPluginVO->set_status(-1);
            array_push($addon_list, $pgPluginVO);

            // POST
            $pstPluginVO = new PluginVO();
            $pstPluginVO->set_id(BIS_PLATFORM_POST_PLUGIN_ID);
            $pstPluginVO->set_apiKey(BIS_PLATFORM_POST_API_KEY);
            $pstPluginVO->set_display_name(BIS_PLATFORM_POST_PLUGIN_DISPLAY_NAME);
            $pstPluginVO->set_description(BIS_PLATFORM_POST_PLUGIN_DESCRIPTION);
            $pstPluginVO->set_css_class(BIS_PLATFORM_POST_CSS_CLASS);
            $pstPluginVO->set_path(BIS_PLATFORM_POST_PLUGIN_PATH);
            $pstPluginVO->set_abs_path(BIS_PLATFORM_POST_PLUGIN_ABSPATH);
            $pstPluginVO->set_status(-1);
            array_push($addon_list, $pstPluginVO);

            // PRODUCT
            $productPluginVO = new PluginVO();
            $productPluginVO->set_id(BIS_PLATFORM_PRODUCT_PLUGIN_ID);
            $productPluginVO->set_apiKey(BIS_PLATFORM_PRODUCT_API_KEY);
            $productPluginVO->set_display_name(BIS_PLATFORM_PRODUCT_PLUGIN_DISPLAY_NAME);
            $productPluginVO->set_description(BIS_PLATFORM_PRODUCT_PLUGIN_DESCRIPTION);
            $productPluginVO->set_css_class(BIS_PLATFORM_PRODUCT_CSS_CLASS);
            $productPluginVO->set_path(BIS_PLATFORM_PRODUCT_PLUGIN_PATH);
            $productPluginVO->set_abs_path(BIS_PLATFORM_PRODUCT_PLUGIN_ABSPATH);
            $productPluginVO->set_status(-1);
            array_push($addon_list, $productPluginVO);

            // CATEGORY
            $categoryPluginVO = new PluginVO();
            $categoryPluginVO->set_id(BIS_PLATFORM_CATEGORY_PLUGIN_ID);
            $categoryPluginVO->set_apiKey(BIS_PLATFORM_CATEGORY_API_KEY);
            $categoryPluginVO->set_display_name(BIS_PLATFORM_CATEGORY_PLUGIN_DISPLAY_NAME);
            $categoryPluginVO->set_description(BIS_PLATFORM_CATEGORY_PLUGIN_DESCRIPTION);
            $categoryPluginVO->set_css_class(BIS_PLATFORM_CATEGORY_CSS_CLASS);
            $categoryPluginVO->set_path(BIS_PLATFORM_CATEGORY_PLUGIN_PATH);
            $categoryPluginVO->set_abs_path(BIS_PLATFORM_CATEGORY_PLUGIN_ABSPATH);
            $categoryPluginVO->set_status(-1);
            array_push($addon_list, $categoryPluginVO);

            // WIDGET
            $wPluginVO = new PluginVO();
            $wPluginVO->set_id(BIS_PLATFORM_WIDGET_PLUGIN_ID);
            $wPluginVO->set_apiKey(BIS_PLATFORM_WIDGET_API_KEY);
            $wPluginVO->set_display_name(BIS_PLATFORM_WIDGET_PLUGIN_DISPLAY_NAME);
            $wPluginVO->set_description(BIS_PLATFORM_WIDGET_PLUGIN_DESCRIPTION);
            $wPluginVO->set_css_class(BIS_PLATFORM_WIDGET_CSS_CLASS);
            $wPluginVO->set_path(BIS_PLATFORM_WIDGET_PLUGIN_PATH);
            $wPluginVO->set_abs_path(BIS_PLATFORM_WIDGET_PLUGIN_ABSPATH);
            $wPluginVO->set_status(-1);
            array_push($addon_list, $wPluginVO);

            // THEME
            $themePluginVO = new PluginVO();
            $themePluginVO->set_id(BIS_PLATFORM_THEME_PLUGIN_ID);
            $themePluginVO->set_apiKey(BIS_PLATFORM_THEME_API_KEY);
            $themePluginVO->set_display_name(BIS_PLATFORM_THEME_PLUGIN_DISPLAY_NAME);
            $themePluginVO->set_description(BIS_PLATFORM_THEME_PLUGIN_DESCRIPTION);
            $themePluginVO->set_css_class(BIS_PLATFORM_THEME_CSS_CLASS);
            $themePluginVO->set_path(BIS_PLATFORM_THEME_PLUGIN_PATH);
            $themePluginVO->set_abs_path(BIS_PLATFORM_THEME_PLUGIN_ABSPATH);
            $themePluginVO->set_status(-1);
            array_push($addon_list, $themePluginVO);

            // LANGUAGE
            $langPluginVO = new PluginVO();
            $langPluginVO->set_id(BIS_PLATFORM_LANGUAGE_PLUGIN_ID);
            $langPluginVO->set_apiKey(BIS_PLATFORM_LANGUAGE_API_KEY);
            $langPluginVO->set_display_name(BIS_PLATFORM_LANGUAGE_PLUGIN_DISPLAY_NAME);
            $langPluginVO->set_description(BIS_PLATFORM_LANGUAGE_PLUGIN_DESCRIPTION);
            $langPluginVO->set_css_class(BIS_PLATFORM_LANGUAGE_CSS_CLASS);
            $langPluginVO->set_path(BIS_PLATFORM_LANGUAGE_PLUGIN_PATH);
            $langPluginVO->set_abs_path(BIS_PLATFORM_LANGUAGE_PLUGIN_ABSPATH);
            $langPluginVO->set_status(-1);
            array_push($addon_list, $langPluginVO);

            // REDIRECT
            $redirectPluginVO = new PluginVO();
            $redirectPluginVO->set_id(BIS_PLATFORM_REDIRECT_PLUGIN_ID);
            $redirectPluginVO->set_apiKey(BIS_PLATFORM_REDIRECT_API_KEY);
            $redirectPluginVO->set_display_name(BIS_PLATFORM_REDIRECT_PLUGIN_DISPLAY_NAME);
            $redirectPluginVO->set_description(BIS_PLATFORM_REDIRECT_PLUGIN_DESCRIPTION);
            $redirectPluginVO->set_css_class(BIS_PLATFORM_REDIRECT_CSS_CLASS);
            $redirectPluginVO->set_path(BIS_PLATFORM_REDIRECT_PLUGIN_PATH);
            $redirectPluginVO->set_abs_path(BIS_PLATFORM_REDIRECT_PLUGIN_ABSPATH);
            $redirectPluginVO->set_status(-1);
            array_push($addon_list, $redirectPluginVO);

            // GOOGLE ANALYTICS
            $gooleAnalyticsPluginVO = new PluginVO();
            $gooleAnalyticsPluginVO->set_id(BIS_PLATFORM_GOOGLE_ANALYTICS_PLUGIN_ID);
            $gooleAnalyticsPluginVO->set_apiKey(BIS_PLATFORM_GOOGLE_ANALYTICS_API_KEY);
            $gooleAnalyticsPluginVO->set_display_name(BIS_PLATFORM_GOOGLE_ANALYTICS_PLUGIN_DISPLAY_NAME);
            $gooleAnalyticsPluginVO->set_description(BIS_PLATFORM_GOOGLE_ANALYTICS_PLUGIN_DESCRIPTION);
            $gooleAnalyticsPluginVO->set_css_class(BIS_PLATFORM_GOOGLE_ANALYTICS_CSS_CLASS);
            $gooleAnalyticsPluginVO->set_path(BIS_PLATFORM_GOOGLE_ANALYTICS_PLUGIN_PATH);
            $gooleAnalyticsPluginVO->set_abs_path(BIS_PLATFORM_GOOGLE_ANALYTICS_PLUGIN_ABSPATH);
            $gooleAnalyticsPluginVO->set_status(-1);
            array_push($addon_list, $gooleAnalyticsPluginVO);

            // Delete the plugin details.
            RulesEngineUtil::delete_option(BIS_RULESENGINE_ADDONS_CONST, is_network_admin());

            // Add them after delete.
            RulesEngineUtil::add_option(BIS_RULESENGINE_ADDONS_CONST, json_encode($addon_list), is_network_admin());
        }

        /**
         * Create specific tables for specific sites when plugin installed.
         */
        function bis_create_site_specific_tables() {

            global $wpdb;

            $table_prefix = $wpdb->prefix;

            $charset_collate = $wpdb->get_charset_collate();

            /* Table structure for table `bis_re_logical_rule_value` */
            $bis_re_logical_rules = "CREATE TABLE " . $table_prefix . "bis_re_logical_rules (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `name` varchar(50) NOT NULL,
                `description` varchar(500) DEFAULT NULL,
                `action_hook` varchar(50) DEFAULT NULL,
                `status` tinyint(1) NOT NULL DEFAULT '1',
                `site_id` int(11) NOT NULL DEFAULT '1',
                `general_col1` varchar(100) DEFAULT NULL,
                `general_col2` varchar(100) DEFAULT NULL,
                `eval_type` int(3) DEFAULT '1',
                `add_rule_type` int(1) DEFAULT '2',
                `rule_query` varchar(20000) DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `name` (`name`),
                KEY " . $table_prefix . "bis_status (`status`),
                KEY " . $table_prefix . "site_id (`site_id`)
            ) ENGINE = InnoDB " . $charset_collate . ";";

            dbDelta($bis_re_logical_rules);

            /* Table structure for table `bis_re_logical_rules_criteria` */
            //  operator_id '0=None; 1=And; 2=Or
            // eval_type '1=Session;2=Request;3=Context           
            $bis_re_logical_rules_criteria = "CREATE TABLE " . $table_prefix . "bis_re_logical_rules_criteria (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `option_id` int(11) NOT NULL,
                `sub_option_id` int(11) NOT NULL,
                `condition_id` int(11) NOT NULL,
                `value` varchar(20000) NOT NULL,
                `logical_rule_id` int(11) unsigned NOT NULL,
                `operator_id` int(5) NOT NULL DEFAULT '0',
                `left_bracket` tinyint(1) NOT NULL DEFAULT '0',
                `right_bracket` tinyint(1) NOT NULL DEFAULT '0',
                `eval_type` tinyint(1) NOT NULL DEFAULT '1',
                `general_col1` varchar(500) DEFAULT NULL,
                `general_col2` varchar(5000) DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY " . $table_prefix . "Logical_Rule_Id (`logical_rule_id`),
                KEY " . $table_prefix . "sub_option_id (`sub_option_id`),
                KEY " . $table_prefix . "condition_id (`condition_id`),
                KEY " . $table_prefix . "option_id (`option_id`),
                CONSTRAINT " . $table_prefix . "bis_logical_rule FOREIGN KEY (`logical_rule_id`) REFERENCES " . $table_prefix . "bis_re_logical_rules (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE = InnoDB " . $charset_collate . ";";

            dbDelta($bis_re_logical_rules_criteria);

            /* Table structure for table `bis_re_rule_details` */
            // rule_type_id  '1=Page Rule; 2=Redirect Rule; 3=Post Rule; 4=Widget Rule; 5=Theme Rule'
            $bis_re_rule_details = "CREATE TABLE " . $table_prefix . "bis_re_rule_details (
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `name` varchar(50) NOT NULL,
                `description` varchar(500) DEFAULT NULL,
                `action` varchar(250) DEFAULT NULL,
                `action_hook` varchar(100) DEFAULT NULL,
                `rule_type_id` int(11) NOT NULL,
                `status` tinyint(1) NOT NULL DEFAULT '1',
                `logical_rule_id` int(11) NOT NULL,
                `parent_type_value` varchar(50) DEFAULT NULL,
                `priority` int(5) NOT NULL DEFAULT '1',
                `child_sub_rule` int(5) DEFAULT NULL,
                `general_col1` varchar(5000) DEFAULT NULL,
                `general_col2` varchar(2500) DEFAULT NULL,
                `general_col3` varchar(1000) DEFAULT NULL,
                `general_col4` varchar(100) DEFAULT NULL,
                `general_col5` varchar(1000) DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `unique_rule` (`name`, `rule_type_id`),
                KEY " . $table_prefix . "logical_rule (`logical_rule_id`),
                KEY " . $table_prefix . "status (`status`),
                KEY " . $table_prefix . "rule_type_id (`rule_type_id`)
            ) ENGINE = InnoDB " . $charset_collate . ";";

            dbDelta($bis_re_rule_details);

            /* Table structure for table `bis_re_rules` */
            // parent_type_id  '1=Page or Post; 2=widget; 3=sidebar; 5=Theme'
            $bis_re_rules = "CREATE TABLE " . $table_prefix . "bis_re_rules (
                `parent_id` varchar(100) DEFAULT NULL,
                `rule_details_id` int(11) unsigned NOT NULL,
                `parent_type_id` int(11) NOT NULL DEFAULT '1',
                `general_col1` varchar(100) DEFAULT NULL,
                `general_col2` varchar(100) DEFAULT NULL,
                `general_col3` varchar(100) DEFAULT NULL,
                `general_col4` varchar(100) DEFAULT NULL,
                KEY " . $table_prefix . "rule_detail_id (`rule_details_id`),
                KEY " . $table_prefix . "parent_type_id (`parent_type_id`),
                CONSTRAINT " . $table_prefix . "rule_detail_constraint FOREIGN KEY (`rule_details_id`) REFERENCES " . $table_prefix . "bis_re_rule_details (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE = InnoDB " . $charset_collate . ";";


            dbDelta($bis_re_rules);
        }

        function bis_create_demo_site_specific_tables() {

            global $wpdb;

            $table_prefix = $wpdb->prefix;

            $charset_collate = $wpdb->get_charset_collate();

            /* Table structure for table `bis_re_logical_rule_value` */
            $bis_re_logical_rules = "CREATE TABLE " . $table_prefix . "bis_re_logical_rules (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `name` varchar(50) NOT NULL,
                `description` varchar(500) DEFAULT NULL,
                `action_hook` varchar(50) DEFAULT NULL,
                `status` tinyint(1) NOT NULL DEFAULT '1',
                `site_id` int(11) NOT NULL DEFAULT '1',
                `general_col1` varchar(100) DEFAULT NULL,
                `general_col2` varchar(100) DEFAULT NULL,
                `eval_type` int(3) DEFAULT '1',
                `add_rule_type` int(1) DEFAULT '2',
                `rule_query` varchar(20000) DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `name` (`name`),
                KEY " . $table_prefix . "bis_status (`status`),
                KEY " . $table_prefix . "site_id (`site_id`)
            ) ENGINE = InnoDB " . $charset_collate . ";";

            dbDelta($bis_re_logical_rules);

            /* Table structure for table `bis_re_logical_rules_criteria` */
            //  operator_id '0=None; 1=And; 2=Or
            // eval_type '1=Session;2=Request;3=Context           
            $bis_re_logical_rules_criteria = "CREATE TABLE " . $table_prefix . "bis_re_logical_rules_criteria (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `option_id` int(11) NOT NULL,
                `sub_option_id` int(11) NOT NULL,
                `condition_id` int(11) NOT NULL,
                `value` varchar(20000) NOT NULL,
                `logical_rule_id` int(11) unsigned NOT NULL,
                `operator_id` int(5) NOT NULL DEFAULT '0',
                `left_bracket` tinyint(1) NOT NULL DEFAULT '0',
                `right_bracket` tinyint(1) NOT NULL DEFAULT '0',
                `eval_type` tinyint(1) NOT NULL DEFAULT '1',
                `general_col1` varchar(500) DEFAULT NULL,
                `general_col2` varchar(5000) DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY " . $table_prefix . "Logical_Rule_Id (`logical_rule_id`),
                KEY " . $table_prefix . "sub_option_id (`sub_option_id`),
                KEY " . $table_prefix . "condition_id (`condition_id`),
                KEY " . $table_prefix . "option_id (`option_id`)
            ) ENGINE = InnoDB " . $charset_collate . ";";

            dbDelta($bis_re_logical_rules_criteria);

            /* Table structure for table `bis_re_rule_details` */
            // rule_type_id  '1=Page Rule; 2=Redirect Rule; 3=Post Rule; 4=Widget Rule; 5=Theme Rule'
            $bis_re_rule_details = "CREATE TABLE " . $table_prefix . "bis_re_rule_details (
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `name` varchar(50) NOT NULL,
                `description` varchar(500) DEFAULT NULL,
                `action` varchar(250) DEFAULT NULL,
                `action_hook` varchar(100) DEFAULT NULL,
                `rule_type_id` int(11) NOT NULL,
                `status` tinyint(1) NOT NULL DEFAULT '1',
                `logical_rule_id` int(11) NOT NULL,
                `parent_type_value` varchar(50) DEFAULT NULL,
                `priority` int(5) NOT NULL DEFAULT '1',
                `child_sub_rule` int(5) DEFAULT NULL,
                `general_col1` varchar(5000) DEFAULT NULL,
                `general_col2` varchar(2500) DEFAULT NULL,
                `general_col3` varchar(1000) DEFAULT NULL,
                `general_col4` varchar(100) DEFAULT NULL,
                `general_col5` varchar(100) DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `unique_rule` (`name`, `rule_type_id`),
                KEY " . $table_prefix . "logical_rule (`logical_rule_id`),
                KEY " . $table_prefix . "status (`status`),
                KEY " . $table_prefix . "rule_type_id (`rule_type_id`)
            ) ENGINE = InnoDB " . $charset_collate . ";";

            dbDelta($bis_re_rule_details);

            /* Table structure for table `bis_re_rules` */
            // parent_type_id  '1=Page or Post; 2=widget; 3=sidebar; 5=Theme'
            $bis_re_rules = "CREATE TABLE " . $table_prefix . "bis_re_rules (
                `parent_id` varchar(100) DEFAULT NULL,
                `rule_details_id` int(11) unsigned NOT NULL,
                `parent_type_id` int(11) NOT NULL DEFAULT '1',
                `general_col1` varchar(100) DEFAULT NULL,
                `general_col2` varchar(100) DEFAULT NULL,
                `general_col3` varchar(100) DEFAULT NULL,
                `general_col4` varchar(100) DEFAULT NULL,
                KEY " . $table_prefix . "rule_detail_id (`rule_details_id`),
                KEY " . $table_prefix . "parent_type_id (`parent_type_id`)
            ) ENGINE = InnoDB " . $charset_collate . ";";


            dbDelta($bis_re_rules);
        }

        /**
         * Creating tables when a new site is created.
         * @param type $blog_id
         * @param type $user_id
         * @param type $domain
         * @param type $path
         * @param type $site_id
         * @param type $meta
         */
        function bis_on_site_create($blog_id, $user_id, $domain, $path, $site_id, $meta) {
            $current_site = get_current_site()->domain;
            if (!(\RulesEngineUtil::isContains($current_site, BIS_DEMO_SITE))) {
                switch_to_blog($blog_id);
                $this->bis_create_site_specific_tables();
                $this->bis_create_site_analytics_tables();
                restore_current_blog();
            } else {
                switch_to_blog($blog_id);
                $this->bis_create_demo_site_specific_tables();
                $this->bis_create_demo_site_analytics_tables();
                restore_current_blog();
            }
        }

        /**
         * Deleting tables when a site is deleted.
         * @global type $wpdb
         * @param type $tables
         * @return string
         */
        function bis_on_site_delete($tables) {
            global $wpdb;
            $current_site = get_current_site()->domain;
            if (!(\RulesEngineUtil::isContains($current_site, BIS_DEMO_SITE))) {
                $tables[] = $wpdb->prefix . 'bis_re_logical_rules_criteria';
                $tables[] = $wpdb->prefix . 'bis_re_logical_rules';
                $tables[] = $wpdb->prefix . 'bis_re_rules';
                $tables[] = $wpdb->prefix . 'bis_re_rule_details';
                $tables[] = $wpdb->prefix . 'bis_re_report_auth';
                $tables[] = $wpdb->prefix . 'bis_re_report_data';
                $tables[] = $wpdb->prefix . 'bis_re_report';
            } else {
                $tables[] = $wpdb->prefix . 'bis_re_sub_option_condition';
                $tables[] = $wpdb->prefix . 'bis_re_sub_option';
                $tables[] = $wpdb->prefix . 'bis_re_option';
                $tables[] = $wpdb->prefix . 'bis_re_rules';
                $tables[] = $wpdb->prefix . 'bis_re_rule_details';
                $tables[] = $wpdb->prefix . 'bis_re_condition';
                $tables[] = $wpdb->prefix . 'bis_re_logical_rule_value';
                $tables[] = $wpdb->prefix . 'bis_re_logical_rules_criteria';
                $tables[] = $wpdb->prefix . 'bis_re_logical_rules';
                $tables[] = $wpdb->prefix . 'bis_re_report_auth';
                $tables[] = $wpdb->prefix . 'bis_re_report_data';
                $tables[] = $wpdb->prefix . 'bis_re_report';
                $tables[] = $wpdb->prefix . 'cf_forms';
                $tables[] = $wpdb->prefix . 'cf_form_entries';
                $tables[] = $wpdb->prefix . 'cf_form_entry_meta';
                $tables[] = $wpdb->prefix . 'cf_form_entry_values';
                $tables[] = $wpdb->prefix . 'cf_tracking';
                $tables[] = $wpdb->prefix . 'cf_tracking_meta';
                $tables[] = $wpdb->prefix . 'woocommerce_log';
            }

            RulesEngineCacheWrapper::delete_all_transient_cache();
            return $tables;
        }

    }
}