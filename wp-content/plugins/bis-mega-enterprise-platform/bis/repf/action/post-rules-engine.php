<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction. This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

namespace bis\repf\action;

use bis\repf\common\RulesEngineCacheWrapper;
use bis\repf\model\PostRulesEngineModel;
use bis\repf\action\BaseRulesEngine;
use RulesEngineCommon;
use RulesDynamicContent;
use RulesEngineUtil;

/**
 * Class PostRulesEngine
 */
class PostRulesEngine extends BaseRulesEngine {

    public function bis_evaluate_request_rules() {


        $applied_rules = $this->get_request_rules();

        if ($applied_rules != null && !empty($applied_rules)) {

            $current_page_id = $this->get_the_ID();
            $current_path = RulesEngineUtil::get_current_url();

            if ($current_page_id == NULL) {
                $current_page_id = url_to_postid($current_path[0]);
            }

            if ($current_page_id == 0) {
                $home_page_url = get_home_url();

                if (!RulesEngineUtil::endsWith($home_page_url, "/")) {
                    $home_page_url = $home_page_url . "/";
                }

                if (is_array($current_path) && in_array($home_page_url, $current_path)) {
                    $current_page_id = get_option('page_on_front');
                }
            }

            $categoryId = null;
            $referer_path = wp_get_referer();
            $current_page_url = RulesEngineUtil::get_current_page_url();
            global $wp_query;

            $cacheVO = new \bis\repf\vo\CacheVO($current_page_url, $current_page_id, $categoryId, $referer_path);
            $rulesEngine = new RulesEngine();
            $rulesEngine->bis_evaluate_request_rules($wp_query, $cacheVO);
        } // End of If 
    }

    public function apply_session_post_rules() {
        $this->get_applied_session_logical_rules();
    }

    /**
     * This method is used to exclude posts.
     *
     * @param $query
     * @return mixed
     */
    public function bis_re_exclude_posts($query) {
        
        if (!$query->is_main_query() || is_admin()) {
            return;
        }
        RulesEngineUtil::update_rule_cache_id(BIS_LOGICAL_CACHE_REFRESH_ID);
        $this->apply_session_post_rules();
        $this->bis_evaluate_request_rules();
        $excluded_posts_id_array = $this->bis_re_get_excluded_post_ids();
        $applied_request_rules = $this->get_request_rules();
        $merged_excluded_posts = null;
        $applied_request_post_rules = null;

        if ($applied_request_rules != null && !empty($applied_request_rules)) {
            $post_rule_modal = new PostRulesEngineModel ();
            $applied_request_post_rules = $post_rule_modal->get_applied_post_rules($applied_request_rules);
        }

        if (!empty($excluded_posts_id_array)) {
            $merged_excluded_posts = $excluded_posts_id_array;
        }

        if ($applied_request_post_rules != null && !empty($applied_request_post_rules)) {
            $excluded_posts_id_req_array = RulesEngineUtil::get_applied_post_rule_ids($applied_request_post_rules);

            if ($merged_excluded_posts == null) {
                $merged_excluded_posts = $excluded_posts_id_req_array;
            } else {
                $merged_excluded_posts = array_merge($merged_excluded_posts, $excluded_posts_id_req_array);
            }
        }
        
        // Add to session if only post rules exists
        if (!empty($merged_excluded_posts)) {
            $post_array = $this->bis_get_post_show_matches($merged_excluded_posts);
            
            if (!empty($post_array['product_cats'])) {
                $taxquery[] = array(
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'id',
                        'terms' => $post_array['product_cats'],
                        'operator' => 'NOT IN'
                ));
            }

            if (!empty($post_array['product_tags'])) {
                $taxquery[] = array(
                    array(
                        'taxonomy' => 'product_tag',
                        'field' => 'slug',
                        'terms' => $post_array['product_tags'],
                        'operator' => 'NOT IN'
                    )
                );
            }

            if (!empty($post_array['show_only_cat'])) {
                $taxquery[] = array(
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'id',
                        'terms' => $post_array['show_only_cat'],
                        'operator' => 'IN'
                    )
                );
            }

            if (!empty($post_array['show_only_tags'])) {
                $taxquery[] = array(
                    array(
                        'taxonomy' => 'product_tag',
                        'field' => 'slug',
                        'terms' => $post_array['show_only_tags'],
                        'operator' => 'IN'
                    )
                );
            }

            if (!empty($post_array['product_atts'])) {
                $atts = $post_array['product_atts'];
                foreach ($atts as $key => $att) {
                    $taxquery[] = array(
                        'taxonomy' => 'pa_' . $key,
                        'field' => 'slug',
                        'terms' => $att,
                        'operator' => 'NOT IN'
                    );
                }
            }

            if (!empty($post_array['show_only_atts'])) {
                $atts = $post_array['show_only_atts'];
                foreach ($atts as $key => $att) {
                    $taxquery[] = array(
                        'taxonomy' => 'pa_' . $key,
                        'field' => 'slug',
                        'terms' => $att,
                        'operator' => 'IN'
                    );
                }
            }

            $hide_all = $post_array['hide_all'];
            if (!empty($hide_all)) {
                set_query_var('post__in', $hide_all);
            }

            $show_only_products = $post_array['show_only_product'];
            if (!empty($show_only_products)) {
                set_query_var('post__in', $show_only_products);
            }
            
            if (!empty($post_array['post_cats'])) {
                $taxquery[] = array(
                    array(
                        'taxonomy' => 'category',
                        'field' => 'id',
                        'terms' => $post_array['post_cats'],
                        'operator' => 'NOT IN'
                ));
            }

            if (!empty($post_array['post_tags'])) {
                $taxquery[] = array(
                    array(
                        'taxonomy' => 'post_tag',
                        'field' => 'slug',
                        'terms' => $post_array['post_tags'],
                        'operator' => 'NOT IN'
                    )
                );
            }
            
            
            $existing_taxquery = $query->get('tax_query');

            if (!empty($existing_taxquery) && isset($taxquery)) {
                array_push($taxquery, $existing_taxquery);
            }
            
            if (isset($taxquery)) {
                $query->set('tax_query', $taxquery);
            }

            $post_ids = $post_array['posts'];
            
            if(!empty($post_ids)) {
                if (is_single()) {
                    $query->set('post__not_in', $post_ids);
                    return;
                }
                $query->set('post__not_in', $post_ids);
            }
            
            RulesEngineCacheWrapper::set_session_attribute(BIS_PROD_CAT_WIDGET_EXCLUDE, $post_array['product_cats']);
            RulesEngineCacheWrapper::set_session_attribute(BIS_PROD_TAG_WIDGET_EXCLUDE, $post_array['product_tags']);
            
            RulesEngineCacheWrapper::set_session_attribute(BIS_POST_CAT_WIDGET_EXCLUDE, $post_array['post_cats']);
            RulesEngineCacheWrapper::set_session_attribute(BIS_POST_TAG_WIDGET_EXCLUDE, $post_array['post_tags']);
        } else {
            $post_model = new PostRulesEngineModel();
            $rows_post = $post_model->get_post_or_product_show_rules(BIS_POST_TYPE_RULE);
            $rows = $post_model->get_post_or_product_show_rules(BIS_WOO_PRODUCT_TYPE_RULE);
            if(!empty($rows_post)){
                $rows = array_merge($rows_post, $rows);
            }
            if (!empty($rows)) {
                $post_array = $this->bis_get_post_show_matches($rows, true);
            } else {
                return;
            }

            $taxquery = array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'id',
                    'terms' => $post_array['product_cats'],
                    'operator' => 'NOT IN',
                ),
                array(
                    'taxonomy' => 'product_tag',
                    'field' => 'slug',
                    'terms' => $post_array['product_tags'],
                    'operator' => 'NOT IN'
                )
            );

            if (!empty($post_array['product_atts'])) {
                $atts = $post_array['product_atts'];
                foreach ($atts as $key => $att) {
                    $taxquery[] = array(
                        'taxonomy' => 'pa_' . $key,
                        'field' => 'slug',
                        'terms' => $att,
                        'operator' => 'NOT IN'
                    );
                }
            }
            
            $taxquery = array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'category',
                    'field' => 'id',
                    'terms' => $post_array['post_cats'],
                    'operator' => 'NOT IN',
                ),
                array(
                    'taxonomy' => 'post_tag',
                    'field' => 'slug',
                    'terms' => $post_array['post_tags'],
                    'operator' => 'NOT IN'
                )
            );
            
            if (!empty($post_array['product_atts'])) {
                $atts = $post_array['product_atts'];
                foreach ($atts as $key => $att) {
                    $taxquery[] = array(
                        'taxonomy' => 'pa_' . $key,
                        'field' => 'slug',
                        'terms' => $att,
                        'operator' => 'NOT IN'
                    );
                }
            }
            
            // Append with existing taxquery.
            $existing_taxquery = $query->get('tax_query');

            if (!empty($existing_taxquery)) {
                array_push($taxquery, $existing_taxquery);
            }

            $query->set('tax_query', $taxquery);

            $post_ids = $post_array['posts'];
            
            if(!empty($post_ids)) {
                if (is_single()) {
                    $query->set('post__not_in', $post_ids);
                    return;
                }

                $query->set('post__not_in', $post_ids);
            }
            
            RulesEngineCacheWrapper::set_session_attribute(BIS_PROD_CAT_WIDGET_EXCLUDE, $post_array['product_cats']);
            RulesEngineCacheWrapper::set_session_attribute(BIS_PROD_TAG_WIDGET_EXCLUDE, $post_array['product_tags']);
            
            RulesEngineCacheWrapper::set_session_attribute(BIS_POST_CAT_WIDGET_EXCLUDE, $post_array['post_cats']);
            RulesEngineCacheWrapper::set_session_attribute(BIS_POST_TAG_WIDGET_EXCLUDE, $post_array['post_tags']);
        }
               
        return $query;
    }
    
    public function bis_get_post_show_matches($rules = null, $fail = false) {
        
        $show_posts = array();
        $hide_posts = array();
        $show_post_cats = array();
        $hide_post_cats = array();
        $show_post_tags = array();
        $hide_post_tags = array();
        $show_product_atts = array();
        $hide_product_atts = array();
        
        $show_product_cats = array();
        $show_product_tags = array();
        $hide_product_cats = array();
        $hide_product_tags = array();
        
        $show_only_post = array();
        $show_only_cat = array();
        $show_only_tags = array();
        $show_only_atts = array();
        
        $posts = array();
        $post_cats = array();
        $post_tags = array();
        $product_atts = array();
        
        $hide_all = array();

        if (!empty($rules) && $fail == false) {
            foreach ($rules as $rule) {
                if ('show_post' === $rule->action) {
                    
                    switch ($rule->parent_type_value) {
                        case 'product':
                        case 'grouped_product':
                        case 'variable_product':
                        case 'external_product':
                            $show_posts[] = $rule->parent_id;
                            break;
                        case 'categories':
                            $show_product_cats[] = $rule->parent_id;
                            $show_cat_prod = $this->get_post_by_taxnomoy($rule->parent_id, "woo_cat");
                            $show_posts = array_merge($show_posts, $show_cat_prod);
                            break;
                        case 'woo-tags':
                            $show_product_tags[] = $rule->parent_id;
                            $show_cat_prod = $this->get_post_by_taxnomoy($rule->parent_id, "woo_tags");
                            $show_posts = array_merge($show_posts, $show_cat_prod);
                            break;
                        case 'attributes':
                            $taxonomy = json_decode($rule->gencol4);
                            $show_product_atts[$taxonomy[0]] = array($rule->parent_id);
                            $show_attr_prod = $this->get_post_by_taxnomoy($show_product_atts, "woo_attr");
                            $show_posts = array_merge($show_posts, $show_attr_prod);
                            break;
                        case 'wp-categories':
                            $show_post_cats[] = $rule->parent_id;
                            $show_cat_post = $this->get_post_by_taxnomoy($rule->parent_id, "wp_cat");
                            $show_posts = array_merge($show_posts, $show_cat_post);
                            break;
                        case 'wp-tags':
                            $show_post_tags[] = $rule->parent_id;
                            $show_tag_post = $this->get_post_by_taxnomoy($rule->parent_id, "wp_tag");
                            $show_posts = array_merge($show_posts, $show_tag_post);
                            break;
                        case 'custom_taxo':
                            $taxonomy = json_decode($rule->gencol4);
                            $show_product_atts[$taxonomy[0]] = array($rule->parent_id);
                            $show_attr_post = $this->get_post_by_taxnomoy($show_product_atts, "attr");
                            $show_posts = array_merge($show_posts, $show_attr_post);
                            
                            if (( $key = array_search($taxonomy[0], $product_atts) ) !== false) {
                                $exists = $product_atts[$taxonomy[0]];
                                if (!empty($exists)) {
                                    unset($exists[$rule->parent_id]);
                                    $product_atts[$taxonomy[0]] = $exists;
                                } else {
                                    unset($product_atts[$key]);
                                }
                            }
                            
                            break;
                        default:
                            $show_posts[] = $rule->parent_id;
                            break;
                    }
                } elseif ('hide_post' === $rule->action) {
                    
                    switch ($rule->parent_type_value) {
                        case 'product':
                        case 'grouped_product':
                        case 'variable_product':
                        case 'external_product':
                            if ($rule->parent_id == "bis_prod_all") {
                                $hide_all[] = $rule->parent_id;
                            }
                            $hide_posts[] = $rule->parent_id;
                            break;
                        case 'categories':
                            $hide_product_cats[] = $rule->parent_id;
                            $hide_cat_prod = $this->get_post_by_taxnomoy($rule->parent_id, "woo_cat");
                            $hide_posts = array_merge($hide_posts, $hide_cat_prod);
                            break;
                        case 'woo-tags':
                            $hide_product_tags[] = $rule->parent_id;
                            $hide_cat_prod = $this->get_post_by_taxnomoy($rule->parent_id, "woo_tags");
                            $hide_posts = array_merge($hide_posts, $hide_cat_prod);
                            break;
                        case 'attributes':
                            $taxonomy = json_decode($rule->gencol4);
                            $hide_product_atts[$taxonomy[0]] = array($rule->parent_id);
                            $hide_attr_prod = $this->get_post_by_taxnomoy($hide_product_atts, "woo_attr");
                            $hide_posts = array_merge($hide_posts, $hide_attr_prod);
                            break;
                        case 'wp-categories':
                            $hide_post_cats[] = $rule->parent_id;
                            $hide_cat_post = $this->get_post_by_taxnomoy($rule->parent_id, "wp_cat");
                            $hide_posts = array_merge($hide_posts, $hide_cat_post);
                            break;
                        case 'wp-tags':
                            $hide_post_tags[] = $rule->parent_id;
                            $hide_tag_post = $this->get_post_by_taxnomoy($rule->parent_id, "wp_tag");
                            $hide_posts = array_merge($hide_posts, $hide_tag_post);
                            break;
                        case 'custom_taxo':
                            $taxonomy = json_decode($rule->gencol4);
                            $hide_product_atts[$taxonomy[0]] = array($rule->parent_id);
                            $hide_attr_post = $this->get_post_by_taxnomoy($hide_product_atts, "attr");
                            $hide_posts = array_merge($hide_posts, $hide_attr_post);
                            
                            if (array_key_exists($taxonomy[0], $product_atts)) {
                                $exists = $product_atts[$taxonomy[0]];
                                $exists[] = $rule->parent_id;
                                $product_atts[$taxonomy[0]] = $exists;
                            } else {
                                $product_atts[$taxonomy[0]] = array($rule->parent_id);
                            }
                            break;
                        default:
                            $hide_posts[] = $rule->parent_id;
                            break;
                    }
                } else if ('show_only_post' === $rule->action) {
                    if ('product' === $rule->parent_type_value || 'grouped_product' === $rule->parent_type_value || 'variable_product' === $rule->parent_type_value || 'external_product' === $rule->parent_type_value) {
                        $show_only_post[] = $rule->parent_id;
                    } else if ('categories' === $rule->parent_type_value) {
                        $show_only_cat[] = $rule->parent_id;
                    } else if ('woo-tags' === $rule->parent_type_value) {
                        $show_only_tags[] = $rule->parent_id;
                    } else if ('attributes' === $rule->parent_type_value || "custom_taxo" === $rule->parent_type_value) {
                        $taxonomy = json_decode($rule->gencol4);
                        if (array_key_exists($taxonomy[0], $show_only_atts)) {
                            $exists_atts = $show_only_atts[$taxonomy[0]];
                            $exists_atts[] = $rule->parent_id;
                            $show_only_atts[$taxonomy[0]] = $exists_atts;
                        } else {
                            $show_only_atts[$taxonomy[0]] = array($rule->parent_id);
                        }
                    }
                }
            }
            
            $posts = array_diff($hide_posts, $show_posts);
            $post_cats = array_diff($hide_post_cats, $show_post_cats);
            $post_tags = array_diff($hide_post_tags, $show_post_tags);
            
            $product_cats = array_diff($hide_product_cats, $show_product_cats);
            $product_tags = array_diff($hide_product_tags, $show_product_tags);
        } else if (!empty($rules) && $fail == true) {
            foreach ($rules as $rule) {
                $valid = RulesEngineCommon::is_rule_valid($rule->name);
                if ('show_post' === $rule->action) {

                    switch ($rule->parent_type_value) {
                        case 'product':
                        case 'grouped_product':
                        case 'variable_product':
                        case 'external_product':
                            $show_posts[] = $rule->parent_id;
                            break;
                        case 'categories':
                            $show_product_cats[] = $rule->parent_id;
                            $show_cat_prod = $this->get_post_by_taxnomoy($rule->parent_id, "woo_cat");
                            $show_posts = array_merge($show_posts, $show_cat_prod);
                            break;
                        case 'woo-tags':
                            $show_product_tags[] = $rule->parent_id;
                            $show_cat_prod = $this->get_post_by_taxnomoy($rule->parent_id, "woo_tags");
                            $show_posts = array_merge($show_posts, $show_cat_prod);
                            break;
                        case 'attributes':
                            $taxonomy = json_decode($rule->gencol4);
                            $show_product_atts[$taxonomy[0]] = array($rule->parent_id);
                            $show_attr_prod = $this->get_post_by_taxnomoy($show_product_atts, "woo_attr");
                            $show_posts = array_merge($show_posts, $show_attr_prod);
                            break;
                        case 'wp-categories':
                            $show_post_cats[] = $rule->parent_id;
                            $show_cat_post = $this->get_post_by_taxnomoy($rule->parent_id, "wp_cat");
                            $show_posts = array_merge($show_posts, $show_cat_post);
                            break;
                        case 'wp-tags':
                            $show_post_tags[] = $rule->parent_id;
                            $show_tag_post = $this->get_post_by_taxnomoy($rule->parent_id, "wp_tag");
                            $show_posts = array_merge($show_posts, $show_tag_post);
                            break;
                        case 'custom_taxo':
                            $taxonomy = json_decode($rule->gencol4);
                            $show_product_atts[$taxonomy[0]] = array($rule->parent_id);
                            $show_attr_post = $this->get_post_by_taxnomoy($show_product_atts, "attr");
                            $show_posts = array_merge($show_posts, $show_attr_post);

                            if (( $key = array_search($taxonomy[0], $product_atts) ) !== false) {
                                $exists = $product_atts[$taxonomy[0]];
                                if (!empty($exists)) {
                                    unset($exists[$rule->parent_id]);
                                    $product_atts[$taxonomy[0]] = $exists;
                                } else {
                                    unset($product_atts[$key]);
                                }
                            }

                            break;
                        default:
                            $show_posts[] = $rule->parent_id;
                            break;
                    }
                } elseif ('hide_post' === $rule->action) {

                    switch ($rule->parent_type_value) {
                        case 'product':
                        case 'grouped_product':
                        case 'variable_product':
                        case 'external_product':
                            if ($rule->parent_id == "bis_prod_all") {
                                $hide_all[] = $rule->parent_id;
                            }
                            $hide_posts[] = $rule->parent_id;
                            break;
                        case 'categories':
                            $hide_product_cats[] = $rule->parent_id;
                            $hide_cat_prod = $this->get_post_by_taxnomoy($rule->parent_id, "woo_cat");
                            $hide_posts = array_merge($hide_posts, $hide_cat_prod);
                            break;
                        case 'woo-tags':
                            $hide_product_tags[] = $rule->parent_id;
                            $hide_cat_prod = $this->get_post_by_taxnomoy($rule->parent_id, "woo_tags");
                            $hide_posts = array_merge($hide_posts, $hide_cat_prod);
                            break;
                        case 'attributes':
                            $taxonomy = json_decode($rule->gencol4);
                            $hide_product_atts[$taxonomy[0]] = array($rule->parent_id);
                            $hide_attr_prod = $this->get_post_by_taxnomoy($hide_product_atts, "woo_attr");
                            $hide_posts = array_merge($hide_posts, $hide_attr_prod);
                            break;
                        case 'wp-categories':
                            $hide_post_cats[] = $rule->parent_id;
                            $hide_cat_post = $this->get_post_by_taxnomoy($rule->parent_id, "wp_cat");
                            $hide_posts = array_merge($hide_posts, $hide_cat_post);
                            break;
                        case 'wp-tags':
                            $hide_post_tags[] = $rule->parent_id;
                            $hide_tag_post = $this->get_post_by_taxnomoy($rule->parent_id, "wp_tag");
                            $hide_posts = array_merge($hide_posts, $hide_tag_post);
                            break;
                        case 'custom_taxo':
                            $taxonomy = json_decode($rule->gencol4);
                            $hide_product_atts[$taxonomy[0]] = array($rule->parent_id);
                            $hide_attr_post = $this->get_post_by_taxnomoy($hide_product_atts, "attr");
                            $hide_posts = array_merge($hide_posts, $hide_attr_post);

                            if (array_key_exists($taxonomy[0], $product_atts)) {
                                $exists = $product_atts[$taxonomy[0]];
                                $exists[] = $rule->parent_id;
                                $product_atts[$taxonomy[0]] = $exists;
                            } else {
                                $product_atts[$taxonomy[0]] = array($rule->parent_id);
                            }
                            break;
                        default:
                            $hide_posts[] = $rule->parent_id;
                            break;
                    }
                }
            }
            $posts = array_diff($hide_posts, $show_posts);
            $post_cats = array_diff($hide_post_cats, $show_post_cats);
            $post_tags = array_diff($hide_post_tags, $show_post_tags);

            $product_cats = array_diff($hide_product_cats, $show_product_cats);
            $product_tags = array_diff($hide_product_tags, $show_product_tags);
        }
        
        array_unique($posts);
        array_unique($post_cats);
        array_unique($post_tags);
        array_unique($product_cats);
        array_unique($product_tags);
        array_unique($product_atts);
        
        array_unique($show_only_post);
        array_unique($show_only_cat);
        array_unique($show_only_tags);
        array_unique($show_only_atts);
        
        return array(
            'posts' => $posts,
            'post_cats' => $post_cats,
            'post_tags' => $post_tags,
            'product_cats' => $product_cats,
            'product_tags' => $product_tags,
            'product_atts' => $product_atts,
            'show_only_product' => $show_only_post,
            'show_only_cat' => $show_only_cat,
            'show_only_tags' => $show_only_tags,
            'show_only_atts' => $show_only_atts,
            'hide_all' => $hide_all
        );
    }
    
    public function get_post_by_taxnomoy($post_cats, $type){
        $posts = array();
        switch ($type) {
            case "attr":
                if (!empty($post_cats)) {
                    global $wp_taxonomies;
                    foreach ($post_cats as $key => $post_cat) {
                        $post_ids = get_posts(array(
                            'numberposts' => -1,
                            'post_type' => $wp_taxonomies["portfolio_cat"]->object_type,
                            'tax_query' => array(
                                array(
                                    'taxonomy' => $key,
                                    'field' => 'slug',
                                    'terms' => $post_cat,
                                ),
                            ),
                            'fields' => 'ids',
                        ));
                        $posts = array_merge($posts, $post_ids);
                    }
                }
                break;
            case "wp_cat":
                $post_ids = get_posts(array(
                    'numberposts' => -1,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'category',
                            'field' => 'id',
                            'terms' => (int) $post_cats,
                        ),
                    ),
                    'fields' => 'ids',
                ));
                $posts = array_merge($posts, $post_ids);
                break;
            case "woo_cat":
                $post_ids = get_posts(array(
                    'numberposts' => -1,
                    'post_type' => 'product',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product_cat',
                            'field' => 'id',
                            'terms' => $post_cats,
                        ),
                    ),
                    'fields' => 'ids',
                ));
                $posts = array_merge($posts, $post_ids);
                break;
            case "woo_tags":
                if (!empty($post_cats)) {
                    $post_ids = get_posts(array(
                        'numberposts' => -1,
                        'post_type' => 'product',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'product_tag',
                                'field' => 'slug',
                                'terms' => $post_cats,
                            ),
                        ),
                        'fields' => 'ids',
                    ));
                    $posts = array_merge($posts, $post_ids);
                }
            break;
            case "woo_attr":
                if (!empty($post_cats)) {
                    foreach ($post_cats as $key => $post_cat) {
                        $post_ids = get_posts(array(
                            'numberposts' => -1,
                            'post_type' => 'product',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'pa_'.$key,
                                    'field' => 'slug',
                                    'terms' => $post_cat,
                                ),
                            ),
                            'fields' => 'ids',
                        ));
                        $posts = array_merge($posts, $post_ids);
                    }
                }
            break;
            case "wp_tag":
                $post_ids = get_posts(array(
                    'numberposts' => -1,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'post_tag',
                            'field' => 'slug',
                            'terms' => $post_cats,
                        ),
                    ),
                    'fields' => 'ids',
                ));
                $posts = array_merge($posts, $post_ids);
            break;
        }
        
        return $posts;
    }

    public function bis_exclude_post_cat_in_widget($cat_args) {
        $cat_exclude = RulesEngineCacheWrapper::get_session_attribute(BIS_POST_CAT_WIDGET_EXCLUDE);

        $cat_args['exclude'] = $cat_exclude;
        return $cat_args;
    }

    public function bis_exclude_post_tag_in_widget($tag_args) {
        $tag_exclude = RulesEngineCacheWrapper::get_session_attribute(BIS_POST_TAG_WIDGET_EXCLUDE);

        $tag_ids = array();
        if (!empty($tag_exclude)) {
            foreach ($tag_exclude as $tag) {
                $tag_ids[] = get_term_by('slug', $tag, 'post_tag')->term_id;
            }
        }

        $tag_args['exclude'] = $tag_ids;
        return $tag_args;
    }

    /**
     * This method is used to get the excluded post ids.
     * @return array
     */
    public function bis_re_get_excluded_post_ids() {

        $excluded_post_rules = $this->get_posts_rules();

        if ($excluded_post_rules == null) {
            $this->init_applied_post_rules();
            $excluded_post_rules = $this->get_posts_rules();
        }

        $excluded_posts_id_array = RulesEngineUtil::get_applied_post_rule_ids($excluded_post_rules);

        return $excluded_posts_id_array;
    }

    private function init_applied_post_rules() {

        // Check if excluded posts exists in session.
        $excluded_post_rules = $this->get_posts_rules();

        // If excluded post does not exist in session, then get from DB.
        if ($excluded_post_rules == null) {
            $excluded_post_rules = array();
            $applied_post_rules = array();
            $applied_rules = $this->get_applied_logical_rules();



            if ($applied_rules != null && !empty($applied_rules)) {
                // Call the rules to get the excluded posts.
                $post_rule_modal = new PostRulesEngineModel ();
                $applied_post_rules = $post_rule_modal->get_applied_post_rules($applied_rules);

                if ($applied_post_rules != null && !empty($applied_post_rules)) {
                    foreach ($applied_post_rules as $applied_post_rule) {
                        if ($applied_post_rule->action == "hide_post") {
                            array_push($excluded_post_rules, $applied_post_rule);
                        } elseif ($applied_post_rule->action == "show_post") {
                            array_push($excluded_post_rules, $applied_post_rule);
                        } elseif ($applied_post_rule->action == "shipping" || $applied_post_rule->action == "payment" ||
                            $applied_post_rule->action == "dependent_prod" || $applied_post_rule->action == "restrict_prod") {
                            array_push($excluded_post_rules, $applied_post_rule);
                        } elseif ($applied_post_rule->action == "show_only_post") {
                            array_push($excluded_post_rules, $applied_post_rule);
                        }
                    }
                }
            }

            RulesEngineCacheWrapper::set_session_attribute(BIS_EXCLUDE_POSTS, $excluded_post_rules);
            RulesEngineCacheWrapper::set_session_attribute(BIS_APPEND_TO_POSTS, $applied_post_rules);
        }
    }

    public function bis_re_get_appended_post_ids() {

        $appended_post_rules = RulesEngineCacheWrapper::get_session_attribute(BIS_APPEND_TO_POSTS);

        if ($appended_post_rules == null) {
            $this->init_applied_post_rules();
            $appended_post_rules = RulesEngineCacheWrapper::get_session_attribute(BIS_APPEND_TO_POSTS);
        }

        return $appended_post_rules;
    }

    /**
     * This method is used to evaluate post rules per request
     * @return null
     */
    public function evaluate_request_post_rules($shop_id = null) {

        $current_post_id = $this->get_the_ID();
        $rules_engine = new RulesEngine();

        if ($shop_id != null) {
            $current_post_id = (int) $shop_id;
        }

        // Null indicates that rule evaluation dependencies are not loaded.
        // Call the is_valid method after loading dependent files.
        
        if ($current_post_id == false) {
            return null;
        }
        
        $applied_rules = $this->get_request_rules();

        if ($applied_rules != null && !empty($applied_rules)) {

            foreach ($applied_rules as $applied_rule) {

                if (RulesEngineUtil::isContains($applied_rule->expression, BIS_RULE_POST_EXPRESSION_APPEND)) {

                    $expression = explode(" ", $applied_rule->expression);

                    foreach ($expression as $key => $value) {

                        if (RulesEngineUtil::isContains($value, BIS_RULE_POST_EXPRESSION_APPEND)) {
                            $rule_values = explode("$", $value);
                            $rule_post_id = (int) $rule_values[1];
                            $condId = (int) $rule_values[2];

                            $eval = RulesEngineUtil::evaluateIntTypeRule($current_post_id, $rule_post_id, $condId);

                            // Rule evaluation is completed, below code is specific to page rule.
                            // If rule page Id and curent page Id is equal.
                            // Check whether the page Id is part of the active page rule.
                            if ($eval) {
                                $eval = "T";
                            } else {
                                $eval = "F";
                            }

                            $expression[$key] = $eval;
                        }
                    } // End of express for loop

                    $eval_expression = implode(" ", $expression);

                    $applied_rule->eval = $eval_expression;

                    if (!RulesEngineUtil::isContains($eval_expression, "X")) {
                        $eval_expression = $rules_engine->evaluate_expression($eval_expression);

                        // If expression is true call hook;
                        if ($eval_expression) {
                            $rules_engine->call_hook($applied_rule);
                        }

                        $applied_rule->eval = $eval_expression;
                    } // End of if
                } // End of applied for loop
                        }

            RulesEngineUtil::set_request_rules($applied_rules);
        } // End of If
    }

    /**
     * This method returns the pages that to be appended with content.
     *
     * @return array
     */
    public function get_request_post_append_contents() {
        $applied_rules = $this->get_request_rules();
        // Call the rules to get the excluded pages.
        $post_rule_modal = new PostRulesEngineModel();
        $applied_post_rules = $post_rule_modal->get_applied_post_rules($applied_rules);

        $content_append_posts = array();

        if ($applied_rules != null) {
            foreach ($applied_rules as $applied_rule) {
                if (isset($applied_rule->eval) && ($applied_rule->eval === true)) {
                    if ($applied_post_rules != null) {
                        foreach ($applied_post_rules as $applied_post_rule) {
                            if ($applied_post_rule->lrId == $applied_rule->ruleId) {
                                array_push($content_append_posts, $applied_post_rule);
                            }
                        }
                    }
                } // End of for loop
            } // End of If
        }
        return $content_append_posts;
    }

    /**
     * This method returns the excluded post from DB if not found in session.
     *
     * @return array of excluded post ids
     */
    public function get_session_post_append_contents() {
        $append_to_posts = RulesEngineCacheWrapper::get_session_attribute(BIS_APPEND_TO_POSTS);

        if ($append_to_posts == null) {
            $this->get_applied_post_rules();
        }

        return $append_to_posts;
    }

    /**
     * This method returns the excluded posts from DB if not found in session.
     *
     * @return array of excluded post ids
     */
    public function bis_re_get_request_excluded_rule_posts() {

        // Check if excluded posts exists in session.
        $exclude_posts_rules = RulesEngineCacheWrapper::get_session_attribute(BIS_EXCLUDE_REQUEST_RULE_POSTS);

        $applied_rules = $this->get_request_rules();
       // $rule_count = count($applied_rules);

        // If excluded post does not exist in session, then get from DB.
        if ($exclude_posts_rules == null && !empty($applied_rules)) {

            // Call the rules to get the excluded posts.
            $post_rule_modal = new PostRulesEngineModel();
            $exclude_posts_rules = $post_rule_modal->get_applied_post_rules($applied_rules);

            if (!empty($exclude_posts_rules)) {
                RulesEngineCacheWrapper::set_session_attribute(BIS_EXCLUDE_REQUEST_RULE_POSTS, $exclude_posts_rules);
            }
        }
    }

    /**
     * This method is used to append content to post or post using shortcodes.
     *
     * @param $atts
     * @param null $title
     * @internal param null $content
     * @return string
     */
    public function bis_post_rule_shortcode($atts, $title = null) {
        global $post;

        $postID = $post->ID;
        $content = "";

        $merged_append_posts = $this->get_all_post_rules();

        extract(shortcode_atts(array(
            'rulename' => '',
            'ruletype' => ''
                        ), $atts));

        if ($merged_append_posts != null && !empty($merged_append_posts)) {

            // Logic to check if shortcode
            foreach ($merged_append_posts as $append_to_post) {

                if (!($append_to_post->action === "hide_post" || $append_to_post->action === "show_post" ||
                        $append_to_post->action === "replace_post_content" || $append_to_post->action === "show_only_post"
                        || $append_to_post->action === "payment" || $append_to_post->action === "shipping")) {

                    $location = json_decode($append_to_post->gencol2)->content_position;
                    
                    if (($location === "pos_cust_scode_post" ||
                            $location === "pos_cust_scode_post") && $append_to_post->parent_id == $postID && $append_to_post->crulename === $rulename) {
                        if ($append_to_post->action === "append_existing_scode_post") {
                            $content = do_shortcode(stripslashes($append_to_post->gencol1));
                        } else {
                            $content = RulesDynamicContent::get_dynamic_content($append_to_post, $title);
                        }
                    }
                    if ($append_to_post->parent_type_value == "wp-categories" || $append_to_post->parent_type_value == "categories") {
                        $current_post_categories = get_the_category($postID);
                        if (empty($current_post_categories)) {
                            $current_post_categories = get_the_terms($postID, 'product_cat');
                        }
                        if (!empty($current_post_categories)) {

                            foreach ($current_post_categories as $current_post_category) {
                                if ($current_post_category->term_id == $append_to_post->parent_id) {
                                    if ($append_to_post->action === "append_existing_scode_post") {
                                        $content = do_shortcode(stripslashes($append_to_post->gencol1));
                                    } else {
                                        $content = RulesDynamicContent::get_dynamic_content($append_to_post, $title);
                                    }
                                }
                            }
                        }
                    } elseif ($append_to_post->parent_type_value == "attributes") {
                        $product_ids = array();
                        $woo_attribute = json_decode($append_to_post->gencol4);
                        $args = array(
                            'post_type' => 'product',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'pa_' . $woo_attribute[0],
                                    'field' => 'slug',
                                    'terms' => $append_to_post->parent_id
                                )
                            )
                        );
                        $products = new \WP_Query($args);
                        while ($products->have_posts()) {
                            $products->the_post();
                            $product_id = $products->post->ID;
                            array_push($product_ids, $product_id);
                        }
                        foreach ($product_ids as $product_id) {
                            if ($product_id == $postID) {
                                if ($append_to_post->action === "append_existing_scode_post") {
                                    $content = do_shortcode(stripslashes($append_to_post->gencol1));
                                } else {
                                    $content = RulesDynamicContent::get_dynamic_content($append_to_post, $title);
                                }
                            }
                        }
                    } elseif ($append_to_post->parent_type_value == "woo-tags" || $append_to_post->parent_type_value == "wp-tags") {
                        $product_ids = array();
                        if ($append_to_post->parent_type_value == "woo-tags") {
                            $args = array(
                                'post_type' => 'product',
                                'product_tag' => $append_to_post->parent_id
                            );
                        } else {
                            $args = array(
                                'post_type' => 'post',
                                'tag' => $append_to_post->parent_id
                            );
                        }

                        $wp_obj = new \WP_Query($args);
                        while ($wp_obj->have_posts()) {
                            $wp_obj->the_post();
                            $product_id = $wp_obj->post->ID;
                            array_push($product_ids, $product_id);
                        }
                        if (!empty($product_ids)) {
                            foreach ($product_ids as $product_id) {
                                if ($product_id == $postID) {
                                    if ($append_to_post->action === "append_existing_scode_post") {
                                        $content = do_shortcode(stripslashes($append_to_post->gencol1));
                                    } else {
                                        $content = RulesDynamicContent::get_dynamic_content($append_to_post, $title);
                                    }   
                                }
                            }
                        }
                    }
                }
            }
        }

        return $content;
    }

    /**
     * This method is used to get all the applied post rules.
     *
     * @return array
     */
    public function get_all_post_rules() {

        $post_request_rules = $this->get_request_post_append_contents();
        $append_to_posts = $this->bis_re_get_appended_post_ids();

        $merged_append_posts = $post_request_rules;

        if ($merged_append_posts != null) {
            if ($append_to_posts != null) {
                $merged_append_posts = array_merge($merged_append_posts, $append_to_posts);
            }
        } else {
            $merged_append_posts = $append_to_posts;
        }

        return $merged_append_posts;
    }
    
    public function before_checkout_create_order($order, $data) {
        $orders_terms = $order->get_items();

        //Woo books(Products)
        $book_ids = array(3909, 3908, 3907, 3906, 3905, 3912, 3913, 3914,
            3915, 866, 867, 868, 869, 861, 862, 863, 864, 870, 859, 865);
        $cat_ids = array();
        $post_duration = array();
        if (!empty($orders_terms)) {
            $post_rules_model = new PostRulesEngineModel();
            foreach ($orders_terms as $orders_term) {
                $product_meta_data = $orders_term->get_product();
                $prod_id = $product_meta_data->get_id();
                if (in_array($prod_id, $book_ids)) {
                    switch ($prod_id) {
                        //Adding post categories
                        //Eduboekballade groep 7 en 8
                        case 3909:
                            $slug = $post_rules_model->get_slug_by_id(74, "portfolio_cat");
                            array_push($cat_ids, $slug);
                            break;
                        //Eduboekballade groep 5 en 6
                        case 3908:
                            $slug = $post_rules_model->get_slug_by_id(73, "portfolio_cat");
                            array_push($cat_ids, $slug);
                            break;
                        //Eduboekballade groep 3 en 4
                        case 3907:
                            $slug = $post_rules_model->get_slug_by_id(72, "portfolio_cat");
                            array_push($cat_ids, $slug);
                            break;
                        //Hoogbegaafd B groep 5 t/m 6
                        case 3906:
                            $slug = $post_rules_model->get_slug_by_id(76, "portfolio_cat");
                            array_push($cat_ids, $slug);
                            break;
                        //Hoogbegaafd A groep 3 t/m 6
                        case 3905:
                            $slug = $post_rules_model->get_slug_by_id(75, "portfolio_cat");
                            array_push($cat_ids, $slug);
                            break;
                        //Kast 3 – groep 7 en 8
                        case 3912:
                        case 3913:
                        case 3914:
                        case 3915:
                            $slug = $post_rules_model->get_slug_by_id(71, "portfolio_cat");
                            array_push($cat_ids, $slug);
                            break;
                        //Kast 2 – groep 5 en 6
                        case 3918:
                        case 3919:
                        case 3920:
                        case 3921:
                            $slug = $post_rules_model->get_slug_by_id(70, "portfolio_cat");
                            array_push($cat_ids, $slug);
                            break;
                        //Kast 1 – groep 3 en 4
                        case 861:
                        case 862:
                        case 863:
                        case 864:
                            $slug = $post_rules_model->get_slug_by_id(69, "portfolio_cat");
                            array_push($cat_ids, $slug);
                            break;
                    }
                    if ($prod_id === 3739 || $prod_id === 3738 || $prod_id === 3736 || $prod_id === 3741 || $prod_id === 3740) {
                        //10 Years
                        array_push($post_duration, "87600:00");
                    } elseif ($prod_id === 874 || $prod_id === 869 || $prod_id === 863) {
                        //1 day
                        array_push($post_duration, "24:00");
                    } elseif ($prod_id === 871 || $prod_id === 866 || $prod_id === 861) {
                        //1 year
                        array_push($post_duration, "8760:00");
                    } elseif ($prod_id === 873 || $prod_id === 868 || $prod_id === 862) {
                        //3 Months
                        array_push($post_duration, "2190:00");
                    } elseif ($prod_id === 872 || $prod_id === 867 || $prod_id === 864) {
                        //6 Months
                        array_push($post_duration, "4380:00");
                    } else {
                        array_push($post_duration, "0:0");
                    }
                }
            }
        }
        if (!empty($cat_ids)) {
            $post_rules_model->create_post_rule_based_on_woo_order($cat_ids, $post_duration, $data);
        }
        //$order->update_meta_data('_custom_meta_key', 'value');
    }

    public function bis_clear_cached_post_rules() {
        RulesEngineCacheWrapper::remove_session_attribute(BIS_EXCLUDE_POSTS);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_APPEND_TO_POSTS);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_EXCLUDE_REQUEST_RULE_POSTS);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_RULE_TYPE_CONST . BIS_POST_TYPE_RULE);
    }
    

}