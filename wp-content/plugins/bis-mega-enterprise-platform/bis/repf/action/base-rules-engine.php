<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

namespace bis\repf\action;

use bis\repf\common\RulesEngineCacheWrapper;
use bis\repf\common\BISSessionWrapper;
use bis\repf\vo\GeolocationVO;
use RulesEngineUtil;

/**
 * Base class for all rule engines
 *
 * Class BaseRulesEngine
 */
abstract class BaseRulesEngine {

    public function clear_applied_rules($user = null) {
        RulesEngineCacheWrapper::remove_session_attribute(BIS_RULES_ARRAY);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_REQUEST_RULES_ARRAY);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_REDIRECT_REQUEST_RULES_ARRAY);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_REDIRECT_SESSION_RULES_ARRAY);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_REQUEST_RULES . session_id());
        //RulesEngineCacheWrapper::remove_session_attribute(BIS_LOGICAL_POPUP_RULES);
        
        // Call the hook so that the addon plugin clean up their session info.
        do_action('bis_clear_cached_rules');
    }

    public function get_applied_session_logical_rules($user = null) {
        return $this->get_applied_logical_rules($user);
    }
    
    /**
     * This method is used to set the geolocation to the current session.
     */
    public function set_geolocation() {

        $sessionWrapper = new BISSessionWrapper();
        $geoPlugin = $sessionWrapper->getGeoPlugin();
        $geoVO = new GeolocationVO();
        $geoVO->setCountryCode($geoPlugin->getCountryCode());
        $geoVO->setCountryCodeLowerCase(strtolower($geoPlugin->getCountryCode()));
        $geoVO->setCountryName($geoPlugin->getCountryName());
        $geoVO->setCity($geoPlugin->getCity());
        $geoVO->setRegion($geoPlugin->getRegion());
        $geoVO->setContinentCode($geoPlugin->getContinentCode());
        $geoVO->setIPAddress($geoPlugin->getClientIP());
        $geoVO->setCurrencyCode($geoPlugin->getCurrencyCode());
        $geoVO->setZipCode($geoPlugin->getPostalCode());
        $geoVO->setLongitude($geoPlugin->getLongitude());
        $geoVO->setLatitude($geoPlugin->getLatitude());
        RulesEngineCacheWrapper::set_session_attribute(BIS_GEOLOCATION_VO, $geoVO);
    }

    /**
     * Get geo location vo from session.
     * @return type
     */
    public function get_geolocation() {
        return RulesEngineCacheWrapper::get_session_attribute(BIS_GEOLOCATION_VO);
    }
    
    /**
     * Get the cache cookie keys
     * 
     * @return type
     */
    public static function get_cache_cookie() {
        return array("bis_mega_pcode", "bis_mega_city", "bis_mega_state",
            "bis_mega_country", "bis_mega_continent");
    }

    /**
     * Set cookie for cache.
     * 
     */
    public function set_cache_cookie() {
        $geoVO = $this->get_geolocation();
        
        if ($geoVO == null) {
            $this->set_geolocation();
            $geoVO = $this->get_geolocation();
        }
        
        $cache_cookies = $this->get_cache_cookie();
        $cache_key = null;

        foreach ($cache_cookies as $value) {
            if (\RulesEngineUtil::get_option($value, false, false) > 0) {
                $cache_key = $value;
                break;
            }
        }
        $dynamic_cookie_value = null;
       
        if (\RulesEngineUtil::isEqual($cache_key, BIS_MEGA_PCODE_KEY)) {
            $dynamic_cookie_value = BIS_MEGA_PCODE_KEY . "_" . $geoVO->getZipCode();
        } else if (\RulesEngineUtil::isEqual($cache_key, BIS_MEGA_CITY_KEY)) {
            $dynamic_cookie_value = BIS_MEGA_CITY_KEY . "_" . $geoVO->getCity();
        } else if (\RulesEngineUtil::isEqual($cache_key, BIS_MEGA_STATE_KEY)) {
            $dynamic_cookie_value = BIS_MEGA_STATE_KEY . "_" . $geoVO->getRegion();
        } else if (\RulesEngineUtil::isEqual($cache_key, BIS_MEGA_COUNTRY_KEY)) {
            $dynamic_cookie_value = BIS_MEGA_COUNTRY_KEY . "_" . $geoVO->getCountryCodeLowerCase();
        } else if (\RulesEngineUtil::isEqual($cache_key, BIS_MEGA_CONTINENT_KEY)) {
            $dynamic_cookie_value = BIS_MEGA_CONTINENT_KEY . "_" . $geoVO->getContinentCode();
        }
        // get logic
        if (!isset($_COOKIE["bis_mega_rule"])) {
            setcookie("bis_mega_rule", $dynamic_cookie_value, time() + BIS_COOKIE_EXPIRE_TIME, COOKIEPATH, COOKIE_DOMAIN);
        }
    }
    /**
     * This method return applied logical rules returns only the session rules.
     *
     * @return $applied_rules
     */
    public function get_applied_logical_rules($user = null) {

        $applied_rules = null;

        if (RulesEngineUtil::is_rules_updated()) {
            $this->clear_applied_rules($user);
            $rulesEngine = new RulesEngine();
            $rulesEngine->add_logical_rules();
            RulesEngineCacheWrapper::set_session_attribute(BIS_LOGICAL_CACHE_REFRESH_ID, RulesEngineUtil::get_rule_cache_id(BIS_LOGICAL_CACHE_REFRESH_ID));
        }

        if (RulesEngineCacheWrapper::get_session_attribute(BIS_RULES_ARRAY) != null) {
            $applied_rules = RulesEngineCacheWrapper::get_session_attribute(BIS_RULES_ARRAY);
        }

        return $applied_rules;
    }

    /**
     * This method return excluded widget from session if available
     *
     * @return $exclude_widgets
     */
    public function get_widget_rules() {

        $exclude_widgets = null;
        $session_reset_time = 0;

        if (RulesEngineCacheWrapper::get_reset_time(BIS_WIDGET_RULE_RESET) !== false) {
            $reset_time = RulesEngineCacheWrapper::get_reset_time(BIS_WIDGET_RULE_RESET);

            if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_WIDGET_RULE_RESET)) {
                $session_reset_time = RulesEngineCacheWrapper::get_session_attribute(BIS_WIDGET_RULE_RESET);
            }

            if ($session_reset_time < $reset_time) {
                // Check if excluded category exists in session.
                if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_EXCLUDE_WIDGETS)) {
                    RulesEngineCacheWrapper::set_session_attribute(BIS_WIDGET_RULE_RESET, $reset_time);
                    RulesEngineCacheWrapper::set_session_attribute(BIS_EXCLUDE_WIDGETS, null);
                    $exclude_widgets = null;
                }
            } else {
                // Check if excluded category exists in session.
                if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_EXCLUDE_WIDGETS)) {
                    $exclude_widgets = RulesEngineCacheWrapper::get_session_attribute(BIS_EXCLUDE_WIDGETS);
                }
            }
        } else {
            // Check if excluded widgets exists in session.
            if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_EXCLUDE_WIDGETS)) {
                $exclude_widgets = RulesEngineCacheWrapper::get_session_attribute(BIS_EXCLUDE_WIDGETS);
            }
        }

        return $exclude_widgets;
    }

    /**
     * This method returns excluded pages if available in session
     * @return $exclude_pages
     */
    public function get_page_rules() {

        $exclude_pages = null;

        // Check if excluded pages exists in session.
        if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_EXCLUDE_PAGES)) {
            $exclude_pages = RulesEngineCacheWrapper::get_session_attribute(BIS_EXCLUDE_PAGES);
        }

        return $exclude_pages;
    }

    /**
     * This method is used get the append page rules.
     *
     * @return $append_to_pages
     */
    public function get_append_page_rules() {

        $append_to_pages = null;

        // Check if excluded pages exists in session.
        if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_APPEND_TO_PAGES)) {
            $append_to_pages = RulesEngineCacheWrapper::get_session_attribute(BIS_APPEND_TO_PAGES);
        }

        return $append_to_pages;
    }

    /**
     * This method is used to get themes from session.
     * @return $load_themes
     */
    public function get_theme_rules() {
        $load_themes = null;

        if (RulesEngineCacheWrapper::get_reset_time(BIS_THEME_RULE_RESET) !== false) {
            $reset_time = RulesEngineCacheWrapper::get_reset_time(BIS_THEME_RULE_RESET);
            $session_reset_time = 0;

            if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_THEME_RULE_RESET)) {
                $session_reset_time = RulesEngineCacheWrapper::get_session_attribute(BIS_THEME_RULE_RESET);
            }

            if ($session_reset_time < $reset_time) {
                // Check if excluded category exists in session.
                if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_LOAD_RULE_THEME)) {
                    RulesEngineCacheWrapper::set_session_attribute(BIS_THEME_RULE_RESET, $reset_time);
                    RulesEngineCacheWrapper::set_session_attribute(BIS_LOAD_RULE_THEME, null);
                    $load_themes = null;
                }
            } else {
                // Check if excluded category exists in session.
                if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_LOAD_RULE_THEME)) {
                    $load_themes = RulesEngineCacheWrapper::get_session_attribute(BIS_LOAD_RULE_THEME);
                }
            }
        } else {
            // Check if excluded widgets exists in session.
            if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_LOAD_RULE_THEME)) {
                $load_themes = RulesEngineCacheWrapper::get_session_attribute(BIS_LOAD_RULE_THEME);
            }
        }

        return $load_themes;
    }

    /**
     * This method is used to get the category rules from session.
     *
     * @return $exclude_categories
     */
    public function get_posts_rules() {

        $exclude_posts = null;

        $session_reset_time = 0;

        if (RulesEngineCacheWrapper::get_reset_time(BIS_POST_RULE_RESET) !== false) {
            $reset_time = RulesEngineCacheWrapper::get_reset_time(BIS_POST_RULE_RESET);

            if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_POST_RULE_RESET)) {
                $session_reset_time = RulesEngineCacheWrapper::get_session_attribute(BIS_POST_RULE_RESET);
            }

            if ($session_reset_time < $reset_time) {
                // Check if excluded posts exists in session.
                if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_EXCLUDE_POSTS)) {

                    RulesEngineCacheWrapper::set_session_attribute(BIS_EXCLUDE_POSTS, null);
                    RulesEngineCacheWrapper::set_session_attribute(BIS_APPEND_TO_POSTS, null);
                    $exclude_posts = RulesEngineCacheWrapper::get_session_attribute(BIS_EXCLUDE_POSTS);
                    RulesEngineCacheWrapper::set_session_attribute(BIS_POST_RULE_RESET, $reset_time);
                }
            } else {
                // Check if excluded posts exists in session.
                if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_EXCLUDE_POSTS)) {
                    $exclude_posts = RulesEngineCacheWrapper::get_session_attribute(BIS_EXCLUDE_POSTS);
                }
            }
        } else {
            // Check if excluded pages exists in session.
            if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_EXCLUDE_POSTS)) {
                $exclude_posts = RulesEngineCacheWrapper::get_session_attribute(BIS_EXCLUDE_POSTS);
            }
        }

        return $exclude_posts;
    }

    /**
     * This method is used to return the request rules like Post, Page and Request URL.
     * @return array|mixed|null
     */
    public function get_request_rules() {
        $request_rules = RulesEngineCacheWrapper::get_session_attribute(BIS_REQUEST_RULES . session_id());

        if ($request_rules == null || $request_rules === false) {

            $request_rules = null;

            if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_REQUEST_RULES_ARRAY)) {
                $request_rules = RulesEngineCacheWrapper::get_session_attribute(BIS_REQUEST_RULES_ARRAY);
            }

            if ($request_rules == null) {
                $request_rules = array();
                RulesEngineCacheWrapper::set_session_attribute(BIS_REQUEST_RULES_ARRAY, $request_rules);
            }

            RulesEngineUtil::set_request_rules($request_rules);
        }

        return $request_rules;
    }

    /**
     * This method is used to get the current post Id if available or return false if not available.
     *
     * @return bool
     */
    public function get_the_ID() {
        $id = null;

        if (get_post()) {
            return get_the_ID();
        }

        return false;
    }

    public function get_applied_logical_rule($applied_rules, $redirect_rule) {
        $red_applied_log_rule = null;

        foreach ($applied_rules as $applied_rule) {
            if ($applied_rule->ruleId === $redirect_rule->lrId) {
                $red_applied_log_rule = $applied_rule;
                break;
            }
        }
        return $red_applied_log_rule;
    }

    /**
     * This method is used to call the defined hook using logical rule.
     *
     * @param $logical_rule
     * @throws RuntimeException
     */
    public function call_hook($logical_rule) {
        if (isset($logical_rule->action_hook)) {
            $hookName = $logical_rule->action_hook;
            if ($hookName == null || $hookName === "") {
                return;
            }

            $strHookName = "'" . $hookName . "'";

            add_action($strHookName, $hookName);

            // Check if hook exists
            if (has_action($strHookName)) {
                do_action($strHookName);
            } else { // If hook not found throw exception
                $message = "Hook with name " . $strHookName . " not found ";
                throw new RuntimeException($message);
            }
        }
    }

    /**
     * 
     * This method is used to return the site URL.
     * 
     * @return type site url
     */
    public function get_site_url() {

        $site_url = get_site_url();

        if (is_ssl()) {
            $scheme = "https";
            $site_url = get_site_url(null, null, $scheme);
        }

        return $site_url;
    }
    
    public function bis_wp_rocket_flush() {

        // Update the WP Rocket rules on the .htaccess file.
        if (function_exists('rocket_activate_bis_mega_platform')) {
            // Regenerate the config file.
            rocket_activate_bis_mega_platform();
        }
    }

}