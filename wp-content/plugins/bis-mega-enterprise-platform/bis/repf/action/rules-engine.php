<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

namespace bis\repf\action;

use bis\repf\common\RulesEngineCacheWrapper;
use RulesEngineCommon;
use bis\repf\model\LogicalRulesEngineModel;
use bis\repf\model\AnalyticsEngineModel;
use bis\repf\common\BISSessionWrapper;
use bis\repf\util\RulesStack;
use bis\repf\vo\GeolocationVO;
use bis\repf\util\uagent_info;
use RulesEngineUtil;
use bis\repf\model\PageRulesEngineModel;
use bis\repf\model\PostRulesEngineModel;
use RulesDynamicContent;

/**
 * This class is used to evaluate logical rules.
 *
 * Class RulesEngine
 */
class RulesEngine extends BaseRulesEngine {
    
    public function bis_start_session() {

        if (RulesEngineUtil::is_reset_rule()) {
            RulesEngineCacheWrapper::set_reset_time(BIS_LOGICAL_RULE_RESET);
        }

        if (RulesEngineCacheWrapper::get_session_attribute(BIS_GEOLOCATION_VO) == null) {
            $this->set_geolocation();
        }

        if (!RulesEngineCacheWrapper::is_session_attribute_set(BIS_SESSION_INITIATED) ||
                (RulesEngineCacheWrapper::get_session_attribute(BIS_SESSION_INITIATED) === false)) {

            RulesEngineCacheWrapper::set_session_attribute(BIS_SESSION_INITIATED, true);
            $bis_capture_analytics = RulesEngineUtil::get_option(BIS_CAPTURE_ANALYTICS_DATA);

            if ($bis_capture_analytics === "true") {
                $auditVO = RulesEngineCacheWrapper::get_session_attribute(BIS_AUDIT_INFO);

                if ($auditVO == null) {
                    $auditVO = AnalyticsEngineModel::audit_user_request(1);
                    RulesEngineCacheWrapper::set_session_attribute(BIS_AUDIT_INFO, $auditVO);
                }
            }
            $this->add_logical_rules();
        } else {
            $session_applied_rules = RulesEngineCacheWrapper::get_session_attribute(BIS_SESSION_APPLIED_RULES);

            if (RulesEngineUtil::is_rules_updated()) {
                if(function_exists('rocket_clean_domain')){
                    rocket_clean_domain();
                }
                $this->clear_applied_rules();
                $this->add_logical_rules();
                RulesEngineCacheWrapper::set_session_attribute(BIS_LOGICAL_CACHE_REFRESH_ID, RulesEngineUtil::get_rule_cache_id(BIS_LOGICAL_CACHE_REFRESH_ID));
            } else if (!empty($session_applied_rules)) {
                do_action('bis_apply_session_rules', $session_applied_rules);
            }
        }
    }

    /**
     * This method check whether the session is started or not
     * @return bool
     */
    public static function is_session_started() {
        if (php_sapi_name() !== 'cli') {
            if (version_compare(phpversion(), '5.4.0', '>=')) {
                return session_status() === PHP_SESSION_ACTIVE ? TRUE : FALSE;
            } else {
                return session_id() === '' ? FALSE : TRUE;
            }
        }
        return FALSE;
    }

    /**
     * Stores all the validated rules in session
     */
    public function add_logical_rules($user = null) {

        $logical_rules_engine_modal = new LogicalRulesEngineModel();
        $logical_rules = $logical_rules_engine_modal->get_active_rules();
        $rules_criteria_list_cached = null;
        $bis_session_rules = null;
        
        if (!empty($logical_rules)) {
            $rules_criteria_list_cached = $logical_rules_engine_modal->get_rule_criteria_by_logical_rules($logical_rules);
        }

        if (!empty($rules_criteria_list_cached)) {
            foreach ($logical_rules as $logical_rule) {
                $eVal = null;
                $rules_criteria_list = $rules_criteria_list_cached[$logical_rule->ruleId];
                
                //popup coutry dropdown form submit
                $coutry_dropdown_is_set = false;
                if (isset($_POST["bis_fea_country_dropdown_select"]) || isset($_POST["bis_fea_country_dropdown_select_widget"]) || isset($_POST["bis_autocomplete"])) {
                    $coutry_dropdown_is_set = true;
                }
                
                //popup coutry dropdown form submit
                $return_array = $this->bis_search_box_rule_is_there($rules_criteria_list);
                if ($coutry_dropdown_is_set == false || ($coutry_dropdown_is_set == true && $return_array == true) || $return_array == false) {
                    $expression = $this->get_rules_expression($rules_criteria_list, $user);
                } else {
                    $expression = "";
                }
                
                // If expression contains X values then donot evaluate,
                // Expressions with X values should be evaluated at request time not session time.
                //@dev
                //var_dump("Expression ", $expression);

                $logical_rule->expression = null;

                if (!RulesEngineUtil::isContains($expression, "X")) {
                    $eVal = $this->evaluate_expression($expression);
                } else {
                    $eVal = true;
                    $logical_rule->expression = trim($expression);
                }

                //@dev
                // var_dump("Final Expression ", $eVal);
                // Final value after evaluating the stack;
                if ($eVal) {
                    $bis_session_rules = $this->register_and_store_hook($logical_rule);
                }
            }
        }

        if (!empty($bis_session_rules)) {
            $session_applied_rules = array(BIS_SESSION_APPLIED_RULES => $bis_session_rules);
            RulesEngineCacheWrapper::set_session_attribute(BIS_SESSION_APPLIED_RULES, $session_applied_rules);
            //add_action($hook, $function_to_add, $priority, $accepted_args);
            do_action('bis_apply_session_rules', $session_applied_rules);
        }

        RulesEngineCacheWrapper::set_session_attribute(BIS_LOGICAL_CACHE_REFRESH_ID, RulesEngineUtil::get_rule_cache_id(BIS_LOGICAL_CACHE_REFRESH_ID));
    }
    
    public function bis_search_box_rule_is_there($rules_criteria_list){
        
        $bis_search_box = false;
        if (!empty($rules_criteria_list)) {
            foreach ($rules_criteria_list as $rule_criteria) {
                //popup coutry dropdown form submit
                if ($rule_criteria->subOptId == "41") {
                    $bis_search_box = true;
                    break;
                }
            }
        }
        
        return $bis_search_box;
    }

    /**
     * This method will evaluate and return the rules status from the list of rules.
     * @param $rules_criteria_list
     * @return string
     */
    public function get_rules_expression($rules_criteria_list, $user = null) {

        $expression = "";
        $geoVO = RulesEngineCacheWrapper::get_session_attribute(BIS_GEOLOCATION_VO);

        if ($geoVO == null) {
            $this->set_geolocation();
            $geoVO = RulesEngineCacheWrapper::get_session_attribute(BIS_GEOLOCATION_VO);
        }
        
        if (!empty($rules_criteria_list) && $geoVO instanceof GeolocationVO) {
            foreach ($rules_criteria_list as $rule_criteria) {
                
                for ($c = 0; (int) $rule_criteria->lb > $c; $c++) {
                    $expression = $expression . "( ";
                }

                $value = $this->evaluate_rule($rule_criteria, $user, $geoVO);
                if (RulesEngineUtil::isContains($value, "X")) {
                    $str_value = $value;
                } else {

                    $str_value = "F";

                    if ($value) {
                        $str_value = "T";
                    }
                }

                $expression = $expression . "" . $str_value;

                for ($c = 0; (int) $rule_criteria->rb > $c; $c++) {
                    $expression = $expression . " )";
                }

                if ($rule_criteria->operId == 1) {
                    $expression = $expression . " + ";
                } else if ($rule_criteria->operId == 2) {
                    $expression = $expression . " - ";
                }
            }
        }

        return $expression;
    }

    /**
     * This method is used to evaluate logical rule.
     *
     * @param $logical_rule
     * @return bool|string
     */
    private function evaluate_rule($logical_rule, $user = null, GeolocationVO $geo_plugin) {

        require_once(ABSPATH . '/wp-includes/pluggable.php');
        
        $selected_country_value = RulesEngineCacheWrapper::get_session_attribute(BIS_COUNTRY_DROPDOWN_VALUE);
        if (isset($_POST["bis_fea_country_dropdown_select"]) || isset($_POST["bis_fea_country_dropdown_select_widget"]) || ($selected_country_value !== null && $selected_country_value !== "")) {
            $searched_country = "";
            if (isset($_POST["bis_fea_country_dropdown_select"])) {
                $searched_country = $_POST["bis_fea_country_dropdown_select"];
            } elseif (isset($_POST["bis_fea_country_dropdown_select_widget"])) {
                $searched_country = $_POST["bis_fea_country_dropdown_select_widget"];
            } elseif ($selected_country_value !== null && $selected_country_value !== "") {
                $searched_country = $selected_country_value;
            }
            
            $searched_continent = RulesEngineUtil::bis_get_continent_code_for_country($searched_country);
            $geo_plugin->continentCode = $searched_continent;
        }
        
        $selected_search_box_value = RulesEngineCacheWrapper::get_session_attribute(BIS_SEARCH_BOX_VALUE);
        if (($selected_search_box_value !== null && $selected_search_box_value !== "") || isset($_POST["bis_autocomplete"]) || isset($_POST["bis_autocomplete_google_city"]) || isset($_POST["bis_autocomplete_google_state"]) || isset($_POST["bis_autocomplete_google_country"])) {
            if (isset($_POST["bis_autocomplete_google_city"]) && $_POST["bis_autocomplete_google_city"] != "" && $_POST["bis_autocomplete_google_state"] != "" && $_POST["bis_autocomplete_google_country"] != "") {
                $geo_plugin->city = $_POST["bis_autocomplete_google_city"];
                $selected_search_box_value = $_POST["bis_autocomplete_google_city"];
            } else if (isset ($_POST["bis_autocomplete_google_state"]) && $_POST["bis_autocomplete_google_city"] == "" && $_POST["bis_autocomplete_google_state"] == "" && $_POST["bis_autocomplete_google_country"] != "") {
                $selected_search_box_value = $_POST["bis_autocomplete_google_city"];
                $geo_plugin->countryCode = $_POST["bis_country_code"];
            } else if (isset ($_POST["bis_autocomplete_google_country"]) && $_POST["bis_autocomplete_google_city"] == "" && $_POST["bis_autocomplete_google_state"] != "" && $_POST["bis_autocomplete_google_country"] != "") {
                $geo_plugin->region = $_POST["bis_autocomplete_google_state"];
                $selected_search_box_value = $_POST["bis_autocomplete_google_city"];
            } 
            RulesEngineCacheWrapper::set_session_attribute(BIS_SEARCH_BOX_VALUE, $selected_search_box_value);
        }
        $sub_opt_id = (int) $logical_rule->subOptId; // Option Id
        $condId = $logical_rule->condId;
        if ($condId == 15) {
            global $wpdb;
            $table_prefix = $wpdb->prefix;
            $neared_cites_obj = $wpdb->get_results("SELECT general_col2 AS near_by_cities FROM " . $table_prefix . "bis_re_logical_rules_criteria WHERE logical_rule_id =" . $logical_rule->lrId);
            if (!empty($neared_cites_obj)) {
                $neared_cites = array();
                foreach ($neared_cites_obj as $neared_city) {
                    $neared_cities = $neared_city->near_by_cities;
                    $neared_city_name = json_decode($neared_cities);
                    foreach ($neared_city_name as $neared_city_id) {
                        array_push($neared_cites, array('id' => $neared_city_id->city));
                    }
                }
                
                $condId = 12;
                $rule_value = json_encode($neared_cites);
            } else {
                $condId = 12;
                $rule_value = $logical_rule->value;
            }
        } else {
            $rule_value = $logical_rule->value;
        }
        $eVal = false;

        switch ($sub_opt_id) {

            case 1:    //User Role
                $eVal = false;
                if(is_user_logged_in()) {    
                    $user = wp_get_current_user();
                    if ($user->ID != 0) {
                        foreach ($user->roles as $roleName) {

                            $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $roleName, $condId);
                            if ($eVal) {
                                break;
                            }
                        }
                    }
                } else {
                    $roleName = "bis_guest";
                    $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $roleName, $condId);
                }

                break;

            case 2: //emailId Rule
                $eVal = false;
                if(is_user_logged_in()) {
                    $logged_in_user = wp_get_current_user();
                    if ($logged_in_user->ID != 0) {
                        $email = $logged_in_user->user_email;
                        if ($condId == 1 || $condId == 2 || $condId == 12 || $condId == 14) {
                            $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $email, $condId);
                        } else {
                            $eVal = RulesEngineUtil::evaluateStringTypeRule($email, $rule_value, $condId);
                        }
                    }
                }
                break;

            case 3: // Registered date
                $eVal = false;
                if(is_user_logged_in()) {
                    $logged_in_user = wp_get_current_user();
                    if ($logged_in_user->ID != 0) {
                        $reg_date = $logged_in_user->user_registered;
                        $reg_date = date_format(date_create($reg_date), "Y-m-d");
                        $eVal = RulesEngineUtil::evaluateDateTypeRule($reg_date, $rule_value, $condId);
                    }
                }
                break;

            case 4: // Country
                    $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $geo_plugin->getCountryCode(), $condId);
                break;
            
            case 5: // Currency
                $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $geo_plugin->getCurrencyCode(), $condId);
                break;

            case 6: // Referral Path
                $eVal = RulesEngineUtil::get_dynamic_rule_expression(BIS_RULE_REFERRAL_PATH_EXPRESSION_APPEND, $rule_value, $condId);
                break;

            case 7: // language
                $eVal = RulesEngineUtil::evaluateLanguageRule($rule_value, $condId);
                break;

            case 8: // Browser
                $eVal = RulesEngineUtil::evaluate_browser_rule($rule_value, $condId);
                break;

            case 9: // Date
                // Get the current date
                $current_date = date("Y-m-d");
                $eVal = RulesEngineUtil::evaluateDateTypeRule($current_date, $rule_value, $condId);
                break;

            case 10: // Time
                $current_time = date('H:i', current_time('timestamp', 0));
                $t1 = strtotime($current_time);
                $t2 = strtotime($rule_value);
                $eVal = RulesEngineUtil::evaluateIntTypeRule($t1, $t2, $condId);
                break;
            
            case 46: // Hours
                $current_time = date('Y-m-d H:i:s', current_time('timestamp', 0));
                $t2 = strtotime($current_time);
                $t1 = strtotime($logical_rule->gencol2);
                $eVal = RulesEngineUtil::evaluateIntTypeRule($t1, $t2, $condId);
                break;

            case 14: // Date and Time rules should be evaluated at runtime.
                // These value will always true.
                // Get the current date
                $current_date = date('Y-m-d H:i', current_time('timestamp', 0));
                $d1 = strtotime($current_date);
                $d2 = strtotime($rule_value);
                $eVal = RulesEngineUtil::evaluateIntTypeRule($d1, $d2, $condId);
                break;

            case 15: // IP Address Rule
                $haystack = $geo_plugin->getIPAddress();
                $needle = $rule_value;

                if ($condId == 12 || $condId == 14) {
                    $needle = $haystack;
                    $haystack = $rule_value;
                }

                $eVal = RulesEngineUtil::evaluateStringTypeRule($haystack, $needle, $condId);
                break;

            case 16: // Status code       
                $eVal = RulesEngineUtil::get_dynamic_rule_expression_val_type(BIS_RULE_STATUS_EXPRESSION_APPEND, $rule_value, $condId);
                break;

            case 17: // WordPress category code       
                $eVal = RulesEngineUtil::get_dynamic_rule_expression(BIS_RULE_CATEGORY_EXPRESSION_APPEND, $rule_value, $condId);
                break;

            case 25: // WooCommerce category code       
                $eVal = RulesEngineUtil::get_dynamic_rule_expression(BIS_RULE_CATEGORY_EXPRESSION_APPEND, $rule_value, $condId);
                break;

            case 11: // Mobile
            case 12: // Tablet
            case 13: // Mobile Operating System
            case 28:  // Mobile Device Type
                $eVal = $this->evaluate_mobile_rule($rule_value);

                // Not equal
                if ($condId == 2) {
                    $eVal = !$eVal;
                }

                break;

            case 18: // User Id
                $eVal = false;
                $logged_in_user = wp_get_current_user();
                if ($logged_in_user->ID != 0) {
                    $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $logged_in_user->user_login, $condId);
                }
                break;

            case 19: // Guest User
                $eVal = false;

                $logged_in_user = wp_get_current_user();

                // Guest User
                if ($logged_in_user->ID == 0 && $rule_value == 25 && $condId == 1) {
                    $eVal = true;
                }

                // Logged In User
                if ($logged_in_user->ID != 0 && $rule_value == 25 && $condId == 2) {
                    $eVal = true;
                }


                break;

            case 23: // Day of the Week
                $day_of_week = jddayofweek(cal_to_jd(CAL_GREGORIAN, date("m"), date("d"), date("Y")), 1);
                $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $day_of_week, $condId);
                break;

            case 24: // Months
                $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, date('M'), $condId);
                break;

            case 20: // Continent
                $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $geo_plugin->getContinentCode(), $condId);
                break;
            
            case 21: // Page Title
                $eVal = RulesEngineUtil::get_dynamic_rule_expression(BIS_RULE_PAGE_EXPRESSION_APPEND, $rule_value, $condId);
                break;

            case 22: // Post Title
                $eVal = RulesEngineUtil::get_dynamic_rule_expression(BIS_RULE_POST_EXPRESSION_APPEND, $rule_value, $condId);
                break;

            case 26: // Param condition
                $eVal = RulesEngineUtil::get_dynamic_rule_expression(BIS_RULE_PARAM_EXPRESSION_APPEND, $rule_value, $condId);
                break;

            case 27: // Form data
                $eVal = RulesEngineUtil::get_dynamic_rule_expression(BIS_RULE_FORM_DATA_EXPRESSION_APPEND, $rule_value, $condId);
                break;

            case 29: // City Rule
                    $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $geo_plugin->getCity(), $condId);
                break;

            case 30: // Region
                    $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $geo_plugin->getRegion(), $condId);
                break;

            case 31: // Referral Path
                $eVal = RulesEngineUtil::get_dynamic_rule_expression(BIS_RULE_REFERRED_PATH_EXPRESSION_APPEND, $rule_value, $condId);
                break;

            case 32: // Cookie Path
                $eVal = RulesEngineUtil::get_dynamic_rule_expression(BIS_RULE_COOKIE_EXPRESSION_APPEND, $rule_value, $condId);
                break;

            case 33: // Operating System
                $eVal = RulesEngineUtil::evaluate_os_rule($rule_value, $condId);
                break;

            case 34: // Product Tags
                $eVal = RulesEngineUtil::get_dynamic_rule_expression(BIS_RULE_WOO_PRODUCT_TAG_EXPRESSION_APPEND, $rule_value, $condId);
                break;

            case 35: // Postal Code
                    $currentZip = $geo_plugin->getZipCode();
                    $ruleZip = $rule_value;

                    if ($condId == 12 || $condId == 14) {
                        $ruleZip = $currentZip;
                        $currentZip = $rule_value;
                    }

                    $eVal = RulesEngineUtil::evaluateStringTypeRule($currentZip, $ruleZip, $condId);
                break;
                
            case 36: // Post Tags
                $eVal = RulesEngineUtil::get_dynamic_rule_expression(BIS_RULE_WP_POST_TAG_EXPRESSION_APPEND, $rule_value, $condId);
                break;

            case 37: // Continent Country Template
            case 38: // Country Flags Template
            case 39: // Continent Template
            case 40: // Country Template
            case 42: // City Template
            case 43: // Region or state Template
                $eVal = RulesEngineUtil::get_dynamic_rule_expression(BIS_RULE_TEMP_EXPRESSION_APPEND, $rule_value, $condId);
                break;
            
            case 41: // Search Shortcode
                $eVal = RulesEngineUtil::get_dynamic_rule_expression(BIS_RULE_SEARCH_EXPRESSION_APPEND, $rule_value, $condId);
                break;

            case 2000: // Product Attributes
                $eVal = RulesEngineUtil::get_dynamic_rule_expression(BIS_RULE_WOO_PRODUCT_ATTRIBUTE_EXPRESSION_APPEND, $rule_value, $condId);
                break;
        } // End of sub_opt_id switch


        return $eVal;
    }

    /**
     * This method is used to evaluate mobile rule.
     *
     * @param $p_rule_value
     * @return bool|int
     */
    private function evaluate_mobile_rule($p_rule_value) {

        $uagent_info = new uagent_info();
        $rule_value = (int) $p_rule_value;
        $eVal = false;

        switch ($rule_value) {

            case 1: //"IOS"
                $eVal = $uagent_info->DetectIos();
                break;

            case 2: //"Andriod"
                $eVal = $uagent_info->DetectAndroid();
                break;

            case 3: //"Windows"
                $eVal = $uagent_info->DetectWindowsPhone();
                break;

            case 4: //"iPhone"
                $eVal = $uagent_info->DetectIphone();
                break;

            case 5: //"Andriod_Phone"
                $eVal = $uagent_info->DetectAndroidPhone();
                break;

            case 6: //"BlackBerry_Phone"
                $eVal = $uagent_info->DetectBlackBerry10Phone();
                break;

            case 7: // iPad
                $eVal = $uagent_info->DetectIpad();
                break;

            case 8: //"Andriod_Tablet"
                $eVal = $uagent_info->DetectAndroidTablet();
                break;

            case 9: //BlackBerry Tablet
                $eVal = $uagent_info->DetectBlackBerryTablet();
                break;

            case 10: //"HP_TouchPad":
                $eVal = $uagent_info->DetectWebOSTablet();
                break;

            case 29: //"Windows_Phone":
                $eVal = $uagent_info->DetectWindowsPhone();
                break;

            case 26: //"Mobile":
                $eVal = $uagent_info->DetectMobileQuick();
                break;

            case 28: //"Smart_Phone":
                $eVal = $uagent_info->DetectSmartphone();
                break;

            case 27: //"Tablet":
                $eVal = $uagent_info->DetectTierTablet();
                break;
        } // End of rule_value Mobile switch

        return $eVal;
    }

    /**
     * This method is used to evaluate expression.
     *
     * @param $expression
     * @return bool
     */
    public function evaluate_expression($expression) {
        $tokens = str_split($expression);

        // Stack for values
        $values = new RulesStack ();

        // Stack for operators
        $ops = new RulesStack ();

        for ($i = 0; $i < count($tokens); $i++) {
            // Current token is a whitespace, skip it
            if ($tokens [$i] == ' ')
                continue;

            // Current token is a number, push it to stack for numbers
            if ($tokens [$i] == 'T' || $tokens [$i] == 'F') {
                $value = false;

                if ($tokens [$i] == 'T') {
                    $value = true;
                }
                $values->push($value);
            }            // Current token is an opening brace, push it to 'ops'
            else if ($tokens [$i] == '(')
                $ops->push($tokens [$i]);

            // Closing brace encountered, solve entire brace
            else if ($tokens [$i] == ')') {

                while (!$ops->isEmpty() && $ops->peek() != '('){

                    $values->push($this->applyOp($ops->pop(), $values->pop(), $values->pop()));
                }
                if (!$ops->isEmpty()) {
                    $ops->pop();
                }
            } // Current token is an operator.
            else if ($tokens [$i] == '+' || $tokens [$i] == '-') {
                // While top of 'ops' has same or greater precedence to current
                // token, which is an operator. Apply operator on top of 'ops'
                // to top two elements in values stack
                while (!$ops->isEmpty() && $this->hasPrecedence($tokens [$i], $ops->peek()))
                    $values->push($this->applyOp($ops->pop(), $values->pop(), $values->pop()));

                // Push current token to 'ops'.
                $ops->push($tokens [$i]);
            }
        }

        // Entire expression has been parsed at this point, apply remaining
        // ops to remaining values
        $final_value = 0;
        while (!$ops->isEmpty()) {

            $first_val = $values->pop();

            if (!$values->isEmpty()) {
                $second_val = $values->pop();
                $values->push($this->applyOp($ops->pop(), $first_val, $second_val));
            } else {

                // Top of 'values' contains result, return it
                if (!$values->isEmpty()) {
                    $final_value = $values->pop();
                }
                if ($final_value == 1) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        // Top of 'values' contains result, return it
        if (!$values->isEmpty()) {
            $final_value = $values->pop();
        }


        if ($final_value == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * This method is used to evaluate operators.
     *
     * @param $op
     * @param $b
     * @param $a
     * @return bool
     */
    public function applyOp($op, $b, $a) {
        // + = AND
        // - = OR
        if ($op == "+") {
            return $a && $b;
        } else {
            return $a || $b;
        }
    }

    /**
     * This method is used to evaluate the precedence of operator.
     * @param $op1
     * @param $op2
     * @return bool
     */
    public function hasPrecedence($op1, $op2) {
        if ($op2 == '(' || $op2 == ')')
            return false;
        // Current expression donot contains * or / below code never executes
        if (($op1 == '*' || $op1 == '/') && ($op2 == '+' || $op2 == '-'))
            return false;
        else
            return true;
    }

    /**
     * Stores all the applicable rules with rule Name and registers the hook name.
     *
     * @param $logical_rule
     */
    public function register_and_store_hook($logical_rule) {

        // For session rules expression will be null
        // Request rules hooks will be called at request time when rules are evaluated.
        if ($logical_rule->expression == null) {
            $this->call_hook($logical_rule);
        }

        $bis_rules_array = array();

        if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_RULES_ARRAY)) {
            $bis_rules_array = RulesEngineCacheWrapper::get_session_attribute(BIS_RULES_ARRAY);
        }

        $bis_request_rules_array = array();

        if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_REQUEST_RULES_ARRAY)) {
            $bis_request_rules_array = RulesEngineCacheWrapper::get_session_attribute(BIS_REQUEST_RULES_ARRAY);
        }

        // Separating request rules from session rules
        if ($logical_rule->expression == null && (in_array($logical_rule, $bis_rules_array) === FALSE)) {
            array_push($bis_rules_array, $logical_rule);
            RulesEngineCacheWrapper::set_session_attribute(BIS_RULES_ARRAY, $bis_rules_array);
        } elseif (!in_array($logical_rule, $bis_request_rules_array)) {
            array_push($bis_request_rules_array, $logical_rule);
            RulesEngineCacheWrapper::
            set_session_attribute(BIS_REQUEST_RULES_ARRAY, $bis_request_rules_array);
        }

        return $bis_rules_array;
    }

    public function bis_reset_rules_engine_cache_login($user_login, $user) {


        if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_AUDIT_INFO)) {
            $auditVO = RulesEngineCacheWrapper::get_session_attribute(BIS_AUDIT_INFO);
            $auditVO->setUser($user);
            AnalyticsEngineModel::audit_user_login($auditVO);
        }

        RulesEngineCacheWrapper::set_reset_time(BIS_LOGICAL_RULE_RESET);

        $this->bis_end_session();
    }

    public function bis_end_session() {
        RulesEngineCacheWrapper::set_session_attribute(BIS_SESSION_INITIATED, false);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_RULES_ARRAY);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_REQUEST_RULES_ARRAY);
        RulesEngineCacheWrapper::destroy_session();
    }

    public function bis_reset_rules_engine_cache_Logout() {

        $auditVO = null;

        if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_AUDIT_INFO)) {
            $auditVO = RulesEngineCacheWrapper::get_session_attribute(BIS_AUDIT_INFO);
            $auditVO->setUser(null);
            AnalyticsEngineModel::audit_user_logout($auditVO->getReportAuthId());
        }

        RulesEngineCacheWrapper::set_reset_time(BIS_LOGICAL_RULE_RESET);
        $this->bis_end_session();
    }

    function bis_evaluate_request_rules($query, \bis\repf\vo\CacheVO $cacheVO = null) {

        global $wp_query;
        $current_page_id = NULL;
        $woo_category_id = NULL;
        $referer_path = NULL;
        $is404 = FALSE;
        $isAjaxRequest = false;
        $current_category = NULL;
        $current_page_url = null;

        if ($cacheVO != NULL) {
            $current_page_id = $cacheVO->getPostId();

            if ($cacheVO->getReferralUrl() != NULL) {
                $referer_path = $cacheVO->getReferralUrl();
            }

            if ($cacheVO->getCategoryId() != NULL) {
                $current_category = array($cacheVO->getCategoryId());
            }

            if ($cacheVO->getPageUrl() != NULL) {
                $current_page_url = $cacheVO->getPageUrl();
            }

            $isAjaxRequest = $cacheVO->isAjaxRequest();
            $is404 = $cacheVO->is404();
        } else {
            if ($current_page_id == NULL) {
                $current_page_id = $this->get_the_ID();
            }
            $is404 = is_404();

            if ($current_page_id === FALSE) {
                return $query;
            }

            if (RulesEngineUtil::is_woocommerce_installed() &&
                    $current_page_id !== FALSE && is_shop()) {
                $current_page_id = (int) get_option('woocommerce_shop_page_id');
            }
            $referer_path = wp_get_referer();

            $current_category = get_the_category();
            // get the query object
            $cat_obj = $wp_query->get_queried_object();
            $woo_category_id = null;

            if (RulesEngineUtil::is_woocommerce_installed() && $cat_obj) {

                $cat = get_query_var('cat');
                $category = get_category($cat);

                if (isset($cat_obj->term_id)) {
                    $woo_category_id = $cat_obj->term_id;
                }
            }
        }
        $applied_rules = $this->get_request_rules();

        if ($applied_rules != null && !empty($applied_rules)) {

            foreach ($applied_rules as $applied_rule) {

                if (RulesEngineUtil::isContains($applied_rule->expression, "X")) {

                    $expression = explode(" ", $applied_rule->expression);
                    // Cookie rule
                    foreach ($expression as $key => $value) {
                        $rule_values = explode("$", $value);

                        if (RulesEngineUtil::isContains($value, BIS_RULE_COOKIE_EXPRESSION_APPEND)) {

                            if (RulesEngineUtil::is_reset_rule()) {
                                $eval = "F";
                            } else {
                                $cookie = '';
                                $rule_value = $rule_values[1];
                                $condId = (int) $rule_values[2];

                                if ($condId == 10 || $condId == 11) {
                                    $cookie_val = '';
                                    $cookie_key = $rule_values[1];
                                } else {
                                    if (RulesEngineUtil::isContains($rule_values[1], "=")) {
                                        $cookie_data = explode("=", $rule_values[1]);
                                        $cookie_key = $cookie_data[0];
                                        $cookie_val = $cookie_data[1];
                                    } else {
                                        $cookie_key = $rule_values[1];
                                        $cookie_val = '';
                                    }
                                }

                                if (isset($_COOKIE[$cookie_key])) {
                                    $cookie = $_COOKIE[$cookie_key];
                                }

                                $eval = RulesEngineUtil::evaluateStringTypeRule($cookie, $cookie_val, $condId);

                                if ($eval) {
                                    $eval = "T";

                                    if ($applied_rule->eval_type == "2") {
                                        $applied_rule->expression = TRUE;
                                    }
                                } else {
                                    $eval = "F";
                                }
                            }

                            $expression[$key] = $eval;

                            // Cookie expresssion end   
                        } else if (RulesEngineUtil::isContains($value, BIS_RULE_FORM_DATA_EXPRESSION_APPEND) && (count($rule_values) > 2)) {

                            // Parameter Rules
                            $rule_value = $rule_values[1];
                            $condId = (int) $rule_values[2];

                            $eval = RulesEngineUtil::evaluateFormDataRules($rule_value, $condId);

                            if ($eval) {
                                $eval = "T";

                                if ($applied_rule->eval_type == "2") {
                                    $applied_rule->expression = TRUE;
                                }
                                if (isset($_POST[BIS_COUNTRY_SELECT])) {
                                    $country = $_POST[BIS_COUNTRY_SELECT];
                                    setcookie(BIS_COUNTRY_SELECT, $country, time() + BIS_COOKIE_EXPIRE_TIME, COOKIEPATH, COOKIE_DOMAIN);
                                }
                            } else {
                                $eval = "F";
                            }

                            $expression[$key] = $eval;

                            // Param expression expresssion    
                        } else if (RulesEngineUtil::isContains($value, BIS_RULE_PARAM_EXPRESSION_APPEND)) {

                            $rule_value = $rule_values[1];
                            $condId = (int) $rule_values[2];
                            $eval = RulesEngineUtil::evaluateParameterRules($rule_value, $condId, $isAjaxRequest);

                            if ($eval) {
                                $eval = "T";

                                if ($applied_rule->eval_type == "2") {
                                    $applied_rule->expression = TRUE;
                                }
                            } else {
                                $eval = "F";
                            }


                            $expression[$key] = $eval;
                            
                            // Template value rule expression
                        } else if(RulesEngineUtil::isContains($value, BIS_RULE_TEMP_EXPRESSION_APPEND)) {
                            
                            $rule_value = $rule_values[1];
                            $condId = (int) $rule_values[2];
                            $rule_value_str = "bis_select_place=" . $rule_value;

                            $eval = RulesEngineUtil::evaluateFormDataRules($rule_value_str, $condId);

                            if ($eval) {
                                $eval = "T";

                                if ($applied_rule->eval_type == "2") {
                                    $applied_rule->expression = TRUE;
                                }
                            } else {
                                $eval = "F";
                            }

                            $expression[$key] = $eval;

                            // Search Shortcode rule expresssion    
                        } else if (RulesEngineUtil::isContains($value, BIS_RULE_SEARCH_EXPRESSION_APPEND)) {

                            $rule_value = $rule_values[1];
                            $condId = (int) $rule_values[2];

                            $eval = RulesEngineUtil::evaluateSearchFormDataRules($rule_value, $condId);

                            if ($eval) {
                                $eval = "T";

                                if ($applied_rule->eval_type == "2") {
                                    $applied_rule->expression = TRUE;
                                }
                            } else {
                                $eval = "F";
                            }

                            $expression[$key] = $eval;

                            // Page rule expresssion    
                        } else if ($referer_path &&
                                RulesEngineUtil::isContains($value, BIS_RULE_REFERRED_PATH_EXPRESSION_APPEND)) {

                            $rule_value = $rule_values[1];
                            $condId = (int) $rule_values[2];
                            $eval = RulesEngineUtil::evaluateURLTypeRule(array($referer_path), $rule_value, $condId, $applied_rule);

                            if ($eval) {
                                $eval = "T";

                                if ($applied_rule->eval_type == "2") {
                                    $applied_rule->expression = TRUE;
                                }
                            } else {
                                $eval = "F";
                            }

                            $expression[$key] = $eval;

                            // Page rule expresssion    
                        } else if (RulesEngineUtil::isContains($value, BIS_RULE_PAGE_EXPRESSION_APPEND)) {
                            $rule_values = explode("$", $value);
                            $rule_page_id = (int) $rule_values[1];
                            $condId = (int) $rule_values[2];

                            $eval = RulesEngineUtil::evaluateIntTypeRule($current_page_id, $rule_page_id, $condId);

                            if ($eval) {
                                $eval = "T";

                                if ($applied_rule->eval_type == "2") {
                                    $applied_rule->expression = TRUE;
                                }
                            } else {
                                $eval = "F";
                            }

                            $expression[$key] = $eval;
                            // Posts rule 
                        } else if (RulesEngineUtil::isContains($value, BIS_RULE_POST_EXPRESSION_APPEND)) {
                            $rule_values = explode("$", $value);
                            $rule_page_id = (int) $rule_values[1];
                            $condId = (int) $rule_values[2];

                            $eval = RulesEngineUtil::evaluateIntTypeRule($current_page_id, $rule_page_id, $condId);

                            if ($eval) {
                                $eval = "T";

                                if ($applied_rule->eval_type == "2") {
                                    $applied_rule->expression = TRUE;
                                }
                            } else {
                                $eval = "F";
                            }

                            $expression[$key] = $eval;
                            // Category 
                        } else if (RulesEngineUtil::isContains($value, BIS_RULE_CATEGORY_EXPRESSION_APPEND)) {
                            $rule_values = explode("$", $value);
                            $rule_cat_id = (int) $rule_values[1];
                            $condId = (int) $rule_values[2];
                            $is_woo_cat = false;

                            if ($woo_category_id != null) {
                                $current_category = array($woo_category_id);
                                $is_woo_cat = true;
                            }

                            $eval = RulesEngineUtil::evaluateCategoryArrayTypeRule($current_category, $rule_cat_id, $condId, $is_woo_cat, $isAjaxRequest);

                            if ($eval) {
                                $eval = "T";

                                if ($applied_rule->eval_type == "2") {
                                    $applied_rule->expression = TRUE;
                                }
                            } else {
                                $eval = "F";
                            }

                            $expression[$key] = $eval;
                            // Request path 
                        } else if (RulesEngineUtil::isContains($value, BIS_RULE_REFERRAL_PATH_EXPRESSION_APPEND)) {
                            $eval = false;
                            $current_path = RulesEngineUtil::get_current_url($current_page_url);
                            if ($current_path != null) {
                                $rule_values = explode("$", $value);
                                $rule_value = $rule_values[1];
                                $condId = (int) $rule_values[2];

                                $eval = RulesEngineUtil::evaluateURLTypeRule($current_path, $rule_value, $condId, $applied_rule);
                            }
                            if ($eval) {
                                $eval = "T";
                                if ($applied_rule->eval_type == "2") {
                                    $applied_rule->expression = TRUE;
                                }
                            } else {
                                $eval = "F";
                            }

                            $expression[$key] = $eval;
                           //status code
                        } else if (RulesEngineUtil::isContains($applied_rule->expression, BIS_RULE_STATUS_EXPRESSION_APPEND)) {

                            $rule_values = explode("$", $value);
                            $rule_status_id = (int) $rule_values[1];
                            $condId = (int) $rule_values[2];
                            $eval = "F";

                            if ($rule_status_id === 21) { // 404
                                if (($is404 === true || $is404 === "true") && $condId === 1) {
                                    $eval = "T";
                                }
                                if (($is404 === false || $is404 === "false") && $condId === 2) {
                                    $eval = "T";
                                }
                            }

                            $expression[$key] = $eval;
                            //Woo tags && Wp tags
                        } elseif (RulesEngineUtil::isContains($value, BIS_RULE_WOO_PRODUCT_TAG_EXPRESSION_APPEND) || RulesEngineUtil::isContains($value, BIS_RULE_WP_POST_TAG_EXPRESSION_APPEND)) {

                            $rule_values = explode("$", $value);
                            $rule_appalied_tag = (int) $rule_values[1];
                            $condId = (int) $rule_values[2];
                            $eval = null;
                            if (isset($current_page_id)) {

                                $tag = 'post_tag';
                                if(RulesEngineUtil::isContains($value, BIS_RULE_WOO_PRODUCT_TAG_EXPRESSION_APPEND) && RulesEngineUtil::is_woocommerce_installed()){
                                    $tag = 'product_tag';
                                }
                                $postid = $current_page_id;
                                $present_post_tags = get_the_terms($postid, $tag);
                                $present_post_tag_ids = array();
                                if(is_array($present_post_tags)){
                                    foreach ($present_post_tags as $present_post_tag) {
                                        $present_post_tag_id = $present_post_tag->term_id;
                                        array_push($present_post_tag_ids, $present_post_tag_id);
                                    }
                                    $eval = RulesEngineUtil::evaluateArrayTypeRule($present_post_tag_ids, $rule_appalied_tag, $condId);
                                }
                            }
                            if ($eval == true) {
                                $eval = "T";
                            } else {
                                $eval = "F";
                            }
                            $expression[$key] = $eval;
                            //Woo attributes
                        } elseif (RulesEngineUtil::isContains($value, BIS_RULE_WOO_PRODUCT_ATTRIBUTE_EXPRESSION_APPEND)) {

                            $rule_values = explode("$", $value);
                            $rule_appalied_attribute = (int) $rule_values[1];
                            $condId = (int) $rule_values[2];
                            $eval = null;
                            if (isset($current_page_id) && RulesEngineUtil::is_woocommerce_installed()) {
                                $postid = $current_page_id;
                                $post = get_post($postid);
                                if (isset($post->post_type) && $post->post_type == "product") {
                                    $present_product = new \WC_Product($postid);
                                    $attributes = $present_product->get_attributes();
                                    if (is_array($attributes)) {
                                        $attribute_term_ids = array();
                                        foreach ($attributes as $attribute_key => $attribute_value) {
                                            $slug_name = $attribute_key;
                                            if (!(RulesEngineUtil::isContains($attribute_key, "pa"))) {
                                                $slug_name = 'pa_' . $attribute_key;
                                            }
                                            if (isset(wp_get_object_terms($postid, $slug_name)[0]->term_id)) {
                                                $attribute_term_id = wp_get_object_terms($postid, $slug_name)[0]->term_id;
                                                array_push($attribute_term_ids, $attribute_term_id);
                                            }
                                        }
                                        $eval = RulesEngineUtil::evaluateArrayTypeRule($attribute_term_ids, $rule_appalied_attribute, $condId);
                                    }
                                }
                            }
                            if ($eval == true) {
                                $eval = "T";
                            } else {
                                $eval = "F";
                            }
                            $expression[$key] = $eval;
                        }
                        
                    } // End of express for loop


                    $eval_expression = implode(" ", $expression);
                    $applied_rule->eval = $eval_expression;

                    if (!RulesEngineUtil::isContains($eval_expression, "X")) {

                        $eval_expression = $this->evaluate_expression($eval_expression);

                        // If expression is true call hook;
                        if ($eval_expression) {
                            $this->call_hook($applied_rule);
                        }

                        $applied_rule->eval = $eval_expression;
                    } // End of if
                } // End of if
            } // End of applied_rules
        } // End of If 

        if (!empty($applied_rules)) {
            $request_applied_rules = array(BIS_REQUEST_APPLIED_RULES => $applied_rules,
                BIS_IS_AJAX_REQUEST => $isAjaxRequest);
            RulesEngineCacheWrapper::set_session_attribute(BIS_REQUEST_RULES_KEY, $applied_rules);
            do_action('bis_apply_request_rules', $request_applied_rules);
        }

        return $query;
    }

    public function bis_country_name($attrs) {
        $geolocVO = RulesEngineCacheWrapper::get_session_attribute(BIS_GEOLOCATION_VO);
        return $geolocVO->getCountryName();
    }

    public function bis_city_name($attrs) {
        $geolocVO = RulesEngineCacheWrapper::get_session_attribute(BIS_GEOLOCATION_VO);
        return $geolocVO->getCity();
    }

    public function bis_region_name($attrs) {
        $geolocVO = RulesEngineCacheWrapper::get_session_attribute(BIS_GEOLOCATION_VO);
        return $geolocVO->getRegion();
    }

    /**
     * 
     * This method is used to support country dropdown.
     * 
     * @param type $atts
     * @return string
     */
    public function bis_country_selector($atts) {

        $atts = shortcode_atts(
                array(
            'include' => '',
            'exclude' => '',
            'search_box' => '',
            'class' => '',
            'default' => '',
            'eng_label' => ''
                ), $atts, 'bis_country_selector');


        $includes = false;
        $exclude = false;
        $bis_class = '';
        $bis_search_box = 10;
        $eng_label = false;
        $default = false;

        if (!RulesEngineUtil::isNullOrEmptyString($atts['include'])) {
            $includes = RulesEngineUtil::convertToSQLString($atts['include']);
        }

        if (!RulesEngineUtil::isNullOrEmptyString($atts['eng_label'])) {
            $eng_label = $atts['eng_label'];
        }

        if (!RulesEngineUtil::isNullOrEmptyString($atts['search_box'])) {

            if ($atts['search_box'] === "false") {
                $bis_search_box = "Infinity";
            }
        }

        if (!RulesEngineUtil::isNullOrEmptyString($atts['exclude'])) {
            $exclude = RulesEngineUtil::convertToSQLString($atts['exclude']);
        }

        if (!RulesEngineUtil::isNullOrEmptyString($atts['default'])) {
            $default = $atts['default'];
        }

        if (!RulesEngineUtil::isNullOrEmptyString($atts['class'])) {
            $bis_class = "class=\"" . $atts['class'] . "\"";
        }

        $countries = $this->get_countries($includes, $exclude, $eng_label);
        // from drop down.   
        if (isset($_POST[BIS_COUNTRY_SELECT])) {
            $country = $_POST[BIS_COUNTRY_SELECT];
            RulesEngineCacheWrapper::set_session_attribute(BIS_COUNTRY_SELECT, $country);
        } else if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_COUNTRY_SELECT)) {
            // From session
            $country = RulesEngineCacheWrapper::get_session_attribute(BIS_COUNTRY_SELECT);
        } else if (isset($_COOKIE[BIS_COUNTRY_SELECT])) {
            // From cookie
            $country = $_COOKIE[BIS_COUNTRY_SELECT];
        } else if ($default != false) {
            // from default value
            $country = $default;
        } else { // from geo location
            $sessionWrapper = new BISSessionWrapper();
            $geo_plugin = $sessionWrapper->getGeoPlugin();
            $country = $geo_plugin->getCountryCode();
        }

        $bis_country_dropdown = "<form method=\"post\" id=\"bis_country_form\" name=\"bis_country_form\">";
        $bis_country_dropdown = $bis_country_dropdown . "<input type=\"hidden\" id=\"bis_reset_rule\" value=\"true\" name=\"bis_reset_rule\">";
        $bis_country_dropdown = $bis_country_dropdown . "<input type=\"hidden\" id=\"bis_search_box\" value=" . $bis_search_box . " name=\"bis_search_box\">";
        $bis_country_dropdown = $bis_country_dropdown . "<select " . $bis_class . "  name=\"bis_country\" id = \"bis_country\">";

        foreach ($countries as $activeCountry) {
            $selected = "";

            if ($country === $activeCountry->id) {
                $selected = "selected";
            }

            $bis_country_dropdown = $bis_country_dropdown . "<option " . $selected . " value=" . $activeCountry->id . ">"
                    . $activeCountry->name .
                    "</option>";
        }

        $bis_country_dropdown = $bis_country_dropdown . "</select></form>";
        return $bis_country_dropdown;
    }

    public function get_countries($includes, $exclude, $eng_label = false) {
        $countries = null;


        /*if (RulesEngineCacheWrapper::is_session_attribute_set(BIS_USER_COUNTRY_DROPDOWN)) {
            $countries = RulesEngineCacheWrapper::get_session_attribute(BIS_USER_COUNTRY_DROPDOWN);
            return $countries;
        }*/

        $logicalRulesEngineModal = new LogicalRulesEngineModel();
        $countries = $logicalRulesEngineModal->get_countries($includes, $exclude);


        if ($eng_label !== "true" || $eng_label === "only") {
            foreach ($countries as $activeCountry) {
                if (RulesEngineUtil::isContains($activeCountry->name, "-")) {
                    $countries_name = explode("-", $activeCountry->name);
                    if ($eng_label === "only") {
                        $activeCountry->name = $countries_name[0];
                    } else {
                        $activeCountry->name = $countries_name[1];
                    }
                }
            }
        }

        RulesEngineCacheWrapper::set_session_attribute(BIS_USER_COUNTRY_DROPDOWN, $countries);

        return $countries;
    }

    function bis_re_append_shop_info() {
        $content = "";
        echo $this->bis_re_append_page_info($content);
    }

    public function bis_re_append_page_info($content) {

        global $wp_query;
        
        if (RulesEngineUtil::is_ajax_request() == true ||  RulesEngineUtil::isContains($content, "bis_re_cache_post_id") == true) {
            return $content;
        }

        $current_page_id = $this->get_the_ID();

        if (RulesEngineUtil::is_woocommerce_installed() &&
                $current_page_id !== FALSE && is_shop()) {
            $current_page_id = (int) get_option('woocommerce_shop_page_id');
        }

        $referer_path = wp_get_referer();
        $current_category = get_the_category();

        $str_categories = RulesEngineUtil::getCommaSeperatedCategories($current_category);
        $woo_category_id = null;
        $woo_tag_id = null;

        $current_wp_tag_id = null;
        $current_post_tag = get_the_terms($current_page_id, 'post_tag');

        if (!empty($current_post_tag)) {
            $current_wp_tag_id = $current_post_tag[0]->slug;
        }
        
        if (RulesEngineUtil::is_woocommerce_installed()) {
            
            // get the query object
            $cat_obj = $wp_query->get_queried_object();

            if ($cat_obj) {
                $cat = get_query_var('cat');

                if (isset($cat_obj->term_id)) {
                    $woo_category_id = $cat_obj->term_id;
                    if ($woo_category_id != null) {
                        $str_categories = $woo_category_id;
                    }
                }
            }

            $product_tags = get_the_terms($current_page_id, 'product_tag');

            if (!empty($product_tags) && !is_wp_error($product_tags)) {
                $woo_tag_id = $product_tags[0]->slug;
            }
        } else {
            if (isset($str_categories) && !empty($str_categories)) {
                $current_category = $str_categories;
            }
        }

        if (RulesEngineUtil::get_option(BIS_RULES_ENGINE_CACHE_INSTALLED, false, false) == "true") {
            $applied_posts_rules = array();
            $applied_product_rules = array();
            $applied_pages_rules = array();
            // Apply rules for page for cached sites
            if (class_exists('BIS_Post_Controller')) {
                $post_rule_modal = new PostRulesEngineModel();
                $applied_posts_rules = $post_rule_modal->get_applied_plugin_rules(BIS_POST_TYPE_RULE);
                if ($applied_posts_rules == null) {
                    $applied_posts_rules = array();
                }
            }
            if (class_exists('BIS_Woo_Product_Controller')) {
                $post_rule_modal = new PostRulesEngineModel();
                $applied_product_rules = $post_rule_modal->get_applied_plugin_rules(BIS_WOO_PRODUCT_TYPE_RULE);
                if (!empty($applied_product_rules)) {
                    $applied_posts_rules = array_merge($applied_posts_rules, $applied_product_rules);
                }
                if ($applied_posts_rules == null) {
                    $applied_posts_rules = $applied_product_rules;
                }
                if ($applied_posts_rules == null) {
                    $applied_posts_rules = array();
                }
            }
            if (class_exists('BIS_Page_Controller')) {
                $page_rule_modal = new PageRulesEngineModel();
                $applied_pages_rules = $page_rule_modal->get_applied_plugin_rules(BIS_PAGE_TYPE_RULE);
                if (!empty($applied_pages_rules)) {
                    $applied_posts_rules = array_merge($applied_posts_rules, $applied_pages_rules);
                }
                if ($applied_posts_rules == null) {
                    $applied_posts_rules = $applied_pages_rules;
                }
            }
            
        }

        $bis_content_bottom = "";
        $bis_content_top = "";
        $bis_popup_id = "";
        $bis_shortcode_data = "";
        if ($current_page_id != null && !empty($applied_posts_rules)) {
            foreach ($applied_posts_rules as $applied_posts_rule) {
                if ($applied_posts_rule->parent_id == $current_page_id && $applied_posts_rule->gencol2 != null) {
                    $content_array = $this->bis_append_content_post($applied_posts_rule);
                    $bis_content_bottom = $content_array[0] . $bis_content_bottom;
                    $bis_content_top = $content_array[1] . $bis_content_top;
                    $bis_popup_id = $content_array[2] . $bis_popup_id;
                    $bis_shortcode_data = $content_array[3] . $bis_shortcode_data;
                } elseif ($applied_posts_rule->gencol2 != null) {
                    switch ($applied_posts_rule->parent_type_value) {
                        case 'wp-tags':
                            $post_ids = array();
                            $args = array(
                                'post_type' => 'post',
                                'tag' => $applied_posts_rule->parent_id
                            );
                            $wp_obj = new \WP_Query($args);
                            while ($wp_obj->have_posts()) {
                                $wp_obj->the_post();
                                $post_id = $wp_obj->post->ID;
                                array_push($post_ids, $post_id);
                            }
                            if (in_array($current_page_id, $post_ids)) {
                                $content_array = $this->bis_append_content_post($applied_posts_rule);
                                $bis_content_bottom = $content_array[0] . $bis_content_bottom;
                                $bis_content_top = $content_array[1] . $bis_content_top;
                                $bis_popup_id = $content_array[2] . $bis_popup_id;
                            }
                            break;
                        case 'wp-categories':
                            $args = array('category' => $applied_posts_rule->parent_id, 'post_type' => 'post');
                            $postslist = get_posts($args);
                            $post_ids = array();
                            foreach ($postslist as $post) {
                                array_push($post_ids, $post->ID);
                            }
                            if (in_array($current_page_id, $post_ids)) {
                                $content_array = $this->bis_append_content_post($applied_posts_rule);
                                $bis_content_bottom = $content_array[0] . $bis_content_bottom;
                                $bis_content_top = $content_array[1] . $bis_content_top;
                                $bis_popup_id = $content_array[2] . $bis_popup_id;
                            }
                            break;
                    }
                }
            }
        }

        $hidden_page_id = '<input type="hidden" name="bis_re_cache_post_id" 
        id="bis_re_cache_post_id" value="' . $current_page_id . '" />';

        $hidden_wp_tag_id = '<input type="hidden" name="bis_re_cache_post_tag_id" 
        id="bis_re_cache_post_tag_id" value="' . $current_wp_tag_id . '" />';

        $hidden_woo_tag_id = '<input type="hidden" name="bis_re_cache_product_tag_id" 
        id="bis_re_cache_product_tag_id" value="' . $woo_tag_id . '" />';

        $hidden_cat_id = '<input type="hidden" name="bis_re_cache_cat_id" 
        id="bis_re_cache_cat_id" value="' . $str_categories . '" />';

        $hidden_refere_path = '<input type="hidden" name="bis_re_cache_reffer_path" 
        id="bis_re_cache_reffer_path" value="' . $referer_path . '" />';

        $hidden_site_url = '<input type="hidden" name="bis_re_site_url" 
        id="bis_re_site_url" value="' . get_site_url() . '" />';

        $url = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];


        $hidden_page_url = '<input type="hidden" name="bis_re_cache_page_url" 
        id="bis_re_cache_page_url" value="' . $url . '" />';

        $content = $bis_content_top . $content . $bis_popup_id . $hidden_page_id . $hidden_wp_tag_id . $hidden_woo_tag_id . $hidden_cat_id .
                $hidden_refere_path . $hidden_site_url . $hidden_page_url . $bis_content_bottom . $bis_shortcode_data;

        return $content;
    }

    public function bis_append_content_post($applied_posts_rule) {

        $content_array = array();
        $bis_content_bottom = "";
        $bis_content_top = "";
        $bis_popup_id = "";
        $bis_shortcode_data = "";
        $dynamic_content = "";
        $content_position = json_decode($applied_posts_rule->gencol2)->content_position;
        switch ($content_position) {
            case "pos_bottom_post":
            case "pos_bottom_page":
                if ($applied_posts_rule->action == "append_image_bg_post" || $applied_posts_rule->action == "append_image_bg_page") {
                    $dynamic_content = RulesDynamicContent::get_dynamic_content($applied_posts_rule);
                    $bis_content_bottom = '<span id="bis_re_cbottom_' . $applied_posts_rule->lrId . '" style="display:none">' . $dynamic_content . '</span>' . $bis_content_bottom;
                } elseif ($applied_posts_rule->action == "append_image_post" || $applied_posts_rule->action == "append_image_page") {
                    $dynamic_content = RulesDynamicContent::get_dynamic_content($applied_posts_rule);
                    $bis_content_bottom = '<span id="bis_re_cbottom_' . $applied_posts_rule->lrId . '" style="display:none">' . $dynamic_content . '</span>' . $bis_content_bottom;
                } else {
                    $content_bottom_data = $applied_posts_rule->gencol1;
                    $bis_content_bottom = '<span id="bis_re_cbottom_' . $applied_posts_rule->lrId . '" style="display:none">' . $content_bottom_data . '</span>' . $bis_content_bottom;
                }
                break;
            case "pos_top_post":
            case "pos_top_page":
                if ($applied_posts_rule->action == "append_image_bg_post" || $applied_posts_rule->action == "append_image_bg_page") {
                    $dynamic_content = RulesDynamicContent::get_dynamic_content($applied_posts_rule);
                    $bis_content_top = '<span id="bis_re_ctop_' . $applied_posts_rule->lrId . '" style="display:none">' . $dynamic_content . '</span>' . $bis_content_top;
                } elseif ($applied_posts_rule->action == "append_image_post" || $applied_posts_rule->action == "append_image_page") {
                    $dynamic_content = RulesDynamicContent::get_dynamic_content($applied_posts_rule);
                    $bis_content_top = '<span id="bis_re_ctop_' . $applied_posts_rule->lrId . '" style="display:none">' . $dynamic_content . '</span>' . $bis_content_top;
                } else {
                    $content_top_data = $applied_posts_rule->gencol1;
                    $bis_content_top = '<span  id="bis_re_ctop_' . $applied_posts_rule->lrId . '" style="display:none">' . $content_top_data . '</span>' . $bis_content_top;
                }
                break;
            case "pos_dialog_post":
            case "pos_dialog_page":
                $popup_data = RulesDynamicContent::get_model_dialog($applied_posts_rule);
                $bis_popup_id = '<span id="bis_re_cpopup_' . $applied_posts_rule->lrId . '" style="display:none">' . $popup_data . '</span>' . $bis_popup_id;
                break;
            case "pos_cust_scode_post":
            case "pos_cust_scode_page":
                if ($applied_posts_rule->action === "append_existing_scode_page") {
                    $short_content = do_shortcode(stripslashes($applied_posts_rule->gencol1));
                    $bis_shortcode_data = '<span id="bis_re_cshortcode_' . $applied_posts_rule->lrId . '" style="display:none">' . $short_content . '</span>' . $bis_shortcode_data;
                } else {
                    $short_content = RulesDynamicContent::get_dynamic_content($applied_posts_rule);
                    $bis_shortcode_data = '<span id="bis_re_cshortcode_' . $applied_posts_rule->lrId . '" style="display:none">' . $short_content . '</span>' . $bis_shortcode_data;
                }
                break;
        }
        array_push($content_array, $bis_content_bottom);
        array_push($content_array, $bis_content_top);
        array_push($content_array, $bis_popup_id);
        array_push($content_array, $bis_shortcode_data);

        return $content_array;
    }

    public static function bis_change_country_hook() {
        ?>

        <script>
            jQuery(function () {

                function template(state, container) {

                    if (!state.id) {
                        return state.text;
                    }

                    var cid = state.id;

                    var $state = jQuery(
                            '<span class="flag-icon flag-icon-' + cid.toLowerCase() + ' flag-icon-squared"></span> <span class="flag-text">' + state.text + '</span>'
                            );
                    return $state;
                }

                var minResultsSearch = jQuery("#bis_search_box").val();

                if (jQuery("#bis_country").attr("class") != null) {
                    var selectClass = jQuery("#bis_country").attr("class");

                    jQuery('#bis_country').select2({
                        dropdownCssClass: selectClass,
                        minimumResultsForSearch: minResultsSearch,
                        templateSelection: template,
                        templateResult: template
                    });
                } else {
                    jQuery('#bis_country').select2({
                        minimumResultsForSearch: minResultsSearch,
                        templateSelection: template,
                        templateResult: template
                    });
                }

                jQuery("#bis_country").change(function () {
                    jQuery("#bis_country_form").attr("action", window.location);
                    jQuery("#bis_country_form").submit();
                });
            });
        </script>
        <?php
    }

    public function bis_content_filter_to_editor() {
        global $typenow;
        $bis_addons = RulesEngineUtil::get_option(BIS_RULESENGINE_ADDONS_CONST);
        $bis_rules = json_decode($bis_addons);
        if (!empty($bis_rules)) {
            foreach ($bis_rules as $bis_rule) {
                if ($bis_rule->status == '1' && $bis_rule->id == 'bis_page_plugin') {
                    $page_ver = true;
                } elseif ($bis_rule->status == '1' && $bis_rule->id == 'bis_posts_plugin') {
                    $post_ver = true;
                } elseif ($bis_rule->status == '1' && $bis_rule->id == 'bis_products_plugin') {
                    $product_ver = true;
                }
            }
        }
        if (isset($page_ver) && $typenow == 'page' || isset($post_ver) && $typenow != 'page' || isset($product_ver) && $typenow != 'page') {
            if (get_user_option('rich_editing') == 'true') {
                add_filter('mce_external_plugins', array($this, 'bis_add_tiny_mce_plugin'));
                add_filter('mce_buttons', array($this, 'bis_register_content_filter'));
            }
        }
    }

    public function bis_add_tiny_mce_plugin($plugin_array) {
        $plugin_name = RulesEngineUtil::get_option(BIS_RULES_ENGINE_PLATFORM_DIR);
        $plugin_array['mega_rules_engine_icon'] = plugins_url('/' . $plugin_name . '/js/bis-custom-tiny-mce.js');
        return $plugin_array;
    }

    public function bis_register_content_filter($buttons) {
        array_push($buttons, "mega_rules_engine_icon");
        return $buttons;
    }

    public function bis_logical_rules_tinymce() {
        global $pagenow;
        if ($pagenow != 'admin.php') {
            $ajaxurl = admin_url('admin-ajax.php');
            wp_nonce_field('bis_rules_engine_nonce', 'bis_rules_engine_nonce');
            ?>
            <script type="text/javascript">
                var ajaxurl = "<?php echo $ajaxurl; ?>";
                jQuery(document).ready(function () {
                    var data = {
                        action: 'bis_logical_rules_popup_tinymce',
                        bis_nonce: jQuery("#bis_rules_engine_nonce").val()
                    };
                    jQuery.post(ajaxurl, data, function (response) {
                        if (response === '-1') {
                            console.log('error');
                        } else {
                            if (typeof (tinyMCE) != 'undefined') {
                                if (tinyMCE.activeEditor != null) {
                                    localStorage.setItem('bis-log-rules', JSON.stringify(response));
                                }
                            }
                        }
                    });
                });
            </script>
            <?php
        }
    }

    public function bis_rules_if_shortcode($atts, $content = null) {
        RulesEngineCacheWrapper::delete_all_transient_cache();
        $rulename = $atts['rule'];
        $valid = RulesEngineCommon::is_rule_valid($rulename);
        RulesEngineCacheWrapper::set_session_attribute(BIS_CHECK_IF_SHORTCODE, $valid);
        if ($valid) {
            return do_shortcode($content);
        }
    }

    public function bis_rules_else_shortcode($atts, $content = null) {
        RulesEngineCacheWrapper::delete_all_transient_cache();
        $valid = RulesEngineCacheWrapper::get_session_attribute(BIS_CHECK_IF_SHORTCODE);
        if ($valid) {
            return '';
        } else {
            return do_shortcode($content);
        }
    }
    
    public function admin_rule_evaluation($bis_criteria){
        global $wp_query;
        $bis_sub_opt[0] = $bis_criteria[0];
        $bis_re_sub_option = $bis_criteria[0];
        $bis_re_condition= 12;
        $bis_re_rule_value[0] = $bis_criteria[2];
        $sub_criteria = explode("_", $bis_criteria[0])[1];

        $value_type_id = "1";
        if ($sub_criteria === "9" || $sub_criteria === "14" || $sub_criteria === "3" || $sub_criteria === "10") {
            $value_type_id = "3";
            $bis_re_condition = 1;
        } else if ($sub_criteria === "6" || $sub_criteria === "15" || $sub_criteria === "35" || $sub_criteria === "27" || $sub_criteria === "26" || $sub_criteria === "31") {
            $value_type_id = "4";
            $bis_re_condition = 1;
        } else if ($sub_criteria === "19" || $sub_criteria === "32" || $sub_criteria === "16" || $sub_criteria === "11" || $sub_criteria === "12" || $sub_criteria === "13" || $sub_criteria === "33" || $sub_criteria === "7" || $sub_criteria === "28") {
            $value_type_id = "2";
            $bis_re_condition = 1;
            $bis_re_rule_value[0] = $bis_criteria[1];
        }
        $rule_value = $bis_re_rule_value[0];
        $sub_opt_id = (int) $sub_criteria;
        $condId = $bis_re_condition;
        
        $geo_plugin = RulesEngineCacheWrapper::get_session_attribute(BIS_GEOLOCATION_VO);

        if ($geo_plugin == null) {
            $this->set_geolocation();
            $geo_plugin = RulesEngineCacheWrapper::get_session_attribute(BIS_GEOLOCATION_VO);
        }
        switch ($sub_opt_id) {

            case 1:    //User Role
                $eVal = true;
                if (is_user_logged_in()) {
                    $user = wp_get_current_user();
                    if ($user->ID != 0) {
                        foreach ($user->roles as $roleName) {

                            $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $roleName, $condId);
                            if ($eVal) {
                                break;
                            }
                        }
                    }
                } else {
                    $roleName = "bis_guest";
                    $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $roleName, $condId);
                }

                break;

            case 2: //emailId Rule
                $eVal = true;
                if (is_user_logged_in()) {
                    $logged_in_user = wp_get_current_user();
                    if ($logged_in_user->ID != 0) {
                        $email = $logged_in_user->user_email;
                        if ($condId == 1 || $condId == 2 || $condId == 12 || $condId == 14) {
                            $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $email, $condId);
                        } else {
                            $eVal = RulesEngineUtil::evaluateStringTypeRule($email, $rule_value, $condId);
                        }
                    }
                }
                break;

            case 3: // Registered date
                $eVal = true;
                if (is_user_logged_in()) {
                    $logged_in_user = wp_get_current_user();
                    if ($logged_in_user->ID != 0) {
                        $reg_date = $logged_in_user->user_registered;
                        $reg_date = date_format(date_create($reg_date), "Y-m-d");
                        $eVal = RulesEngineUtil::evaluateDateTypeRule($reg_date, $rule_value, $condId);
                    }
                }
                break;

            case 4: // Country
                $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $geo_plugin->getCountryCode(), $condId);
                break;

            case 5: // Currency
                $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $geo_plugin->getCurrencyCode(), $condId);
                break;

            case 6: // URL
                $current_path = RulesEngineUtil::get_current_url();
                $eVal = RulesEngineUtil::evaluateURLTypeRule($current_path, $rule_value, $condId);
                break;

            case 7: // language
                $eVal = RulesEngineUtil::evaluateLanguageRule($rule_value, $condId);
                break;

            case 8: // Browser
                $eVal = RulesEngineUtil::evaluate_browser_rule($rule_value, $condId);
                break;

            case 9: // Date
                // Get the current date
                $current_date = date("Y-m-d");
                $eVal = RulesEngineUtil::evaluateDateTypeRule($current_date, $rule_value, $condId);
                break;

            case 10: // Time
                $current_time = date('H:i', current_time('timestamp', 0));
                $t1 = strtotime($current_time);
                $t2 = strtotime($rule_value);
                $eVal = RulesEngineUtil::evaluateIntTypeRule($t1, $t2, $condId);
                break;

            case 14: // Date and Time rules should be evaluated at runtime.
                // These value will always true.
                // Get the current date
                $current_date = date('Y-m-d H:i', current_time('timestamp', 0));
                $d1 = strtotime($current_date);
                $d2 = strtotime($rule_value);
                $eVal = RulesEngineUtil::evaluateIntTypeRule($d1, $d2, $condId);
                break;

            case 15: // IP Address Rule
                $haystack = $geo_plugin->getIPAddress();
                $needle = $rule_value;

                if ($condId == 12 || $condId == 14) {
                    $needle = $haystack;
                    $haystack = $rule_value;
                }

                $eVal = RulesEngineUtil::evaluateStringTypeRule($haystack, $needle, $condId);
                break;

            case 16: // Status code
                $eVal = true;
                $is404 = is_404();
                $rule_status_id = (int) $rule_value;
                if ($rule_status_id === 21) { // 404
                    if (($is404 === true || $is404 === "true") && $condId === 1) {
                        $eVal = true;
                    }
                    if (($is404 === false || $is404 === "false") && $condId === 2) {
                        $eVal = true;
                    }
                } else {
                    $eVal = false;
                }
                break;

             case 17: // WordPress category code  
                $eVal = true;
                if(is_category()){
                    $current_cat_id = get_query_var( 'cat' );
                    $json_tokens = json_decode($rule_value);
                    $cat_ids = array();
                    if (is_array($json_tokens)) {
                        foreach ($json_tokens as $token) {
                            array_push($cat_ids, $token->id);
                        }
                    }
                    $eVal = RulesEngineUtil::evaluateArrayTypeRule($cat_ids, $current_cat_id, $condId);
                }
              
              break; 
              
             case 25: // WooCommerce category code
                $eVal = true;
                if(is_product_category()){
                    $current_cat_id = $wp_query->get_queried_object()->term_id;
                    $json_tokens = json_decode($rule_value);
                    $cat_ids = array();
                    if (is_array($json_tokens)) {
                        foreach ($json_tokens as $token) {
                            array_push($cat_ids, $token->id);
                        }
                    }
                    $eVal = RulesEngineUtil::evaluateArrayTypeRule($cat_ids, $current_cat_id, $condId);
                }
              
              break;

            case 11: // Mobile
            case 12: // Tablet
            case 13: // Mobile Operating System
            case 28:  // Mobile Device Type
                $eVal = $this->evaluate_mobile_rule($rule_value);

                // Not equal
                if ($condId == 2) {
                    $eVal = !$eVal;
                }

                break;

            case 18: // User Id
                $eVal = true;
                $logged_in_user = wp_get_current_user();
                if ($logged_in_user->ID != 0) {
                    $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $logged_in_user->user_login, $condId);
                }
                break;

            case 19: // Guest User
                $eVal = true;

                $logged_in_user = wp_get_current_user();

                // Guest User
                if ($logged_in_user->ID == 0 && $rule_value == 25 && $condId == 1) {
                    $eVal = true;
                }

                // Logged In User
                if ($logged_in_user->ID != 0 && $rule_value == 25 && $condId == 2) {
                    $eVal = true;
                }

                break;

            case 23: // Day of the Week
                $day_of_week = jddayofweek(cal_to_jd(CAL_GREGORIAN, date("m"), date("d"), date("Y")), 1);
                $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $day_of_week, $condId);
                break;

            case 24: // Months
                $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, date('M'), $condId);
                break;

            case 20: // Continent
                $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $geo_plugin->getContinentCode(), $condId);
                break;

            case 21: // Page Title
            case 22: // Post Title
                $eVal = true;
                $current_page_id = get_queried_object_id();
                $json_tokens = json_decode($rule_value);
                $page_ids = array();
                if (is_array($json_tokens)) {
                    foreach ($json_tokens as $token) {
                        array_push($page_ids, $token->id);
                    }
                }
                $eVal = RulesEngineUtil::evaluateArrayTypeRule($page_ids, $current_page_id, $condId);
                break;

            case 26: // Param condition
                $eVal = RulesEngineUtil::evaluateParameterRules($rule_value, $condId, false);
                break;

            case 27: // Form data
                $eVal = RulesEngineUtil::evaluateFormDataRules($rule_value, $condId);
                break;

            case 29: // City Rule
                $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $geo_plugin->getCity(), $condId);
                break;

            case 30: // Region
                $eVal = RulesEngineUtil::evaluateTokenInputRule($rule_value, $geo_plugin->getRegion(), $condId);
                break;

            case 31: // Referral Path
                $referer_path = wp_get_referer();
                $eVal = RulesEngineUtil::evaluateURLTypeRule(array($referer_path), $rule_value, $condId);
                break;

            case 32: // Cookie Path
                if (RulesEngineUtil::isContains($rule_value, "=")) {
                    $cookie_data = explode("=", $rule_value);
                    $cookie_key = $cookie_data[0];
                    $cookie_val = $cookie_data[1];
                } else {
                    $cookie_key = $rule_value;
                    $cookie_val = '';
                }
                if (isset($_COOKIE[$cookie_key])) {
                    $cookie = $_COOKIE[$cookie_key];
                }

                $eVal = RulesEngineUtil::evaluateStringTypeRule($cookie, $cookie_val, $condId);
                break;

            case 33: // Operating System
                $eVal = RulesEngineUtil::evaluate_os_rule($rule_value, $condId);
                break;

            case 34: // Product Tags
                $eVal = true;
                if (is_tax()) {
                    $current_obj = $wp_query->get_queried_object();
                    if(isset($current_obj) && $current_obj->taxonomy == 'product_tag'){
                        $current_tag_id = $current_obj->term_id;
                        $json_tokens = json_decode($rule_value);
                        $wp_tag_ids = array();
                        if (is_array($json_tokens)) {
                            foreach ($json_tokens as $token) {
                                array_push($wp_tag_ids, $token->id);
                            }
                        }
                        $eVal = RulesEngineUtil::evaluateArrayTypeRule($wp_tag_ids, $current_tag_id, $condId);
                    }
                }
                break;

            case 35: // Postal Code
                $currentZip = $geo_plugin->getZipCode();
                $ruleZip = $rule_value;

                if ($condId == 12 || $condId == 14) {
                    $ruleZip = $currentZip;
                    $currentZip = $rule_value;
                }

                $eVal = RulesEngineUtil::evaluateStringTypeRule($currentZip, $ruleZip, $condId);
                break;

            case 36: // Post Tags
                $eVal = true;
                if (is_tag()) {
                    $current_tag_id = $wp_query->get_queried_object()->term_id;
                    $json_tokens = json_decode($rule_value);
                    $wp_tag_ids = array();
                    if (is_array($json_tokens)) {
                        foreach ($json_tokens as $token) {
                            array_push($wp_tag_ids, $token->id);
                        }
                    }
                    $eVal = RulesEngineUtil::evaluateArrayTypeRule($wp_tag_ids, $current_tag_id, $condId);
                }
                break;

            /*case 2000: // Product Attributes
                $eVal = true;
                if (is_tax()) {
                    $current_tag_id = $wp_query->get_queried_object()->term_id;
                    $json_tokens = json_decode($rule_value);
                    $wp_tag_ids = array();
                    if (is_array($json_tokens)) {
                        foreach ($json_tokens as $token) {
                            array_push($wp_tag_ids, $token->id);
                        }
                    }
                    //$eVal = RulesEngineUtil::evaluateArrayTypeRule($wp_tag_ids, $current_tag_id, $condId);
                }
                break;*/
        }
        
        return $eVal;
    }
    
    public function bis_search_box() {
        $search = RulesEngineCacheWrapper::get_session_attribute(BIS_SEARCH_BOX_VALUE);
        ?>  
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDNv8EYSqEcMyoQcpKVjRy6-YmmaWj5gy8&libraries=places"></script>
        <form id="bis_select_location" name="bis_select_location" action="" method="post" onsubmit="return false"> 
            <span id="bis_search_icon"></span>
            <input name="bis_autocomplete" id="bis_autocomplete" value="<?php echo $search; ?>" type="search" placeholder="Enter a location" />
            <span id="bis_searchclear">&otimes;</span>
            <input name="bis_clear_search_box" id="bis_clear_search_box" value="" type="hidden">
            <input name="bis_autocomplete_google_city" id="locality" value="" type="hidden">
            <input name="bis_autocomplete_google_state" id="administrative_area_level_1" value="" type="hidden">
            <input name="bis_autocomplete_google_country" id="country" value="" type="hidden">
            <input name="bis_country_code" id="country_code" value="" type="hidden">
            <input name="bis_search" id="bis_search" value="" type="hidden">
        </form><br>
        <script>
            jQuery(document).ready(function(){
                var placeSearch, autocomplete, input, place;
                var componentForm = {
                    locality: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name'
                };
                
                input = document.getElementById('bis_autocomplete'); 
                autocomplete = new google.maps.places.Autocomplete(input);
                google.maps.event.addListener(autocomplete, 'place_changed', function(){ 
                    fillInAddress();
                    //document.getElementById("bis_select_location").submit();
                });

                var searchVal = jQuery("#bis_autocomplete").val();
                if(searchVal === "") {
                    jQuery("#bis_searchclear").hide();
                }
                jQuery("#bis_autocomplete").keyup(function(event) {
                    if(jQuery("#bis_autocomplete").val() !== "") {  
                        jQuery("#bis_searchclear").addClass("bis-searchclear-after");
                        jQuery("#bis_searchclear").show();
                    } else {
                        jQuery("#bis_searchclear").hide();
                    }
                });
                jQuery("#bis_searchclear").click(function() {
                    jQuery("#bis_clear_search_box").val("clear_search_box");
                    var jqXHR = jQuery.post(BISAjax.ajaxurl,
                            {
                              action:"bis_clear_search_box_val"  
                            });
                    document.getElementById("bis_select_location").submit();
                });

                function fillInAddress() {
                    place = autocomplete.getPlace();

                    if(!place.address_components) {
                        alert("<?php _e('Please select a country from dropdown', BIS_RULES_ENGINE_TEXT_DOMAIN); ?>");
                        return false;
                    }

                    if(place.address_components[0].types[0] === "country") {
                        document.getElementById("bis_search").value = place.address_components[0].short_name;
                    } else {
                        document.getElementById("bis_search").value = place.name;
                    }

                    for (var component in componentForm) {
                        document.getElementById(component).value = '';
                    }

                    for (var i = 0; i < place.address_components.length; i++) {
                        var addressType = place.address_components[i].types[0];
                        if (componentForm[addressType]) {
                            var val = place.address_components[i][componentForm[addressType]];
                            document.getElementById(addressType).value = val;
                        }

                        if (addressType === "country") {
                            document.getElementById("country_code").value = place.address_components[i].short_name;
                        }
                    }
                    document.getElementById("bis_select_location").submit();
                }
            });
        </script>
        <?php
    }

    /*public function bis_re_apply_logical_rules() {
        $logical_rules = array();
        $logical_rules_array = array();
        $logical_popup_rules = array();
        $applied_rules = $this->get_applied_session_logical_rules();
        $request_rules = $this->get_request_rules();
        $logical_rules_model = new LogicalRulesEngineModel();
        
        if ($applied_rules == null) {
            $logical_rules = $request_rules;
        }

        if (empty($request_rules)) {
            $logical_rules = $applied_rules;
        }

        if (!empty($applied_rules) && !empty($request_rules)) {
            $logical_rules = array_merge($applied_rules, $request_rules);
        }
        
        if (!empty($logical_rules) && count($logical_rules) > 0) {
            foreach ($logical_rules as $logical_rule) {
                $logical_rules_array[] = $logical_rules_model->get_rule($logical_rule->ruleId);
            }
        }

        if (!empty($logical_rules_array) && count($logical_rules_array) > 0) {
            foreach ($logical_rules_array as $logical_rules_data) {
                if ($logical_rules_data['rule']->general_col1 === "show_as_dialog") {
                    $logical_popup_rules[] = $logical_rules_data['rule'];
                }
            }
            RulesEngineCacheWrapper::set_session_attribute(BIS_LOGICAL_POPUP_RULES, $logical_popup_rules);
        }
    }*/
}