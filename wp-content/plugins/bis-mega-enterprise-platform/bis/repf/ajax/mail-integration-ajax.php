<?php

$realPath = BIS_RULES_ENGINE_PLATFORM_PATH;
require_once($realPath . '/bis/repf/mailchimp/mailchip-api.php');

add_action('wp_ajax_bis_mail_subscription', 'bis_mail_subscription');

function bis_mail_subscription() {
     
    $mail_data_str = RulesEngineUtil::get_option(BIS_POPUP_MAIL_INTEGRATION_DATA, false, false);
    $mail_data = json_decode($mail_data_str);
    if(isset($mail_data->apikey)){
        
        $api = new MCAPI($mail_data->apikey);
        $list_id = $mail_data->maillist;

        $data =  $_POST['bis_popup_form_data'];
        $params = array();
        parse_str($data, $params);
        $bis_name = "";
        $bis_number = "";
        $bis_text_area = "";
        if ($params['bis_mega_email']) {
            $bis_email = $params['bis_mega_email'];
        }
        if (isset($params['bis_mega_name'])) {
            $bis_name = $params['bis_mega_name'];
        }
        if (isset($params['bis_mega_number'])) {
            $bis_number = $params['bis_mega_number'];
        }
        if (isset($params['bis_mega_txtarea'])) {
            $bis_text_area = $params['bis_mega_txtarea'];
        }
        $merge_vars = array(
            'eamil' => $bis_email,
            'name' => $bis_name,
            'monbile' => $bis_number,
            'text_area' => $bis_text_area
        );

        if(isset($params['bis_mega_email'])){
            if ($api->listSubscribe($list_id, $bis_email, $merge_vars)) {
                $results_map = array();
                $results_map[BIS_DATA] = "";
                $results_map[BIS_STATUS] = "succes";
                RulesEngineUtil::generate_json_response($results_map);
            } else {
                $results_map = array();
                $results_map[BIS_DATA] = "";
                $results_map[BIS_STATUS] = $api->errorMessage;
                RulesEngineUtil::generate_json_response($results_map);
            }
        } else {
            $results_map = array();
            $results_map[BIS_DATA] = "";
            $results_map[BIS_STATUS] = "error";
            RulesEngineUtil::generate_json_response($results_map);
        }
    }
        
}