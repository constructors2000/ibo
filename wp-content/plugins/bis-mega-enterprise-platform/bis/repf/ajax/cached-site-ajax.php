<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

use bis\repf\common\RulesEngineCacheWrapper;
use bis\repf\model\PageRulesEngineModel;
use bis\repf\model\PostRulesEngineModel;
use bis\repf\action\RulesEngine;
use bis\repf\vo\CacheVO;

add_action('wp_ajax_bis_re_mega_rules', 'bis_re_mega_rules');

function bis_re_mega_rules() {

    $nonce = $_GET ['bis_nonce'];

    // check to see if the submitted nonce matches with the generated nonce we created earlier.
    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }

    $query = null;
    $postId = null;
    $pageURL = null;
    $categoryId = null;
    $postTagId = null;
    $productTagId = null;
    $referralUrl = null;
    $is404 = false;
    $bis_cache_installed = RulesEngineUtil::get_option(BIS_RULES_ENGINE_CACHE_INSTALLED, false, false);
    $results_map = array();
    $results_map[BIS_STATUS] = BIS_SUCCESS_WITH_NO_DATA;

    // Rules should be evaluatd if cache is enabled or Redirect rules installed
    if ($bis_cache_installed === "true" || class_exists('BIS_Redirect_Controller')) {

        $results_rules_map = array();

        if (isset($_GET['bis_prd']) && RulesEngineUtil::is_ajax_request()) {
            RulesEngineCacheWrapper::remove_session_attribute(BIS_REDIRECT_POPUP_VO);
            RulesEngineUtil::generate_json_response($results_map);
        }
        if (isset($_GET['bis_re_cache_page_url'])) {
            $pageURL = $_GET['bis_re_cache_page_url'];
        }
        if (isset($_GET['bis_re_cache_post_id'])) {
            $postId = $_GET['bis_re_cache_post_id'];
        }
        if (isset($_GET['bis_re_cache_post_tag_id'])) {
            $postTagId = $_GET['bis_re_cache_post_tag_id'];
        }
        if (isset($_GET['bis_re_cache_product_tag_id'])) {
            $productTagId = $_GET['bis_re_cache_product_tag_id'];
        }
        if (isset($_GET['bis_re_cache_cat_id'])) {
            $categoryId = $_GET['bis_re_cache_cat_id'];
        }
        if (isset($_GET['bis_re_cache_reffer_path'])) {
            $referralUrl = $_GET['bis_re_cache_reffer_path'];
        }
        if (isset($_GET['bis_re_cache_404'])) {
            $is404 = $_GET['bis_re_cache_404'];
        }

        $isAjaxRequest = true;

        $session_applied_rules = RulesEngineCacheWrapper::get_session_attribute(BIS_SESSION_APPLIED_RULES);
        $applied_rules = $session_applied_rules[BIS_SESSION_APPLIED_RULES];

        $cacheVO = new CacheVO($pageURL, $postId, $categoryId, $referralUrl, $is404, $isAjaxRequest);
        $rulesEngine = new RulesEngine();
        $rulesEngine->bis_evaluate_request_rules($query, $cacheVO);

        $request_rules = RulesEngineCacheWrapper::get_session_attribute(BIS_REQUEST_RULES_KEY);

        if(!empty($applied_rules) && !empty($request_rules)) {
            $applied_rules = array_merge($applied_rules, $request_rules);
        }
        if(empty($applied_rules) && !empty($request_rules)) {
            $applied_rules = $request_rules;
        }
    }

    if ($bis_cache_installed === "true") {

        //for site popup
        if(RulesEngineUtil::get_option(BIS_POPUP_VO, false, false)) {
            $popUpVO = json_decode(RulesEngineUtil::get_option(BIS_POPUP_VO, false, false));
            $popup_data = bis_site_popup_show();
            $shop_url = get_permalink(get_option('woocommerce_shop_page_id'));
            $current_url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "";
            if($popUpVO->status === "1" && $current_url == $shop_url) {
                $results_rules_map[BIS_POPUP_KEY] = $popup_data;
                $results_map[BIS_STATUS] = BIS_SUCCESS;
            }
        }

        // Apply rules for pages for cached sites
        if (class_exists('BIS_Page_Controller')) {
            $page_rule_modal = new PageRulesEngineModel();
            $applied_pages_rules = $page_rule_modal->get_applied_page_rules($applied_rules);
            if(!empty($applied_pages_rules)){
                foreach ($applied_pages_rules as $applied_rule) {
                    if (isset($applied_rule->gencol5) && json_decode($applied_rule->gencol2)->content_position == "pos_dialog_page") {
                        $popup_data = bis_re_page_rules($applied_rule);
                        $applied_rule->popup_data = $popup_data;
                    }
                }
            }
            if (!empty($applied_pages_rules)) {
                $results_rules_map[BIS_PAGE_RULES_KEY] = $applied_pages_rules;
                $results_map[BIS_STATUS] = BIS_SUCCESS;
            }
        }

        // Apply rules for posts for cached sites
        if (class_exists('BIS_Post_Controller')) {
            $post_rule_modal = new PostRulesEngineModel();
            $applied_posts_rules = $post_rule_modal->get_applied_post_rules($applied_rules);
            if (!empty($applied_posts_rules)) {
                $applied_post_rules = array();
                foreach ($applied_posts_rules as $applied_rule) {
                    if ($applied_rule->rule_type_id == "3") {
                        switch ($applied_rule->parent_type_value) {
                            case 'wp-tags':
                                if ($postTagId == $applied_rule->parent_id && isset($applied_rule->gencol5) &&
                                        json_decode($applied_rule->gencol2)->content_position == "pos_dialog_post") {
                                    $popup_data = bis_re_post_rules($applied_rule);
                                    $applied_rule->popup_data = $popup_data;
                                }
                                array_push($applied_post_rules, $applied_rule);
                                break;
                            case 'wp-categories':
                                if ($categoryId == $applied_rule->parent_id && isset($applied_rule->gencol5) &&
                                        json_decode($applied_rule->gencol2)->content_position == "pos_dialog_post") {
                                    $popup_data = bis_re_post_rules($applied_rule);
                                    $applied_rule->popup_data = $popup_data;
                                }
                                array_push($applied_post_rules, $applied_rule);
                                break;
                            default:
                                if ($applied_rule->parent_id == $postId && isset($applied_rule->gencol5) &&
                                        json_decode($applied_rule->gencol2)->content_position == "pos_dialog_post") {
                                    $popup_data = bis_re_post_rules($applied_rule);
                                    $applied_rule->popup_data = $popup_data;
                                }
                                array_push($applied_post_rules, $applied_rule);
                                break;
                        }
                    }
                }
            }
            if (!empty($applied_post_rules)) {
                $results_rules_map[BIS_POST_RULES_KEY] = $applied_post_rules;
                $results_map[BIS_STATUS] = BIS_SUCCESS;
            }
        }

        // Apply rules for productts for cached sites
        if (class_exists('BIS_Woo_Product_Controller')) {
            $post_rule_modal = new PostRulesEngineModel();
            $applied_posts_rules = $post_rule_modal->get_applied_post_rules($applied_rules);
            if (!empty($applied_posts_rules)) {
                $applied_product_rules = array();
                foreach ($applied_posts_rules as $applied_rule) {
                    switch ($applied_rule->parent_type_value) {
                        case 'categories':
                            if ($categoryId == $applied_rule->parent_id && isset($applied_rule->gencol5) &&
                                    json_decode($applied_rule->gencol2)->content_position == "pos_dialog_post") {
                                $popup_data = bis_re_product_rules($applied_rule);
                                $applied_rule->popup_data = $popup_data;
                            }
                            array_push($applied_product_rules, $applied_rule);
                            break;
                        case 'woo-tags':
                            if ($productTagId == $applied_rule->parent_id && isset($applied_rule->gencol5) &&
                                    json_decode($applied_rule->gencol2)->content_position == "pos_dialog_post") {
                                $popup_data = bis_re_product_rules($applied_rule);
                                $applied_rule->popup_data = $popup_data;
                            }
                            array_push($applied_product_rules, $applied_rule);
                            break;
                        case 'attributes':
                            break;
                        default:
                            if ($applied_rule->parent_id == $postId && isset($applied_rule->gencol5) &&
                                    json_decode($applied_rule->gencol2)->content_position == "pos_dialog_post") {
                                $popup_data = bis_re_product_rules($applied_rule);
                                $applied_rule->popup_data = $popup_data;
                            }
                            array_push($applied_product_rules, $applied_rule);
                            break;
                    }
                }
            }
            if (!empty($applied_posts_rules)) {
                $results_rules_map[BIS_WOO_PRODUCT_RULES_KEY] = $applied_posts_rules;
                $results_map[BIS_STATUS] = BIS_SUCCESS;
            }
        }
    }

    // Apply rules for widgets for cached sites
    /* if (class_exists('BIS_Widget_Controller')) {
      $widget_rule_modal = new WidgetRulesEngineModel();
      $applied_widget_rules = $widget_rule_modal->get_widget_applied_rule_details($applied_rules);

      if (!empty($applied_widget_rules)) {
      $results_rules_map[BIS_WIDGET_RULES_KEY] = $applied_widget_rules;
      $results_map[BIS_STATUS] = BIS_SUCCESS;
      }
      } */

    // Redirect rules should be applied irrespective of cache
    if (class_exists('BIS_Redirect_Controller')) {
        $send_resp = TRUE;

        //Caldera forms requirement
        if ((isset($_POST['action'])) && ($_POST['action'] === 'cf_process_ajax_submit')) {
            $send_resp = FALSE;
        }

        if (RulesEngineUtil::is_redirect()) {
            $redirect_rules = bis_re_redirect($applied_rules, $send_resp, $bis_cache_installed, $is404);

            if (!empty($redirect_rules) && $send_resp) {
                $results_rules_map[BIS_REDIRECT_RULES_KEY] = $redirect_rules;
                $results_map[BIS_STATUS] = BIS_SUCCESS;
            }
        }
    }

    if (!empty($results_rules_map)) {
        $results_map[BIS_DATA] = $results_rules_map;
    }

    RulesEngineUtil::generate_json_response($results_map);
}