<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

use bis\repf\IP2Location\Database;
use bis\repf\model\LogicalRulesEngineModel;
use bis\repf\util\GeoPluginWrapper;
use bis\repf\vo\PopUpVO;

add_action('wp_ajax_bis_re_save_settings', 'bis_re_save_settings');
add_action('wp_ajax_bis_re_save_popup_settings', 'bis_re_save_popup_settings');
add_action('wp_ajax_bis_re_activate_plugin', 'bis_re_activate_plugin');
add_action('wp_ajax_bis_re_acplg_error', 'bis_re_acplg_error');
add_action('wp_ajax_bis_dowload_city_countries_db', 'bis_dowload_city_countries_db');
add_action('wp_ajax_bis_ip2location_db_import', 'bis_ip2location_db_import');
add_action('wp_ajax_bis_delete_rules_engin_platform', 'bis_delete_rules_engin_platform');
add_action('wp_ajax_bis_get_allowable_tags', 'bis_get_allowable_tags');
add_action('wp_ajax_bis_check_db_name', 'bis_check_db_name');
add_action('wp_ajax_bis_save_configurations', 'bis_save_configurations');
add_action('wp_ajax_bis_save_whitelist_ips', 'bis_save_whitelist_ips');
add_action('wp_ajax_bis_save_blacklist_ips', 'bis_save_blacklist_ips');
add_action('wp_ajax_bis_get_mailing_list', 'bis_get_mailing_list');
add_action('wp_ajax_bis_save_mail_integrtion_sett', 'bis_save_mail_integrtion_sett');

function bis_check_db_name() {
    $file_name = $_POST['ip2lFile'];
    $check_box = $_POST['ip2LocationCheckbox'];
    $status = BIS_ERROR;
    $source_name = null;
    $result_map = array();
    $file_path = RulesEngineUtil::get_file_upload_path();
    $scanned_directory = array_diff(scandir($file_path), array('..', '.'));

    if ($check_box == "true") {
        foreach ($scanned_directory as $value) {
            if ($value === $file_name) {
                $source_name = $value;
                $status = BIS_SUCCESS;
            }
        }
        $result_map[BIS_STATUS] = $status;
        $result_map[BIS_DATA] = $source_name;
    } else {
        $result_map[BIS_STATUS] = BIS_SUCCESS;
        $result_map[BIS_DATA] = "Maxmind DB";
    }

    RulesEngineUtil::generate_json_response($result_map);
}

function bis_get_allowable_tags() {

    $results_map = array();

    $allowable_tags_str = RulesEngineUtil::get_option(BIS_RULES_ENGINE_ALLOWABLE_TAGS_CONST);
    $allowable_tags = RulesEngineContentUtil::convert_string_to_tags($allowable_tags_str, $from = "ajax");

    $results_map[BIS_STATUS] = BIS_SUCCESS;
    $results_map[BIS_DATA] = $allowable_tags;
    RulesEngineUtil::generate_json_response($results_map);
}

function bis_dowload_city_countries_db() {

    $city_status = bis_city_country_DB_dowload("city");
    $country_status = bis_city_country_DB_dowload("country");
    $result_map = array(
        'country' => $country_status[BIS_STATUS],
        'city' => $city_status[BIS_STATUS]
    );
    wp_send_json($result_map);
}

function bis_ip2location_db_import() {
    $nonce = $_GET ['bis_nonce'];
    $file_name = $_GET ['file_name'];

    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }

    $logical_modal = new bis\repf\model\LogicalRulesEngineModel();
    $file_path = RulesEngineUtil::get_file_upload_path() . $file_name;
    $str = str_replace('\\', '/', $file_path);
    
    $executon_time= ini_get("max_execution_time");
    $execution_memory_limit=ini_get('memory_limit');
    
    ini_set('memory_limit', SET_MAX_EXECUTION_MEMORY );
    ini_set("max_execution_time", SET_MAX_EXECUTION_TIME);
    
    $result = $logical_modal->bis_ip2location_import_db($str);
    
    ini_set("max_execution_time", $executon_time);
    ini_set('memory_limit', $execution_memory_limit);
    wp_send_json($result);
     
}

function bis_city_country_DB_dowload($db_name) {

    $delete_file = null;
    $results_map = array();

    try {
        if ($db_name === "city") {
            $db_file = BIS_DOWNLOAD_CITY_DB;
            $folder_name = "GeoLite2-City.tar.gz";
        }
        if ($db_name === "country") {
            $db_file = BIS_DOWNLOAD_CONTRY_DB;
            $folder_name = "GeoLite2-Country.tar.gz";
        }
        file_put_contents(RulesEngineUtil::get_file_upload_path() . $folder_name, fopen($db_file, 'r'));
        $phar = new PharData(RulesEngineUtil::get_file_upload_path() . $folder_name);
        $phar->extractTo(RulesEngineUtil::get_file_upload_path(), NULL, true); // extract all files, and overwrite
        $db_file = RulesEngineUtil::get_file_upload_path() . $folder_name;
        $newfile = $_SERVER['DOCUMENT_ROOT'] . '/img/submitted/yoyo.jpg';

        $file_path = RulesEngineUtil::get_file_upload_path() . $phar . "/";
        $files = scandir($file_path);
        $source = $file_path;
        $destination = RulesEngineUtil::get_file_upload_path();

        foreach ($files as $file) {
            if (in_array($file, array(".", "..")))
                continue;

            if (RulesEngineUtil::isContains($file, ".mmdb")) {
                if ($db_name == "city") {
                    copy($source . $file, $destination . "GeoLite2-City.mmdb");
                    $delete_file[] = $source . $file;
                }
                if ($db_name == "country") {
                    copy($source . $file, $destination . "GeoLite2-Country.mmdb");
                    $delete_file[] = $source . $file;
                }
            } else {
                if ($db_name == "city") {
                    copy($source . $file, $destination . $file . "city");
                    $delete_file[] = $source . $file;
                }
                if ($db_name == "country") {
                    copy($source . $file, $destination . $file . "country");
                    $delete_file[] = $source . $file;
                }
            }
        }
        // Delete all successfully-copied files
        /* foreach ($delete as $file) {
          unlink($file);
          } */
        if ($delete_file !== null) {
            $results_map[BIS_STATUS] = BIS_SUCCESS;
            return $results_map;
        } else {
            $results_map[BIS_STATUS] = BIS_ERROR;
            return $results_map;
        }
    } catch (Exception $exc) {
        $results_map[BIS_STATUS] = $e;
        return $results_map;
    }
}

function bis_re_acplg_error() {

    $nonce = $_POST ['bis_nonce'];
    $product_id = $_POST ['product_id'];
    $purchase_code = $_POST ['pur_code'];

    // generated nonce we created earlier
    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }

    $domain_name = $_SERVER['SERVER_NAME'];
    $subject = $product_id . ' : ' . $domain_name . ' : Invalid purchase code used';
    $msg_body = "Product Id : " . $product_id . "\n Domain Name : " . $domain_name .
            "\n Is using an invalid purchase code. \n ."
            . "Purchase Code : " . $purchase_code;

    wp_mail(RULES_ENGINE_MAIL, $subject, $msg_body);

    $results_map = array();
    $results_map[BIS_STATUS] = BIS_SUCCESS;
    RulesEngineUtil::generate_json_response($results_map);
}

function bis_re_activate_plugin() {
    $nonce = $_POST ['bis_nonce'];
    $item_name = $_POST ['item_name'];
    $license = $_POST ['licence_type'];
    $buyer = $_POST ['buyer'];
    $product_id = $_POST ['product_id'];
    $purchase_code = $_POST ['pur_code'];
    $purchase_date = $_POST ['purchase_date'];


    // generated nonce we created earlier
    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }

    $bis_re_prd_vrf = RulesEngineUtil::get_purchase_code($product_id);
    $bis_re_pur_key = BIS_PUR_CODE . $product_id;

    if ($bis_re_prd_vrf == false || empty($bis_re_prd_vrf)) {
        RulesEngineUtil::add_option($bis_re_pur_key, $purchase_code);
    } else {
        RulesEngineUtil::update_option($bis_re_pur_key, $purchase_code);
    }


    $domain_name = $_SERVER['SERVER_NAME'];
    $subject = $item_name . ' : ' . $license . ' Domain : ' . $domain_name;
    $msg_body = "Customer Name : " . $buyer . "\nItem Name : " . $item_name .
            "\nLicense : " . $license . "\nDomain : " . $domain_name .
            "\nPurchase Code : " . $purchase_code .
            "\nPurchase Date : " . $purchase_date;

    wp_mail(RULES_ENGINE_MAIL, $subject, $msg_body);

    $results_map = array();
    $results_map[BIS_STATUS] = BIS_SUCCESS;
    RulesEngineUtil::generate_json_response($results_map);
}

function copy_country_db($fileName, $directory) {

    $plugin_dir = plugin_dir_path(__FILE__) . 'library/front-page.php';
    $theme_dir = get_stylesheet_directory() . '/front-page.php';

    if (!copy($plugin_dir, $theme_dir)) {
        echo "failed to copy $plugin_dir to $theme_dir...\n";
    }
}

function bis_re_save_settings() {
    $nonce = $_POST ['bis_nonce'];

    // generated nonce we created earlier
    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }

    $bis_re_delete_db = false;
    $bis_re_del_plugin = false;
    $bis_re_enable_analytics = false;
    //$bis_re_cache_enabled = false;

    $bis_geo_maxmind_db_file = $_POST['selectedFileNmae'];
    $bis_ip2_location_db_file = $_POST['ip2locationfile'];
    $bis_ip2_location_db_csv_file = $_POST['ip2locationcsvfile'];
    $bis_geolocation_db = 0;
    $bis_geolocation_ws = 0;
    $bis_ip2location_db = 0;
    $bis_ip2location_csv_db = 0;
    $bis_geolocation = $_POST['maxmind'];
    $bis_ip2location = $_POST['ip2LOactionCheckbox'];
    $bis_ip2location_csv = $_POST['ip2location_csv'];
    $bis_geolocation_service = $_POST['geoService'];
    $bis_re_del_plugin = $_POST['deletePlugin'];

    if ($bis_geolocation === "true") {
        $bis_geolocation_db = 1;
    }
    if ($bis_ip2location === "true") {
        $bis_ip2location_db = 1;
    }
     if ($bis_ip2location_csv === "true") {
         $bis_ip2location_csv_db = 1;
    }

    if ($bis_geolocation_service === "true") {
        $bis_geolocation_ws = 1;
    }

    if ($bis_re_del_plugin === "true") {
        RulesEngineUtil::update_option(BIS_RULES_ENGINE_PLUGIN_FORCE_DELETE, "true");
    } else {
        RulesEngineUtil::update_option(BIS_RULES_ENGINE_PLUGIN_FORCE_DELETE, "false");
    }

    $results_map = array();

    if ($bis_geolocation_db == 1) { // validate file only if maxmind db selected
        try {
            $filePath = RulesEngineUtil::get_file_upload_path() . $bis_geo_maxmind_db_file;
            $reader = new bis\repf\MaxMind\Db\Reader($filePath);
            RulesEngineUtil::update_option(BIS_GEO_MAXMIND_DB_FILE, $bis_geo_maxmind_db_file);
        } catch (Exception $ex) {
            $results_map[BIS_STATUS] = BIS_ERROR;
            $results_map[BIS_MESSAGE_KEY] = BIS_INVALID_DATABASE_FILE;
            RulesEngineUtil::generate_json_response($results_map);
        }
    }
    if ($bis_ip2location_db == 1) {
        try {
            $filePath = RulesEngineUtil::get_file_upload_path() . $bis_ip2_location_db_file;
            $db = new Database(RulesEngineUtil::get_file_upload_path() . $bis_ip2_location_db_file, Database::FILE_IO);

            RulesEngineUtil::update_option(BIS_GEO_IP2LOCATION_DB, $bis_ip2_location_db_file);
        } catch (Exception $ex) {
            $results_map[BIS_STATUS] = BIS_ERROR;
            $results_map[BIS_MESSAGE_KEY] = BIS_INVALID_DATABASE_FILE;
            RulesEngineUtil::generate_json_response($results_map);
        }
    }
    if ($bis_ip2location_csv_db == 1) {
        try {
            $filePath = RulesEngineUtil::get_file_upload_path() . $bis_ip2_location_db_csv_file;
            $db = new Database(RulesEngineUtil::get_file_upload_path() . $bis_ip2_location_db_csv_file, Database::FILE_IO);

            RulesEngineUtil::update_option(BIS_GEO_IP2LOCATION_CSV_DB, $bis_ip2_location_db_csv_file);
        } catch (Exception $ex) {
            $results_map[BIS_STATUS] = BIS_ERROR;
            $results_map[BIS_MESSAGE_KEY] = BIS_INVALID_DATABASE_FILE;
            RulesEngineUtil::generate_json_response($results_map);
        }
    }

    RulesEngineUtil::update_option(BIS_GEO_LOOKUP_TYPE, $bis_geolocation_db);
    RulesEngineUtil::update_option(BIS_GEO_IP2LOCATION_LOOKUP_TYPE, $bis_ip2location_db);
    RulesEngineUtil::update_option(BIS_GEO_IP2LOCATION_CSV_LOOKUP_TYPE, $bis_ip2location_csv_db);
    RulesEngineUtil::update_option(BIS_GEO_LOOKUP_WEBSERVICE_TYPE, $bis_geolocation_ws);

    $bis_re_delete_db = $_POST ["deleteEnable"];
    //$bis_re_cache_enabled = $_POST ["cacheEnable"];

    /* if (isset($_POST ["bis_re_delete_db"])) {
      $bis_re_delete_db = $_POST ["bis_re_delete_db"];
      }

      if (isset($_POST ["bis_re_cache_enabled"])) {
      $bis_re_cache_enabled = $_POST ["bis_re_cache_enabled"];
      } */

    if ($bis_re_delete_db === "true") {
        RulesEngineUtil::update_option(BIS_RULES_ENGINE_DELETE_DB, "true");
    } else {
        RulesEngineUtil::update_option(BIS_RULES_ENGINE_DELETE_DB, "false");
    }

    /* if ($bis_re_cache_enabled === "true") {
      RulesEngineUtil::update_option(BIS_RULES_ENGINE_CACHE_INSTALLED, "true");
      } else {
      RulesEngineUtil::update_option(BIS_RULES_ENGINE_CACHE_INSTALLED, "false");
      } */

    if (isset($_POST ["geonameuser"])) {
        RulesEngineUtil::update_option(BIS_GEO_NAME_USER, $_POST ["geonameuser"]);
    }

    $bis_re_allowable_tags = $_POST ["selectedTags"];
    RulesEngineUtil::update_option(BIS_RULES_ENGINE_ALLOWABLE_TAGS_CONST, $bis_re_allowable_tags);

    $results_map = array();

    $results_map[BIS_DATA] = RulesEngineUtil::get_option(BIS_RULES_ENGINE_DELETE_DB);
    $results_map[BIS_STATUS] = BIS_SUCCESS;
    RulesEngineUtil::generate_json_response($results_map);
}

function bis_delete_rules_engin_platform() {

    $bis_re_del_plugin = $_GET['deletePlugin'];
    $bis_re_enable_analytics = $_GET['enablAnalytics'];
    if ($bis_re_del_plugin === "true") {
        RulesEngineUtil::update_option(BIS_RULES_ENGINE_PLUGIN_FORCE_DELETE, "true");
    } else {
        RulesEngineUtil::update_option(BIS_RULES_ENGINE_PLUGIN_FORCE_DELETE, "false");
    }
    if ($bis_re_enable_analytics === "true") {
        RulesEngineUtil::update_option(BIS_CAPTURE_ANALYTICS_DATA, "true");
    } else {
        RulesEngineUtil::update_option(BIS_CAPTURE_ANALYTICS_DATA, "false");
    }
}

function bis_save_configurations() {

    $bis_rules_debug_mode = $_GET['rulesDebugSwitch'];
    $bis_re_cache_enabled = $_GET['cacheEnableSwitch'];
    $bis_country_dropdown = $_GET['countryDropdownSwitch'];
    
    /* $bis_nprogress_bar_color = $_GET['progressBarColor'];
      $bis_rules_evaluation_enabled = $_GET['ruleEvaluationSwitch'];

      RulesEngineUtil::update_option(BIS_RULES_EVALUATION_CONT, $bis_rules_evaluation_enabled, false, false);
      RulesEngineUtil::update_option(BIS_NPROGRESS_BAR_COLOR_CONST, $bis_nprogress_bar_color, false, false); */

    RulesEngineUtil::update_option(BIS_RULES_DEBUG_MODE_CONT, $bis_rules_debug_mode, false, false);
    RulesEngineUtil::update_option(BIS_MEGA_COUNTRY_DROPDOWN, $bis_country_dropdown, false, false);

    if ($bis_re_cache_enabled === "Yes") {
        RulesEngineUtil::update_option(BIS_RULES_ENGINE_CACHE_INSTALLED, "true", false, false);
    } else {
        RulesEngineUtil::update_option(BIS_RULES_ENGINE_CACHE_INSTALLED, "false", false, false);
    }

}

function bis_save_whitelist_ips() {
    $bis_sel_ips = $_GET['selIPs'];
    RulesEngineUtil::update_option(BIS_MEGA_SEL_IPS, $bis_sel_ips, false, false);
}

function bis_save_blacklist_ips() {
    $bis_black_ips = $_GET['blIPs'];
    $bis_black_red = $_GET['blRed'];
    RulesEngineUtil::update_option(BIS_MEGA_BLACK_IPS, $bis_black_ips, false, false);
    RulesEngineUtil::update_option(BIS_MEGA_BLACK_RED, $bis_black_red, false, false);
}

function bis_re_save_popup_settings() {
    $nonce = $_POST ['bis_nonce'];

    // generated nonce we created earlier
    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }
    
    $logical_rules_model = new LogicalRulesEngineModel();

    $popUpVO = new PopUpVO();
    

    if (isset($_POST["bis_re_template"])) {

        $popUpVO->set_status($_POST['bis_re_rule_status']);
        $selectedCountries = array();
        $all_countries = $logical_rules_model->get_all_countries();
        foreach ($all_countries as $all_country) {
            $total_countries[$all_country->value] = $all_country->display_name;
        }
        $geo_wrapper = new GeoPluginWrapper();
        $country_continent_map = $geo_wrapper->getCountryContinentMap();
        $continents = $geo_wrapper->getContinentMap();

        $template_file_name = $_POST["bis_re_template"];
        $popUpVO->setTempalteFileName($template_file_name);
        if ($template_file_name == "bis-continent-country-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/popup-templates/" . $template_file_name);
            RulesEngineUtil::update_option(BIS_CONTINENT_COUNTRY_POPUP_TEMPLATE, $dynamicContent);
            if (isset($_POST["bis_re_select_country"])) {
                $countries = $_POST["bis_re_select_country"];
                foreach ($countries as $country) {
                    $continent_code = $country_continent_map[$country];
                    $continent = $continents[$continent_code];
                    if (array_key_exists($continent, $selectedCountries)) {
                        $exists = $selectedCountries[$continent];
                        $exists[$country] = explode("-", $total_countries[$country])[0];
                        $selectedCountries[$continent] = $exists;
                    } else {
                        $selectedCountries[$continent] = array($country => explode("-", $total_countries[$country])[0]);
                    }
                }
                $popUpVO->setSelectedCountries($countries);
                $popUpVO->setContinentCountryMap($selectedCountries);
            }
        } else if ($template_file_name == "bis-continent-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/popup-templates/" . $template_file_name);
            RulesEngineUtil::update_option(BIS_CONTINENT_POPUP_TEMPLATE, $dynamicContent);
            if (isset($_POST["bis_re_select_continent"])) {
                $continent_codes = $_POST["bis_re_select_continent"];
                foreach ($continent_codes as $continent_code) {
                    $selectedContinents[$continent_code] = $continents[$continent_code];
                }
                $popUpVO->setSelectedCountries($selectedContinents);
            }
        } else if ($template_file_name == "bis-country-flags-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/popup-templates/" . $template_file_name);
            RulesEngineUtil::update_option(BIS_COUNTRY_FLAGS_POPUP_TEMPLATE, $dynamicContent);
            if (isset($_POST["bis_re_select_country"])) {
                $countries = $_POST["bis_re_select_country"];
                foreach ($countries as $country) {
                    $selectedCountries[$country] = explode("-", $total_countries[$country])[0];
                }
                $popUpVO->setSelectedCountries($selectedCountries);
            }
        } else if ($template_file_name == "bis-country-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/popup-templates/" . $template_file_name);
            RulesEngineUtil::update_option(BIS_COUNTRY_POPUP_TEMPLATE, $dynamicContent);
            if (isset($_POST["bis_re_select_country"])) {
                $countries = $_POST["bis_re_select_country"];
                foreach ($countries as $country) {
                    $selectedCountries[$country] = explode("-", $total_countries[$country])[0];
                }
                $popUpVO->setSelectedCountries($selectedCountries);
            }
        } else if ($template_file_name == "bis-region-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/popup-templates/" . $template_file_name);
            RulesEngineUtil::update_option(BIS_REGION_POPUP_TEMPLATE, $dynamicContent);
        } else if ($template_file_name == "bis-city-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/popup-templates/" . $template_file_name);
            RulesEngineUtil::update_option(BIS_CITY_POPUP_TEMPLATE, $dynamicContent);
        } else {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/bis-popup-template.html");
            RulesEngineUtil::update_option(BIS_POPUP_TEMPLATE, $dynamicContent);
        }

        if (isset($_POST["bis_re_common_popup_title"])) {
            $popUpVO->setTitle($_POST["bis_re_common_popup_title"]);
        }

        if (isset($_POST["bis_re_common_popup_title_class"])) {
            $popUpVO->setTitleClass($_POST["bis_re_common_popup_title_class"]);
        }

        if (isset($_POST["bis_re_common_popup_color"])) {
            $popUpVO->setPopUpBackgroundColor($_POST["bis_re_common_popup_color"]);
        }

        if (isset($_POST["bis_re_common_heading"])) {
            $popUpVO->setHeadingOne($_POST["bis_re_common_heading"]);
        }
        
        if (isset($_POST["bis_re_common_heading_class"])) {
            $popUpVO->setHeadingOneClass($_POST["bis_re_common_heading_class"]);
        }
        
        if (isset($_POST["bis_re_common_sub_heading"])) {
            $popUpVO->setHeadingTwo($_POST["bis_re_common_sub_heading"]);
        }
        
        if (isset($_POST["bis_re_common_sub_heading_class"])) {
            $popUpVO->setHeadingTwoClass($_POST["bis_re_common_sub_heading_class"]);
        }
        
        if (isset($_POST["bis_re_image_id"])) {
            $image_id = $_POST["bis_re_image_id"];
            $image_size = $_POST["bis_re_cont_img_size"];
            $image = $logical_rules_model->get_image_from_media_library($image_id, $image_size);
            $popUpVO->setImageOneId($image_id);
            $popUpVO->setImageOneSize($image_size);
            $popUpVO->setImageOneUrl($image->get_url());
        }

        $popUpVO->setButtonLabelOne($_POST["bis_re_btn1_label"]);

        if (isset($_POST["bis_re_btn1_class"])) {
            $popUpVO->setButtonOneClass($_POST["bis_re_btn1_class"]);
        }

        $popUpVO->setButtonLabelTwo($_POST["bis_re_but2_label"]);

        if (isset($_POST["bis_re_but2_class"])) {
            $popUpVO->setButtonTwoClass($_POST["bis_re_but2_class"]);
        }
        
        if (isset($_POST["bis_re_common_auto_red"])) {
            $popUpVO->setAutoCloseTime(intval($_POST["bis_re_common_auto_red"]));
        } else {
            $popUpVO->setAutoCloseTime(0);
        }
        
        if (isset($_POST["bis_re_but1_url"])) {
            $popUpVO->setButtonOneUrl($_POST['bis_re_but1_url']);
        }
        
        if (isset($_POST["bis_re_but2_url"])) {
            $popUpVO->setButtonTwoUrl($_POST['bis_re_but2_url']);
        }

        /*if (filter_input(INPUT_POST, "bis_re_common_red_window", FILTER_VALIDATE_INT)) {
            $popUpVO->setRedirectWindow($_POST['bis_re_common_red_window']);
        }*/

        if (filter_input(INPUT_POST, "bis_re_common_popup_occur", FILTER_VALIDATE_INT)) {
            $popUpVO->setPopUpOccurr($_POST['bis_re_common_popup_occur']);
        }

        RulesEngineUtil::update_option(BIS_POPUP_VO, json_encode($popUpVO), false, false);
    }
}

function bis_site_popup_show() {
    $popUpVo = RulesEngineUtil::get_option(BIS_POPUP_VO, false, false);
    if ($popUpVo != false && json_decode($popUpVo)->status == "1") {
        $results_map = bis_logical_popup($popUpVo);
        return $results_map;
    }
}

function bis_re_popup_show() {
    $nonce = $_GET ['bis_nonce'];

    //generated nonce we created earlier
    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }
    
    $shop_url = get_permalink(get_option('woocommerce_shop_page_id'));
    $current_url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "";

    $results_map = array();
    $results_map[BIS_STATUS] = BIS_SUCCESS_WITH_NO_DATA;
    if($current_url === $shop_url) {
        $popUpVo = RulesEngineUtil::get_option(BIS_POPUP_VO, false, false);
        if ($popUpVo != false && json_decode($popUpVo)->status == "1") {
            $results_map = bis_logical_popup($popUpVo);
        }
    }

    RulesEngineUtil::generate_json_response($results_map);
}

function bis_logical_popup($popUpVo) {

    $results_map = array();
    $popupMap = array();
    $dynamicContent = "";
    $donotShow = __("Do not show me this message again.", "rulesengine");
    $timerMsg = __("Popup close in ", "rulesengine");
    $secMsg = __("seconds", "rulesengine");
    //$jsonObj = json_decode($rulesVO->general_col2);
    $jsonObj = json_decode($popUpVo);
    $jsonObj->donot_show_msg = $donotShow;

    if ($jsonObj->autoCloseTime !== "0") {
        $jsonObj->timerMsg = $timerMsg;
        $jsonObj->secMsg = $secMsg;
    }

    $template_file_name = $jsonObj->tempalteFileName;
    if ($template_file_name == "bis-continent-country-popup-template.html") {
        $dynamicContent = RulesEngineUtil::get_option(BIS_CONTINENT_COUNTRY_POPUP_TEMPLATE);
    } else if ($template_file_name == "bis-continent-popup-template.html") {
        $dynamicContent = RulesEngineUtil::get_option(BIS_CONTINENT_POPUP_TEMPLATE);
    } else if ($template_file_name == "bis-country-flags-popup-template.html") {
        $dynamicContent = RulesEngineUtil::get_option(BIS_COUNTRY_FLAGS_POPUP_TEMPLATE);
    } else if ($template_file_name == "bis-country-popup-template.html") {
        $dynamicContent = RulesEngineUtil::get_option(BIS_COUNTRY_POPUP_TEMPLATE);
    } else if ($template_file_name == "bis-region-popup-template.html") {
        $dynamicContent = RulesEngineUtil::get_option(BIS_REGION_POPUP_TEMPLATE);
    } else if ($template_file_name == "bis-city-popup-template.html") {
        $dynamicContent = RulesEngineUtil::get_option(BIS_CITY_POPUP_TEMPLATE);
    } else {
        $dynamicContent = RulesEngineUtil::get_option(BIS_POPUP_TEMPLATE);
    }
    $jsonObj->popupTemplate = $dynamicContent;

    $results_map[BIS_STATUS] = BIS_SUCCESS;
    //$popupMap[BIS_GEOLOCATION_DATA] = $geoLocVO;
    $popupMap[BIS_POPUP_DATA] = $jsonObj;
    $results_map[BIS_DATA] = $popupMap;
    $results_map[BIS_ROW_COUNT] = null;

    return $results_map;
}

function bis_get_mailing_list(){
    
    
    $api_key = $_POST ['apiKey'];
    $data = array('fields' => 'lists');
    $url = 'https://' . substr($api_key, strpos($api_key, '-') + 1) . '.api.mailchimp.com/3.0/lists/';
    
    $curl_data_json = RulesEngineUtil::bis_rudr_mailchimp_curl_connect($url, 'GET', $api_key, $data);
    $curl_data = json_decode($curl_data_json);
    if(!empty($curl_data->lists)){
        $status = BIS_SUCCESS;
    } else {
        $status = BIS_ERROR;
    }
    $results_map = array();
    $results_map[BIS_DATA] = $curl_data;
    $results_map[BIS_STATUS] = $status;
    RulesEngineUtil::generate_json_response($results_map);
}

function bis_save_mail_integrtion_sett(){
    
    $integrator = $_POST ['integrator'];
    $apiKey = $_POST ['apiKey'];
    $mailList = $_POST ['mailList'];
    $mail_data = array(
        "integrator" => $integrator,
        "apikey" => $apiKey,
        "maillist" => $mailList
    );
    RulesEngineUtil::update_option(BIS_POPUP_MAIL_INTEGRATION_DATA, json_encode($mail_data), false, false);
}