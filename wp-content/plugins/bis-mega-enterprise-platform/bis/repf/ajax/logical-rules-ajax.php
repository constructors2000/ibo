<?php
/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

use bis\repf\model\LogicalRulesEngineModel;
use bis\repf\vo\SearchVO;
use bis\repf\vo\LogicalRulesCriteriaVO;
use bis\repf\vo\LogicalRulesVO;
use bis\repf\common\RulesEngineCacheWrapper;
use bis\repf\common\RulesEngineLocalization;
use bis\repf\model\PostRulesEngineModel;
use bis\repf\vo\LabelValueVO;
use bis\repf\action\QueryBuilderEngine;
use bis\repf\model\QueryBuilderModel;
/*use bis\repf\util\GeoPluginWrapper;
use bis\repf\vo\PopUpVO;*/

add_action('wp_ajax_bis_get_logical_rules', 'bis_get_logical_rules');
add_action('wp_ajax_bis_get_sub_options', 'bis_get_sub_options');
add_action('wp_ajax_bis_get_conditions', 'bis_get_conditions');
add_action('wp_ajax_bis_create_logical_rule', 'bis_create_logical_rule');
add_action('wp_ajax_bis_create_logical_rule_new', 'bis_create_logical_rule_new');
add_action('wp_ajax_bis_re_get_value', 'bis_re_get_value');
add_action('wp_ajax_bis_re_logical_rule_action', 'bis_re_logical_rule_action');
add_action('wp_ajax_bis_re_edit_rule', 'bis_re_edit_rule');
add_action('wp_ajax_bis_re_new_rule', 'bis_re_new_rule');
add_action('wp_ajax_bis_re_update_rule', 'bis_re_update_rule');
add_action('wp_ajax_bis_re_update_rule_new', 'bis_re_update_rule_new');
add_action('wp_ajax_bis_re_get_rule_values', 'bis_re_get_rule_values');
add_action('wp_ajax_bis_re_search_rule', 'bis_re_search_rule');

add_action('wp_ajax_bis_re_basic_rule_include', 'bis_re_basic_rule_include');
add_action('wp_ajax_bis_re_advance_rule_include', 'bis_re_advance_rule_include');
add_action('wp_ajax_bis_re_exising_rule_include', 'bis_re_exising_rule_include');
add_action('wp_ajax_bis_re_query_builder_rule_include', 'bis_re_query_builder_rule_include');
add_action('wp_ajax_bis_re_query_builder_rule_edit_include', 'bis_re_query_builder_rule_edit_include');

add_action('wp_ajax_bis_re_basic_rule_edit_include', 'bis_re_basic_rule_edit_include');
add_action('wp_ajax_bis_re_advance_rule_edit_include', 'bis_re_advance_rule_edit_include');
add_action('wp_ajax_bis_re_exising_rule_edit_include', 'bis_re_exising_rule_edit_include');

add_action('wp_ajax_bis_re_logical_rule', 'bis_re_logical_rule');
add_action('wp_ajax_bis_re_use_exising_rule', 'bis_re_use_exising_rule');
add_action('wp_ajax_bis_clear_transiant_cache', 'bis_clear_transiant_cache');
add_action('wp_ajax_bis_delete_all_rules', 'bis_delete_all_rules');
add_action('wp_ajax_bis_get_all_query_data', 'bis_get_all_query_data');
add_action('wp_ajax_bis_create_query_builder_logical_rule_new', 'bis_create_query_builder_logical_rule_new');
add_action('wp_ajax_bis_update_query_builder_logical_rule_new', 'bis_update_query_builder_logical_rule_new');
add_action('wp_ajax_bis_logical_rules_popup_tinymce', 'bis_logical_rules_popup_tinymce');
add_action('wp_ajax_bis_std_to_query', 'bis_std_to_query');
add_action('wp_ajax_bis_get_near_cities', 'bis_get_near_cities');

//clear search box value.
add_action('wp_ajax_bis_clear_search_box_val', 'bis_clear_search_box_val');


function bis_get_all_query_data() {
   
    $nonce = $_GET ['bis_nonce'];
    $plugin_type = $_GET ['plugInType'];
    
    // check to see if the submitted nonce matches with the
    // generated nonce we created earlier
    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }
    
    $query_builder_engine = new QueryBuilderEngine();
    $data_var = $query_builder_engine->get_all_query_builder_data($plugin_type);
    $results_map[BIS_DATA] = $data_var;
    $results_map[BIS_STATUS] = BIS_SUCCESS;
    
    RulesEngineUtil::generate_json_response($results_map);
}

function bis_re_use_exising_rule() {
    
    if(isset($_POST['bis_nonce'])) {
        $nonce = $_POST ['bis_nonce'];
    } else {
        $nonce = $_POST ['bis_rules_engine_nonce'];
    }
    
    $rule_id = $_POST ['bis_re_ex_rule_id'];

    // check to see if the submitted nonce matches with the
    // generated nonce we created earlier
    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }

    $rules_vo = RulesEngineCacheWrapper::get_session_attribute(BIS_SESSION_RULEVO);
    $logical_rules_engine_modal = new LogicalRulesEngineModel();
    
    if ($rules_vo != null) {
        $rules_vo->set_logical_rule_id($rule_id);
        
        // create rule
        if($rules_vo->get_id() == null || $rules_vo->get_id() == 0) {
            $results_map = $logical_rules_engine_modal->save_child_rule($rules_vo);
        } else { // update child rule
            $results_map = $logical_rules_engine_modal->update_child_rule($rules_vo);
        }
        
        $status = $results_map[BIS_STATUS];
        
        if ($status == BIS_SUCCESS) {
            RulesEngineCacheWrapper::set_reset_time($rules_vo->get_reset_rule_key());
        }
    }
    
    RulesEngineUtil::generate_json_response($results_map);
}

function bis_re_logical_rule() {
    $nonce = $_GET ['bis_nonce'];
    $ruleId = $_GET ['rule_id'];

    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce'))
        RulesEngineUtil::handle_request_forgery_error();
    
    $logical_rules_engine_modal = new LogicalRulesEngineModel();

    $results_map = $logical_rules_engine_modal->get_logical_rule($ruleId);

    RulesEngineUtil::generate_json_response($results_map);
}

function bis_re_advance_rule_include() {
    bis_re_file_include("logical-rules-advance-add-body.php");
}

function bis_re_advance_rule_edit_include() {
    bis_re_file_include("logical-rules-advance-edit-body.php");
}

function bis_re_basic_rule_edit_include() {
    bis_re_file_include("logical-rules-basic-edit-body.php");
}

function bis_re_basic_rule_include() {
    bis_re_file_include("logical-rules-basic-add-body.php");
}

function bis_re_exising_rule_include() {
    bis_re_file_include("bis-use-existing-rule.php");
}

function bis_re_query_builder_rule_include() {
    bis_re_file_include("logical-rules-query-builder-add-body.php");
}

function bis_re_query_builder_rule_edit_include() {
    bis_re_file_include("logical-rules-query-builder-edit-body.php");
}

function bis_re_file_include($file_include) {

    $nonce = $_GET ['bis_nonce'];
   
    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce'))
        RulesEngineUtil::handle_request_forgery_error();

    $pluginPath = RulesEngineUtil::getIncludesDirPath();
    include $pluginPath . $file_include;
    flush();
    exit;
}

function bis_re_search_rule() {

    RulesEngineUtil::remove_search_request();
    $nonce = $_POST ['bis_nonce'];

    // generated nonce we created earlier
    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }
    
    if (isset($_POST["searchData"])) {
        $bis_re_search_by = $_POST["searchData"]["search_by"];
        $bis_re_search_value = $_POST["searchData"]["search_value"];
        $bis_re_status = $_POST["searchData"]["search_status"];
    } else {
        $bis_re_search_by = $_POST ["bis_re_search_by"];
        $bis_re_search_value = $_POST ["bis_re_search_value"];
        $bis_re_status = $_POST ["bis_re_search_status"];
    }
    
    $logical_rules_engine_modal = new LogicalRulesEngineModel();

    $search_vo = new SearchVO();
    $search_vo->set_search_by($bis_re_search_by);
    $search_vo->set_search_value($bis_re_search_value);
    $search_vo->set_status($bis_re_status);

    $page_start_index = RulesEngineUtil::get_start_page_index();
    $results_map = $logical_rules_engine_modal->search_logical_rules_only($search_vo, $page_start_index);

    RulesEngineUtil::set_search_request();
    RulesEngineUtil::generate_json_response($results_map, BIS_SEARCH_REQUEST);
}

/**
 *
 * Gets the rule values based on the sub option Id
 *
 */
function bis_re_get_rule_values() {

    $nonce = $_GET ['bis_nonce'];

    // generated nonce we created earlier
    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }

    $logical_rules_engine_modal = new LogicalRulesEngineModel();
    $sub_option_id = $_GET["bis_sub_criteria"];
    if($sub_option_id == "bis_logical_rule"){
        $rows = $logical_rules_engine_modal->get_all_logical_rules_only();
    } else {
        $sub_option_id = intval($_GET["bis_sub_criteria"]);
        $ssubOptionId = null;
        if(isset($_GET['bis_ssubCriteria'])){
            $ssubOptionId = $_GET['bis_ssubCriteria'];
        }
        $rows = $logical_rules_engine_modal->get_rule_values($sub_option_id, true, false, false, $ssubOptionId);
    }
    wp_send_json($rows);
}

/**
 *
 * Updates the logical rule.
 *
 */
function bis_re_update_rule() {
    
    RulesEngineCacheWrapper::delete_all_transient_cache();

    $nonce = $_POST ['bis_nonce'];

    // check to see if the submitted nonce matches with the
    // generated nonce we created earlier
    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }
    
    $bis_re_rId = $_POST ['bis_re_rId'];

    $json_query = stripslashes($_POST ['bis_jquery_result']);
    $array_query = json_decode($json_query, true);
    $bis_re_eval_type = $_POST ['bis_re_eval_type'];
    $bis_add_rule_type = 4;

    $query_model = new QueryBuilderModel();
    $rule_criteria_array = $query_model->bis_quey_builder_function($array_query);
    
    /*$rule_criteria_array = array();

    $bis_re_condition = $_POST ['bis_re_condition'];
    $bis_re_rule_option = $_POST ['bis_re_rule_option'];
    $bis_re_rule_value = $_POST ['bis_re_rule_value'];
    $bis_re_sub_option = $_POST ['bis_re_sub_option'];
    $bis_re_logical_op = $_POST ['bis_re_logical_op'];
    $bis_re_left_bracket = $_POST ['bis_re_left_bracket'];
    $bis_re_right_bracket = $_POST ['bis_re_right_bracket'];
    $bis_re_sub_opt_type_id = $_POST ['bis_re_sub_opt_type_id'];
    $bis_re_rId = $_POST ['bis_re_rId'];
    $bis_add_rule_type = $_POST ['bis_add_rule_type'];
    $bis_re_status = $_POST ['bis_re_status'];
    $bis_re_eval_type = $_POST ['bis_re_eval_type'];
    $bis_re_rcId = $_POST ['bis_re_rcId'];

    $bis_re_delete = $_POST['bis_re_delete'];

    $bis_re_rule_type = "option";
    // Loop over the list of rule criteria
    for ($rows = 0; $rows < count($bis_re_rule_option); $rows++) {

        $logical_rules_criteria_vo = new LogicalRulesCriteriaVO();

        if (isset($bis_re_rcId[$rows])) {
            $logical_rules_criteria_vo->set_Id($bis_re_rcId[$rows]);
        }

        $sub_option = (int) $bis_re_sub_option[$rows];
        $bis_re_rule_value_str = $bis_re_rule_value[$rows];
        $condition = (int) $bis_re_condition[$rows];
        $value_type_id = (int) $bis_re_sub_opt_type_id[$rows];

        // Check for Input Token
        if ($value_type_id == 1) {

            // If condition = equal or condition = not equal
            if (($condition == 1 || $condition == 2) ||
                    (($sub_option == 4 || $sub_option == 5 || $sub_option == 23 || $sub_option == 24 ||
                    $sub_option == 20 || $sub_option == 21 || $sub_option == 1 || $sub_option == 18 || $sub_option == 30 ||
                    $sub_option == 22 || $sub_option == 25 || $sub_option == 17 || $sub_option == 29) && ($condition == 12 || $condition == 14))) {
                $bis_re_rule_value_str = stripslashes($bis_re_rule_value_str);
            }
        }
        
        $option_id = (int) $bis_re_rule_option[$rows];

        if ($option_id == 9 || $option_id == 10 || $option_id == 3 || $option_id == 12
                || $option_id == 11) {
            $logical_rules_criteria_vo->set_evalType(BIS_EVAL_REQUEST_TYPE);
        }

        // Option is Request
        if ($option_id == 3 || $sub_option == 32) {

            // Filter the url
            if ($sub_option == 6 && ($condition == 1 || $condition == 2)) {
                $bis_re_rule_value_str = RulesEngineUtil::get_filter_url($bis_re_rule_value_str);
            }
            $bis_re_rule_value_str = json_encode(array(array("id" => $bis_re_rule_value_str)));
        }

        // Save Only Ids for Posts and Pages
        if ($option_id == 9 || $option_id == 10 || $option_id == 12) {
            $bis_re_rule_value_str = RulesEngineUtil::get_json_ids($bis_re_rule_value_str);
        }

        $logical_rules_criteria_vo->set_rightBracket($bis_re_right_bracket[$rows]);
        $logical_rules_criteria_vo->set_leftBracket($bis_re_left_bracket[$rows]);
        $logical_rules_criteria_vo->set_value($bis_re_rule_value_str);
        $logical_rules_criteria_vo->set_optionId($option_id);
        $logical_rules_criteria_vo->set_subOptionId($bis_re_sub_option[$rows]);
        $logical_rules_criteria_vo->set_conditionId($bis_re_condition[$rows]);
        $logical_rules_criteria_vo->set_ruleType($bis_re_rule_type);
        $logical_rules_criteria_vo->set_operatorId($bis_re_logical_op[$rows]);

        array_push($rule_criteria_array, $logical_rules_criteria_vo);
    }*/

    $bis_re_name = $_POST ['bis_re_name'];
    $bis_re_description = $_POST ['bis_re_description'];
    $bis_re_hook = trim($_POST ['bis_re_hook']);
    $bis_re_status = $_POST['bis_re_status'];
    /*$general_col1 = null;

    if (isset($_POST['bis_re_select_action'])) {
        $general_col1 = $_POST['bis_re_select_action'];
    }*/
    
    // Validation for hook
    if ($bis_re_hook != null && $bis_re_hook !== "" && !function_exists($bis_re_hook) === true) {
        $results_map = array();
        $results_map[BIS_STATUS] = BIS_ERROR;
        $results_map[BIS_MESSAGE_KEY] = BIS_NO_METHOD_FOUND;
        RulesEngineUtil::generate_json_response($results_map);
    }
    
    $logical_rules_engine_modal = new LogicalRulesEngineModel();
    
    /*$popUpVO = new PopUpVO();

    $show_popUp = null;
    $general_col2 = null;

    if (isset($_POST['bis_re_select_action']) == "show_as_dialog") {
        
        $show_popUp = 1;

        if (isset($_POST["bis_re_common_popup_title"])) {
            $popUpVO->setTitle($_POST["bis_re_common_popup_title"]);
        }

        if (isset($_POST["bis_re_common_auto_red"])) {
            $popUpVO->setAutoCloseTime(intval($_POST["bis_re_common_auto_red"]));
        } else {
            $popUpVO->setAutoCloseTime(0);
        }

        if (filter_input(INPUT_POST, "bis_re_common_popup_occur", FILTER_VALIDATE_INT)) {
            $popUpVO->setPopUpOccurr($_POST['bis_re_common_popup_occur']);
        }
        
        $all_countries = $logical_rules_engine_modal->get_all_countries();
        foreach ($all_countries as $all_country) {
            $total_countries[$all_country->value] = $all_country->display_name;
        }
        $geo_wrapper = new GeoPluginWrapper();
        $country_continent_map = $geo_wrapper->getCountryContinentMap();
        $continents = $geo_wrapper->getContinentMap();

        $template_file_name = $_POST["bis_re_tempalte"];
        $popUpVO->setTempalteFileName($template_file_name);
        if ($template_file_name == "bis-continent-country-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/popup-templates/" . $template_file_name);
            RulesEngineUtil::update_option(BIS_CONTINENT_COUNTRY_POPUP_TEMPLATE, $dynamicContent);
            if (isset($_POST["bis_re_select_country"])) {
                $countries = $_POST["bis_re_select_country"];
                $selectedCountries = array();
                foreach ($countries as $country) {
                    $continent_code = $country_continent_map[$country];
                    $continent = $continents[$continent_code];
                    if (array_key_exists($continent, $selectedCountries)) {
                        $exists = $selectedCountries[$continent];
                        $exists[$country] = explode("-", $total_countries[$country])[0];
                        $selectedCountries[$continent] = $exists;
                    } else {
                        $selectedCountries[$continent] = array($country => explode("-", $total_countries[$country])[0]);
                    }
                }
                $popUpVO->setSelectedCountries($countries);
                $popUpVO->setContinentCountryMap($selectedCountries);
            }
        } else if ($template_file_name == "bis-continent-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/popup-templates/" . $template_file_name);
            RulesEngineUtil::update_option(BIS_CONTINENT_POPUP_TEMPLATE, $dynamicContent);
            if (isset($_POST["bis_re_select_continent"])) {
                $continent_codes = $_POST["bis_re_select_continent"];
                foreach ($continent_codes as $continent_code) {
                    $selectedContinents[$continent_code] = $continents[$continent_code];
                }
                $popUpVO->setSelectedCountries($selectedContinents);
            }
        } else if ($template_file_name == "bis-country-flags-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/popup-templates/" . $template_file_name);
            RulesEngineUtil::update_option(BIS_COUNTRY_FLAGS_POPUP_TEMPLATE, $dynamicContent);
            if (isset($_POST["bis_re_select_country"])) {
                $countries = $_POST["bis_re_select_country"];
                foreach ($countries as $country) {
                    $selectedCountries[$country] = explode("-", $total_countries[$country])[0];
                }
                $popUpVO->setSelectedCountries($selectedCountries);
            }
        } else if ($template_file_name == "bis-country-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/popup-templates/" . $template_file_name);
            RulesEngineUtil::update_option(BIS_COUNTRY_POPUP_TEMPLATE, $dynamicContent);
            if (isset($_POST["bis_re_select_country"])) {
                $countries = $_POST["bis_re_select_country"];
                foreach ($countries as $country) {
                    $selectedCountries[$country] = explode("-", $total_countries[$country])[0];
                }
                $popUpVO->setSelectedCountries($selectedCountries);
            }
        } else if ($template_file_name == "bis-region-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/popup-templates/" . $template_file_name);
            RulesEngineUtil::update_option(BIS_REGION_POPUP_TEMPLATE, $dynamicContent);
        } else if ($template_file_name == "bis-city-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/popup-templates/" . $template_file_name);
            RulesEngineUtil::update_option(BIS_CITY_POPUP_TEMPLATE, $dynamicContent);
        }
        // else {
          //$file_path = RulesEngineUtil::get_file_upload_path() . BIS_LOGICAL_RULE_DIRECTORY . '/';
          //$dynamicContent = file_get_contents($file_path . $template_file_name);
          //}
        
        $general_col2 = json_encode($popUpVO);
    }*/

    $logical_rules_vo = new LogicalRulesVO($bis_re_name, $bis_re_description, 
            $bis_re_hook, $bis_re_status, $rule_criteria_array, $bis_re_eval_type, $bis_add_rule_type, $json_query);
    
    $logical_rules_vo->set_id($bis_re_rId);
    $logical_rules_vo->set_status($bis_re_status);

    $results_map = $logical_rules_engine_modal->update_rule_only($logical_rules_vo);

    $status = $results_map[BIS_STATUS];

    if ($status == BIS_SUCCESS) {
        RulesEngineCacheWrapper::set_reset_time(BIS_LOGICAL_RULE_RESET);
    }

    RulesEngineUtil::generate_json_response($results_map);
}

function bis_update_query_builder_logical_rule_new($rules_vo){
    
    RulesEngineCacheWrapper::delete_all_transient_cache();

    $bis_re_rId = $_POST ['bis_re_rId'];

    $json_query = stripslashes($_POST ['bis_jquery_result']);
    $array_query = json_decode($json_query, true);
    $bis_re_eval_type = $_POST ['bis_re_eval_type'];
    $bis_add_rule_type =  $_POST ['bis_add_rule_type'];
    
    $query_model = new QueryBuilderModel();
    $rule_criteria_array = $query_model->bis_quey_builder_function($array_query);
    
    $bis_re_name = $_POST ['bis_re_name'];
    $bis_re_description = $_POST ['bis_re_description'];
    $bis_re_hook = trim($_POST ['bis_re_hook']);
    $bis_re_status = $_POST['bis_re_rule_status'];

    /*if (isset($_POST['bis_re_city_lat']) && isset($_POST['bis_re_city_lng'])) {
        $bis_latitude = $_POST['bis_re_city_lat'];
        $bis_longitude = $_POST['bis_re_city_lng'];
        $bis_re_city_radius = $_POST['bis_re_city_radius'];
        $bis_radius_unit = $_POST['bis_re_mi_km'];
    } else {
        $bis_latitude = '';
        $bis_longitude = '';
        $bis_re_city_radius = '';
        $bis_radius_unit = '';
    }
    $near_by_cities = null;*/
    // Validation for hook
    if ($bis_re_hook != null && $bis_re_hook !== "" && !function_exists($bis_re_hook) === true) {
        $results_map = array();
        $results_map[BIS_STATUS] = BIS_ERROR;
        $results_map[BIS_MESSAGE_KEY] = BIS_NO_METHOD_FOUND;
        RulesEngineUtil::generate_json_response($results_map);
    }
    
    $logical_rules_engine_modal = new LogicalRulesEngineModel();

    /*if ($bis_longitude != '' && $bis_latitude != '') {
        $cities = $logical_rules_engine_modal->get_nearby_cities($bis_latitude, $bis_longitude, $bis_re_city_radius);
        $near_cities = array(
            'latitude' => $bis_latitude,
            'longitude' => $bis_longitude,
            'city_radius' => $bis_re_city_radius,
            'near_by_cities' => $cities,
            'radius_unit' => $bis_radius_unit
        );
        $near_by_cities = json_encode($near_cities);
    }*/

    $logical_rules_vo = new LogicalRulesVO($bis_re_name, $bis_re_description, $bis_re_hook,
            $bis_re_status, $rule_criteria_array, $bis_re_eval_type, $bis_add_rule_type, $json_query);
    
    // Call to model to logical rules
    
    $logical_rules_vo->set_id($bis_re_rId);

    if (isset($_POST ['bis_re_status'])) {
        $logical_rules_vo->set_status($_POST ['bis_re_status']);
    }
    
    if ($bis_re_status !== NULL) {
        $logical_rules_vo->set_status($bis_re_status);
    }
    $results_map = $logical_rules_engine_modal->update_rule($logical_rules_vo);

    $status = $results_map[BIS_STATUS];

    RulesEngineUtil::update_rule_cache_id(BIS_LOGICAL_CACHE_REFRESH_ID);
    
    if ($status == BIS_SUCCESS) {
        $results_map = $logical_rules_engine_modal->update_child_rule($rules_vo);
        $status = $results_map[BIS_STATUS];
    }

    RulesEngineCacheWrapper::remove_session_attribute(BIS_PLUGIN_TYPE_VALUE);
    RulesEngineUtil::generate_json_response($results_map);
}

/**
 *
 * Updates the logical rule.
 *
 */
function bis_re_update_rule_new($rules_vo, $bis_criteria = null, $fun_call = null) {

    RulesEngineCacheWrapper::delete_all_transient_cache();
    
    $rule_criteria_array = array();
    if ($bis_criteria != null && $fun_call !== "user_based") {

        $bis_sub_opt[0] = $bis_criteria[0];
        $bis_re_sub_option = $bis_sub_opt;
        $bis_re_condition[0] = "12";
        $bis_re_rule_value[0] = $bis_criteria[2];
        $bis_re_left_bracket = "0";
        $bis_re_right_bracket = "0";
        $sub_criteria = explode("_", $bis_criteria[0])[1];
        $bis_re_rId = $rules_vo->ruleId;

        $value_type_id = "1";
        if ($sub_criteria === "9" || $sub_criteria === "14" || $sub_criteria === "3" || $sub_criteria === "10") {
            $value_type_id = "3";
            $bis_re_condition[0] = "1";
        } else if ($sub_criteria === "6" || $sub_criteria === "15" || $sub_criteria === "35" || $sub_criteria === "27" || $sub_criteria === "26" || $sub_criteria === "31") {
            $value_type_id = "4";
            $bis_re_condition[0] = "1";
        } else if ($sub_criteria === "19" || $sub_criteria === "32" || $sub_criteria === "16" || $sub_criteria === "11" || $sub_criteria === "12" || $sub_criteria === "13" || $sub_criteria === "33" || $sub_criteria === "7" || $sub_criteria === "28") {
            $value_type_id = "2";
            $bis_re_condition[0] = "1";
            $bis_re_rule_value[0] = $bis_criteria[1];
        }
        $bis_re_sub_opt_type_id[0] = $value_type_id;
        $bis_re_eval_type = "1";
        $bis_add_rule_type = "1";
        $bis_re_status = "1";
    }  elseif ($fun_call === "user_based") {
        $bis_re_sub_option = $bis_criteria['bis_re_sub_option'];
        $bis_re_condition = $bis_criteria['bis_re_condition'];
        $bis_re_rule_value = $bis_criteria ['bis_re_rule_value'];
        $bis_re_left_bracket = $bis_criteria ['bis_re_left_bracket'];
        $bis_re_right_bracket = $bis_criteria ['bis_re_right_bracket'];
        $bis_re_sub_opt_type_id = $bis_criteria ['bis_re_sub_opt_type_id'];
        $bis_re_rId = $bis_criteria ['bis_re_rId'];
        $bis_re_eval_type = $bis_criteria ['bis_re_eval_type'];
        $bis_re_rcId = $bis_criteria ['bis_re_rcId'];
        $bis_add_rule_type = $bis_criteria ['bis_add_rule_type'];
        $bis_re_status = $bis_criteria['bis_re_rule_status'];
    } else {
        $bis_re_status = $_POST['bis_re_rule_status'];
        $bis_re_condition = $_POST ['bis_re_condition'];
        $bis_re_rule_value = $_POST ['bis_re_rule_value'];
        $bis_re_sub_option = $_POST ['bis_re_sub_option'];
        $bis_re_sub_opt_type_id = $_POST ['bis_re_sub_opt_type_id'];
        $bis_re_rId = $_POST ['bis_re_rId'];
        $bis_re_eval_type = $_POST ['bis_re_eval_type'];
        $bis_re_rcId = $_POST ['bis_re_rcId'];
        $bis_add_rule_type = $_POST ['bis_add_rule_type'];
        $bis_re_delete = $_POST['bis_re_delete'];
    }

    $bis_re_rule_type = "option";
       
    $logical_rules_engine_modal = new LogicalRulesEngineModel();

    // Loop over the list of rule criteria
    $rules_count = count($bis_re_sub_option);
    for ($rows = 0; $rows < $rules_count; $rows++) {

        $logical_rules_criteria_vo = new LogicalRulesCriteriaVO();
        
        $options = explode("_", $bis_re_sub_option[$rows]);
        $option_id = (int) $options[0];
        $sub_option = (int) $options[1];

        if (isset($bis_re_rcId[$rows])) {
            $logical_rules_criteria_vo->set_Id($bis_re_rcId[$rows]);
        }

        $bis_re_rule_value_str = $bis_re_rule_value[$rows];
        $condition = (int) $bis_re_condition[$rows];
        $value_type_id = (int) $bis_re_sub_opt_type_id[$rows];

        // Check for Input Token
        if ($value_type_id == 1) {

            // If condition = equal or condition = not equal
            if (($condition == 1 || $condition == 2) ||
                    (($sub_option == 2 || $sub_option == 4 || $sub_option == 5 || $sub_option == 23 || $sub_option == 24 ||
                    $sub_option == 20 || $sub_option == 21 || $sub_option == 1 || $sub_option == 18 || $sub_option == 30 ||
                    $sub_option == 22 || $sub_option == 25 || $sub_option == 17 || $sub_option == 29) && ($condition == 12 || $condition == 14 || $condition == 15))) {
                $bis_re_rule_value_str = stripslashes($bis_re_rule_value_str);
                if (isset($_POST["bis_city_radius_" . $rows . "_value_1"]) && isset($_POST["bis_city_radius_units_" . $rows . "_value_2"]) 
                        && isset($_POST["bis_city_latitude_" . $rows . "_value_3"]) && isset($_POST["bis_city_longitude_" . $rows . "_value_4"])) {
                    $city_radius = $_POST["bis_city_radius_" . $rows . "_value_1"];
                    $radius_units = $_POST["bis_city_radius_units_" . $rows . "_value_2"];
                    $city_latitude = $_POST["bis_city_latitude_" . $rows . "_value_3"];
                    $city_longitude = $_POST["bis_city_longitude_" . $rows . "_value_4"];
                    $cities = $logical_rules_engine_modal->get_nearby_cities($city_latitude, $city_longitude, $city_radius, $radius_units);
                    $lat_lng_rad_units = array(
                        'latitude' => $city_latitude,
                        'longitude' => $city_longitude,
                        'city_radius' => $city_radius,
                        'radius_unit' => $radius_units
                    );
                    $lat_lng_rad_units_json = json_encode((array) $lat_lng_rad_units);
                    $near_by_cities = json_encode((array) $cities);
                    $logical_rules_criteria_vo->set_general_col1($lat_lng_rad_units_json);
                    $logical_rules_criteria_vo->set_general_col2($near_by_cities);
                }
            }
        }
        
        //distance units
        if($sub_option == 44){
            $distance_units = $_POST["bis_distance_units_" . $rows . "_value_1"];
            $logical_rules_criteria_vo->set_general_col2($distance_units);
        }
        
        // Hours based condtion
        if ($option_id == 7 && $sub_option == 46) {
            $current_date = date('Y-m-d H:i', current_time('timestamp', 0));
            if (RulesEngineUtil::isContains($bis_re_rule_value_str, ":")) {
                $bis_hrs_mints = explode(":", $bis_re_rule_value_str);
                $bis_hours = $bis_hrs_mints[0];
                $bis_minutes = $bis_hrs_mints[1];
            } else {
                $bis_hours = 0;
                $bis_minutes = 0;
            }
            $cenvertedTime = date('Y-m-d H:i:s', strtotime('+'. $bis_hours .' hour +'. $bis_minutes .' minutes', strtotime($current_date)));
            $logical_rules_criteria_vo->set_general_col2($cenvertedTime);
        }
        
        if ($option_id == 9 || $option_id == 10 || $option_id == 3 || $option_id == 12 || $option_id == 11) {
            $logical_rules_criteria_vo->set_evalType(BIS_EVAL_REQUEST_TYPE);
        }

        // Option is Request
        if ($option_id == 3 || $sub_option == 32) {

            // Filter the url
            if ($sub_option == 6 && ($condition == 1 || $condition == 2)) {
                $bis_re_rule_value_str = RulesEngineUtil::get_filter_url($bis_re_rule_value_str);
            }
            $bis_re_rule_value_str = json_encode(array(array("id" => $bis_re_rule_value_str)));
        }

        // Save Only Ids for Posts and Pages
        if ($option_id == 9 || $option_id == 10 || $option_id == 12) {
            $bis_re_rule_value_str = RulesEngineUtil::get_json_ids($bis_re_rule_value_str);
        }

        if(isset($_POST ['bis_re_right_bracket'])) {
            $bis_re_right_bracket = $_POST ['bis_re_right_bracket'];
            $logical_rules_criteria_vo->set_rightBracket($bis_re_right_bracket[$rows]);
        }
        
        if(isset($_POST ['bis_re_left_bracket'])) {
            $bis_re_left_bracket = $_POST ['bis_re_left_bracket'];
            $logical_rules_criteria_vo->set_leftBracket($bis_re_left_bracket[$rows]);
        }
        
        if($sub_option == 2000) {
            $gen_col1 = $bis_re_sub_option[$rows];
            $logical_rules_criteria_vo->set_general_col1($gen_col1);
        } 
        
        $logical_rules_criteria_vo->set_value($bis_re_rule_value_str);
        $logical_rules_criteria_vo->set_optionId($option_id);
        $logical_rules_criteria_vo->set_subOptionId($sub_option);
        $logical_rules_criteria_vo->set_conditionId($bis_re_condition[$rows]);
        $logical_rules_criteria_vo->set_ruleType($bis_re_rule_type);
        
        // Add operator not to the last row.
        if ($rows < ($rules_count - 1)) {
            if ($bis_add_rule_type == 1) {
                $logical_rules_criteria_vo->set_operatorId(1);
            } else {
                $bis_re_logical_op = $_POST ['bis_re_logical_op'];
                $logical_rules_criteria_vo->set_operatorId($bis_re_logical_op[$rows]);
            }
        } else {
            $logical_rules_criteria_vo->set_operatorId(0);
        }
        
        array_push($rule_criteria_array, $logical_rules_criteria_vo);
    }

    if ($bis_criteria != null) {
        $bis_re_name = $rules_vo->name;
        $bis_re_description = $rules_vo->description;
        $bis_re_hook = "";
    } else {
        $bis_re_name = $_POST ['bis_re_name'];
        $bis_re_description = $_POST ['bis_re_description'];
        $bis_re_hook = trim($_POST ['bis_re_hook']);
    }
    
    /*if (isset($_POST['bis_re_city_lat']) && isset($_POST['bis_re_city_lng'])) {
        $bis_latitude = $_POST['bis_re_city_lat'];
        $bis_longitude = $_POST['bis_re_city_lng'];
        $bis_re_city_radius = $_POST['bis_re_city_radius'];
        $bis_radius_unit = $_POST['bis_re_mi_km'];
    } else {
        $bis_latitude = '';
        $bis_longitude = '';
        $bis_re_city_radius = '';
        $bis_radius_unit = '';
    }
    $near_by_cities = null;*/
    $json_query = null;
    // Validation for hook
    if ($bis_re_hook != null && $bis_re_hook !== "" && !function_exists($bis_re_hook) === true) {
        $results_map = array();
        $results_map[BIS_STATUS] = BIS_ERROR;
        $results_map[BIS_MESSAGE_KEY] = BIS_NO_METHOD_FOUND;
        RulesEngineUtil::generate_json_response($results_map);
    }

    /*if ($bis_longitude != '' && $bis_latitude != '') {
        $cities = $logical_rules_engine_modal->get_nearby_cities($bis_latitude, $bis_longitude, $bis_re_city_radius);
        $near_cities = array(
            'latitude' => $bis_latitude,
            'longitude' => $bis_longitude,
            'city_radius' => $bis_re_city_radius,
            'near_by_cities' => $cities,
            'radius_unit' => $bis_radius_unit
        );
        $near_by_cities = json_encode($near_cities);
    }*/
    
    $logical_rules_vo = new LogicalRulesVO($bis_re_name, $bis_re_description, 
            $bis_re_hook, $bis_re_status, $rule_criteria_array, $bis_re_eval_type, $bis_add_rule_type, $json_query);
    

    $logical_rules_vo->set_id($bis_re_rId);
    
    if (isset($_POST ['bis_re_status'])) {
        $logical_rules_vo->set_status($_POST ['bis_re_status']);
    }
    
    if ($bis_re_status !== NULL) {
        $logical_rules_vo->set_status($bis_re_status);
    }
    $results_map = $logical_rules_engine_modal->update_rule($logical_rules_vo);

    $status = $results_map[BIS_STATUS];

    RulesEngineUtil::update_rule_cache_id(BIS_LOGICAL_CACHE_REFRESH_ID);
    
    if ($status == BIS_SUCCESS) {
        RulesEngineCacheWrapper::set_reset_time(BIS_LOGICAL_RULE_RESET);
        $results_map = $logical_rules_engine_modal->update_child_rule($rules_vo);
        $status = $results_map[BIS_STATUS];
    }

    RulesEngineCacheWrapper::remove_session_attribute(BIS_PLUGIN_TYPE_VALUE);
    if ($bis_criteria == null) {
        RulesEngineUtil::generate_json_response($results_map);
    }
}

/**
 * This method shows the add new rule page.
 *
 */
function bis_re_new_rule() {

    $nonce = $_GET ['bis_nonce'];

    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce'))
        RulesEngineUtil::handle_request_forgery_error();

    $pluginPath = RulesEngineUtil::getIncludesDirPath();
    include $pluginPath . "logical-rules-add.php";

    flush();
    exit;
}

function bis_re_edit_rule() {

    $nonce = $_GET ['bis_nonce'];

    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }

    $pluginPath = RulesEngineUtil::getIncludesDirPath();

    include $pluginPath . "logical-rules-edit.php";

    flush();
    exit();
}

/**
 * Method used for deleting a single rule
 */
function bis_re_logical_rule_action() {

    $nonce = $_GET ['bis_nonce'];

    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }

    header("Content-Type: application/json");

    $logical_rules_engine = new LogicalRulesEngineModel ();

    $ruleId = $_GET ['ruleId'];
    
    $selectedAction = $_GET ['selectedAction'];
     
    $bis_logical_rules_map = $logical_rules_engine->logical_rule_action($ruleId, $selectedAction);

    if($selectedAction === "delete"){
        
        $status = $bis_logical_rules_map["status"];

        if ($status === "success") {
            $data = array("status" => "success", "data" => $bis_logical_rules_map["data"]);
        } else if ($status === "childs_rules_exists") {
            $data = array("status" => "error", "data" => BIS_MESSAGE_LOGICAL_RULE_DELETE_FAILED);
        } else {
            $data = array("status" => "error", "data" => BIS_MESSAGE_NO_RECORD_FOUND);
        }
    }else{
        $data = array("status" => "success", "data" => $bis_logical_rules_map["data"]);
    }
    echo json_encode($data);

    flush();
    exit();
}

function bis_re_get_value() {

    $nonce = $_POST ['bis_nonce'];

    // generated nonce we created earlier
    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }

    header("Content-Type: application/json");

    // Get the logical rule here
    $autoValue = $_POST ['q'];
    $subCriteria = (int) $_POST ['subcriteria'];
    
    $valArr = array();
    $count = 0;
    $rules_engine = new LogicalRulesEngineModel();

    switch ($subCriteria) {
        case 1: // User Role
            $bis_re_editable_roles = get_editable_roles();
            foreach ($bis_re_editable_roles as $key=>$role) {
                if (stripos($role['name'], $autoValue) !== false) {
                    $valArr[$count++] = array('id' => $key, 'name' => $role['name']);
                }
            }
            break;

        case 2: // Email
            $rows = $rules_engine->get_user_emails();

            foreach ($rows as $email) {
                // Check if email exists in the list
                if (stripos($email->user_email, $autoValue) !== false) {
                    $valArr[$count++] = array('id' => $email->user_email, 'name' => $email->user_email);
                }
            }
            break;

        case 4: // Country Code
        case 37: // Continent Country Template
        case 38: // Country Flags Template
        case 40: // Country Template
            $valArr = RulesEngineLocalization::get_countries($autoValue);
            break;
        
        case 20: // Continent
        case 39: // Continent Template
            $valArr = RulesEngineLocalization::get_continents($autoValue);
            break;

        case 5: // Currency
        case 23: // Week days
        case 24: // Months
            $valArr = $rules_engine->get_rule_values_by_display_name($subCriteria, $autoValue);
            $valArr = RulesEngineLocalization::get_localized_values($valArr);
            break;

        case 21: // Page Rule
            $args = array(
                'offset' => 0,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_type' => 'page',
                'post_status' => 'publish',
                'suppress_filters' => true
            );

            $rows = get_pages($args);

            $count = 0;

            if (($rows != null) && (!empty($rows))) {
                foreach ($rows as $page) {

                    if (stripos($page->post_title, $autoValue) !== false) {
                        $valArr[$count++] = array('id' => $page->ID, 'name' => $page->post_title);
                    }
                }
            }

            break;

        case 22: // Post Rule
            $post_rule_modal = new PostRulesEngineModel();
            $posts_type_list = $post_rule_modal->get_post_types();
            $count =0;
            foreach ($posts_type_list as $post) {
                $type = $post->get_name();
                $args = array(
                    'offset' => 0,
                    'orderby' => 'post_date',
                    'order' => 'DESC',
                    'post_type' => $type,
                    'post_status' => 'publish',
                    'suppress_filters' => true,
                    'posts_per_page' => -1
                );

                $rows[$count++] = get_posts($args);
            }
            $count = 0;
            
            if (($rows != null) && (!empty($rows))) {
                foreach ($rows as $key ) {
                    foreach ($key as $key1 => $post) {     
                        // Check if email exists in the list
                        if (stripos($post->post_title, $autoValue) !== false) {
                            $valArr[$count++] = array('id' => $post->ID, 'name' => $post->post_title);
                        }
                    }
                }
            }

            break;

        case 17: // wordpress category
            $args = array(
                'orderby' => 'name',
                'order' => 'ASC',
                'hide_empty' => FALSE,
                'name__like' => $autoValue
            );

            $taxonomy = 'category';
            $categories = get_terms($taxonomy, $args);

            if (!empty($categories)) {
                foreach ($categories as $category) {
                    $valArr[$count++] = array('id' => $category->term_id, 'name' => $category->name);
                }
            }

            break;
        case 18: // userId
            $rows = $rules_engine->get_userIds();

            foreach ($rows as $user) {
                // Check if email exists in the list
                if (stripos($user->user_login, $autoValue) !== false) {
                    $valArr[$count++] = array('id' => $user->user_login, 'name' => $user->user_login);
                }
            }

            break;

        case 25:  // WooCommerce category

            $args = array(
                'orderby' => 'name',
                'order' => 'ASC',
                'hide_empty' => FALSE,
                'name__like' => $autoValue
            );

            $taxonomy = 'product_cat';
            $categories = get_terms($taxonomy, $args);

            if (!empty($categories)) {
                foreach ($categories as $category) {
                    $valArr[$count++] = array('id' => $category->term_id, 'name' => $category->name);
                }
            }
            break;
            
        case 41: // Search Shortcode
            $bis_geoname_user = RulesEngineUtil::get_option(BIS_GEO_NAME_USER);
            $url = "https://secure.geonames.org/searchJSON?userName=" . $bis_geoname_user . "&lang=en&maxRows=12&name_startsWith=" . $autoValue;
            $response = RulesEngineUtil::get_url_content($url);

            if ($response != null) {
                $j_response = json_decode($response);
                $cities = $j_response->geonames;

                if (!empty($cities)) {
                    foreach ($cities as $citi) {
                        if($citi->fcl === "P") {
                            $display_name = $citi->name . ", " . $citi->adminName1 . ", " . $citi->countryName;
                            $valArr[$count++] = array('id' => $citi->name, 'name' => $display_name, 'lng' => $citi->lng, 'lat' => $citi->lat);
                        } else if($citi->fcl === "A" && $citi->fcode === "ADM1") {
                            $display_name = $citi->name . ", " . $citi->countryName;
                            $valArr[$count++] = array('id' => $citi->name, 'name' => $display_name);
                        }
                    }
                    $countries = RulesEngineLocalization::get_countries($autoValue);
                    foreach ($countries as $country) {
                        $valArr[$count++] = $country;
                    }
                }
            }
            break;
        
        case 42: // City Template
        case 29:  // City
            $bis_geoname_user = RulesEngineUtil::get_option(BIS_GEO_NAME_USER);
            $url = "https://secure.geonames.org/searchJSON?userName=" . $bis_geoname_user . "&lang=en&featureClass=P&maxRows=12&name_startsWith=" . $autoValue;
            $response = RulesEngineUtil::get_url_content($url);

            if ($response != null) {
                $j_response = json_decode($response);
                $cities = $j_response->geonames;

                if (!empty($cities)) {
                    foreach ($cities as $citi) {
                        $display_name = $citi->name . ", " . $citi->adminName1 . ", " . $citi->countryName;
                        $valArr[$count++] = array('id' => $citi->name, 'name' => $display_name, 'lng' => $citi->lng, 'lat'=> $citi->lat);
                    }
                }
            }
            break;
            
        case 43: // Region or state Template
        case 30:  // Region or state
            //userName=tompi
            // State or Region
            $bis_geoname_user = RulesEngineUtil::get_option(BIS_GEO_NAME_USER);
            $url = "https://secure.geonames.org/searchJSON?userName=" . $bis_geoname_user . "&lang=en&featureClass=A&fcode=ADM1&maxRows=12&name_startsWith=" . $autoValue;
            $response = RulesEngineUtil::get_url_content($url);

            if ($response != null) {
                $j_response = json_decode($response);
                $cities = $j_response->geonames;

                if (!empty($cities)) {
                    foreach ($cities as $citi) {
                        $display_name = $citi->name . ", " . $citi->countryName;
                        $valArr[$count++] = array('id' => $citi->name, 'name' => $display_name);
                    }
                }
            }
            break;
        
        case 2000: 
            $ssubOptionId = $_POST['ssubCriteria'];
           
            // Add pa to indicate product attributes
            $ssubOptionId = 'pa_'.$ssubOptionId;
            
            $terms = get_terms(array(
                'taxonomy' => $ssubOptionId,
                'hide_empty' => false
                    ));

            if (!empty($terms) && !is_wp_error($terms)) {
                foreach ($terms as $term) {
                    if (RulesEngineUtil::isContains($term->name, $autoValue)) {
                        $valArr[$count++] = array('id' => $term->term_id, 'name' => $term->name);
                    }
                }
            }
            break;
            
        case 34:
            
            $woo_tags = get_terms(array(
                'hide_empty' => false, // only if you want to hide false
                'taxonomy' => 'product_tag',
                    )
            );
            
            if (!empty($woo_tags) && !is_wp_error($woo_tags)) {
                foreach ($woo_tags as $woo_tag) {
                    if (RulesEngineUtil::isContains($woo_tag->name, $autoValue)) {
                        $valArr[$count++] = array('id' => $woo_tag->term_id, 'name' => $woo_tag->name);
                    }
                }
            }
            break;
        case 36:
            
            $wp_tags = get_terms(array(
                'hide_empty' => false, // only if you want to hide false
                'taxonomy' => 'post_tag',
                    )
            );
            
            if (!empty($wp_tags) && !is_wp_error($wp_tags)) {
                foreach ($wp_tags as $wp_tag) {
                    if (RulesEngineUtil::isContains($wp_tag->name, $autoValue)) {
                        $valArr[$count++] = array('id' => $wp_tag->term_id, 'name' => $wp_tag->name);
                    }
                }
            }
            break;
    } // End of switch

    $jsonRoles = json_encode($valArr);

    echo $jsonRoles;
    flush();
    exit();
}

function bis_get_logical_rules() {
    
    $page_start_index = RulesEngineUtil::get_start_page_index();
    // Get the logical rule here
    $logical_rules_engine = new LogicalRulesEngineModel ();
    $results_map = $logical_rules_engine->get_logical_rules_only($page_start_index);

    RulesEngineUtil::generate_json_response($results_map);
}

function bis_get_sub_options() {

    // Get the logical rule here
    $optionId = $_POST ['optionId'];
    $plugin_type_value = $_POST['plugin_type_value'];
    header("Content-Type: application/json");

    $logical_rules_engine = new LogicalRulesEngineModel ();
    $bis_sub_options = $logical_rules_engine->get_rules_sub_options($optionId, $plugin_type_value);

    if ($bis_sub_options != null) {
        $bis_sub_options = RulesEngineLocalization::get_localized_values($bis_sub_options);
        $bis_sub_options = json_encode($bis_sub_options);
    } else {
        // NO data found
    }

    echo $bis_sub_options;
    flush();
    exit();
}

function bis_get_conditions() {
    $optionId = $_POST ['optionId'];
    
    header("Content-Type: application/json");

    $logical_rules_engine = new LogicalRulesEngineModel ();
    $rules_conditions = $logical_rules_engine->get_rules_conditions($optionId);

    if ($rules_conditions != null) {
        $rules_conditions["RuleConditions"] = RulesEngineLocalization::get_localized_values($rules_conditions["RuleConditions"]);
        $rules_conditions = json_encode($rules_conditions);
    } else {
        // NO data found
    }

    echo $rules_conditions;
    flush();
    exit();
}

/**
 * This method is used to save the logical Rule
 *
 */
function bis_create_logical_rule() {
    
    RulesEngineCacheWrapper::delete_all_transient_cache();
      
    $nonce = $_POST ['bis_rules_engine_nonce'];
   
    // check to see if the submitted nonce matches with the
    // generated nonce we created earlier
    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }
    
    $json_query = stripslashes($_POST ['bis_jquery_result']);
    $array_query = json_decode($json_query, true);
    $bis_re_eval_type = $_POST ['bis_re_eval_type'];
    $bis_add_rule_type = 4;

    $query_model = new QueryBuilderModel();
    $rule_criteria_array = $query_model->bis_quey_builder_function($array_query);
    
    /*$rule_criteria_array = array();

    $bis_re_sub_option = $_POST ['bis_re_sub_option'];
    
    if(RulesEngineUtil::isContains($bis_re_sub_option[0], "_")) {
        $bis_re_options = explode("_", $bis_re_sub_option);
        $bis_re_sub_option = $bis_re_options[0];
        $bis_re_rule_option = $bis_re_options[1];
    } else {
        $bis_re_rule_option = $_POST ['bis_re_rule_option'];
    }
    
    $bis_re_condition = $_POST ['bis_re_condition']; 
    $bis_re_rule_value = $_POST ['bis_re_rule_value'];
    $bis_re_logical_op = $_POST ['bis_re_logical_op'];
    $bis_re_left_bracket = $_POST ['bis_re_left_bracket'];
    $bis_re_right_bracket = $_POST ['bis_re_right_bracket'];
    $bis_re_sub_opt_type_id = $_POST ['bis_re_sub_opt_type_id'];
    $bis_re_eval_type = $_POST ['bis_re_eval_type'];

    $bis_re_rule_type = "option";
    // Loop over the list of rule criteria
    for ($rows = 0; $rows < count($bis_re_rule_option); $rows++) {

        $logical_rules_criteria_vo = new LogicalRulesCriteriaVO();
        $sub_option = (int) $bis_re_sub_option[$rows];
        $bis_re_rule_value_str = $bis_re_rule_value[$rows];
        $condition = (int) $bis_re_condition[$rows];
        $value_type_id = (int) $bis_re_sub_opt_type_id[$rows];

        // Check for Input Token
        if ($value_type_id == 1) {

            // If condition = equal or condition = not equal
            if (($condition == 1 || $condition == 2) ||
               (($sub_option == 4 || $sub_option == 5 || $sub_option == 23 || $sub_option == 24 || 
                 $sub_option == 20 || $sub_option == 21 || $sub_option == 1 || $sub_option == 18 || $sub_option == 30 ||
                 $sub_option == 22 || $sub_option == 25 || $sub_option == 17 || $sub_option == 29) && ($condition == 12 || $condition == 14))) {
                $bis_re_rule_value_str = stripslashes($bis_re_rule_value_str);
            }
        }

        $option_id = (int) $bis_re_rule_option[$rows];

        if ($option_id == 9 || $option_id == 10 || $option_id == 3 || $option_id == 12
                || $option_id == 11) {
            $logical_rules_criteria_vo->set_evalType(BIS_EVAL_REQUEST_TYPE);
        }

        // Option is Request
        if ($option_id == 3 || $sub_option == 32) {

            // Filter the url
            if ($sub_option == 6 && ($condition == 1 || $condition == 2)) {
                $bis_re_rule_value_str = RulesEngineUtil::get_filter_url($bis_re_rule_value_str);
            }
            $bis_re_rule_value_str = json_encode(array(array("id" => $bis_re_rule_value_str)));
        }

        // Save Only Ids for Posts and Pages
        if ($option_id == 9 || $option_id == 10 || $option_id == 12) {
            $bis_re_rule_value_str = RulesEngineUtil::get_json_ids($bis_re_rule_value_str);
        }

        if(isset($bis_re_right_bracket[$rows])) {
            $logical_rules_criteria_vo->set_rightBracket($bis_re_right_bracket[$rows]);
        }
        
        if(isset($bis_re_left_bracket[$rows])) {
            $logical_rules_criteria_vo->set_leftBracket($bis_re_left_bracket[$rows]);
        }
       
        $logical_rules_criteria_vo->set_value($bis_re_rule_value_str);
        $logical_rules_criteria_vo->set_optionId($option_id);
        $logical_rules_criteria_vo->set_subOptionId($bis_re_sub_option[$rows]);
        $logical_rules_criteria_vo->set_conditionId($bis_re_condition[$rows]);
        $logical_rules_criteria_vo->set_ruleType($bis_re_rule_type);
        $logical_rules_criteria_vo->set_operatorId($bis_re_logical_op[$rows]);

        array_push($rule_criteria_array, $logical_rules_criteria_vo);
    }*/

    $bis_re_name = $_POST ['bis_re_name'];
    $bis_re_description = $_POST ['bis_re_description'];
    $bis_re_hook = trim($_POST ['bis_re_hook']);
    $bis_re_status = $_POST['bis_re_status'];
    /*$general_col1 = null;

    if(isset($_POST['bis_re_select_action'])) {
        $general_col1 = $_POST['bis_re_select_action'];
    }*/
    
    // Validation for hook
    if ($bis_re_hook != null && $bis_re_hook !== "" && !function_exists($bis_re_hook) === true) {
        $results_map = array();
        $results_map[BIS_STATUS] = BIS_ERROR;
        $results_map[BIS_MESSAGE_KEY] = BIS_NO_METHOD_FOUND;
        RulesEngineUtil::generate_json_response($results_map);
    }
    
    // Call to model to logical rules
    $logical_rules_engine_modal = new LogicalRulesEngineModel();
    
    /*$popUpVO = new PopUpVO();

    $show_popUp = null;
    $general_col2 = null;

    if (isset($_POST['bis_re_select_action']) == "show_as_dialog") {

        $show_popUp = 1;

        if (isset($_POST["bis_re_common_popup_title"])) {
            $popUpVO->setTitle($_POST["bis_re_common_popup_title"]);
        }

        if (isset($_POST["bis_re_common_auto_red"])) {
            $popUpVO->setAutoCloseTime(intval($_POST["bis_re_common_auto_red"]));
        } else {
            $popUpVO->setAutoCloseTime(0);
        }

        if (filter_input(INPUT_POST, "bis_re_common_popup_occur", FILTER_VALIDATE_INT)) {
            $popUpVO->setPopUpOccurr($_POST['bis_re_common_popup_occur']);
        }
        
        $all_countries = $logical_rules_engine_modal->get_all_countries();
        foreach ($all_countries as $all_country) {
            $total_countries[$all_country->value] = $all_country->display_name;
        }
        $geo_wrapper = new GeoPluginWrapper();
        $country_continent_map = $geo_wrapper->getCountryContinentMap();
        $continents = $geo_wrapper->getContinentMap();

        $template_file_name = $_POST["bis_re_tempalte"];
        $popUpVO->setTempalteFileName($template_file_name);
        if ($template_file_name == "bis-continent-country-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/popup-templates/" . $template_file_name);
            RulesEngineUtil::update_option(BIS_CONTINENT_COUNTRY_POPUP_TEMPLATE, $dynamicContent);
            if (isset($_POST["bis_re_select_country"])) {
                $countries = $_POST["bis_re_select_country"];
                $selectedCountries = array();
                foreach ($countries as $country) {
                    $continent_code = $country_continent_map[$country];
                    $continent = $continents[$continent_code];
                    if (array_key_exists($continent, $selectedCountries)) {
                        $exists = $selectedCountries[$continent];
                        $exists[] = explode("-", $total_countries[$country])[0];
                        $selectedCountries[$continent] = $exists;
                    } else {
                        $selectedCountries[$continent] = array(explode("-", $total_countries[$country])[0]);
                    }
                }
                $popUpVO->setSelectedCountries($countries);
                $popUpVO->setContinentCountryMap($selectedCountries);
            }
        } else if ($template_file_name == "bis-continent-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/popup-templates/" . $template_file_name);
            RulesEngineUtil::update_option(BIS_CONTINENT_POPUP_TEMPLATE, $dynamicContent);
            if(isset($_POST["bis_re_select_continent"])) {
                $continent_codes = $_POST["bis_re_select_continent"];
                foreach ($continent_codes as $continent_code) {
                    $selectedContinents[$continent_code] = $continents[$continent_code];
                }
                $popUpVO->setSelectedCountries($selectedContinents);
            }
        } else if ($template_file_name == "bis-country-flags-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/popup-templates/" . $template_file_name);
            RulesEngineUtil::update_option(BIS_COUNTRY_FLAGS_POPUP_TEMPLATE, $dynamicContent);
            if (isset($_POST["bis_re_select_country"])) {
                $countries = $_POST["bis_re_select_country"];
                foreach ($countries as $country) {
                    $selectedCountries[$country] = explode("-", $total_countries[$country])[0];
                }
                $popUpVO->setSelectedCountries($selectedCountries);
            }
        } else if ($template_file_name == "bis-country-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/popup-templates/" . $template_file_name);
            RulesEngineUtil::update_option(BIS_COUNTRY_POPUP_TEMPLATE, $dynamicContent);
            if (isset($_POST["bis_re_select_country"])) {
                $countries = $_POST["bis_re_select_country"];
                foreach ($countries as $country) {
                    $selectedCountries[$country] = explode("-", $total_countries[$country])[0];
                }
                $popUpVO->setSelectedCountries($selectedCountries);
            }
        } else if ($template_file_name == "bis-region-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/popup-templates/" . $template_file_name);
            RulesEngineUtil::update_option(BIS_REGION_POPUP_TEMPLATE, $dynamicContent);
        } else if ($template_file_name == "bis-city-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/popup-templates/" . $template_file_name);
            RulesEngineUtil::update_option(BIS_CITY_POPUP_TEMPLATE, $dynamicContent);
        }
        // else {
          //$file_path = RulesEngineUtil::get_file_upload_path() . BIS_LOGICAL_RULE_DIRECTORY . '/';
          //$dynamicContent = file_get_contents($file_path . $template_file_name);
        //} 

        $general_col2 = json_encode($popUpVO);
    }*/

    $logical_rules_vo = new LogicalRulesVO($bis_re_name, $bis_re_description, 
            $bis_re_hook, $bis_re_status, $rule_criteria_array, $bis_re_eval_type, $bis_add_rule_type, $json_query);
    
    $results_map = $logical_rules_engine_modal->save_rule_only($logical_rules_vo);

    $status = $results_map[BIS_STATUS];
    
    $rules_vo = RulesEngineCacheWrapper::get_session_attribute(BIS_SESSION_RULEVO);

    if ($rules_vo != null) {
        $rules_vo->set_logical_rule_id($logical_rules_vo->get_id());
    }    

    if ($status === BIS_SUCCESS) {
        RulesEngineCacheWrapper::set_reset_time(BIS_LOGICAL_RULE_RESET);
    }
    
    RulesEngineUtil::generate_json_response($results_map);
}

function bis_create_query_builder_logical_rule_new($rules_vo){
    
    RulesEngineCacheWrapper::delete_all_transient_cache();
    
    $json_query = stripslashes($_POST ['bis_jquery_result']);
    $array_query = json_decode($json_query, true);
    $bis_re_eval_type = $_POST ['bis_re_eval_type'];
    $bis_add_rule_type =  $_POST ['bis_add_rule_type'];

    $query_model = new QueryBuilderModel();
    $rule_criteria_array = $query_model->bis_quey_builder_function($array_query);
    
    $bis_re_name = $_POST ['bis_re_name'];
    $bis_re_description = $_POST ['bis_re_description'];
    $bis_re_hook = trim($_POST ['bis_re_hook']);
    $bis_re_status = $_POST['bis_re_rule_status'];
    
    /*if (isset($_POST['bis_re_city_lat']) && isset($_POST['bis_re_city_lng'])) {
        $bis_latitude = $_POST['bis_re_city_lat'];
        $bis_longitude = $_POST['bis_re_city_lng'];
        $bis_re_city_radius = $_POST['bis_re_city_radius'];
        $bis_radius_unit = $_POST['bis_re_mi_km'];
    } else {
        $bis_latitude = '';
        $bis_longitude = '';
        $bis_re_city_radius = '';
        $bis_radius_unit = '';
    }
    $near_by_cities = null;*/
    
    if ($bis_re_hook != null && $bis_re_hook !== "" && !function_exists($bis_re_hook) === true) {
        $results_map = array();
        $results_map[BIS_STATUS] = BIS_ERROR;
        $results_map[BIS_MESSAGE_KEY] = BIS_NO_METHOD_FOUND;
        RulesEngineUtil::generate_json_response($results_map);
    }
    
    $logical_rules_engine_modal = new LogicalRulesEngineModel();
    
    /*if ($bis_longitude != '' && $bis_latitude != '') {
        $cities = $logical_rules_engine_modal->get_nearby_cities($bis_latitude, $bis_longitude, $bis_re_city_radius);
        $near_cities = array(
            'latitude' => $bis_latitude,
            'longitude' => $bis_longitude,
            'city_radius' => $bis_re_city_radius,
            'near_by_cities' => $cities,
            'radius_unit' => $bis_radius_unit
        );
        $near_by_cities = json_encode($near_cities);
    }*/

    $logical_rules_vo = new LogicalRulesVO($bis_re_name, $bis_re_description, $bis_re_hook, $bis_re_status,
            $rule_criteria_array, $bis_re_eval_type, $bis_add_rule_type, $json_query);
    
     // Call to model to logical rules

    $results_map = $logical_rules_engine_modal->save_rule($logical_rules_vo);

    $status = $results_map[BIS_STATUS];

    if ($rules_vo != null) {
        $rules_vo->set_logical_rule_id($logical_rules_vo->get_id());
    }
    
    if ($status === BIS_SUCCESS) {
        RulesEngineCacheWrapper::set_reset_time(BIS_LOGICAL_RULE_RESET);

        $results_map = $logical_rules_engine_modal->save_child_rule($rules_vo);
        $status = $results_map[BIS_STATUS];

        if ($status == BIS_SUCCESS) {
            RulesEngineCacheWrapper::set_reset_time($rules_vo->get_reset_rule_key());
            RulesEngineUtil::update_rule_cache_id(BIS_LOGICAL_CACHE_REFRESH_ID);
        }
    }

    RulesEngineCacheWrapper::remove_session_attribute(BIS_PLUGIN_TYPE_VALUE);
    RulesEngineUtil::generate_json_response($results_map);
}

function bis_create_logical_rule_new($rules_vo, $bis_criteria = null, $fun_call = null) {
    
    RulesEngineCacheWrapper::delete_all_transient_cache();
    
    $rule_criteria_array = array();
    if($bis_criteria != null && $fun_call !== "user_based"){
        
        $bis_sub_opt[0] = $bis_criteria[0];
        $bis_re_sub_option = $bis_sub_opt;
        $bis_re_condition[0] = "12";
        $bis_re_rule_value[0] = $bis_criteria[2];
        $bis_re_left_bracket = "0";
        $bis_re_right_bracket = "0";
        $sub_criteria = explode("_", $bis_criteria[0])[1];
        
        $value_type_id = "1";
        if ($sub_criteria === "9" || $sub_criteria === "14" || $sub_criteria === "3" || $sub_criteria === "10") {
            $value_type_id = "3";
            $bis_re_condition[0] = "1";
        } else if ($sub_criteria === "6" || $sub_criteria === "15" || $sub_criteria === "35" || $sub_criteria === "27" || $sub_criteria === "26" || $sub_criteria === "31") {
            $value_type_id = "4";
            $bis_re_condition[0] = "1";
        } else if ($sub_criteria === "19" || $sub_criteria === "32" || $sub_criteria === "16" || $sub_criteria === "11" || $sub_criteria === "12" || $sub_criteria === "13" || $sub_criteria === "33" || $sub_criteria === "7" || $sub_criteria === "28") {
            $value_type_id = "2";
            $bis_re_condition[0] = "1";
            $bis_re_rule_value[0] = $bis_criteria[1];
        }
        $bis_re_sub_opt_type_id[0] = $value_type_id;
        $bis_re_eval_type = "1";
        $bis_add_rule_type = "1";
        $bis_re_status = "1";
    } elseif ($fun_call === "user_based") {
        $bis_re_sub_option = $bis_criteria['bis_re_sub_option'];
        $bis_re_condition = $bis_criteria['bis_re_condition'];
        $bis_re_rule_value = $bis_criteria ['bis_re_rule_value'];
        $bis_re_left_bracket = $bis_criteria ['bis_re_left_bracket'];
        $bis_re_right_bracket = $bis_criteria ['bis_re_right_bracket'];
        $bis_re_sub_opt_type_id = $bis_criteria ['bis_re_sub_opt_type_id'];
        $bis_re_eval_type = $bis_criteria ['bis_re_eval_type'];
        $bis_add_rule_type = $bis_criteria ['bis_add_rule_type'];
        $bis_re_status = $bis_criteria['bis_re_rule_status'];
    } else {
        $bis_re_sub_option = $_POST ['bis_re_sub_option'];
        $bis_re_condition = $_POST ['bis_re_condition'];
        $bis_re_rule_value = $_POST ['bis_re_rule_value'];
        $bis_re_left_bracket = $_POST ['bis_re_left_bracket'];
        $bis_re_right_bracket = $_POST ['bis_re_right_bracket'];
        $bis_re_sub_opt_type_id = $_POST ['bis_re_sub_opt_type_id'];
        $bis_re_eval_type = $_POST ['bis_re_eval_type'];
        $bis_add_rule_type = $_POST ['bis_add_rule_type'];
        $bis_re_status = $_POST['bis_re_rule_status'];
    }

    $bis_re_rule_type = "option";
    
    $logical_rules_engine_modal = new LogicalRulesEngineModel();

    $rules_count = count($bis_re_sub_option);
    // Loop over the list of rule criteria
    for ($rows = 0; $rows < count($bis_re_sub_option); $rows++) {

        $logical_rules_criteria_vo = new LogicalRulesCriteriaVO();
        $options = explode("_", $bis_re_sub_option[$rows]);
        $option_id = (int) $options[0];
        $sub_option = (int) $options[1];
        
        $bis_re_rule_value_str = $bis_re_rule_value[$rows];
        $condition = (int) $bis_re_condition[$rows];
        $value_type_id = (int) $bis_re_sub_opt_type_id[$rows];

        // Check for Input Token
        if ($value_type_id == 1) {
            // If condition = equal or condition = not equal
            if (($condition == 1 || $condition == 2) || 
                    (($sub_option == 2 || $sub_option == 4 || $sub_option == 5 || $sub_option == 23 || $sub_option == 24 ||
                    $sub_option == 20 || $sub_option == 21 || $sub_option == 1 || $sub_option == 18 || $sub_option == 30 ||
                    $sub_option == 22 || $sub_option == 25 || $sub_option == 17 || $sub_option == 29) && ($condition == 12 || $condition == 14 || $condition == 15))) {
                $bis_re_rule_value_str = stripslashes($bis_re_rule_value_str);
                if($bis_criteria == null){
                    if (isset($_POST["bis_city_radius_" . $rows . "_value_1"]) && isset($_POST["bis_city_radius_units_" . $rows . "_value_2"]) 
                            && isset($_POST["bis_city_latitude_" . $rows . "_value_3"]) && isset($_POST["bis_city_longitude_" . $rows . "_value_4"])) {
                        $city_radius = $_POST["bis_city_radius_" . $rows . "_value_1"];
                        $radius_units = $_POST["bis_city_radius_units_" . $rows . "_value_2"];
                        $city_latitude = $_POST["bis_city_latitude_" . $rows . "_value_3"];
                        $city_longitude = $_POST["bis_city_longitude_" . $rows . "_value_4"];
                        $cities = $logical_rules_engine_modal->get_nearby_cities($city_latitude, $city_longitude, $city_radius, $radius_units);
                        $lat_lng_rad_units = array(
                            'latitude' => $city_latitude,
                            'longitude' => $city_longitude,
                            'city_radius' => $city_radius,
                            'radius_unit' => $radius_units
                        );
                        $lat_lng_rad_units_json = json_encode((array) $lat_lng_rad_units);
                        $near_by_cities = json_encode((array) $cities);
                        $logical_rules_criteria_vo->set_general_col1($lat_lng_rad_units_json);
                        $logical_rules_criteria_vo->set_general_col2($near_by_cities);
                    }
                }
            }
        }
        //distance units
        if ($sub_option == 44) {
            $distance_units = $_POST["bis_distance_units_" . $rows . "_value_1"];
            $logical_rules_criteria_vo->set_general_col2($distance_units);
        }
        
        if ($option_id == 9 || $option_id == 10 || $option_id == 3 || $option_id == 12 || $option_id == 11) {
            $logical_rules_criteria_vo->set_evalType(BIS_EVAL_REQUEST_TYPE);
        }
        
        // Hours based condtion
        if ($option_id == 7 && $sub_option == 46) {
            $current_date = date('Y-m-d H:i', current_time('timestamp', 0));
            if (RulesEngineUtil::isContains($bis_re_rule_value_str, ":")) {
                $bis_hrs_mints = explode(":", $bis_re_rule_value_str);
                $bis_hours = $bis_hrs_mints[0];
                $bis_minutes = $bis_hrs_mints[1];
            } else {
                $bis_hours = 0;
                $bis_minutes = 0;
            }
            $cenvertedTime = date('Y-m-d H:i:s', strtotime('+' . $bis_hours . ' hour +' . $bis_minutes . ' minutes', strtotime($current_date)));
            $logical_rules_criteria_vo->set_general_col2($cenvertedTime);
        }

        // Option is Request
        if ($option_id == 3 || $sub_option == 32) {

            // Filter the url
            if ($sub_option == 6 && ($condition == 1 || $condition == 2)) {
                $bis_re_rule_value_str = RulesEngineUtil::get_filter_url($bis_re_rule_value_str);
            }
            $bis_re_rule_value_str = json_encode(array(array("id" => $bis_re_rule_value_str)));
        }

        // Save Only Ids for Posts and Pages
        if ($option_id == 9 || $option_id == 10 || $option_id == 12) {
            $bis_re_rule_value_str = RulesEngineUtil::get_json_ids($bis_re_rule_value_str);
        }

        if (isset($bis_re_right_bracket[$rows])) {
            $logical_rules_criteria_vo->set_rightBracket($bis_re_right_bracket[$rows]);
        }

        if (isset($bis_re_left_bracket[$rows])) {
            $logical_rules_criteria_vo->set_leftBracket($bis_re_left_bracket[$rows]);
        }

        $logical_rules_criteria_vo->set_value($bis_re_rule_value_str);
        $logical_rules_criteria_vo->set_optionId($option_id);
        $logical_rules_criteria_vo->set_subOptionId($sub_option);
        $logical_rules_criteria_vo->set_conditionId($bis_re_condition[$rows]);
        $logical_rules_criteria_vo->set_ruleType($bis_re_rule_type);
        
        // Add operator not to the last row.
        if($rows < ($rules_count - 1)) {
            if ($bis_add_rule_type == 1) {
                $logical_rules_criteria_vo->set_operatorId(1);
            } else {
                $bis_re_logical_op = $_POST ['bis_re_logical_op'];
                $logical_rules_criteria_vo->set_operatorId($bis_re_logical_op[$rows]);
            }    
        } else {
          $logical_rules_criteria_vo->set_operatorId(0);
        }
        array_push($rule_criteria_array, $logical_rules_criteria_vo);
    }
    
    if ($bis_criteria != null) {
        $bis_re_name = $rules_vo->name;
        $bis_re_description = "";
        $bis_re_hook = "";
    } else {
        $bis_re_name = $_POST ['bis_re_name'];
        $bis_re_description = $_POST ['bis_re_description'];
        $bis_re_hook = trim($_POST ['bis_re_hook']);
        
    }
    
    /*if (isset($_POST['bis_re_city_lat']) && isset($_POST['bis_re_city_lng'])) {
        $bis_latitude = $_POST['bis_re_city_lat'];
        $bis_longitude = $_POST['bis_re_city_lng'];
        $bis_re_city_radius = $_POST['bis_re_city_radius'];
        $bis_radius_unit = $_POST['bis_re_mi_km'];
    } else {
        $bis_latitude = '';
        $bis_longitude = '';
        $bis_re_city_radius = '';
        $bis_radius_unit = '';
    }
    $near_by_cities = null;*/
    $json_query = null;

    // Validation for hook
    if ($bis_re_hook != null && $bis_re_hook !== "" && !function_exists($bis_re_hook) === true) {
        $results_map = array();
        $results_map[BIS_STATUS] = BIS_ERROR;
        $results_map[BIS_MESSAGE_KEY] = BIS_NO_METHOD_FOUND;
        RulesEngineUtil::generate_json_response($results_map);
    }
        
    /*if ($bis_longitude != '' && $bis_latitude != '') {
        $cities = $logical_rules_engine_modal->get_nearby_cities($bis_latitude, $bis_longitude, $bis_re_city_radius);
        $near_cities = array(
            'latitude' => $bis_latitude,
            'longitude' => $bis_longitude,
            'city_radius' => $bis_re_city_radius,
            'near_by_cities' => $cities,
            'radius_unit' => $bis_radius_unit
        );
        $near_by_cities = json_encode($near_cities);
    }*/
    
    $logical_rules_vo = new LogicalRulesVO($bis_re_name, $bis_re_description, $bis_re_hook, $bis_re_status, 
            $rule_criteria_array, $bis_re_eval_type, $bis_add_rule_type, $json_query);

    
    $results_map = $logical_rules_engine_modal->save_rule($logical_rules_vo);

    $status = $results_map[BIS_STATUS];

    if ($rules_vo != null) {
        $rules_vo->set_logical_rule_id($logical_rules_vo->get_id());
    }

    if ($status === BIS_SUCCESS) {
        RulesEngineCacheWrapper::set_reset_time(BIS_LOGICAL_RULE_RESET);
    
        $results_map = $logical_rules_engine_modal->save_child_rule($rules_vo);
        $status = $results_map[BIS_STATUS];

        if ($status == BIS_SUCCESS) {
            RulesEngineCacheWrapper::set_reset_time($rules_vo->get_reset_rule_key());
            RulesEngineUtil::update_rule_cache_id(BIS_LOGICAL_CACHE_REFRESH_ID);
        }
    }

    RulesEngineCacheWrapper::remove_session_attribute(BIS_PLUGIN_TYPE_VALUE);
    if($bis_criteria == null){
        RulesEngineUtil::generate_json_response($results_map);
    } 
}

function bis_use_existing_rule() {
    
    $rule_id = $_POST ['bis_re_rule_id'];
    $rules_vo = RulesEngineCacheWrapper::get_session_attribute(BIS_SESSION_RULEVO);

    if ($rules_vo != null) {
        $rules_vo->set_logical_rule_id($rule_id);
    }

    if ($status === BIS_SUCCESS) {
        RulesEngineCacheWrapper::set_reset_time(BIS_LOGICAL_RULE_RESET);
    }

    RulesEngineUtil::generate_json_response($results_map);
}

function bis_clear_transiant_cache(){
    
    try {
        $status = RulesEngineCacheWrapper::delete_all_transient_cache();
    } catch (Exception $status) {
        
    }
    return $status;   
}

function bis_delete_all_rules() {
    
    $logical_rules_engine_modal = new LogicalRulesEngineModel();

    $status = $logical_rules_engine_modal->delete_all_rules_from_DB();
    return $status;
}

function bis_logical_rules_popup_tinymce() {
    $nonce = $_POST ['bis_nonce'];

    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }

    $logical_model = new LogicalRulesEngineModel();
    $logical_rules = $logical_model->get_all_logical_rules_only();

    wp_send_json($logical_rules);
}

function bis_std_to_query() {
    
    $logical_rule_engine_modal = new LogicalRulesEngineModel();
    $result = $logical_rule_engine_modal->std_to_query_conv();
    wp_send_json($result);
}

function bis_get_near_cities(){
    
    $bis_latitude = $_GET ['bis_latitude'];
    $bis_longitude = $_GET ['bis_longitude'];
    $bis_re_city_radius = $_GET ['bis_re_city_radius'];
    $bis_radius_unit = $_GET ['bis_radius_unit'];
    $logical_rule_engine_modal = new LogicalRulesEngineModel();
    $cities = $logical_rule_engine_modal->get_nearby_cities($bis_latitude, $bis_longitude, $bis_re_city_radius, $bis_radius_unit);
    
    wp_send_json($cities);
}

//clear search box value
function bis_clear_search_box_val() {
    
    RulesEngineCacheWrapper::remove_session_attribute(BIS_SEARCH_BOX_VALUE);
}
/*function bis_re_logical_rules($applied_rule) {

    $rulesVO = null;

    if (!empty($applied_rule)) {
        $rulesVO = $applied_rule;
    }

    $results_map = array();
    $results_map[BIS_STATUS] = BIS_SUCCESS_WITH_NO_DATA;

    if ($rulesVO != null || $rulesVO != FALSE) {

        $results_map = bis_logical_popup($rulesVO);
        // Get the popupvo
        $logical_popup = json_decode($rulesVO->general_col2);
        // Check if popup occurrence is once
        if ($logical_popup->popUpOccurr == 1) {
            setcookie(BIS_POPUP_SHOWN_ONCE, TRUE);
            RulesEngineCacheWrapper::remove_session_attribute(BIS_LOGICAL_POPUP_RULES);
        }
    }

    return $results_map;
}

function bis_re_logical_popup() {

    $applied_logical_rules = RulesEngineCacheWrapper::get_session_attribute(BIS_LOGICAL_POPUP_RULES);
    RulesEngineCacheWrapper::remove_session_attribute(BIS_LOGICAL_POPUP_RULES);

    $rulesVO = null;
    if (!empty($applied_logical_rules)) {
        $rulesVO = $applied_logical_rules[0];
        $cposition = $applied_logical_rules[0]->general_col1;
    }
    $results_map = array();
    $results_map[BIS_STATUS] = BIS_SUCCESS_WITH_NO_DATA;

    if ($rulesVO != null || $rulesVO != FALSE) {

        if ($cposition == "show_as_dialog") {
            $results_map = bis_logical_popup($rulesVO);
            // Get the popupvo
            $logical_popup = json_decode($rulesVO->general_col2);
            // Check if popup occurrence is once
            if ($logical_popup->popUpOccurr == 1) {
                setcookie(BIS_POPUP_SHOWN_ONCE, TRUE);
                RulesEngineCacheWrapper::remove_session_attribute(BIS_LOGICAL_POPUP_RULES);
            }
        }
    }

    RulesEngineUtil::generate_json_response($results_map);
}*/