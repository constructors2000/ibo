<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

use bis\repf\model\LogicalRulesEngineModel;

class RulesEngineDebug {

    public function bis_logical_rules_status_shortcode() {
      
        $logicalRulesEngineModel = new LogicalRulesEngineModel();
        
        $succsess_array = array();
        
        $failure_array = array();

        $all_active_rules = $logicalRulesEngineModel->get_active_rules();
        $count = 0;
        if($all_active_rules) {
            foreach ($all_active_rules as $key => $value) {
                $valid = RulesEngineCommon::is_rule_valid($value->name);
                if($valid){
                    $succsess_array[$count++] = $value;
                }else{
                    $failure_array[$count++] = $value;
                }
            }
        }
        $sn = 1;
        ?><table border="1" width="100%">
            <thead>
                <tr style="background-color: lightslategray">
                    <th width="5%">S.no</th>
                    <th width="10%" style="padding-left:5px">Id</th>
                    <th width="20%">Rule Name</th>
                    <th>Description</th>
                    <th width="20%">Hook</th>
                    <th width="10%">Status</th> 
                </tr>
            </thead>
            <tbody><?php
                if(empty($succsess_array)&& empty($failure_array)) {
                  echo "<tr><td colspan='6'>No rules found</td></tr>";  
                } 
                foreach ($succsess_array as $key => $row) {
                  ?><tr style="background-color: lightgreen">
                        <td > <?php echo $sn; ?></td> 
                        <td style="padding-left:5px"> <?php echo $row->ruleId ; ?></td>
                        <td > <?php echo $row->name ; ?></td>
                        <td> <?php echo $row->description ; ?></td>
                        <td > <?php echo $row->action_hook ; ?></td>  
                        <td > Success</td>  
                    </tr><?php
                      $sn++;
                    }
                foreach ($failure_array as $key => $row) {
                  ?><tr style="background-color: lightsalmon">
                        <td > <?php echo $sn; ?></td> 
                        <td style="padding-left:5px"> <?php echo $row->ruleId ; ?></td>
                        <td > <?php echo $row->name ; ?></td>
                        <td> <?php echo $row->description ; ?></td>
                        <td > <?php echo $row->action_hook ; ?></td>  
                        <td > Failure</td> 
                        
                    </tr><?php
                     $sn++;
                }
          ?></tbody>
        </table><?php
    }
    
    public function bis_geo_location_shortcode($atts){
        
        $geoLocation = new bis\repf\util\GeoPluginWrapper();
        $country_code = strtolower($geoLocation->getCountryCode());
        $val = $atts['geo_pro'];
        switch ($atts['geo_pro']) {
            case 'ip_add':
                return $geoLocation->getIPAddress();
                break;
            case 'continent':
                return $geoLocation->getContinentName();
                break;
            case 'country':
                return $geoLocation->getCountryName();
                break;
            case 'currency':
                return $geoLocation->getCurrencyCode();
                break;
            case 'region':
                return $geoLocation->getRegion();
                break;
            case 'city':
                return $geoLocation->getCity();
                break;
            case 'longitude':
                return $geoLocation->getLongitude();
                break;
            case 'latitude':
                return $geoLocation->getLatitude();
                break;
            case 'countrycode':
                return $geoLocation->getCountryCode();
                break;
            case 'image':
                $str = '<span class = "flag-icon flag-icon-'.strtolower($geoLocation->getCountryCode()) .'" style=" width: 255px !important;line-height: 20 !important;"></span>';
                return $str;
                break;

        }
       
    }
}
?>