<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

namespace bis\repf\vo;

/**
 * Class DiscountActionVo
 */

class DiscountActionVo {
    
    public $discount_amount = null;
    public $discount_percentage = null;
    public $discount_quantity_type = null;
    public $discount_buy_products = null;
    public $discount_get_products = null;
    public $discount_get_percentage = null;
    public $discount_type = null;
    
    function getDiscount_amount() {
        return $this->discount_amount;
    }

    function getDiscount_percentage() {
        return $this->discount_percentage;
    }

    function getDiscount_quantity_type() {
        return $this->discount_quantity_type;
    }

    function getDiscount_buy_products() {
        return $this->discount_buy_products;
    }

    function getDiscount_get_products() {
        return $this->discount_get_products;
    }

    function getDiscount_get_percentage() {
        return $this->discount_get_percentage;
    }

    function getDiscount_type() {
        return $this->discount_type;
    }

    function setDiscount_amount($discount_amount) {
        $this->discount_amount = $discount_amount;
    }

    function setDiscount_percentage($discount_percentage) {
        $this->discount_percentage = $discount_percentage;
    }

    function setDiscount_quantity_type($discount_quantity_type) {
        $this->discount_quantity_type = $discount_quantity_type;
    }

    function setDiscount_buy_products($discount_buy_products) {
        $this->discount_buy_products = $discount_buy_products;
    }

    function setDiscount_get_products($discount_get_products) {
        $this->discount_get_products = $discount_get_products;
    }

    function setDiscount_get_percentage($discount_get_percentage) {
        $this->discount_get_percentage = $discount_get_percentage;
    }

    function setDiscount_type($discount_type) {
        $this->discount_type = $discount_type;
    }

}
