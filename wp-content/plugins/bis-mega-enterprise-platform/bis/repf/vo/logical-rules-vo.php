<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

namespace bis\repf\vo;

/**
 * Class LogicalRulesVO
 */
class LogicalRulesVO {

    public $id = null;
    public $name = null;
    public $description = null;
    public $actionHook = null;
    public $ruleCriteriaArray = null;
    public $status = null;
    //public $gen_col1 = null;
    //public $gen_col2 = null;
    public $ruleEvalType = 1;  /* 1=Always 2= Atleast Once used for evaluating the rule for once or every time. 
                                Used for Refferal URL option */
    public $addRuleType = 1;
    public $query_builder = null;
    //public $near_by_cities = null;

    /**
     * @param null $name
     * @param null $description
     * @param null $actionHook
     * @param null $ruleCriteriaArray
     */
    function __construct($name = null, $description = null, $actionHook = null, $status = null,
                            $ruleCriteriaArray = null, $ruleEvalType = 1, $bisAddRuleType = 1, $query_builder = null) {
        $this->name = $name;
        $this->description = $description;
        $this->actionHook = $actionHook;
        $this->status = $status;
        //$this->gen_col1 = $general_col1;
        //$this->gen_col2 = $general_col2;
        $this->ruleCriteriaArray = $ruleCriteriaArray;
        $this->ruleEvalType = $ruleEvalType;
        $this->addRuleType = $bisAddRuleType;
        $this->query_builder = $query_builder;
        //$this->near_by_cities = $near_by_cities;
    }

    /**
     * Get the rules evaluation type.
     * 
     * @return type
     */
    public function getAddRuleType() {
        return $this->addRuleType;
    }
    /**
     * Get the rules evaluation type.
     * 
     * @return type
     */
    public function setAddRuleType($addRuleType) {
         $this->addRuleType = $addRuleType;
    }
    /**
     * Get the rules evaluation type.
     * 
     * @return type
     */
    public function getRuleEvalType() {
        return $this->ruleEvalType;
    }

    /**
     * Sets the Rule Evaluation Type.
     * @param type $ruleEvalType
     */
    public function setRuleEvalType($ruleEvalType) {
        $this->ruleEvalType = $ruleEvalType;
    }

    /**
     * @return null
     */
    public function get_status() {
        return $this->status;
    }

    /**
     * @param $status
     */
    public function set_status($status) {
        $this->status = $status;
    }
    
    /*public function getGeneralCol1() {
        return $this->gen_col1;
    }

    public function setGeneralCol1($general_col1) {
        $this->gen_col1 = $general_col1;
    }
    
    public function getGeneralCol2() {
        return $this->gen_col2;
    }

    public function setGeneralCol2($general_col2) {
        $this->gen_col2 = $general_col2;
    }*/

    /**
     * @return null
     */
    public function get_id() {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function set_id($id) {
        $this->id = $id;
    }

    /**
     * @return null
     */
    public function get_name() {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function set_name($name) {
        $this->name = $name;
    }

    /**
     * @return null
     */
    public function get_description() {
        return $this->description;
    }

    /**
     * @param $description
     */
    public function set_description($description) {
        $this->description = $description;
    }

    /**
     * @return null
     */
    public function get_actionHook() {
        return $this->actionHook;
    }

    /**
     * @param $actionHook
     */
    public function set_actionHook($actionHook) {
        $this->actionHook = $actionHook;
    }

    /**
     * @return null
     */
    public function get_ruleCriteriaArray() {
        return $this->ruleCriteriaArray;
    }

    /**
     * @param $ruleCriteriaArray
     */
    public function set_ruleCriteriaArray($ruleCriteriaArray) {
        $this->ruleCriteriaArray = $ruleCriteriaArray;
    }
    
    public function getQuery_builder() {
        return $this->query_builder;
    }

    public function setQuery_builder($query_builder) {
        $this->query_builder = $query_builder;
    }

    /*public function getNearby_cities() {
        return $this->near_by_cities;
    }

    public function setNearby_cities($near_by_cities) {
        $this->near_by_cities = $near_by_cities;
    }*/

}