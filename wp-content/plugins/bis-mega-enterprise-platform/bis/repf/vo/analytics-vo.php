<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

namespace bis\repf\vo;

class AnalyticsVO {
    private $profileId = null;
    private $token = null;
    private $from = null;
    private $to = null;
    private $query = null;
    
    function getProfileId() {
        return $this->profileId;
    }

    function getToken() {
        return $this->token;
    }

    function getFrom() {
        return $this->from;
    }

    function getTo() {
        return $this->to;
    }

    function getQuery() {
        return $this->query;
    }

    function setProfileId($profileId) {
        $this->profileId = $profileId;
    }

    function setToken($token) {
        $this->token = $token;
    }

    function setFrom($from) {
        $this->from = $from;
    }

    function setTo($to) {
        $this->to = $to;
    }

    function setQuery($query) {
        $this->query = $query;
    }
    
}