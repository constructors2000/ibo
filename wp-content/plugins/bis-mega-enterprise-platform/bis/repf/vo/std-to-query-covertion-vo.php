<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

namespace bis\repf\vo;

class StdToQueryVO {

    public $id = null;
    public $field = null;
    public $type = null;
    public $input = null;
    public $operator = null;
    public $value = null;

    function getId() {
        return $this->id;
    }

    function getField() {
        return $this->field;
    }

    function getType() {
        return $this->type;
    }

    function getInput() {
        return $this->input;
    }

    function getOperator() {
        return $this->operator;
    }

    function getValue() {
        return $this->value;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setField($field) {
        $this->field = $field;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setInput($input) {
        $this->input = $input;
    }

    function setOperator($operator) {
        $this->operator = $operator;
    }

    function setValue($value) {
        $this->value = $value;
    }

}
