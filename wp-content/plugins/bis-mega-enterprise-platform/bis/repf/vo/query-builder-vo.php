<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

namespace bis\repf\vo;

class QueryFilterVO {

    public $id = null; 
    public $label = null; 
    public $type = null; 
    public $input = null; 
    public $values = null; 
    public $operators = null;
    public $plugin = null;
    public $plugin_config = null;
    public $field = null;
    public $optgroup = null;
    public $placeholder = null;
    
    function getPlaceholder() {
        return $this->placeholder;
    }

    function setPlaceholder($placeholder) {
        $this->placeholder = $placeholder;
    }

    function getOptgroup() {
        return $this->optgroup;
    }

    function setOptgroup($optgroup) {
        $this->optgroup = $optgroup;
    }
        
    function getField() {
        return $this->field;
    }

    function setField($field) {
        $this->field = $field;
    }
    
    function getPlugin() {
        return $this->plugin;
    }

    function getPlugin_config() {
        return $this->plugin_config;
    }

    function setPlugin($plugin) {
        $this->plugin = $plugin;
    }

    function setPlugin_config($plugin_config) {
        $this->plugin_config = $plugin_config;
    }

        function getId() {
        return $this->id;
    }

    function getLabel() {
        return $this->label;
    }

    function getType() {
        return $this->type;
    }

    function getInput() {
        return $this->input;
    }

    function getValues() {
        return $this->values;
    }

    function getOperators() {
        return $this->operators;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setLabel($label) {
        $this->label = $label;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setInput($input) {
        $this->input = $input;
    }

    function setValues($values) {
        $this->values = $values;
    }

    function setOperators($operators) {
        $this->operators = $operators;
    }

}