<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

class RulesEngineContentUtil {
    /**
     * This method used convert array of allowable tags into string of tags
     * 
     * @return string
     */
    public static function set_allowble_tags_as_string() {      
        return RulesEngineContentUtil::convert_string_to_tags(RulesEngineUtil::get_option(BIS_RULES_ENGINE_ALLOWABLE_TAGS_CONST));
    }
    
    public static function convert_string_to_tags($allowable_tags, $from = null) {
     
        if (is_string($allowable_tags)) {
            $tags = explode(";", $allowable_tags);
            $tags_array = null;
            foreach ($tags as $value) {
                if (RulesEngineUtil::isContains($value, "s")) {
                    $tag = trim(explode(":", $value)[2]);
                    $tags_array[] = trim($tag, '"');
                }
            }
        } else {
            $tags_array = $allowable_tags;
        }
        if($from == null) {
            $allowed_tags = "";
            if(is_string($allowable_tags)){
                $allowable_tags = $tags_array;
            }
            foreach ($allowable_tags as $tag) {
                $allowed_tags = $allowed_tags . "<" . $tag . ">";
            }
            return $allowed_tags;
        }
        return $tags_array;
    }
}