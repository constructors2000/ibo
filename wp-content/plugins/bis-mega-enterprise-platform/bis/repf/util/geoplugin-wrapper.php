<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

namespace bis\repf\util;

use bis\repf\util\geoPlugin;
use bis\repf\MaxMind\Db\Reader;
use bis\repf\IP2Location\Database;
use RulesEngineUtil;

class GeoPluginWrapper {
    
    private $countryName = "Other";
    private $countryCode = null;
    private $currencyCode = null;
    private $continentCode = null;
    private $continentName = null;
    private $ipAddress = null;
    private $currencySymbol = null;
    private $currencyConverter = null;
    private $city = "Other";
    private $region = "Other";
    private $latitude = null;
    private $longitude = null;
    private $ipLookUp = "Server";
    private $postalCode = null;
    private $timeZone = null;
    private $ipVersion = null;
    private $country_continent_map = array("AD" => "EU", "AE" => "AS", "AF" => "AS", "AG" => "NA", "AI" => "NA", "AL" => "EU", "AM" => "AS", "AN" => "NA", "AO" => "AF", "AP" => "AS", "AQ" => "AN", "AR" => "SA", "AS" => "OC", "AT" => "EU", "AU" => "OC", "AW" => "NA", "AX" => "EU", "AZ" => "AS", "BA" => "EU", "BB" => "NA", "BD" => "AS", "BE" => "EU", "BF" => "AF", "BG" => "EU", "BH" => "AS", "BI" => "AF", "BJ" => "AF", "BL" => "NA", "BM" => "NA", "BN" => "AS", "BO" => "SA", "BR" => "SA", "BS" => "NA", "BT" => "AS", "BV" => "AN", "BW" => "AF", "BY" => "EU", "BZ" => "NA", "CA" => "NA", "CC" => "AS", "CD" => "AF", "CF" => "AF", "CG" => "AF", "CH" => "EU", "CI" => "AF", "CK" => "OC", "CL" => "SA", "CM" => "AF", "CN" => "AS", "CO" => "SA", "CR" => "NA", "CU" => "NA", "CV" => "AF", "CX" => "AS", "CY" => "AS", "CZ" => "EU", "DE" => "EU", "DJ" => "AF", "DK" => "EU", "DM" => "NA", "DO" => "NA", "DZ" => "AF", "EC" => "SA", "EE" => "EU", "EG" => "AF", "EH" => "AF", "ER" => "AF", "ES" => "EU", "ET" => "AF", "EU" => "EU", "FI" => "EU", "FJ" => "OC", "FK" => "SA", "FM" => "OC", "FO" => "EU", "FR" => "EU", "FX" => "EU", "GA" => "AF", "GB" => "EU", "GD" => "NA", "GE" => "AS", "GF" => "SA", "GG" => "EU", "GH" => "AF", "GI" => "EU", "GL" => "NA", "GM" => "AF", "GN" => "AF", "GP" => "NA", "GQ" => "AF", "GR" => "EU", "GS" => "AN", "GT" => "NA", "GU" => "OC", "GW" => "AF", "GY" => "SA", "HK" => "AS", "HM" => "AN", "HN" => "NA", "HR" => "EU", "HT" => "NA", "HU" => "EU", "ID" => "AS", "IE" => "EU", "IL" => "AS", "IM" => "EU", "IN" => "AS", "IO" => "AS", "IQ" => "AS", "IR" => "AS", "IS" => "EU", "IT" => "EU", "JE" => "EU", "JM" => "NA", "JO" => "AS", "JP" => "AS", "KE" => "AF", "KG" => "AS", "KH" => "AS", "KI" => "OC", "KM" => "AF", "KN" => "NA", "KP" => "AS", "KR" => "AS", "KW" => "AS", "KY" => "NA", "KZ" => "AS", "LA" => "AS", "LB" => "AS", "LC" => "NA", "LI" => "EU", "LK" => "AS", "LR" => "AF", "LS" => "AF", "LT" => "EU", "LU" => "EU", "LV" => "EU", "LY" => "AF", "MA" => "AF", "MC" => "EU", "MD" => "EU", "ME" => "EU", "MF" => "NA", "MG" => "AF", "MH" => "OC", "MK" => "EU", "ML" => "AF", "MM" => "AS", "MN" => "AS", "MO" => "AS", "MP" => "OC", "MQ" => "NA", "MR" => "AF", "MS" => "NA", "MT" => "EU", "MU" => "AF", "MV" => "AS", "MW" => "AF", "MX" => "NA", "MY" => "AS", "MZ" => "AF", "NA" => "AF", "NC" => "OC", "NE" => "AF", "NF" => "OC", "NG" => "AF", "NI" => "NA", "NL" => "EU", "NO" => "EU", "NP" => "AS", "NR" => "OC", "NU" => "OC", "NZ" => "OC", "O1" => "--", "OM" => "AS", "PA" => "NA", "PE" => "SA", "PF" => "OC", "PG" => "OC", "PH" => "AS", "PK" => "AS", "PL" => "EU", "PM" => "NA", "PN" => "OC", "PR" => "NA", "PS" => "AS", "PT" => "EU", "PW" => "OC", "PY" => "SA", "QA" => "AS", "RE" => "AF", "RO" => "EU", "RS" => "EU", "RU" => "EU", "RW" => "AF", "SA" => "AS", "SB" => "OC", "SC" => "AF", "SD" => "AF", "SE" => "EU", "SG" => "AS", "SH" => "AF", "SI" => "EU", "SJ" => "EU", "SK" => "EU", "SL" => "AF", "SM" => "EU", "SN" => "AF", "SO" => "AF", "SR" => "SA", "ST" => "AF", "SV" => "NA", "SY" => "AS", "SZ" => "AF", "TC" => "NA", "TD" => "AF", "TF" => "AN", "TG" => "AF", "TH" => "AS", "TJ" => "AS", "TK" => "OC", "TL" => "AS", "TM" => "AS", "TN" => "AF", "TO" => "OC", "TR" => "EU", "TT" => "NA", "TV" => "OC", "TW" => "AS", "TZ" => "AF", "UA" => "EU", "UG" => "AF", "UM" => "OC", "US" => "NA", "UY" => "SA", "UZ" => "AS", "VA" => "EU", "VC" => "NA", "VE" => "SA", "VG" => "NA", "VI" => "NA", "VN" => "AS", "VU" => "OC", "WF" => "OC", "WS" => "OC", "YE" => "AS", "YT" => "AF", "ZA" => "AF", "ZM" => "AF", "ZW" => "AF");
    private $continent_map = array("AS" => "Asia", "EU" => "Europe", "AF" => "Africa", "AN" => "Antarctica", "NA" => "North America", "OC" => "Oceania", "SA" => "South America");

    function __construct() {

        $this->ipAddress = $this->getClientIP();
        
        if(isset($_SERVER["HTTP_CF_IPCOUNTRY"])) {
            $this->countryCode = $_SERVER["HTTP_CF_IPCOUNTRY"];
        }
        
        if (isset($_SERVER["HTTPS_CF_IPCOUNTRY"])) {
            $this->countryCode = $_SERVER["HTTPS_CF_IPCOUNTRY"];
        }

        if (RulesEngineUtil::get_option(BIS_GEO_LOOKUP_WEBSERVICE_TYPE) == 1) {
            $geoplugin = new geoPlugin();
            $geoplugin->locate($this->ipAddress);
            $this->countryName = $geoplugin->countryName;
            $this->countryCode = $geoplugin->countryCode;
            $this->currencyCode = $geoplugin->currencyCode;
            $this->continentCode = $geoplugin->continentCode;
            $this->city = $geoplugin->city;
            $this->region = $geoplugin->region;
            $this->currencySymbol = $geoplugin->currencySymbol;
            $this->currencyConverter = $geoplugin->currencyConverter;
            $this->latitude = $geoplugin->latitude;
            $this->longitude = $geoplugin->longitude;
        }
        
        if (RulesEngineUtil::get_option(BIS_GEO_IP2LOCATION_LOOKUP_TYPE) == 1) {
            
            $ip2lo = RulesEngineUtil::get_option(BIS_GEO_IP2LOCATION_DB);
         
            if($ip2lo !== false) {
                $db = new Database(RulesEngineUtil::get_file_upload_path() . $ip2lo, Database::FILE_IO);
            }else {
                $db = new Database(RulesEngineUtil::get_file_upload_path() . BIS_GEO_IP2LOCATION_DB, Database::FILE_IO);
            }
            
            $records = $db->lookup($this->ipAddress, Database::ALL);
            
            if ($records != null) {
                $this->countryName = $records['countryName'];
                $this->countryCode = $records['countryCode'];
                $this->region = $records['regionName'];
                $this->city = $records['cityName'];
                $this->postalCode = $records['zipCode'];
                $this->timeZone = $records['timeZone'];
                $this->latitude = $records['latitude'];
                $this->longitude = $records['longitude'];
                $this->ipVersion = $records['ipVersion'];
            }
        } else if (RulesEngineUtil::get_option(BIS_GEO_IP2LOCATION_CSV_LOOKUP_TYPE) == 1) {
            
            global $wpdb;
            $query_str="SELECT * FROM wp_ip2location_db2 WHERE INET_ATON('$this->ipAddress') <= ip_to LIMIT 1";
            $records = $wpdb->get_results($query_str);
            $record=$records[0];
            if ($record != null && !empty($record)) {
                $this->countryName = $record->country_name;
                $this->countryCode = $record->country_code;
                $this->region = $record->region_name;
                $this->city = $record->city;
                $this->postalCode = $record->zipcode;
                $this->latitude = $record->latitude;
                $this->longitude = $record->longitude;
            }
            
        } else if (RulesEngineUtil::get_option(BIS_GEO_LOOKUP_TYPE) == 1) {
            $mmdb = RulesEngineUtil::get_option(BIS_GEO_MAXMIND_DB_FILE);
            $reader = new Reader(RulesEngineUtil::get_file_upload_path() . $mmdb);

            $ipData = $reader->get($this->ipAddress);

            if ($ipData != null) {
                $this->countryName = $ipData['country']['names']['en'];
                $this->countryCode = $ipData['country']['iso_code'];
                $this->continentCode = $ipData['continent']['code'];

                if (isset($ipData['city'])) {
                    $this->city = $ipData['city']['names']['en'];
                    
                    if(isset($ipData['postal'])) {
                        $this->postalCode = $ipData['postal']['code'];
                    }
                    $this->subDivIsoCode = $ipData['subdivisions'][0]['iso_code'];
                }
            }
            $reader->close();
            }
            $this->continentCode = $this->country_continent_map[$this->countryCode];
            $this->continentName = $this->continent_map[$this->continentCode];
    }
    
    function getClientIP() {
        $ipaddress = 'UNKNOWN';
        
        // Cloudflare
        if (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
            $ipaddress = $_SERVER['HTTP_CF_CONNECTING_IP'];
            $this->ipLookUp = "CLOUDFLARE";
        } elseif (isset($_SERVER['CF-Connecting-IP'])) {
            $ipaddress = $_SERVER['CF-Connecting-IP'];
            $this->ipLookUp = "CLOUDFLARE";
        } elseif (isset($_SERVER['X-Real-IP'])) {
            $ipaddress = $_SERVER['X-Real-IP'];
            $this->ipLookUp = "X-Real-IP";
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            // Proxy servers can send through this header like this: X-Forwarded-For: client1, proxy1, proxy2
            // Make sure we always only send through the first IP in the list which should always be the client IP.
            $ipaddress = trim(current(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR'])));
            $this->ipLookUp = "Proxy Servers";
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ipaddress = $_SERVER['REMOTE_ADDR'];
            $this->ipLookUp = "REMOTE_ADDR";
        }

        // Logic for local testing        
        if (!filter_var($ipaddress, FILTER_VALIDATE_IP) === false) {
            if (isset($_SERVER['SERVER_NAME']) &&
                    ($ipaddress === "127.0.0.1" || $ipaddress === "::1")) {
                if ($_SERVER['SERVER_NAME'] === "localhost") {
                    $ipaddress = $this->get_external_ip_address();
                    $this->ipLookUp = "External Service";
                }
            }

            if (RulesEngineUtil::isContains($ipaddress, ":")) {
                $ipaddress = $this->get_external_ip_address();
                $this->ipLookUp = "External Service";
            }

            return $ipaddress;
        }

        $ipaddress = $this->getBisClientIpEnv();

        if (!filter_var($ipaddress, FILTER_VALIDATE_IP) === false) {
            return $ipaddress;
        }

        $ipaddress = $this->getBisClientIpServer();

        if (!filter_var($ipaddress, FILTER_VALIDATE_IP) === false) {
            return $ipaddress;
        }

        return $ipaddress;
    }

    // Function to get the client ip address
    function getBisClientIpEnv() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

// Function to get the client ip address
    function getBisClientIpServer() {
        $ipaddress = '';
        if ($_SERVER['HTTP_CLIENT_IP'])
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if ($_SERVER['HTTP_X_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if ($_SERVER['HTTP_X_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if ($_SERVER['HTTP_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if ($_SERVER['HTTP_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if ($_SERVER['REMOTE_ADDR'])
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

    function get_external_ip_address() {
        $service_endpoint = 'http://icanhazip.com';

        $ip_lookup_apis = array(
            'icanhazip' => 'http://icanhazip.com',
            'ipify' => 'http://api.ipify.org/',
            'ipecho' => 'http://ipecho.net/plain',
            'ident' => 'http://ident.me',
            'whatismyipaddress' => 'http://bot.whatismyipaddress.com'
        );

        $external_ip_address = null;

        foreach ($ip_lookup_apis as $service_endpoint) {
            $response = wp_safe_remote_get($service_endpoint, array('timeout' => 2));

            if (!is_wp_error($response) && $response['body']) {
                $external_ip_address = sanitize_text_field($response['body']);
                break;
            }
        }
        if (!filter_var($external_ip_address, FILTER_VALIDATE_IP)) {
            return "127.0.0.1";
        }

        return sanitize_text_field($external_ip_address);
    }

    function getIPAddress() {
        return $this->ipAddress;
    }

    function getCountryName() {
        return $this->countryName;
    }

    function getCountryCode() {
        return $this->countryCode;
    }

    function getCurrencyCode() {
        return $this->currencyCode;
    }

    function getContinentCode() {
        return $this->continentCode;
    }
    
    function setContinentName($continentName) {
        $this->continentName = $continentName;
    }
    
    function getContinentName() {
        return $this->continentName;
    }

    function getCity() {
        return $this->city;
    }

    function getRegion() {
        return $this->region;
    }

    function getCurrencySymbol() {
        return $this->currencySymbol;
    }

    function getCurrencyConverter() {
        return $this->currencyConverter;
    }

    function getLatitude() {
        return $this->latitude;
    }

    function getLongitude() {
        return $this->longitude;
    }

    function getIPLookUp() {
        return $this->ipLookUp;
    }
    
      function getPostalCode() {
        return $this->postalCode;
    }

    function getTimeZone() {
        return $this->timeZone;
    }

    function getIpVersion() {
        return $this->ipVersion;
    }

    function setPostalCode($postalCode) {
        $this->postalCode = $postalCode;
    }

    function setTimeZone($timeZone) {
        $this->timeZone = $timeZone;
    }

    function setIpVersion($ipVersion) {
        $this->ipVersion = $ipVersion;
    }
    
    function getCountryContinentMap() {
        return $this->country_continent_map;
    }

    function getContinentMap() {
        return $this->continent_map;
    }

}