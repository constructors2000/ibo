<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

use bis\repf\common\RulesEngineCacheWrapper;

if (!class_exists('BISRE_Common_Platform')) {

    class BISRE_Common_Platform {
        
        private $rules_engine = null;
        
        public function __clone() {
            _doing_it_wrong(__FUNCTION__, __('You cannot clone Mega Platform', BIS_RULES_ENGINE_TEXT_DOMAIN), '1.03');
        }

        public function __wakeup() {
            _doing_it_wrong(__FUNCTION__, __('You cannot wakeup Mega Platform', BIS_RULES_ENGINE_TEXT_DOMAIN), '1.03');
        }
        
        public function __construct() {
            $this->define_constants();
        }

        private function define_constants() {
            $dirName = dirname(__FILE__);
            $realPath = realpath($dirName);
            $this->define("BIS_PLATFORM_HOME_DIR", $realPath);
        }
        
        private function define($name, $value) {
            if (!defined($name)) {
                define($name, $value);
            }
        }

        public function includes() {
            $realPath = BIS_PLATFORM_HOME_DIR;
            
            require_once($realPath . '/bis/repf/common/BISSession.php');
            require_once($realPath . '/bis/repf/common/BISLogger.php');
            require_once($realPath . "/common/bis-rules-constants.php");

            require_once($realPath . "/bis/repf/ip2location/IP2Location.php");
            require_once($realPath . '/bis/repf/MaxMind/Db/Reader.php');
            require_once($realPath . '/bis/repf/MaxMind/Db/Reader/Decoder.php');
            require_once($realPath . '/bis/repf/MaxMind/Db/Reader/InvalidDatabaseException.php');
            require_once($realPath . '/bis/repf/MaxMind/Db/Reader/Metadata.php');
            require_once($realPath . '/bis/repf/MaxMind/Db/Reader/Util.php');
                       
            require_once($realPath . "/bis/repf/common/bis-rules-engine-localization.php");
            require_once($realPath . "/bis/repf/install/bis-rules-plugin-install.php");
            require_once($realPath . "/bis/repf/install/bis-rules-addon-install.php");
            require_once($realPath . "/bis/repf/model/base-rules-engine-model.php");
            require_once($realPath . "/bis/repf/model/page-rules-engine-model.php");
            require_once($realPath . "/bis/repf/model/post-rules-engine-model.php");
            require_once($realPath . "/bis/repf/model/logical-rules-engine-model.php");
            require_once($realPath . "/bis/repf/model/analytics-rules-model.php");
            require_once($realPath . "/bis/repf/model/query-builder-model.php");
            
            require_once($realPath . "/bis/repf/util/rules-stack.php");
            require_once($realPath . "/bis/repf/util/mdetect.php");
            require_once($realPath . "/bis/repf/util/geoplugin.php");
            require_once($realPath . "/bis/repf/util/browser-detection.php");
            require_once($realPath . "/bis/repf/util/geoplugin-wrapper.php");
            require_once($realPath . "/bis/repf/util/bis-cache-wrapper.php");
            require_once($realPath . "/bis/repf/util/bis-content-util.php");
            require_once($realPath . "/util/rules-engine-util.php");
            require_once($realPath . "/util/rules-engine-common.php");
            
            require_once($realPath . "/bis/repf/common/bis-rules-engine-cache-wrapper.php");
            require_once($realPath . "/bis/repf/common/bis-session-wrapper.php");
            require_once($realPath . "/bis/repf/common/bis-rules-engine-debug.php");

            require_once($realPath . "/bis/repf/action/base-rules-engine.php");
            require_once($realPath . "/bis/repf/action/rules-engine.php");
            require_once($realPath . "/bis/repf/action/page-rules-engine.php");
            require_once($realPath . "/bis/repf/action/post-rules-engine.php");
            require_once($realPath . "/bis/repf/action/query-builder-engine.php");

            require_once($realPath . "/bis/repf/vo/plugin-vo.php");
            require_once($realPath . "/bis/repf/vo/logical-rules-criteria-vo.php");
            require_once($realPath . "/bis/repf/vo/logical-rules-vo.php");
            require_once($realPath . "/bis/repf/vo/rules-vo.php");
            require_once($realPath . "/bis/repf/vo/label-value-vo.php");
            require_once($realPath . "/bis/repf/vo/search-vo.php");
            require_once($realPath . "/bis/repf/vo/image-vo.php");
            require_once($realPath . "/bis/repf/vo/audit-report-vo.php");
            require_once($realPath . "/bis/repf/vo/geolocation-vo.php");
            require_once($realPath . "/bis/repf/vo/popup-vo.php");
            require_once($realPath . "/bis/repf/vo/cache-vo.php");
            require_once($realPath . "/bis/repf/vo/query-builder-vo.php");
            require_once($realPath . "/bis/repf/vo/std-to-query-covertion-vo.php");
            require_once($realPath . "/bis/repf/vo/discount-action-vo.php");
      
            require_once($realPath . "/template/bis-rules-dynamic-content-template.php");
           
        }
        
        public function init_hooks() {
            
            $rulesEngine = new bis\repf\action\RulesEngine();
            $this->rules_engine = $rulesEngine;
            add_action('init', array(&$this, 'bis_load_dependents'), 1);  
            
            
            if (is_admin()) {
                
                if(RulesEngineUtil::is_bis_re_plugin_page()) {
                    add_action('admin_enqueue_scripts', array(&$this, 'load_custom_wp_admin_style'), 999);
                }
                add_action('admin_menu', array(&$this, 'bis_show_settings_menu'), 100);
                add_action('admin_menu', array(&$this, 'bis_show_rulesengine_menu'));
                add_filter('plugin_row_meta', array(&$this, 'plugin_row_meta'), 10, 2);
                
            }

            // Add filters and actions for non admins
            if (!is_admin()) {
            
                $page_rules_engine = new bis\repf\action\PageRulesEngine();
                $post_rules_engine = new bis\repf\action\PostRulesEngine();
               
                //Evaluate request rule at the header load
                add_action('wp', array(&$rulesEngine, 'bis_evaluate_request_rules'), 15);
                
                // Content Filter
                //add_filter('the_content', array(&$rulesEngine, 'bis_re_append_page_info'), 10);
                if(RulesEngineUtil::get_option(BIS_RULES_ENGINE_CACHE_INSTALLED, false, false) == "false") {
                    add_filter('woocommerce_after_shop_loop', array(&$rulesEngine, 'bis_re_append_shop_info'), 55);
                }
                add_filter('the_content', array(&$page_rules_engine, 'bis_re_apply_content_rule'), 50);

                // Filter added to excluded pages and posts in search results
                add_shortcode('bis_search_box', array(&$rulesEngine, 'bis_search_box'));
                add_filter('pre_get_posts', array(&$page_rules_engine, 'bis_search_filter'));
                add_action('pre_get_posts', array(&$post_rules_engine, 'bis_re_exclude_posts'), 53);
                add_filter('widget_categories_args', array(&$post_rules_engine, 'bis_exclude_post_cat_in_widget'), 56);
                add_filter('widget_tag_cloud_args', array(&$post_rules_engine, 'bis_exclude_post_tag_in_widget'), 57);

                // Action added for showing country dropdown
                if (RulesEngineUtil::get_option(BIS_MEGA_COUNTRY_DROPDOWN, false, false) === 'Yes') {
                    add_action('get_footer', array(&$rulesEngine, 'bis_change_country_hook'), 1000);
                }
                
                /*if(RulesEngineUtil::get_option(BIS_RULES_ENGINE_CACHE_INSTALLED, false, false) == "true") {
                    add_action('template_redirect', array(&$this, 'bis_page_cache_loader_script'), 1);
                }*/
                
            }
            
            // Logic used to reset the cache
            if (isset($_GET['bis_reset']) &&
                    ($_GET['bis_reset'] === '1')) {
                RulesEngineCacheWrapper::delete_all_transient_cache();
            }
            
            if(RulesEngineUtil::get_option(BIS_MEGA_COUNTRY_DROPDOWN, false, false) === 'Yes'){
                add_shortcode('bis_country_selector', array($rulesEngine, 'bis_country_selector'));
                add_shortcode('bis_country_name', array(&$rulesEngine, 'bis_country_name'));
                add_shortcode('bis_city_name', array(&$rulesEngine, 'bis_city_name'));
                add_shortcode('bis_region_name', array(&$rulesEngine, 'bis_region_name'));
            }
            
            // Debugging the rules
            $rulesEngineDebug = new RulesEngineDebug();
            if(RulesEngineUtil::get_option(BIS_RULES_DEBUG_MODE_CONT, false, false) == "Yes"){
                add_shortcode('bis_logical_rules_status', array(&$rulesEngineDebug, 'bis_logical_rules_status_shortcode'));
            }
            
            // Showing Geo-location details in demo sites
            add_shortcode('bis_geo_location_shortcode', array(&$rulesEngineDebug, 'bis_geo_location_shortcode'));
            
            add_action('plugins_loaded', array(&$this, 'localization_init'));
            add_action('wp_login', array(&$rulesEngine, 'bis_reset_rules_engine_cache_login'), 10, 2);
            add_action('wp_logout', array(&$rulesEngine, 'bis_reset_rules_engine_cache_Logout'));
            
            add_shortcode('bis-rules-if', array(&$rulesEngine, 'bis_rules_if_shortcode'));
            add_shortcode('bis-rules-else', array(&$rulesEngine, 'bis_rules_else_shortcode'));
            add_action('bis_clear_cached_rules', array(&$rulesEngine, 'bis_wp_rocket_flush'));
        }
        
        function load_custom_wp_admin_style() {
            // Fix for query builder loading for ADS PRO Plugin
            wp_dequeue_script('buy_sell_ads_pro_admin_jquery_ui_js_script');
            wp_dequeue_script('buy_sell_ads_pro_admin_switch_button_js_script');
        }

    /**
         * This method is used to show additional links in Plugin installation page.
         * 
         * @param type $links
         * @param type $file
         * @return type
         */
        public function plugin_row_meta($links, $file) {
            if (BIS_RULES_ENGINE_PLUGIN_BASENAME == $file) {
                unset($links[2]);
                $row_meta = array(
                    'docs' => '<a target="_blank" href="' . esc_url('http://docs.megaedzee.com/docs/mega-enterprise-platform/') . '">' . esc_html__('Docs', 'woocommerce') . '</a>',
                    'support' => '<a target="_blank" href="' . esc_url('http://support.megaedzee.com/')  . '">' . esc_html__('Support', 'rulesengine') . '</a>',
                );

                return array_merge($links, $row_meta);
            }

            return (array) $links;
        }

        /**
         * This method is used for adding rulesengine menu to wordpress
         */
        function bis_show_rulesengine_menu() {
            add_menu_page('Mega Platform', __("Mega Platform", BIS_RULES_ENGINE_TEXT_DOMAIN), 'manage_options', 'bis_pg_dashboard', array(&$this, 'show_rules_engine_config'), plugins_url('/image/rules-engine-small-icon.png', __FILE__));
            add_submenu_page('bis_pg_dashboard', 'Dashboard', __('Dashboard', BIS_RULES_ENGINE_TEXT_DOMAIN), 'manage_options', 'bis_pg_dashboard', array(&$this, 'show_rules_engine_config'));
        }
        
        /**
         * This method is used to load dependent for rulesengine plugin
         */
        function bis_load_dependents() {
            $realPath = BIS_PLATFORM_HOME_DIR;
           
            // From drop down
            if (isset($_POST[BIS_COUNTRY_SELECT])) {
                $country = $_POST[BIS_COUNTRY_SELECT];
                setcookie(BIS_COUNTRY_SELECT, $country, time() + BIS_COOKIE_EXPIRE_TIME, COOKIEPATH, COOKIE_DOMAIN);
            }
            
            // Load Rules engine configuration dependency file only for admin console.
            if (is_admin()) {
                
                if (current_user_can('administrator')) {
                    add_action('admin_bar_menu', array($this, 'add_mega_toolbar_items'), 999);
                } 
                
                wp_dequeue_style('bis-pf-re-site-css-min');
                wp_dequeue_script('bis-mega-cache-ctrl-site');

                if (!RulesEngineUtil::is_bis_re_plugin_page() && (admin_url('edit.php') || admin_url('post-new.php') || admin_url('post.php'))) {
                    wp_enqueue_style('bis-token-input-css', plugins_url('/css/token-input-facebook.css', __FILE__));
                    wp_enqueue_script('bis-token-input-js', plugins_url('/js/lib/jquery.tokeninput.js', __FILE__));
                    wp_enqueue_style('bis-date-picker-css', plugins_url('/css/jquery.datetimepicker.css', __FILE__));
                    wp_enqueue_script('bis-date-picker-js', plugins_url('/js/lib/jquery.datetimepicker.js', __FILE__));
                    add_action('admin_head', array($this->rules_engine, 'bis_content_filter_to_editor'));
                    add_action('admin_footer', array($this->rules_engine, 'bis_logical_rules_tinymce'));
                    
                    wp_register_style('bis-dashboard-css', plugins_url('/css/bis-dashboard.css', __FILE__));
                    wp_enqueue_style('bis-dashboard-css');

                    wp_enqueue_script('bis-ajax-request', plugin_dir_url(__FILE__) . 'js/bis-admin-action.js', array('jquery'));
                  
                    // declare the URL to the file that handles the AJAX request (wp-admin/admin-ajax.php)
                    wp_localize_script('bis-ajax-request', 'BISAjax', array(
                        // URL to wp-admin/admin-ajax.php to process the request
                        'ajaxurl' => admin_url('admin-ajax.php'),
                        // so that you can check it later when an AJAX request is sent
                        'bis_rules_engine_nonce' => wp_create_nonce('bis_rules_engine_nonce')
                    ));
                }

                if (RulesEngineUtil::is_bis_re_plugin_page()) {
                    wp_enqueue_script('jquery');
                    wp_enqueue_script('bis-bootpag-pagination', plugins_url('/js/lib/full/jquery.bootpag.js', __FILE__));
                    wp_enqueue_script('bis-bootstrap-multiselect', plugins_url('/js/lib/bootstrap-multiselect.js', __FILE__));
                    //wp_enqueue_script('bis-google-places-api-js', plugins_url('/js/lib/google-places-api.js', __FILE__));
                    wp_enqueue_script('bis-logical-rules', plugins_url('/js/bis-logical-rules.js', __FILE__));
                    wp_enqueue_script('bis-settings', plugins_url('/js/bis-settings.js', __FILE__));
                    wp_enqueue_script('bis-rulesengine', plugins_url('/js/bis-rulesengine.js', __FILE__));
                    wp_enqueue_script('bis-ajax-request', plugins_url('/js/bis-pf-re-admin.min.js', __FILE__));
                    
                    // declare the URL to the file that handles the AJAX request (wp-admin/admin-ajax.php)
                    wp_localize_script('bis-ajax-request', 'BISAjax', array(
					// URL to wp-admin/admin-ajax.php to process the request
                        'ajaxurl' => admin_url('admin-ajax.php'),
                        // so that you can check it later when an AJAX request is sent
                        'bis_rules_engine_nonce' => wp_create_nonce('bis_rules_engine_nonce')
                    ));

                    wp_enqueue_script('jquery-form');
                    wp_register_style('bis-pf-re-admin-css-min', plugins_url('/css/bis-pf-re-admin.min.css', __FILE__));
                    wp_enqueue_style('bis-pf-re-admin-css-min');
                    wp_register_style('bis-rules-engine-analytics-css', plugins_url('/css/bis-rules-engine-analytics.css', __FILE__));
                    wp_enqueue_style('bis-rules-engine-analytics-css');
                    wp_enqueue_style('bis-admin-icons',plugins_url('/css/bis-admin-icons.css',__FILE__), '', '1.0');
                    wp_enqueue_style('bis-admin-icons');
                    wp_enqueue_style('font-awesome.min', plugins_url('/css/font-awesome.min.css', __FILE__));
                    wp_enqueue_style('font-awesome.min');
                    wp_enqueue_style('bis-jquery-builder', plugins_url('/css/bis-jquery-builder.css', __FILE__));
                    wp_enqueue_style('bis-jquery-builder');

                    wp_enqueue_script('bis-logical-rules-validator', plugins_url('/js/bis-logical-rules-validator.js', __FILE__));
                    wp_enqueue_script('bis-child-rules-validator-js', plugins_url('/js/bis-child-rules-validator.js', __FILE__));
                    wp_enqueue_script('bis-query-builder.standalone', plugins_url('/js/lib/query-builder.standalone.min.js', __FILE__));
                    wp_enqueue_script('bis-sortable-interact', plugins_url('/js/lib/interact.min.js', __FILE__));
                    wp_localize_script('bis-child-rules-validator-js', 'breCV', \bis\repf\common\RulesEngineLocalization::get_bis_re_child_validations());
                } else {
                    wp_dequeue_style('bis-pf-re-admin-css-min');
                    wp_dequeue_style('font-awesome.min');
                }
            } 
            if(!is_admin()) {
                // Site libraries
 		wp_enqueue_script('jquery');
                wp_dequeue_script('bis-mega-cache-ctrl-site');
                wp_dequeue_script('bis-logical-rules-validator');
                wp_dequeue_script('bis-child-rules-validator-js');
                wp_dequeue_script('bis-query-builder.standalone');
                wp_dequeue_style('bis-pf-re-admin-css-min');
                wp_enqueue_script('bis-ajax-request', plugin_dir_url(__FILE__) . 'js/bis-pf-re-site.min.js', array('jquery'));
                
                if (RulesEngineUtil::get_option(BIS_RULES_ENGINE_CACHE_INSTALLED, false, false) == "true") {
                    wp_enqueue_script('bis-mega-cache-ctrl-site', plugin_dir_url(__FILE__) .'js/bis-mega-cache-ctrl-site.js', array('jquery'));
                }  
                
                //wp_enqueue_script('bis-google-places-api-js', plugins_url('/js/lib/google-places-api.js', __FILE__));
                //wp_enqueue_script('bis-logical-site', plugins_url('/js/bis-logical-site.js', __FILE__));
                wp_enqueue_script('bis-popup-site', plugins_url('/js/bis-popup-site.js', __FILE__));
                
                /*add_action('wp', array(&$this->rules_engine, 'bis_re_apply_logical_rules'), 15);

                if ((isset($_GET['bis_re_action']) &&
                        ($_GET['bis_re_action'] === 'bis_re_logical_popup'))) {
                    bis_re_logical_popup();
                }*/
                
                if ($GLOBALS['pagenow'] !== 'wp-login.php') {
                    if ((isset($_GET['bis_re_action']) &&
                            ($_GET['bis_re_action'] === 'bis_re_popup_show'))) {
                        bis_re_popup_show();
                    }
                }

                // declare the URL to the file that handles the AJAX request (wp-admin/admin-ajax.php)
                wp_localize_script('bis-ajax-request', 'BISAjax', array(
					// URL to wp-admin/admin-ajax.php to process the request
                    'ajaxurl' => admin_url('admin-ajax.php'),
                    // so that you can check it later when an AJAX request is sent
                    'bis_rules_engine_nonce' => wp_create_nonce('bis_rules_engine_nonce'),
                    'bis_nprg_color' => RulesEngineUtil::get_option(BIS_NPROGRESS_BAR_COLOR_CONST, false, false),
                    'bis_cached_site' => RulesEngineUtil::get_option(BIS_RULES_ENGINE_CACHE_INSTALLED, false, false)
                ));
                
                wp_register_style('bis-pf-re-site-css-min', plugins_url('/css/bis-pf-re-site.min.css', __FILE__));
                
                wp_enqueue_style('bis-pf-re-site-css-min');
                wp_register_style('bis-re-remodal-theme', plugins_url('/css/bis-re-remodal-theme.css', __FILE__));
                wp_enqueue_style('bis-re-remodal-theme');
            }
            
            $this->rules_engine->bis_start_session();
            $this->rules_engine->set_cache_cookie();
            
            //Code to handle cache sites
            if (!is_admin() && isset($_GET['bis_re_action']) &&
                    ($_GET['bis_re_action'] === 'bis_re_mega_rules')) {
                bis_re_mega_rules();
            }
            
            if (isset($_POST['bis_clear_search_box']) && $_POST['bis_clear_search_box'] === 'clear_search_box') {
                bis_clear_search_box_val();
            }
        }

        /**
         * This method used to load the template and config file
         */
        function show_rules_engine_config() {
            $dirName = dirname(__FILE__);

            // Give you the real path

            global $baseName;

            $baseName = basename(realpath($dirName));

            ob_start();

            if (is_admin()) {
                require_once('includes/config-rules.php');
                require_once('template/bis-rules-engine-template.html');
            }

            echo ob_get_clean();
        }

        function localization_init() {
            $path = dirname(plugin_basename(__FILE__)) . '/languages/';
            $loaded = load_plugin_textdomain('rulesengine', false, $path);

            // Debug to check localization file loader
            /* if (!$loaded) {
              echo '<div class="error"> Localization: ' . __('Could not load the localization file: ' . $path, 'rulesengine') . '</div>';
              return;
              } */
        }

        function bis_page_cache_loader_script() {
            // capture output from here on...
            ob_start(function($html) {
                // this function runs after the capture ends,
                // you can replace your tags here 
                $html = "<!--Page updated by Mega Platform on ". current_time( 'mysql' )."-->"
                    . $this->rules_engine->bis_re_append_page_info($html) . "</span>";

                return $html;
            });
        }

        function bis_show_settings_menu() {
            add_submenu_page('bis_pg_dashboard', 'Logical Rules', __('Logical Rules', BIS_RULES_ENGINE_TEXT_DOMAIN), 'manage_options', 'bis_pg_rulesengine', array(&$this, 'show_rules_engine_config'));
            add_submenu_page('bis_pg_dashboard', 'Site Settings', __('Site Settings', BIS_RULES_ENGINE_TEXT_DOMAIN), 'manage_options', 'bis_pg_site_settings', array(&$this, 'show_rules_engine_config'));
            /*if(BIS_RULES_ENGINE_PLATFORM_TYPE === "BIS_RE_PROF_EDITION") {
                add_submenu_page('bis_pg_dashboard', 'Analytics', __('Analytics', BIS_RULES_ENGINE_TEXT_DOMAIN), 'manage_options', 'bis_pg_analytics', array(&$this, 'show_rules_engine_config'));                   
            }*/
           
            if (!is_multisite()) {
                add_submenu_page('bis_pg_dashboard', 'Global Settings', __('Global Settings', BIS_RULES_ENGINE_TEXT_DOMAIN), 'manage_options', 'bis_pg_global_settings', array(&$this, 'show_rules_engine_config'));
            }
            
        }
        
        /*
         * This mehod add Mega Platform toolbar
         */
        function add_mega_toolbar_items($wp_admin_bar) {
            if (RulesEngineUtil::is_bis_re_plugin_page()) {
                $args = array(
                    'id' => 'wpmegaplatform',
                    'title' => '<span class = "ab-icon"><img src = ' . plugins_url('/image/rules-engine-small-icon.png', __FILE__) . ' style = "padding-bottom: inherit;"></span><span class = "ab-label awaiting-mod pending-count count-0" aria-hidden = "true">' . __('Mega Platform') . '</span>',
                    'href' => get_dashboard_url() . 'admin.php?page=bis_pg_dashboard',
                    'meta' => array(
                        'class' => 'wpmegaplatform',
                        'title' => 'Goto Mega Platform dashboard'
                    )
                );
            } else {
                $args = array(
                    'id' => 'wpmegaplatform',
                    'title' => '<span class = "ab-icon"><img src = ' . plugins_url('/image/rules-engine-small-icon.png', __FILE__) . ' style = "padding-top: inherit;"></span><span class = "ab-label awaiting-mod pending-count count-0" aria-hidden = "true">' . __('Mega Platform') . '</span>',
                    'href' => get_dashboard_url() . 'admin.php?page=bis_pg_dashboard',
                    'meta' => array(
                        'class' => 'wpmegaplatform',
                        'title' => 'Goto Mega Platform dashboard'
                    )
                );
            }
            $wp_admin_bar->add_node($args);
        }

        /**
         * This function cleans up the rules engine database.
         */
        public static function bis_rules_engine_uninstall() {

            global $wpdb;

            $bis_delete_db = RulesEngineUtil::get_option(BIS_RULES_ENGINE_DELETE_DB, is_network_admin());

            // Delete db only if checkbox for delete is selected.
            if ($bis_delete_db === "true") {

                $tables = array();

                if (is_multisite()) {
                    $blog_ids = $wpdb->get_col("SELECT blog_id FROM " . $wpdb->prefix . "blogs");
                    foreach ($blog_ids as $blog_id) {
                        switch_to_blog($blog_id);
                        $table_prefix = $wpdb->prefix;
                        array_push($tables, $table_prefix . "bis_re_sub_option_condition");
                        array_push($tables, $table_prefix . "bis_re_sub_option");
                        array_push($tables, $table_prefix . "bis_re_option");
                        array_push($tables, $table_prefix . "bis_re_rules");
                        array_push($tables, $table_prefix . "bis_re_rule_details");
                        array_push($tables, $table_prefix . "bis_re_condition");
                        array_push($tables, $table_prefix . "bis_re_logical_rule_value");
                        array_push($tables, $table_prefix . "bis_re_logical_rules_criteria");
                        array_push($tables, $table_prefix . "bis_re_logical_rules");
                        array_push($tables, $table_prefix . "bis_re_report_auth");
                        array_push($tables, $table_prefix . "bis_re_report_data");
                        array_push($tables, $table_prefix . "bis_re_report");
                        restore_current_blog();
                    }
                } else {
                    $base_prefix = $wpdb->base_prefix;
                    array_push($tables, $base_prefix . "bis_re_sub_option_condition");
                    array_push($tables, $base_prefix . "bis_re_sub_option");
                    array_push($tables, $base_prefix . "bis_re_option");
                    array_push($tables, $base_prefix . "bis_re_rules");
                    array_push($tables, $base_prefix . "bis_re_rule_details");
                    array_push($tables, $base_prefix . "bis_re_condition");
                    array_push($tables, $base_prefix . "bis_re_logical_rule_value");
                    array_push($tables, $base_prefix . "bis_re_logical_rules_criteria");
                    array_push($tables, $base_prefix . "bis_re_logical_rules");
                    array_push($tables, $base_prefix . "bis_re_report_auth");
                    array_push($tables, $base_prefix . "bis_re_report_data");
                    array_push($tables, $base_prefix . "bis_re_report");
                }
         
                foreach ($tables as $table) {
                    $wpdb->query("DROP TABLE IF EXISTS $table");
                }

                // Delete options
                RulesEngineUtil::delete_option(BIS_RULES_ENGINE_VERSION_CONST, is_network_admin());

                RulesEngineUtil::delete_option(BIS_RULES_ENGINE_DELETE_DB, is_network_admin());

                RulesEngineUtil::delete_option(BIS_RULES_ENGINE_ALLOWABLE_TAGS_CONST, is_network_admin());
                
                RulesEngineUtil::delete_option(BIS_MEGA_PLATFORM_PLUGIN_ID, is_network_admin());
                
                // Delete addons
                RulesEngineUtil::delete_option(BIS_RULESENGINE_ADDONS_CONST, is_network_admin());

                // Delete Geo name user
                RulesEngineUtil::delete_option(BIS_GEO_NAME_USER, is_network_admin());

                // Delete plugin location
                RulesEngineUtil::delete_option(BIS_RULES_ENGINE_PLATFORM_DIR, is_network_admin());

                RulesEngineUtil::delete_option(BIS_RULES_ENGINE_PLUGIN_FORCE_DELETE, is_network_admin());

                RulesEngineUtil::delete_option(BIS_CAPTURE_ANALYTICS_DATA, is_network_admin());

                RulesEngineUtil::delete_option(BIS_GEO_LOOKUP_TYPE, is_network_admin());
                
                RulesEngineUtil::delete_option(BIS_GEO_IP2LOCATION_CSV_LOOKUP_TYPE);
                
                RulesEngineUtil::delete_option(BIS_POPUP_VO, false, false);
                RulesEngineUtil::delete_option(BIS_CONTINENT_COUNTRY_POPUP_TEMPLATE);
                RulesEngineUtil::delete_option(BIS_CONTINENT_POPUP_TEMPLATE);
                RulesEngineUtil::delete_option(BIS_COUNTRY_FLAGS_POPUP_TEMPLATE);
                RulesEngineUtil::delete_option(BIS_COUNTRY_POPUP_TEMPLATE);
                RulesEngineUtil::delete_option(BIS_REGION_POPUP_TEMPLATE);
                RulesEngineUtil::delete_option(BIS_CITY_POPUP_TEMPLATE);
                RulesEngineUtil::delete_option(BIS_POPUP_TEMPLATE);

                RulesEngineUtil::delete_option(BIS_GEO_MAXMIND_DB_FILE, is_network_admin());

                RulesEngineUtil::delete_option(BIS_RULES_ENGINE_CACHE_INSTALLED, is_network_admin());

                RulesEngineUtil::delete_option(BIS_REDIRECT_META_TEMPLATE, is_network_admin());
                
                RulesEngineUtil::delete_option(BIS_GEO_LOOKUP_WEBSERVICE_TYPE, is_network_admin());
                
                RulesEngineUtil::delete_option(BIS_GEO_IP2LOCATION_LOOKUP_TYPE, is_network_admin());

                RulesEngineUtil::delete_option(BIS_REDIRECT_POPUP_TEMPLATE, is_network_admin());
                
                RulesEngineUtil::delete_option(BIS_LOGICAL_CACHE_REFRESH_ID, is_network_admin());
                
                // Delete nprogress bar color
                //RulesEngineUtil::delete_option(BIS_NPROGRESS_BAR_COLOR_CONST, false, false);
                //RulesEngineUtil::delete_option(BIS_RULES_EVALUATION_CONT, false, false);
                
                RulesEngineUtil::delete_option(BIS_RULES_DEBUG_MODE_CONT, false, false);
                
                // Delete keys used for whitelist and blocklist ip address
                RulesEngineUtil::delete_option(BIS_MEGA_SEL_IPS);
                RulesEngineUtil::delete_option(BIS_MEGA_BLACK_IPS);
                RulesEngineUtil::delete_option(BIS_MEGA_BLACK_RED);

                //Delete keys used for cache
                RulesEngineUtil::delete_option(BIS_MEGA_PCODE_KEY, false, false);
                RulesEngineUtil::delete_option(BIS_MEGA_CITY_KEY, false, false);
                RulesEngineUtil::delete_option(BIS_MEGA_STATE_KEY, false, false);
                RulesEngineUtil::delete_option(BIS_MEGA_COUNTRY_KEY, false, false);
                RulesEngineUtil::delete_option(BIS_MEGA_CONTINENT_KEY, false, false);

                RulesEngineUtil::delete_option(BIS_RULES_ENGINE_CACHE_INSTALLED, false, false);
                RulesEngineUtil::delete_option(BIS_MEGA_COUNTRY_DROPDOWN, false, false);
                RulesEngineUtil::delete_option(BIS_PUR_CODE . BIS_MEGA_PLATFORM_PLUGIN_ID, false, false);

                $upload_dir = wp_upload_dir();
                $upload_dirname = $upload_dir['basedir'] . '/' . BIS_UPLOAD_DIRECTORY;
                RulesEngineUtil::delete_directory($upload_dirname);
                
            }
        }
        
    }
}