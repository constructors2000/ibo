<?php
use bis\repf\common\RulesEngineCacheWrapper;

$productRulesEngineModal = new ProductRulesEngineModel();
$countries = $productRulesEngineModal->get_countries();
$current_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$selected_country_value  = RulesEngineCacheWrapper::get_session_attribute(BIS_COUNTRY_DROPDOWN_VALUE);
$session_selected_country_value = "";
if (RulesEngineCacheWrapper::get_session_attribute(BIS_GEOLOCATION_VO) !== null) {
    $geo_values = RulesEngineCacheWrapper::get_session_attribute(BIS_GEOLOCATION_VO);
    $session_selected_country_value = $geo_values->countryCode;
    if ($selected_country_value == null && $selected_country_value == "") {
        $selected_country_value = $session_selected_country_value;
    }
}
if($selected_country_value !== null && $selected_country_value !== "" && $selected_country_value !== $session_selected_country_value){
    $country_popup_display = "none";
} else {
    $country_popup_display = "block";
}
$type = get_post_type();
if($type === 'product'){ ?>
    <html>
        <head>
            <style>
                div#bis_fea_modal_popup {
                    position: fixed; 
                    width: 100%; 
                    height: 100%; 
                    background: #00000036;
                    display:block;
                    top:0; 	
                    z-index:9999999;
                }
                .bis_fea_bis_mega_country_dropdown{
                    position:relative;
                    width:600px;
                    height:auto;
                    top:50%;
                    left:50%;
                    transform:translate(-50%,-50%) scale(1);
                }
                .bis_fea_bis_mega_country_popup{
                    background:#fff;
                    text-align:center;
                }
                .bis_fea_country_dropdown_heading{
                    padding:10px;
                }
                .bis_fea_country_dropdown_select {
                    padding: 40px;
                }
                .bis_fea_country_dropdown_select select {
                    height: 33px;
                    box-sizing: border-box;
                    padding-left: 22px;
                    width: 250px;
                    max-width: 100%;
                    max-height: 100%;
                    border-radius: 18px;
                }
                span.bis_fea_bis_mega_country-popup-close {
                    position: absolute;
                    right: 11px;
                    font-size: 35px;
                    cursor: pointer;
                }
            </style>
            <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
        </head>
        <body>
            <div class="bis_fea_pop_up" id="bis_fea_modal_popup" style="display:<?php echo $country_popup_display; ?>">
                <div class="bis_fea_bis_mega_country_dropdown">
                    <span class="bis_fea_bis_mega_country-popup-close">&times;</span>
                    <div class="bis_fea_bis_mega_country_popup">
                        <div class="bis_fea_country_dropdown_heading">
                            <h2>Choose your country</h2>
                        </div>	
                        <div class="bis_fea_country_dropdown_select">
                            <form id="bis_fea_country_dropdown_form" name="bis_fea_country_dropdown_form" action="<?php echo $current_url; ?>" method="post">
                                <select id="bis_fea_country_dropdown_select" name="bis_fea_country_dropdown_select">
                                    <?php 
                                        if(!empty($countries)){
                                            foreach ($countries as $country) { ?>
                                    <option value="<?php echo $country->id ?>" <?php echo selected($country->id, $selected_country_value); ?> ><?php echo $country->name; ?></option>
                                            <?php }
                                        }
                                    ?>
                                </select>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </body>
        <script>
            jQuery(document).ready(function () {
                jQuery('#bis_fea_country_dropdown_select').change(function(){
                    jQuery("#bis_fea_country_dropdown_form").submit();
                });
                jQuery('.bis_fea_bis_mega_country-popup-close').click(function(){
                    jQuery("#bis_fea_modal_popup").hide();
                });
                var selectedVal = '<?php echo $selected_country_value; ?>';
                if(selectedVal !== null && selectedVal !== ''){
                    jQuery("#bis_fea_country_dropdown_select").val(selectedVal);
                }
            });
        </script>
    </html>
<?php } 