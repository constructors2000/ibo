<?php
/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

/*
 * Plugin Name: Mega Addon - WooCommerce Product Restrictions
 * Plugin URI: http://megaedzeetechnologies.com/
 * Description: This plugin is designed for defining rules for woocommerce Product, Shipping & Payment Restrictions</strong>. This plugin requires "Mega Enterprise Platform". 
 * Version: 2.06
 * Author: MegaEdzee Technologies
 * Author URI:  http://megaedzeetechnologies.com/
 * Text Domain: productrules
 * Domain Path: /languages/
 * License: Commercial
 *
 */

use bis\repf\action\PostRulesEngine;

if (!class_exists('BIS_Woo_Product_Controller')) {

    class BIS_Woo_Product_Controller {

        public $version = '2.06';
        private static $_instance = null;

        public static function instance() {

            if (is_null(self::$_instance)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        public function __clone() {
            _doing_it_wrong(__FUNCTION__, __('You cannot clone Product Rules', BIS_PRODUCT_RULES_TEXT_DOMAIN), '2.06');
        }

        public function __wakeup() {
            _doing_it_wrong(__FUNCTION__, __('You cannot clone Product Rules', BIS_PRODUCT_RULES_TEXT_DOMAIN), '2.06');
        }

        function __construct() {
            $dirName = dirname(__FILE__);
            $realPath = realpath($dirName);

            $this->define_constants($realPath);
            $this->includes($realPath);
            $this->init_hooks($realPath);
        }

        private function check_prerequisites() {
            if (!class_exists('BISRE_Common_Platform')) {
                die("<span style=\"color:red\">\"Woo Product Controller \" plugin requires 
                    \"Mega Enterprise Platform\" plugin.<br/> Please install and activate \"Mega Enterprise Platform\" plugin
                    before activating \"Woo Product Controller \". </span>");
            }
        }

        /**
         * Define Platform Constants.
         */
        private function define_constants($realPath) {
            require_once($realPath . "/util/product-rules-util.php");
            $this->define("BIS_PRODUCT_RULES_VERSION", $this->version);
            $this->define("BIS_PRODUCT_RULES_HOME_DIR", $realPath);
            $this->define('BIS_PRODUCT_RULES_PLUGIN_BASENAME', plugin_basename(__FILE__));
            $this->define("BIS_PRODUCT_RULESENGINE_PLATFORM", ProductRulesUtil::get_option("BIS_RULES_ENGINE_PLATFORM_DIR"));
            $this->define("BIS_PRODUCT_RULESENGINE_PLATFORM_FULL_PATH", ProductRulesUtil::bis_get_platform_plugin_path());
        }

        private function includes($realPath) {

            $this->check_prerequisites();
            require_once(BIS_PRODUCT_RULESENGINE_PLATFORM_FULL_PATH . "/bis-platfom-common-include.php");
            $bis_common_platform = new BISRE_Common_Platform();
            $bis_common_platform->includes();

            require_once($realPath . "/common/product-rules-constants.php");
            require_once($realPath . "/install/bis-product-rules-install.php");


            require_once(BIS_PRODUCT_RULESENGINE_PLATFORM_FULL_PATH . "/bis/repf/install/bis-rules-addon-install.php");
            require_once(BIS_PRODUCT_RULESENGINE_PLATFORM_FULL_PATH . "/bis/repf/model/base-rules-engine-model.php");
            require_once(BIS_PRODUCT_RULESENGINE_PLATFORM_FULL_PATH . "/bis/repf/action/base-rules-engine.php");
            require_once(BIS_PRODUCT_RULESENGINE_PLATFORM_FULL_PATH . "/bis/repf/vo/plugin-vo.php");
            require_once(BIS_PRODUCT_RULESENGINE_PLATFORM_FULL_PATH . "/bis/repf/vo/cache-vo.php");
            require_once(BIS_PRODUCT_RULESENGINE_PLATFORM_FULL_PATH . "/bis/repf/model/post-rules-engine-model.php");
            require_once(BIS_PRODUCT_RULESENGINE_PLATFORM_FULL_PATH . "/bis/repf/action/post-rules-engine.php");

            require_once($realPath . "/model/product-rules-engine-model.php");
            require_once($realPath . "/includes/product-rule-admin.php");
            require_once($realPath . '/ajax/product-rules-ajax.php');
            require_once($realPath . "/action/product-rules-engine.php");
        }

        private function init_hooks($realPath) {

            $product_rules_engine = new ProductRulesEngine();

            add_action('init', array(&$this, 'bis_load_product_dependents'), 20);

            add_action('admin_menu', array(&$this, 'bis_show_product_menu'), 51);

            // Localization
            add_action('plugins_loaded', array(&$this, 'product_localization_init'));

            // Delete cached session rules
            add_action('bis_clear_cached_rules', array($product_rules_engine, 'bis_clear_cached_product_rules'));

            // Content Filter
            // Activate and deactivate plugin hooks
            if (is_admin()) {
                register_activation_hook(__FILE__, 'ProductRulesInstall::bis_product_rules_activation');
                register_deactivation_hook(__FILE__, 'ProductRulesInstall::bis_product_rules_deactivation');
                register_uninstall_hook(__FILE__, 'ProductRulesInstall::bis_product_rules_uninstall');
                add_filter('plugin_row_meta', array(&$this, 'bis_product_plugin_row_meta'), 10, 2);

                //apply rules from products list
                add_action('manage_posts_extra_tablenav', 'bis_add_rule_to_products');
                add_filter('manage_posts_columns', 'bis_add_product_rule_applied');
                add_action('manage_posts_custom_column', 'bis_is_product_rule_applied', 10, 2);
                add_action('manage_posts_extra_tablenav', 'bis_add_product_rules');

                //product meta box
                add_action('add_meta_boxes', 'bis_product_add_rule_meta_box');
                add_action('admin_init', 'bis_product_add_rule_meta_box', 1);
                add_action('save_post', 'bis_apply_product_rule_from_meta_box');
                
                //product tab
                add_filter('woocommerce_product_data_tabs', array($product_rules_engine, 'bis_custom_product_tabs'));
                add_filter('woocommerce_product_data_panels', array($product_rules_engine, 'bis_add_product_general_box_fields'));
                add_action('woocommerce_process_product_meta_simple', array($product_rules_engine, 'bis_save_product_general_box_fields'));

                //payment tab
                add_filter('woocommerce_product_data_tabs', array($product_rules_engine, 'bis_custom_payment_tabs'));
                add_filter('woocommerce_product_data_panels', array($product_rules_engine, 'bis_add_payment_general_box_fields'));
                add_action('woocommerce_process_product_meta_simple', array($product_rules_engine, 'bis_save_payment_general_box_fields'));

                //shipping tab
                add_filter('woocommerce_product_data_tabs', array($product_rules_engine, 'bis_custom_shipping_tabs'));
                add_filter('woocommerce_product_data_panels', array($product_rules_engine, 'bis_add_shipping_general_box_fields'));
                add_action('woocommerce_process_product_meta_simple', array($product_rules_engine, 'bis_save_shipping_general_box_fields'));
                add_action('admin_head', array($product_rules_engine, 'bis_product_tab_icon_style'));
                
                //Custome developement
                add_action('save_post', array($product_rules_engine, 'bis_add_product_rule_based_on_author'));

                //woo settings new tab
                add_filter('woocommerce_settings_tabs_array', array($product_rules_engine, 'bis_woo_sett_product_rules_tab'), 50);
                add_action('woocommerce_settings_tabs_bis_product_rules_tab', array($product_rules_engine, 'bis_product_rules_tab'));
                add_action('woocommerce_admin_field_bis_woo_settings_product_rule_section_fields', array($product_rules_engine, 'bis_woo_settings_product_rule_section_fields'));
                add_action('woocommerce_admin_field_bis_woo_settings_shipping_rule_section_fields', array($product_rules_engine, 'bis_woo_settings_shipping_rule_section_fields'));
                add_action('woocommerce_admin_field_bis_woo_settings_payment_rule_section_fields', array($product_rules_engine, 'bis_woo_settings_payment_rule_section_fields'));
                add_filter('woocommerce_update_options_bis_product_rules_tab', array($product_rules_engine, 'bis_woo_settings_product_rule_page_fields_update_option'));
            }
            if (!is_admin()) {
                
                add_shortcode('bis_country_dropdown_popup', array($product_rules_engine, 'bis_country_dropdown_popup'));
                add_shortcode('bis_country_dropdown_popup_widget', array($product_rules_engine, 'bis_country_dropdown_popup_widget'));
                add_action('woocommerce_product_query', array(&$product_rules_engine, 'bis_re_exclude_products'), 55);
                add_filter('woocommerce_product_categories_widget_args', array(&$product_rules_engine, 'bis_exclude_product_cat_in_widget'), 60);
                add_filter('woocommerce_product_tag_cloud_widget_args', array(&$product_rules_engine, 'bis_exclude_product_tag_in_widget'), 61);
                add_filter('woocommerce_available_payment_gateways', array($product_rules_engine, 'bis_conditional_payment_gateways'));
                add_action('woocommerce_single_product_summary', array($product_rules_engine, 'bis_add_shipping_desc_to_product'), 60);
                
                //shipping enable/disable
                add_action('woocommerce_checkout_process', array(&$product_rules_engine, 'bis_conditional_shipping_enable_disable'));
                add_action('woocommerce_checkout_process', array(&$product_rules_engine, 'bis_conditional_shipping_based_on_distance'));
                
                //restrict products
                add_action('woocommerce_add_to_cart_validation', array(&$product_rules_engine, 'bis_restrict_products_based_on_rules'), 20, 3);
                //restrict products
                add_action('woocommerce_add_to_cart_validation', array(&$product_rules_engine, 'bis_add_dependent_products_based_on_rules'), 20, 3);
                add_action('woocommerce_remove_cart_item', array(&$product_rules_engine, 'bis_remove_dependent_products_based_on_rules'), 10, 2);
            }
            
            //shipping charges
            add_filter('woocommerce_package_rates', array($product_rules_engine, 'bis_add_shipping_charges_based_on_rules'), 10, 2);
            add_filter('woocommerce_package_rates', array($product_rules_engine, 'bis_add_shipping_charges_based_on_distance'), 10, 2);
            
            // this is for while woocommerce ajax triggers
            if(defined( 'DOING_AJAX' )){

                //restrict products
                add_action('woocommerce_add_to_cart_validation', array(&$product_rules_engine, 'bis_restrict_products_based_on_rules'), 20, 3);

                //restrict products
                add_action('woocommerce_add_to_cart_validation', array(&$product_rules_engine, 'bis_add_dependent_products_based_on_rules'), 20, 3);
            }
        }

        function bis_load_product_dependents() {
            
            if (is_admin()) {
                wp_enqueue_script('bis-product-new-fields-js', plugins_url('/js/bis-product-new-fields.js', __FILE__));
                wp_enqueue_script('bis-woo-sett-mega-tab-js', plugins_url('/js/bis-woo-settings-mega-tabs.js', __FILE__));
                wp_register_style('product-rules-css', plugins_url('/css/product-rules.css', __FILE__));
                wp_enqueue_style('product-rules-css');
            }
            if (is_admin() && RulesEngineUtil::is_bis_re_plugin_page()) {
                wp_enqueue_script('bis-product-rules-js', plugins_url('/js/bis-product-rules.js', __FILE__));
                wp_localize_script('bis-product-rules-js', 'productreCV', ProductRulesUtil::get_bis_re_product_validations());
            } elseif (!is_admin()) {
                wp_enqueue_script('bis-product-site', plugin_dir_url(__FILE__) . 'js/bis-product-site.js', array('jquery'));
                $post_rule_engine = new PostRulesEngine();
                add_shortcode('bis-post-rule-append', array(&$post_rule_engine, 'bis_post_rule_shortcode'));
                if ((isset($_GET['bis_re_action']) &&
                        ($_GET['bis_re_action'] === 'bis_re_product_popup'))) {
                    bis_re_product_popup();
                }
            }
        }

        function bis_show_product_menu() {
            add_submenu_page('bis_pg_dashboard', 'Woo Product Controller', __('Product Controller', BIS_PRODUCT_RULES_TEXT_DOMAIN), 'manage_options', 'bis_pg_productrules', array(&$this, 'show_product_rules_config'));
        }

        /**
         * This method is used to show additional links in Plugin installation page.
         * 
         * @param type $links
         * @param type $file
         * @return type
         */
        public function bis_product_plugin_row_meta($links, $file) {
            if (BIS_PRODUCT_RULES_PLUGIN_BASENAME == $file) {
                unset($links[2]);
                $row_meta = array(
                    'docs' => '<a target="_blank" href="' . esc_url('http://docs.megaedzee.com/docs/product-controller/') . '">' . esc_html__('Docs', 'woocommerce') . '</a>',
                    'support' => '<a target="_blank" href="' . esc_url('http://support.megaedzee.com/') . '">' . esc_html__('Support', 'rulesengine') . '</a>',
                );

                return array_merge($links, $row_meta);
            }

            return (array) $links;
        }

        /**
         * This method used to load the template and config file
         */
        function show_product_rules_config() {

            $dirName = dirname(__FILE__);
            // Give you the real path

            global $baseName;

            $baseName = basename(realpath($dirName));

            ob_start();

            if (is_admin()) {

                if (RulesEngineUtil::get_option(BIS_PUR_CODE . BIS_PLATFORM_PRODUCT_PLUGIN_ID)) {
                    require_once('includes/config-rules.php');
                    require_once('template/bis-product-rules-template.html');
                } else {
                    require_once(BIS_PRODUCT_RULESENGINE_PLATFORM_FULL_PATH . "/includes/bis-verify-pcode.php");
                }
            }
        }

        function product_localization_init() {
            $path = dirname(plugin_basename(__FILE__)) . '/languages/';
            $loaded = load_plugin_textdomain(BIS_PRODUCT_RULES_TEXT_DOMAIN, false, $path);

            // Debug to check localization file loader
            //    if (!$loaded) {
            //      echo '<div class="error"> Localization: ' . __('Could not load the localization file: ' . $path, BIS_PRODUCT_RULES_TEXT_DOMAIN) . '</div>';
            //      return;
            //      } 
        }

        private function define($name, $value) {
            if (!defined($name)) {
                define($name, $value);
            }
        }

    }

}

function product_rule_init() {
    return BIS_Woo_Product_Controller::instance();
}

// Global for backwards compatibility.
$GLOBALS['productrules'] = product_rule_init();
