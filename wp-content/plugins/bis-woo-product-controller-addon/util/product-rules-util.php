<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

use bis\repf\vo\LabelValueVO;

class ProductRulesUtil {

    private function __construct() {
        
    }

    public static function getIncludesDirPath() {

        $pluginPath = ProductRulesUtil::getPluginAbsPath() . "/includes/";

        return $pluginPath;
    }

    public static function getPluginAbsPath() {

        $dirName = plugin_dir_path(__FILE__);
        $pluginPath = realpath($dirName);
        $path_array = explode("util", $pluginPath);

        return $path_array[0];
    }

    public static function get_bis_re_product_validations() {
        $translation_array = array(
        'enter_rule_name' => __('Enter rule name', BIS_PRODUCT_RULES_TEXT_DOMAIN ),
        'select_logical_rule' => __('Select logical rule', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'rule_name_special_char' => __('Rule name should not contain special characters.', BIS_PRODUCT_RULES_TEXT_DOMAIN ),
        'select_product' => __('Select product', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'delete_confirm' => __('Do want to delete rule', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'invalid_product' => __('Invalid product file, Enter a mo product file.', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'enter_product_file' => __('Enter product file name.', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'enter_product_button_label' => __('Enter product button label.', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'enter_cancel_button_label' => __('Enter cancel button label.', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'select_product_type' => __('Select product type', BIS_PRODUCT_RULES_TEXT_DOMAIN ),
        'select_products' => __('Select product', BIS_PRODUCT_RULES_TEXT_DOMAIN ),
        'select_woo_tags' => __('Select tag', BIS_PRODUCT_RULES_TEXT_DOMAIN ),
        'select_woo_attrs' => __('Select attribute', BIS_PRODUCT_RULES_TEXT_DOMAIN ),
        'select_woo_cats' => __('Select category', BIS_PRODUCT_RULES_TEXT_DOMAIN ),
        'select_action' => __('Select action', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'enter_shortcode' => __('Enter shortcode', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'enter_content_body' => __('Enter content body', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'select_location' => __('Select location', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'select_image' => __('Select image', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'select_image_size' => __('Select image size', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'select_terms' => __('Select attribute terms', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'select_shipping' => __('Select shipping action', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'select_payment_method' => __('Select payment methods', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'select_shipping_method' => __('Select shipping methods', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'select_payment' => __('Select payment', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'enter_charge' => __('Enter valid shipping charge', BIS_PRODUCT_RULES_TEXT_DOMAIN),
        'restrict_prod' => __('Select restrict product', BIS_PRODUCT_RULES_TEXT_DOMAIN)
        );
        return $translation_array;
    }
    
    public static function get_option($option, $net_opt = false, $option_site = true) {
        if (is_multisite() && $option_site) { // Sets in network options if multisite
            return get_site_option($option);
        } else {
            return get_option($option);
        }
    }
    
    public static function delete_option($option, $net_opt = false, $option_site = true) {
        if (is_multisite() && $option_site) {
            delete_site_option($option);
        } else {
            delete_option($option);
        }
    }
    
    public static function update_option($option, $value, $net_opt = false, $option_site = true) {
        if (is_multisite() && $option_site) {
            update_site_option($option, $value);
        } else {
            update_option($option, $value);
        }
    }
    
    public static function bis_get_platform_plugin_path() {
        $string = plugins_url();
        $str_arr = preg_split("/\//", $string);
        $ar_len = sizeof($str_arr);
        $plugin_path = $str_arr[$ar_len - 2] . "/" . $str_arr[$ar_len - 1] . "/";
        return ABSPATH . $plugin_path . ProductRulesUtil::get_option("BIS_RULES_ENGINE_PLATFORM_DIR");
    }

}