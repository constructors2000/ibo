// wait for the DOM to be loaded
jQuery(document).ready(function () {
    
    if (BISAjax.bis_cached_site === "false") {
        jQuery.ajax({
            type: "GET",
            dataType: "json",
            url: jQuery("#bis_re_site_url").val(),
            data: {
                bis_re_action: "bis_re_product_popup",
                bis_nonce: BISAjax.bis_rules_engine_nonce
            },
            success: function (response) {
                var bis_product_rules = new bisWooProductCtrl(response);
                bis_product_rules.show_product_popup();
            }
        });
    }
    
    window.bisWooProductCtrl = function (options) {
        
        this.product_rules = options;
        this.popUp = null;
        var that = this;
        this.execute_woo_product_rules = function () {
            var post_id = jQuery("#bis_re_cache_post_id").val();
            var tag_id = jQuery("#bis_re_cache_product_tag_id").val();
            var cat_id = jQuery("#bis_re_cache_cat_id").val();
            for (i = 0; i < this.product_rules.length; i++) {
                switch (this.product_rules[i].action) {
                    case "append_post_content":
                    case "append_image_bg_post":
                    case "append_image_post":
                        bisProductContentAction(this.product_rules[i], post_id, cat_id, tag_id);
                        break;
                }
            }
        };
        
        this.show_product_popup = function () {

            var showPage = true;

            var redCookie = jQuery.cookie("bis_prod_cookie");

            if (redCookie === "redirected") {
                return true;
            }

            if (options.data && options.status !== "success_with_no_data") {

                var bis_popup_once = jQuery.cookie("bis_prod_popup_occur");

                if (options.data.popup_data) {
                    this.popUp = options.data.popup_data;
                    if (redCookie == null) {
                        if (bis_popup_once !== '1') {
                            this.bis_show_product_popup(options);
                        }
                    }
                }
            }

            return showPage;
        };
        
        this.bis_show_product_popup = function(response){

            var templateContent = response.data.popup_data.popupTemplate;
            jQuery("body").append('<div id="bis_re_modal_div" style="text-align:center"></div>');
            jQuery("body").append(templateContent);

            var redirectPopUp = tmpl("bis_dailog_popup_template", response['data']);

            var popUp = response['data']['popup_data'];

            jQuery("#bis_re_modal_div").html(redirectPopUp);

            if (popUp.popUpBackgroundColor && popUp.popUpBackgroundColor != '') {
                jQuery("#bis_re_dailog_modal").attr("Style", "background-color:" + popUp.popUpBackgroundColor);
            }

            if (popUp.headingOneClass && popUp.headingOneClass != '') {
                jQuery(".redirect-popup-heading").addClass(popUp.headingOneClass);
                jQuery(".redirect-popup-header").removeClass("redirect-popup-heading");
            }

            if (popUp.headingTwoClass && popUp.headingTwoClass != '') {
                jQuery(".redirect-popup-subheading").addClass(popUp.headingTwoClass);
                jQuery(".redirect-popup-subheading").removeClass("redirect-popup-subheading");
            }

            if (popUp.buttonOneClass && popUp.buttonOneClass != '') {
                jQuery(".remodal-confirm").addClass(popUp.buttonOneClass);
                // Uncommment below link to remove completely existing class
                //jQuery(".remodal-confirm").removeClass("remodal-confirm");
            }

            if (popUp.buttonTwoClass && popUp.buttonTwoClass != '') {
                jQuery(".remodal-cancel").addClass(popUp.buttonTwoClass);
                // Uncommment below link to remove completely existing class
                //jQuery(".remodal-cancel").removeClass("remodal-cancel");
            }

            if (popUp.titleClass && popUp.titleClass != '') {
                jQuery(".redirect-popup-title").addClass(popUp.titleClass);
                jQuery(".redirect-popup-title").removeClass("redirect-popup-title");
            }

            var options = {
                closeOnOutsideClick: false,
                closeOnEscape: false,
                hashTracking: false
            };

            var bis_remodal = jQuery('#bis_re_dailog_modal').remodal(options);
            bis_remodal.open();

            var secInterval;

            // Set cookie
            jQuery.cookie("bis_prod_popup_occur", 0);

            if (popUp.popUpOccurr === '1') {
                jQuery.cookie("bis_prod_popup_occur", 1);
            }

            if (popUp.autoCloseTime !== 0) {
                var counter = Number(popUp.autoCloseTime) / 1000;
                jQuery("#bis_stimer").text(counter);

                secInterval = setInterval(function () {
                    counter = counter - 1;
                    if (counter <= 0) {
                        clearInterval(secInterval);
                        bis_remodal.close();
                    }
                }, 1000);
            }

            jQuery("#bis_lable2_redirect").click(function () {

                if (jQuery("#bis_red_remem").is(':checked')) {
                    bis_setCookie("bis_prod_cookie", "cancel", 7);
                }
                clearInterval(secInterval);
                if (that.popUp.buttonTwoUrl !== "") {
                    if (jQuery("#bis_red_remem").is(':checked')) {
                        bis_setCookie("bis_prod_cookie", "redirect", 7);
                    }

                    if (that.popUp.redirectWindow && that.popUp.redirectWindow == 1) {
                        window.open(that.popUp.buttonTwoUrl, '_blank');
                    } else {
                        window.location.href = that.popUp.buttonTwoUrl;
                    }
                }
            });

            jQuery("#bis_lable1_redirect").click(function () {
                if (jQuery("#bis_red_remem").is(':checked')) {
                    bis_setCookie("bis_prod_cookie", "redirect", 7);
                }

                if (that.popUp.buttonOneUrl !== "") {
                    if (that.popUp.redirectWindow && that.popUp.redirectWindow == 1) {
                        window.open(that.popUp.buttonOneUrl, '_blank');
                    } else {
                        window.location.href = that.popUp.buttonOneUrl;
                    }
                }
            });
            
            jQuery("#bis_re_close_btn").click(function () {
                if (jQuery("#bis_red_remem").is(':checked')) {
                    bis_setCookie("bis_prod_cookie", "cancel", 7);
                }
            });
        };
    };
    
    function bisProductContentAction(product_rule, post_id, cat_id, tag_id) {
        var position = jQuery.parseJSON(product_rule.gencol2);
        switch (position['content_position']) {
            case "pos_dialog_post":
                if (product_rule.parent_type_value == "product" && post_id == product_rule.parent_id) {
                    show_product_popup_in_cache_sites(product_rule.popup_data);
                } else if(product_rule.parent_type_value == "categories" && cat_id == product_rule.parent_id) {
                    show_product_popup_in_cache_sites(product_rule.popup_data);
                } else if (product_rule.parent_type_value == "woo-tags" && tag_id == product_rule.parent_id) {
                    show_product_popup_in_cache_sites(product_rule.popup_data);
                }
                break;  
        }
    }
    
    function show_product_popup_in_cache_sites(options) {

        var showPage = true;

        var redCookie = jQuery.cookie("bis_prod_cookie");

        if (redCookie === "redirected") {
            return true;
        }

        if (options.data && options.status !== "success_with_no_data") {

            var bis_popup_once = jQuery.cookie("bis_prod_popup_occur");

            if (options.data.popup_data) {
                this.popUp = options.data.popup_data;
                if (redCookie == null) {
                    if (bis_popup_once !== '1') {
                        bis_product_popup_show(options);
                    }
                }
            }

        }

        return showPage;

    }

    function bis_product_popup_show(response) {

        var templateContent = response.data.popup_data.popupTemplate;
        jQuery("body").append('<div id="bis_re_modal_div" style="text-align:center"></div>');
        jQuery("body").append(templateContent);

        var redirectPopUp = tmpl("bis_dailog_popup_template", response['data']);

        var popUp = response['data']['popup_data'];

        jQuery("#bis_re_modal_div").html(redirectPopUp);

        if (popUp.popUpBackgroundColor && popUp.popUpBackgroundColor != '') {
            jQuery("#bis_re_dailog_modal").attr("Style", "background-color:" + popUp.popUpBackgroundColor);
        }

        if (popUp.headingOneClass && popUp.headingOneClass != '') {
            jQuery(".redirect-popup-heading").addClass(popUp.headingOneClass);
            jQuery(".redirect-popup-header").removeClass("redirect-popup-heading");
        }

        if (popUp.headingTwoClass && popUp.headingTwoClass != '') {
            jQuery(".redirect-popup-subheading").addClass(popUp.headingTwoClass);
            jQuery(".redirect-popup-subheading").removeClass("redirect-popup-subheading");
        }

        if (popUp.buttonOneClass && popUp.buttonOneClass != '') {
            jQuery(".remodal-confirm").addClass(popUp.buttonOneClass);
        }

        if (popUp.buttonTwoClass && popUp.buttonTwoClass != '') {
            jQuery(".remodal-cancel").addClass(popUp.buttonTwoClass);
        }

        if (popUp.titleClass && popUp.titleClass != '') {
            jQuery(".redirect-popup-title").addClass(popUp.titleClass);
            jQuery(".redirect-popup-title").removeClass("redirect-popup-title");
        }

        var options = {
            closeOnOutsideClick: false,
            closeOnEscape: false,
            hashTracking: false
        };

        var bis_remodal = jQuery('#bis_re_dailog_modal').remodal(options);
        bis_remodal.open();

        var secInterval;

        // Set cookie
        jQuery.cookie("bis_prod_popup_occur", 0);

        if (popUp.popUpOccurr === '1') {
            jQuery.cookie("bis_prod_popup_occur", 1);
        }

        if (popUp.autoCloseTime !== 0) {
            var counter = Number(popUp.autoCloseTime) / 1000;
            jQuery("#bis_stimer").text(counter);

            secInterval = setInterval(function () {
                counter = counter - 1;
                if (counter <= 0) {
                    clearInterval(secInterval);
                    bis_remodal.close();
                } 
            }, 1000);
        }

        jQuery("#bis_lable2_redirect").click(function () {
            if (jQuery("#bis_red_remem").is(':checked')) {
                bis_setCookie("bis_prod_cookie", "cancel", 7);
            }
            clearInterval(secInterval);
            if (response.data.popup_data.buttonTwoUrl !== "") {
                if (jQuery("#bis_red_remem").is(':checked')) {
                    bis_setCookie("bis_prod_cookie", "redirect", 7);
                }

                if (response.data.popup_data.redirectWindow && response.data.popup_data.redirectWindow == 1) {
                    window.open(response.data.popup_data.buttonTwoUrl, '_blank');
                } else {
                    window.location.href = response.data.popup_data.buttonTwoUrl;
                }
            }
        });
        
        jQuery("#bis_lable1_redirect").click(function () {
            confirm_redirect_button = "clicked";
            if (jQuery("#bis_red_remem").is(':checked')) {
                bis_setCookie("bis_prod_cookie", "redirect", 7);
            }

            if (response.data.popup_data.buttonOneUrl !== "") {
                if (response.data.popup_data.redirectWindow && response.data.popup_data.redirectWindow == 1) {
                    window.open(response.data.popup_data.buttonOneUrl, '_blank');
                } else {
                    window.location.href = response.data.popup_data.buttonOneUrl;
                }
            }
        });
        
        jQuery("#bis_re_close_btn").click(function () {
            if (jQuery("#bis_red_remem").is(':checked')) {
                bis_setCookie("bis_prod_cookie", "cancel", 7);
            }
        });
    }
});