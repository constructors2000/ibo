function bis_showProductResponse(responseText, statusText, xhr, $form) {
    bis_showProductRulesView(responseText);
}

function bis_productRuleAction() {
    var actionIds = [];
    var i = 0;
    var rule = "";
    var actionNames = " ";
    var selectedAction = jQuery("#bis_select_action option:selected").val();
    jQuery('input[name="delete_rule"]:checked').each(function () {

        actionIds[i++] = this.value;
        if (actionIds.length > 1) {
            actionNames = jQuery("#delete_rule_" + this.value).text() + ", " + actionNames;
            rule = "rules";
        } else {
            actionNames = jQuery("#delete_rule_" + this.value).text() + actionNames;
            rule = "rule";
        }
    });

    bis_confirm("<h4>Do want to " + selectedAction + " the " + rule + " \"" + actionNames + "\" ?</h4>", function (result) {
        if (result) {
            var jqXHR = jQuery.get(ajaxurl,
                    {
                        action: "bis_re_product_rule_action",
                        ruleId: actionIds,
                        selectedAction: selectedAction,
                        bis_nonce: BISAjax.bis_rules_engine_nonce,
                        beforeSend: bis_before_request_send
                    });

            jqXHR.done(function (response) {
                bis_showProductRulesView(response);
                bis_setProductSearchButtonWidth();
                bis_productFieldsReset();
                bis_getProductPageCount(0);
                jQuery(".pagination").bootpag({page: 1});
                jQuery("#bis_check_all").attr('checked', false);
                jQuery("#bis_select_action").multiselect('deselect', selectedAction);
                jQuery('#bis_select_action').multiselect("disable");
                bis_request_complete();
            });
        }

    });
}

function bis_showEditProductRule() {

    var ruleId = jQuery(this).closest("div").children()[0].value;

    var jqXHR = jQuery.get(ajaxurl,
            {
                beforeSend: bis_before_request_send,
                action: "bis_re_show_edit_product_rule",
                ruleId: ruleId,
                bis_nonce: BISAjax.bis_rules_engine_nonce
            });

    jqXHR.done(function (data) {
        jQuery("#product_rules_child_content").html(data);
        jQuery("#product_rules_list_content").hide();
        jQuery("#product_rules_child_content").show();
        jQuery("#bis_re_products").show();
        jQuery(".pagination").bootpag({page: 1});
        jQuery("#bis_check_all").attr('checked', false);
        jQuery('#bis_select_action').multiselect("disable");
        bis_setProductSearchButtonWidth();
        bis_productFieldsReset();
        bis_request_complete();

    });

}

function bis_showProductRulesList(pageIndex) {

    jQuery.get(
            BISAjax.ajaxurl,
            {
                action: 'bis_re_product_rules_list',
                pg_st_index: pageIndex,
                beforeSend: bis_before_request_send,
                bis_nonce: BISAjax.bis_rules_engine_nonce
            },
            function (response) {
                bis_showProductRulesView(response);
                bis_request_complete();
            });
}

function bis_showProductRulesView(response) {

    jQuery("#product_rules_list_content").show();
    jQuery("#product_rules_child_content").html("");
    jQuery("#product_rules_parent_content").html("");
    var status = response["status"];

    if (status === "success_with_no_data") {

        jQuery("#bis-product-rules-table").hide();
        jQuery("#bis-product-no-result").children().html('Product rules not found');
        jQuery("#bis-product-no-result").show();
        jQuery('.pagination').hide();
        jQuery('#bis_select_action').next().hide();
        jQuery('.bis-select-action').hide();
        jQuery(".bis-rows-no").html(0);

    } else {
        var data = {
            productrules: response["data"]
        };

        if (status === "error") {
            bis_showErrorMessage("Error occurred while retrieving product rules.");
        }

        jQuery(".bis-rows-no").html(response["total_records"]);
        jQuery('#bis_select_action').next().show();
        jQuery('.bis-select-action').show();
        jQuery('.pagination').show();
        jQuery("#bis-product-rules-table").show();
        jQuery("#bis-product-no-result").hide();
        var source = jQuery("#bis-productRulesListTemplate").html();
        var template = Handlebars.compile(source);
        var productRulesListContent = template(data);
        jQuery("#bis-productRulesListContent").html(productRulesListContent);
        jQuery(".bis-product-rule-edit").on("click", bis_showEditProductRule);
        jQuery(".bis-checkbox").on("change", bis_uncheckCheckbox);
        jQuery(".bis-checkbox").on("change", bis_actionButton);
        jQuery('#bis_select_action').multiselect("disable");
        jQuery(".glyphicon-thumbs-down").parent().parent().addClass("warning");
    }

    if (window.location.search.match('rule_action=productadd')) {
        jQuery("#bis_re_show_add_product_rule").click();
        jQuery("#product_rules_list_content").hide();
    }
    // stop loading icon
    bis_request_complete();
}


function bis_productFieldsReset() {

    jQuery("#bis_re_product_search_value").val("");
    jQuery("#bis_select_action").val("");
    jQuery("#bis_re_product_search_status").multiselect({});
    jQuery('#bis_re_product_search_status').multiselect('select', 'all');
    jQuery('#bis_re_product_search_status').multiselect('deselect', ['1', '0']);
    jQuery("#bis_re_product_search_by").multiselect('select', 'name');
    jQuery("#bis_re_product_search_by").multiselect('deselect', 'description');
    jQuery("#bis_check_all").attr('checked', false);
    jQuery('#bis_select_action').multiselect("disable");
}

function bis_searchResultsProductRulesList(pageIndex) {

    jQuery.post(
            BISAjax.ajaxurl,
            {
                action: 'bis_re_product_search_rule',
                pg_st_index: pageIndex,
                searchData: {search_by: jQuery("#bis_re_product_search_by").val(),
                    search_value: jQuery("#bis_re_product_search_value").val(),
                    search_status: jQuery("#bis_re_product_search_status").val()
                },
                bis_nonce: BISAjax.bis_rules_engine_nonce
            },
            function (response) {
                bis_showProductRulesView(response);
                bis_request_complete();
                return false;
            }
    );
}

function bis_getProductPageCount(pageIndex) {

    jQuery.get(
            BISAjax.ajaxurl,
            {
                action: 'bis_re_product_rules_list',
                pg_st_index: pageIndex,
                bis_nonce: BISAjax.bis_rules_engine_nonce
            },
            function (response) {
                var row_count = response["total_records"];
                var total_pages = Math.ceil(row_count / 10);
                bis_productBootpages(total_pages);
                bis_showProductRulesView(response);
                bis_request_complete();
            }
    );
}

function bis_productBootpages(total_pages) {
    jQuery('.pagination').bootpag({
        total: total_pages,
        maxVisible: 10,
        firstLastUse: true,
        first: '<span aria-hidden="true">First</span>',
        last: '<span aria-hidden="true">Last</span>',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
    }).on("page", function (event, num) {
        event.preventDefault();
        var search = jQuery('#searchPagination').val();
        if (search) {
            bis_searchResultsProductRulesList(num);
            jQuery("#bis_check_all").attr('checked', false);
            jQuery('#bis_select_action').multiselect("disable");
        } else {
            bis_showProductRulesList(num);
            jQuery("#bis_check_all").attr('checked', false);
            jQuery('#bis_select_action').multiselect("disable");
        }
        return false;
    });
}

function bis_setProductSearchButtonWidth() {
    jQuery('#bis_re_product_search_by').multiselect({

    });

    jQuery('#bis_re_product_search_status').multiselect({

    });
    jQuery('#bis_select_action').multiselect({

    });

}

/**
 * This method validates the Add Product Rules Form.
 *
 * @param arr
 * @param form
 * @param options
 * @returns {boolean}
 */
function bis_validateAddProductRulesForm(arr, form, options) {

    var ruleName = jQuery.trim(jQuery("#bis_re_product_rule").val());

    if (!bis_validateRulesName(ruleName)) {
        return false;
    }

    var producttype = jQuery.trim(jQuery("#bis_re_product_type").val());

    if (producttype === "") {
        bis_alert(productreCV.select_product_type);
        return false;
    }

    var productName = jQuery.trim(jQuery("#bis_re_product_list").val());
    var selectOption = jQuery("#bis_selected_type").text();
    if (productName === "") {
        switch (selectOption) {
            case "Select Tag":
                bis_alert(productreCV.select_woo_tags);
                break;
            case "Select Category":
                bis_alert(productreCV.select_woo_cats);
                break;
            case "Select Attribute":
                bis_alert(productreCV.select_woo_attrs);
                break;
            default:
                bis_alert(productreCV.select_products);
                break;
        }
        return false;
    }

    var attributTerms = jQuery.trim(jQuery("#bis_re_term_list").val());
    if (selectOption === "Select Attribute" && attributTerms === "") {
        bis_alert(productreCV.select_terms);
        return false;
    }
    var action = jQuery.trim(jQuery("#bis_re_product_action").val());

    if (action === "") {
        bis_alert(productreCV.select_action);
        return false;
    }

    if (!(action === "append_image_post" || action === "hide_post" ||
            action === "show_post" || action === "show_only_post")) {

        var shippingIs = jQuery.trim(jQuery("#bis_shipping_is").val());
        
        if (action === "shipping" && shippingIs === "") {

            bis_alert(productreCV.select_shipping);
            return false;
        }
        
        var shippingMethods = jQuery.trim(jQuery('#bis_shipping_methods').val());
        if(action === "shipping" && shippingIs === "charge" && shippingMethods === ""){
            bis_alert(productreCV.select_shipping_method);
            return false;
        }
        
        var shippingCharge = parseInt(jQuery('#bis_product_shipping_charge').val());
        if (action === "shipping" && shippingIs === "charge" && (shippingCharge == "" || shippingCharge < 0 || isNaN(shippingCharge))) {

            bis_alert(productreCV.enter_charge);
            return false;
        }

        var paymentMethods = jQuery.trim(jQuery("#bis_payment_method").val());

        if (action === "payment" && paymentMethods === "") {

            bis_alert(productreCV.select_payment_method);
            return false;
        }

        var paymentIs = jQuery.trim(jQuery("#bis_payment_is").val());

        if (action === "payment" && paymentIs === "") {

            bis_alert(productreCV.select_payment);
            return false;
        }
        
        var restrictIds = jQuery.trim(jQuery("#bis_restrict_product_list").val());

        if (action === "restrict_prod" && restrictIds === "") {

            bis_alert(productreCV.restrict_prod);
            return false;
        }

        if (!(action === "append_image_post" || action === "hide_post" || 
                action === "shipping" || action === "payment" || action === "dependent_prod" ||action === "restrict_prod"|| action === "show_post")) {

            var content = jQuery.trim(jQuery("#bis_re_product_dc_content").val());

            if (content === "") {
                if (action === "append_existing_scode_post") {
                    bis_alert(productreCV.enter_shortcode);
                } else {
                    bis_alert(productreCV.enter_content_body);
                }
                return false;
            }
        }

        var location = jQuery.trim(jQuery("#bis_re_cont_pos").val());

        if (location === "" && !(action === "replace_post_content" || action === "show_post" || action === "show_only_post"
                || action === "hide_post" || action === "payment" || action === "shipping" || action === "restrict_prod" || action === "dependent_prod" || action === "append_existing_scode_post")) {
            bis_alert(productreCV.select_location);
            return false;
        }

        // Validation for image
        if (action.indexOf("image") >= 0) {

            var image = jQuery.trim(jQuery("#bis_re_image_id").val());

            if (image === "") {
                bis_alert(productreCV.select_image);
                return false;
            }

            var image_size = jQuery("#bis_re_cont_img_size").val();

            if (image_size === null || image_size === "") {
                bis_alert(productreCV.select_image_size);
                return false;
            }

        }
    }
    
    return bis_validateAddRulesForm(arr, form, options);
}

function bis_payment_options_controll() {
    jQuery(".bis-payment-method").hide();
    jQuery(".bis-payment-is").hide();
    jQuery("#bis_payment_method").multiselect('deselect', jQuery("#bis_payment_method").val());
    jQuery("#bis_payment_is").multiselect('deselect', jQuery("#bis_payment_is").val());
}

function bis_restrict_product_controll(action) {
    jQuery(".bis-prod-restrict").hide();
    jQuery(".bis-restrict-error-div").hide();
    jQuery(".bis-restrict-error-div-vv").hide();
    jQuery("#bis_restrict_product_list").multiselect('deselect', jQuery("#bis_restrict_product_list").val());
}

function bis_shipping_options_controll() {
    jQuery('.bis-shipping-charge-div').hide();
    jQuery(".bis-shipping-is").hide();
    jQuery(".bis-shipping-des").hide();
    jQuery(".bis-shipping-error-des-div").hide();
    jQuery("#bis_shipping_is").multiselect('deselect', jQuery("#bis_shipping_is").val());
}

function bis_controll_shipping_methods(shipping){
    if (shipping === "charge") {
        jQuery('.bis-shipping-des').hide();
        jQuery(".bis-shipping-error-des-div").hide();
        jQuery(".bis_shipping_methods_div").show();
        jQuery(".bis_shipping_method_charge_div").show();
    } else if(shipping === "enable" || shipping === "disable") {
        jQuery(".bis_shipping_methods_div").show();
        jQuery(".bis_shipping_method_charge_div").hide();
        jQuery('.bis-shipping-des').show();
        jQuery(".bis-shipping-error-des-div").show();
    }
}
function bis_show_product_view_controls(action, position, call) {
    jQuery("#bis_re_product_dc_label").html("Content Body");
    jQuery(".bis-content-span").find(".popoverData").attr("data-content", "This field is required and supports html tags and inline style");

    switch (action) {

        case "hide_post":
            jQuery(".bis-content-pos").hide();
            jQuery(".bis-content-span").hide();
            jQuery(".bis-content-image").hide();
            bis_payment_options_controll();
            bis_shipping_options_controll();
            bis_restrict_product_controll();
            jQuery(".bis-common-popup-span").hide();
            jQuery("#bis_re_image_id").val("");
            jQuery("#bis_re_image_id").multiselect('select', '');
            jQuery("#bis_re_cont_img_size").val("");
            jQuery("#bis_re_cont_img_size").multiselect('select', '');
            break;

        case "append_post_content":
            if (position === "pos_dialog_post") {
                jQuery(".bis-common-popup-span").show();
            }
            jQuery(".bis-content-span").show();
            jQuery(".bis-content-pos").show();
            jQuery(".bis-content-image").hide();
            bis_payment_options_controll();
            bis_shipping_options_controll();
            bis_restrict_product_controll();
            jQuery("#bis_re_image_id").val("");
            jQuery("#bis_re_image_id").multiselect('select', '');
            jQuery("#bis_re_cont_img_size").val("");
            jQuery("#bis_re_cont_img_size").multiselect('select', '');
            jQuery("#bis_re_product_dc_label").html("Content Body");
            jQuery(".bis-content-span").find(".popoverData").attr("data-content", "This field is required and supports html tags and inline style");
            jQuery("#bis_re_product_dc_content").attr('placeholder', 'Enter dynamic content, HTML tags also supported.');
            break;

        case "replace_post_content":
            jQuery(".bis-content-pos").hide();
            jQuery(".bis-content-image").hide();
            jQuery(".bis-common-popup-span").hide();
            bis_payment_options_controll();
            bis_shipping_options_controll();
            bis_restrict_product_controll();
            jQuery(".bis-content-span").show();
            jQuery("#bis_re_image_id").val("");
            jQuery("#bis_re_image_id").multiselect('select', '');
            jQuery("#bis_re_cont_img_size").val("");
            jQuery("#bis_re_cont_img_size").multiselect('select', '');
            jQuery("#bis_re_product_dc_content").val("");
            jQuery("#bis_re_product_dc_label").html("Content Body");
            jQuery(".bis-content-span").find(".popoverData").attr("data-content", "This field is required and supports html tags and inline style");
            jQuery("#bis_re_product_dc_content").attr('placeholder', 'Enter dynamic content, HTML tags also supported.');
            break;

        case "append_image_post":
            if (position === "pos_dialog_post") {
                jQuery(".bis-common-popup-span").show();
            }
            jQuery(".bis-content-span").hide();
            bis_payment_options_controll();
            bis_shipping_options_controll();
            bis_restrict_product_controll();
            jQuery(".bis-content-pos").show();
            jQuery(".bis-content-image").show();
            jQuery("#bis_re_product_dc_content").val("");
            break;

        case "append_image_bg_post":
            if (position === "pos_dialog_post") {
                jQuery(".bis-common-popup-span").show();
            }
            jQuery(".bis-content-span").show();
            jQuery(".bis-content-pos").show();
            jQuery(".bis-content-image").show();
            bis_payment_options_controll();
            bis_shipping_options_controll();
            bis_restrict_product_controll();
            jQuery("#bis_re_product_dc_content").val("");
            jQuery("#bis_re_product_dc_label").html("Content Body");
            jQuery(".bis-content-span").find(".popoverData").attr("data-content", "This field is required and supports html tags and inline style");
            jQuery("#bis_re_product_dc_content").attr('placeholder', 'Enter dynamic content, HTML tags also supported.');
            break;

        case "append_existing_scode_post":
            jQuery(".bis-content-pos").hide();
            jQuery(".bis-content-image").hide();
            jQuery(".bis-common-popup-span").hide();
            bis_payment_options_controll();
            bis_shipping_options_controll();
            bis_restrict_product_controll();
            jQuery(".bis-content-span").show();
            jQuery("#bis_re_image_id").val("");
            jQuery("#bis_re_image_id").multiselect('select', '');
            jQuery("#bis_re_cont_img_size").val("");
            jQuery("#bis_re_cont_img_size").multiselect('select', '');
            jQuery("#bis_re_product_dc_content").val("");
            jQuery("#bis_re_product_dc_label").html("Shortcode");
            jQuery(".bis-content-span").find(".popoverData").attr("data-content", "This field is required, Use an existing Third-party shortcode");
            jQuery("#bis_re_product_dc_content").attr('placeholder', 'Enter a shortcode. (ex: [bis_logical_rules_status] )');
            break;

        case "show_post":
            jQuery(".bis-content-pos").hide();
            jQuery(".bis-content-span").hide();
            jQuery(".bis-common-popup-span").hide();
            jQuery(".bis-content-image").hide();
            bis_payment_options_controll();
            bis_shipping_options_controll();
            bis_restrict_product_controll();
            jQuery("#bis_re_image_id").val("");
            jQuery("#bis_re_image_id").multiselect('select', '');
            jQuery("#bis_re_cont_img_size").val("");
            jQuery("#bis_re_cont_img_size").multiselect('select', '');
            break;

        case "shipping":
            jQuery(".bis-content-pos").hide();
            jQuery(".bis-content-span").hide();
            jQuery(".bis-content-image").hide();
            jQuery(".bis-common-popup-span").hide();
            jQuery(".bis-shipping-is").show();
            jQuery(".bis-shipping-error-des-div").show();
            jQuery('.bis-shipping-charge-div').show();
//            if (jQuery(".bis-shipping-is").val() === "charge") {
//                jQuery('.bis-shipping-charge-div').show();
//                jQuery(".bis-shipping-error-des-div").hide();
//            }
            bis_controll_shipping_methods(jQuery(".bis-shipping-is").val());
            //jQuery(".bis-shipping-des").hide();
            jQuery("#bis_re_image_id").val("");
            bis_payment_options_controll();
            bis_restrict_product_controll();
            jQuery("#bis_re_image_id").multiselect('select', '');
            jQuery("#bis_re_cont_img_size").val("");
            jQuery("#bis_re_cont_img_size").multiselect('select', '');
            break;
        case "payment":
            jQuery(".bis-content-pos").hide();
            jQuery(".bis-content-span").hide();
            jQuery(".bis-content-image").hide();
            jQuery(".bis-common-popup-span").hide();
            jQuery(".bis-payment-method").show();
            jQuery(".bis-payment-is").show();
            bis_shipping_options_controll();
            bis_restrict_product_controll();
            jQuery("#bis_re_image_id").val("");
            jQuery("#bis_re_image_id").multiselect('select', '');
            jQuery("#bis_re_cont_img_size").val("");
            jQuery("#bis_re_cont_img_size").multiselect('select', '');
            break;
        case "restrict_prod":
        case "dependent_prod":
            jQuery(".bis-content-pos").hide();
            jQuery(".bis-content-span").hide();
            jQuery(".bis-content-image").hide();
            jQuery(".bis-common-popup-span").hide();
            jQuery(".bis-prod-restrict").show();
            jQuery(".bis-restrict-error-div").show();
            jQuery(".bis-restrict-error-div-vv").show();
            bis_shipping_options_controll();
            bis_payment_options_controll();
            restrict_dependent_message(action, call);
            if(action === "dependent_prod"){
                jQuery('#bis_depen_rest').text("Dependent products");
                jQuery('#bis_restrict_error_des_label').text("Dependent message");
                jQuery('#bis_restrict_error_des_label_vv').text("Remove Depentdant message");
                jQuery('#bis_restrict_error').attr("placeholder", "Enter dependent message");
                jQuery('#bis_restrict_error_vv').attr("placeholder", "Enter remove dependent product message");
            } else {
                jQuery('#bis_depen_rest').text("Restrict products");
                jQuery('#bis_restrict_error_des_label').text("Restrict message");
                jQuery('#bis_restrict_error_des_label_vv').text("Restrict vice versa message");
                jQuery('#bis_restrict_error').attr("placeholder","Enter restrict message");
                jQuery('#bis_restrict_error_vv').attr("placeholder","Enter restrict vice versa message");
            }
            jQuery("#bis_re_image_id").val("");
            jQuery("#bis_re_image_id").multiselect('select', '');
            jQuery("#bis_re_cont_img_size").val("");
            jQuery("#bis_re_cont_img_size").multiselect('select', '');
            break;
        case "show_only_post":
            jQuery(".bis-content-pos").hide();
            jQuery(".bis-content-span").hide();
            jQuery(".bis-content-image").hide();
            bis_payment_options_controll();
            bis_shipping_options_controll();
            bis_restrict_product_controll();
            jQuery(".bis-common-popup-span").hide();
            jQuery("#bis_re_image_id").val("");
            jQuery("#bis_re_image_id").multiselect('select', '');
            jQuery("#bis_re_cont_img_size").val("");
            jQuery("#bis_re_cont_img_size").multiselect('select', '');
            break;
    }
    
    
    jQuery("#bis_re_product_type").change(function(){
        restrict_dependent_message(jQuery("#bis_re_product_action").val(), "");
    });
    
    jQuery("#bis_re_product_list").live('change', function(){
        restrict_dependent_message(jQuery("#bis_re_product_action").val(), "");
    });
    
    jQuery("#bis_restrict_product_list").change(function(){
        restrict_dependent_message(jQuery("#bis_re_product_action").val(), "");
    });
    
    function restrict_dependent_message(action, called) {
        debugger;
        var listProdIds = jQuery("#bis_re_product_list option:selected").map(function () {
                                return jQuery(this).text();
                            }).get().join(',');
        var restProdIds = jQuery("#bis_restrict_product_list").val();
        var restProdIdsn = jQuery("#bis_restrict_product_list option:selected").map(function () {
                                return jQuery(this).text();
                            }).get().join(',');
        
        if(listProdIds !== "" && restProdIdsn !== "" && called !== "func-from-onload-edit" ){
            debugger;
            if(action === "restrict_prod" && restProdIds !== null){
                jQuery("#bis_restrict_error_vv").val("");
                jQuery("#bis_restrict_error").val("");
                jQuery("#bis_restrict_error_vv").val("You cannot add Product" + listProdIds + "to the cart because it is incompatible with Product" + restProdIdsn + " which is already in your cart.");
                jQuery("#bis_restrict_error").val("You cannot add Product" + restProdIdsn + "to the cart because it is incompatible with Product" + listProdIds + " which is already in your cart.");
                jQuery("#bis_restrict_error_vv").text("You cannot add Product" + listProdIds + "to the cart because it is incompatible with Product" + restProdIdsn + " which is already in your cart.");
                jQuery("#bis_restrict_error").text("You cannot add Product" + restProdIdsn + "to the cart because it is incompatible with Product" + listProdIds + " which is already in your cart.");
            } else if(action === "dependent_prod" && restProdIds !== null) {
                jQuery("#bis_restrict_error_vv").val("");
                jQuery("#bis_restrict_error").val("");
                jQuery("#bis_restrict_error_vv").val("Removed dependency products(" + listProdIds + ").");
                jQuery("#bis_restrict_error").val("Product" + listProdIds + "requires Product" + restProdIdsn + "in order to function properly. Product" + restProdIdsn + "is being automatically added to your cart.");
                jQuery("#bis_restrict_error").text("Product" + listProdIds + "requires Product" + restProdIdsn + "in order to function properly. Product" + restProdIdsn + "is being automatically added to your cart.");  
                jQuery("#bis_restrict_error_vv").text("Removed dependency products(" + listProdIds + ").");
            }
        } else {
            jQuery("#bis_restrict_error").text("");  
        }
    }
}