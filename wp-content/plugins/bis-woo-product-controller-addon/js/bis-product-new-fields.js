function bis_ship_gen_filed_edit(critVal,textVal, funcCall,field,drop_down_val = null) {

    if (critVal !== "none") {
        jQuery("#bis_"+field+"_gen_criteria_value_lable").show();
        var valueTypeId = 1;
        if (critVal === "bis_logical_rule") {
            valueTypeId = 2;
        } else {
            var opt = critVal.split("_")[0];
            var subCrVal = critVal.split("_")[1];

            var subCriteria = Number(subCrVal);

            if (subCriteria === 9 || subCriteria === 14 || subCriteria === 3 || subCriteria === 10) {
                valueTypeId = 3;
            } else if (subCriteria === 32 || subCriteria === 6 || subCriteria === 15 || subCriteria === 35 || subCriteria === 27 || subCriteria === 26 || subCriteria === 31) {
                valueTypeId = 4;
            } else if (subCriteria === 19 || subCriteria === 16 || subCriteria === 11 || subCriteria === 12
                    || subCriteria === 13 || subCriteria === 33 || subCriteria === 7 || subCriteria === 28) {
                valueTypeId = 2;
            }
        }

        switch (valueTypeId) {

            case 1: // Token Input
                jQuery("#bis_"+field+"_gen_criteria_value_text").val("");
                jQuery("#bis_"+field+"_gen_criteria_value_dropdown").hide();
                jQuery("#bis_"+field+"_gen_criteria_value_text").show();
                bis_prod_addTokenInput(subCriteria, opt, textVal, funcCall,field);
                break;
            case 2:  // Select Option
                jQuery("#bis_"+field+"_gen_criteria_value_dropdown").show();
                jQuery("#bis_"+field+"_gen_criteria_value_text").hide();
                bis_prod_addSelectBox(subCriteria, 0, critVal,field,drop_down_val);
                break;
            case 3: // Calendar Date
                if (jQuery("#bis_"+field+"_gen_criteria_value_text").prev()[0].nodeName !== "LABEL") {
                    jQuery("#bis_"+field+"_gen_criteria_value_text").prev().remove();
                }
                if (funcCall === "fromClick") {
                    jQuery("#bis_"+field+"_gen_criteria_value_text").val("");
                } else {
                    jQuery("#bis_"+field+"_gen_criteria_value_text").val(textVal);
                }
                jQuery("#bis_"+field+"_gen_criteria_value_text").show();
                jQuery("#bis_"+field+"_gen_criteria_value_dropdown").hide();
                var tpicker = false;
                var dformat = 'Y-m-d';
                var dpicker = true;

                if (subCriteria === 14) { // Date & Time
                    tpicker = true;
                    dformat = 'Y-m-d H:i';
                } else if (subCriteria === 10) {  // Only Time
                    dpicker = false;
                    dformat = 'H:i';
                    tpicker = true;
                }
                jQuery("#bis_"+field+"_gen_criteria_value_text").datetimepicker({
                    timepicker: tpicker,
                    format: dformat,
                    datepicker: dpicker,
                    closeOnDateSelect: true,
                    mask: true
                });

                break;
            case 4:
                if (jQuery("#bis_"+field+"_gen_criteria_value_text").prev()[0].nodeName !== "LABEL") {
                    jQuery("#bis_"+field+"_gen_criteria_value_text").prev().remove();
                }
                jQuery("#bis_"+field+"_gen_criteria_value_dropdown").hide();
                jQuery("#bis_"+field+"_gen_criteria_value_text").show();
                if (funcCall === "fromClick") {
                    jQuery("#bis_"+field+"_gen_criteria_value_text").val("");
                } else {
                    jQuery("#bis_"+field+"_gen_criteria_value_text").val(textVal);
                }
        }
    } else {
        if (jQuery("#bis_"+field+"_gen_criteria_value_text").prev()[0].nodeName !== "LABEL") {
            jQuery("#bis_"+field+"_gen_criteria_value_text").prev().remove();
        }
        jQuery("#bis_"+field+"_gen_criteria_value_lable").hide();
        jQuery("#bis_"+field+"_gen_criteria_value_text").hide();
        jQuery("#bis_"+field+"_gen_criteria_value_dropdown").hide();
    }
}

/**
 Add the tokenInput component with values.
 */
function bis_prod_addTokenInput(subCriteria, opt,textVal, funcCall, field) {

    if (jQuery("#bis_"+field+"_gen_criteria_value_text").prev()[0].nodeName !== "LABEL") {
        jQuery("#bis_"+field+"_gen_criteria_value_text").prev().remove();
    }
    var bis_nonce = BISAjax.bis_rules_engine_nonce;
    var condition = 12;
    var ssubCriteria = 0;
    if (funcCall === "fromEdit" && textVal !== "") {
        var params = "?action=bis_re_get_value&bis_nonce=" + bis_nonce + "&criteria=" + opt + "&subcriteria=" + subCriteria + "&condition=" + condition;
        var $tokenInput = jQuery("#bis_"+field+"_gen_criteria_value_text").tokenInput(ajaxurl + params,
                {
                    theme: "facebook",
                    prePopulate: jQuery.parseJSON(textVal),
                    minChars: 2,
                    method: "POST",
                    tokenLimit: null,
                    onAdd: function (item) {
                        jQuery(this).val(JSON.stringify(this.tokenInput("get")));
                    },
                    onDelete: function (item) {
                        jQuery(this).val(JSON.stringify(this.tokenInput("get")));
                    }
                });
        $tokenInput.val(textVal);
    } else {
        jQuery("#bis_"+field+"_gen_criteria_value_text").tokenInput(ajaxurl + "?action=bis_re_get_value&bis_nonce=" + bis_nonce +
                "&subcriteria=" + subCriteria + "&ssubCriteria=" + ssubCriteria + "&condition=" + condition, {
                    theme: "facebook",
                    minChars: 2,
                    method: "POST",
                    tokenLimit: null,
                    onAdd: function (item) {
                        jQuery("#bis_"+field+"_gen_criteria_value_text").val(JSON.stringify(this.tokenInput("get")));
                    },
                    onDelete: function (item) {
                        jQuery(this).val(JSON.stringify(this.tokenInput("get")));
                    }
                });
    }
    jQuery('.token-input-token-facebook').addClass('tokenInputClassProd');
}

function bis_prod_addSelectBox(subCriteria, ssubCriteria, critVal,field,drop_down_val) {
    
    if (jQuery("#bis_"+field+"_gen_criteria_value_text").prev()[0].nodeName !== "LABEL") {
        jQuery("#bis_"+field+"_gen_criteria_value_text").prev().remove();
    }
    jQuery('#bis_'+field+'_gen_criteria_value_dropdown').html('');

    if (critVal === "bis_logical_rule") {
        subCriteria = critVal;
    }
    var jqXHR = jQuery.get(ajaxurl,
            {
                action: "bis_re_get_rule_values",
                bis_nonce: BISAjax.bis_rules_engine_nonce,
                bis_sub_criteria: subCriteria,
                bis_ssubCriteria: ssubCriteria

            });

    jqXHR.done(function (response) {
        if (critVal === "bis_logical_rule") {
            jQuery.each(response, function (key, objval) {
                jQuery('#bis_'+field+'_gen_criteria_value_dropdown').append(jQuery('<option>', {value: objval["value"]}).text(objval["text"]));
            });
        } else {
            jQuery.each(response, function (key, objval) {
                jQuery('#bis_'+field+'_gen_criteria_value_dropdown').append(jQuery('<option>', {value: objval["id"]}).text(objval["name"]));
            });
        }
        var value = drop_down_val;
        jQuery('#bis_'+field+'_gen_criteria_value_dropdown').find('option[value="' + value + '"]').attr("selected", "selected");
    });

}