
function bis_woo_sett_mega_tab(bis_criteria, bis_criteria_text_val, dropdownVal, funcCall,section){

    var criteriaVal = bis_criteria ;
    var textVal = bis_criteria_text_val;
    var dropdownVal = dropdownVal;
    var funcCall = funcCall;
    bis_prod_gen_filed_edit(criteriaVal, funcCall, section, textVal, dropdownVal);
}

function bis_prod_gen_filed_edit(critVal, funcCall, section, textVal = null, dropdownVal = null) {

    if (critVal !== "none") {
        //jQuery("#bis_pro_gen_criteria_value_lable").show();
        var valueTypeId = 1;
        if (critVal === "bis_logical_rule") {
            valueTypeId = 2;
        } else {
            var opt = critVal.split("_")[0];
            var subCrVal = critVal.split("_")[1];

            var subCriteria = Number(subCrVal);

            if (subCriteria === 9 || subCriteria === 14 || subCriteria === 3 || subCriteria === 10) {
                valueTypeId = 3;
            } else if (subCriteria === 32 || subCriteria === 6 || subCriteria === 15 || subCriteria === 35 || subCriteria === 27 || subCriteria === 26 || subCriteria === 31) {
                valueTypeId = 4;
            } else if (subCriteria === 8 || subCriteria === 19 || subCriteria === 16 || subCriteria === 11 || subCriteria === 12
                    || subCriteria === 13 || subCriteria === 33 || subCriteria === 7 || subCriteria === 28) {
                valueTypeId = 2;
            }
        }
        switch (valueTypeId) {

            case 1: // Token Input
                jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").val("");
                jQuery("#bis_woo_sett_"+section+"_rule_criteria_dropdown_val").hide();
                jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").show();
                bis_woo_sett_addTokenInput(subCriteria, opt, funcCall, textVal, section);
                break;
            case 2:  // Select Option
                jQuery("#bis_woo_sett_"+section+"_rule_criteria_dropdown_val").show();
                jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").hide();
                bis_woo_sett_addSelectBox(subCriteria, 0, critVal, dropdownVal, section);
                break;
            case 3: // Calendar Date
                if (jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").prev().length !== 0) {
                    jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").prev().remove();
                }
                if (funcCall === "fromClick") {
                    jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").val("");
                } else {
                    jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").val(textVal);
                }
                jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").show();
                jQuery("#bis_woo_sett_"+section+"_rule_criteria_dropdown_val").hide();
                var tpicker = false;
                var dformat = 'Y-m-d';
                var dpicker = true;

                if (subCriteria === 14) { // Date & Time
                    tpicker = true;
                    dformat = 'Y-m-d H:i';
                } else if (subCriteria === 10) {  // Only Time
                    dpicker = false;
                    dformat = 'H:i';
                    tpicker = true;
                }
                jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").datetimepicker({
                    timepicker: tpicker,
                    format: dformat,
                    datepicker: dpicker,
                    closeOnDateSelect: true,
                    mask: true
                });

                break;
            case 4:
                jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").datetimepicker({
                    timepicker: false,
                    format: '',
                    datepicker: false,
                    closeOnDateSelect: true,
                    mask: false
                });
                if (jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").prev().length !== 0) {
                    jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").prev().remove();
                }
                jQuery("#bis_woo_sett_"+section+"_rule_criteria_dropdown_val").hide();
                jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").show();
                if (funcCall === "fromClick") {
                    jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").val("");
                } else {
                    jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").val(textVal);
                }
        }
    } else {
        if (jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").prev().length !== 0) {
            jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").prev().remove();
        }
        jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").val("");
        jQuery('#bis_woo_sett_'+section+'_rule_criteria_dropdown_val').html('');
        jQuery("#bis_woo_sett_"+section+"_rule_criteria_dropdown_val").hide();
    }
}

/**
 Add the tokenInput component with values.
 */
function bis_woo_sett_addTokenInput(subCriteria, opt, funcCall, textVal, section) {
    
    if (jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").prev().length !== 0) {
        jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").prev().remove();
    }
    var bis_nonce = BISAjax.bis_rules_engine_nonce;
    var condition = 12;
    var ssubCriteria = 0;
    if (funcCall === "fromEdit") {
        var params = "?action=bis_re_get_value&bis_nonce=" + bis_nonce + "&criteria=" + opt + "&subcriteria=" + subCriteria + "&condition=" + condition;
        var $tokenInput = jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").tokenInput(ajaxurl + params,
                {
                    theme: "facebook",
                    prePopulate: jQuery.parseJSON(textVal),
                    minChars: 2,
                    method: "POST",
                    tokenLimit: null,
                    onAdd: function (item) {
                        jQuery(this).val(JSON.stringify(this.tokenInput("get")));
                    },
                    onDelete: function (item) {
                        jQuery(this).val(JSON.stringify(this.tokenInput("get")));
                    }
                });
        $tokenInput.val(textVal);
    } else {
        jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").tokenInput(ajaxurl + "?action=bis_re_get_value&bis_nonce=" + bis_nonce +
                "&subcriteria=" + subCriteria + "&ssubCriteria=" + ssubCriteria + "&condition=" + condition, {
                    theme: "facebook",
                    minChars: 2,
                    method: "POST",
                    tokenLimit: null,
                    onAdd: function (item) {
                        jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").val(JSON.stringify(this.tokenInput("get")));
                    },
                    onDelete: function (item) {
                        jQuery(this).val(JSON.stringify(this.tokenInput("get")));
                    }
                });
    }

}

function bis_woo_sett_addSelectBox(subCriteria, ssubCriteria, critVal, dropdownVal, section) {

    if (jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").prev().length !== 0) {
        jQuery("#bis_woo_sett_"+section+"_rule_criteria_txt_val").prev().remove();
    }
    jQuery('#bis_woo_sett_'+section+'_rule_criteria_dropdown_val').html('');

    if (critVal === "bis_logical_rule") {
        subCriteria = critVal;
    }
    var jqXHR = jQuery.get(ajaxurl,
            {
                action: "bis_re_get_rule_values",
                bis_nonce: BISAjax.bis_rules_engine_nonce,
                bis_sub_criteria: subCriteria,
                bis_ssubCriteria: ssubCriteria

            });

    jqXHR.done(function (response) {
        if (critVal === "bis_logical_rule") {
            jQuery.each(response, function (key, objval) {
                jQuery('#bis_woo_sett_'+section+'_rule_criteria_dropdown_val').append(jQuery('<option>', {value: objval["value"]}).text(objval["text"]));
            });
        } else {
            jQuery.each(response, function (key, objval) {
                jQuery('#bis_woo_sett_'+section+'_rule_criteria_dropdown_val').append(jQuery('<option>', {value: objval["id"]}).text(objval["name"]));
            });
        }
        var value = dropdownVal;
        jQuery('#bis_woo_sett_'+section+'_rule_criteria_dropdown_val').find('option[value="' + value + '"]').attr("selected", "selected");
    });

}
          


