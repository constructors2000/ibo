<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

use bis\repf\common\RulesEngineCacheWrapper;
use bis\repf\vo\RulesVO;
use bis\repf\vo\SearchVO;
use bis\repf\vo\PopUpVO;

add_action('wp_ajax_bis_re_product_rules_list', 'bis_re_product_rules_list');
add_action('wp_ajax_bis_re_product_search_rule', 'bis_re_product_search_rule');
add_action('wp_ajax_bis_re_product_rule_action', 'bis_re_product_rule_action');
add_action('wp_ajax_bis_re_show_add_product_rule', 'bis_re_show_add_product_rule');
add_action('wp_ajax_bis_re_update_next_product_rule', 'bis_re_update_next_product_rule');
add_action('wp_ajax_bis_re_show_edit_product_rule', 'bis_re_show_edit_product_rule');
add_action('wp_ajax_bis_re_create_product_rule_wizard', 'bis_re_create_product_rule_wizard');
add_action('wp_ajax_bis_re_get_product_list', 'bis_re_get_product_list');
add_action('wp_ajax_bis_apply_product_rule', 'bis_apply_product_rule');
add_action('wp_ajax_bis_re_get_term_list_product', 'bis_re_get_term_list_product');

function bis_re_create_product_rule_wizard() {

    $nonce = $_POST ['bis_nonce'];

    // generated nonce we created earlier
    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }

    $rules_vo = RulesEngineCacheWrapper::get_session_attribute(BIS_SESSION_RULEVO);

    $re_rules_engine_modal = new ProductRulesEngineModel();

    $results_map = $re_rules_engine_modal->save_post_rule($rules_vo);
    $status = $results_map[BIS_STATUS];

    if ($status == BIS_SUCCESS) {
        RulesEngineCacheWrapper::set_reset_time(BIS_PRODUCT_RULE_RESET);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_SESSION_RULEVO);
    }

    RulesEngineUtil::generate_json_response($results_map);
}

/**
 * This method used to get product rules
 */
function bis_re_product_rules_list() {

    $nonce = $_GET ['bis_nonce'];

    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }
    $page_start_index = RulesEngineUtil::get_start_page_index();
    // Every plugin should remove the session value
    RulesEngineCacheWrapper::remove_session_attribute(BIS_LOGICAL_RULE_ID);

    $re_rules_engine_modal = new ProductRulesEngineModel();

    $results_map = $re_rules_engine_modal->get_product_rules($page_start_index);

    RulesEngineUtil::generate_json_response($results_map);
}

/**
 * This method used for search the product rules 
 */
function bis_re_product_search_rule() {

    RulesEngineUtil::remove_search_request();
    $nonce = $_POST ['bis_nonce'];

    // generated nonce we created earlier
    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }

    if (isset($_POST["searchData"])) {
        $bis_re_search_by = $_POST["searchData"]["search_by"];
        $bis_re_search_value = $_POST["searchData"]["search_value"];
        $bis_re_status = $_POST["searchData"]["search_status"];
    } else {
        $bis_re_search_by = $_POST ["bis_re_product_search_by"];
        $bis_re_search_value = $_POST ["bis_re_product_search_value"];
        $bis_re_status = $_POST ["bis_re_product_search_status"];
    }

    $product_rules_engine_modal = new ProductRulesEngineModel();

    $search_vo = new SearchVO();
    $search_vo->set_search_by($bis_re_search_by);
    $search_vo->set_search_value($bis_re_search_value);
    $search_vo->set_status($bis_re_status);

    $page_start_index = RulesEngineUtil::get_start_page_index();
    $results_map = $product_rules_engine_modal->search_child_rules(BIS_WOO_PRODUCT_TYPE_RULE, $search_vo, $page_start_index);

    RulesEngineUtil::set_search_request();
    RulesEngineUtil::generate_json_response($results_map, BIS_SEARCH_REQUEST);
}

/**
 * This method used to get the product-rules-add.php page
 */
function bis_re_show_add_product_rule() {

    $nonce = $_POST ['bis_nonce'];

    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }

    include ProductRulesUtil::getIncludesDirPath() . 'product-rules-add.php';

    flush();
    exit();
}

/**
 *
 * Method used to add product rules
 *
 */
function bis_re_update_next_product_rule() {

    $nonce = $_POST ['bis_nonce'];

    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }

    $product_rules_model = new ProductRulesEngineModel();
    $rules_vo = new RulesVO();

    $rule_name = $_POST['bis_re_product_rule'];

    $rdetail_id = 0;

    if (isset($_POST['bis_re_edit_detail_id'])) {
        $rdetail_id = $_POST['bis_re_edit_detail_id'];
        $rules_vo->set_id($rdetail_id);
    }
    // Check if product rule exists
    $results_map = $product_rules_model->is_product_rule_exists($rule_name, $rdetail_id);
    $status = $results_map[BIS_STATUS];

    if ($status === BIS_ERROR) {
        RulesEngineUtil::generate_json_response($results_map);
    }

    
    $action = $_POST['bis_re_product_action'];
    $rules_vo->set_name($rule_name);
    $rules_vo->set_description($_POST['bis_re_product_description']);
    $rules_vo->set_action($action);
    $rules_vo->set_status($_POST['bis_re_rule_status']);
    $bis_re_product_type = $_POST['bis_re_product_type'];
    $rules_vo->set_parent_type_value($bis_re_product_type);
    $rules_vo->set_rule_type_id(BIS_WOO_PRODUCT_TYPE_RULE);
    $content_position = null;
    
    
    if($action == "shipping" && isset($_POST['bis_shipping_is'])){
        $rules_vo->set_general_col2($_POST['bis_shipping_is']);
        if ($_POST['bis_shipping_is'] === "charge") {
            $shipping_charge_array = array();
            $shipping_charge_array['shipping_charge'] = $_POST['bis_product_shipping_charge'];
            $shipping_charge_array['shipping_methods'] = $_POST['bis_shipping_methods'];
            $rules_vo->set_general_col3(json_encode($shipping_charge_array));
        } else {
            $shipping_array = array();
            $shipping_array['bis_shipping_des'] = strip_tags($_POST['bis_shipping_des']);
            $shipping_array['shipping_methods'] = $_POST['bis_shipping_methods'];
            $rules_vo->set_general_col3(json_encode($shipping_array));
            $rules_vo->set_general_col5(strip_tags($_POST['bis_shipping_error_des']));
        }
    } elseif ($action == "payment") {
        $rules_vo->set_general_col2($_POST['bis_payment_is']);
        $rules_vo->set_general_col3(json_encode($_POST['bis_payment_method']));
    }elseif ($action == "restrict_prod" || $action === "dependent_prod") {
        $rules_vo->set_general_col1($_POST['bis_restrict_error_vv']);
        $rules_vo->set_general_col2($_POST['bis_restrict_error']);
        $rules_vo->set_general_col3(json_encode($_POST['bis_restrict_product_list']));
    }
    
    if ($action === "append_existing_scode_post") {
        $content = $_POST['bis_re_product_dc_content'];

        if (RulesEngineUtil::is_valid_shortcode($content)) {
            $rules_vo->set_general_col1(trim($content));

            // Shortcode is the implicit definition for third-party shortcodes.
            $content_position = array("content_position" => "pos_cust_scode_post");
            $rules_vo->set_general_col2(json_encode($content_position));
        }
    } elseif ($action !== "restrict_prod" && $action !== "dependent_prod" && isset($_POST['bis_re_product_dc_content']) && $_POST['bis_re_product_dc_content'] != null ) {  // Dynamic Content Body
        $rules_vo->set_general_col1
                (strip_tags($_POST['bis_re_product_dc_content'], RulesEngineContentUtil::set_allowble_tags_as_string()));
    }

    if (isset($_POST['bis_re_cont_pos'])) {
        $content_position = array("content_position" => $_POST['bis_re_cont_pos']);
    }

    if (isset($_POST["bis_re_term_list"]) || (isset($_POST['bis_re_product_type']) && $_POST['bis_re_product_type'] === "categories")) {
        $rules_vo->set_general_col4(json_encode($_POST['bis_re_product_list']));
        if(isset($_POST["bis_re_term_list"])) {
            $rules_vo->set_rule_type_value($_POST['bis_re_term_list']);
        }
        /*elseif ($_POST['bis_re_product_type'] === "categories") {
            $product_cat_ids_array = $_POST['bis_re_product_list'];
            $product_ids = null;
            foreach ($product_cat_ids_array as $product_cat_id) {
                $terms = get_term($product_cat_id, 'product_cat');
                $theslug = $terms->slug;
                $args = array('post_type' => 'product', 'posts_per_page' => -1, 'product_cat' => $theslug);
                $products = new WP_Query($args);
                while ($products->have_posts()) {
                    $products->the_post();
                    $product_ids[] = $products->post->ID;
                }
            }
            $rules_vo->set_rule_type_value($product_ids);
        }*/
    }
    
    // Dynamic Content position
    if (!($action === "hide_post" || $action === "show_post" || $action === "restrict_prod" || $action === "payment" || $action === "shipping" ||
    $action === "dependent_prod" || $action === "replace_post_content" || $action === "append_existing_scode_post")) {
        $rules_vo->set_general_col2(json_encode($content_position));
    }

    // Background modal url
    if ($action === "append_image_post" ||
            $action === "append_image_bg_post") {
        $image_attr_array = array("image_id" => $_POST['bis_re_image_id'],
            "image_size" => $_POST['bis_re_cont_img_size']);

        $rules_vo->set_general_col3(json_encode($image_attr_array));
    }
    if (!(isset($_POST["bis_re_term_list"])) || $bis_re_product_type == 'categories' || $bis_re_product_type == 'product' || $bis_re_product_type == 'woo-tags') {
        $rules_vo->set_rule_type_value($_POST['bis_re_product_list']);
    }
    
    if ($action === "hide_post" && isset($_POST["bis_re_product_redirect_url"])) {
        // Redirect url
        $redirect_url = $_POST["bis_re_product_redirect_url"];
        $rules_vo->set_general_col3(RulesEngineUtil::get_filter_url($redirect_url));
    }

    if (isset($_POST['bis_re_product_rule_id'])) {
        $rules_vo->set_rule_id($_POST['bis_re_product_rule_id']);
        $rules_vo->set_logical_rule_id($_POST['bis_re_product_rule_id']);
    }
    if (isset($_POST['bis_auto_rule_str'])) {
        $rules_vo->set_general_col5($_POST['bis_auto_rule_str']);
    }
    
    $status = $results_map[BIS_STATUS];

    if ($status == BIS_SUCCESS) {
        RulesEngineCacheWrapper::set_reset_time(BIS_PRODUCT_RULE_RESET);
    }
    
    $popUpVO = new PopUpVO();

    $show_popUp = null;

    if (isset($_POST['bis_re_cont_pos']) && $_POST['bis_re_cont_pos'] == "pos_dialog_post") {

        $template_file_name = $_POST["bis_re_tempalte"];
        $popUpVO->setTempalteFileName($template_file_name);
        if ($template_file_name == "bis-popup-template.html") {
            $dynamicContent = file_get_contents(BIS_PLATFORM_HOME_DIR . "/template/bis-popup-template.html");
        } else {
            $file_path = RulesEngineUtil::get_file_upload_path() . BIS_PRODUCT_RULE_DIRECTORY . '/';
            $dynamicContent = file_get_contents($file_path . $template_file_name);
        }
        
        ProductRulesUtil::update_option(BIS_PRODUCT_POPUP_TEMPLATE, $dynamicContent);
        
        $show_popUp = 1;

        if (isset($_POST["bis_re_common_popup_title"])) {
            $popUpVO->setTitle($_POST["bis_re_common_popup_title"]);
        }

        if (isset($_POST["bis_re_common_popup_title_class"])) {
            $popUpVO->setTitleClass($_POST["bis_re_common_popup_title_class"]);
        }

        if (isset($_POST["bis_re_common_popup_color"])) {
            $popUpVO->setPopUpBackgroundColor($_POST["bis_re_common_popup_color"]);
        }

        if (isset($_POST["bis_re_common_heading"])) {

            $popUpVO->setHeadingOne($_POST["bis_re_common_heading"]);
        }
        if (isset($_POST["bis_re_common_heading_class"])) {
            $popUpVO->setHeadingOneClass($_POST["bis_re_common_heading_class"]);
        }
        if (isset($_POST["bis_re_common_sub_heading"])) {
            $popUpVO->setHeadingTwo($_POST["bis_re_common_sub_heading"]);
        }
        if (isset($_POST["bis_re_common_sub_heading_class"])) {
            $popUpVO->setHeadingTwoClass($_POST["bis_re_common_sub_heading_class"]);
        }

        if (isset($_POST["bis_re_image_id"])) {
            $image_id = $_POST["bis_re_image_id"];
            $image_size = $_POST["bis_re_cont_img_size"];
            $image = $product_rules_model->get_image_from_media_library($image_id, $image_size);
            $popUpVO->setImageOneId($image_id);
            $popUpVO->setImageOneSize($image_size);
            $popUpVO->setImageOneUrl($image->get_url());
        }

        $popUpVO->setButtonLabelOne($_POST["bis_re_btn1_label"]);

        if (isset($_POST["bis_re_btn1_class"])) {
            $popUpVO->setButtonOneClass($_POST["bis_re_btn1_class"]);
        }

        $popUpVO->setButtonLabelTwo($_POST["bis_re_but2_label"]);

        if (isset($_POST["bis_re_but2_class"])) {
            $popUpVO->setButtonTwoClass($_POST["bis_re_but2_class"]);
        }
        if (isset($_POST["bis_re_common_auto_red"])) {
            $popUpVO->setAutoCloseTime(intval($_POST["bis_re_common_auto_red"]));
        } else {
            $popUpVO->setAutoCloseTime(0);
        }
        if (isset($_POST["bis_re_but1_url"])) {
            $popUpVO->setButtonOneUrl($_POST['bis_re_but1_url']);
        }
        if (isset($_POST["bis_re_but2_url"])) {
            $popUpVO->setButtonTwoUrl($_POST['bis_re_but2_url']);
        }

        if (filter_input(INPUT_POST, "bis_re_common_red_window", FILTER_VALIDATE_INT)) {
            $popUpVO->setRedirectWindow($_POST['bis_re_common_red_window']);
        }

        if (filter_input(INPUT_POST, "bis_re_common_popup_occur", FILTER_VALIDATE_INT)) {
            $popUpVO->setPopUpOccurr($_POST['bis_re_common_popup_occur']);
        }

        // show popup
        //$rules_vo->set_general_col1($show_popUp);
        $rules_vo->set_general_col5(json_encode($popUpVO));
    }

    $rules_vo->set_reset_rule_key(BIS_PRODUCT_RULE_RESET);
    
    if (isset($_POST["bis_add_rule_type"]) && $_POST["bis_add_rule_type"] == "4") {
        if (isset($_POST['bis_re_edit_detail_id'])) {
            bis_update_query_builder_logical_rule_new($rules_vo);
        } else {
            bis_create_query_builder_logical_rule_new($rules_vo);
        }
    } else {
        if (isset($_POST['bis_re_edit_detail_id'])) {
            bis_re_update_rule_new($rules_vo);
        } else {
            bis_create_logical_rule_new($rules_vo);
        }
    }
}

/**
 * This method used to get product-rule-edit.php page
 */
function bis_re_show_edit_product_rule() {

    $nonce = $_GET ['bis_nonce'];

    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }

    $pluginPath = ProductRulesUtil::getIncludesDirPath();
    include $pluginPath . "product-rule-edit.php";

    flush();
    exit;
}

/**
 * This method used apply action (delete,deactivate,activate) for selected rules
 */
function bis_re_product_rule_action() {

    $nonce = $_GET ['bis_nonce'];

    $rule_id = $_GET ['ruleId'];
    
    $selectedAction = $_GET['selectedAction'];

    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }
    
    header("Content-Type: application/json");

    $product_rules_engine_modal = new ProductRulesEngineModel();

    $results_map = $product_rules_engine_modal->product_rule_action($rule_id, $selectedAction);

    RulesEngineUtil::generate_json_response($results_map);
}

/**
 * This method is used to get the products based on product type
 */
function bis_re_get_product_list() {
    
    $nonce = $_GET ['bis_nonce'];

    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }

    $product_rule_model = new ProductRulesEngineModel();

    $type_id = $_GET['bis_type_id'];

    if($type_id == "attributes" || $type_id == "categories" || $type_id == "woo-tags" || $type_id == "custom_taxo"){
        $results_map = $product_rule_model->get_all_product_taxonomies($type_id);
    } else {
        $results_map = $product_rule_model->get_all_post_list($type_id);
    }

    RulesEngineUtil::generate_json_response($results_map);
}

/**
 * This method is apply product rule from woo product list page
 */
function bis_apply_product_rule() {

    $product_rule_model = new ProductRulesEngineModel();
    $productid = $_GET['productIds'];
    $productrule = $_GET['product_rule'];
    $post_type = $_GET['post_type'];
    $results_map = $product_rule_model->applying_post_rule($productid, $productrule, $post_type);
}

/*
 * This method is used for getting list of terms by its slug.
 */
function bis_re_get_term_list_product(){
    
    $nonce = $_GET ['bis_nonce'];

    if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
        RulesEngineUtil::handle_request_forgery_error();
    }
    
    $ssubOptionId = $_GET["bis_term_id"];
    $product_type = $_GET["productId"];
    
    $product_rule_model = new ProductRulesEngineModel();
    $results_map = $product_rule_model->get_term_list_by_slug($ssubOptionId, $product_type);
    
    RulesEngineUtil::generate_json_response($results_map);
}

function bis_re_product_rules($applied_rule) {

    $rulesVO = null;

    if (!empty($applied_rule)) {
        $rulesVO = $applied_rule;
    }

    $results_map = array();
    $results_map[BIS_STATUS] = BIS_SUCCESS_WITH_NO_DATA;

    if ($rulesVO != null || $rulesVO != FALSE) {

        $results_map = bis_product_popup($rulesVO);
        // Get the popupvo
        $redirect_popup = json_decode($rulesVO->gencol5);
        // Check if popup occurrence is once
        if ($redirect_popup->popUpOccurr == 1) {
            setcookie(BIS_POPUP_SHOWN_ONCE, TRUE);
            RulesEngineCacheWrapper::remove_session_attribute(BIS_PRODUCT_POPUP_RULES);
        }
    }

    return $results_map;
}

function bis_re_product_popup() {

    $applied_product_rules = RulesEngineCacheWrapper::get_session_attribute(BIS_PRODUCT_POPUP_RULES);
    RulesEngineCacheWrapper::remove_session_attribute(BIS_PRODUCT_POPUP_RULES);
    
    $rulesVO = null;
    if(!empty($applied_product_rules)){
        $rulesVO = $applied_product_rules[0];
        $cposition = json_decode($applied_product_rules[0]->gencol2)->content_position;
        $rule_type_id = $applied_product_rules[0]->rule_type_id;
    }
    $results_map = array();
    $results_map[BIS_STATUS] = BIS_SUCCESS_WITH_NO_DATA;

    if ($rulesVO != null || $rulesVO != FALSE) {

        if ($cposition == "pos_dialog_post" && $rule_type_id == 8) {
            $results_map = bis_product_popup($rulesVO);
            // Get the popupvo
            $redirect_popup = json_decode($rulesVO->gencol5);
            // Check if popup occurrence is once
            if ($redirect_popup->popUpOccurr == 1) {
                setcookie(BIS_POPUP_SHOWN_ONCE, TRUE);
                RulesEngineCacheWrapper::remove_session_attribute(BIS_PRODUCT_POPUP_RULES);
            }
        } 
    }

    RulesEngineUtil::generate_json_response($results_map); 
}

function bis_product_popup($rulesVO) {

    $results_map = array();
    $popupMap = array();
    $donotShow = __("Do not show me this message again.", "productrules");
    $timerMsg = __("Site redirects in ", "productrules");
    $secMsg = __("seconds", "productrules");
    $jsonObj = json_decode($rulesVO->gencol5);
    $jsonObj->donot_show_msg = $donotShow;
    $appended_content = $rulesVO->gencol1;
    $jsonObj->appended_content = $appended_content;
    $popup_action = $rulesVO->action;
    $jsonObj->popup_action = $popup_action;

    if ($jsonObj->autoCloseTime !== "0") {
        $jsonObj->timerMsg = $timerMsg;
        $jsonObj->secMsg = $secMsg;
    }
    
    $geoLocVO = RulesEngineCacheWrapper::get_session_attribute(BIS_GEOLOCATION_VO);
    $jsonObj->headingOne = RulesEngineUtil::replace_geo_placeholders($jsonObj->headingOne, $geoLocVO);
    $jsonObj->headingTwo = RulesEngineUtil::replace_geo_placeholders($jsonObj->headingTwo, $geoLocVO);
    $dynamicContent = RulesEngineUtil::get_option(BIS_PRODUCT_POPUP_TEMPLATE);
    $jsonObj->popupTemplate = $dynamicContent;

    $results_map[BIS_STATUS] = BIS_SUCCESS;
    $popupMap[BIS_GEOLOCATION_DATA] = $geoLocVO;
    $popupMap[BIS_POPUP_DATA] = $jsonObj;
    $results_map[BIS_DATA] = $popupMap;
    $results_map[BIS_ROW_COUNT] = null;

    return $results_map;
}