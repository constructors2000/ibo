<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

// Plugin version constant
define("BIS_PRODUCT_RULES_VERSION_CONST", "BIS_PRODUCT_RULES_VERSION");
define("BIS_PRODUCT_RULES_PLUGIN_IS_ACTIVE_CONST", "BIS_PRODUCT_RULES_PLUGIN_IS_ACTIVE");
define("BIS_PRODUCT_SUBOPTION_ID", 7);
define("BIS_PRODUCT_PLUGIN_ID", "bis_products_plugin");
define("BIS_PRODUCT_PLUGIN_DISPLAY_NAME", "Product Controller");
define("BIS_PRODUCT_CSS_CLASS", "product-rule-icon");
define("BIS_PRODUCT_PLUGIN_PATH", "admin.php?page=bis_pg_productrules");
define("BIS_PRODUCT_PLUGIN_DESCRIPTION", "Define rules for hiding, appending and replacing content for products.");
define("BIS_PRODUCT_RULES_TEXT_DOMAIN", "productrules");
define("BIS_PRODUCT_API_KEY", "cs5nm273hbj6v9uyk2j1i2pd32el4tbr");
define("BIS_PRODUCT_PUR_CODE", "BIS_PUR_CODE_".BIS_PRODUCT_PLUGIN_ID);
define("BIS_PRODUCT_POPUP_VO", "BIS_PRODUCT_POPUP_VO");
//define("BIS_PRODUCT_POPUP_TEMPLATE", "BIS_PRODUCT_POPUP_TEMPLATE");
define("BIS_SUCCESS_WITH_NO_PRODUCT", "success_with_no_product");
define("BIS_PRODUCT_SESSION_RULE_EVALUATED", "BIS_PRODUCT_SESSION_RULE_EVALUATED");
define("BIS_PRODUCT_PAGE", "bis_product_page");
define("BIS_PRODUCT_RULE_ACTION", "bis_product_rule_action");
define("BIS_PRODUCT_TYPE", "bis_product_type");
define("BIS_PRODUCT_IDS", "bis_product_ids");
define("BIS_PRODUCT_TITLE", "bis_product_title");
define("BIS_ADD_PRODUCT_RULE", "bis_add_product_rule");
define("BIS_PROD_GEN_FIELD_RULE", "bis_prod_gen_field_rule");

//test
//define("BIS_ADD_PAYMENT_RULE", "bis_add_payment_rule");
define("BIS_PAYM_GEN_FIELD_RULE","bis_paym_gen_field_rule");

define("BIS_ADD_SHIPPING_RULE", "bis_add_shipping_rule");
define("BIS_SHIP_GEN_FIELD_RULE", "bis_ship_gen_field_rule");

//Distance
define("BIS_DISTANCE_GOOGLE_API_URL", "http://maps.googleapis.com/maps/api/distancematrix/json");
