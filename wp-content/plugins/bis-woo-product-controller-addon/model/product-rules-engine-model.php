<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

use bis\repf\model\PostRulesEngineModel;
use bis\repf\vo\RulesVO;
use bis\repf\common\RulesEngineCacheWrapper;
use bis\repf\model\LogicalRulesEngineModel;

/**
 * Class ProductRulesEngineModel
 */
class ProductRulesEngineModel extends PostRulesEngineModel {

    /**
     * This method is used to get all product rules.
     *
     * @return multitype:
     */
    public function get_product_rules($page_start_index = 0) {
        return $this->get_child_rules(BIS_WOO_PRODUCT_TYPE_RULE, $page_start_index);
    }
    
    /**
     * This method is used to get all product rules.
     *
     * @return multitype:
     */
    public function is_product_rule_exists($rule_name, $rdetail_id = 0) {

        return $this->is_child_rule_exists($rule_name, $rdetail_id, BIS_WOO_PRODUCT_TYPE_RULE);
    }
    
    /**
     * Delete product rule using ruleId.
     *
     * @param  $ruleId .
     * @return \multitype
     */
    public function product_rule_action($ruleId, $selectedAction) {

        $status = $this->child_rule_action($ruleId, $selectedAction);
        $results_map = $this->get_product_rules();

        if (!$status) {
            $results_map[BIS_STATUS] = BIS_ERROR;
        }

        return $results_map;
    }
   
    /*
     * This method used for checking the rule is logical rule or not.
     * @param $bis_re_rule_detail_id
     * @return $row
     */
    public function check_is_logical_rule($bis_re_rule_detail_id) {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $query = "select general_col5 from " . $table_prefix . "bis_re_rule_details WHERE id=" . $bis_re_rule_detail_id;
        $row = $wpdb->get_results($query)[0]->general_col5;
        return $row;
    }
    
    public function get_product_show_rules() {
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $query = "SELECT rrd.id, rrd.action_hook, rrd.name, rrd.description, rrd.status,
                    rrd.child_sub_rule, rrd.action, rrd.parent_type_value, rrd.general_col1 AS gencol1,
                    rrd.general_col2 AS location, rrd.general_col4 AS slug, rr.parent_id
                FROM " . $table_prefix . "bis_re_rule_details rrd 
                JOIN " . $table_prefix . "bis_re_rules rr ON rr.rule_details_id = rrd.id 
                WHERE rrd.rule_type_id = 8 AND rrd.action ='show_post' AND rrd.status=1;";

        $rows = $wpdb->get_results($query);

        return $rows;
    }
 
    /*
     * This method is used for get term ids by using product ids.
     * @param $product_ids, $parent_type_val
     * @return $term_ids
     */
    public function get_product_term_ids($product_ids, $parent_type_val){
        
        $term_ids = array();
        
        if($parent_type_val == "attributes"){
            $term_ids = null;
            foreach ($product_ids as $product_id_key => $product_id_val) {
                $product = new WC_Product_Variable($product_id_val);
                $variations = $product->get_available_variations();
                if(!empty($variations)){
                    foreach ($variations as $variation) {
                        if ($term_ids == null) {
                            $term_ids = array_values($variation['attributes']);
                        } else {
                            $term_ids = array_merge($term_ids, array_values($variation['attributes']));
                        }
                    }
                }else{
                    $term_ids = array();
                }
            }
        } elseif ($parent_type_val == "custom_taxo") {
            $taxonomies = get_taxonomies();
            foreach ($product_ids as $product_id_key => $product_id_val) {
                $prod_taxo = wp_get_post_terms($product_id_val, $taxonomies);
                if (!empty($prod_taxo)) {
                    foreach ($prod_taxo as $prod_taxo) {
                        array_push($term_ids, $prod_taxo->slug);
                    }
                } 
            }
        }elseif ($parent_type_val == "woo-tags") {
            foreach ($product_ids as $product_id_key => $product_id_val) {
                $terms = get_the_terms($product_id_val, 'product_tag');
                if(!empty($terms)){
                    foreach ($terms as $term) {
                        array_push($term_ids, $term->slug);
                    }
                }
            }
        } elseif ($parent_type_val == "categories") {
            foreach ($product_ids as $product_id_key => $product_id_val) {
                $terms = get_the_terms($product_id_val, 'product_cat');
                if (!empty($terms)) {
                    foreach ($terms as $term) {
                        array_push($term_ids, $term->term_id);
                    }
                }
            }
        }
        
        return $term_ids;
    }
    
    /*
     * This method used for get the active woo payment gateways.
     * @retuen $active_payment_methods
     */
    public function get_active_payment_methods(){
        global $woocommerce;
        $active_payment_methods = array();
        $payment_methods = $woocommerce->payment_gateways->payment_gateways();
        $method = null;
        if(!empty($payment_methods)){
            foreach ($payment_methods as $payment_method) {
                if ($payment_method->enabled == 'yes') {
                    $method['name'] = $payment_method->title;
                    $method['id'] = $payment_method->id;
                    array_push($active_payment_methods, $method);
                }
            }
        }
        return $active_payment_methods;
    }
    
    /*
     * This method used for saving product rule, which is created at woo settings mega product rules tab.
     * @param $criteria_array, $bis_woo_sett_pro_rule, $bis_woo_sett_pro_exist_rule, $rule_type
     */
    public function save_woo_settiings_tab_rules($criteria_array, $bis_woo_sett_pro_rule, $bis_woo_sett_pro_exist_rule, $rule_type){
        
        if ($rule_type == "product_rule") {
            $parent_id = 'bis_prod_all';
            $exist_cri = 'product_criteria';
        } elseif ($rule_type == "shipping_rule") {
            $parent_id = 'bis_ship_all';
            $exist_cri = 'shipping_criteria';
        } elseif ($rule_type == "payment_rule") {
            $parent_id = 'bis_payment_all';
            $exist_cri = 'payment_criteria';
        }
        
        if ($criteria_array['criteria'] == "none") {
            $bis_re_rule_detail_id = $this->get_woo_sett_product_rule_by_parent_id($parent_id);
            if ($bis_woo_sett_pro_exist_rule[$exist_cri] == "bis_logical_rule") {
                $status = $this->child_rule_action($bis_re_rule_detail_id, "delete");
            } else {
                $status = $this->child_rule_action($bis_re_rule_detail_id, "delete", "delete_previous_criteria");
            }
        } else {
            if ($criteria_array['criteria'] != "bis_logical_rule") {
                $bis_criteria_array[0] = $criteria_array['criteria'];
                if (isset($criteria_array['dropdown_val'])) {
                    $bis_criteria_array[1] = $criteria_array['dropdown_val'];
                } else {
                    $bis_criteria_array[1] = null;
                }
                if (isset($criteria_array['txt_val'])) {
                    $bis_criteria_array[2] = $criteria_array['txt_val'];
                } else {
                    $bis_criteria_array[2] = null;
                }
            }
            if (!empty($bis_woo_sett_pro_exist_rule) && $bis_woo_sett_pro_exist_rule[$exist_cri] != 'none') {
                
                $bis_re_rule_detail_id = $this->get_woo_sett_product_rule_by_parent_id($parent_id);
                $row = $this->check_is_logical_rule($bis_re_rule_detail_id);
                if ($bis_woo_sett_pro_exist_rule[$exist_cri] == "bis_logical_rule" || $row == "logical_rule") {
                    $status = $this->child_rule_action($bis_re_rule_detail_id, "delete");
                } else {
                    $status = $this->child_rule_action($bis_re_rule_detail_id, "delete", "delete_previous_criteria");
                }
            }
    
            $rule_name = "bis_product_" . microtime(true);
            $rdetail_id = 0;
            $description = "";
            $re_status = "1";
            $re_product_type = "product";
            $rules_vo = new RulesVO();
            
            // Check if product rule exists
            $results_map = $this->is_product_rule_exists($rule_name, $rdetail_id);
            $status = $results_map[BIS_STATUS];

            if ($status === BIS_ERROR) {
                RulesEngineUtil::generate_json_response($results_map);
            }

            $rules_vo->set_name($rule_name);
            $rules_vo->set_description($description);
            $rules_vo->set_status($re_status);
            $rules_vo->set_parent_type_value($re_product_type);
            $rules_vo->set_rule_type_id(BIS_WOO_PRODUCT_TYPE_RULE);
            
            if ($rule_type == "product_rule"){
                $rules_vo->set_action($bis_woo_sett_pro_rule['product_action']);
                $product_id[0] = "bis_prod_all";
            } elseif ($rule_type == "shipping_rule"){
                $rules_vo->set_action('shipping');
                $rules_vo->set_general_col2($bis_woo_sett_pro_rule['shipping_action']);
                $rules_vo->set_general_col3("");
                $product_id[0] = "bis_ship_all";
            } elseif ($rule_type == "payment_rule") {
                $rules_vo->set_action('payment');
                $rules_vo->set_general_col2($bis_woo_sett_pro_rule['payment_action']);
                $rules_vo->set_general_col3(json_encode($bis_woo_sett_pro_rule['payment_method']));
                $product_id[0] = "bis_payment_all";
            }
            $rules_vo->set_rule_type_value($product_id);
            
            if ($criteria_array['criteria'] == "bis_logical_rule") {
                $rules_vo->set_logical_rule_id($criteria_array['dropdown_val']);
                $rules_vo->set_general_col5("logical_rule");
            } else {
                $rules_vo->set_general_col4("bis_admin_rule");
            }

            $status = $results_map[BIS_STATUS];

            if ($status == BIS_SUCCESS) {
                RulesEngineCacheWrapper::set_reset_time(BIS_PRODUCT_RULE_RESET);
            }

            $rules_vo->set_reset_rule_key(BIS_PRODUCT_RULE_RESET);

            if ($criteria_array['criteria'] == "bis_logical_rule") {
                $logical_rule_engine_modal = new LogicalRulesEngineModel();
                $results_map = $logical_rule_engine_modal->save_child_rule($rules_vo);
            } else {
                bis_create_logical_rule_new($rules_vo, $bis_criteria_array);
            }
        }
    }
    
    /*
     * This method is used for get the rule detail id based on parent_id
     * @param $parent_id
     * @return $brrd_id
     */
    public function get_woo_sett_product_rule_by_parent_id($parent_id){
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $wpp_query = "SELECT brrd.id FROM " . $table_prefix . "bis_re_rule_details AS brrd JOIN " . $table_prefix . "bis_re_rules AS brr ON brrd.id = brr.rule_details_id WHERE brr.parent_id =%s";
        $rows = $wpdb->get_results($wpdb->prepare($wpp_query, $parent_id));
        $brrd_id = null;
        if(!empty($rows)){
            $brrd_id = $rows[0]->id;
        }
        return $brrd_id;
    }

    public function bis_save_general_box_rule($bis_action, $bis_criteria, $bis_dropdown, $bis_text_value, $post_id, $bis_re_rule_detail_id, $rule_type, $bis_payment_action = null, $selected_payment_methods = null) {
        
        if ($bis_criteria == "none") {
            $product_rules_engine_modal = new ProductRulesEngineModel();
            if ($bis_re_rule_detail_id != "") {
                $row = $product_rules_engine_modal->check_is_logical_rule($bis_re_rule_detail_id);
                if ($bis_criteria == "bis_logical_rule" || $row == "logical_rule") {
                    $status = $product_rules_engine_modal->child_rule_action($bis_re_rule_detail_id, "delete");
                } else {
                    $status = $product_rules_engine_modal->child_rule_action($bis_re_rule_detail_id, "delete", "delete_previous_criteria");
                }
            }
        } else {
            $product_rules_model = new ProductRulesEngineModel();

            if ($bis_criteria != "bis_logical_rule") {
                $bis_criteria_array[0] = $bis_criteria;
                $bis_criteria_array[1] = $bis_dropdown;
                $bis_criteria_array[2] = $bis_text_value;
            }
            if ($bis_re_rule_detail_id != "") {
                $product_rules_engine_modal = new ProductRulesEngineModel();
                $row = $product_rules_engine_modal->check_is_logical_rule($bis_re_rule_detail_id);
                if ($bis_criteria == "bis_logical_rule" || $row == "logical_rule") {
                    $status = $product_rules_engine_modal->child_rule_action($bis_re_rule_detail_id, "delete");
                } else {
                    $status = $product_rules_engine_modal->child_rule_action($bis_re_rule_detail_id, "delete", "delete_previous_criteria");
                }
            }
            $rule_name = "bis_product_" . microtime(true);
            $rdetail_id = 0;
            $description = "";
            $re_status = "1";
            $re_product_type = "product";
            $rules_vo = new RulesVO();

            $product_id[0] = $post_id;

            // Check if product rule exists
            $results_map = $product_rules_model->is_product_rule_exists($rule_name, $rdetail_id);
            $status = $results_map[BIS_STATUS];

            if ($status === BIS_ERROR) {
                RulesEngineUtil::generate_json_response($results_map);
            }

            $rules_vo->set_name($rule_name);
            $rules_vo->set_description($description);
            $rules_vo->set_action($bis_action);
            $rules_vo->set_status($re_status);
            $rules_vo->set_parent_type_value($re_product_type);
            $rules_vo->set_rule_type_id(BIS_WOO_PRODUCT_TYPE_RULE);
            $rules_vo->set_rule_type_value($product_id);
            
            if ($rule_type == "shipping_rule") {
                $rules_vo->set_general_col2($bis_payment_action);
                $rules_vo->set_general_col3("");
            } elseif ($rule_type == "payment_rule") {
                $rules_vo->set_general_col2($bis_payment_action);
                $rules_vo->set_general_col3(json_encode($selected_payment_methods));
            }
            
            if ($bis_criteria == "bis_logical_rule") {
                $rules_vo->set_logical_rule_id($bis_dropdown);
                $rules_vo->set_general_col5("logical_rule");
            } else {
                $rules_vo->set_general_col4("bis_admin_rule");
            }

            $status = $results_map[BIS_STATUS];

            if ($status == BIS_SUCCESS) {
                RulesEngineCacheWrapper::set_reset_time(BIS_PRODUCT_RULE_RESET);
            }

            $rules_vo->set_reset_rule_key(BIS_PRODUCT_RULE_RESET);

            if ($bis_criteria == "bis_logical_rule") {
                $logical_rule_engine_modal = new LogicalRulesEngineModel();
                $results_map = $logical_rule_engine_modal->save_child_rule($rules_vo);
            } else {
                bis_create_logical_rule_new($rules_vo, $bis_criteria_array);
            }
        }
        RulesEngineUtil::update_rule_cache_id(BIS_LOGICAL_CACHE_REFRESH_ID);
    }
    
    public function bis_get_woo_shiping_methods(){
        global $woocommerce;
        $shipping_zones = WC_Shipping_Zones::get_zones();
        $shipping_methods = [];
        $method = [];
        $shipping_method = [];
        foreach ($shipping_zones as $shipping_zone) {
            $shipping_method['zone_id'] = $shipping_zone['id'];
            $shipping_method['zone_title'] = $shipping_zone['zone_name'];
            $shipping_method['shipping_methods'] = [];
            if(!empty($shipping_zone['shipping_methods'])){
                foreach ($shipping_zone['shipping_methods'] as $shipping_zone_method) {
                    $method['id'] = $shipping_zone_method->id;
                    $method['method_title'] = $shipping_zone_method->method_title;
                    $shipping_method['shipping_methods'][] = $method; 
                }
                
            }
            $shipping_methods[] = $shipping_method;
        }
        $default_methods = $this->get_defaul_zone_methods();
        array_push($shipping_methods, $default_methods);
        return $shipping_methods;
    }
    
    public function get_defaul_zone_methods(){
        global $wpdb;
        $table_prefix = $wpdb->prefix;
        $wpp_query = "SELECT * FROM " . $table_prefix . "woocommerce_shipping_zone_methods WHERE zone_id=0";
        $rows = $wpdb->get_results($wpp_query);
        $default_methods = array();
        if (!empty($rows)) {
            $method = [];
            $default_methods['zone_id'] = 0;
            $default_methods['zone_title'] = "Other";
            $default_methods['shipping_methods'] = [];
            foreach ($rows as $row) {
                $method['id'] = $row->method_id;
                $method['method_title'] = $row->method_id;
                $default_methods['shipping_methods'][] = $method;
            }
        }
        return $default_methods;
    }

    public function create_product_rule_based_user($post) {
        
        $author_id = (int) $post->post_author;
        $user_meta = get_userdata($author_id);
        $user_roles = $user_meta->roles;
        $author_data = get_user_meta($author_id);
        if(!empty($user_roles) && $user_roles[0] === "wcfm_vendor"){
            
            global $wpdb;
            $base_prefix = $wpdb->base_prefix;
            for($i = 1; $i <= 2 ; $i++){
                $rule_status = "add";
                $logical_rule_id = 0;
                $detail_id = 0;
                $exp_ids = array();
                $prod_ids = array();
                $bis_re_rcId = array();
                if($i === 1){
                    $query = "SELECT id, logical_rule_id, general_col5 FROM " . $base_prefix . "bis_re_rule_details WHERE general_col5 LIKE 'auto_rule%'";
                    $rows = $wpdb->get_results($query);
                    if (!empty($rows) && $rows !== null) {
                        foreach ($rows as $row) {
                            $detail_id = (int) $row->id;
                            $logical_rule_id = (int) $row->logical_rule_id;
                            $saved_prod_id_str = $row->general_col5;
                            $exp_ids = explode("_", $saved_prod_id_str);
                            if ((int) $exp_ids[2] === $author_id) {
                                $rule_status = "update";
                                break;
                            }
                        }
                    }

                    $gen_col_str = "auto_rule_" . $author_id;
                    if ($rule_status === "update") {
                        foreach ($exp_ids as $exp_id) {
                            if ((int) $exp_id !== (int) $author_id && (int) $exp_id !== (int) $post->ID && $exp_id !== "auto" && $exp_id !== "rule") {
                                $gen_col_str = $gen_col_str . "_" . $exp_id;
                                array_push($prod_ids, $exp_id);
                            }
                        }
                        $gen_col_str = $gen_col_str . "_" . $post->ID;
                        array_push($prod_ids, $post->ID);

                        $query = "SELECT id FROM " . $base_prefix . "bis_re_logical_rules_criteria WHERE logical_rule_id = " . $logical_rule_id;
                        $crite_ids = $wpdb->get_results($query);
                        foreach ($crite_ids as $crite_id) {
                            array_push($bis_re_rcId, $crite_id->id);
                        }
                    } else {
                        $gen_col_str = $gen_col_str . "_" . $post->ID;
                        array_push($prod_ids, $post->ID);
                    }
                    $author_name = $author_data['nickname'][0];
                    $rule_action = "hide_post";
                    $bis_re_sub_option = array("6_29");
                    $bis_re_condition = array("2");
                    
                    RulesEngineCacheWrapper::delete_all_transient_cache();
                    $rule_name = $author_name;
                    $author_city = $author_data['billing_city'][0];
                    /* $author_country = $author_data['billing_country'][0];
                      $includes = RulesEngineUtil::convertToSQLString($author_country);
                      $country = $this->get_countries($includes);
                      $author_country_id = $country[0]->id;
                      $author_country_name = $country[0]->name;
                      $author_mail = $user_meta->user_email; */

                    $rdetail_id = $detail_id;

                    // Check if product rule exists
                    $results_map = $this->is_product_rule_exists($rule_name, $rdetail_id);
                    $status = $results_map[BIS_STATUS];

                    if ($status === BIS_ERROR) {
                        RulesEngineUtil::generate_json_response($results_map);
                    }
                    $rules_vo = new RulesVO();
                    $rules_vo->set_name($rule_name);
                    $rules_vo->set_description("");
                    $rules_vo->set_action($rule_action);
                    $rules_vo->set_status(1);
                    $rules_vo->set_parent_type_value("product");
                    $rules_vo->set_rule_type_id(BIS_WOO_PRODUCT_TYPE_RULE);
                    $rules_vo->set_rule_type_value($prod_ids);
                    $rules_vo->set_reset_rule_key(BIS_PRODUCT_RULE_RESET);
                    $rules_vo->set_general_col5($gen_col_str);

                    if ($rule_status === "update") {
                        $rules_vo->set_id($rdetail_id);
                        $rules_vo->set_rule_id($logical_rule_id);
                        $rules_vo->set_logical_rule_id($logical_rule_id);
                    }

                    $bis_criteria = array(
                        "bis_re_sub_option" => $bis_re_sub_option,
                        "bis_re_condition" => $bis_re_condition,
                        "bis_re_rule_value" => array('[{\"id\":\"' . $author_city . '\",\"name\":\"' . $author_city . '\"}]'),
                        "bis_re_left_bracket" => "0",
                        "bis_re_right_bracket" => "0",
                        "bis_re_sub_opt_type_id" => array("1"),
                        "bis_re_eval_type" => "1",
                        "bis_add_rule_type" => "1",
                        "bis_re_rule_status" => "1",
                        "bis_re_rcId" => $bis_re_rcId,
                        "bis_re_rId" => $logical_rule_id
                    );

                    if ($rule_status === "update") {
                        bis_re_update_rule_new($rules_vo, $bis_criteria, "user_based");
                    } else {
                        bis_create_logical_rule_new($rules_vo, $bis_criteria, "user_based");
                    }
                    
                } elseif ($i === 2) {
                    $query = "SELECT id, logical_rule_id, general_col5 FROM " . $base_prefix . "bis_re_rule_details WHERE general_col5 LIKE 'auto_rule_s%'";
                    $rows = $wpdb->get_results($query);
                    if (!empty($rows) && $rows !== null) {
                        foreach ($rows as $row) {
                            $detail_id = (int) $row->id;
                            $logical_rule_id = (int) $row->logical_rule_id;
                            $saved_prod_id_str = $row->general_col5;
                            $exp_ids = explode("_", $saved_prod_id_str);
                            if ((int) $exp_ids[3] === $author_id) {
                                $rule_status = "update";
                                break;
                            }
                        }
                    }

                    $gen_col_str = "auto_rule_s_" . $author_id;
                    if ($rule_status === "update") {
                        foreach ($exp_ids as $exp_id) {
                            if ((int) $exp_id !== (int) $author_id && (int) $exp_id !== (int) $post->ID && $exp_id !== "auto" && $exp_id !== "rule" && $exp_id !== "s") {
                                $gen_col_str = $gen_col_str . "_" . $exp_id;
                                array_push($prod_ids, $exp_id);
                            }
                        }
                        $gen_col_str = $gen_col_str . "_" . $post->ID;
                        array_push($prod_ids, $post->ID);

                        $query = "SELECT id FROM " . $base_prefix . "bis_re_logical_rules_criteria WHERE logical_rule_id = " . $logical_rule_id;
                        $crite_ids = $wpdb->get_results($query);
                        foreach ($crite_ids as $crite_id) {
                            array_push($bis_re_rcId, $crite_id->id);
                        }
                    } else {
                        $gen_col_str = $gen_col_str . "_" . $post->ID;
                        array_push($prod_ids, $post->ID);
                    }
                    
                    $author_name = $author_data['nickname'][0] . "sbox";
                    $rule_action = "show_only_post";
                    $bis_re_sub_option = array("16_41");
                    $bis_re_condition = array("1");
                    
                    RulesEngineCacheWrapper::delete_all_transient_cache();
                    $rule_name = $author_name;
                    $author_city = $author_data['billing_city'][0];
                    $rdetail_id = $detail_id;
                    // Check if product rule exists
                    $results_map = $this->is_product_rule_exists($rule_name, $rdetail_id);
                    $status = $results_map[BIS_STATUS];

                    if ($status === BIS_ERROR) {
                        RulesEngineUtil::generate_json_response($results_map);
                    }
                    $rules_vo = new RulesVO();
                    $rules_vo->set_name($rule_name);
                    $rules_vo->set_description("");
                    $rules_vo->set_action($rule_action);
                    $rules_vo->set_status(1);
                    $rules_vo->set_parent_type_value("product");
                    $rules_vo->set_rule_type_id(BIS_WOO_PRODUCT_TYPE_RULE);
                    $rules_vo->set_rule_type_value($prod_ids);
                    $rules_vo->set_reset_rule_key(BIS_PRODUCT_RULE_RESET);
                    $rules_vo->set_general_col5($gen_col_str);

                    if ($rule_status === "update") {
                        $rules_vo->set_id($rdetail_id);
                        $rules_vo->set_rule_id($logical_rule_id);
                        $rules_vo->set_logical_rule_id($logical_rule_id);
                    }
                    $bis_criteria = array(
                        "bis_re_sub_option" => $bis_re_sub_option,
                        "bis_re_condition" => $bis_re_condition,
                        "bis_re_rule_value" => array('[{\"id\":\"' . $author_city . '\",\"name\":\"' . $author_city . '\"}]'),
                        "bis_re_left_bracket" => "0",
                        "bis_re_right_bracket" => "0",
                        "bis_re_sub_opt_type_id" => array("1"),
                        "bis_re_eval_type" => "1",
                        "bis_add_rule_type" => "1",
                        "bis_re_rule_status" => "1",
                        "bis_re_rcId" => $bis_re_rcId,
                        "bis_re_rId" => $logical_rule_id
                    );

                    if ($rule_status === "update") {
                        bis_re_update_rule_new($rules_vo, $bis_criteria, "user_based");
                    } else {
                        bis_create_logical_rule_new($rules_vo, $bis_criteria, "user_based");
                    }
                }  
            }
        }
    }
}
