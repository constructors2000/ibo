<?php
use bis\repf\common\RulesEngineCacheWrapper;

$rule_action = "";
if (isset($_GET['rule_action'])) {
    $page = $_GET['page'];
    $rule_action = $_GET['rule_action'];
    $post_type = $_GET['pos_type'];
    $productids = $_GET['productids'];
    $product_title = "";
    if(isset($_GET['product_title'])){
        $post_title = $_GET['product_title'];
    }
    $array_values = array(
        BIS_PRODUCT_PAGE => $page, 
        BIS_PRODUCT_RULE_ACTION => $rule_action,
        BIS_PRODUCT_TYPE => $post_type, 
        BIS_PRODUCT_IDS => $productids, 
        BIS_PRODUCT_TITLE => $product_title
    );
    RulesEngineCacheWrapper::set_session_attribute(BIS_ADD_PRODUCT_RULE, $array_values);
}
?>
<span id="product_rule_tab"> 
    <span id="product_rules_list_content">
        <form name="productRulesListForm" id="productRulesListForm" method="POST">
            <!-- Search Panel -->
            <div class="panel panel-default">
                <div class="panel-heading dashboard-panel-heading">
                    <div>
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="panel-title"><label><?php _e("Product Rules", BIS_PRODUCT_RULES_TEXT_DOMAIN); ?></label></h3>
                            </div>
                            <div class="col-md-6">
                                <strong class="panel-help"><label style="float:right;"><a id="bis_help_url" target="_blank" href="http://docs.megaedzee.com/docs/product-controller/"><?php _e("Help", BIS_PRODUCT_RULES_TEXT_DOMAIN); ?> &nbsp;<span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label></strong>
                            </div>
                        </div>
                    </div>
                    <div class="bis-searh-box-main-container"><a class="search-box-a" href="javascript:void(0)"><i class="glyphicon glyphicon-menu-down bis-glyphi-position"></i><?php _e("Search", "rulesengine");?></a></div>
                </div>
                <div class="panel-body dashboard-panel post-rules-container">
                    <div class="container-fluid search-container">
                        <div>
                            <div class="col-sm-6 pl-0 pr-0">
                                <div class="input-group search-group-postrules">
                                    <div class="input-group-btn">
                                        <select name="bis_re_product_search_by" size="2" id="bis_re_product_search_by"class="bis-multiselect">
                                            <option value="name" selected="selected"><?php _e("Name", "productrules"); ?></option>
                                            <option value="description"><?php _e("Description", "productrules"); ?></option>
                                        </select>
                                    </div>
                                    <input type="text" id="bis_re_product_search_value" name="bis_re_product_search_value" placeholder='<?php _e("Enter search criteria", "productrules"); ?>' class="form-control bis-search-post-rules hasclear">
                                    <span class="bis-clearer glyphicon glyphicon-remove-circle form-control-feedback"></span>
                                </div>
                            </div>
                            <div class="col-sm-4 pr-0">
                                <label><?php _e("Status", "productrules"); ?> : </label>
                                <select name="bis_re_product_search_status" size="2" id="bis_re_product_search_status" class="bis-multiselect">
                                    <option selected="selected" value="all"><?php _e("All", "productrules"); ?></option>
                                    <option value="1"><?php _e("Active", "productrules"); ?></option>
                                    <option value="0"><?php _e("Deactive", "productrules"); ?></option>
                                </select>
                                <button class="form-control btn btn-primary bis-main-search-button" type="submit">
                                    <?php _e("Go", "productrules"); ?>
                                </button>
                            </div>
                            
                        </div>
                    </div>

                    <div class="bis-table-scroll">
                        <!-- Message span -->
                        <div id="bis-product-no-result" class="bis-no-results-message alert alert-info text-center"><center></center></div>
                            <div class="col-md-3 pl-0 mb-10">
                                <select name="bis_select_action" size="2" id="bis_select_action" disabled="disable" data-placeholder="<?php _e("Bulk Actions", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>">
                                    <option value="activate"><?php _e("Activate", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                    <option value="deactivate"><?php _e("Deactivate", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                    <option value="delete"><?php _e("Delete", BIS_RULES_ENGINE_TEXT_DOMAIN); ?></option>
                                </select>
                            </div>
                        <div class=" pull-right">
                            <button id="bis_re_show_add_product_rule" type="button" class="btn btn-primary">
                                <i class="glyphicon glyphicon-plus bis-glyphi-position"></i><?php _e("Add Rule", "productrules"); ?>
                            </button>
                        </div> 
                        <table id="bis-product-rules-table" style="display: none" class="table table-hover table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="2%"><input type="checkbox" id="bis_check_all" name="checked_id[]" class="checkbox" value=""/></th>
                                    <th width="20%"><?php _e("Name", "productrules"); ?></th>
                                    <th width="15%"><?php _e("Products", "productrules"); ?></th>
                                    <th width="20%"><?php _e("Description", "productrules"); ?></th>
                                    <th width="20%"><?php _e("Action", "rulesengine"); ?></th>
                                    <th width="10%"><?php _e("Status", "productrules"); ?></th>
                                </tr>
                            </thead>
                            <tbody id="bis-productRulesListContent"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div>
                <div class="bis-pagination-top col-md-7 pull-left">
                    <span class="pagination"></span>
                    <input id="searchPagination" type="hidden" value="">
                </div>
                <div class="col-md-2 pull-right">
                    <label class="bis-rows-count"><?php _e("Number of Rules", "productrules"); ?></label><span class="badge bis-rows-no">0</span>
                </div>
            </div>
        </form>
    </span> 
    <input type="hidden" id="bis_child_list_callback" name="bis_child_list_callback" value="bis_showProductRulesList" />
    <span id="product_rules_child_content"> </span>	
    <span id="product_rules_parent_content"> </span>	
</span>
<script>

    // wait for the DOM to be loaded
    jQuery(document).ready(function () {
        
        bis_setProductSearchButtonWidth();
        bis_productFieldsReset();

        var options = {
            success: function(response){
                if(response["status"] === "success") {
                    var row_count = response["total_records"];
                    var total_pages = Math.ceil(row_count/10);
                    var action = response["bis_action"];
                    jQuery('.pagination').show();
                    jQuery('#searchPagination').val(action);
                    bis_productBootpages(total_pages);
                    jQuery('.pagination').bootpag({page: 1});
                    jQuery('#bis_select_action').multiselect("disable");
                } else {
                    jQuery('.pagination').hide();
                    jQuery('#bis_select_action').next().hide();
                }
                bis_showProductResponse(response);
                return false;
            },
            url: BISAjax.ajaxurl,
            data: {
                action: 'bis_re_product_search_rule',
                bis_nonce: BISAjax.bis_rules_engine_nonce
            }
        };

        jQuery('#productRulesListForm').ajaxForm(options);
       
        jQuery("#bis_re_show_add_product_rule").click(function () {
            jQuery.post(
                    BISAjax.ajaxurl,
                    {                       
                        action: 'bis_re_show_add_product_rule',
                        beforeSend: bis_before_request_send,
                        bis_nonce: BISAjax.bis_rules_engine_nonce
                    },
                    function (response) {
                        jQuery("#product_rules_child_content").html(response);
                        jQuery("#product_rules_list_content").hide();
                        jQuery("#product_rules_child_content").show();
                        bis_setProductSearchButtonWidth();
                        bis_productFieldsReset();
                        bis_request_complete();
                    });

        });
        jQuery("#bis_check_all").change(function(){
            jQuery("input:checkbox").prop('checked',jQuery(this).prop("checked"));
        });
        jQuery("#bis_select_action").change(function(){
                bis_productRuleAction();
        });
        
        jQuery("#bis_check_all").click(function() {
            if (jQuery(this).is(':checked')) {
                jQuery('#bis_select_action').multiselect("enable");
            }else{
                jQuery('#bis_select_action').multiselect("disable");
            }
        });
        
        bis_getProductPageCount(0); 
        jQuery('.search-container').hide();
        jQuery(".bis-searh-box-main-container").click(function() {
        jQuery(this).find('i').toggleClass("glyphicon-menu-down glyphicon-menu-up");
            jQuery('.search-container').slideToggle('fast');
        });
        jQuery(".hasclear").keyup(function () {
            var t = jQuery(this);
            t.next('span').toggle(Boolean(t.val()));
        });

        jQuery(".bis-clearer").hide(jQuery(this).prev('input').val());

        jQuery(".bis-clearer").click(function () {
            jQuery(this).prev('input').val('').focus();
            jQuery(this).hide();
            jQuery('.bis-main-search-button').click();
        });
    });
</script>