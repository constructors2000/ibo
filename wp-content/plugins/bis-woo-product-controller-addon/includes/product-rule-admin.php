<?php
/* ######################################################################################

  Copyright (C) 2015 by Ritu.  All rights reserved.  This software
  is an unpublished work and trade secret of Ritu, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of Ritu.  Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or Ritu, then Ritu will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */


function get_all_types_product($type){
    $product_rule_model = new ProductRulesEngineModel();
    $result_map = $product_rule_model->bis_get_all_post_rules($type);
    $product_rules = $result_map["data"];
    return $product_rules;
}
function bis_add_rule_to_products($loc) {
    $type = get_post_type();
    $product_rules = get_all_types_product($type);
    if($loc === 'top' && $type != "" && $type == 'product') {       
        ?><div class="alignleft actions">
            <select name="bis_select_rule" id="bis_select_rule"> 
                <option value=''><?php _e('Select Product Rule', 'productrules'); ?></option><?php
                if(is_array($product_rules)){
                    foreach ($product_rules as $product_rule) {
                        $selected = "";
                        if ($product_rule == $product_rule->id) {
                            $selected = "selected";
                        }
                      ?><option <?php echo $selected ?> value='<?php echo $product_rule->id ?>'><?php echo $product_rule->name ?></option><?php
                    }
                }
            ?></select> 
            <button type="button" id="bis_apply_product_rule" name="apply_rule" onclick="bis_apply_rule()" class="button action">
                <img class="bis-hoverIcon" src="<?php echo plugins_url('../image/rules-engine-small-icon.png', __FILE__); ?>" />  
                    <?php _e('Apply', 'productrules'); ?>
            </button>
        </div>
        <script>
            function bis_apply_rule() {
                var productIds = [];
                var i = 0;
                jQuery('input[name="post[]"]:checked').each(function () {
                    productIds[i++] = this.value;
                });           
                var rId = jQuery("#bis_select_rule").val();
                if(productIds.length > 0 && rId !== ""){
                    jQuery.get(BISAjax.ajaxurl,
                        {
                            action: 'bis_apply_product_rule',
                            productIds: productIds,
                            product_rule: rId,
                            post_type:'product'
                        },
                        function(data){
                            if(data == 0){
                                window.location.reload();
                            }
                        }
                    );
                } else {
                    var message;
                    if(productIds.length === 0 && rId === ""){
                        message = '<?php _e("Select products and product rule.", "productrules") ?>';
                    } else {
                        if(productIds.length === 0){
                            message = '<?php _e("Select one or more products.", "productrules") ?>';
                        } else {
                            message = '<?php _e("Select product rule.", "productrules") ?>';
                        }
                    } 
                    jQuery(".subsubsub").before('<div id="bis_message" class="error notice notice-error is-dismissible"><p>' + message + '</p><button type="button" id="bis_close" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    jQuery("#bis_close").click(function(){
                        jQuery("#bis_message").replaceWith();
                    });
                }
            }
        </script><?php
    }
}

function bis_add_product_rule_applied($defaults) {
    $type = get_post_type();
    if($type === 'product') {
        $image_path = plugins_url('../image/rules-engine-small-icon.png', __FILE__);
        $defaults['rules_engine'] = "<img width='25px' height='25px' src='" . $image_path . "' />";
    }
    return $defaults;
}

function bis_is_product_rule_applied($column_name, $post_ID) {
    if ($column_name == 'rules_engine') {
        $product_rule_model = new ProductRulesEngineModel();
        $post_type = 'product';
        $rules = $product_rule_model->get_post_rule_values($post_type);
        $is_there = false;
        foreach ($rules as $rule) {
            $post_names = [];
            if ($rule->parent_id == $post_ID) {
                array_push($post_names, $rule->name);
                $is_there = true;
                break;
            }
        }
        $settings_data = RulesEngineUtil::get_option(BIS_ANALYTICS_SETTINGS, false, false);

        $sttingsArray = json_decode($settings_data, true);

        $switch = $sttingsArray[0]["enableReportsOnPostAndPage"];
        $selected_role = $sttingsArray[0]["backendPermission"];
        $current_user = wp_get_current_user();
        $roles = $current_user->roles;
        $role = array_shift($roles);
        $in_array = null;
        if ($selected_role != null) {
            $in_array = in_array($role, $selected_role);
        }
        if ($is_there) {
            ?><span class="dashicons dashicons-yes bis-hoverIcon" >  
                <span class="bis-hoverInfo"><?php
            foreach ($post_names as $key => $value) {
                echo $value;
            }
                ?></span></span> <?php
            if (($is_there == true) && (($switch == "true") && ($in_array == true))) {
                echo "|";
            }
        }

        if (($switch == "true") && ($in_array == true)) {
            if ($switch == "true") {
                ?> <a href="javascript:void(0)" id="bis_post_analytics" class="dashicons dashicons-chart-area" style="color:#527cf9;"></a> <?php
            } 
        }                   
    }
}

function bis_add_product_rules($location) {
    $type = get_post_type();
    if ('top' === $location && $type != "" && $type == 'product') {
    ?>
        <div id="bis_add_post_rule_content" class="alignleft actions">            
            <button type="button" id="bis_add_product_rule" onclick="bis_addPostRule()" class="button action">
                <img class="bis-hoverIcon" src="<?php echo plugins_url('../image/rules-engine-small-icon.png', __FILE__); ?>" />  
                        <?php _e('Add Product Rule', 'productrules'); ?>
            </button>
        </div>            
        <script>
            function bis_addPostRule() {
                var productIds = [];
                var i = 0;
                jQuery('input[name="post[]"]:checked').each(function() {
                    productIds[i++] = this.value;
                });
                if(productIds.length > 0) {
                    jQuery("#bis_add_post_rule_content").append(
                        jQuery("<form/>", {
                            id: 'bis_add_rule_form',
                            method: 'get'
                        }).append(
                            jQuery('<input/>').attr({
                                type: 'hidden',
                                value: 'bis_pg_productrules',
                                name: 'page'
                            }),
                            jQuery('<input/>').attr({
                                type: 'hidden',
                                value: 'productadd',
                                name: 'rule_action'
                            }),
                            jQuery('<input/>').attr({
                                type: 'hidden',
                                value: '<?php echo $type; ?>',
                                name: 'pos_type'
                            }),
                            jQuery('<input/>').attr({
                                type: 'hidden',
                                value: productIds,
                                name: 'productids'
                            })
                        )
                    );
                            
                    var url = '<?php echo admin_url('admin.php'); ?>';                
                    var formData = jQuery("#bis_add_rule_form").serialize();
                    var urlAction = url+'?'+formData;
                                    
                    jQuery("#bis_add_rule_form").attr('action', urlAction);
                    jQuery("#bis_add_rule_form").submit();
                } else {
                    var message = '<?php _e("Select one or more products.", "productrules") ?>';
                    jQuery(".subsubsub").before('<div id="bis_message" class="error notice notice-error is-dismissible"><p>' + message + '</p><button type="button" id="bis_close" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    jQuery("#bis_close").click(function(){
                        jQuery("#bis_message").replaceWith();
                    });
                }
            }             
        </script>
    <?php
    }
}

function bis_product_add_rule_meta_box($post_type) {
    if ($post_type !== 'page' && $post_type == 'product') {
        add_meta_box(
            'metabox_sidebar_select', __('Mega Addon : Product Controller', "productrules"), 'bis_product_add_meta_box_content', $post_type
        );
    }
}

function bis_product_add_meta_box_content() {

    wp_nonce_field('product_rules_custom_meta_box', 'product_rules_meta_box_nonce');

    global $post;
    $type = get_post_type();
    $product_rule_model = new ProductRulesEngineModel();
    $result_map = $product_rule_model->bis_get_all_post_rules($type);
    $product_rules = $result_map["data"];
    $status = get_post_status();
    ?>
        <div>
            <label for="apply_product_rule_info">
                <?php _e('Before the product is published or update, If you want to apply existing product rules for this product.', "productrules"); ?>
            </label><br/><br/>
            <b><label for="product_rules_meta_box_field">
                <?php _e('Existing Rules : ', "productrules"); ?>
            </label>
            <select name="bis_select_rule" id="bis_select_rule">
                <option><?php _e('Select Product Rules', "productrules"); ?></option> <?php
                    if (is_array($product_rules)) {
                        foreach ($product_rules as $product_rule) {
                            $selected = "";
                            if ($product_rule == $product_rule->id) {
                                $selected = "selected";
                            }
                          ?><option <?php echo $selected ?> value='<?php echo $product_rule->id ?>'><?php echo $product_rule->name ?></option><?php
                        }
                    }
          ?></select>
        </b>
        </div><br/>
        <div id="bis_add_post_rule_content">
            <label for="add_post_rule_info">
                <?php _e('After the product is published, If you want to add a new product rule for this product.', "productrules"); ?>
            </label><br/>
            <a id="bis_post_add" href="javascript:void(0)" class="hide-if-no-js taxonomy-add-new">
                + Add New Product Rule				
            </a>
        </div>
        <script>
            var status = '<?php echo $status; ?>';
            if(status === 'publish'){
                jQuery("#bis_post_add").click(function(){
                    var productId = '<?php echo get_the_ID(); ?>';
                    var productTitle = '<?php echo $name = get_the_title($post); ?>';
                    jQuery("#bis_add_post_rule_content").append(
                        jQuery("<form/>", {
                            id: 'bis_add_post_rule_form',
                            method: 'get'
                        }).append(
                            jQuery('<input/>').attr({
                                type: 'hidden',
                                value: 'bis_pg_productrules',
                                name: 'page'
                            }),
                            jQuery('<input/>').attr({
                                type: 'hidden',
                                value: 'productadd',
                                name: 'rule_action'
                            }),
                            jQuery('<input/>').attr({
                                type: 'hidden',
                                value: '<?php echo $type; ?>',
                                name: 'pos_type'
                            }),
                            jQuery('<input/>').attr({
                                type: 'hidden',
                                value: productId,
                                name: 'productids'
                            }),
                            jQuery('<input/>').attr({
                                type: 'hidden',
                                value: productTitle,
                                name: 'product_title'
                            })
                        )
                    );

                    var url = '<?php echo admin_url('admin.php'); ?>';
                    var formData = jQuery("#bis_add_post_rule_form").serialize();
                    var urlAction = url + '?' + formData;

                    jQuery("#bis_add_post_rule_form").attr('action', urlAction);
                    jQuery("#bis_add_post_rule_form").submit();
                });            
            } else {
                jQuery("#bis_post_add").click(function () {
                    var message = '<?php _e("Product Rules : After publish, you can create a new product rule for this product.", "productrules") ?>';
                    jQuery(".wp-heading-inline").after('<div id="bis_message" class="error notice notice-error is-dismissible"><p>' + message + '</p><button type="button" id="bis_close" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    jQuery("#bis_close").click(function(){
                        jQuery("#bis_message").replaceWith();
                    });
                });
            }            
        </script>
    <?php
}

function bis_apply_product_rule_from_meta_box() {
    
    global $current_screen;
    global $post;
    $type = null;
    if (isset($current_screen)) {
        $data = get_current_screen();
        $type = $data->post_type;
    }
    if($type == "product"){
        if (isset($_POST['bis_select_rule']) && $_POST['bis_select_rule'] != 0) {
            $ruleId = (int) $_POST['bis_select_rule'];
            $product_id[] = $post->ID;
            $product_rule_model = new ProductRulesEngineModel();
            $product_rule_model->applying_post_rule($product_id, $ruleId, $type);
        }
    }
}