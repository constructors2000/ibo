<?php

use bis\repf\common\RulesEngineCacheWrapper;

$nonce = $_GET['bis_nonce'];

$ruleId = $_GET['ruleId'];

if (!wp_verify_nonce($nonce, 'bis_rules_engine_nonce')) {
    RulesEngineUtil::handle_request_forgery_error();
}
$product_rule_modal = new ProductRulesEngineModel();
$product_rule = $product_rule_modal->get_post_rule($ruleId);
RulesEngineCacheWrapper::set_session_attribute(BIS_LOGICAL_RULE_ID, $product_rule->ruleId);
RulesEngineCacheWrapper::set_session_attribute(BIS_PLUGIN_TYPE_VALUE, BIS_PRODUCT_PLUGIN_ID);
$active_payment_methods = $product_rule_modal->get_active_payment_methods();
$res_product_list = $product_rule_modal->get_all_post_list("product")["data"];
$shiping_zones = $product_rule_modal->bis_get_woo_shiping_methods();

$showPopupStyle = "";

$images_list = $product_rule_modal->get_images_from_media_library();

$actions = $product_rule_modal->get_rule_values(BIS_POST_ACTION_ID);

$pos_values = $product_rule_modal->get_rule_values(BIS_POST_CONTENT_POSITION);

$pos_img_sizes = $product_rule_modal->get_rule_values(BIS_CONTENT_IMAGE_SIZE);

$rules = $product_rule_modal->get_logical_rule_names();

$type_id = $product_rule->parentTypeValue;

$redirect_url = $product_rule->general_col3;

$popup_data = json_decode($product_rule->general_col5);
// If no value for redirect url then use Home Page url as default redirect url.
if (RulesEngineUtil::isNullOrEmptyString($redirect_url)) {
    $redirect_url = get_home_url();
}

$bis_re_rule_id = $product_rule->ruleId;

$terms = $product_rule->all_terms;
$term_style = "display:none";
$term_list = null;

if ($type_id == "attributes" || $type_id == "custom_taxo") {
    $product_list = $product_rule_modal->get_all_product_taxonomies($type_id);
    $term_list_array = $product_rule_modal->get_term_list_by_slug($product_rule->ruleTypeValue[0], $type_id);
    $term_list = $term_list_array["data"];
    $term_style = "display:block";
} else if ($type_id == "categories") {
    $product_list = $product_rule_modal->get_all_product_taxonomies($type_id);
} elseif ($type_id == "woo-tags") {
    $product_list = $product_rule_modal->get_all_product_taxonomies($type_id);
} else {
    $product_list = $product_rule_modal->get_all_post_list($type_id);
}
$products = $product_list["data"];

$cont_position = "";
$image_size = "";
$image_id = "";
$image_style = "display:block";
$content_style = "display:block";
$shipping_error_des_style = "display:none";
$position_style = "display:block";
$shipping_style = "display:none";
$shipping_des_style = "display:none";
$shipping_des = "";
$payment_style = "display:none";
$res_prod_style = "display:none";
$res_prod_dec_style = "display:none";
$res_prod_dec_style_vv = "display:none";
$shipping_is = "";
$payment_methods = "";
$res_prod_ids = "";
$res_prod_dec = "";
$res_prod_dec_vv = "";
$payment_is = "";
$shipping_error_des = "";
$auto_rule = "";
$placeholder = __("Enter dynamic content, HTML tags also supported.", "rulesengine");
$content_label = __("Content Body", "rulesengine");
$post_content_help = __("This field is required and supports html tags and inline style", "rulesengine");


$post_action = $product_rule->action;

switch ($post_action) {
    case "hide_post":
        $auto_rule = $product_rule->general_col5;
        $image_style = "display:none";
        $content_style = "display:none";
        $position_style = "display:none";
        break;

    case "replace_post_content":
        $content_style = "display:block";
        $image_style = "display:none";
        $position_style = "display:none";
        break;

    case "append_image_post":
        $content_style = "display:none";
        $cont_position = json_decode($product_rule->general_col2)->content_position;
        $img_attr = json_decode($product_rule->general_col3);
        $image_size = $img_attr->image_size;
        $image_id = $img_attr->image_id;
        break;

    case "append_post_content":
        $image_style = "display:none";
        $cont_position = json_decode($product_rule->general_col2)->content_position;
        break;

    case "append_existing_scode_post":
        $image_style = "display:none";
        $position_style = "display:none";
        $content_label = __("Shortcode", "rulesengine");
        $post_content_help = __("This field is required, Use an existing Third-party shortcode", "rulesengine");
        $placeholder = __("Enter a Third-Party shortcode", "rulesengine");
        break;

    case "append_image_bg_post":
        $cont_position = json_decode($product_rule->general_col2)->content_position;
        $img_attr = json_decode($product_rule->general_col3);
        $image_size = $img_attr->image_size;
        $image_id = $img_attr->image_id;
        break;

    case "show_post":
        $image_style = "display:none";
        $content_style = "display:none";
        $position_style = "display:none";
        break;

    case "show_only_post":
        $auto_rule = $product_rule->general_col5;
        $image_style = "display:none";
        $content_style = "display:none";
        $position_style = "display:none";
        break;

    case "shipping":
        $image_style = "display:none";
        $content_style = "display:none";
        $position_style = "display:none";
        $shipping_style = "display:block";
        $shipping_des_style = "display:block";
        $shipping_error_des = $product_rule->general_col5;
        $payment_style = "display:none";
        $shipping_is = $product_rule->general_col2;
        if($shipping_is !== "charge"){
            $shipping_error_des_style = "display:block";
            $shipping_des = json_decode($product_rule->general_col3)->bis_shipping_des;
        }
        break;
    case "payment":
        $image_style = "display:none";
        $content_style = "display:none";
        $position_style = "display:none";
        $shipping_style = "display:none";
        $payment_style = "display:block";
        $payment_is = $product_rule->general_col2;
        $payment_methods = json_decode($product_rule->general_col3);
        break;
    case "restrict_prod":
    case "dependent_prod":
        $image_style = "display:none";
        $content_style = "display:none";
        $position_style = "display:none";
        $shipping_style = "display:none";
        $payment_style = "display:none";
        $res_prod_style = "display:block";
        $res_prod_dec_style = "display:block";
        $res_prod_dec_style_vv = "display:block";
        $res_prod_ids = json_decode($product_rule->general_col3);
        $res_prod_dec = $product_rule->general_col2;
        $res_prod_dec_vv = $product_rule->general_col1;
        break;
}
if (isset($product_rule->general_col2) && $post_action != "show_only_post" && $post_action != "dependent_prod" && $post_action != "restrict_prod" && $post_action != "payment" && $post_action != "shipping") {
    $cont_position = json_decode($product_rule->general_col2)->content_position;
}
if ($cont_position == "pos_dialog_post") {
    $bis_popup_style = "display:block";
} else {
    $bis_popup_style = "display:none";
}
$plugin_rule_dir = BIS_PRODUCT_RULE_DIRECTORY;
?>

<form name="bis_re_editproductruleform" id="bis_re_editproductruleform" method="POST">
    <input type="hidden" id="bis_auto_rule_str" name="bis_auto_rule_str" value="<?php echo $auto_rule; ?>">
    <div class="panel panel-default">
        <div class="panel-heading dashboard-panel-heading">
            <div class="container-fluid">
                <div class="row">
                    <h3 class="panel-title"><label><?php _e("Edit Product Rule", "productrules"); ?></label></h3>
                </div>
            </div>
        </div>
        <div class="panel-body dashboard-panel post-rules-container-edit">
            <div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label><?php _e("Rule Name", "rulesengine"); ?></label>
                        <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                        <a class="popoverData " target="_blank" href="<?php echo BIS_RULES_ENGINE ?>"
                           data-content='<?php _e("Name is required and should be unique.", "productrules"); ?>' rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                        <input type="text" class="form-control" maxlength="30"id="bis_re_product_rule" name="bis_re_product_rule"
                               value='<?php echo $product_rule->name ?>'
                               placeholder='<?php _e("Enter rule name", "rulesengine") ?>'>
                    </div>
                    <div class="form-group col-md-3">
                        <label><?php _e("Status", "rulesengine"); ?></label>
                        <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                        <div>
                            <select class="form-control" name="bis_re_rule_status" id="bis_re_rule_status"size="2">
                                <option value="1"><?php _e("Active", "rulesengine"); ?></option>
                                <option value="0"><?php _e("Deactive", "rulesengine"); ?></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6" >
                        <div class="form-group col-md-6 pr-0 pl-0">
                            <label class="d-block"><?php _e("Select Product", "productrules"); ?>  <span class="glyphicon glyphicon-asterisk bis-icon-red"></span></label>
                            <select name="bis_re_product_type" id="bis_re_product_type">
                                <optgroup label="Products" >
                                    <option value="<?php echo "product" ?>" id="bis_product_names"><?php _e("Product Name", "productrules"); ?></option>
                                    <option value="<?php echo "grouped_product" ?>" id="bis_grouped_product"><?php _e("Grouped Product", "productrules"); ?></option>
                                    <option value="<?php echo "external_product" ?>" id="bis_external_product"><?php _e("External/Affiliate Product", "productrules"); ?></option>
                                    <option value="<?php echo "variable_product" ?>" id="bis_variable_product"><?php _e("Variable Product", "productrules"); ?></option>
                                    <option value="<?php echo "attributes" ?>" id="bis_product_attributes"><?php _e("Attributes", "productrules"); ?></option>
                                    <option value="<?php echo "categories" ?>" id="bis_product_attributes"><?php _e("Categories", "productrules"); ?></option>
                                    <option value="<?php echo "woo-tags" ?>" id="bis_product_attributes"><?php _e("Woo Tags", "productrules"); ?></option>
                                    <option value="<?php echo "custom_taxo" ?>" id="bis_product_attributes"><?php _e("Custom taxonomies", "productrules"); ?></option>
                                </optgroup>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <span id="bis_re_products">
                                <label id="bis_selected_type"><?php
                                    if (RulesEngineUtil::isContains($type_id, "product")) {
                                        echo "Select Products";
                                    }elseif ($type_id == "custom_taxo") {
                                        echo "Select Taxonomy";
                                    } else {
                                        echo "Select " . ucfirst($type_id);
                                    }
                                    ?></label>
                                <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                                <div>
                                    <span id="bis_re_product_list_span">
                                        <?php
                                        $multiselect = "multiple = 'multiple'";
                                        if ($type_id == "attributes") {
                                            $multiselect = "";
                                        }
                                        ?>
                                        <select id="bis_re_product_list" <?php echo $multiselect ?> name="bis_re_product_list[]">
                                            <?php
                                            foreach ($products as $product) {
                                                if (isset($product->image)) {
                                                    ?>
                                                    <option data-img="<?php echo $product->image; ?>" value="<?php echo $product->id; ?>"> <?php echo $product->name; ?> </option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $product->id; ?>"> <?php echo $product->name; ?> </option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </span>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <span id="bis_re_attribute_terms" style="<?php echo $term_style ?>">
                            <label><?php _e("Select Terms", "productrules"); ?></label>
                            <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                            <div>
                                <span id="bis_re_term_list_span">
                                    <select id="bis_re_term_list" multiple="multiple" name="bis_re_term_list[]">
                                        <?php
                                        if ($term_list != null) {
                                            foreach ($term_list as $term) {
                                                ?>
                                                <option  value="<?php echo $term->id ?>"><?php echo $term->name ?></option>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </span>
                            </div>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label><?php _e("Action", "rulesengine"); ?></label>
                        <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                        <div>
                            <select class="form-control" name="bis_re_product_action" id="bis_re_product_action"size="2">
                                <option  value="shipping" id="shipping"><?php _e("Shipping", "productrules"); ?></option>
                                <option  value="payment" id="payment"><?php _e("Payment Methods", "productrules"); ?></option>
                                <optgroup label="<?php _e("Product", "productrules"); ?>">
                                    <?php foreach ($actions as $action) { ?>
                                        <option  value="<?php echo $action->value ?>" id="<?php echo $action->value ?>"><?php echo str_replace("Post", "Product", $action->name) ?></option>
<?php } ?>
                                    <option  value="show_only_post" id="show_only_post"><?php _e("Show Only Product", "productrules"); ?></option>
                                    <option  value="restrict_prod" id="bis_restrict_prod"><?php _e("Restrict Product", "productrules"); ?></option>
                                    <option  value="dependent_prod" id="bis_dependent_prod"><?php _e("Dependent Product", "productrules"); ?></option>
                                </optgroup>    
                            </select>
                        </div>
                    </div>                    
                    <div class="col-md-3">
                        <span class="form-group bis-content-pos" style="<?php echo $position_style ?>">
                            <label for="description"><?php _e("Location", "productrules"); ?></label>
                            <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                            <div>
                                <select name="bis_re_cont_pos" id="bis_re_cont_pos"size="2">
                                    <?php foreach ($pos_values as $pos_value) { ?>
                                        <option value="<?php echo $pos_value->value ?>"id="<?php echo $pos_value->value ?>"><?php echo str_replace("Post", "Product", $pos_value->name); ?></option>
<?php } ?>
                                </select>
                            </div>
                        </span>
                        <span class="form-group bis-shipping-is" style="<?php echo $shipping_style ?>">
                            <label for="description"><?php _e("Shipping", "productrules"); ?></label>
                            <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                            <div>
                                <select name="bis_shipping_is" id="bis_shipping_is"
                                        size="2">
                                    <option  value="charge"><?php _e("Charge", "productrules"); ?></option>  
                                    <option  value="enable"><?php _e("Enable", "productrules"); ?></option>  
                                    <option  value="disable"><?php _e("Disable", "productrules"); ?></option>  
                                </select>

                            </div>
                        </span>
                        <span class="form-group bis-payment-method" style="<?php echo $payment_style ?>">
                            <label for="Payment method"><?php _e("Payment Methods", "productrules"); ?></label>
                            <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                            <div>
                                <select name="bis_payment_method[]" multiple="multiple" id="bis_payment_method"
                                        size="2">
                                            <?php
                                            if (!empty($active_payment_methods)) {
                                                foreach ($active_payment_methods as $active_payment_method) {
                                                    ?>
                                            <option  value="<?php echo $active_payment_method['id'] ?>"><?php echo $active_payment_method['name'] ?></option>  
                                            <?php
                                        }
                                    }
                                    ?>  
                                </select>

                            </div>
                        </span>
                        <span class="form-group bis-prod-restrict" style="<?php echo $res_prod_style ?>">
                            <label id="bis_depen_rest" for="description"><?php _e("Restrict product", "productrules"); ?></label>
                            <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                            <div>
                                <select id="bis_restrict_product_list" multiple="multiple" name="bis_restrict_product_list[]">
                                    <?php
                                    foreach ($res_product_list as $product) {
                                        if (isset($product->image)) {
                                            ?>
                                            <option data-img="<?php echo $product->image; ?>" value="<?php echo $product->id; ?>"> <?php echo $product->name; ?> </option>
                                        <?php } else { ?>
                                            <option value="<?php echo $product->id; ?>"> <?php echo $product->name; ?> </option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </span>
                    </div>
                    <div class="col-md-3 bis-payment-is" style="<?php echo $payment_style ?>">
                        <label for="payment is"><?php _e("Payment", "rulesengine"); ?></label>
                        <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                        <div>
                            <select name="bis_payment_is" id="bis_payment_is" size="2">
                                <option  value="enable"><?php _e("Enable", "productrules"); ?></option>  
                                <option  value="disable"><?php _e("Disable", "productrules"); ?></option>  
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6 bis-shipping-charge-div">
                        <div class="row">
                            <div class="col-md-6 bis_shipping_methods_div" style="display: none">
                                <label for="shipping methods"><?php _e("Shipping Methods", "productrules"); ?></label>
                                <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                                <div>
                                    <select name="bis_shipping_methods[]" multiple="multiple" id="bis_shipping_methods"
                                            size="2">
                                                <?php
                                                if (!empty($shiping_zones)) {
                                                    foreach ($shiping_zones as $shipping_zone) {
                                                        if (isset($shipping_zone['zone_title'])) {?>
                                                <optgroup label="<?php echo $shipping_zone['zone_title']; ?>" >
                                                    <?php
                                                    if (!empty($shipping_zone['shipping_methods'])) {
                                                        foreach ($shipping_zone['shipping_methods'] as $shipping_method) {
                                                            ?>
                                                    <option value = "<?php echo $shipping_zone['zone_id'] . '_' . $shipping_method['id'] ?>"><?php echo $shipping_method['method_title'] ?></option>  
                                                        <?php }
                                                    }
                                                    ?>
                                                </optgroup>  <?php
                                                }
                                                    }
                                            }
                                            ?>  
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-6 bis_shipping_method_charge_div" style="display: none">
                                <label><?php _e("Shipping Charge", "productrules"); ?></label>
                                <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                                <a class="popoverData" target="_blank" href="<?php echo BIS_RULES_ENGINE ?>"
                                   data-content='<?php _e("This entered shipping charge will replace with selected method.", "productrules"); ?>' rel="popover"
                                   data-placement="bottom" data-trigger="hover">
                                    <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                                </a>
                                <input type="number" class="form-control" id="bis_product_shipping_charge" name="bis_product_shipping_charge" placeholder="0.0" style="height:35px !important;">
                            </div>
                            <span class="col-md-6 bis-shipping-des" style="<?php echo $shipping_des_style ?>">
                                <label><?php _e("Shipping Description", "rulesengine"); ?></label>
                                <a class="popoverData " target="_blank" href="http://docs.megaedzee.com/wp-content/uploads/2018/06/shipping-description.png"
                                   data-content='<?php _e("This description shown below for the selected products.", "rulesengine"); ?>' rel="popover"
                                   data-placement="bottom" data-trigger="hover">
                                    <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                                </a>
                                <input type="text" class="form-control" maxlength="500"
                                       id="bis_shipping_des" name="bis_shipping_des" value="<?php _e($shipping_des, "productrules"); ?>"
                                       placeholder='<?php _e("Enter shipping description", "productrules"); ?>'>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <span class="form-group bis-content-image" style="<?php echo $image_style ?>">
                            <label for="description"><?php _e("Image", "productrules"); ?></label>
                            <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                            <div>
                                <select name="bis_re_image_id" id="bis_re_image_id" size="2">
                                    <?php foreach ($images_list as $image) { ?>
                                        <option data-img="<?php echo $image->image; ?>" value="<?php echo $image->id; ?>" id="<?php echo $image->id; ?>"><?php echo $image->name; ?></option>
<?php } ?>
                                </select>
                            </div>
                        </span>
                    </div>
                    <div class="form-group col-md-3">
                        <span class="form-group bis-content-image" style="<?php echo $image_style ?>">
                            <label><?php _e("Image Size", "productrules"); ?></label>
                            <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                            <div>
                                <select class="form-control" name="bis_re_cont_img_size" id="bis_re_cont_img_size" size="2">
<?php foreach ($pos_img_sizes as $img_size) { ?>
                                        <option value="<?php echo $img_size->value ?>" id="<?php echo $img_size->value ?>"><?php echo $img_size->name ?>
                                        </option>
<?php } ?>
                                </select>
                            </div>
                        </span>
                    </div>
                </div>
                <div class="form-group col-md-9 pr-0 pl-0">
                    <span class="bis-content-span" style="<?php echo $content_style ?>">
                        <label id="bis_re_pg_dc_label" for="description"><?php echo $content_label ?></label>
                        <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                        <a class="popoverData " target="_blank" href="<?php echo BIS_RULES_ENGINE ?>"
                           data-content="<?php echo $post_content_help ?>" rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                        <textarea rows="2" maxlength="1000" class="form-control" name="bis_re_product_dc_content"
                                  id="bis_re_product_dc_content"
                                  placeholder='<?php _e("Enter dynamic content, HTML tags also supported.", "rulesengine") ?>'><?php echo stripslashes($product_rule->general_col1); ?></textarea>
                    </span>
                    <span class="bis-shipping-error-des-div" style="<?php echo $shipping_error_des_style ?>">
                        <label id="bis_shipping_error_des_label" for="Error description"><?php _e("Error message", "productrules"); ?></label>
                        <a class="popoverData " target="_blank" href="http://docs.megaedzee.com/wp-content/uploads/2018/06/error-message1.png"
                           data-content='<?php _e("This field is shown on checkout page while performing checkout process.", "rulesengine"); ?>' rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                        <textarea maxlength="1000" rows="2" class="form-control" name="bis_shipping_error_des"
                                  id="bis_shipping_error_des"
                                  placeholder='<?php _e("Enter message for showing an error message on checkout page.", "rulesengine") ?>'><?php echo $shipping_error_des ?></textarea>
                    </span>
                    <span class="bis-restrict-error-div" style="<?php echo $res_prod_dec_style ?>">
                        <label id="bis_restrict_error_des_label" for="Restrict description"><?php _e("Restrict message", "productrules"); ?></label>
                        <a class="popoverData " target="_blank" href="http://docs.megaedzee.com/wp-content/uploads/2018/06/error-message1.png"
                           data-content='<?php _e("This field is shown on product while adding product to cart.", "rulesengine"); ?>' rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                        <textarea maxlength="1000" rows="2" class="form-control" name="bis_restrict_error"
                                  id="bis_restrict_error"
                                  placeholder='<?php _e("Enter restrict message.", "rulesengine") ?>'><?php echo $res_prod_dec ?></textarea>
                    </span>
                    <span class="bis-restrict-error-div-vv" style="<?php echo $res_prod_dec_style_vv ?>">
                        <label id="bis_restrict_error_des_label_vv" for="Restrict vice versa description"><?php _e("Restrict vice versa message", "productrules"); ?></label>
                        <a class="popoverData " target="_blank" href="http://docs.megaedzee.com/wp-content/uploads/2018/06/error-message1.png"
                           data-content='<?php _e("This field is shown on product while adding product to cart.", "rulesengine"); ?>' rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                        <textarea maxlength="1000" rows="2" class="form-control" name="bis_restrict_error_vv"
                                  id="bis_restrict_error_vv"
                                  placeholder='<?php _e("Enter restrict message.", "rulesengine") ?>'><?php echo $res_prod_dec_vv ?></textarea>
                    </span>
                </div>
                <div class="row">
                    <div class="form-group col-md-9 pr-0">
                        <label><?php _e("Description", "rulesengine"); ?></label>
                        <textarea rows="2" maxlength="500" class="form-control" name="bis_re_product_description"
                                  id="bis_re_product_description"
                                  placeholder='<?php _e("Enter product rule description", "rulesengine"); ?>'><?php echo stripslashes($product_rule->description); ?></textarea>
                    </div>
                </div>
                <span class="bis-common-popup-span" style="<?php echo $bis_popup_style ?>">
<?php include_once(ABSPATH . "wp-content/plugins/" . BIS_PRODUCT_RULESENGINE_PLATFORM . "/includes/bis-popup-configuration.php"); ?>  
                </span>
            </div>
        </div>
        <input type="hidden" id="bis_re_product_rule_id" name="bis_re_product_rule_id" value="<?php echo $bis_re_rule_id; ?>"/>
        <input type="hidden" id="bis_re_edit_detail_id" name="bis_re_edit_detail_id" value="<?php echo $ruleId; ?>"/>
    </div>
<?php include_once(ABSPATH . "wp-content/plugins/" . BIS_PRODUCT_RULESENGINE_PLATFORM . "/includes/logical-rules-parent-edit.php"); ?>
    <div class="row">
        <div class="form-group col-md-5">
            <button type="button" id="bis_re_show_product_rules_list" class="btn btn-primary">
                <i class="glyphicon glyphicon-chevron-left bis-glyphi-position"></i><?php _e("Go Back", "rulesengine"); ?>
            </button>
        </div>
        <div class="form-group col-md-2 col-md-offset-5" align="right">
            <button id="bis_re_product_rule_update"  type="button" class="btn btn-primary">
<?php _e(" Update Rule", "rulesengine"); ?>
            </button>
        </div>
    </div>
    <input type="hidden" id="bis_re_pr_add" name="bis_re_pr_add" value="true" /> 
</form>
<script>

    // wait for the DOM to be loaded
    jQuery(document).ready(function () {

        jQuery('.popoverData').popover();

        jQuery("#bis_re_cont_pos").change(function () {
            if (jQuery("#bis_re_cont_pos").val() === "pos_dialog_post") {
                jQuery(".bis-common-popup-span").show();
            } else {
                jQuery(".bis-common-popup-span").hide();
            }
        });

        function bis_loadProducts(productId) {

            var includeSelectAllOption, is_multiselect, selectOption, ntg_found;
            jQuery('#bis_re_attribute_terms').hide();
            switch (productId) {
                case "attributes" :
                    includeSelectAllOption = false;
                    is_multiselect = "bis-selectComponent";
                    selectOption = "Select Attribute";
                    ntg_found = "No attributes found";
                    break;
                case "categories" :
                    includeSelectAllOption = true;
                    is_multiselect = "bis-selectComponent-multiple";
                    selectOption = "Select Category";
                    ntg_found = "No categories found";
                    break;
                case "woo-tags" :
                    includeSelectAllOption = true;
                    is_multiselect = "bis-selectComponent-multiple";
                    selectOption = "Select Tag";
                    ntg_found = "No tags found";
                    break;
                case "custom_taxo" :
                    includeSelectAllOption = true;
                    is_multiselect = "bis-selectComponent-multiple";
                    selectOption = "Select Taxonomy";
                    ntg_found = "No taxonomies found";
                    break;
                default :
                    includeSelectAllOption = true;
                    is_multiselect = "bis-selectComponent-multiple";
                    selectOption = "Select Products";
                    ntg_found = "No products found";
            }
            jQuery("#bis_selected_type").text(selectOption);
            var jqXHR = jQuery.get(ajaxurl,
                    {
                        action: "bis_re_get_product_list",
                        bis_nonce: BISAjax.bis_rules_engine_nonce,
                        bis_type_id: productId
                    });

            jqXHR.done(function (response) {
                if (response["status"] === "success") {
                    var data = {
                        compId: 'bis_re_product_list',
                        compName: 'bis_re_product_list[]',
                        suboptions: response['data']
                    };

                    var source = jQuery("#" + is_multiselect).html();
                    var template = Handlebars.compile(source);
                    var producttlistContent = template(data);
                    var subOptObj = jQuery("#bis_re_product_list_span");

                    if (!response['data']) {
                        producttlistContent = "<span style='color:red'>" + ntg_found + "</span>";
                    }

                    subOptObj.html(producttlistContent);

                    jQuery('#bis_re_product_list').multiselect({
                        includeSelectAllOption: includeSelectAllOption,
                        enableCaseInsensitiveFiltering: true,
                        maxHeight: 300,
                        selectedClass: null,
                        enableHTML: true,
                        optionLabel: function (element) {
                            var url = jQuery(element).attr('data-img');
                            if (url && url.indexOf("http") > -1) {
                                return jQuery(element).text();
                            } else {
                                return jQuery(element).text();
                            }
                        },
                        nonSelectedText: selectOption,
                        buttonWidth: '250px',
                        onChange: function (element, checked) {
                            if (productId === "attributes" || productId ==="custom_taxo") {
                                var action = element[0].value;
                                get_all_term_list(action, productId);
                            }
                        }
                    });
                }
            });
        }

        jQuery('#bis_re_product_list').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            maxHeight: 300,
            selectedClass: null,
            enableHTML: true,
            optionLabel: function (element) {
                var url = jQuery(element).attr('data-img');
                if (url && url.indexOf("http") > -1) {
                    return jQuery(element).text();
                } else {
                    return jQuery(element).text();
                }
            },
            nonSelectedText: '<?php _e("Select Product", "productrules"); ?>',
            buttonWidth: '250px',
            onChange: function (element, checked) {
                var prod_type = "<?php echo $type_id; ?>";
                if (prod_type === "attributes" || prod_type === "custom_taxo") {
                    var action = element[0].value;
                    get_all_term_list(action, prod_type);
                }
            }
        });

        jQuery("#bis_re_product_type").change(function () {
            bis_loadProducts(this.value);
        });

        jQuery("#bis_re_product_rule_update").click(function () {
            var ruleName = jQuery.trim(jQuery("#bis_re_product_rule").val());
            jQuery("#bis_re_name").val(ruleName + " Product");
            jQuery("#bis_re_description").val(jQuery.trim(jQuery("#bis_re_product_description").val()));
            if (jQuery(".bis-query-builder").is(':checked')) {
                var builder = jQuery('#bis-rule-builder').queryBuilder('getRules');
                jQuery("#bis_jquery_result").val(JSON.stringify(builder));
            }
            jQuery("#bis_re_editproductruleform").submit();
            jQuery(".pagination").bootpag({page: 1});
            jQuery('#bis_select_action').multiselect("disable");
        });

        jQuery("#bis_re_show_product_rules_list").click(function () {
            bis_showProductRulesList();
            jQuery(".pagination").bootpag({page: 1});
            jQuery('#bis_select_action').multiselect("disable");
        });

        var options = {
            success: showResponse,
            beforeSend: bis_before_request_send,
            url: BISAjax.ajaxurl,
            beforeSubmit: bis_validateAddProductRulesForm,

            data: {
                action: 'bis_re_update_next_product_rule',
                bis_nonce: BISAjax.bis_rules_engine_nonce
            }
        };

        jQuery('#bis_re_editproductruleform').ajaxForm(options);

        function showResponse(responseText, statusText, xhr, $form) {

            if (responseText["status"] === "error") {
                bis_request_complete();
                if (responseText["message_key"] === "duplicate_entry") {
                    bis_showErrorMessage('<?php _e("Duplicate Product Rule name, Product Rule exists with same name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                } else if (responseText["message_key"] === "no_shortcode_found") {
                    bis_showErrorMessage('<?php _e("Shortcode not found, Please enter an existing Shortcode.", BIS_RULES_ENGINE_TEXT_DOMAIN) ?>');
                } else if (responseText["message_key"] === "invalid_shortcode") {
                    bis_showErrorMessage('<?php _e("Invalid Shortcode, Please enter a valid Shortcode.", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                } else if (responseText["message_key"] === "no_method_found") {
                    bis_showErrorMessage('<?php _e("Action hook method does not exist, Please define method with name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'
                            + " \"" + jQuery("#bis_re_hook").val() + "\".");
                } else {
                    bis_showErrorMessage('<?php _e("Error occurred while creating Product rule", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>');
                }
            } else {
                bis_getProductPageCount(0);
            }
        }

        jQuery('.bis-multiselect').multiselect();

        jQuery('#bis_re_image_id').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            maxHeight: 250,
            selectedClass: null,
            enableHTML: true,
            optionLabel: function (element) {
                return jQuery(element).text();
            },
            nonSelectedText: '<?php _e("Select image", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>',
            buttonWidth: '250px'
        });

        jQuery('#bis_re_cont_img_size').multiselect({
            selectedClass: null,
            nonSelectedText: '<?php _e("Select size", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>',
            buttonWidth: '300px'
        });

        jQuery('#bis_shipping_is').change(function () {
            bis_controll_shipping_methods(this.value);
        });

        jQuery('#bis_shipping_is').multiselect({
            maxHeight: 300,
            selectedClass: null,
            buttonWidth: '250px'
        });

        jQuery('#bis_payment_is').multiselect({
            maxHeight: 300,
            selectedClass: null,
            buttonWidth: '250px'
        });

        jQuery('#bis_payment_method').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            maxHeight: 300,
            selectedClass: null,
            buttonWidth: '250px'
        });
        
        jQuery('#bis_restrict_product_list').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            maxHeight: 300,
            selectedClass: null,
            buttonWidth: '250px'
        });
        
        jQuery('#bis_shipping_methods').multiselect({
            enableCaseInsensitiveFiltering: true,
            maxHeight: 300,
            nonSelectedText: '<?php _e("Select methods", "productrules") ?>',
            buttonWidth: '250px'
        });
        
        jQuery('#bis_re_product_type').multiselect({
            enableCaseInsensitiveFiltering: true,
            maxHeight: 300,
            nonSelectedText: '<?php _e("Select Type", "rulesengine") ?>',
            buttonWidth: '250px'
        });

        jQuery('#bis_re_cont_pos').multiselect({
            maxHeight: 300,
            selectedClass: null,
            nonSelectedText: '<?php _e("Select location", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>',
            buttonWidth: '250px'
        });

        jQuery('#bis_re_product_action').multiselect({
            nonSelectedText: '<?php _e("Select action", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>',
            maxHeight: 300,
            buttonWidth: '250px',
            onChange: function (element, checked) {
                var action = element[0].value;
                var position = jQuery("#bis_re_cont_pos").val();
                bis_show_product_view_controls(action, position, "");
            }
        });

        jQuery('#bis_re_rule_status').multiselect({
            nonSelectedText: '<?php _e("Select status", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>',
            buttonWidth: '250px'
        });

        var product_action = '<?php echo $product_rule->action; ?>';

        var selectedType = jQuery("#bis_re_product_type").val();
        jQuery("#bis_re_product_type").multiselect('deselect', selectedType);
        jQuery("#bis_re_product_type").multiselect('select', '<?php echo $type_id; ?>');
        jQuery("#bis_re_cont_pos").multiselect('select', '<?php echo $cont_position ?>');
        jQuery("#bis_re_edit_rule_id").multiselect('select', '<?php echo $product_rule->ruleId; ?>');
        jQuery("#bis_re_product_action").multiselect('select', product_action);
        jQuery("#bis_re_rule_status").multiselect('select', '<?php echo $product_rule->status; ?>');

        if (product_action === "shipping") {
            jQuery("#bis_shipping_is").multiselect('select', '<?php echo $shipping_is ?>');
            var bis_shipping_charge_array = jQuery.parseJSON('<?php echo $product_rule->general_col3; ?>');
            if ('<?php echo $shipping_is ?>' === 'charge') {
                jQuery('#bis_product_shipping_charge').val(bis_shipping_charge_array['shipping_charge']);
            }
            jQuery.each(bis_shipping_charge_array['shipping_methods'], function(key, value){
                jQuery('#bis_shipping_methods').multiselect('select', value);
            });
            bis_controll_shipping_methods('<?php echo $shipping_is ?>');
        } else if (product_action === "payment") {
            jQuery("#bis_payment_is").multiselect('select', '<?php echo $payment_is ?>');

            <?php if ($payment_methods !== "") {
                foreach ($payment_methods as $payment_method) {
                    ?>
                    jQuery("#bis_payment_method").multiselect('select', '<?php echo $payment_method; ?>');
                <?php }
            }
            ?>
        } else if (product_action === "restrict_prod" || product_action === "dependent_prod") {
             jQuery("#bis_restrict_error_vv").val("<?php echo $res_prod_dec_vv; ?>");
             jQuery("#bis_restrict_error").val("<?php echo $res_prod_dec; ?>");
            <?php if ($res_prod_ids !== "") {
                foreach ($res_prod_ids as $res_prod_id) {
                    ?>
                                jQuery("#bis_restrict_product_list").multiselect('select', '<?php echo $res_prod_id; ?>');
                <?php }
            }
            ?>
        }

        if (product_action === 'append_image_post' || product_action === 'append_image_bg_post') {
            jQuery("#bis_re_image_id").multiselect('select', '<?php echo $image_id; ?>');
            jQuery("#bis_re_cont_img_size").multiselect('select', '<?php echo $image_size; ?>');
        }

        if ("<?php echo $type_id; ?>" === "attributes" || "<?php echo $type_id; ?>" === "custom_taxo") {
            var selectedAction = jQuery("#bis_re_product_list option:selected").val();
            jQuery("#bis_re_product_list").multiselect('deselect', selectedAction);
            <?php
            if ($type_id == "attributes" || $type_id == "custom_taxo") {
                $selected_prod_list = $product_rule->ruleTypeValue;
                ?>
                jQuery("#bis_re_product_list").multiselect('select', '<?php echo $selected_prod_list[0] ?>');
            <?php } ?>
        } else if ("<?php echo $type_id; ?>" === "categories") {

                <?php
                if ($type_id == "categories") {
                    $selected_cat_list = json_decode($product_rule->general_col4);
                    if (is_array($selected_cat_list)) {
                        foreach ($selected_cat_list as $selected_cat) {
                            ?>
                            jQuery("#bis_re_product_list").multiselect('select', '<?php echo $selected_cat ?>');
                            <?php
                        }
                    }
                }
                ?>
        } else {
            <?php
            if ($type_id !== "categories" && $type_id !== "attributes" && $type_id !== "custom_taxo") {
                $selected_prod_list = $product_rule->ruleTypeValue;
                if (is_array($selected_prod_list)) {
                    foreach ($selected_prod_list as $selected_prod) {
                        ?>
                        jQuery("#bis_re_product_list").multiselect('select', '<?php echo $selected_prod->value ?>');
                        <?php
                    }
                }
            }
            ?>
        }
        bis_show_product_view_controls(product_action, "", "func-from-onload-edit");
        jQuery('#bis_re_term_list').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            maxHeight: 300,
            nonSelectedText: '<?php _e("Select Term", "rulesengine"); ?>',
            buttonWidth: '250px'
        });
        
        function get_all_term_list(term, productId) {
            var jqXHR = jQuery.get(ajaxurl,
                    {
                        action: "bis_re_get_term_list_product",
                        bis_nonce: BISAjax.bis_rules_engine_nonce,
                        bis_term_id: term,
                        productId: productId
                    });
            jqXHR.done(function (response) {
                if (response["status"] === "success") {
                    var data = {
                        compId: 'bis_re_term_list',
                        compName: 'bis_re_term_list[]',
                        suboptions: response['data']
                    };

                    var source = jQuery("#bis-selectComponent-multiple").html();
                    var template = Handlebars.compile(source);
                    var termlistContent = template(data);
                    var subOptObj = jQuery("#bis_re_term_list_span");
                }

                if (!response['data']) {
                    termlistContent = "<span style='color:red'>No Terms found</span>";
                }
                subOptObj.html(termlistContent);

                jQuery('#bis_re_term_list').multiselect({
                    includeSelectAllOption: true,
                    enableCaseInsensitiveFiltering: true,
                    maxHeight: 300,
                    selectedClass: null,
                    enableHTML: true,
                    optionLabel: function (element) {
                        return jQuery(element).text();
                    },
                    nonSelectedText: '<?php _e('Select Term', "rulesengine"); ?>',
                    buttonWidth: '250px',
                });
            });
            jQuery('#bis_re_attribute_terms').show();
        }

        var selected_terms = jQuery.parseJSON('<?php echo json_encode($product_rule->all_terms); ?>');
        jQuery.each(selected_terms, function (index, data) {
            jQuery("#bis_re_term_list").multiselect('select', data.value);
        });


    });
</script>