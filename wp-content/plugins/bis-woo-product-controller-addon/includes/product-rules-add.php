<?php
use bis\repf\common\RulesEngineCacheWrapper;

$product_values = RulesEngineCacheWrapper::get_session_attribute(BIS_ADD_PRODUCT_RULE);
RulesEngineCacheWrapper::remove_session_attribute(BIS_ADD_PRODUCT_RULE);
RulesEngineCacheWrapper::set_session_attribute(BIS_PLUGIN_TYPE_VALUE, BIS_PRODUCT_PLUGIN_ID);

$post_type = "product";
$rule_action = "";
$product_ids = "";
$product_title = "";
if (isset($product_values)) {
    $rule_action = $product_values[BIS_PRODUCT_RULE_ACTION];
    $post_type = $product_values[BIS_PRODUCT_TYPE];
    $productids = $product_values[BIS_PRODUCT_IDS];
    $product_ids = explode(",", $productids);
    $product_title = $product_values[BIS_PRODUCT_TITLE];
}

$countryCheckbox = "";
$showPopupStyle = "";
$popUpVO = null;
$product_rule_modal = new ProductRulesEngineModel();
$product_list = $product_rule_modal->get_all_post_list($post_type);
$products = $product_list["data"];
$popup_data = null;
$plugin_rule_dir = BIS_PRODUCT_RULE_DIRECTORY;
$active_payment_methods = $product_rule_modal->get_active_payment_methods();
$shiping_zones = $product_rule_modal->bis_get_woo_shiping_methods();
?>
<form name="bis_re_addproductruleform" id="bis_re_addproductruleform" method="POST">
    <div class="panel panel-default">
        <div class="panel-heading dashboard-panel-heading">
            <div class="container-fluid">
                <div class="row">
                    <h3 class="panel-title"><label><?php _e("New Product Rule", "productrules"); ?></label></h3>
                </div>
            </div>
        </div>
        <div class="panel-body dashboard-panel post-rules-container-edit">
            <div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label><?php _e("Rule Name", "rulesengine"); ?></label>
                        <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                        <a class="popoverData" target="_blank" href="<?php echo BIS_RULES_ENGINE ?>"
                           data-content='<?php _e("Name is required and should be unique.", "productrules"); ?>' rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                        <input type="text" class="form-control" maxlength="30"
                               id="bis_re_product_rule" name="bis_re_product_rule"
                               placeholder='<?php _e("Enter product rule name", "productrules"); ?>'>
                    </div>

                    <?php
                    $images_list = $product_rule_modal->get_images_from_media_library();

                    $actions = $product_rule_modal->get_rule_values(BIS_POST_ACTION_ID);

                    $pos_values = $product_rule_modal->get_rule_values(BIS_POST_CONTENT_POSITION);

                    $pos_img_sizes = $product_rule_modal->get_rule_values(BIS_CONTENT_IMAGE_SIZE);

                    $image_style = "display:block";
                    $content_style = "display:block";
                    $position_style = "display:block";
                    ?>
                    <div class="form-group col-md-3">
                        <label><?php _e("Status", "rulesengine"); ?></label>
                        <div>
                            <select name="bis_re_rule_status" id="bis_re_rule_status">
                                <option value="1"><?php _e("Active", "rulesengine"); ?></option>
                                <option value="0"><?php _e("Deactive", "rulesengine"); ?></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group col-md-6 pr-0 pl-0">
                            <label><?php _e("Select Product", "productrules"); ?></label>
                            <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                            <select name="bis_re_product_type" id="bis_re_product_type">  <optgroup label="Products" >
                                    <option value="<?php echo "product" ?>" id="bis_product_names"><?php _e("Product Name", "productrules"); ?></option>
                                    <option value="<?php echo "grouped_product" ?>" id="bis_grouped_product"><?php _e("Grouped Product", "productrules"); ?></option>
                                    <option value="<?php echo "external_product" ?>" id="bis_external_product"><?php _e("External/Affiliate Product", "productrules"); ?></option>
                                    <option value="<?php echo "variable_product" ?>" id="bis_variable_product"><?php _e("Variable Product", "productrules"); ?></option>
                                    <option value="<?php echo "attributes" ?>" id="bis_product_attributes"><?php _e("Attributes", "productrules"); ?></option>
                                    <option value="<?php echo "categories" ?>" id="bis_product_attributes"><?php _e("Categories", "productrules"); ?></option>
                                    <option value="<?php echo "woo-tags" ?>" id="bis_product_attributes"><?php _e("Woo Tags", "productrules"); ?></option>
                                    <option value="<?php echo "custom_taxo" ?>" id="bis_product_attributes"><?php _e("Custom taxonomies", "productrules"); ?></option>
                                </optgroup>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <span id="bis_re_products">
                                <label id="bis_selected_type"><?php _e("Select Products", "productrules"); ?></label>
                                <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                                <div>
                                    <span id="bis_re_product_list_span">
                                        <select id="bis_re_product_list" multiple="multiple" name="bis_re_product_list[]">
                                            <?php
                                            foreach ($products as $product) {
                                                if (isset($product->image)) {
                                                    ?>
                                                    <option data-img="<?php echo $product->image; ?>" value="<?php echo $product->id; ?>"> <?php echo $product->name; ?> </option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $product->id; ?>"> <?php echo $product->name; ?> </option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </span>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <span id="bis_re_attribute_terms" style="display:none">
                            <label><?php _e("Select Terms", "productrules"); ?></label>
                            <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                            <div>
                                <span id="bis_re_term_list_span">
                                    <select id="bis_re_term_list" multiple="multiple" name="bis_re_term_list[]">

                                    </select>
                                </span>
                            </div>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="select Action"><?php _e("Select Action", "rulesengine"); ?></label>
                        <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                        <div>
                            <select name="bis_re_product_action" id="bis_re_product_action" size="2">
                                <option  value="shipping" id="shipping"><?php _e("Shipping", "productrules"); ?></option>
                                <option  value="payment" id="payment"><?php _e("Payment Methods", "productrules"); ?></option>
                                <optgroup label="<?php _e("Product", "productrules"); ?>">
                                    <?php foreach ($actions as $action) { ?>

                                        <option  value="<?php echo $action->value ?>" id="<?php echo $action->value ?>"> <?php echo str_replace("Post", "Product", $action->name); ?></option>
                                    <?php } ?>
                                    <option  value="show_only_post" id="show_only_post"><?php _e("Show Only Product", "productrules"); ?></option>
                                    <option  value="restrict_prod" id="bis_restrict_prod"><?php _e("Restrict Product", "productrules"); ?></option>
                                    <option  value="dependent_prod" id="bis_dependent_prod"><?php _e("Dependent Product", "productrules"); ?></option>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <span class="form-group bis-content-pos" style="display: none">
                            <label for="description"><?php _e("Location", "productrules"); ?></label>
                            <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                            <div>
                                <select name="bis_re_cont_pos" id="bis_re_cont_pos"
                                        size="2">
                                            <?php foreach ($pos_values as $pos_value) { ?>

                                        <option  value="<?php echo $pos_value->value ?>"
                                                 id="<?php echo $pos_value->value ?>">
                                                     <?php echo str_replace("Post", "Product", $pos_value->name); ?>
                                        </option>
                                    <?php } ?>
                                </select>

                            </div>
                        </span>
                        <span class="form-group bis-shipping-is" style="display: none">
                            <label for="description"><?php _e("Shipping", "productrules"); ?></label>
                            <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                            <div>
                                <select name="bis_shipping_is" id="bis_shipping_is"
                                        size="2">
                                    <option  value="charge"><?php _e("Charge", "productrules"); ?></option>  
                                    <option  value="enable"><?php _e("Enable", "productrules"); ?></option>  
                                    <option  value="disable"><?php _e("Disable", "productrules"); ?></option>  
                                </select>

                            </div>
                        </span>
                        <span class="form-group bis-prod-restrict" style="display: none">
                            <label id="bis_depen_rest" for="description"><?php _e("Restrict products", "productrules"); ?></label>
                            <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                            <div>
                                <select id="bis_restrict_product_list" multiple="multiple" name="bis_restrict_product_list[]">
                                    <?php
                                    foreach ($products as $product) {
                                        if (isset($product->image)) {
                                            ?>
                                            <option data-img="<?php echo $product->image; ?>" value="<?php echo $product->id; ?>"> <?php echo $product->name; ?> </option>
                                        <?php } else { ?>
                                            <option value="<?php echo $product->id; ?>"> <?php echo $product->name; ?> </option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </span>
                        <span class="form-group bis-payment-method" style="display: none">
                            <label for="Payment method"><?php _e("Payment Methods", "productrules"); ?></label>
                            <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                            <div>
                                <select name="bis_payment_method[]" multiple="multiple" id="bis_payment_method"
                                        size="2">
                                            <?php
                                            if (!empty($active_payment_methods)) {
                                                foreach ($active_payment_methods as $active_payment_method) {
                                                    ?>
                                            <option  value="<?php echo $active_payment_method['id'] ?>"><?php echo $active_payment_method['name'] ?></option>  
                                            <?php
                                        }
                                    }
                                    ?>  
                                </select>

                            </div>
                        </span>
                    </div>
                    <div class="form-group col-md-3 bis-payment-is" style="display:none">
                        <label for="payment is"><?php _e("Payment", "rulesengine"); ?></label>
                        <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                        <div>
                            <select name="bis_payment_is" id="bis_payment_is" size="2">
                                <option  value="enable"><?php _e("Enable", "productrules"); ?></option>  
                                <option  value="disable"><?php _e("Disable", "productrules"); ?></option>  
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6 bis-shipping-charge-div">
                        <div class="row">
                            <div class="col-md-6 bis_shipping_methods_div" style="display:none">
                                <label for="shipping methods"><?php _e("Shipping Methods", "productrules"); ?></label>
                                <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                                <div>
                                    <select name="bis_shipping_methods[]" multiple="multiple" id="bis_shipping_methods"
                                            size="2">
                                                <?php
                                                if (!empty($shiping_zones)) {
                                                    foreach ($shiping_zones as $shipping_zone) { 
                                                        if(isset($shipping_zone['zone_title'])){?>
                                        <optgroup label="<?php echo $shipping_zone['zone_title']; ?>" >
                                             <?php
                                                       if (!empty($shipping_zone['shipping_methods'])) { 
                                                                    foreach ($shipping_zone['shipping_methods'] as $shipping_method) {  ?>
                                            <option value = "<?php echo $shipping_zone['zone_id'] . '_' . $shipping_method['id'] ?>"><?php echo $shipping_method['method_title']?></option>  
                                                             <?php  }

                                                                } ?>
                                                        </optgroup>  <?php 
                                                    }
                                                }
                                                }?>  
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-6 bis_shipping_method_charge_div" style="display: none">
                                <label><?php _e("Shipping Charge", "productrules"); ?></label>
                                <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                                <a class="popoverData" target="_blank" href="<?php echo BIS_RULES_ENGINE ?>"
                                   data-content='<?php _e("This entered shipping charge will replace with selected method.", "productrules"); ?>' rel="popover"
                                   data-placement="bottom" data-trigger="hover">
                                    <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                                </a>
                                <input type="number" class="form-control" id="bis_product_shipping_charge" name="bis_product_shipping_charge" placeholder="0.0" style="height:35px !important;">
                            </div>
                            <span class="col-md-6 bis-shipping-des" style="display:none">
                                <label><?php _e("Shipping Description", "rulesengine"); ?></label>
                                <a class="popoverData " target="_blank" href="http://docs.megaedzee.com/wp-content/uploads/2018/06/shipping-description.png"
                                   data-content='<?php _e("This description shown below for the selected products.", "rulesengine"); ?>' rel="popover"
                                   data-placement="bottom" data-trigger="hover">
                                    <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                                </a>
                                <input type="text" class="form-control" maxlength="500"
                                       id="bis_shipping_des" name="bis_shipping_des"
                                       placeholder='<?php _e("Enter shipping description", "productrules"); ?>'>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <span class="bis-content-image" style="display: none">
                            <label for="description"><?php _e("Image", "productrules"); ?></label>
                            <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                            <div>
                                <select name="bis_re_image_id" id="bis_re_image_id" size="2">
                                    <?php foreach ($images_list as $image) { ?>
                                        <option data-img="<?php echo $image->image; ?>" value="<?php echo $image->id ?>" id="<?php echo $image->id ?>"><?php echo $image->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </span>
                    </div>
                    <div class="form-group col-md-6">
                        <span class="form-group bis-content-image" style="display: none">
                            <label><?php _e("Image Size", "productrules"); ?></label>
                            <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                            <div>
                                <select class="form-control" name="bis_re_cont_img_size" id="bis_re_cont_img_size"
                                        size="2">
                                            <?php foreach ($pos_img_sizes as $img_size) { ?>
                                        <option value="<?php echo $img_size->value ?>"
                                                id="<?php echo $img_size->value ?>"><?php echo $img_size->name ?></option>
                                            <?php } ?>
                                </select>
                            </div>
                        </span>
                    </div>
                </div>
                <div class="form-group col-md-9 pr-0 pl-0">
                    <span class="bis-content-span" style="display: none">
                        <label id="bis_re_product_dc_label" for="description"><?php _e("Content Body", "productrules"); ?></label>
                        <span class="glyphicon glyphicon-asterisk bis-icon-red"></span>
                        <a class="popoverData " target="_blank" href="<?php echo BIS_RULES_ENGINE ?>"
                           data-content='<?php _e("This field is required and supports html tags and inline style", "rulesengine"); ?>' rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                        <textarea maxlength="1000" rows="2" class="form-control" name="bis_re_product_dc_content"
                                  id="bis_re_product_dc_content"
                                  placeholder='<?php _e("Enter dynamic content, HTML tags also supported.", "rulesengine") ?>'></textarea>
                    </span>
                    <span class="bis-shipping-error-des-div" style="display: none">
                        <label id="bis_shipping_error_des_label" for="Error description"><?php _e("Error message", "productrules"); ?></label>
                        <a class="popoverData " target="_blank" href="http://docs.megaedzee.com/wp-content/uploads/2018/06/error-message1.png"
                           data-content='<?php _e("This field is shown on checkout page while performing checkout process.", "rulesengine"); ?>' rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                        <textarea maxlength="1000" rows="2" class="form-control" name="bis_shipping_error_des"
                                  id="bis_re_product_dc_content"
                                  placeholder='<?php _e("Enter message for showing an error message on checkout page.", "rulesengine") ?>'></textarea>
                    </span>
                    <span class="bis-restrict-error-div" style="display: none">
                        <label id="bis_restrict_error_des_label" for="Restrict description"><?php _e("Restrict message", "productrules"); ?></label>
                        <a class="popoverData " target="_blank" href="http://docs.megaedzee.com/wp-content/uploads/2018/06/error-message1.png"
                           data-content='<?php _e("This field is shown on product while adding product to cart.", "rulesengine"); ?>' rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                        <textarea maxlength="1000" rows="2" class="form-control" name="bis_restrict_error"
                                  id="bis_restrict_error"
                                  placeholder='<?php _e("Enter restrict message.", "rulesengine") ?>'></textarea>
                    </span>
                    <span class="bis-restrict-error-div-vv" style="display: none">
                        <label id="bis_restrict_error_des_label_vv" for="Restrict vice versa description"><?php _e("Restrict vice versa message", "productrules"); ?></label>
                        <a class="popoverData " target="_blank" href="http://docs.megaedzee.com/wp-content/uploads/2018/06/error-message1.png"
                           data-content='<?php _e("This field is shown on product while adding product to cart.", "rulesengine"); ?>' rel="popover"
                           data-placement="bottom" data-trigger="hover">
                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </a>
                        <textarea maxlength="1000" rows="2" class="form-control" name="bis_restrict_error_vv"
                                  id="bis_restrict_error_vv"
                                  placeholder='<?php _e("Enter restrict message.", "rulesengine") ?>'></textarea>
                    </span>
                </div>
                <div class="row">
                    <div class="form-group col-md-9 pr-0">
                        <label for="description"><?php _e("Description", "rulesengine"); ?></label>
                        <textarea rows="2" maxlength="500" class="form-control" name="bis_re_product_description"
                                  id="bis_re_product_description" placeholder='<?php _e("Enter product rule description", "rulesengine") ?>'></textarea>
                    </div>
                </div>
                <span class="bis-common-popup-span" style="display: none;">
                    <?php include_once(ABSPATH . "wp-content/plugins/" . BIS_PRODUCT_RULESENGINE_PLATFORM . "/includes/bis-popup-configuration.php"); ?>  
                </span>
            </div>
        </div>
    </div>    
    <input type="hidden" id="bis_re_rd_add" name="bis_re_rd_add" value="true" />   
    <?php include_once(ABSPATH . "wp-content/plugins/" . BIS_PRODUCT_RULESENGINE_PLATFORM . "/includes/logical-rules-parent-add.php"); ?>
    <div class = "row">
        <div class = "col-md-10">
            <button type="button" id="bis_re_show_product_rules_list" class="btn btn-primary">
                <i class="glyphicon glyphicon-chevron-left bis-glyphi-position"></i>
                <?php _e("Go Back", "rulesengine"); ?>
            </button>
        </div>
        <div class="col-md-2" align="right">
            <button id="bis_re_product_submit" type="button" class="btn btn-primary">
                <i class="glyphicon glyphicon-ok-sign bis-glyphi-position"></i><?php _e(" Save Rule", "rulesengine"); ?>
            </button>
        </div>
    </div>
</form>
<script>

    // wait for the DOM to be loaded
    jQuery(document).ready(function () {

        jQuery("#bis_re_cont_pos").change(function () {
            if (jQuery("#bis_re_cont_pos").val() === "pos_dialog_post") {
                jQuery(".bis-common-popup-span").show();
            } else {
                jQuery(".bis-common-popup-span").hide();
            }
        });

        function bis_loadProduct(productId) {

            jQuery('#bis_re_attribute_terms').hide();
            var includeSelectAllOption, is_multiselect, selectOption, ntg_found;
            switch (productId) {
                case "attributes" :
                    includeSelectAllOption = false;
                    is_multiselect = "bis-selectComponent";
                    selectOption = "Select Attribute";
                    ntg_found = "No attributes found";
                    break;
                case "categories" :
                    includeSelectAllOption = true;
                    is_multiselect = "bis-selectComponent-multiple";
                    selectOption = "Select Category";
                    ntg_found = "No categories found";
                    break;
                case "woo-tags" :
                    includeSelectAllOption = true;
                    is_multiselect = "bis-selectComponent-multiple";
                    selectOption = "Select Tag";
                    ntg_found = "No tags found";
                    break;
                case "custom_taxo" :
                    includeSelectAllOption = true;
                    is_multiselect = "bis-selectComponent-multiple";
                    selectOption = "Select Taxonomy";
                    ntg_found = "No taxonomies found";
                    break;
                default :
                    includeSelectAllOption = true;
                    is_multiselect = "bis-selectComponent-multiple";
                    selectOption = "Select Products";
                    ntg_found = "No products found";
            }
            jQuery("#bis_selected_type").text(selectOption);
            var jqXHR = jQuery.get(ajaxurl,
                    {
                        action: "bis_re_get_product_list",
                        bis_nonce: BISAjax.bis_rules_engine_nonce,
                        bis_type_id: productId
                    });

            jqXHR.done(function (response) {
                if (response["status"] === "success") {
                    var data = {
                        compId: 'bis_re_product_list',
                        compName: 'bis_re_product_list[]',
                        suboptions: response['data']
                    };

                    var source = jQuery("#" + is_multiselect).html();
                    var template = Handlebars.compile(source);
                    var productlistContent = template(data);
                    var subOptObj = jQuery("#bis_re_product_list_span");

                    if (!response['data']) {
                        productlistContent = "<span style='color:red'>" + ntg_found + "</span>";
                    }
                    subOptObj.html(productlistContent);

                    jQuery('#bis_re_product_list').multiselect({
                        includeSelectAllOption: includeSelectAllOption,
                        enableCaseInsensitiveFiltering: true,
                        maxHeight: 300,
                        selectedClass: null,
                        enableHTML: true,
                        optionLabel: function (element) {
                            var url = jQuery(element).attr('data-img');
                            if (url && url.indexOf("http") > -1) {
                                return jQuery(element).text();
                            } else {
                                return jQuery(element).text();
                            }
                        },
                        nonSelectedText: selectOption,
                        buttonWidth: '250px',
                        onChange: function (element, checked) {
                            if (productId === "attributes" || productId ==="custom_taxo") {
                                var action = element[0].value;
                                get_all_term_list(action, productId);
                            }
                        }
                    });
                    jQuery('#bis_re_products').show();
                }
            });

        }
        jQuery('#bis_re_product_list').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            maxHeight: 300,
            selectedClass: null,
            enableHTML: true,
            optionLabel: function (element) {
                var url = jQuery(element).attr('data-img');
                if (url && url.indexOf("http") > -1) {
                    return '<img height="30px" width="30px" src="' + jQuery(element).attr('data-img') + '"> ' + jQuery(element).text();
                } else {
                    return jQuery(element).text();
                }
            },
            nonSelectedText: '<?php _e("Select Product", "rulesengine"); ?>',
            buttonWidth: '250px'
        });
        jQuery("#bis_re_product_type").change(function () {
            bis_loadProduct(this.value);
        });

        jQuery('.popoverData').popover({
            trigger: "click hover focus"
        });

        jQuery("#bis_re_show_product_rules_list").click(function () {
            bis_showProductRulesList();
            jQuery(".pagination").bootpag({page: 1});
        });

        jQuery('#bis_re_term_list').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            maxHeight: 300,
            nonSelectedText: '<?php _e("Select Term", "rulesengine"); ?>',
            buttonWidth: '250px'
        });

        function get_all_term_list(term, productId) {
            var jqXHR = jQuery.get(ajaxurl,
                    {
                        action: "bis_re_get_term_list_product",
                        bis_nonce: BISAjax.bis_rules_engine_nonce,
                        bis_term_id: term,
                        productId:productId
                    });
            jqXHR.done(function (response) {
                if (response["status"] === "success") {
                    var data = {
                        compId: 'bis_re_term_list',
                        compName: 'bis_re_term_list[]',
                        suboptions: response['data']
                    };

                    var source = jQuery("#bis-selectComponent-multiple").html();
                    var template = Handlebars.compile(source);
                    var termlistContent = template(data);
                    var subOptObj = jQuery("#bis_re_term_list_span");
                }

                if (!response['data']) {
                    termlistContent = "<span style='color:red'>No Terms found</span>";
                }
                subOptObj.html(termlistContent);

                jQuery('#bis_re_term_list').multiselect({
                    includeSelectAllOption: true,
                    enableCaseInsensitiveFiltering: true,
                    maxHeight: 300,
                    selectedClass: null,
                    enableHTML: true,
                    optionLabel: function (element) {
                        var url = jQuery(element).attr('data-img');
                        if (url && url.indexOf("http") > -1) {
                            return jQuery(element).text();
                        } else {
                            return jQuery(element).text();
                        }
                    },
                    nonSelectedText: '<?php _e('Select Term', "rulesengine"); ?>',
                    buttonWidth: '250px',
                });
            });
            jQuery('#bis_re_attribute_terms').show();
        }

        var options = {
            success: showResponse,
            url: BISAjax.ajaxurl,
            beforeSend: bis_before_request_send,
            beforeSubmit: bis_validateAddProductRulesForm,
            data: {
                action: 'bis_re_update_next_product_rule',
                bis_nonce: BISAjax.bis_rules_engine_nonce

            }
        };

        jQuery('#bis_re_addproductruleform').ajaxForm(options);

        jQuery('.bis-multiselect').multiselect();

        jQuery('#bis_re_cont_pos').multiselect({
            maxHeight: 300,
            selectedClass: null,
            nonSelectedText: '<?php _e("Select location", "productrules"); ?>',
            buttonWidth: '250px'
        });

        jQuery('#bis_shipping_is').change(function () {
            debugger;
            bis_controll_shipping_methods(this.value);
        });

        jQuery('#bis_shipping_is').multiselect({
            maxHeight: 300,
            selectedClass: null,
            buttonWidth: '250px'
        });

        jQuery('#bis_payment_is').multiselect({
            maxHeight: 300,
            selectedClass: null,
            buttonWidth: '250px'
        });

        jQuery('#bis_payment_method').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            maxHeight: 300,
            selectedClass: null,
            buttonWidth: '250px'
        });
        
        jQuery('#bis_restrict_product_list').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            maxHeight: 300,
            selectedClass: null,
            buttonWidth: '250px'
        });

        jQuery('#bis_re_product_action').multiselect({
            nonSelectedText: '<?php _e("Select action", "rulesengine"); ?>',
            buttonWidth: '250px',
            maxHeight: 300,
            enableCaseInsensitiveFiltering: true,
            onChange: function (element, checked) {
                var action = element[0].value;
                var position = jQuery("#bis_re_cont_pos").val();
                bis_show_product_view_controls(action, position, "func-from-add");
            }
        });

        jQuery('#bis_re_image_id').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            maxHeight: 250,
            selectedClass: null,
            enableHTML: true,
            optionLabel: function (element) {
                return jQuery(element).text();
            },
            nonSelectedText: '<?php _e("Select image", "productrules") ?>',
            buttonWidth: '250px'
        });
        jQuery('#bis_re_cont_img_size').multiselect({
            selectedClass: null,
            nonSelectedText: '<?php _e("Select size", "productrules") ?>',
            buttonWidth: '250px'
        });

        jQuery('#bis_re_product_type').multiselect({
            enableCaseInsensitiveFiltering: true,
            maxHeight: 300,
            nonSelectedText: '<?php _e("Select Type", "productrules") ?>',
            buttonWidth: '250px'
        });
        
        jQuery('#bis_shipping_methods').multiselect({
            enableCaseInsensitiveFiltering: true,
            maxHeight: 300,
            nonSelectedText: '<?php _e("Select methods", "productrules") ?>',
            buttonWidth: '250px'
        });

        jQuery("#bis_re_product_submit").click(function () {
            var ruleName = jQuery.trim(jQuery("#bis_re_product_rule").val());
            jQuery("#bis_re_name").val(ruleName + " Product");
            jQuery("#bis_re_description").val(jQuery.trim(jQuery("#bis_re_product_description").val()));
            if (jQuery(".bis-query-builder").is(':checked')) {
                var builder = jQuery('#bis-rule-builder').queryBuilder('getRules');
                jQuery("#bis_jquery_result").val(JSON.stringify(builder));
            }
            jQuery("#bis_re_addproductruleform").submit();
            jQuery(".pagination").bootpag({page: 1});
            jQuery('#bis_select_action').multiselect("disable");
        });

        jQuery('#bis_re_rule_status').multiselect({
            nonSelectedText: '<?php _e("Select status", "rulesengine"); ?>',
            buttonWidth: '250px'
        });
        jQuery('#bis_re_product_action').multiselect({
            nonSelectedText: '<?php _e("Select action", "rulesengine"); ?>',
            buttonWidth: '300px'
        });

        function showCreateProductResponse(responseText, statusText, xhr, $form) {


            if (responseText["status"] === "error") {
                jQuery("#alert_failure_id").show();

                if (responseText["message_key"] === "duplicate_entry") {
                    bis_showErrorMessage('<?php _e("Duplicate Product Rule name, Product Rule name should be unique.", "rulesengine"); ?>');
                } else {
                    bis_showErrorMessage('<?php _e("Error occurred while creating Product rule", "rulesengine"); ?>');
                }

            } else {
                jQuery("#product_rules_child_content").html("");
                jQuery("#product_rules_parent_content").html("");
                bis_showProductRulesList();
            }
        }

        function showResponse(data, statusText, xhr, $form) {

            // If Logical rule select then enable below code.

            if (data["status"] === "error") {
                jQuery("#alert_failure_id").show();
                bis_request_complete();
                if (data["message_key"] === "duplicate_entry") {
                    bis_showErrorMessage('<?php _e("Duplicate Product Rule name, Product Rule name should be unique.", "productrules"); ?>');
                } else if (data["message_key"] === "invalid_shortcode") {
                    bis_showErrorMessage('<?php _e("Invalid Shortcode", "productrules"); ?>');
                } else if (data["message_key"] === "no_method_found") {
                    bis_showErrorMessage('<?php _e("Action hook method does not exist, Please define method with name", BIS_RULES_ENGINE_TEXT_DOMAIN); ?>'
                            + " \"" + jQuery("#bis_re_hook").val() + "\".");
                } else {
                    bis_showErrorMessage('<?php _e("Error occurred while creating Product rule", "productrules"); ?>');
                }
            } else {
                if ('<?php echo $rule_action ?>' === 'productadd') {
                    var url, posturl, productid;
                    var productIds = jQuery.parseJSON('<?php echo json_encode($product_ids) ?>');
                    jQuery.each(productIds, function (key, value) {
                        productid = value;
                    });
                    if ('<?php echo $product_title ?>') {
                        url = '<?php echo admin_url('post.php'); ?>';
                        posturl = url + '?post=' + productid + '&action=edit';
                        window.location.href = posturl;
                    } else {
                        var type = '<?php echo $post_type ?>';
                        url = '<?php echo admin_url('edit.php'); ?>';
                        posturl = url + '?post_type=' + type;
                        window.location.href = posturl;
                    }
                } else {
                    bis_getProductPageCount(0);
                }
            }
            jQuery('#bis_select_action').multiselect("disable");
            jQuery("#bis_check_all").attr('checked', false);
        }

        jQuery("#failureButtonId").click(function () {
            jQuery("#alert_failure_id").hide();
        });

        jQuery("#failureButtonId").click(function () {
            jQuery("#alert_failure_id").hide();
        });

        jQuery("#bis_product_back_add_rules").click(function () {
            jQuery("#product_rules_parent_content").hide();
            jQuery("#product_rules_child_content").show();
        });

        jQuery("#bis_re_product_type").multiselect('select', '<?php echo $post_type; ?>');

        if ('<?php echo $rule_action ?>' === 'productadd') {
            jQuery("#bis_re_show_product_rules_list").hide();
            var productIds = jQuery.parseJSON('<?php echo json_encode($product_ids) ?>');

            jQuery.each(productIds, function (key, value) {
                jQuery("#bis_re_product_list").multiselect('select', value);
            });
        }

    });
</script>