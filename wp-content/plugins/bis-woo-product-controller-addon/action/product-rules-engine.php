<?php
/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

use bis\repf\action\PostRulesEngine;
use bis\repf\action\RulesEngine;
use bis\repf\model\LogicalRulesEngineModel;
use bis\repf\common\RulesEngineLocalization;
use bis\repf\common\RulesEngineCacheWrapper;
use bis\repf\model\PostRulesEngineModel;

/**
 * Class ProductRulesEngine
 */
class ProductRulesEngine extends PostRulesEngine {

    public function bis_re_exclude_products($query) {

        if (!$query->is_main_query() || is_admin()) {
            return;
        }
        
        RulesEngineUtil::update_rule_cache_id(BIS_LOGICAL_CACHE_REFRESH_ID);
        $this->apply_session_post_rules();
        $this->bis_evaluate_request_rules();
        $excluded_posts_id_array = $this->bis_re_get_excluded_post_ids();
        $applied_request_rules = $this->get_request_rules();
        $merged_excluded_posts = null;
        $applied_request_post_rules = null;
        
        if ($applied_request_rules != null && !empty($applied_request_rules)) {
            $post_rule_modal = new PostRulesEngineModel();
            $applied_request_post_rules = $post_rule_modal->get_applied_post_rules($applied_request_rules);
        }

        if (!empty($excluded_posts_id_array)) {
            $merged_excluded_posts = $excluded_posts_id_array;
        }

        if ($applied_request_post_rules != null && !empty($applied_request_post_rules)) {
            $excluded_posts_id_req_array = RulesEngineUtil::get_applied_post_rule_ids($applied_request_post_rules);

            if ($merged_excluded_posts == null) {
                $merged_excluded_posts = $excluded_posts_id_req_array;
            } else {
                $merged_excluded_posts = array_merge($merged_excluded_posts, $excluded_posts_id_req_array);
            }
        }
        
        // Add to session if only post rules exists
        if (!empty($merged_excluded_posts)) {

            $product_array = $this->bis_get_product_show_matches($merged_excluded_posts);

            if (!empty($product_array['product_cats'])) {
                $taxquery[] = array(
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'id',
                        'terms' => $product_array['product_cats'],
                        'operator' => 'NOT IN'
                ));
            }

            if (!empty($product_array['product_tags'])) {
                $taxquery[] = array(
                    array(
                        'taxonomy' => 'product_tag',
                        'field' => 'slug',
                        'terms' => $product_array['product_tags'],
                        'operator' => 'NOT IN'
                    )
                );
            }

            if (!empty($product_array['show_only_cat'])) {
                $taxquery[] = array(
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'id',
                        'terms' => $product_array['show_only_cat'],
                        'operator' => 'IN'
                    )
                );
            }

            if (!empty($product_array['show_only_tags'])) {
                $taxquery[] = array(
                    array(
                        'taxonomy' => 'product_tag',
                        'field' => 'slug',
                        'terms' => $product_array['show_only_tags'],
                        'operator' => 'IN'
                    )
                );
            }

            if (!empty($product_array['product_atts'])) {
                $atts = $product_array['product_atts'];
                foreach ($atts as $key => $att) {
                    $taxquery[] = array(
                        'taxonomy' => 'pa_' . $key,
                        'field' => 'slug',
                        'terms' => $att,
                        'operator' => 'NOT IN'
                    );
                }
            }

            if (!empty($product_array['show_only_atts'])) {
                $atts = $product_array['show_only_atts'];
                foreach ($atts as $key => $att) {
                    $taxquery[] = array(
                        'taxonomy' => 'pa_' . $key,
                        'field' => 'slug',
                        'terms' => $att,
                        'operator' => 'IN'
                    );
                }
            }

            $existing_taxquery = $query->get('tax_query');

            if (!empty($existing_taxquery) && isset($taxquery)) {
                array_push($taxquery, $existing_taxquery);
            }

            if (isset($taxquery)) {
                $query->set('tax_query', $taxquery);
            }

            $product_ids = $product_array['products'];

            if (!empty($product_ids)) {
                if (is_single()) {
                    $query->set('post__not_in', $product_ids);
                    return;
                }

                $query->set('post__not_in', $product_ids);
            }

            $hide_all = $product_array['hide_all'];
            if (!empty($hide_all)) {
                set_query_var('post__in', $hide_all);
            }

            $show_only_products = $product_array['show_only_product'];
            if (!empty($show_only_products)) {
                set_query_var('post__in', $show_only_products);
            }

            RulesEngineCacheWrapper::set_session_attribute(BIS_PROD_CAT_WIDGET_EXCLUDE, $product_array['product_cats']);
            RulesEngineCacheWrapper::set_session_attribute(BIS_PROD_TAG_WIDGET_EXCLUDE, $product_array['product_tags']);
        } else {
            $product_model = new ProductRulesEngineModel();
            $rows = $product_model->get_post_or_product_show_rules(BIS_WOO_PRODUCT_TYPE_RULE);
            if (!empty($rows)) {
                $product_array = $this->bis_get_product_show_matches($rows, true);
            } else {
                return;
            }

            $taxquery = array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'id',
                    'terms' => $product_array['product_cats'],
                    'operator' => 'NOT IN',
                ),
                array(
                    'taxonomy' => 'product_tag',
                    'field' => 'slug',
                    'terms' => $product_array['product_tags'],
                    'operator' => 'NOT IN'
                )
            );

            if (!empty($product_array['product_atts'])) {
                $atts = $product_array['product_atts'];
                foreach ($atts as $key => $att) {
                    $taxquery[] = array(
                        'taxonomy' => 'pa_' . $key,
                        'field' => 'slug',
                        'terms' => $att,
                        'operator' => 'NOT IN'
                    );
                }
            }

            // Append with existing taxquery.
            $existing_taxquery = $query->get('tax_query');

            if (!empty($existing_taxquery)) {
                array_push($taxquery, $existing_taxquery);
            }

            $query->set('tax_query', $taxquery);

            $product_ids = $product_array['products'];

            if (!empty($product_ids)) {
                if (is_single()) {
                    $query->set('post__not_in', $product_ids);
                    return;
                }
                $query->set('post__not_in', $product_ids);
            }

            $hide_all = $product_array['hide_all'];
            if (!empty($hide_all)) {
                set_query_var('post__in', $hide_all);
            }

            RulesEngineCacheWrapper::set_session_attribute(BIS_PROD_CAT_WIDGET_EXCLUDE, $product_array['product_cats']);
            RulesEngineCacheWrapper::set_session_attribute(BIS_PROD_TAG_WIDGET_EXCLUDE, $product_array['product_tags']);
        }

        return $query;
    }

    public function bis_get_product_show_matches($rules = null, $fail = false) {

        $products = array();
        $product_cats = array();
        $product_tags = array();
        $product_atts = array();
        $hide_all = array();
        $show_only_post = array();
        $show_only_cat = array();
        $show_only_tags = array();
        $show_only_atts = array();
        
        if (!empty($rules) && $fail == false) {
            foreach ($rules as $rule) {
                if ('show_post' === $rule->action) {
                    if ('product' === $rule->parent_type_value || 'grouped_product' === $rule->parent_type_value || 'variable_product' === $rule->parent_type_value || 'external_product' === $rule->parent_type_value) {
                        if (( $key = array_search($rule->parent_id, $products) ) !== false) {
                            unset($products[$key]);
                        }
                    } else if ('categories' === $rule->parent_type_value) {
                        if (( $key = array_search($rule->parent_id, $product_cats) ) !== false) {
                            unset($product_cats[$key]);
                        }
                    } else if ('woo-tags' === $rule->parent_type_value) {
                        if (( $key = array_search($rule->parent_id, $product_tags) ) !== false) {
                            unset($product_tags[$key]);
                        }
                    } else if ('attributes' === $rule->parent_type_value || "custom_taxo" === $rule->parent_type_value) {
                        $taxonomy = json_decode($rule->gencol4);
                        if (( $key = array_search($taxonomy[0], $product_atts) ) !== false) {
                            $exists = $product_atts[$taxonomy[0]];
                            if (!empty($exists)) {
                                unset($exists[$rule->parent_id]);
                                $product_atts[$taxonomy[0]] = $exists;
                            } else {
                                unset($product_atts[$key]);
                            }
                        }
                    }
                } elseif ('hide_post' === $rule->action) {
                    if ('product' === $rule->parent_type_value || 'grouped_product' === $rule->parent_type_value || 'variable_product' === $rule->parent_type_value || 'external_product' === $rule->parent_type_value) {
                        if ($rule->parent_id == "bis_prod_all") {
                            $hide_all[] = $rule->parent_id;
                        }
                        $products[] = $rule->parent_id;
                    } else if ('categories' === $rule->parent_type_value) {
                        $product_cats[] = $rule->parent_id;
                    } else if ('woo-tags' === $rule->parent_type_value) {
                        $product_tags[] = $rule->parent_id;
                    } else if ('attributes' === $rule->parent_type_value || "custom_taxo" === $rule->parent_type_value) {
                        $taxonomy = json_decode($rule->gencol4);
                        if (array_key_exists($taxonomy[0], $product_atts)) {
                            $exists = $product_atts[$taxonomy[0]];
                            $exists[] = $rule->parent_id;
                            $product_atts[$taxonomy[0]] = $exists;
                        } else {
                            $product_atts[$taxonomy[0]] = array($rule->parent_id);
                        }
                    }
                } else if ('show_only_post' === $rule->action) {
                    if ('product' === $rule->parent_type_value || 'grouped_product' === $rule->parent_type_value || 'variable_product' === $rule->parent_type_value || 'external_product' === $rule->parent_type_value) {
                        $show_only_post[] = $rule->parent_id;
                    } else if ('categories' === $rule->parent_type_value) {
                        $show_only_cat[] = $rule->parent_id;
                    } else if ('woo-tags' === $rule->parent_type_value) {
                        $show_only_tags[] = $rule->parent_id;
                    } else if ('attributes' === $rule->parent_type_value || "custom_taxo" === $rule->parent_type_value) {
                        $taxonomy = json_decode($rule->gencol4);
                        if (array_key_exists($taxonomy[0], $show_only_atts)) {
                            $exists_atts = $show_only_atts[$taxonomy[0]];
                            $exists_atts[] = $rule->parent_id;
                            $show_only_atts[$taxonomy[0]] = $exists_atts;
                        } else {
                            $show_only_atts[$taxonomy[0]] = array($rule->parent_id);
                        }
                    }
                }
            }
        } else if (!empty($rules) && $fail == true) {
            foreach ($rules as $rule) {
                $valid = RulesEngineCommon::is_rule_valid($rule->name);
                if ('show_post' === $rule->action && $valid == true) {
                    if ('product' === $rule->parent_type_value || 'grouped_product' === $rule->parent_type_value || 'variable_product' === $rule->parent_type_value || 'external_product' === $rule->parent_type_value) {
                        if (( $key = array_search($rule->parent_id, $products) ) !== false) {
                            unset($products[$key]);
                        }
                    } else if ('categories' === $rule->parent_type_value) {
                        if (( $key = array_search($rule->parent_id, $product_cats) ) !== false) {
                            unset($product_cats[$key]);
                        }
                    } else if ('woo-tags' === $rule->parent_type_value) {
                        if (( $key = array_search($rule->parent_id, $product_tags) ) !== false) {
                            unset($product_tags[$key]);
                        }
                    } else if ('attributes' === $rule->parent_type_value || "custom_taxo" === $rule->parent_type_value) {
                        $taxonomy = json_decode($rule->gencol4);
                        if (( $key = array_search($taxonomy[0], $product_atts) ) !== false) {
                            $exists = $product_atts[$taxonomy[0]];
                            if (!empty($exists)) {
                                unset($exists[$rule->parent_id]);
                                $product_atts[$taxonomy[0]] = $exists;
                            } else {
                                unset($product_tags[$key]);
                            }
                        }
                    }
                } else {
                    if ('product' === $rule->parent_type_value || 'grouped_product' === $rule->parent_type_value || 'variable_product' === $rule->parent_type_value || 'external_product' === $rule->parent_type_value) {
                        if ($rule->parent_id == "bis_prod_all") {
                            $hide_all[] = $rule->parent_id;
                        }
                        $products[] = $rule->parent_id;
                    } else if ('categories' === $rule->parent_type_value) {
                        $product_cats[] = $rule->parent_id;
                    } else if ('woo-tags' === $rule->parent_type_value) {
                        $product_tags[] = $rule->parent_id;
                    } else if ('attributes' === $rule->parent_type_value || "custom_taxo" === $rule->parent_type_value) {
                        $taxonomy = json_decode($rule->gencol4);
                        if (array_key_exists($taxonomy[0], $product_atts)) {
                            $exists = $product_atts[$taxonomy[0]];
                            $exists[] = $rule->parent_id;
                            $product_atts[$taxonomy[0]] = $exists;
                        } else {
                            $product_atts[$taxonomy[0]] = array($rule->parent_id);
                        }
                    }
                }
            }
        }

        array_unique($products);
        array_unique($product_cats);
        array_unique($product_tags);
        array_unique($product_atts);
        array_unique($show_only_post);
        array_unique($show_only_cat);
        array_unique($show_only_tags);
        array_unique($show_only_atts);

        return array(
            'products' => $products,
            'product_cats' => $product_cats,
            'product_tags' => $product_tags,
            'product_atts' => $product_atts,
            'show_only_product' => $show_only_post,
            'show_only_cat' => $show_only_cat,
            'show_only_tags' => $show_only_tags,
            'show_only_atts' => $show_only_atts,
            'hide_all' => $hide_all
        );
    }

    public function bis_exclude_product_cat_in_widget($cat_args) {
        $cat_exclude = RulesEngineCacheWrapper::get_session_attribute(BIS_PROD_CAT_WIDGET_EXCLUDE);

        $cat_args['exclude'] = $cat_exclude;
        return $cat_args;
    }

    public function bis_exclude_product_tag_in_widget($tag_args) {
        $tag_exclude = RulesEngineCacheWrapper::get_session_attribute(BIS_PROD_TAG_WIDGET_EXCLUDE);
        $tag_ids = array();
        if (!empty($tag_exclude)) {
            foreach ($tag_exclude as $tag) {
                $tag_ids[] = get_term_by('slug', $tag, 'product_tag')->term_id;
            }
        }

        $tag_args['exclude'] = $tag_ids;
        return $tag_args;
    }

    public function bis_add_product_general_box_fields() {
        global $post;
        $logical_rule_engine_modal = new LogicalRulesEngineModel();
        $bis_re_rule_options = $logical_rule_engine_modal->get_rules_options(BIS_PROD_GEN_FIELD_RULE);
        ?><div id='bis_gen_product_rules' class='panel woocommerce_options_panel'>
            <div class='options_group'><?php
                woocommerce_wp_select(
                        array(
                            'id' => 'bis_pro_gen_action',
                            'label' => __('Select Action', 'productrules'),
                            'options' => array(
                                'hide_post' => __('Hide', 'productrules'),
                                'show_post' => __('Show', 'productrules')
                            )
                        )
                );

                $bis_criteria = get_post_meta($post->ID, 'bis_pro_gen_field_select', true);
                $bis_criteria_text_val = get_post_meta($post->ID, 'bis_pro_gen_criteria_value_text', true);
                $bis_criteria_drop_down_val = get_post_meta($post->ID, 'bis_pro_gen_criteria_value_dropdown', true);
                ?>
                <input type="hidden" id="bis_pro_created_rule_id" name="bis_pro_created_rule_id">
                <p class="form-field bis_pro_gen_field_select_field">
                    <label for="bis_pro_gen_field_select"><?php _e("Select Criteria", "productrules"); ?></label>
                    <select class="select short " name="bis_pro_gen_field_select" id="bis_pro_gen_field_select">
                        <option value="none"> <?php echo "None" ?></option>
                        <?php
                        foreach ($bis_re_rule_options as $row) {
                            $bis_sub_options = $logical_rule_engine_modal->get_rules_sub_options($row->id, BIS_PRODUCT_PLUGIN_ID);
                            $bis_sub_options = RulesEngineLocalization::get_localized_values($bis_sub_options);
                            ?>
                            <optgroup label="<?php echo __($row->name, 'rulesengine'); ?>">
                                <?php foreach ($bis_sub_options as $sub_row) { ?>
                                    <?php
                                    if ($bis_criteria == $row->id . '_' . $sub_row->id) {
                                        $is_selected = "selected=\"selected\"";
                                    } else {
                                        $is_selected = "";
                                    }
                                    ?>
                                    <option value="<?php echo $row->id . '_' . $sub_row->id ?>"
                                            <?php echo $is_selected ?> id="<?php echo $sub_row->id ?>"> <?php echo __($sub_row->name, 'rulesengine'); ?>
                                    </option>
                                    <?php
                                }
                                ?>        
                            </optgroup>
                        <?php } ?>  
                        <optgroup label="Logical Rule">
                            <option value="bis_logical_rule"><?php echo "Logical Rule" ?></option>
                        </optgroup>
                    </select>
                </p>
                <p class="form-field bis_pro_gen_criteria_value_field ">
                    <label id="bis_pro_gen_criteria_value_lable" for="bis_pro_gen_criteria_value"><?php _e("Enter Criteria Value", "productrules"); ?></label>
                    <input type="text" value="<?php echo $bis_criteria_text_val ?>" class="short" name="bis_pro_gen_criteria_value_text" id="bis_pro_gen_criteria_value_text" style="display: none" placeholder="Enter value">
                    <select  class="select short " name="bis_pro_gen_criteria_value_dropdown" id="bis_pro_gen_criteria_value_dropdown" style="display: none">

                    </select>
                </p>
                <?php ?>
                <style>
                    .tokenInputClassProd{
                        max-height: 23px !important;           
                    }
                </style>
                <script>
                    jQuery(document).ready(function () {

                        var criteriaVal = "<?php echo $bis_criteria ?>";
                        var textVal = '<?php echo $bis_criteria_text_val ?>';
                        var funcCall = "fromEdit";
                        var drop_down_val = '<?php echo $bis_criteria_drop_down_val ?>';
                        var field = "pro";

                        bis_ship_gen_filed_edit(criteriaVal, textVal, funcCall, field, drop_down_val);

                        jQuery("#bis_pro_gen_field_select").change(function () {
                            var criteria = jQuery("#bis_pro_gen_field_select").val();
                            funcCall = "fromClick";
                            bis_ship_gen_filed_edit(criteria, textVal, funcCall, field);
                        });
                        jQuery('#bis_pro_gen_field_select').val(criteriaVal);
                    });
                </script>
            </div>
        </div>
        <?php
    }

    /* This method is used for generating fields in payment rule tab
     * @ Param $original_tabs
     * @ return $tabs
     */

    public function bis_add_payment_general_box_fields() {
        global $post;
        $logical_rule_engine_modal = new LogicalRulesEngineModel();
        $bis_re_rule_options = $logical_rule_engine_modal->get_rules_options(BIS_PROD_GEN_FIELD_RULE);
        $product_rule_modal = new ProductRulesEngineModel();
        $active_payment_methods = $product_rule_modal->get_active_payment_methods();
        $bis_payment_methods = get_post_meta($post->ID, 'bis_payment_methods', true);
        if (isset($bis_payment_methods)) {
            $bis_payment_method = json_decode($bis_payment_methods);
        } else {
            $bis_payment_method = null;
        }
        ?>

        <div id='bis_gen_payment_rules' class='panel woocommerce_options_panel'>
            <div class='options_group'>

                <p class="form-field bis_pro_gen_field_select_field">
                    <label for="bis_payment_methods"><?php _e("Payment Methods", "productrules"); ?></label>
                    <select class="basic-multiple" multiple="multiple" name="bis_payment_methods[]" id="bis_payment_methods">



                        <?php
                        if (!empty($active_payment_methods)) {
                            foreach ($active_payment_methods as $active_payment_method) {
                                if ($bis_payment_method != null && in_array($active_payment_method['id'], $bis_payment_method)) {
                                    $selected = "selected='selected'";
                                } else {
                                    $selected = "";
                                }
                                ?>
                                <option  <?php echo $selected ?> value="<?php echo $active_payment_method['id'] ?>"><?php echo $active_payment_method['name'] ?></option>  
                                <?php
                            }
                        }
                        ?>  

                    </select>
                </p>


                <?php
                woocommerce_wp_select(
                        array(
                            'id' => 'bis_payment_gen_action',
                            'label' => __('Payment Actions', 'productrules'),
                            'options' => array(
                                'enable' => __('Enable', 'productrules'),
                                'disable' => __('Disable', 'productrules')
                            )
                        )
                );

                $bis_criteria = get_post_meta($post->ID, 'bis_payment_gen_field_select', true);
                $bis_criteria_text_val = get_post_meta($post->ID, 'bis_payment_gen_criteria_value_text', true);
                $bis_criteria_drop_down_val = get_post_meta($post->ID, 'bis_payment_gen_criteria_value_dropdown', true);
                ?>
                <input type="hidden" id="bis_payment_created_rule_id" name="bis_payment_created_rule_id">
                <p class="form-field bis_pro_gen_field_select_field">
                    <label for="bis_payment_gen_field_select"><?php _e("Select Criteria", "productrules"); ?></label>
                    <select class="select short " name="bis_payment_gen_field_select" id="bis_payment_gen_field_select">
                        <option value="none"> <?php echo "None" ?></option>
                        <?php
                        foreach ($bis_re_rule_options as $row) {
                            $bis_sub_options = $logical_rule_engine_modal->get_rules_sub_options($row->id, BIS_PRODUCT_PLUGIN_ID);
                            $bis_sub_options = RulesEngineLocalization::get_localized_values($bis_sub_options);
                            ?>
                            <optgroup label="<?php echo __($row->name, 'rulesengine'); ?>">
                                <?php foreach ($bis_sub_options as $sub_row) { ?>
                                    <?php
                                    if ($bis_criteria == $row->id . '_' . $sub_row->id) {
                                        $is_selected = "selected=\"selected\"";
                                    } else {
                                        $is_selected = "";
                                    }
                                    ?>
                                    <option value="<?php echo $row->id . '_' . $sub_row->id ?>"
                                            <?php echo $is_selected ?> id="<?php echo $sub_row->id ?>"> <?php echo __($sub_row->name, 'rulesengine'); ?>
                                    </option>
                                    <?php
                                }
                                ?>        
                            </optgroup>
                        <?php } ?>  
                        <optgroup label="Logical Rule">
                            <option value="bis_logical_rule"><?php echo "Logical Rule" ?></option>
                        </optgroup>
                    </select>
                </p>
                <p class="form-field bis_pro_gen_criteria_value_field ">
                    <label id="bis_payment_gen_criteria_value_lable" for="bis_payment_gen_criteria_value_text"><?php _e("Enter Criteria Value", "productrules"); ?></label>
                    <input type="text" value="<?php echo $bis_criteria_text_val ?>" class="short" name="bis_payment_gen_criteria_value_text" id="bis_payment_gen_criteria_value_text" style="display: none" placeholder="Enter value">
                    <select  class="select short " name="bis_payment_gen_criteria_value_dropdown" id="bis_payment_gen_criteria_value_dropdown" style="display: none">

                    </select>
                </p>
                <?php ?>
                <style>
                    .tokenInputClassProd{
                        max-height: 23px !important;           
                    }
                </style>
                <script>
                    jQuery(document).ready(function () {
                        var criteriaVal = "<?php echo $bis_criteria ?>";
                        var textVal = '<?php echo $bis_criteria_text_val ?>';
                        var funcCall = "fromEdit";
                        var drop_down_val = '<?php echo $bis_criteria_drop_down_val ?>';
                        var field = "payment";

                        bis_ship_gen_filed_edit(criteriaVal, textVal, funcCall, field, drop_down_val);

                        jQuery("#bis_payment_gen_field_select").change(function () {
                            var criteria = jQuery("#bis_payment_gen_field_select").val();
                            funcCall = "fromClick";
                            bis_ship_gen_filed_edit(criteria, textVal, funcCall, field);
                        });

                        jQuery("#bis_payment_methods").select2({
                            tags: true
                        });
                        jQuery('#bis_payment_gen_field_select').val(criteriaVal);

                    });
                </script>
            </div>
        </div>
        <?php
    }

    public function bis_save_payment_general_box_fields($post_id) {

        $bis_criteria = null;
        $bis_dropdown = null;
        $bis_text_value = null;
        $bis_action = "payment";
        if (isset($_POST['bis_payment_methods'])) {
            $selected_payment_methods = $_POST['bis_payment_methods'];
            $json_methods = json_encode($selected_payment_methods);
            update_post_meta($post_id, 'bis_payment_methods', $json_methods);
        }

        if (isset($_POST['bis_payment_gen_field_select'])) {
            $bis_criteria = $_POST['bis_payment_gen_field_select'];
            update_post_meta($post_id, 'bis_payment_gen_field_select', $bis_criteria);
        }
        if (isset($_POST['bis_payment_gen_action'])) {
            $bis_payment_action = $_POST['bis_payment_gen_action'];
            if ($bis_criteria == "none") {
                $bis_payment_action = null;
            }
            update_post_meta($post_id, 'bis_payment_gen_action', esc_attr($bis_payment_action));
        }
        if (isset($_POST['bis_payment_gen_criteria_value_dropdown'])) {
            $bis_dropdown = $_POST['bis_payment_gen_criteria_value_dropdown'];
            if ($bis_criteria == "none") {
                $bis_dropdown = null;
            }
            update_post_meta($post_id, 'bis_payment_gen_criteria_value_dropdown', $bis_dropdown);
        }
        if (isset($_POST['bis_payment_gen_criteria_value_text'])) {
            $bis_text_value = $_POST['bis_payment_gen_criteria_value_text'];
            if ($bis_criteria == "none") {
                $bis_text_value = null;
            }
            update_post_meta($post_id, 'bis_payment_gen_criteria_value_text', $bis_text_value);
        }

        $bis_re_rule_detail_id = get_post_meta($post_id, 'bis_payment_created_rule_id', true);

        if ($bis_criteria != null) {
            $product_rules_engine_model = new ProductRulesEngineModel();
            $product_rules_engine_model->bis_save_general_box_rule($bis_action, $bis_criteria, $bis_dropdown, $bis_text_value, $post_id, $bis_re_rule_detail_id, "payment_rule", $bis_payment_action, $selected_payment_methods);
        }

        $bis_rule_detail_id = RulesEngineCacheWrapper::get_session_attribute(BIS_PROD_GEN_FEILD_LOGICAL_ID);
        if ($bis_criteria == "none") {
            $bis_rule_detail_id = null;
        }
        update_post_meta($post_id, 'bis_payment_created_rule_id', $bis_rule_detail_id);
    }

    public function bis_add_shipping_general_box_fields() {
        global $post;
        $logical_rule_engine_modal = new LogicalRulesEngineModel();
        $bis_re_rule_options = $logical_rule_engine_modal->get_rules_options(BIS_PROD_GEN_FIELD_RULE);
        ?>
        <div id='bis_gen_shipping_rules' class='panel woocommerce_options_panel'>
            <div class='options_group'><?php
                woocommerce_wp_select(
                        array(
                            'id' => 'bis_shipping_gen_action',
                            'label' => __('Shipping Actions', 'productrules'),
                            'options' => array(
                                'enable' => __('Enable', 'productrules'),
                                'disable' => __('Disable', 'productrules')
                            )
                        )
                );

                $bis_criteria = get_post_meta($post->ID, 'bis_shipping_gen_field_select', true);
                $bis_criteria_text_val = get_post_meta($post->ID, 'bis_shipping_gen_criteria_value_text', true);
                $bis_criteria_drop_down_val = get_post_meta($post->ID, 'bis_shipping_gen_criteria_value_dropdown', true);
                ?>

                <input type="hidden" id="bis_shipping_created_rule_id" name="bis_shipping_created_rule_id">
                <p class="form-field bis_pro_gen_field_select_field">
                    <label for="bis_shipping_gen_field_select"><?php _e("Select Criteria", "productrules"); ?></label>
                    <select class="select short " name="bis_shipping_gen_field_select" id="bis_shipping_gen_field_select">
                        <option value="none"> <?php echo "None" ?></option>
                        <?php
                        foreach ($bis_re_rule_options as $row) {
                            $bis_sub_options = $logical_rule_engine_modal->get_rules_sub_options($row->id, BIS_PRODUCT_PLUGIN_ID);
                            $bis_sub_options = RulesEngineLocalization::get_localized_values($bis_sub_options);
                            ?>
                            <optgroup label="<?php echo __($row->name, 'rulesengine'); ?>">
                                <?php foreach ($bis_sub_options as $sub_row) { ?>
                                    <?php
                                    if ($bis_criteria == $row->id . '_' . $sub_row->id) {
                                        $is_selected = "selected=\"selected\"";
                                    } else {
                                        $is_selected = "";
                                    }
                                    ?>
                                    <option value="<?php echo $row->id . '_' . $sub_row->id ?>"
                                            <?php echo $is_selected ?> id="<?php echo $sub_row->id ?>"> <?php echo __($sub_row->name, 'rulesengine'); ?>
                                    </option>
                                    <?php
                                }
                                ?>        
                            </optgroup>
                        <?php } ?>  
                        <optgroup label="Logical Rule">
                            <option value="bis_logical_rule"><?php echo "Logical Rule" ?></option>
                        </optgroup>
                    </select>
                </p>
                <p class="form-field bis_pro_gen_criteria_value_field ">
                    <label id="bis_shipping_gen_criteria_value_lable" for="bis_shipping_gen_criteria_value_text"><?php _e("Enter Criteria Value", "productrules"); ?></label>
                    <input type="text" value="<?php echo $bis_criteria_text_val ?>" class="short" name="bis_shipping_gen_criteria_value_text" id="bis_shipping_gen_criteria_value_text" style="display: none" placeholder="Enter value">
                    <select  class="select short " name="bis_shipping_gen_criteria_value_dropdown" id="bis_shipping_gen_criteria_value_dropdown" style="display: none">

                    </select>
                </p>
                <?php ?>
                <style>
                    .tokenInputClassProd{
                        max-height: 23px !important;           
                    }
                </style>
                <script>
                    jQuery(document).ready(function () {
                        var criteriaVal = "<?php echo $bis_criteria ?>";
                        var textVal = '<?php echo $bis_criteria_text_val ?>';
                        var drop_down_val = '<?php echo $bis_criteria_drop_down_val ?>';
                        var funcCall = "fromEdit";
                        var field = "shipping";
                        bis_ship_gen_filed_edit(criteriaVal, textVal, funcCall, field, drop_down_val);

                        jQuery("#bis_shipping_gen_field_select").change(function () {
                            var criteria = jQuery("#bis_shipping_gen_field_select").val();
                            funcCall = "fromClick";
                            bis_ship_gen_filed_edit(criteria, textVal, funcCall, field);
                        });

                        jQuery('#bis_shipping_gen_field_select').val(criteriaVal);
                    });
                </script>
            </div>
        </div>
        <?php
    }

    public function bis_save_shipping_general_box_fields($post_id) {

        $bis_criteria = null;
        $bis_dropdown = null;
        $bis_text_value = null;
        $bis_action = "shipping";

        if (isset($_POST['bis_shipping_gen_field_select'])) {
            $bis_criteria = $_POST['bis_shipping_gen_field_select'];
            update_post_meta($post_id, 'bis_shipping_gen_field_select', $bis_criteria);
        }
        if (isset($_POST['bis_shipping_gen_action'])) {
            $bis_shipping_action = $_POST['bis_shipping_gen_action'];
            update_post_meta($post_id, 'bis_shipping_gen_action', esc_attr($bis_shipping_action));
        }
        if (isset($_POST['bis_shipping_gen_criteria_value_dropdown'])) {
            $bis_dropdown = $_POST['bis_shipping_gen_criteria_value_dropdown'];
            if ($bis_criteria == "none") {
                $bis_dropdown = null;
            }
            update_post_meta($post_id, 'bis_shipping_gen_criteria_value_dropdown', $bis_dropdown);
        }
        if (isset($_POST['bis_shipping_gen_criteria_value_text'])) {
            $bis_text_value = $_POST['bis_shipping_gen_criteria_value_text'];
            if ($bis_criteria == "none") {
                $bis_text_value = null;
            }
            update_post_meta($post_id, 'bis_shipping_gen_criteria_value_text', $bis_text_value);
        }

        $bis_re_rule_detail_id = get_post_meta($post_id, 'bis_shipping_created_rule_id', true);

        if ($bis_criteria != null) {
            $product_rules_engine_model = new ProductRulesEngineModel();
            $product_rules_engine_model->bis_save_general_box_rule($bis_action, $bis_criteria, $bis_dropdown, $bis_text_value, $post_id, $bis_re_rule_detail_id, "shipping_rule", $bis_shipping_action);
        }

        $bis_rule_detail_id = RulesEngineCacheWrapper::get_session_attribute(BIS_PROD_GEN_FEILD_LOGICAL_ID);
        if ($bis_criteria == "none") {
            $bis_rule_detail_id = null;
        }
        update_post_meta($post_id, 'bis_shipping_created_rule_id', $bis_rule_detail_id);
    }

    public function bis_save_product_general_box_fields($post_id) {

        $bis_action = null;
        $bis_criteria = null;
        $bis_dropdown = null;
        $bis_text_value = null;

        if (isset($_POST['bis_pro_gen_field_select'])) {
            $bis_criteria = $_POST['bis_pro_gen_field_select'];
            update_post_meta($post_id, 'bis_pro_gen_field_select', $bis_criteria);
        }
        if (isset($_POST['bis_pro_gen_action'])) {
            $bis_action = $_POST['bis_pro_gen_action'];
            if ($bis_criteria == "none") {
                $bis_action = null;
            }
            update_post_meta($post_id, 'bis_pro_gen_action', esc_attr($bis_action));
        }
        if (isset($_POST['bis_pro_gen_criteria_value_dropdown'])) {
            $bis_dropdown = $_POST['bis_pro_gen_criteria_value_dropdown'];
            if ($bis_criteria == "none") {
                $bis_dropdown = null;
            }
            update_post_meta($post_id, 'bis_pro_gen_criteria_value_dropdown', $bis_dropdown);
        }
        if (isset($_POST['bis_pro_gen_criteria_value_text'])) {
            $bis_text_value = $_POST['bis_pro_gen_criteria_value_text'];
            if ($bis_criteria == "none") {
                $bis_text_value = null;
            }
            update_post_meta($post_id, 'bis_pro_gen_criteria_value_text', $bis_text_value);
        }

        $bis_re_rule_detail_id = get_post_meta($post_id, 'bis_pro_created_rule_id', true);

        if ($bis_criteria != null) {
            $product_rules_engine_model = new ProductRulesEngineModel();
            $product_rules_engine_model->bis_save_general_box_rule($bis_action, $bis_criteria, $bis_dropdown, $bis_text_value, $post_id, $bis_re_rule_detail_id, "product_rule");
        }

        $bis_rule_detail_id = RulesEngineCacheWrapper::get_session_attribute(BIS_PROD_GEN_FEILD_LOGICAL_ID);
        if ($bis_criteria == "none") {
            $bis_rule_detail_id = null;
        }
        update_post_meta($post_id, 'bis_pro_created_rule_id', $bis_rule_detail_id);
    }

    public function bis_custom_product_tabs($original_tabs) {

        $new_tab['bis_product_rule'] = array(
            'label' => __('Product Rules', 'productrules'),
            'target' => 'bis_gen_product_rules',
            'class' => array('show_if_simple', 'show_if_variable'),
        );
        $insert_at_position = 5;
        $tabs = array_slice($original_tabs, 0, $insert_at_position, true);
        $tabs = array_merge($tabs, $new_tab);
        $tabs = array_merge($tabs, array_slice($original_tabs, $insert_at_position, null, true));
        return $tabs;
    }

    /* This method is used for creating payment rule tab
     * @ Param $original_tabs
     * @ return $tabs
     */

    public function bis_custom_payment_tabs($original_tabs) {

        $new_tab['bis_payment_rule'] = array(
            'label' => __('Payment Rules', 'productrules'),
            'target' => 'bis_gen_payment_rules',
            'class' => array('show_if_simple', 'show_if_variable'),
        );
        $insert_at_position = 7;
        $tabs = array_slice($original_tabs, 0, $insert_at_position, true);
        $tabs = array_merge($tabs, $new_tab);
        $tabs = array_merge($tabs, array_slice($original_tabs, $insert_at_position, null, true));
        return $tabs;
    }

    public function bis_custom_shipping_tabs($original_tabs) {

        $new_tab['bis_shipping_rule'] = array(
            'label' => __('Shipping Rules', 'productrules'),
            'target' => 'bis_gen_shipping_rules',
            'class' => array('show_if_simple', 'show_if_variable'),
        );
        $insert_at_position = 6;
        $tabs = array_slice($original_tabs, 0, $insert_at_position, true);
        $tabs = array_merge($tabs, $new_tab);
        $tabs = array_merge($tabs, array_slice($original_tabs, $insert_at_position, null, true));
        return $tabs;
    }

    /*
     * This method is used for applying the rules for shipping. 
     */

    function bis_conditional_shipping_enable_disable() {

        if (!isset(WC()->cart)) {
            return;
        }

        $post_rule_modal = new PostRulesEngineModel ();
        $applied_product_rules = $post_rule_modal->get_applied_plugin_rules(BIS_WOO_PRODUCT_TYPE_RULE);
        $product_name = "";
        
        $geo_location_rule_arrays = $this->bis_eval_rules_based_on_shipping_location($applied_product_rules);
        
        $cart_contents = WC()->cart->cart_contents;
        $cart_product_ids = wp_list_pluck($cart_contents, 'product_id');
        
        if (!empty($applied_product_rules) && $applied_product_rules != null) {
            foreach ($applied_product_rules as $applied_product_rule) {
                if ($applied_product_rule->action == 'shipping') {
                    if ($applied_product_rule->parent_type_value == "product" || $applied_product_rule->parent_type_value == "external_product" || $applied_product_rule->parent_type_value == "variable_product") {
                        if (in_array($applied_product_rule->lrId, $geo_location_rule_arrays['status_true']) && $applied_product_rule->gencol2 == "disable" && array_intersect(array($applied_product_rule->parent_id), $cart_product_ids)) {
                            if ($product_name != "") {
                                $product_name = get_the_title($applied_product_rule->parent_id) . ", " . $product_name;
                            } else {
                                $product_name = get_the_title($applied_product_rule->parent_id);
                            }
                        } elseif (in_array($applied_product_rule->lrId, $geo_location_rule_arrays['status_false']) && $applied_product_rule->gencol2 == "enable" && array_intersect(array($applied_product_rule->parent_id), $cart_product_ids)) {
                            if ($product_name != "") {
                                $product_name = get_the_title($applied_product_rule->parent_id) . ", " . $product_name;
                            } else {
                                $product_name = get_the_title($applied_product_rule->parent_id);
                            }
                        } elseif ($applied_product_rule->gencol2 == "disable" && $applied_product_rule->parent_id == "bis_ship_all" && in_array($applied_product_rule->lrId, $geo_location_rule_arrays['status_true'])) {
                            if ($product_name != "") {
                                $product_name = get_the_title($applied_product_rule->parent_id) . ", " . $product_name;
                            } else {
                                $product_name = get_the_title($applied_product_rule->parent_id);
                            }
                        } elseif ($applied_product_rule->gencol2 == "enable" && $applied_product_rule->parent_id == "bis_ship_all" && in_array($applied_product_rule->lrId, $geo_location_rule_arrays['status_false'])) {
                            if ($product_name != "") {
                                $product_name = get_the_title($applied_product_rule->parent_id) . ", " . $product_name;
                            } else {
                                $product_name = get_the_title($applied_product_rule->parent_id);
                            }
                        }
                    } elseif ($applied_product_rule->parent_type_value == "grouped_product") {
                        $grouped_product = wc_get_product($applied_product_rule->parent_id);
                        $children_products = $grouped_product->get_children();
                        if (in_array($applied_product_rule->lrId, $geo_location_rule_arrays['status_true']) && $applied_product_rule->gencol2 == "disable" && array_intersect($children_products, $cart_product_ids)) {
                            if ($product_name != "") {
                                $product_name = get_the_title($applied_product_rule->parent_id) . " grouped products" . ", " . $product_name;
                            } else {
                                $product_name = get_the_title($applied_product_rule->parent_id) . " grouped products";
                            }
                        } elseif (in_array($applied_product_rule->lrId, $geo_location_rule_arrays['status_false']) && $applied_product_rule->gencol2 == "enable" && array_intersect($children_products, $cart_product_ids)) {
                            if ($product_name != "") {
                                $product_name = get_the_title($applied_product_rule->parent_id) . " grouped products" . ", " . $product_name;
                            } else {
                                $product_name = get_the_title($applied_product_rule->parent_id) . " grouped products";
                            }
                        }
                    } else {
                        $product_engine_model = new ProductRulesEngineModel();
                        $term_ids = $product_engine_model->get_product_term_ids($cart_product_ids, $applied_product_rule->parent_type_value);
                        $ruled_term_ids = array($applied_product_rule->parent_id);
                        if (in_array($applied_product_rule->lrId, $geo_location_rule_arrays['status_true']) && $applied_product_rule->gencol2 == "disable" && array_intersect($ruled_term_ids, $term_ids)) {
                            if ($applied_product_rule->parent_type_value == "categories") {
                                if ($product_name != "") {
                                    $product_name = get_term_by('id', $applied_product_rule->parent_id, 'product_cat')->name . " category" . ", " . $product_name;
                                } else {
                                    $product_name = get_term_by('id', $applied_product_rule->parent_id, 'product_cat')->name . " category";
                                }
                            } elseif ($applied_product_rule->parent_type_value == "woo-tags") {
                                if ($product_name != "") {
                                    $product_name = get_term_by('slug', $applied_product_rule->parent_id, 'product_tag')->name . " category" . ", " . $product_name;
                                } else {
                                    $product_name = get_term_by('slug', $applied_product_rule->parent_id, 'product_tag')->name . " category";
                                }
                            } elseif ($applied_product_rule->parent_type_value == "attributes" || $applied_product_rule->parent_type_value == "custom_taxo") {
                                if ($product_name != "") {
                                    $product_name = $applied_product_rule->parent_id . " product" . ", " . $product_name;
                                } else {
                                    $product_name = $applied_product_rule->parent_id . " product";
                                }
                            }
                        } elseif (in_array($applied_product_rule->lrId, $geo_location_rule_arrays['status_false']) && $applied_product_rule->gencol2 == "enable" && array_intersect($ruled_term_ids, $term_ids)) {
                            if ($applied_product_rule->parent_type_value == "categories") {
                                if ($product_name != "") {
                                    $product_name = get_term_by('id', $applied_product_rule->parent_id, 'product_cat')->name . " category" . ", " . $product_name;
                                } else {
                                    $product_name = get_term_by('id', $applied_product_rule->parent_id, 'product_cat')->name . " category";
                                }
                            } elseif ($applied_product_rule->parent_type_value == "woo-tags") {
                                if ($product_name != "") {
                                    $product_name = get_term_by('slug', $applied_product_rule->parent_id, 'product_tag')->name . " category" . ", " . $product_name;
                                } else {
                                    $product_name = get_term_by('slug', $applied_product_rule->parent_id, 'product_tag')->name . " category";
                                }
                            } elseif ($applied_product_rule->parent_type_value == "attributes" || $applied_product_rule->parent_type_value == "custom_taxo") {
                                if ($product_name != "") {
                                    $product_name = $applied_product_rule->parent_id . " product" . ", " . $product_name;
                                } else {
                                    $product_name = $applied_product_rule->parent_id . " product";
                                }
                            }
                        }
                    }
                }
            }
            if ($product_name != "") {
                if(isset($applied_product_rule->gencol5) && $applied_product_rule->gencol5 !== ""){
                    wc_add_notice(__($applied_product_rule->gencol5), 'error');
                } else {
                    wc_add_notice(__('Following Product(s) ' . $product_name . ' cannot be shipped to your location.'), 'error');
                }
            }
        }

        $excluded_posts_id_array = $this->bis_re_get_excluded_post_ids();
        $this->evaluate_request_post_rules();
        $applied_request_rules = $this->get_request_rules();
        $merged_excluded_posts = null;
        $applied_request_post_rules = null;
        $product_name = "";

        if ($applied_request_rules != null && !empty($applied_request_rules)) {
            $post_rule_modal = new PostRulesEngineModel ();
            $applied_request_post_rules = $post_rule_modal->get_applied_post_rules($applied_request_rules);
        }

        if (!empty($excluded_posts_id_array)) {
            $merged_excluded_posts = $excluded_posts_id_array;
        }

        if ($applied_request_post_rules != null && !empty($applied_request_post_rules)) {
            $excluded_posts_id_req_array = RulesEngineUtil::get_applied_post_rule_ids($applied_request_post_rules);

            if ($merged_excluded_posts == null) {
                $merged_excluded_posts = $excluded_posts_id_req_array;
            } else {
                $merged_excluded_posts = array_merge($merged_excluded_posts, $excluded_posts_id_req_array);
            }
        }

        if (!empty($merged_excluded_posts)) {
            foreach ($merged_excluded_posts as $key => $merged_excluded_post) {
                if (in_array($merged_excluded_post->lrId, $geo_location_rule_arrays['logical_ids'])) {
                    unset($merged_excluded_posts[$key]);
                }
            }
        }

        if (!empty($merged_excluded_posts)) {

            foreach ($merged_excluded_posts as $merged_excluded_post) {
                if ($merged_excluded_post->action == 'shipping') {
                    if ($merged_excluded_post->parent_type_value == "product" || $merged_excluded_post->parent_type_value == "external_product" || $merged_excluded_post->parent_type_value == "variable_product") {
                        if ($merged_excluded_post->gencol2 == "disable" && array_intersect(array($merged_excluded_post->parent_id), $cart_product_ids)) {
                            if ($product_name != "") {
                                $product_name = get_the_title($merged_excluded_post->parent_id) . ", " . $product_name;
                            } else {
                                $product_name = get_the_title($merged_excluded_post->parent_id);
                            }
                        } elseif ($merged_excluded_post->gencol2 == "disable" && $merged_excluded_post->parent_id == "bis_ship_all") {
                            if ($product_name != "") {
                                $product_name = get_the_title($merged_excluded_post->parent_id) . ", " . $product_name;
                            } else {
                                $product_name = get_the_title($merged_excluded_post->parent_id);
                            }
                        }
                    } elseif ($merged_excluded_post->parent_type_value == "grouped_product") {
                        $grouped_product = wc_get_product($merged_excluded_post->parent_id);
                        $children_products = $grouped_product->get_children();
                        if ($merged_excluded_post->gencol2 == "disable" && array_intersect($children_products, $cart_product_ids)) {
                            if ($product_name != "") {
                                $product_name = get_the_title($merged_excluded_post->parent_id) . " grouped products" . ", " . $product_name;
                            } else {
                                $product_name = get_the_title($merged_excluded_post->parent_id) . " grouped products";
                            }
                        } elseif ($merged_excluded_post->gencol2 == "enable" && array_intersect($children_products, $cart_product_ids)) {
                            if ($product_name != "") {
                                $product_name = get_the_title($merged_excluded_post->parent_id) . " grouped products" . ", " . $product_name;
                            } else {
                                $product_name = get_the_title($merged_excluded_post->parent_id) . " grouped products";
                            }
                        }
                    } else {
                        $product_engine_model = new ProductRulesEngineModel();
                        $term_ids = $product_engine_model->get_product_term_ids($cart_product_ids, $merged_excluded_post->parent_type_value);
                        $ruled_term_ids = array($merged_excluded_post->parent_id);
                        if ($merged_excluded_post->gencol2 == "disable" && array_intersect($ruled_term_ids, $term_ids)) {
                            if ($applied_product_rule->parent_type_value == "categories") {
                                if ($product_name != "") {
                                    $product_name = get_term_by('id', $applied_product_rule->parent_id, 'product_cat')->name . " category" . ", " . $product_name;
                                } else {
                                    $product_name = get_term_by('id', $applied_product_rule->parent_id, 'product_cat')->name . " category";
                                }
                            } elseif ($applied_product_rule->parent_type_value == "woo-tags") {
                                if ($product_name != "") {
                                    $product_name = get_term_by('slug', $applied_product_rule->parent_id, 'product_tag')->name . " category" . ", " . $product_name;
                                } else {
                                    $product_name = get_term_by('slug', $applied_product_rule->parent_id, 'product_tag')->name . " category";
                                }
                            } elseif ($applied_product_rule->parent_type_value == "attributes" || $applied_product_rule->parent_type_value == "custom_taxo") {
                                if ($product_name != "") {
                                    $product_name = $applied_product_rule->parent_id . " product" . ", " . $product_name;
                                } else {
                                    $product_name = $applied_product_rule->parent_id . " product";
                                }
                            }
                        }/* elseif ($error_count == 0 && $merged_excluded_post->gencol2 == "enable" && array_intersect($ruled_cat_ids, $cat_ids)) {
                          wc_add_notice(__('There are some products in your cart that cannot be shipped to your location.'), 'error');
                          $error_count++;
                          break;
                          } */
                    }
                }
            }
            if ($product_name != "") {
                if (isset($applied_product_rule->gencol5) && $applied_product_rule->gencol5 !== "") {
                    wc_add_notice(__($applied_product_rule->gencol5), 'error');
                } else {
                    wc_add_notice(__('Following Product(s) ' . $product_name . ' cannot be shipped.'), 'error');
                }
            }
        }

        $product_name = "";
        if (!empty($applied_product_rules)) {
            foreach ($applied_product_rules as $applied_product_rule) {
                $distance_rule = false;
                $applied_product_rules_criteria = $post_rule_modal->get_rule_criteria_by_logical_rules(array($applied_product_rule));
                foreach ($applied_product_rules_criteria as $rule_criterias) {
                    foreach ($rule_criterias as $rule_criteri1a) {
                        if($rule_criteri1a->subOptId == "44"){
                            $distance_rule = true;
                            break;
                        }
                    }
                }
                if ($distance_rule == false && $applied_product_rule->action == "shipping" && $applied_product_rule->gencol2 == "enable" && empty($merged_excluded_posts) && !(in_array($applied_product_rule->lrId, $geo_location_rule_arrays['logical_ids']))) {
                    $applied_product_rule->ruleId = $applied_product_rule->lrId;
                    if ($applied_product_rule->parent_type_value == "product" || $applied_product_rule->parent_type_value == "external_product" || $applied_product_rule->parent_type_value == "variable_product") {
                        if (array_intersect(array($applied_product_rule->parent_id), $cart_product_ids)) {
                            if ($product_name != "") {
                                $product_name = get_the_title($applied_product_rule->parent_id) . ", " . $product_name;
                            } else {
                                $product_name = get_the_title($applied_product_rule->parent_id);
                            }
                        } elseif ($applied_product_rule->parent_id == "bis_ship_all") {
                            if ($product_name != "") {
                                $product_name = get_the_title($applied_product_rule->parent_id) . ", " . $product_name;
                            } else {
                                $product_name = get_the_title($applied_product_rule->parent_id);
                            }
                        }
                    } elseif ($applied_product_rule->parent_type_value == "grouped_product") {
                        $grouped_product = wc_get_product($applied_product_rule->parent_id);
                        $children_products = $grouped_product->get_children();
                        if (array_intersect($children_products, $cart_product_ids)) {
                            if ($product_name != "") {
                                $product_name = get_the_title($applied_product_rule->parent_id) . " grouped products" . ", " . $product_name;
                            } else {
                                $product_name = get_the_title($applied_product_rule->parent_id) . " grouped products";
                            }
                        }
                    } else {
                        $product_engine_model = new ProductRulesEngineModel();
                        $term_ids = $product_engine_model->get_product_term_ids($cart_product_ids, $applied_product_rule->parent_type_value);
                        $ruled_term_ids = array($applied_product_rule->parent_id);
                        if (array_intersect($ruled_term_ids, $term_ids)) {
                            if ($applied_product_rule->parent_type_value == "categories") {
                                if ($product_name != "") {
                                    $product_name = get_term_by('id', $applied_product_rule->parent_id, 'product_cat')->name . " category" . ", " . $product_name;
                                } else {
                                    $product_name = get_term_by('id', $applied_product_rule->parent_id, 'product_cat')->name . " category";
                                }
                            } elseif ($applied_product_rule->parent_type_value == "woo-tags") {
                                if ($product_name != "") {
                                    $product_name = get_term_by('slug', $applied_product_rule->parent_id, 'product_tag')->name . " category" . ", " . $product_name;
                                } else {
                                    $product_name = get_term_by('slug', $applied_product_rule->parent_id, 'product_tag')->name . " category";
                                }
                            } elseif ($applied_product_rule->parent_type_value == "attributes" || $applied_product_rule->parent_type_value == "custom_taxo") {
                                if ($product_name != "") {
                                    $product_name = $applied_product_rule->parent_id . " product" . ", " . $product_name;
                                } else {
                                    $product_name = $applied_product_rule->parent_id . " product";
                                }
                            }
                        }
                    }
                }
            }
            if ($product_name != "") {
                if (isset($applied_product_rule->gencol5) && $applied_product_rule->gencol5 !== "") {
                    wc_add_notice(__($applied_product_rule->gencol5), 'error');
                } else {
                    wc_add_notice(__('Following Product(s) ' . $product_name . ' cannot be shipped.'), 'error');
                }
            }
        }
    }

    /*
     * This method is used for apply the rules for payment gateways.
     * @param $payment_methods
     * @return $payment_methods
     */

    public function bis_conditional_payment_gateways($payment_methods) {

        if (!isset(WC()->cart)) {
            return $payment_methods;
        }

        $cart_contents = WC()->cart->cart_contents;
        $cart_product_ids = wp_list_pluck($cart_contents, 'product_id');
        
        $post_rule_modal = new PostRulesEngineModel ();
        $applied_product_rules = $post_rule_modal->get_applied_plugin_rules(BIS_WOO_PRODUCT_TYPE_RULE);

        $geo_location_rule_arrays = $this->bis_eval_rules_based_on_shipping_location($applied_product_rules);

        if (!empty($applied_product_rules) && $applied_product_rules != null) {
            foreach ($applied_product_rules as $applied_product_rule) {
                if ($applied_product_rule->action == 'payment') {
                    if ($applied_product_rule->parent_type_value == "product" || $applied_product_rule->parent_type_value == "external_product" || $applied_product_rule->parent_type_value == "variable_product") {
                        if (in_array($applied_product_rule->lrId, $geo_location_rule_arrays['status_true']) && $applied_product_rule->gencol2 == "disable" && array_intersect(array($applied_product_rule->parent_id), $cart_product_ids)) {
                            $selected_payment_methods = json_decode($applied_product_rule->gencol3);
                            foreach ($selected_payment_methods as $payment_method) {
                                unset($payment_methods[$payment_method]);
                            }
                            break;
                        } elseif (in_array($applied_product_rule->lrId, $geo_location_rule_arrays['status_false']) && $applied_product_rule->gencol2 == "enable" && array_intersect(array($applied_product_rule->parent_id), $cart_product_ids)) {
                            $selected_payment_methods = json_decode($applied_product_rule->gencol3);
                            foreach ($selected_payment_methods as $payment_method) {
                                unset($payment_methods[$payment_method]);
                            }
                            break;
                        }
                    } elseif ($applied_product_rule->parent_type_value == "grouped_product") {
                        $grouped_product = wc_get_product($applied_product_rule->parent_id);
                        $children_products = $grouped_product->get_children();
                        if (in_array($applied_product_rule->lrId, $geo_location_rule_arrays['status_true']) && $applied_product_rule->gencol2 == "disable" && array_intersect($children_products, $cart_product_ids)) {
                            $selected_payment_methods = json_decode($applied_product_rule->gencol3);
                            foreach ($selected_payment_methods as $payment_method) {
                                unset($payment_methods[$payment_method]);
                            }
                            break;
                        } elseif (in_array($applied_product_rule->lrId, $geo_location_rule_arrays['status_false']) && $applied_product_rule->gencol2 == "enable" && array_intersect($children_products, $cart_product_ids)) {
                            $selected_payment_methods = json_decode($applied_product_rule->gencol3);
                            foreach ($selected_payment_methods as $payment_method) {
                                unset($payment_methods[$payment_method]);
                            }
                            break;
                        }
                    } else {
                        $product_engine_model = new ProductRulesEngineModel();
                        $term_ids = $product_engine_model->get_product_term_ids($cart_product_ids, $applied_product_rule->parent_type_value);
                        $ruled_term_ids = array($applied_product_rule->parent_id);
                        if (in_array($applied_product_rule->lrId, $geo_location_rule_arrays['status_true']) && $applied_product_rule->gencol2 == "disable" && array_intersect($ruled_term_ids, $term_ids)) {
                            $selected_payment_methods = json_decode($applied_product_rule->gencol3);
                            foreach ($selected_payment_methods as $payment_method) {
                                unset($payment_methods[$payment_method]);
                            }
                            break;
                        } elseif (in_array($applied_product_rule->lrId, $geo_location_rule_arrays['status_false']) && $applied_product_rule->gencol2 == "enable" && array_intersect($ruled_term_ids, $term_ids)) {
                            $selected_payment_methods = json_decode($applied_product_rule->gencol3);
                            foreach ($selected_payment_methods as $payment_method) {
                                unset($payment_methods[$payment_method]);
                            }
                            break;
                        }
                    }
                }
            }
        }

        $excluded_posts_id_array = $this->bis_re_get_excluded_post_ids();
        $this->evaluate_request_post_rules();
        $applied_request_rules = $this->get_request_rules();
        $merged_excluded_posts = null;
        $applied_request_post_rules = null;

        if ($applied_request_rules != null && !empty($applied_request_rules)) {
            $post_rule_modal = new PostRulesEngineModel ();
            $applied_request_post_rules = $post_rule_modal->get_applied_post_rules($applied_request_rules);
        }

        if (!empty($excluded_posts_id_array)) {
            $merged_excluded_posts = $excluded_posts_id_array;
        }

        if ($applied_request_post_rules != null && !empty($applied_request_post_rules)) {
            $excluded_posts_id_req_array = RulesEngineUtil::get_applied_post_rule_ids($applied_request_post_rules);

            if ($merged_excluded_posts == null) {
                $merged_excluded_posts = $excluded_posts_id_req_array;
            } else {
                $merged_excluded_posts = array_merge($merged_excluded_posts, $excluded_posts_id_req_array);
            }
        }

        if (!empty($merged_excluded_posts)) {
            foreach ($merged_excluded_posts as $merged_excluded_post) {
                if ($merged_excluded_post->action == 'payment') {
                    if ($merged_excluded_post->parent_type_value == "product" || $merged_excluded_post->parent_type_value == "external_product" || $merged_excluded_post->parent_type_value == "variable_product") {
                        if (($merged_excluded_post->gencol2 == "disable" && array_intersect(array($merged_excluded_post->parent_id), $cart_product_ids)) || ($merged_excluded_post->gencol2 == "disable" && $merged_excluded_post->parent_id == "bis_payment_all")) {
                            $selected_payment_methods = json_decode($merged_excluded_post->gencol3);
                            foreach ($selected_payment_methods as $payment_method) {
                                unset($payment_methods[$payment_method]);
                            }
                            break;
                        }
                    } elseif ($merged_excluded_post->parent_type_value == "grouped_product") {
                        $grouped_product = wc_get_product($merged_excluded_post->parent_id);
                        $children_products = $grouped_product->get_children();
                        if ($merged_excluded_post->gencol2 == "disable" && array_intersect($children_products, $cart_product_ids)) {
                            $selected_payment_methods = json_decode($merged_excluded_post->gencol3);
                            foreach ($selected_payment_methods as $payment_method) {
                                unset($payment_methods[$payment_method]);
                            }
                            break;
                        }
                    } else {
                        $product_engine_model = new ProductRulesEngineModel();
                        $term_ids = $product_engine_model->get_product_term_ids($cart_product_ids, $merged_excluded_post->parent_type_value);
                        $ruled_term_ids = array($merged_excluded_post->parent_id);
                        if ($merged_excluded_post->gencol2 == "disable" && array_intersect($ruled_term_ids, $term_ids)) {
                            $selected_payment_methods = json_decode($merged_excluded_post->gencol3);
                            foreach ($selected_payment_methods as $payment_method) {
                                unset($payment_methods[$payment_method]);
                            }
                            break;
                        }
                    }
                }
            }
        }

        $post_rule_modal = new PostRulesEngineModel ();
        $applied_product_rules = $post_rule_modal->get_applied_plugin_rules(BIS_WOO_PRODUCT_TYPE_RULE);
        if (!empty($applied_product_rules)) {
            foreach ($applied_product_rules as $applied_product_rule) {
                if ($applied_product_rule->action == "payment" && $applied_product_rule->gencol2 == "enable" && empty($merged_excluded_posts) && !(in_array($applied_product_rule->lrId, $geo_location_rule_arrays['logical_ids']))) {
                    if ($applied_product_rule->parent_type_value == "product" || $applied_product_rule->parent_type_value == "external_product" || $applied_product_rule->parent_type_value == "variable_product") {
                        if ((array_intersect(array($applied_product_rule->parent_id), $cart_product_ids)) || ($applied_product_rule->gencol2 == "enable" && $applied_product_rule->parent_id == "bis_payment_all")) {
                            $selected_payment_methods = json_decode($applied_product_rule->gencol3);
                            foreach ($selected_payment_methods as $payment_method) {
                                unset($payment_methods[$payment_method]);
                            }
                        }
                    } elseif ($applied_product_rule->parent_type_value == "grouped_product") {
                        $grouped_product = wc_get_product($applied_product_rule->parent_id);
                        $children_products = $grouped_product->get_children();
                        if (array_intersect($children_products, $cart_product_ids)) {
                            $selected_payment_methods = json_decode($applied_product_rule->gencol3);
                            foreach ($selected_payment_methods as $payment_method) {
                                unset($payment_methods[$payment_method]);
                            }
                            break;
                        }
                    } else {
                        $product_engine_model = new ProductRulesEngineModel();
                        $term_ids = $product_engine_model->get_product_term_ids($cart_product_ids, $applied_product_rule->parent_type_value);
                        $ruled_term_ids = array($applied_product_rule->parent_id);
                        if (array_intersect($ruled_term_ids, $term_ids)) {
                            $selected_payment_methods = json_decode($applied_product_rule->gencol3);
                            foreach ($selected_payment_methods as $payment_method) {
                                unset($payment_methods[$payment_method]);
                            }
                        }
                    }
                }
            }
        }

        return $payment_methods;
    }

    /*
     * This method is used for add product fields to mega product rules tab.
     */

    public function bis_woo_settings_product_rule_section_fields($product_section) {

        $logical_rule_engine_modal = new LogicalRulesEngineModel();
        $bis_re_rule_options = $logical_rule_engine_modal->get_rules_options(BIS_PROD_GEN_FIELD_RULE);
        $bis_woo_sett_pro_rule = get_option('bis_woo_sett_pro_rule', array());
        if (!empty($bis_woo_sett_pro_rule) && isset($bis_woo_sett_pro_rule['product_criteria'])) {
            $bis_criteria = $bis_woo_sett_pro_rule['product_criteria'];
        } else {
            $bis_criteria = null;
        }
        if (isset($bis_woo_sett_pro_rule['product_dropdown_val'])) {
            $bis_criteria_drop_down_val = $bis_woo_sett_pro_rule['product_dropdown_val'];
        } else {
            $bis_criteria_drop_down_val = null;
        }
        if (isset($bis_woo_sett_pro_rule['product_txt_val'])) {
            $bis_criteria_text_val = $bis_woo_sett_pro_rule['product_txt_val'];
        } else {
            $bis_criteria_text_val = null;
        }
        if (isset($bis_woo_sett_pro_rule['product_action'])) {
            $action = $bis_woo_sett_pro_rule['product_action'];
        } else {
            $action = null;
        }
        ?>
        <table class="form-table bis-mega-tab-class">
            <tbody>
                <tr valign="top" class="">
                    <th scope="row" class="titledesc"><?php _e("Select Action", "productrules"); ?></span></th>
                    <td class="forminp">
                        <select name="bis_woo_sett_pro_rule[product_action]" style="min-width:150px;" id="bis_woo_sett_pro_rule_action">
                            <option value="hide_post"><?php _e("Hide", "productrules"); ?></option>
                            <option value="show_post"><?php _e("Show", "productrules"); ?></option>
                        </select>
                    </td>
                </tr>
                <tr valign="top" class="">
                    <th scope="row" class="titledesc"><?php _e("Select Criteria", "productrules"); ?></th>
                    <td class="forminp">
                        <select name="bis_woo_sett_pro_rule[product_criteria]" style="min-width:150px;" id="bis_woo_sett_pro_rule_criteria">
                            <option value="none"> <?php echo "None" ?></option>
                            <?php
                            foreach ($bis_re_rule_options as $row) {
                                $bis_sub_options = $logical_rule_engine_modal->get_rules_sub_options($row->id, BIS_PRODUCT_PLUGIN_ID);
                                $bis_sub_options = RulesEngineLocalization::get_localized_values($bis_sub_options);
                                ?>
                                <optgroup label="<?php echo __($row->name, 'rulesengine'); ?>">
                                    <?php foreach ($bis_sub_options as $sub_row) { ?>
                                        <?php
                                        if ($bis_criteria == $row->id . '_' . $sub_row->id) {
                                            $is_selected = "selected=\"selected\"";
                                        } else {
                                            $is_selected = "";
                                        }
                                        ?>
                                        <option value="<?php echo $row->id . '_' . $sub_row->id ?>"
                                                <?php echo $is_selected ?> id="<?php echo $sub_row->id ?>"> <?php echo __($sub_row->name, 'rulesengine'); ?>
                                        </option>
                                        <?php
                                    }
                                    ?>        
                                </optgroup>
                            <?php } ?>  
                            <optgroup label="Logical Rule">
                                <option value="bis_logical_rule"><?php echo "Logical Rule" ?></option>
                            </optgroup>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row" class="titledesc"><?php _e("Enter Criteria Value", "productrules"); ?></th>
                    <td>
                        <input type="text" value="<?php ?>" class="short" name="bis_woo_sett_pro_rule[product_txt_val]" id="bis_woo_sett_pro_rule_criteria_txt_val" style="display: block" placeholder="Enter value">
                        <select  class="select short " name="bis_woo_sett_pro_rule[product_dropdown_val]" id="bis_woo_sett_pro_rule_criteria_dropdown_val" style="display: none">

                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
        <script>
            jQuery(document).ready(function () {
                var criteriaVal = "<?php echo $bis_criteria ?>";
                var textVal = '<?php echo $bis_criteria_text_val ?>';
                var dropdownVal = '<?php echo $bis_criteria_drop_down_val ?>';
                var funcCall = "fromEdit";
                var section = "pro";
                bis_woo_sett_mega_tab(criteriaVal, textVal, dropdownVal, funcCall, section);
                jQuery("#bis_woo_sett_pro_rule_criteria").change(function () {
                    var criteria = jQuery("#bis_woo_sett_pro_rule_criteria").val();
                    var funcCall = "fromClick";
                    var section = "pro";
                    bis_prod_gen_filed_edit(criteria, funcCall, section);
                });

                jQuery('#bis_woo_sett_pro_rule_action').val('<?php echo $action ?>');

            });
        </script>
        <?php
    }

    /*
     * This method is used for add shipping fields to mega product rules tab.
     */

    public function bis_woo_settings_shipping_rule_section_fields($shipping_section) {

        $logical_rule_engine_modal = new LogicalRulesEngineModel();
        $bis_re_rule_options = $logical_rule_engine_modal->get_rules_options(BIS_PROD_GEN_FIELD_RULE);
        $bis_woo_sett_pro_rule = get_option('bis_woo_sett_pro_rule', array());
        if (!empty($bis_woo_sett_pro_rule)) {
            $bis_criteria = $bis_woo_sett_pro_rule['shipping_criteria'];
        } else {
            $bis_criteria = null;
        }
        if (isset($bis_woo_sett_pro_rule['shipping_dropdown_val'])) {
            $bis_criteria_drop_down_val = $bis_woo_sett_pro_rule['shipping_dropdown_val'];
        } else {
            $bis_criteria_drop_down_val = null;
        }
        if (isset($bis_woo_sett_pro_rule['shipping_txt_val'])) {
            $bis_criteria_text_val = $bis_woo_sett_pro_rule['shipping_txt_val'];
        } else {
            $bis_criteria_text_val = null;
        }

        if (isset($bis_woo_sett_pro_rule['shipping_action'])) {
            $action = $bis_woo_sett_pro_rule['shipping_action'];
        } else {
            $action = null;
        }
        ?>
        <table class="form-table bis-mega-tab-class">
            <tbody>
                <tr valign="top" class="">
                    <th scope="row" class="titledesc"><?php _e("Shipping Action", "productrules"); ?></span></th>
                    <td class="forminp">
                        <select name="bis_woo_sett_pro_rule[shipping_action]" style="min-width:150px;" id="bis_woo_sett_shipping_rule_action">
                            <option value="enable"><?php _e("Enable", "productrules"); ?></option>
                            <option value="disable"><?php _e("Disable", "productrules"); ?></option>
                        </select>
                    </td>
                </tr>
                <tr valign="top" class="">
                    <th scope="row" class="titledesc"><?php _e("Select Criteria", "productrules"); ?></th>
                    <td class="forminp">
                        <select name="bis_woo_sett_pro_rule[shipping_criteria]" style="min-width:150px;" id="bis_woo_sett_shipping_rule_criteria">
                            <option value="none"> <?php echo "None" ?></option>
                            <?php
                            foreach ($bis_re_rule_options as $row) {
                                $bis_sub_options = $logical_rule_engine_modal->get_rules_sub_options($row->id, BIS_PRODUCT_PLUGIN_ID);
                                $bis_sub_options = RulesEngineLocalization::get_localized_values($bis_sub_options);
                                ?>
                                <optgroup label="<?php echo __($row->name, 'rulesengine'); ?>">
                                    <?php foreach ($bis_sub_options as $sub_row) { ?>
                                        <?php
                                        if ($bis_criteria == $row->id . '_' . $sub_row->id) {
                                            $is_selected = "selected=\"selected\"";
                                        } else {
                                            $is_selected = "";
                                        }
                                        ?>
                                        <option value="<?php echo $row->id . '_' . $sub_row->id ?>"
                                                <?php echo $is_selected ?> id="<?php echo $sub_row->id ?>"> <?php echo __($sub_row->name, 'rulesengine'); ?>
                                        </option>
                                        <?php
                                    }
                                    ?>        
                                </optgroup>
                            <?php } ?>  
                            <optgroup label="Logical Rule">
                                <option value="bis_logical_rule"><?php echo "Logical Rule" ?></option>
                            </optgroup>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row" class="titledesc"><?php _e("Enter Criteria Value", "productrules"); ?></th>
                    <td>
                        <input type="text" value="<?php ?>" class="short" name="bis_woo_sett_pro_rule[shipping_txt_val]" id="bis_woo_sett_shipping_rule_criteria_txt_val" style="display: block" placeholder="Enter value">
                        <select  class="select short " name="bis_woo_sett_pro_rule[shipping_dropdown_val]" id="bis_woo_sett_shipping_rule_criteria_dropdown_val" style="display: none">

                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
        <script>
            jQuery(document).ready(function () {
                var criteriaVal = "<?php echo $bis_criteria ?>";
                var textVal = '<?php echo $bis_criteria_text_val ?>';
                var dropdownVal = '<?php echo $bis_criteria_drop_down_val ?>';
                var funcCall = "fromEdit"
                var section = "shipping";
                bis_woo_sett_mega_tab(criteriaVal, textVal, dropdownVal, funcCall, section);
                jQuery("#bis_woo_sett_shipping_rule_criteria").change(function () {
                    var criteria = jQuery("#bis_woo_sett_shipping_rule_criteria").val();
                    var funcCall = "fromClick";
                    var section = "shipping";
                    bis_prod_gen_filed_edit(criteria, funcCall, section);
                });

                jQuery('#bis_woo_sett_shipping_rule_action').val('<?php echo $action ?>');
                //jQuery('#bis_pro_gen_field_select').val(criteriaVal);
            });
        </script>
        <?php
    }

    /*
     * This method is uused for add payment fields in mega product rules tab.
     */

    public function bis_woo_settings_payment_rule_section_fields($payment_section) {

        $logical_rule_engine_modal = new LogicalRulesEngineModel();
        $product_rule_modal = new ProductRulesEngineModel();
        $bis_re_rule_options = $logical_rule_engine_modal->get_rules_options(BIS_PROD_GEN_FIELD_RULE);
        $active_payment_methods = $product_rule_modal->get_active_payment_methods();
        $bis_woo_sett_pro_rule = get_option('bis_woo_sett_pro_rule', array());
        if (!empty($bis_woo_sett_pro_rule)) {
            $bis_criteria = $bis_woo_sett_pro_rule['payment_criteria'];
        } else {
            $bis_criteria = null;
        }
        if (isset($bis_woo_sett_pro_rule['payment_dropdown_val'])) {
            $bis_criteria_drop_down_val = $bis_woo_sett_pro_rule['payment_dropdown_val'];
        } else {
            $bis_criteria_drop_down_val = null;
        }
        if (isset($bis_woo_sett_pro_rule['payment_txt_val'])) {
            $bis_criteria_text_val = $bis_woo_sett_pro_rule['payment_txt_val'];
        } else {
            $bis_criteria_text_val = null;
        }
        if (isset($bis_woo_sett_pro_rule['payment_action'])) {
            $action = $bis_woo_sett_pro_rule['payment_action'];
        } else {
            $action = null;
        }
        if (isset($bis_woo_sett_pro_rule['payment_method'])) {
            $payment_method = $bis_woo_sett_pro_rule['payment_method'];
        } else {
            $payment_method = null;
        }
        ?>
        <table class="form-table bis-mega-tab-class">
            <tbody>
                <tr valign="top" class="">
                    <th scope="row" class="titledesc">Payment Action</span></th>
                    <td class="forminp">
                        <select name="bis_woo_sett_pro_rule[payment_action]" style="min-width:150px;" id="bis_woo_sett_payment_rule_action">
                            <option value="enable"><?php _e("Enable", "productrules"); ?></option>
                            <option value="disable"><?php _e("Disable", "productrules"); ?></option>
                        </select>
                    </td>
                </tr>
                <tr valign="top" class="">
                    <th scope="row" class="titledesc"><?php _e("Payment Methods", "productrules"); ?></span></th>
                    <td class="forminp">
                        <select class="basic-multiple" multiple="multiple" name="bis_woo_sett_pro_rule[payment_method][]" id="bis_payment_method">
                            <?php
                            if (!empty($active_payment_methods)) {
                                foreach ($active_payment_methods as $active_payment_method) {
                                    if ($payment_method != null && in_array($active_payment_method['id'], $payment_method)) {
                                        $selected = "selected='selected'";
                                    } else {
                                        $selected = "";
                                    }
                                    ?>
                                    <option  <?php echo $selected ?> value="<?php echo $active_payment_method['id'] ?>"><?php echo $active_payment_method['name'] ?></option>  
                                    <?php
                                }
                            }
                            ?>  
                        </select>
                    </td>
                </tr>
                <tr valign="top" class="">
                    <th scope="row" class="titledesc"><?php _e("Select Criteria", "productrules"); ?></th>
                    <td class="forminp">
                        <select name="bis_woo_sett_pro_rule[payment_criteria]" style="min-width:150px;" id="bis_woo_sett_payment_rule_criteria">
                            <option value="none"> <?php echo "None" ?></option>
                            <?php
                            foreach ($bis_re_rule_options as $row) {
                                $bis_sub_options = $logical_rule_engine_modal->get_rules_sub_options($row->id, BIS_PRODUCT_PLUGIN_ID);
                                $bis_sub_options = RulesEngineLocalization::get_localized_values($bis_sub_options);
                                ?>
                                <optgroup label="<?php echo __($row->name, 'rulesengine'); ?>">
                                    <?php foreach ($bis_sub_options as $sub_row) { ?>
                                        <?php
                                        if ($bis_criteria == $row->id . '_' . $sub_row->id) {
                                            $is_selected = "selected=\"selected\"";
                                        } else {
                                            $is_selected = "";
                                        }
                                        ?>
                                        <option value="<?php echo $row->id . '_' . $sub_row->id ?>"
                                                <?php echo $is_selected ?> id="<?php echo $sub_row->id ?>"> <?php echo __($sub_row->name, 'rulesengine'); ?>
                                        </option>
                                        <?php
                                    }
                                    ?>        
                                </optgroup>
                            <?php } ?>  
                            <optgroup label="Logical Rule">
                                <option value="bis_logical_rule"><?php echo "Logical Rule" ?></option>
                            </optgroup>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row" class="titledesc"><?php _e("Enter Criteria Value", "productrules"); ?></th>
                    <td>
                        <input type="text" value="<?php ?>" class="short" name="bis_woo_sett_pro_rule[payment_txt_val]" id="bis_woo_sett_payment_rule_criteria_txt_val" style="display: block" placeholder="Enter value">
                        <select  class="select short " name="bis_woo_sett_pro_rule[payment_dropdown_val]" id="bis_woo_sett_payment_rule_criteria_dropdown_val" style="display: none">

                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
        <script>
            jQuery(document).ready(function () {
                jQuery("#bis_payment_method").select2({
                    tags: true
                });
                var criteriaVal = "<?php echo $bis_criteria ?>";
                var textVal = '<?php echo $bis_criteria_text_val ?>';
                var dropdownVal = '<?php echo $bis_criteria_drop_down_val ?>';
                var funcCall = "fromEdit";
                var section = "payment";
                bis_woo_sett_mega_tab(criteriaVal, textVal, dropdownVal, funcCall, section);
                jQuery("#bis_woo_sett_payment_rule_criteria").change(function () {
                    var criteria = jQuery("#bis_woo_sett_payment_rule_criteria").val();
                    var funcCall = "fromClick";
                    var section = "payment";
                    if (criteria === "none") {
                        jQuery('#bis_payment_method').val("");
                    }
                    bis_prod_gen_filed_edit(criteria, funcCall, section);
                });
                jQuery('#bis_woo_sett_payment_rule_action').val('<?php echo $action ?>');



            });
        </script>
        <?php
    }

    /*
     * This method is used for updating the sections of mega product rules tab.
     */

    public function bis_woo_settings_product_rule_page_fields_update_option() {

        $bis_woo_sett_pro_rule = $_POST['bis_woo_sett_pro_rule'];
        $bis_woo_sett_pro_exist_rule = get_option('bis_woo_sett_pro_rule', array());
        $product_rules_engine_modal = new ProductRulesEngineModel();

        for ($i = 0; $i <= 2; $i++) {
            if ($i == 0) {
                if ((isset($bis_woo_sett_pro_rule['product_dropdown_val']) || $bis_woo_sett_pro_rule['product_txt_val'] != "") || ($bis_woo_sett_pro_rule['product_criteria'] == 'none' && $bis_woo_sett_pro_exist_rule['product_criteria'] != 'none' )) {
                    $criteria_array['criteria'] = $bis_woo_sett_pro_rule['product_criteria'];
                    if (isset($bis_woo_sett_pro_rule['product_dropdown_val'])) {
                        $criteria_array['dropdown_val'] = $bis_woo_sett_pro_rule['product_dropdown_val'];
                    } else {
                        $criteria_array['dropdown_val'] = null;
                    }
                    if (isset($bis_woo_sett_pro_rule['product_txt_val'])) {
                        $criteria_array['txt_val'] = $bis_woo_sett_pro_rule['product_txt_val'];
                    } else {
                        $bis_criteria_array['txt_val'] = null;
                    }
                    if (($bis_woo_sett_pro_rule['product_criteria'] == "none") || ($bis_woo_sett_pro_rule['product_criteria'] != "none" && $criteria_array['txt_val'] != null && $criteria_array['txt_val'] != "")) {
                        $product_rules_engine_modal->save_woo_settiings_tab_rules($criteria_array, $bis_woo_sett_pro_rule, $bis_woo_sett_pro_exist_rule, 'product_rule');
                    } else {
                        $bis_woo_sett_pro_rule['product_criteria'] = "none";
                    }
                } else {
                    $bis_woo_sett_pro_rule['product_criteria'] = "none";
                }
            } elseif ($i == 1) {
                if ((isset($bis_woo_sett_pro_rule['shipping_dropdown_val']) || $bis_woo_sett_pro_rule['shipping_txt_val'] != "") || ($bis_woo_sett_pro_rule['shipping_criteria'] == 'none' && $bis_woo_sett_pro_exist_rule['shipping_criteria'] != 'none' )) {
                    $criteria_array['criteria'] = $bis_woo_sett_pro_rule['shipping_criteria'];
                    if (isset($bis_woo_sett_pro_rule['shipping_dropdown_val'])) {
                        $criteria_array['dropdown_val'] = $bis_woo_sett_pro_rule['shipping_dropdown_val'];
                    } else {
                        $criteria_array['dropdown_val'] = null;
                    }
                    if (isset($bis_woo_sett_pro_rule['shipping_txt_val'])) {
                        $criteria_array['txt_val'] = $bis_woo_sett_pro_rule['shipping_txt_val'];
                    } else {
                        $bis_criteria_array['txt_val'] = null;
                    }
                    if (($bis_woo_sett_pro_rule['shipping_criteria'] == "none") || ($bis_woo_sett_pro_rule['shipping_criteria'] != "none" && $criteria_array['txt_val'] != null && $criteria_array['txt_val'] != "")) {
                        $product_rules_engine_modal->save_woo_settiings_tab_rules($criteria_array, $bis_woo_sett_pro_rule, $bis_woo_sett_pro_exist_rule, 'shipping_rule');
                    } else {
                        $bis_woo_sett_pro_rule['shipping_criteria'] = "none";
                    }
                } else {
                    $bis_woo_sett_pro_rule['shipping_criteria'] = "none";
                }
            } elseif ($i == 2) {
                if ((isset($bis_woo_sett_pro_rule['payment_dropdown_val']) || $bis_woo_sett_pro_rule['payment_txt_val'] != "") || ($bis_woo_sett_pro_rule['payment_criteria'] == 'none' && $bis_woo_sett_pro_exist_rule['payment_criteria'] != 'none' )) {
                    $criteria_array['criteria'] = $bis_woo_sett_pro_rule['payment_criteria'];
                    if (isset($bis_woo_sett_pro_rule['payment_dropdown_val'])) {
                        $criteria_array['dropdown_val'] = $bis_woo_sett_pro_rule['payment_dropdown_val'];
                    } else {
                        $criteria_array['dropdown_val'] = null;
                    }
                    if (isset($bis_woo_sett_pro_rule['payment_txt_val'])) {
                        $criteria_array['txt_val'] = $bis_woo_sett_pro_rule['payment_txt_val'];
                    } else {
                        $bis_criteria_array['txt_val'] = null;
                    }
                    if (isset($bis_woo_sett_pro_rule['payment_method'])) {
                        $criteria_array['payment_method'] = $bis_woo_sett_pro_rule['payment_method'];
                    } else {
                        $criteria_array['payment_method'] = "";
                    }
                    if (($bis_woo_sett_pro_rule['payment_criteria'] == "none") || ($bis_woo_sett_pro_rule['payment_criteria'] != "none" && $criteria_array['txt_val'] != null && $criteria_array['txt_val'] != "")) {
                        $product_rules_engine_modal->save_woo_settiings_tab_rules($criteria_array, $bis_woo_sett_pro_rule, $bis_woo_sett_pro_exist_rule, 'payment_rule');
                    } else {
                        $bis_woo_sett_pro_rule['payment_criteria'] = "none";
                    }
                } else {
                    $bis_woo_sett_pro_rule['payment_criteria'] = "none";
                }
            }
        }

        RulesEngineUtil::update_rule_cache_id(BIS_LOGICAL_CACHE_REFRESH_ID);
        update_option('bis_woo_sett_pro_rule', $bis_woo_sett_pro_rule);
    }

    /*
     * This method is used for add new tab to woo settings tabs.
     * @param $settings_tabs
     * @return $settings_tabs
     */

    public static function bis_woo_sett_product_rules_tab($settings_tabs) {
        $settings_tabs['bis_product_rules_tab'] = __('Mega Product Rules', 'rulesengine');
        return $settings_tabs;
    }

    /*
     * This method is used for call the mega product rules tab in woo setting page.
     */

    public static function bis_product_rules_tab() {
        woocommerce_admin_fields(ProductRulesEngine::bis_woo_sett_mega_tab_sections());
    }

    /*
     * This method is used for add sections in woo settinigs mega product rules tab.
     * @return apply_filters()
     */

    public static function bis_woo_sett_mega_tab_sections() {
        $settings = array(
            array(
                'name' => __('Product Rules'),
                'type' => 'title',
                'desc' => __('Hide or show entire products in your shop for specific criteria like Geolocation, User Role, Date etc.'),
                'id' => 'bis_woo_product_rule'
            ),
            array(
                'type' => 'bis_woo_settings_product_rule_section_fields',
                'id' => 'bis_woo_settings_product_rule_section_fields'
            ),
            array(
                'name' => __('Shipping Rules'),
                'type' => 'title',
                'desc' => __('Enable or disable shipping of entire products in your shop for specific criteria like Geolocation, User Role, Date etc.'),
                'id' => 'bis_woo_shipping_rule'
            ),
            array(
                'type' => 'bis_woo_settings_shipping_rule_section_fields',
                'id' => 'bis_woo_settings_shipping_rule_section_fields'
            ),
            array(
                'name' => __('Payment Rules'),
                'type' => 'title',
                'desc' => __('Enable or disable payment gateway using criteria like Geolocation, User Role, Date etc.'),
                'id' => 'bis_woo_payment_rule'
            ),
            array(
                'type' => 'bis_woo_settings_payment_rule_section_fields',
                'id' => 'bis_woo_settings_payment_rule_section_fields'
            ),
            array('type' => 'sectionend', 'id' => 'bis_woo_settings_product_rule_section_fields'),
        );
        return apply_filters('wc_settings_tab_bis_product_rules_tab', $settings);
    }

    /*
     * This method is used for add shipping description to product if rule applied. 
     */

    public function bis_add_shipping_desc_to_product() {
        $post = get_post();
        if (!empty($post)) {
            $current_post_id = $post->ID;
        } else {
            $current_post_id = null;
        }
        $shippinh_dec = "";
        $post_rule_modal = new PostRulesEngineModel ();
        $applied_product_rules = $post_rule_modal->get_applied_plugin_rules(BIS_WOO_PRODUCT_TYPE_RULE);

        if (!empty($applied_product_rules) && $current_post_id != null) {
            foreach ($applied_product_rules as $applied_product_rule) {
                if ($applied_product_rule->action == "shipping") {
                    if ($applied_product_rule->parent_type_value == "product") {
                        if ($applied_product_rule->parent_id == $current_post_id) {
                            $shippinh_dec = json_decode($applied_product_rule->gencol3)->bis_shipping_des;
                        }
                    } else {
                        $product_engine_model = new ProductRulesEngineModel();
                        $term_ids = $product_engine_model->get_product_term_ids(array($current_post_id), $applied_product_rule->parent_type_value);
                        $ruled_term_ids = array($applied_product_rule->parent_id);
                        if (array_intersect($ruled_term_ids, $term_ids)) {
                            $shippinh_dec = json_decode($applied_product_rule->gencol3)->bis_shipping_des;
                        }
                    }
                }
            }
        }
        ?>
        <script>
            jQuery('.woocommerce-product-details__short-description').append("<div><span style='background-color: #fcfe65;'><?php echo $shippinh_dec ?></span></div>");
            if (jQuery('.woocommerce-product-details__short-description').length === 0) {
                jQuery('.woocommerce-variation-add-to-cart').prepend("<div><span style='background-color: yellow; margin-bottom: 10px;'><?php echo $shippinh_dec ?></span></div>");
            }
        </script>
        <?php
    }

    public function bis_product_tab_icon_style() {
        $plugin_url = plugins_url('image/rules-engine-small.png', __FILE__);
        $image_url = str_replace('/action', '', $plugin_url);
        ?><style>
            #woocommerce-product-data .bis_product_rule:hover > a:before,
            #woocommerce-product-data .bis_product_rule_options > a:before,
            #woocommerce-product-data .bis_payment_rule.active:hover > a:before,
            #woocommerce-product-data .bis_payment_rule_options > a:before,
            #woocommerce-product-data .bis_shipping_rule.active:hover > a:before,
            #woocommerce-product-data .bis_shipping_rule_options > a:before {
                background: url('<?php echo $image_url ?>') center center no-repeat;
                content: " " !important;
                background-size: 100%;
                width: 13px;
                height: 13px;
                display: inline-block;
                line-height: 1;
            }
            @media only screen and (max-width: 900px) {
                #woocommerce-product-data .bis_product_rule_options.active:hover > a:before,
                #woocommerce-product-data .bis_product_rule_options > a:before,
                #woocommerce-product-data .bis_product_rule_options:hover a:before {
                    background-size: 35%;
                }
            }

            /*#woocommerce-product-data .bis_payment_rule.active:hover > a:before,
            #woocommerce-product-data .bis_payment_rule_options > a:before {
                background: url('<?php echo $image_url ?>') center center no-repeat;
                content: " " !important;
                background-size: 100%;
                width: 13px;
                height: 13px;
                display: inline-block;
                line-height: 1;
            }*/

            @media only screen and (max-width: 900px) {
                #woocommerce-product-data .bis_payment_rule_options.active:hover > a:before,
                #woocommerce-product-data .bis_payment_rule_options > a:before,
                #woocommerce-product-data .bis_payment_rule_options:hover a:before {
                    background-size: 35%;
                }
            }
            /* #woocommerce-product-data .bis_shipping_rule.active:hover > a:before,
             #woocommerce-product-data .bis_shipping_rule_options > a:before {
                 background: url('<?php echo $image_url ?>') center center no-repeat;
                             content: " " !important;
                             background-size: 100%;
                             width: 13px;
                             height: 13px;
                             display: inline-block;
                             line-height: 1;
                         }*/
            @media only screen and (max-width: 900px) {
                #woocommerce-product-data .bis_shipping_rule_options.active:hover > a:before,
                #woocommerce-product-data .bis_shipping_rule_options > a:before,
                #woocommerce-product-data .bis_shipping_rule_options:hover a:before {
                    background-size: 35%;
                }
            }
            .pricing_options:hover a:before {
                background: url('<?php echo $image_url ?>') center center no-repeat;
            }
        </style><?php
    }

    public function bis_add_shipping_charges_based_on_rules($rates, $package) {
        
        if (!isset(WC()->cart)) {
            return;
        }
        
        $post_rule_modal = new PostRulesEngineModel ();
        $applied_product_rules = $post_rule_modal->get_applied_plugin_rules(BIS_WOO_PRODUCT_TYPE_RULE);
        $geo_location_rule_arrays = $this->bis_eval_rules_based_on_shipping_location($applied_product_rules);
        $cart_contents = WC()->cart->cart_contents;
        $cart_product_ids = wp_list_pluck($cart_contents, 'product_id');
        
        if (!empty($applied_product_rules) && $applied_product_rules != null) {
            foreach ($applied_product_rules as $applied_product_rule) {
                if ($applied_product_rule->action == 'shipping' && isset($applied_product_rule->gencol2)) {
                    $shipping_charg_array = json_decode($applied_product_rule->gencol3);
                    if ($applied_product_rule->parent_type_value == "product" || $applied_product_rule->parent_type_value == "external_product" || $applied_product_rule->parent_type_value == "variable_product") {
                        if (in_array($applied_product_rule->lrId, $geo_location_rule_arrays['status_true']) && array_intersect(array($applied_product_rule->parent_id), $cart_product_ids)) {
                            $rates = $this->get_applied_shipping_charges($rates, $shipping_charg_array, $package, $applied_product_rule->gencol2);
                        } 
                    } elseif ($applied_product_rule->parent_type_value == "grouped_product") {
                        $grouped_product = wc_get_product($applied_product_rule->parent_id);
                        $children_products = $grouped_product->get_children();
                        if (in_array($applied_product_rule->lrId, $geo_location_rule_arrays['status_true']) && array_intersect($children_products, $cart_product_ids)) {
                            $rates = $this->get_applied_shipping_charges($rates, $shipping_charg_array, $package, $applied_product_rule->gencol2);
                        } 
                    } else {
                        $product_engine_model = new ProductRulesEngineModel();
                        $term_ids = $product_engine_model->get_product_term_ids($cart_product_ids, $applied_product_rule->parent_type_value);
                        $ruled_term_ids = array($applied_product_rule->parent_id);
                        if (in_array($applied_product_rule->lrId, $geo_location_rule_arrays['status_true']) && array_intersect($ruled_term_ids, $term_ids)) {
                            $rates = $this->get_applied_shipping_charges($rates, $shipping_charg_array, $package, $applied_product_rule->gencol2);
                        } 
                    }
                }
            }
        }

        $excluded_posts_id_array = $this->bis_re_get_excluded_post_ids();
        $this->evaluate_request_post_rules();
        $applied_request_rules = $this->get_request_rules();
        $merged_excluded_posts = null;
        $applied_request_post_rules = null;

        if ($applied_request_rules != null && !empty($applied_request_rules)) {
            $post_rule_modal = new PostRulesEngineModel ();
            $applied_request_post_rules = $post_rule_modal->get_applied_post_rules($applied_request_rules);
        }

        if (!empty($excluded_posts_id_array)) {
            $merged_excluded_posts = $excluded_posts_id_array;
        }

        if ($applied_request_post_rules != null && !empty($applied_request_post_rules)) {
            $excluded_posts_id_req_array = RulesEngineUtil::get_applied_post_rule_ids($applied_request_post_rules);

            if ($merged_excluded_posts == null) {
                $merged_excluded_posts = $excluded_posts_id_req_array;
            } else {
                $merged_excluded_posts = array_merge($merged_excluded_posts, $excluded_posts_id_req_array);
            }
        }
        
        if (!empty($merged_excluded_posts)) {
            foreach ($merged_excluded_posts as $key => $merged_excluded_post) {
                if (in_array($merged_excluded_post->lrId, $geo_location_rule_arrays['logical_ids'])) {
                    unset($merged_excluded_posts[$key]);
                }
            }
        }
        
        if (!empty($merged_excluded_posts)) {
            foreach ($merged_excluded_posts as $merged_excluded_post) {
                if ($merged_excluded_post->action == 'shipping' && isset($merged_excluded_post->gencol2)) {
                    $shipping_charg_array = json_decode($merged_excluded_post->gencol3);
                    if ($merged_excluded_post->parent_type_value == "product" || $merged_excluded_post->parent_type_value == "external_product" || $merged_excluded_post->parent_type_value == "variable_product") {
                        if (array_intersect(array($merged_excluded_post->parent_id), $cart_product_ids)) {
                            $rates = $this->get_applied_shipping_charges($rates, $shipping_charg_array, $package, $applied_product_rule->gencol2);
                        }
                    } elseif ($merged_excluded_post->parent_type_value == "grouped_product") {
                        $grouped_product = wc_get_product($merged_excluded_post->parent_id);
                        $children_products = $grouped_product->get_children();
                        if (array_intersect($children_products, $cart_product_ids)) {
                            $rates = $this->get_applied_shipping_charges($rates, $shipping_charg_array, $package, $applied_product_rule->gencol2);
                        }
                    } else {
                        $product_engine_model = new ProductRulesEngineModel();
                        $term_ids = $product_engine_model->get_product_term_ids($cart_product_ids, $merged_excluded_post->parent_type_value);
                        $ruled_term_ids = array($merged_excluded_post->parent_id);
                        if (array_intersect($ruled_term_ids, $term_ids)) {
                            $rates = $this->get_applied_shipping_charges($rates, $shipping_charg_array, $package, $applied_product_rule->gencol2);
                        }
                    }
                }
            }
        }

        return $rates;
    }

    public function get_applied_shipping_charges($rates, $shipping_charg_array, $package, $shipping_action) {
        
        global $woocommerce;
        $current_shipping_zone = WC_Shipping_Zones::get_zone_matching_package($package);
        if($woocommerce->version > 3){
            $current_shipping_zone_id = (string) $current_shipping_zone->get_id();
        } else {
            $current_shipping_zone_id = (string) $current_shipping_zone->get_zone_id();
        }
        $selected_shipping_methods = $shipping_charg_array->shipping_methods;
        $enabled_rates = array();
        foreach ($rates as $id => $rate) {
            if(!empty($selected_shipping_methods)){
                foreach($selected_shipping_methods as $shipping_method){
                    if(RulesEngineUtil::isContains($shipping_method, $current_shipping_zone_id)){
                        $trimmed_method = str_replace($current_shipping_zone_id.'_', '', $shipping_method);
                        if($trimmed_method == $rate->method_id){
                            if($shipping_action === "disable"){
                                unset($rates[$id]);
                            } elseif ($shipping_action === "enable") {
                                $enabled_rates[] = $rates[$id];
                            } else
                            if ($shipping_action === "charge") {
                                $rates[$id]->cost = $shipping_charg_array->shipping_charge;
                            }
                        }
                    }
                }
            }
            /*if (!RulesEngineUtil::isContains($rate->method_id, 'free_shipping')) {
                $rates[$id]->cost = $shipping_charg_array->shipping_charge;
            }*/
        }
        
        if(!empty($enabled_rates) && $shipping_action === "enable"){
            return $enabled_rates;
        } else {
            return $rates;
        }
    }
    
    public function get_continents() {
        if (empty($this->continents)) {
            $this->continents = apply_filters('woocommerce_continents', include WC()->plugin_path() . '/i18n/continents.php');
        }
        return $this->continents;
    }

    public function get_continent_code_for_country($cc) {
        $cc = trim(strtoupper($cc));
        $continents = $this->get_continents();
        $continents_and_ccs = wp_list_pluck($continents, 'countries');
        foreach ($continents_and_ccs as $continent_code => $countries) {
            if (false !== array_search($cc, $countries, true)) {
                return $continent_code;
            }
        }
        return '';
    }

    public function bis_eval_rules_based_on_shipping_location($applied_product_rules){
        
        global $woocommerce;
        $geo_location_rule_arrays = array(
            'logical_ids' => array(),
            'status_true' => array(),
            'status_false' => array()
        );
        
        if (!empty($applied_product_rules) && $applied_product_rules != null) {
            
            foreach ($applied_product_rules as $applied_product_rule) {
                $applied_product_rule->ruleId = $applied_product_rule->lrId;
            }
        
            $post_rule_modal = new PostRulesEngineModel ();
            RulesEngineCacheWrapper::delete_transient(BIS_RULE_CRITERIA_TRANSIENT_CONST);
            $applied_product_rules_criteria = $post_rule_modal->get_rule_criteria_by_logical_rules($applied_product_rules);
            RulesEngineCacheWrapper::delete_transient(BIS_RULE_CRITERIA_TRANSIENT_CONST);
            $shipping_country = WC()->customer->get_shipping_country();
            $shipping_continent = $this->get_continent_code_for_country($shipping_country);
            $shipping_state = WC()->customer->get_shipping_state();
            $shipping_city = WC()->customer->get_shipping_city();
            $shipping_postcode = WC()->customer->get_shipping_postcode();
            
            $expression = "";
            foreach ($applied_product_rules_criteria as $key => $value) {
                $logical_rules = $value;
                foreach ($logical_rules as $logical_rule) {
                    
                    for ($c = 0; (int) $logical_rule->lb > $c; $c++) {
                        $expression = $expression . "( ";
                    }
                    
                    if (($logical_rule->optId == "6" && $logical_rule->subOptId !== "44") || ($logical_rule->optId == "17")) {
                        array_push($geo_location_rule_arrays['logical_ids'], $key);
                        switch ($logical_rule->subOptId) {
                            case "4":
                                $status = RulesEngineUtil::evaluateTokenInputRule($logical_rule->value, $shipping_country, $logical_rule->condId);
                                break;
                            case "29":
                                $status = RulesEngineUtil::evaluateTokenInputRule($logical_rule->value, $shipping_city, $logical_rule->condId);
                                break;
                            case "35":
                                $currentZip = $shipping_postcode;
                                $ruleZip = $logical_rule->value;

                                if ($logical_rule->condId == "12" || $logical_rule->condId == "14") {
                                    $ruleZip = $currentZip;
                                    $currentZip = $logical_rule->value;
                                }
                                $status = RulesEngineUtil::evaluateStringTypeRule($currentZip, $ruleZip, $logical_rule->condId);
                                break;
                            case "30":
                                $status = RulesEngineUtil::evaluateTokenInputRule($logical_rule->value, $shipping_state, $logical_rule->condId);
                                break;
                            case "20":
                                $status = RulesEngineUtil::evaluateTokenInputRule($logical_rule->value, $shipping_continent, $logical_rule->condId);
                                break;
                            case "45":
                                if (!isset(WC()->cart)) {
                                    $status = false;
                                } else {
                                    $cart_sub_total = $woocommerce->cart->subtotal;
                                    $status = RulesEngineUtil::evaluateIntTypeRule((float) $cart_sub_total, (float) $logical_rule->value, $logical_rule->condId);
                                }
                                break;
                        }
                    }
                    $str_value = "F";

                    if (isset($status)) {
                        $str_value = "T";
                    }
                    
                    $expression = $expression . "" . $str_value;

                    for ($c = 0; (int) $logical_rule->rb > $c; $c++) {
                        $expression = $expression . " )";
                    }
                    
                    if ($logical_rule->operId == 1) {
                        $expression = $expression . " + ";
                    } else if ($logical_rule->operId == 2) {
                        $expression = $expression . " - ";
                    }
                }
                $rules_engine = new RulesEngine();
                $eVal = $rules_engine->evaluate_expression($expression);
                if ($eVal === true) {
                    array_push($geo_location_rule_arrays['status_true'], $key);
                } elseif($eVal === false) {
                    array_push($geo_location_rule_arrays['status_false'], $key);
                }
            }
        }
        return $geo_location_rule_arrays;
    }
    
    public function bis_conditional_shipping_based_on_distance(){
        
        if (!isset(WC()->cart)) {
            return;
        }
        $product_name = "";
        $post_rule_modal = new PostRulesEngineModel ();
        $applied_product_rules = $post_rule_modal->get_applied_plugin_rules(BIS_WOO_PRODUCT_TYPE_RULE);
        $true_rule_ids = $this->eval_shipping_rules_based_on_distance($applied_product_rules);
        $cart_contents = WC()->cart->cart_contents;
        $cart_product_ids = wp_list_pluck($cart_contents, 'product_id');

        if (!empty($true_rule_ids) && !empty($applied_product_rules) && $applied_product_rules != null) {
            foreach ($applied_product_rules as $applied_product_rule) {
                if ($applied_product_rule->action == 'shipping') {
                    if ($applied_product_rule->parent_type_value == "product" || $applied_product_rule->parent_type_value == "external_product" || $applied_product_rule->parent_type_value == "variable_product") {
                        if (in_array($applied_product_rule->lrId, $true_rule_ids) && $applied_product_rule->gencol2 == "disable" && array_intersect(array($applied_product_rule->parent_id), $cart_product_ids)) {
                            if ($product_name != "") {
                                $product_name = get_the_title($applied_product_rule->parent_id) . ", " . $product_name;
                            } else {
                                $product_name = get_the_title($applied_product_rule->parent_id);
                            }
                        } elseif (!in_array($applied_product_rule->lrId, $true_rule_ids) && $applied_product_rule->gencol2 == "enable" && array_intersect(array($applied_product_rule->parent_id), $cart_product_ids)) {
                            if ($product_name != "") {
                                $product_name = get_the_title($applied_product_rule->parent_id) . ", " . $product_name;
                            } else {
                                $product_name = get_the_title($applied_product_rule->parent_id);
                            }
                        }
                    } elseif ($applied_product_rule->parent_type_value == "grouped_product") {
                        $grouped_product = wc_get_product($applied_product_rule->parent_id);
                        $children_products = $grouped_product->get_children();
                        if (in_array($applied_product_rule->lrId, $true_rule_ids) && $applied_product_rule->gencol2 == "disable" && array_intersect($children_products, $cart_product_ids)) {
                            if ($product_name != "") {
                                $product_name = get_the_title($applied_product_rule->parent_id) . " grouped products" . ", " . $product_name;
                            } else {
                                $product_name = get_the_title($applied_product_rule->parent_id) . " grouped products";
                            }
                        } elseif (!in_array($applied_product_rule->lrId, $true_rule_ids) && $applied_product_rule->gencol2 == "enable" && array_intersect($children_products, $cart_product_ids)) {
                            if ($product_name != "") {
                                $product_name = get_the_title($applied_product_rule->parent_id) . " grouped products" . ", " . $product_name;
                            } else {
                                $product_name = get_the_title($applied_product_rule->parent_id) . " grouped products";
                            }
                        }
                    } else {
                        $product_engine_model = new ProductRulesEngineModel();
                        $term_ids = $product_engine_model->get_product_term_ids($cart_product_ids, $applied_product_rule->parent_type_value);
                        $ruled_term_ids = array($applied_product_rule->parent_id);
                        if (in_array($applied_product_rule->lrId, $true_rule_ids) && $applied_product_rule->gencol2 == "disable" && array_intersect($ruled_term_ids, $term_ids)) {
                            if ($applied_product_rule->parent_type_value == "categories") {
                                if ($product_name != "") {
                                    $product_name = get_term_by('id', $applied_product_rule->parent_id, 'product_cat')->name . " category" . ", " . $product_name;
                                } else {
                                    $product_name = get_term_by('id', $applied_product_rule->parent_id, 'product_cat')->name . " category";
                                }
                            } elseif ($applied_product_rule->parent_type_value == "woo-tags") {
                                if ($product_name != "") {
                                    $product_name = get_term_by('slug', $applied_product_rule->parent_id, 'product_tag')->name . " category" . ", " . $product_name;
                                } else {
                                    $product_name = get_term_by('slug', $applied_product_rule->parent_id, 'product_tag')->name . " category";
                                }
                            } elseif ($applied_product_rule->parent_type_value == "attributes") {
                                if ($product_name != "") {
                                    $product_name = $applied_product_rule->parent_id . " product" . ", " . $product_name;
                                } else {
                                    $product_name = $applied_product_rule->parent_id . " product";
                                }
                            }
                        } elseif (!in_array($applied_product_rule->lrId, $true_rule_ids) && $applied_product_rule->gencol2 == "enable" && array_intersect($ruled_term_ids, $term_ids)) {
                            if ($applied_product_rule->parent_type_value == "categories") {
                                if ($product_name != "") {
                                    $product_name = get_term_by('id', $applied_product_rule->parent_id, 'product_cat')->name . " category" . ", " . $product_name;
                                } else {
                                    $product_name = get_term_by('id', $applied_product_rule->parent_id, 'product_cat')->name . " category";
                                }
                            } elseif ($applied_product_rule->parent_type_value == "woo-tags") {
                                if ($product_name != "") {
                                    $product_name = get_term_by('slug', $applied_product_rule->parent_id, 'product_tag')->name . " category" . ", " . $product_name;
                                } else {
                                    $product_name = get_term_by('slug', $applied_product_rule->parent_id, 'product_tag')->name . " category";
                                }
                            } elseif ($applied_product_rule->parent_type_value == "attributes") {
                                if ($product_name != "") {
                                    $product_name = $applied_product_rule->parent_id . " product" . ", " . $product_name;
                                } else {
                                    $product_name = $applied_product_rule->parent_id . " product";
                                }
                            }
                        }
                    }
                }
            }
            if ($product_name != "") {
                if (isset($applied_product_rule->gencol5) && $applied_product_rule->gencol5 !== "") {
                    wc_add_notice(__($applied_product_rule->gencol5), 'error');
                } else {
                    wc_add_notice(__('Following Product(s) ' . $product_name . ' cannot be shipped to your location.'), 'error');
                }
            }
        }
    }

    public function bis_add_shipping_charges_based_on_distance($rates, $package) {

        
        if (!isset(WC()->cart)) {
            return;
        }
        
        $post_rule_modal = new PostRulesEngineModel ();
        $applied_product_rules = $post_rule_modal->get_applied_plugin_rules(BIS_WOO_PRODUCT_TYPE_RULE);
        $true_rule_ids = $this->eval_shipping_rules_based_on_distance($applied_product_rules);
        $cart_contents = WC()->cart->cart_contents;
        $cart_product_ids = wp_list_pluck($cart_contents, 'product_id');
        if(!empty($applied_product_rules) && $applied_product_rules !== null){
            
            foreach ($applied_product_rules as $applied_product_rule) {
                if ($applied_product_rule->action == 'shipping' && isset($applied_product_rule->gencol2)) {
                    $shipping_charg_array = json_decode($applied_product_rule->gencol3);
                    if ($applied_product_rule->parent_type_value == "product" || $applied_product_rule->parent_type_value == "external_product" || $applied_product_rule->parent_type_value == "variable_product") {
                        if (in_array($applied_product_rule->lrId, $true_rule_ids) && array_intersect(array($applied_product_rule->parent_id), $cart_product_ids)) {
                            $rates = $this->get_applied_shipping_charges($rates, $shipping_charg_array, $package, $applied_product_rule->gencol2);
                        }
                    } elseif ($applied_product_rule->parent_type_value == "grouped_product") {
                        $grouped_product = wc_get_product($applied_product_rule->parent_id);
                        $children_products = $grouped_product->get_children();
                        if (in_array($applied_product_rule->lrId, $true_rule_ids) && array_intersect($children_products, $cart_product_ids)) {
                            $rates = $this->get_applied_shipping_charges($rates, $shipping_charg_array, $package, $applied_product_rule->gencol2);
                        }
                    } else {
                        $product_engine_model = new ProductRulesEngineModel();
                        $term_ids = $product_engine_model->get_product_term_ids($cart_product_ids, $applied_product_rule->parent_type_value);
                        $ruled_term_ids = array($applied_product_rule->parent_id);
                        if (in_array($applied_product_rule->lrId, $true_rule_ids) && array_intersect($ruled_term_ids, $term_ids)) {
                            $rates = $this->get_applied_shipping_charges($rates, $shipping_charg_array, $package, $applied_product_rule->gencol2);
                        }
                    }
                }
            }
        }

        return $rates;
    }
    
    public function eval_shipping_rules_based_on_distance($applied_product_rules) {
        
        $true_rule_ids = array();
        
        if (!empty($applied_product_rules) && $applied_product_rules != null) {

            foreach ($applied_product_rules as $applied_product_rule) {
                $applied_product_rule->ruleId = $applied_product_rule->lrId;
            }

            $post_rule_modal = new PostRulesEngineModel ();
            RulesEngineCacheWrapper::delete_transient(BIS_RULE_CRITERIA_TRANSIENT_CONST);
            $applied_product_rules_criteria = $post_rule_modal->get_rule_criteria_by_logical_rules($applied_product_rules);
            RulesEngineCacheWrapper::delete_transient(BIS_RULE_CRITERIA_TRANSIENT_CONST);
            $shipping_postcode = WC()->customer->get_shipping_postcode();
            $countries = new WC_Countries();
            $store_zipcode = $countries->get_base_postcode();
            $distance = $this->get_distance_by_zipcodes($store_zipcode, $shipping_postcode);
            foreach ($applied_product_rules_criteria as $key => $value) {
                $logical_rules = $value;
                foreach ($logical_rules as $logical_rule) {
                    if($logical_rule->gencol2 === "mi" || $logical_rule->gencol2 === "MI"){
                        $distance_float = ((int)$distance/(1.609344));
                    } else {
                        $distance_float = $distance;
                    }
                    if($logical_rule->condId === "17"){
                        if ((float) $distance_float > (float) $logical_rule->value) {
                            $rule_ids[] = "T";
                        } else {
                            $rule_ids[] = "F";
                        }
                    } elseif ($logical_rule->condId === "18") {
                        if ((float) $distance_float < (float) $logical_rule->value) {
                            $rule_ids[] = "T";
                        } else {
                            $rule_ids[] = "F";
                        }
                    } elseif ($logical_rule->condId === "16") {
                        if ((float) $distance_float === (float) $logical_rule->value) {
                            $rule_ids[] = "T";
                        } else {
                            $rule_ids[] = "F";
                        }
                    }
                }
                if(isset($rule_ids) && !in_array("F", $rule_ids)){
                    $true_rule_ids[] = $key;
                }
            }
        }
        return $true_rule_ids;
    }
    
    public function get_distance_by_zipcodes($postcode1, $postcode2) {
        
        $url = BIS_DISTANCE_GOOGLE_API_URL."?origins=$postcode1&destinations=$postcode2&mode=driving&language=en-EN&sensor=false";

        $data = @file_get_contents($url);

        $result = json_decode($data, true);
        $distance = "";
        if(!empty($result) && isset($result['rows'])){
            foreach ($result['rows'] as $row) {
                if(isset($row['elements'][0]['distance']['text'])){
                    $distance = $row['elements'][0]['distance']['text'] ;
                }
            }
            if(RulesEngineUtil::isContains($distance, "km")){
                $distance = str_replace('km', '', $distance);
            }
        }
        return $distance;
    }
    
    public function bis_restrict_products_based_on_rules($noties, $prod_id, $qty){
        
        if (WC()->cart->get_cart_contents_count() == 0) {
            return $noties;
        } 
        
        $cart_contents = WC()->cart->get_cart();
        $cart_product_ids = wp_list_pluck($cart_contents, 'product_id');
        
        $excluded_posts_id_array = $this->bis_re_get_excluded_post_ids();
        $applied_request_rules = $this->get_request_rules();
        $merged_excluded_posts = null;
        $applied_request_post_rules = null;

        if ($applied_request_rules != null && !empty($applied_request_rules)) {
            $post_rule_modal = new PostRulesEngineModel();
            $applied_request_post_rules = $post_rule_modal->get_applied_post_rules($applied_request_rules);
        }

        if (!empty($excluded_posts_id_array)) {
            $merged_excluded_posts = $excluded_posts_id_array;
        }

        if ($applied_request_post_rules != null && !empty($applied_request_post_rules)) {
            $excluded_posts_id_req_array = RulesEngineUtil::get_applied_post_rule_ids($applied_request_post_rules);

            if ($merged_excluded_posts == null) {
                $merged_excluded_posts = $excluded_posts_id_req_array;
            } else {
                $merged_excluded_posts = array_merge($merged_excluded_posts, $excluded_posts_id_req_array);
            }
        }
        if (!empty($merged_excluded_posts)) {
            foreach ($merged_excluded_posts as $merged_excluded_post) {
                if ($merged_excluded_post->action == 'restrict_prod') {
                    $restrict_msg = "Sorry, you can not be add to cart this product.";
                    if($merged_excluded_post->gencol2 != "" && $merged_excluded_post->gencol2 != null){
                        $restrict_msg = $merged_excluded_post->gencol2;
                    }
                    $restrict_prod_ids = json_decode($merged_excluded_post->gencol3);
                    if ($merged_excluded_post->parent_type_value == "product" || $merged_excluded_post->parent_type_value == "external_product" || $merged_excluded_post->parent_type_value == "variable_product") {
                        if (array_intersect(array($merged_excluded_post->parent_id), $cart_product_ids)) {
                            if(in_array($prod_id, $restrict_prod_ids)){
                                $notice = __($restrict_msg, 'woocommerce');
                                $noties = false;
                            }
                        } elseif (array_intersect($restrict_prod_ids, $cart_product_ids)) {
                            if ($prod_id == $merged_excluded_post->parent_id) {
                                if ($merged_excluded_post->gencol1 != "" && $merged_excluded_post->gencol1 != null) {
                                    $restrict_msg = $merged_excluded_post->gencol1;
                                }
                                $notice = __($restrict_msg, 'woocommerce');
                                $noties = false;
                            }
                        }
                    } elseif ($merged_excluded_post->parent_type_value == "grouped_product") {
                        $grouped_product = wc_get_product($merged_excluded_post->parent_id);
                        $children_products = $grouped_product->get_children();
                        if (array_intersect($children_products, $cart_product_ids)) {
                            if (in_array($prod_id, $restrict_prod_ids)) {
                                $notice = __($restrict_msg, 'woocommerce');
                                $noties = false;
                            }
                        } elseif (array_intersect($restrict_prod_ids, $cart_product_ids)) {
                            if (in_array($prod_id, $children_products)) {
                                if ($merged_excluded_post->gencol1 != "" && $merged_excluded_post->gencol1 != null) {
                                    $restrict_msg = $merged_excluded_post->gencol1;
                                }
                                $notice = __($restrict_msg, 'woocommerce');
                                $noties = false;
                            }
                        }
                    } else {
                        $product_engine_model = new ProductRulesEngineModel();
                        $term_ids = $product_engine_model->get_product_term_ids($cart_product_ids, $merged_excluded_post->parent_type_value);
                        $ruled_term_ids = array($merged_excluded_post->parent_id);
                        if (array_intersect($ruled_term_ids, $term_ids)) {
                            if (in_array($prod_id, $restrict_prod_ids)) {
                                $notice = __($restrict_msg, 'woocommerce');
                                $noties = false;
                            }
                        } elseif (array_intersect($restrict_prod_ids, $cart_product_ids)) {
                            $prod_term_ids = $product_engine_model->get_product_term_ids(array($prod_id), $merged_excluded_post->parent_type_value);
                            if (array_intersect($prod_term_ids, $ruled_term_ids)) {
                                if ($merged_excluded_post->gencol1 != "" && $merged_excluded_post->gencol1 != null) {
                                    $restrict_msg = $merged_excluded_post->gencol1;
                                }
                                $notice = __($restrict_msg, 'woocommerce');
                                $noties = false;
                            }
                        }
                    }
                } 
            }
        }
        
        if($noties == false){
            wc_add_notice($notice, 'error');
        }
        return $noties;
    }
    
    public function bis_add_dependent_products_based_on_rules($noties, $prod_id, $qty) {
        
        $cart_contents = WC()->cart->get_cart();
        $cart_product_ids = wp_list_pluck($cart_contents, 'product_id');
        
        $excluded_posts_id_array = $this->bis_re_get_excluded_post_ids();
        $applied_request_rules = $this->get_request_rules();
        $merged_excluded_posts = null;
        $applied_request_post_rules = null;

        if ($applied_request_rules != null && !empty($applied_request_rules)) {
            $post_rule_modal = new PostRulesEngineModel();
            $applied_request_post_rules = $post_rule_modal->get_applied_post_rules($applied_request_rules);
        }

        if (!empty($excluded_posts_id_array)) {
            $merged_excluded_posts = $excluded_posts_id_array;
        }

        if ($applied_request_post_rules != null && !empty($applied_request_post_rules)) {
            $excluded_posts_id_req_array = RulesEngineUtil::get_applied_post_rule_ids($applied_request_post_rules);

            if ($merged_excluded_posts == null) {
                $merged_excluded_posts = $excluded_posts_id_req_array;
            } else {
                $merged_excluded_posts = array_merge($merged_excluded_posts, $excluded_posts_id_req_array);
            }
        }
        $added_dependent_prod = false;
        if (!empty($merged_excluded_posts)) {
            foreach ($merged_excluded_posts as $merged_excluded_post) {
                if ($merged_excluded_post->action == 'dependent_prod') {
                    $Dependent_msg = "One product being automatically added to your cart";
                    if ($merged_excluded_post->gencol2 != "" && $merged_excluded_post->gencol2 != null) {
                        $Dependent_msg = $merged_excluded_post->gencol2;
                    }
                    $dependent_prod_ids = json_decode($merged_excluded_post->gencol3);
                    if ($merged_excluded_post->parent_type_value == "product" || $merged_excluded_post->parent_type_value == "external_product" || $merged_excluded_post->parent_type_value == "variable_product") {
                        if ($merged_excluded_post->parent_id == $prod_id) {
                            foreach ($dependent_prod_ids as $dependent_prod_id) {
                                if(empty($cart_product_ids) || empty(array_intersect($cart_product_ids, array($dependent_prod_id)))){
                                    WC()->cart->add_to_cart($dependent_prod_id);
                                    $notice = __($Dependent_msg, 'woocommerce');
                                    $added_dependent_prod = true;
                                }
                            }
                        }
                    } elseif ($merged_excluded_post->parent_type_value == "grouped_product") {
                        $grouped_product = wc_get_product($merged_excluded_post->parent_id);
                        $children_products = $grouped_product->get_children();
                        if (in_array($prod_id, $children_products)) {
                            foreach ($dependent_prod_ids as $dependent_prod_id) {
                                if (empty($cart_product_ids) || empty(array_intersect($cart_product_ids, array($dependent_prod_id)))) {
                                    WC()->cart->add_to_cart($dependent_prod_id);
                                    $notice = __($Dependent_msg, 'woocommerce');
                                    $added_dependent_prod = true;
                                }
                            }
                        }
                    } else {
                        $product_engine_model = new ProductRulesEngineModel();
                        $term_ids = $product_engine_model->get_product_term_ids(array($prod_id), $merged_excluded_post->parent_type_value);
                        $ruled_term_ids = array($merged_excluded_post->parent_id);
                        if (array_intersect($ruled_term_ids, $term_ids)) {
                            foreach ($dependent_prod_ids as $dependent_prod_id) {
                                if (empty($cart_product_ids) || empty(array_intersect($cart_product_ids, array($dependent_prod_id)))) {
                                    WC()->cart->add_to_cart($dependent_prod_id);
                                    $notice = __($Dependent_msg, 'woocommerce');
                                    $added_dependent_prod = true;
                                }
                            }
                        }
                    }
                }
            } 
        }     
        if($added_dependent_prod == true){
            wc_add_notice($notice, 'success');
        }
        return $noties;
    }
    
    public function bis_remove_dependent_products_based_on_rules($cart_item_key, $cart) {
        
        if (WC()->cart->get_cart_contents_count() == 0) {
            return $noties;
        }
        
        $cart_contents = WC()->cart->get_cart();
        $cart_product_ids = wp_list_pluck($cart_contents, 'product_id');
        $removed_id = "";
        foreach ($cart_product_ids as $key => $cart_product_id) {
            if($key == $cart_item_key){
                $removed_id = $cart_product_id; 
            }
        }
        $excluded_posts_id_array = $this->bis_re_get_excluded_post_ids();
        $applied_request_rules = $this->get_request_rules();
        $merged_excluded_posts = null;
        $applied_request_post_rules = null;

        if ($applied_request_rules != null && !empty($applied_request_rules)) {
            $post_rule_modal = new PostRulesEngineModel();
            $applied_request_post_rules = $post_rule_modal->get_applied_post_rules($applied_request_rules);
        }

        if (!empty($excluded_posts_id_array)) {
            $merged_excluded_posts = $excluded_posts_id_array;
        }

        if ($applied_request_post_rules != null && !empty($applied_request_post_rules)) {
            $excluded_posts_id_req_array = RulesEngineUtil::get_applied_post_rule_ids($applied_request_post_rules);

            if ($merged_excluded_posts == null) {
                $merged_excluded_posts = $excluded_posts_id_req_array;
            } else {
                $merged_excluded_posts = array_merge($merged_excluded_posts, $excluded_posts_id_req_array);
            }
        }
        $removed_dependent_prod = false;
        if (!empty($merged_excluded_posts)) {
            foreach ($merged_excluded_posts as $merged_excluded_post) {
                if ($merged_excluded_post->action == 'dependent_prod') {
                    $Dependent_msg = "Removed dependency products";
                    if ($merged_excluded_post->gencol1 != "" && $merged_excluded_post->gencol1 != null) {
                        $Dependent_msg = $merged_excluded_post->gencol1;
                    }
                    $dependent_prod_ids = json_decode($merged_excluded_post->gencol3);
                    if(in_array($removed_id, $dependent_prod_ids)){
                        if ($merged_excluded_post->parent_type_value == "product" || $merged_excluded_post->parent_type_value == "external_product" || $merged_excluded_post->parent_type_value == "variable_product") {
                            $prod_unique_id = WC()->cart->generate_cart_id($merged_excluded_post->parent_id);
                            unset(WC()->cart->cart_contents[$prod_unique_id]);
                            $removed_dependent_prod = true;
                        } elseif ($merged_excluded_post->parent_type_value == "grouped_product") {
                            $grouped_product = wc_get_product($merged_excluded_post->parent_id);
                            $children_products = $grouped_product->get_children();
                            if(!empty($children_products)){
                                foreach ($children_products as $children_product) {
                                    $prod_unique_id = WC()->cart->generate_cart_id($children_product);
                                    unset(WC()->cart->cart_contents[$prod_unique_id]);
                                    $removed_dependent_prod = true;
                                }
                            }
                        } else {
                            $product_engine_model = new ProductRulesEngineModel();
                            $ruled_term_ids = array($merged_excluded_post->parent_id);
                            foreach ($cart_product_ids as $cart_product_id) {
                                $term_ids = $product_engine_model->get_product_term_ids(array($cart_product_id), $merged_excluded_post->parent_type_value);
                                if (array_intersect($ruled_term_ids, $term_ids)) {
                                    $prod_unique_id = WC()->cart->generate_cart_id($cart_product_id);
                                    unset(WC()->cart->cart_contents[$prod_unique_id]);
                                    $removed_dependent_prod = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if($removed_dependent_prod == true){
            $notice = __($Dependent_msg, 'woocommerce');
            wc_add_notice($notice, 'error');
        }
    }
    
    public function bis_country_dropdown_popup(){
        
        include_once BIS_PRODUCT_RULES_HOME_DIR.'/template/bis-country-popup-template.php';
    }
    
    public function bis_country_dropdown_popup_widget(){
        $productRulesEngineModal = new ProductRulesEngineModel();
        $countries = $productRulesEngineModal->get_countries();
        $current_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $selected_country_value = RulesEngineCacheWrapper::get_session_attribute(BIS_COUNTRY_DROPDOWN_VALUE);
        $session_selected_country_value = "";
        if (RulesEngineCacheWrapper::get_session_attribute(BIS_GEOLOCATION_VO) !== null) {
            $geo_values = RulesEngineCacheWrapper::get_session_attribute(BIS_GEOLOCATION_VO);
            $session_selected_country_value = $geo_values->countryCode;
            if ($selected_country_value == null && $selected_country_value == "") {
                $selected_country_value = $session_selected_country_value;
            }
        }
        if ($selected_country_value !== null && $selected_country_value !== "" && $selected_country_value !== $session_selected_country_value) {
            $country_popup_display = "none";
        } else {
            $country_popup_display = "block";
        }
        ?>
        <div class="bis_fea_country_dropdown_select">
            <form id="bis_fea_country_dropdown_form_widget" name="bis_fea_country_dropdown_form_widget" action="<?php echo $current_url; ?>" method="post">
                <select id="bis_fea_country_dropdown_select_widget" name="bis_fea_country_dropdown_select_widget">
                    <?php
                    if (!empty($countries)) {
                        foreach ($countries as $country) {
                            ?>
                    <option value="<?php echo $country->id ?>" <?php echo selected($country->id, $selected_country_value); ?> ><?php echo $country->name; ?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
            </form>
        </div>
        <style>
            .bis_fea_country_dropdown_select {
                padding: 40px;
            }
            .bis_fea_country_dropdown_select select {
                height: 33px;
                box-sizing: border-box;
                padding-left: 22px;
                width: 250px;
                max-width: 100%;
                max-height: 100%;
                border-radius: 18px;
            }
        </style>
        <script>
           jQuery(document).ready(function () {
                jQuery('#bis_fea_country_dropdown_select_widget').change(function(){
                    jQuery("#bis_fea_country_dropdown_form_widget").submit();
                });
                var selectedVal = '<?php echo $selected_country_value; ?>';
                if(selectedVal !== null && selectedVal !== ''){
                    jQuery("#bis_fea_country_dropdown_select_widget").val(selectedVal);
                }
            }); 
        </script>
        <?php
    }
    
    public function bis_add_product_rule_based_on_author($post_id){
        
        if($post_id !== null && $post_id !== ""){
            $current_post =  get_post($post_id);
            if ($current_post->post_type == "product") {
                $product_rules_engine_modal = new ProductRulesEngineModel();
                $product_rules_engine_modal->create_product_rule_based_user($current_post);
            }
            
        }
    }
   
    public function bis_clear_cached_product_rules() {
        RulesEngineCacheWrapper::remove_session_attribute(BIS_EXCLUDE_POSTS);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_APPEND_TO_POSTS);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_EXCLUDE_REQUEST_RULE_POSTS);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_RULE_TYPE_CONST . BIS_WOO_PRODUCT_TYPE_RULE);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_POST_CAT_WIDGET_EXCLUDE);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_POST_TAG_WIDGET_EXCLUDE);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_PROD_CAT_WIDGET_EXCLUDE);
        RulesEngineCacheWrapper::remove_session_attribute(BIS_PROD_TAG_WIDGET_EXCLUDE);
    }

}
