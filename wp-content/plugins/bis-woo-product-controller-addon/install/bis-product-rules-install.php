<?php

/* ######################################################################################

  Copyright (C) 2017 by MegaEdzee Technologies Pvt Ltd.  All rights reserved. This software
  is an unpublished work and trade secret of MegaEdzee Technologies, and distributed only
  under restriction.  This software (or any part of it) may not be used,
  modified, reproduced, stored on a retrieval system, distributed, or
  transmitted without the express written consent of MegaEdzee Technologies. Violation of
  the provisions contained herein may result in severe civil and criminal
  penalties, and any violators will be prosecuted to the maximum extent
  possible under the law.  Further, by using this software you acknowledge and
  agree that if this software is modified by anyone such as you, a third party
  or MegaEdzee Technologies, then MegaEdzee Technologies will have no obligation to provide support or
  maintenance for this software.

  ##################################################################################### */

use bis\repf\vo\PluginVO;
use bis\repf\install\RulesEngineAddOn;

class ProductRulesInstall {

    public static function bis_product_rules_activation() {

        if (!is_network_admin() && is_multisite()) {
            die("<span style=\"color:red\">\"Mega - Woo Product Controller for WP\" 
            should be activated from network admin. </span>"
            );
        }
        ProductRulesInstall::bis_product_activate_parent_rule();

        $pluginVO = new PluginVO();
        $pluginVO->set_id(BIS_PRODUCT_PLUGIN_ID);
        $pluginVO->set_display_name(BIS_PRODUCT_PLUGIN_DISPLAY_NAME);
        $pluginVO->set_css_class(BIS_PRODUCT_CSS_CLASS);
        $pluginVO->set_path(BIS_PRODUCT_PLUGIN_PATH);
        $pluginVO->set_description(BIS_PRODUCT_PLUGIN_DESCRIPTION);
        $pluginVO->set_version(BIS_PRODUCT_RULES_VERSION);
        $pluginVO->set_apiKey(BIS_PRODUCT_API_KEY);
        $pluginVO->set_status(1);

        RulesEngineAddOn::bis_addon_activation($pluginVO);

        // First time install
        if (RulesEngineUtil::get_option(BIS_PRODUCT_RULES_VERSION_CONST) == null) {
            RulesEngineUtil::add_option(BIS_PRODUCT_RULES_VERSION_CONST, BIS_PRODUCT_RULES_VERSION);
        } else {
            RulesEngineUtil::update_option(BIS_PRODUCT_RULES_VERSION_CONST, BIS_PRODUCT_RULES_VERSION);
        }
    }

    public static function bis_product_rules_deactivation() {

        $pluginVO = new PluginVO();
        $pluginVO->set_id(BIS_PRODUCT_PLUGIN_ID);
        $pluginVO->set_version(BIS_PRODUCT_RULES_VERSION);

        RulesEngineAddOn::bis_addon_deactivation($pluginVO);
    }

    public static function bis_product_rules_uninstall() {
        ProductRulesUtil::delete_option(BIS_PRODUCT_RULES_VERSION_CONST);
        ProductRulesUtil::delete_option(BIS_PRODUCT_PUR_CODE);
        ProductRulesUtil::delete_option(BIS_PRODUCT_POPUP_TEMPLATE);
        ProductRulesInstall::bis_addon_uninstall(BIS_PRODUCT_PLUGIN_ID);
    }

    public static function bis_product_activate_parent_rule() {
        require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        if (is_plugin_active(BIS_PRODUCT_RULESENGINE_PLATFORM . "/wp-rulesengine-platform.php") === false) {
            activate_plugins(BIS_PRODUCT_RULESENGINE_PLATFORM . "/wp-rulesengine-platform.php", true);
            wp_redirect("plugins.php");
        }
    }

    public static function bis_is_platform_installed() {

        if (ProductRulesUtil::get_option("BIS_RULES_ENGINE_PLATFORM_DIR") == false) {
            return false;
        }

        return true;
    }

    public static function bis_addon_uninstall($plugin_id) {
        $pluginArray = ProductRulesUtil::get_option("BIS_RULESENGINE_ADDONS");
        $pluginArrayObj = json_decode($pluginArray, true);

        foreach ($pluginArrayObj as $key => $pluginVO) {
            if ($pluginVO["id"] === $plugin_id) {
                unset($pluginArrayObj[$key]);
                break;
            }
        }

        ProductRulesUtil::update_option("BIS_RULESENGINE_ADDONS", json_encode($pluginArrayObj), is_network_admin());
    }

}